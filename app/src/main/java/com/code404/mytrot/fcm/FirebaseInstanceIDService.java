package com.code404.mytrot.fcm;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.etc.AtvAlrim;
import com.code404.mytrot.atv.main.AtvChart;
import com.code404.mytrot.atv.push.AtvPush;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.PicassoCircleTransformation;
import com.code404.mytrot.util.SPUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.Picasso;

import java.util.Iterator;

public class FirebaseInstanceIDService extends FirebaseMessagingService {

    /**
     * 구글 토큰을 얻는 값입니다.
     * 아래 토큰은 앱이 설치된 디바이스에 대한 고유값으로 푸시를 보낼때 사용됩니다.
     **/
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        LogUtil.e("FirebaseInstanceIDService token : " + s);
    }

    /**
     * 메시지를 받았을 경우 그 메시지에 대하여 구현하는 부분입니다.
     **/
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage != null && remoteMessage.getData().size() > 0) {
            sendNotification(remoteMessage);
        }
    }


    /**
     * remoteMessage 메시지 안애 getData와 getNotification이 있습니다.
     * 이부분은 차후 테스트 날릴때 설명 드리겠습니다.
     **/
    private void sendNotification(RemoteMessage remoteMessage) {

        try {
            Bundle extras = new Bundle();
            Iterator<String> keys = remoteMessage.getData().keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = remoteMessage.getData().get(key);
                LogUtil.d(String.format("sendNotification, %s : %s", key, value));

                extras.putString(key, value);
            }

            String title = "마이트롯";
            String type = remoteMessage.getData().get("type");
            String message = remoteMessage.getData().get("content");
            String other_member_pic1 = remoteMessage.getData().get("other_member_pic1");

//            if(type.equals("-10")) {
//                Alert.toastLong(getBaseContext(), message);
//                return;
//            }

            Intent intent = new Intent(this, AtvPush.class);

            if (CommonUtil.isAppRunning(getBaseContext())) {
                intent = new Intent(this, AtvVote.class);
                intent.setAction("start_from_push");
            }

            if(type.equals("-10")) {
                intent = new Intent(this, AtvAlrim.class);
                intent.setAction("start_from_push");
                return;
            }

            intent.putExtra("push", extras);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // use System.currentTimeMillis() to have a unique ID for the pending intent
            PendingIntent pendingIntent = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S){
                pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_IMMUTABLE);
            }else {
                pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            //String title = remoteMessage.getData().get("subject");

            if (!FormatUtil.isNullorEmpty(other_member_pic1) && !other_member_pic1.startsWith("http")) {
                other_member_pic1 = Constants.IMAGE_URL + other_member_pic1;
            }

            LogUtil.d("other_member_pic1 : " + other_member_pic1);

            /**
             * 오레오 버전부터는 Notification Channel이 없으면 푸시가 생성되지 않는 현상이 있습니다.
             * **/
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                String channel = "채널";
                String channel_nm = "채널명";

                NotificationManager notichannel = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationChannel channelMessage = new NotificationChannel(channel, channel_nm,
                        NotificationManager.IMPORTANCE_DEFAULT);
                channelMessage.setDescription("채널에 대한 설명.");
                channelMessage.enableLights(true);
                channelMessage.enableVibration(true);
                channelMessage.setShowBadge(false);
                channelMessage.setVibrationPattern(new long[]{100, 200, 100, 200});
                notichannel.createNotificationChannel(channelMessage);

                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(this, channel)
                                .setSmallIcon(R.drawable.app_icon)
                                .setVibrate(new long[]{300, 100, 300})
                                .setContentTitle(title)
                                .setContentText(message)
                                .setChannelId(channel)
                                .setContentIntent(pendingIntent)
                                .setAutoCancel(true)
                                .setColor(ContextCompat.getColor(this, R.color.white2)) // 빨간색
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);


                try {
                    if (!FormatUtil.isNullorEmpty(other_member_pic1) && other_member_pic1.startsWith("http")) {

                        Bitmap bitmap = Picasso.with(this)
                                .load(other_member_pic1)
                                .error(R.drawable.app_icon)
                                .placeholder(R.drawable.app_icon)
                                .transform(new PicassoCircleTransformation())
                                .get();

                        notificationBuilder.setLargeIcon(bitmap);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(9999, notificationBuilder.build());

            } else {
                NotificationCompat.Builder notificationBuilder =
                        new NotificationCompat.Builder(this, "")
                                .setSmallIcon(R.drawable.app_icon)
                                .setVibrate(new long[]{300, 100, 300})
                                .setContentTitle(title)
                                .setContentText(message)
                                .setAutoCancel(true)
                                .setContentIntent(pendingIntent)
                                .setColor(ContextCompat.getColor(this, R.color.white2))
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);

                try {
                    if (!FormatUtil.isNullorEmpty(other_member_pic1) && other_member_pic1.startsWith("http")) {

                        Bitmap bitmap = Picasso.with(this)
                                .load(other_member_pic1)
                                .error(R.drawable.app_icon)
                                .placeholder(R.drawable.app_icon)
                                .transform(new PicassoCircleTransformation())
                                .get();

                        notificationBuilder.setLargeIcon(bitmap);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                notificationManager.notify(9999, notificationBuilder.build());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


