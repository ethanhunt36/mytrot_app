package com.code404.mytrot.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter
{
	ArrayList<Fragment> mFragments	= new ArrayList<Fragment>();

	public MyFragmentPagerAdapter(FragmentManager fm)
	{
		super(fm);
	}

	public void addItem(Fragment f)
	{
		mFragments.add(f);
	}

	@Override
	public Fragment getItem(int position)
	{
		return mFragments.get(position);
	}

	@Override
	public int getCount()
	{
		return mFragments.size();
	}

	public void clear()
	{
		mFragments.clear();
	}

}
