package com.code404.mytrot.bean;

public class CodeBean {
    public int code = -1;
    public String codeName = "";

    public CodeBean(int code, String codeName) {
        this.code = code;
        this.codeName = codeName;
    }
}
