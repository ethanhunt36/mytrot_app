package com.code404.mytrot.bean;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.code404.mytrot.R;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.view.SquareImageView;

import java.io.File;

public class ImageBean {

    public static final String STATUS_MODY = "MODY";
    public static final String STATUS_DROP = "DROP";
    public static final String STATUS_STAY = "STAY";
    public static final String STATUS_NONE = "NONE";

    /*
    MODY : 업로드(신규/변경)
    DROP : 삭제
    STAY : 유지
    NONE : 변동없음
     */

    private String status = ImageBean.STATUS_NONE;


    private SquareImageView imgView;
    private Bitmap bitmap;
    private File file;
    private String localUri;


    public ImageBean() {

    }


    public void remove() {
        this.status = ImageBean.STATUS_DROP;
        if (imgView != null) {
            imgView.setImageResource(0);
        }
        this.file = null;
        this.bitmap = null;
        this.localUri = "";
    }


    public void setImageView(SquareImageView imgView) {
        this.imgView = imgView;
    }


    public void setImageBitmap(Bitmap bitmap) {
        setImageBitmap(bitmap, ImageView.ScaleType.CENTER_CROP);
    }

    public void setImageBitmap(Bitmap bitmap, ImageView.ScaleType scaleType) {
        if (bitmap != null) {
            imgView.setScaleType(scaleType);
            imgView.setImageBitmap(bitmap);
        } else {
            imgView.setImageResource(R.drawable.bt_camera_big);
        }
        this.bitmap = bitmap;
    }


    public void setFile(File file) {
        status = ImageBean.STATUS_MODY;
        this.file = file;
    }

    public void setLocalUri(String localUri) {
        //status = "MODY";
        this.localUri = localUri;
    }

    public boolean isExistsData() {
        if (file != null || !FormatUtil.isNullorEmpty(localUri)) {
            return true;
        }

        return false;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }


    public File getFile() {
        return file;
    }

    public String getLocalUri() {
        return localUri;
    }


    public String getStatus() {
        return status;
    }


    public String toString() {
        return String.format("status : %s, uri : %s", status, localUri);
    }

}
