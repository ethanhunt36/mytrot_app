package com.code404.mytrot.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class GMultiListView extends ListView implements IScollState, OnScrollListener {
    private GListAdapter mAdapter = null;
    private int mResourceID;

    private boolean mHasMore = true;
    private String mPageNo = "";

    private View mNoData = null;

    private boolean mIsTop = true;
    private boolean mIsBottom = false;

    private boolean mIsUpdate = false;

    private int mTotalCount = 0;

    private int mRowItemCounts = 2;


    public GMultiListView(Context context) {
        super(context);
        initialize();
    }


    public GMultiListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }


    public GMultiListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }


    private void initialize() {
        setCacheColorHint(0x00000000);
        mAdapter = new GListAdapter(getContext(), new ArrayList<ArrayList<JSONObject>>());
        setAdapter(mAdapter);
        setOnScrollListener(this);
    }


    public void setRowItemCounts(int count) {
        mRowItemCounts = count;
    }


    public void setNoData(View view) {
        mNoData = view;
        if (mNoData != null) mNoData.setVisibility(View.GONE);
    }


    @Override
    public void addHeaderView(View v) {
        setAdapter(null);
        super.addHeaderView(v);
        setAdapter(mAdapter);
    }


    @Override
    public void addFooterView(View v) {
        setAdapter(null);
        super.addFooterView(v);
        setAdapter(mAdapter);
    }


    public JSONObject getLastItem() {
        int count = getCountAll();
        LogUtil.e(count);
        if (count == 0) {
            return null;
        } else {
            ArrayList<JSONObject> items = getAdapter().getItem(count - 1);
            LogUtil.e(items.size());
            if (items.size() > 0) return items.get(items.size() - 1);
            else return null;
        }
    }


    public GListAdapter getAdapter() {
        return mAdapter;
    }


    public void addItem(JSONObject item) {
        if (mTotalCount % mRowItemCounts == 0) {
            ArrayList<JSONObject> items = new ArrayList<JSONObject>();
            items.add(item);
            mTotalCount++;
            mAdapter.add(items);
        } else {
            ArrayList<JSONObject> items = mAdapter.getItem(mTotalCount / mRowItemCounts);
            items.add(item);
            mTotalCount++;
        }
        setNoDataVisibility(getCountAll() == 0);
    }


    public void addItems(JSONObject... items) {
        for (int i = 0; i < items.length; i++) {
            addItem(items[i]);
        }

        setNoDataVisibility(getCountAll() == 0);
    }


    public void addItems(JSONArray items) {
        if (items != null && items.length() != 0) {
            int itemSize = items.length();
            for (int i = 0; i < itemSize; i++) {
                addItem(JSONUtil.getJSONObject(items, i));
            }
        }
        setNoDataVisibility(getCountAll() == 0);
    }


    public void addItems(ArrayList<JSONObject> items) {
        if (items != null && items.size() != 0) {
            int itemSize = items.size();
            for (int i = 0; i < itemSize; i++) {
                addItem(items.get(i));
            }
        }

        setNoDataVisibility(getCountAll() == 0);
    }


    /**
     * 다른 페이지가있을 경우 true
     *
     * @param state
     */

    public void setHasMore(boolean state) {
        if (!state) {
            setPageNo("");
        }
        mHasMore = state;
    }


    public void setPageNo(String pageNo) {
        mPageNo = pageNo;
    }


    public boolean getHasMore() {
        return mHasMore;
    }


    public int getCountAll() {
        return mAdapter.getCount();
    }


    public void removeAll() {
        mAdapter.clear();
        mTotalCount = 0;
        this.refreshDrawableState();
        mHasMore = true;
        setNoDataVisibility(true);
    }


    public ArrayList<JSONObject> getItem(int position) {

        ArrayList<JSONObject> items = null;
        if (mAdapter != null) {
            items = mAdapter.getItem(position);
        }
        return items;
    }


    public void setNoDataVisibility(boolean state) {
        if (state) {
            if (mNoData != null) {
                mNoData.setVisibility(View.VISIBLE);
                this.setVisibility(View.GONE);
            }
        } else {
            if (mNoData != null) mNoData.setVisibility(View.GONE);

            this.setVisibility(View.VISIBLE);
        }
    }


    public void refresh() {
        try {
            mAdapter.notifyDataSetChanged();
            this.refreshDrawableState();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private IMakeView mMv = null;


    public interface IMakeView {
        public View makeView(GListAdapter adapter, int position, View convertView, ViewGroup parent);
    }


    public void setViewMaker(int resourceID, IMakeView mv) {
        mResourceID = resourceID;
        mMv = mv;
    }


    public View inflateItemView(View convertView, int resourceId) {
        if (convertView == null) {
            convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resourceId, null);
        }

        return convertView;
    }


    public class GListAdapter extends ArrayAdapter<ArrayList<JSONObject>> {
        public GListAdapter(Context context, List<ArrayList<JSONObject>> objects) {
            super(context, 0, 0, objects);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return mMv.makeView(this, position, inflateItemView(convertView, mResourceID), parent);
        }

    }


    @Override
    public boolean isTop() {
        LogUtil.d("GMultiListview, isTop : " + mIsTop);
        return mIsTop;
    }


    @Override
    public boolean isBottom() {
        LogUtil.d("GMultiListview, mIsBottom : " + mIsBottom);
        return mIsBottom;
    }


    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        View firstView = view.getChildAt(0);
        if (firstView == null) {
            mIsTop = true;
            return;
        }

        mIsTop = (firstVisibleItem == 0 && firstView.getTop() == view.getPaddingTop());

        if (!mIsTop) {
            View lastView = view.getChildAt(view.getChildCount() - 1);
            if (lastView == null) {
                mIsBottom = true;
                return;
            }
            mIsBottom = (totalItemCount == firstVisibleItem + visibleItemCount && lastView.getBottom() <= (view.getHeight() - view.getPaddingBottom()));
        } else {
            mIsBottom = false;
        }
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}
