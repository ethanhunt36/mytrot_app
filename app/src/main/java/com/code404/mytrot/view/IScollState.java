package com.code404.mytrot.view;

public interface IScollState
{
	public boolean isTop();

	public boolean isBottom();
}
