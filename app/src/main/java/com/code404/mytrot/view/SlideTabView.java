package com.code404.mytrot.view;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.ImageUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.widget.ItemTabButton;

import org.json.JSONObject;

import java.util.ArrayList;


public class SlideTabView extends HorizontalScrollView {
    private LinearLayout mBase = null;
    private int[] mDeviceSize = null;
    private int TAB_MARGIN = 7;
    private int TAB_WIDTH = 0;
    private ColorStateList mTabColorList = null;

    private InterfaceSet.OnClickJsonListener mOnClickJsonListener = null;

    private int mSelcetTabIdx = 0;

    private int TAB_VIEW_COUNT = 3;

    //private int mColor_over = 0xff328dcd;
    private int mColor_over = 0xfffff176;
    private int mColor_bg = 0xff161823;

    private ArrayList<ItemTabButton> _btnList = new ArrayList<ItemTabButton>();


    public SlideTabView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }


    public SlideTabView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    public SlideTabView(Context context) {
        super(context);
        init();
    }


    public void setTabViewCount(int count) {
        TAB_VIEW_COUNT = count;

        TAB_WIDTH = mDeviceSize[0] / TAB_VIEW_COUNT;

        if (TAB_VIEW_COUNT > 7) {
            TAB_WIDTH = mDeviceSize[0] / 7;
        }

        LogUtil.d("setTabViewCount, TAB_VIEW_COUNT : " + TAB_VIEW_COUNT);
        LogUtil.d("setTabViewCount, TAB_WIDTH : " + TAB_WIDTH);
    }


    public void setColorOver(int color) {
        mColor_over = color;
    }


    private void init() {
        setHorizontalScrollBarEnabled(false);
        mDeviceSize = ImageUtil.getDisplaySize(getContext());
        TAB_MARGIN = (int) ImageUtil.dipToPixel(getContext(), 1);
        TAB_WIDTH = mDeviceSize[0] / TAB_VIEW_COUNT;

        int[][] states = new int[][]
                {new int[]
                        {android.R.attr.state_pressed}, new int[]
                        {android.R.attr.state_selected}, new int[]
                        {0}};
        int[] colors = new int[]
                {0xffffffff, 0xffffffff, 0xff6a7285};
        mTabColorList = new ColorStateList(states, colors);

        mBase = new LinearLayout(getContext());
        addView(mBase, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
    }


    public void clearTab() {
        mSelcetTabIdx = 0;
        mBase.removeAllViews();
    }


    public void addTab(String title) {
       addTab(title, 0);
    }

    public void addTab(String title, int count) {
        final int poistion = mBase.getChildCount();

        ItemTabButton tabButton = new ItemTabButton(getContext());
        tabButton.setIndex(poistion);
        tabButton.setTitle(title, count);
        tabButton.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                if (mOnClickJsonListener != null) {
                    mOnClickJsonListener.onClick(v, poistion, null);
                }
            }
        });
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(TAB_WIDTH, LayoutParams.MATCH_PARENT);
        mBase.addView(tabButton, layoutParams);


        _btnList.add(tabButton);
    }


    public void setButtonText(int index, String btnText) {
        setButtonText(index, btnText, 0);
    }

    public void setButtonText(int index, String btnText, int count) {
        if (index < _btnList.size()) {
            ItemTabButton tabButton = _btnList.get(index);

            if(tabButton != null) {
                tabButton.setTitle(btnText, count);
            }
        }
    }

    public void setButtonNewCount(int index, int count) {
        if (index < _btnList.size()) {
            ItemTabButton tabButton = _btnList.get(index);

            if(tabButton != null) {
                tabButton.setCount(count);
            }
        }
    }


    private void setTab(Button button, final int idx, final JSONObject json, String title) {
        button.setText(title);
        button.setGravity(Gravity.CENTER);


        if (TAB_VIEW_COUNT >= 7) {
            button.setPadding(40, 0, 40, 0);

            if (idx == 0) {
                button.setPadding(60, 0, 40, 0);
            }
        } else {
            button.setPadding(0, 0, 0, 0);
        }

        button.setBackgroundColor(mColor_bg);

        button.setTextColor(mTabColorList);
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mOnClickJsonListener != null) {
                    mOnClickJsonListener.onClick(v, idx, json);
                }
            }
        });
    }


    public void setTabOn(int idx) {
        setTabOn(idx, true);
    }


    public void setTabOn(int idx, boolean isAnim) {


        int childCount = TAB_VIEW_COUNT;
        if (childCount > 0 && idx < childCount) {
            mSelcetTabIdx = idx;
            for (int i = 0; i < childCount; i++) {

                ItemTabButton tabButton = _btnList.get(i);
                tabButton.setSelected(i == mSelcetTabIdx);
                //Button button = (Button) ((ViewGroup) mBase.getChildAt(i)).getChildAt(0);
//                Button button = mBase.getChildAt(i).findViewById(R.id.btnTab);
//                View baseLine = mBase.getChildAt(i).findViewById(R.id.baseLine);

//                if (i == mSelcetTabIdx) {
//                    //button.setSelected(true);
//                    // button.setBackgroundResource(R.drawable.bg_tab_green);
//                    // button.setTextColor(0xff000000);
//                    button.setTextColor(mColor_over);
//                    button.setTextSize(16);
//                    baseLine.setVisibility(View.VISIBLE);
//                } else {
//                    //button.setSelected(false);
//                    // button.setBackgroundResource(R.drawable.bg_tab_gray);
//                    // button.setBackgroundColor(0x00ffffff);
//                    // button.setTextColor(0xff000000);
//                    button.setTextSize(16);
//                    button.setTextColor(mTabColorList);
//                    baseLine.setVisibility(View.INVISIBLE);
//                }
            }

            LogUtil.d("smoothScrollTo : " + (mDeviceSize[0] * (mSelcetTabIdx) / 2));
            LogUtil.d("smoothScrollTo : " + (TAB_WIDTH * (mSelcetTabIdx) / 2));
            LogUtil.d("smoothScrollTo : isAnim : " + isAnim);
            LogUtil.d("smoothScrollTo : isAnim : " + isAnim);

            if (isAnim) {
                int width = 0;
                LinearLayout base = null;
                for (int i = 0; i < mBase.getChildCount(); i++) {
                    if (mSelcetTabIdx == i) break;

                    base = (LinearLayout) mBase.getChildAt(i);

                    LogUtil.d("base.getWidth() : " + base.getWidth());
                    width += base.getWidth();

                }

                int point = TAB_WIDTH * (mSelcetTabIdx) / 2;

                LogUtil.d("smoothScrollTo : point : " + point);
                LogUtil.d("smoothScrollTo : width : " + width);

                smoothScrollTo(width, 0);
            } else {
                scrollTo(TAB_WIDTH * (mSelcetTabIdx) / 2, 0);
            }
        }
    }


    public void setOnClickJsonListener(InterfaceSet.OnClickJsonListener listener) {
        mOnClickJsonListener = listener;
    }

}