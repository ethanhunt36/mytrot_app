package com.code404.mytrot.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class GListView extends ListView implements IScollState, OnScrollListener {
    private GListAdapter mAdapter = null;
    private int mResourceID;

    private boolean mHasMore = true;

    private View mNoData = null;

    private boolean mIsTop = true;
    private boolean mIsBottom = false;


    public GListView(Context context) {
        super(context);
        initialize();
    }


    public GListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }


    public GListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initialize();
    }


    private void initialize() {
        setCacheColorHint(0x00000000);
        mAdapter = new GListAdapter(getContext(), new ArrayList<JSONObject>());
        setAdapter(mAdapter);
        setOnScrollListener(this);
    }


    public void setNoData(View view) {
        mNoData = view;
        if (mNoData != null) mNoData.setVisibility(View.GONE);
    }


    @Override
    public void addHeaderView(View v) {
        setAdapter(null);
        super.addHeaderView(v);
        setAdapter(mAdapter);
    }


    @Override
    public void addFooterView(View v) {
        setAdapter(null);
        super.addFooterView(v);
        setAdapter(mAdapter);
    }


    public JSONObject getLastItem() {
        int count = getCountAll();
        if (count == 0) {
            return null;
        } else {
            return getAdapter().getItem(count - 1);
        }
    }


    public GListAdapter getAdapter() {
        return mAdapter;
    }


    public String getPageNo(String key) {
        if (!FormatUtil.isNullorEmpty(key)) {
            if (getCountAll() > 0) {
                return JSONUtil.getString(mAdapter.getItem(getCountAll() - 1), key, "");
            }
        }

        return "";
    }


    public void addItem(JSONObject item) {
        mAdapter.add(item);
        setNoDataVisibility(getCountAll() == 0);
    }


    public void addItems(JSONObject... items) {
        for (int i = 0; i < items.length; i++) {
            mAdapter.add(items[i]);
        }

        setNoDataVisibility(getCountAll() == 0);
    }


    public void addItems(JSONArray items) {
        if (items != null && items.length() != 0) {
            int itemSize = items.length();
            for (int i = 0; i < itemSize; i++) {
                mAdapter.add(JSONUtil.getJSONObject(items, i));
            }
        }

        setNoDataVisibility(getCountAll() == 0);
    }


    public void addItems(ArrayList<JSONObject> items) {
        if (items != null && items.size() != 0) {
            int itemSize = items.size();
            for (int i = 0; i < itemSize; i++) {
                mAdapter.add(items.get(i));
            }
        }

        setNoDataVisibility(getCountAll() == 0);
    }


    /**
     * 다른 페이지가있을 경우 true
     *
     * @param state
     */

    public void setHasMore(boolean state) {
        mHasMore = state;
    }


    public boolean getHasMore() {
        return mHasMore;
    }


    public int getCountAll() {
        return mAdapter.getCount();
    }


    public void removeAll() {
        mAdapter.clear();
        this.refreshDrawableState();
        mHasMore = true;
        setNoDataVisibility(true);
    }


    public JSONObject getItem(int position) {

        JSONObject item = null;
        if (mAdapter != null) {
            item = mAdapter.getItem(position);
        }
        return item;
    }


    public void remove(JSONObject item) {
        mAdapter.remove(item);
        setNoDataVisibility(getCountAll() == 0);
    }


    public void setNoDataVisibility(boolean state) {
        if (state) {
            if (mNoData != null) {
                mNoData.setVisibility(View.VISIBLE);
                //this.setVisibility(View.GONE);
            }
        } else {
            if (mNoData != null) mNoData.setVisibility(View.GONE);

            this.setVisibility(View.VISIBLE);
        }
    }


    public void refresh() {
        try {
            mAdapter.notifyDataSetChanged();
            this.refreshDrawableState();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private IMakeView mMv = null;


    public interface IMakeView {
        public View makeView(GListAdapter adapter, int position, View convertView, ViewGroup parent);
    }


    public void setViewMaker(int resourceID, IMakeView mv) {
        setAdapter(null);
        mAdapter = new GListAdapter(getContext(), new ArrayList<JSONObject>());
        setAdapter(mAdapter);
        mResourceID = resourceID;
        mMv = mv;
    }


    public View inflateItemView(View convertView, int resourceId) {
        if (convertView == null) {
            convertView = ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resourceId, null);
        }

        return convertView;
    }


    public class GListAdapter extends ArrayAdapter<JSONObject> {
        public GListAdapter(Context context, List<JSONObject> objects) {
            super(context, 0, 0, objects);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return mMv.makeView(this, position, inflateItemView(convertView, mResourceID), parent);
        }

    }


    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        View firstView = view.getChildAt(0);
        if (firstView == null) {
            mIsTop = true;
            return;
        }

        // LogUtil.e("firstVisibleItem : " + firstVisibleItem);
        // LogUtil.e("firstView.getTop() : " + firstView.getTop());
        // LogUtil.e("view.getPaddingTop() : " + view.getPaddingTop());

        mIsTop = (firstVisibleItem == 0 && firstView.getTop() == view.getPaddingTop());

        if (!mIsTop) {
            View lastView = view.getChildAt(view.getChildCount() - 1);
            if (lastView == null) {
                mIsBottom = true;
                return;
            }
            mIsBottom = (totalItemCount == firstVisibleItem + visibleItemCount && lastView.getBottom() <= (view.getHeight() - view.getPaddingBottom()));

            // LogUtil.i("firstVisibleItem : " + firstVisibleItem);
            // LogUtil.i("firstView.getTop() : " + firstView.getTop());
            // LogUtil.i("view.getPaddingTop() : " + view.getPaddingTop());
        } else {
            mIsBottom = false;
        }

        // LogUtil.e(mIsTop + "//" + mIsBottom);
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }


    @Override
    public boolean isTop() {
        return mIsTop;
    }


    @Override
    public boolean isBottom() {
        return mIsBottom;
    }
}
