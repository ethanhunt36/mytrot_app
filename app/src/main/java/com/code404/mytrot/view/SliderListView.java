package com.code404.mytrot.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.util.LogUtil;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


public class SliderListView extends RelativeLayout
{
    
    private float                       mStartX            = 0.0f;
    private float                       mStartY            = 0.0f;
                                                           
    private View mTopView           = null;
    private View mBaseView          = null;
    private View mBottomView        = null;
    private View mListView          = null;
                                                           
    private LayoutParams mTopViewParams     = null;
    private LayoutParams mBaseViewParams    = null;
    private LayoutParams mBottomViewParams  = null;
                                                           
    private AtomicBoolean isAnimation        = new AtomicBoolean(false);
                                                           
    final public static int             MODE_OPEN          = 1;
    final public static int             MODE_CLOSE         = 2;
                                                           
    final private static int            DIRECT_NONE        = 0;
    final private static int            DIRECT_TOP         = 3;
    final private static int            DIRECT_BOTTON      = 4;
                                                           
    final private static int            SLIDE_NONE         = 0;
    final private static int            SLIDE_HORIZONTAL   = 1;
    final private static int            SLIDE_VERTICAL     = 2;
                                                           
    private AtomicInteger mSlideMode         = new AtomicInteger(SLIDE_NONE);
                                                           
    private boolean                     isSlide            = false;
    private AtomicInteger mDirect            = new AtomicInteger(DIRECT_TOP);
                                                           
    private int                         mLimited           = 50;
    private int                         mLimitedPull       = 0;
                                                           
    private int                         mLimitedPullBottom = 0;
                                                           
    private boolean                     mIsOver            = false;
                                                           
    private TextView mTxtMessageTop     = null;
    private TextView mTxtMessageBottom  = null;
                                                           
    private Context _context           = null;
                                                           
                                                           
    public SliderListView(Context context)
    {
        super(context);
        _context = context;
        init();
    }
    
    
    public SliderListView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        _context = context;
        init();
    }
    
    
    public SliderListView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        _context = context;
        init();
    }
    
    
    private void init()
    {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display display = ((WindowManager) getContext().getSystemService(Activity.WINDOW_SERVICE)).getDefaultDisplay();
        display.getMetrics(displayMetrics);
        
        mLimitedPull = (int) ((float) displayMetrics.heightPixels * 0.1);
        mLimitedPullBottom = (int) ((float) displayMetrics.heightPixels * 0.02);
        LogUtil.e("mLimitedPull=" + mLimitedPull);
        LogUtil.e("mLimitedPull=" + mLimitedPull);
        LogUtil.e("mLimitedPull=" + mLimitedPull);
        LogUtil.e("mLimitedPullBottom=" + mLimitedPullBottom);
        LogUtil.e("mLimitedPullBottom=" + mLimitedPullBottom);
        LogUtil.e("mLimitedPullBottom=" + mLimitedPullBottom);
    }
    
    
    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        
        mBaseView = getChildAt(0);
        mListView = ((ViewGroup) mBaseView).findViewById(R.id.list);
        mTopView = getChildAt(1);
        mBottomView = getChildAt(2);
        
        mTxtMessageTop = (TextView) ((ViewGroup) mTopView).getChildAt(0);
        mTxtMessageBottom = (TextView) ((ViewGroup) mBottomView).getChildAt(0);
        
        mTopViewParams = (LayoutParams) mTopView.getLayoutParams();
        mTopViewParams.height = mLimited;
        mTopViewParams.topMargin = -mLimited;
        mBaseViewParams = (LayoutParams) mBaseView.getLayoutParams();
        mBottomViewParams = (LayoutParams) mBottomView.getLayoutParams();
        mBottomViewParams.height = mLimited;
        mBottomViewParams.bottomMargin = -mLimited;
    }
    
    
    public void setListView(View view)
    {
        mListView = view;
    }
    
    
    @Override
    public boolean dispatchTouchEvent(MotionEvent event)
    {
        
        if (isAnimation.get()) return true;
        
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
            mStartX = event.getX();
            mStartY = event.getY();
            mSlideMode.set(SLIDE_NONE);
            // System.out.println("다운");
            
        }
        else if (event.getAction() == MotionEvent.ACTION_MOVE)
        {
            float moveX = event.getX() - mStartX;
            float moveY = event.getY() - mStartY;
            mIsOver = false;
            
            if (Math.abs(moveX) > 10)
            {
                mSlideMode.compareAndSet(SLIDE_NONE, SLIDE_HORIZONTAL);
            }
            
//            if (((IScollState) mListView).isTop())
//            {
//                if (moveY < 0) moveY = 0;
//                
//                int max = getHeight() - mLimited;
//                
//                if (moveY > max) moveY = max;
//                
//                if (moveY > 10)
//                {
//                    mSlideMode.compareAndSet(SLIDE_NONE, SLIDE_VERTICAL);
//                }
//                
//                if (moveY > 30)
//                {
//                    mDirect.set(DIRECT_TOP);
//                    isSlide = true;
//                }
//                else if (moveX == 0)
//                {
//                    mDirect.set(DIRECT_NONE);
//                }
//                else
//                {
//                    mDirect.set(DIRECT_TOP);
//                }
//                
//                if (moveY > mLimitedPull)
//                {
//                    mIsOver = true;
//                }
//                
//                if (mSlideMode.get() == SLIDE_VERTICAL)
//                {
//                    moveY = moveY / 2;
//                    int topViewTopMargin = (int) (moveY - mLimited);
//                    
//                    mTopViewParams.height = ((int) moveY < mLimited) ? mLimited : (int) moveY;
//                    mTopViewParams.topMargin = topViewTopMargin > 0 ? 0 : topViewTopMargin;
//                    mTopView.setLayoutParams(mTopViewParams);
//                    
//                    mBaseViewParams.topMargin = (int) moveY;
//                    mBaseViewParams.bottomMargin = (int) -moveY;
//                    mBaseViewParams.height = getHeight();
//                    mBaseView.setLayoutParams(mBaseViewParams);
//                    
//                    if (mIsOver) onRefresh();
//                    else onRefreshWait();
//                }
//                
//                if (isSlide && mSlideMode.get() == SLIDE_VERTICAL)
//                {
//                    event.setAction(MotionEvent.ACTION_CANCEL);
//                    super.dispatchTouchEvent(event);
//                }
//            }
//            else if (((IScollState) mListView).isBottom())
//            {
//                
//                if (moveY > 0) moveY = 0;
//                
//                int min = (getHeight() - mLimited) * -1;
//                
//                if (moveY < min) moveY = min;
//                
//                if (moveY < -10)
//                {
//                    mSlideMode.compareAndSet(SLIDE_NONE, SLIDE_VERTICAL);
//                }
//                
//                if (moveY < -30)
//                {
//                    mDirect.set(DIRECT_BOTTON);
//                    isSlide = true;
//                }
//                else if (moveX == 0)
//                {
//                    mDirect.set(DIRECT_NONE);
//                }
//                else
//                {
//                    mDirect.set(DIRECT_BOTTON);
//                }
//                
//                if (moveY < -mLimitedPullBottom)
//                {
//                    mIsOver = true;
//                }
//                
//                if (mSlideMode.get() == SLIDE_VERTICAL)
//                {
//                    moveY = moveY / 2;
//                    int bottomViewTopMargin = (int) (mLimited + moveY);
//                    
//                    mBottomViewParams.height = bottomViewTopMargin > 0 ? mLimited : (int) -moveY;
//                    mBottomViewParams.bottomMargin = bottomViewTopMargin > 0 ? -bottomViewTopMargin : 0;
//                    mBottomView.setLayoutParams(mBottomViewParams);
//                    
//                    mBaseViewParams.bottomMargin = (int) -moveY;
//                    mBaseViewParams.topMargin = (int) moveY;
//                    mBaseViewParams.height = getHeight();
//                    mBaseView.setLayoutParams(mBaseViewParams);
//                    
//                    if (mIsOver) onMore();
//                    else onMoreWait();
//                    
//                }
//                
//                if (isSlide && mSlideMode.get() == SLIDE_VERTICAL)
//                {
//                    event.setAction(MotionEvent.ACTION_CANCEL);
//                    super.dispatchTouchEvent(event);
//                }
//            }

            try {
                if (mSlideMode.get() == SLIDE_VERTICAL) return false;
                else return super.dispatchTouchEvent(event);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
            // return false;
            
        }
        else if (event.getAction() == MotionEvent.ACTION_UP)
        {
            
            if (mSlideMode.get() == SLIDE_VERTICAL)
            {
                
                switch (mDirect.get())
                {
                    case DIRECT_TOP:
                        mTopViewParams.height = mLimited;
                        mTopViewParams.topMargin = -mLimited;
                        mTopView.setLayoutParams(mTopViewParams);
                        
                        mBaseViewParams.topMargin = 0;
                        mBaseViewParams.height = getHeight();
                        mBaseView.setLayoutParams(mBaseViewParams);
                        if (mIsOver) if (mListener != null) mListener.onPullDown(getContext());
                        break;
                    case DIRECT_BOTTON:
                        //
                        mBottomViewParams.height = mLimited;
                        mBottomViewParams.bottomMargin = -mLimited;
                        mBottomView.setLayoutParams(mBottomViewParams);
                        
                        mBaseViewParams.bottomMargin = 0;
                        mBaseViewParams.topMargin = 0;
                        mBaseView.setLayoutParams(mBaseViewParams);
                        if (mIsOver) if (mListener != null) mListener.onPullUp(getContext());
                        break;
                    default:
                        break;
                }
            }
        }
        
        isSlide = false;
        mIsOver = false;
        return super.dispatchTouchEvent(event);
    }
    
    
    public void setLimit(int limit)
    {
        mLimited = limit;
    }
    
    
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
        {
            return true;
        }
        
        return false;
    }
    
    
    public void onRefreshWait()
    {
        mTxtMessageTop.setText(_context.getString(R.string.txt_195));
    }
    
    
    public void onRefresh()
    {
        mTxtMessageTop.setText(_context.getString(R.string.txt_195));
    }
    
    
    public void onMoreWait()
    {
        mTxtMessageBottom.setText(_context.getString(R.string.txt_196));
    }
    
    
    public void onMore()
    {
        mTxtMessageBottom.setText(_context.getString(R.string.txt_196));
    }
    
    
    public void toggle()
    {
        // post(getRunnable((mMode.get() == MODE_OPEN) ? MODE_CLOSE : MODE_OPEN));
    }
    
    
    private IScrollPullListener mListener = null;
    
    
    public void setOnPullListener(IScrollPullListener l)
    {
        mListener = l;
    }
}
