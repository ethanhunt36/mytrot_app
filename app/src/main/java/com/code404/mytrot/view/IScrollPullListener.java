package com.code404.mytrot.view;

import android.content.Context;

public interface IScrollPullListener
{
	/**
	 * 새로고침
	 * 
	 * @param
	 */
	public void onPullDown(Context context);

	/**
	 * 더보기
	 * 
	 * @param
	 */
	public void onPullUp(Context context);
}
