package com.code404.mytrot.common;


import android.os.Build;

import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;

import java.util.Random;

public class Constants {

    // dev
//    public static final boolean DEBUG = true;                       // true
//    public static final boolean IS_REAL_SEVER = false;              // false
//    public static final boolean IS_VIEW_AD_GOOGLE = true;           // true
//    public static final boolean IS_AUTO_VIEW_NEWSPIC = true;        // true


    // live
    public static final boolean DEBUG = false;                  // false
    public static final boolean IS_REAL_SEVER = true;           // true
    public static final boolean IS_VIEW_AD_GOOGLE = true;       // true
    public static final boolean IS_AUTO_VIEW_NEWSPIC = false;   // false

    public static final boolean DISPLAY_AD = true;              // true
    public static final boolean DISPLAY_AD_HOUSE = true;        // true
    public static final boolean DISPLAY_AD_ROW = false;         // false
    public static final boolean DISPLAY_AD_TYPE_TNK = true;     // true

    public static final boolean DISPLAY_AD_TYPE_MAX = true;     // true
    public static final boolean DISPLAY_AD_TYPE_UNITY = true;   // true
    public static boolean IS_VIEW_SEQ_AD = false;               // false
    public static final boolean IS_ONE_STORE = false;           // false 원스토어 앱인지 여부

    public static final int TRY_MAX_COUNT_ADMANAGER = 1;
    public static final int SUCCESS_COUNT_ADMANAGER = 1;

    public enum enum_ad_type {none, reward, full}

    public static enum enum_ad {
        none
        , g_full_screen, g_reward, g_reward_full
        //, unityAds_full
//        , cauly_full
        , wad_full
        , max_reward, max_full, tnk_full, tnk_reward
        , adpc_full, adpc_reward_full, adpc_reward
        , facebook_full
        //, vungle_reward, vungle_full
        , newspic_a, newspic_b, newspic_c, newspic_d, newspic_x
        //, unityAds_reward
        //, adcolony_full
    }

    public static String IsAdFullScreen = "N";


    public static final int NSTATION_M_KEY = 1148;
    public static final int NSTATION_MC_KEY = 10470;

    public static final int AD_UNIT = 24;
    //    public static final int AD_UNIT = 12;
    public static final int AD_UNIT_MOD = 1;
    public static final int AD_UNIT_MOD_02 = 14;

    //public static final int AD_BOARD_WRITE_UNIT

    public static final String KEY_CAULY_AD_ID = "eZ4ZMC6A";           // app:appcode 같이 확인 필요
    public static final String KEY_UNITY_GAME_ID = "3821705";

    public static final String KEY_AD_APP_ID = "ca-app-pub-9706798414570356~2964907948";
    public static final String KEY_AD_NATIVE = "ca-app-pub-9706798414570356/8958351095";


    // 최적화(전체) - 포인트를 지급하지 않는 경우에만 노출
    public static String KEY_AD_FULL_SCREEN_01 = "ca-app-pub-9706798414570356/9722386393";
    // 포인트를 지급하는 경우에만 노출
    public static String KEY_AD_FULL_SCREEN_02 = "ca-app-pub-9706798414570356/4260239326";
    // 포인트를 지급하는 경우에만 노출
    public static String KEY_AD_FULL_SCREEN_03 = "ca-app-pub-9706798414570356/1822132378";


    public static String getKeyAdGoogleFullScreenID(boolean isOnlyFull, String chargeMode) {

        LogUtil.d("Constants.getKeyAdGoogleFullScreenID, isOnlyFull : " + isOnlyFull + ", chargeMode : " + chargeMode);

        if (isOnlyFull || chargeMode.contains("vote") || chargeMode.contains("heart")  || "N".equals(IsAdFullScreen))
            return KEY_AD_FULL_SCREEN_01;

        String[] keyArr = new String[]{KEY_AD_FULL_SCREEN_02, KEY_AD_FULL_SCREEN_03};

        int index = new Random().nextInt(keyArr.length);
        LogUtil.d("Constants.getKeyAdGoogleFullScreenID, index : " + index);
        return keyArr[index];
    }


    public static String KEY_AD_REWARD_01 = "ca-app-pub-9706798414570356/6530243669";
    public static String KEY_AD_REWARD_02 = "ca-app-pub-9706798414570356/8241626873";
    public static String KEY_AD_REWARD_03 = "ca-app-pub-9706798414570356/5105366296";


    public static String getKeyAdGoogleRewardID(boolean isOnlyFull, String chargeMode) {

        LogUtil.d("Constants.getKeyAdGoogleRewardID, isOnlyFull : " + isOnlyFull + ", chargeMode : " + chargeMode);

        if (isOnlyFull || chargeMode.contains("vote") || chargeMode.contains("heart") || "N".equals(IsAdFullScreen))
            return KEY_AD_REWARD_01;

        String[] keyArr = new String[]{KEY_AD_REWARD_02, KEY_AD_REWARD_03};

        int index = new Random().nextInt(keyArr.length);
        LogUtil.d("Constants.getKeyAdGoogleRewardID, index : " + index);
        return keyArr[index];
    }

    public static String KEY_AD_REWARD_FULL_01 = "ca-app-pub-9706798414570356/8750123334";
    public static String KEY_AD_REWARD_FULL_02 = "ca-app-pub-9706798414570356/8571695718";
    public static String KEY_AD_REWARD_FULL_03 = "ca-app-pub-9706798414570356/9258549515";

    public static String getKeyAdGoogleRewardFullID(boolean isOnlyFull, String chargeMode) {

        LogUtil.d("Constants.getKeyAdGoogleRewardFullID, isOnlyFull : " + isOnlyFull + ", chargeMode : " + chargeMode);

        if (isOnlyFull || chargeMode.contains("vote") || chargeMode.contains("heart") || "N".equals(IsAdFullScreen))
            return KEY_AD_REWARD_FULL_01;

        String[] keyArr = new String[]{KEY_AD_REWARD_FULL_02, KEY_AD_REWARD_FULL_03};

        int index = new Random().nextInt(keyArr.length);
        LogUtil.d("Constants.getKeyAdGoogleRewardFullID, index : " + index);
        return keyArr[index];
    }

    public static final String KEY_AD_BANNER = "ca-app-pub-9706798414570356/5016356215";
    public static final String KEY_AD_INTRO = "ca-app-pub-9706798414570356/4270207871";
//    public static final String KEY_AD_INTRO = "ca-app-pub-3940256099942544/9257395921";         // TEST

    public static final String KEY_TNK_FULL = "ad_full";
    public static final String KEY_TNK_REWARD = "ad_full_reward";

    private static int _index_g_full = 0;
    private static int _index_g_reward = 0;
    private static int _index_g_reward_full = 0;


    public static final String KEY_WAD_ID = "60293ad5dea4226412b789bd";
    public static final String KEY_WAD_APP_ID = "60293b76dea4226412b789be";
    public static final String KEY_WAD_APP_UNIT_ID = "602a898ddea4226412b794a5";

    public static final String KEY_VUNGLE_ID = "5fd8094463eab5b0fa45d2f4";
    public static final String KEY_VUNGLE_FULL_PLACE_ID = "VUNGLE_FULL-9921958";
    public static final String KEY_VUNGLE_REWARD_PLACE_ID = "VUNGLE_REWARD-8029088";

    public static final String KEY_MAX_FULL_ID = "ee2f91eaafc6928d";
    public static final String KEY_MAX_REWARD_ID = "245a484e449b6d4a";
    public static final String KEY_MAX_NATIVE_ID = "16bedf8f61c79158";
    public static final String KEY_MAX_NATIVE_SMALL_ID = "dd9a85750ecbba45";

    public static final String KEY_ADPC_FULL_ID = "LBNScSwyh8Pjf34";
    public static final String KEY_ADPC_REWARD_ID = "VOjReoDdhIQNhYU";
    public static final String KEY_ADPC_REWARD_FULL_ID = "BsP3eju4cBfBRjk";


//    public static final String KEY_AD_MOPUB_FULL_ID = "0d7f85bae6a24d888d103124460dc4ca";
//    public static final String KEY_AD_MOPUB_REWARD_ID       = "8342f0abd2cf44479efbae1f293846cf";


    //TEST
//    public static final String KEY_AD_FULL_SCREEN           = "ca-app-pub-3940256099942544/1033173712";
//    public static final String KEY_AD_REWARD                = "ca-app-pub-3940256099942544/5224354917";
//    public static final String KEY_AD_REWARD_FULL           = "ca-app-pub-3940256099942544/5354046379";
//    public static final String KEY_AD_BANNER                = "ca-app-pub-3940256099942544/6300978111";
//    public static final String KEY_AD_INTRO                 = "ca-app-pub-3940256099942544/3419835294";
//    public static final String KEY_AD_INTRO                 = "/6499/example/app-open";
//    public static final String KEY_AD_NATIVE                = "ca-app-pub-3940256099942544/2247696110";

    public static final String LOG_CLICK_REVIEW = "REVIEW";
    public static final String LOG_CLICK_REVIEW_01 = "REVIEW_01";
    public static final String LOG_CLICK_REVIEW_02 = "REVIEW_02";
    public static final String LOG_CLICK_REVIEW_03 = "REVIEW_03";
    public static final String LOG_CLICK_SHARE = "SHARE_000";

    public static final String LOG_CLICK_MORE_APP = "MORE_APP";
    public static final String LOG_CLICK_MORE_AD_ROW = "AH_ROW_";
    public static final String LOG_CLICK_MORE_AD_FULL = "AH_FUL_";

    // 하루에 투표권 지급 가능한 횟수
    public static final int ADD_TICKET_REVIEW = 1;
    public static final int ADD_TICKET_SHARE = 3;

    public static final int TICKET_REVIEW_COUNT = 3;
    public static final int TICKET_SHARE_COUNT = 2;

    public static final String APP_TYPE_ID = "1";
    public static final String APP_DEVICE_ID = !IS_ONE_STORE ? "MYTROT" : "MYTROT_ONE";

    //    public static final String AD_ONLBAM_URL = "http://www.onlbam.com/ad.php?from=TROT";
    public static final String AD_CHIMEA_GAME_URL = "https://www.mytrot.co.kr/ad.php?from=chimaegame";
    public static final String MARKET_URL = !IS_ONE_STORE ? "market://details?id=com.code404.mytrot" : "onestore://common/product/0000756272";
    public static final String MARKET_WEB_URL = "http://www.mytrot.co.kr/ad.php?from=mytrot";


    public static final int RECORD_SIZE = 30;
    public static final int RECORD_SIZE_MAX = 99999;
    public static final int RECORD_SIZE_TOP100 = 100;


    public static final String GooglePlayLicenseKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjEARYaaSXgo2LWE/bldECUmZexnHM2jxzV8RfWdq09lp+4MTZX9r1K0qgPQuqsh9DJ6ppGcMpFEZBzfkjAWenPP+eTXGsXkv3B7UnXIP9W+uJ7LzLCJXIMCEwhRtmiFdvzSwmX/JhhtWhzYuZ5xVIVEDmOSRjWEJsCI2YN1aCWZJlU/m94Jf/Pv0u0jhSwL0w5+2ZyUj6ZMDyognzkpnGVLEEhw2T6CQFxDCJcOWBbCM9dhIHQMGJgvAKC320x+z5ZpZjgYCfRCJYvgFRWcQnSx4sUf5fOXvTrBW0QDsQImhpwNvu8MYQa9uj2YZFlWTRcigBOPulKacoAVKvMfGCQIDAQAB";

    // REAL SERVER
    public static String SERVER_URL = "https://www.mytrot.co.kr";
    //public static String SERVER_URL_DEBUG = "http://210.221.8.7:8988";
    public static String SERVER_URL_DEBUG = "http://192.168.0.8:8988";
//    public static String SERVER_URL_DEBUG = "http://192.168.0.48:8988";


    public static String getServerUrl() {
        return IS_REAL_SEVER ? SERVER_URL : SERVER_URL_DEBUG;
    }

    public static String IMAGE_URL = "http://mytrot.s3.ap-northeast-2.amazonaws.com";
    /*************************** API URL ***************************/


    /***************************
     * INTENT BUNDLE KEY
     ***************************/
    public static final String EXTRAS_MEMBER_NO = "EXTRAS_MEMBER_NO";
    public static final String EXTRAS_ACCUSE_TYPE = "EXTRAS_ACCUSE_TYPE";
    public static final String EXTRAS_TYPE = "EXTRAS_TYPE";
    public static final String EXTRAS_FRG_INDEX = "EXTRAS_FRG_INDEX";
    public static final String EXTRAS_JSON_STRING = "EXTRAS_JSON_STRING";

    public static final String EXTRAS_NO = "EXTRAS_NO";
    public static final String EXTRAS_VOD_NO = "EXTRAS_VOD_NO";
    public static final String EXTRAS_COMMENT_NO = "EXTRAS_COMMENT_NO";
    //    public static final String EXTRAS_VOD_LIST_JSON_STRING = "EXTRAS_VOD_LIST_JSON_STRING";
    public static final String EXTRAS_TITLE = "EXTRAS_TITLE";
    public static final String EXTRAS_ARTIST_NO = "EXTRAS_ARTIST_NO";
    public static final String EXTRAS_ARTIST_NAME = "EXTRAS_ARTIST_NAME";
    public static final String EXTRAS_ARTIST_BOARD_ID = "EXTRAS_ARTIST_BOARD_ID";
    public static final String EXTRAS_DONATE_NTH = "EXTRAS_DONATE_NTH";
    public static final String EXTRAS_DONATE_GOAL_NO = "EXTRAS_DONATE_GOAL_NO";
    public static final String EXTRAS_KEYWORD = "EXTRAS_KEYWORD";
    public static final String EXTRAS_MSG = "EXTRAS_MSG";
    public static final String EXTRAS_BOARD_NO = "EXTRAS_BOARD_NO";
    public static final String EXTRAS_URL = "EXTRAS_URL";

    public static final String EXTRAS_AD_TICKET = "EXTRAS_AD_TICKET";
    public static final String EXTRAS_AD_POINT = "EXTRAS_AD_POINT";


    public static Constants.enum_ad getEnumAd(String ad_name) {

        if (ad_name.equals("g_full_screen")) return Constants.enum_ad.g_full_screen;
        if (ad_name.equals("g_reward")) return Constants.enum_ad.g_reward;
        if (ad_name.equals("g_reward_full")) return Constants.enum_ad.g_reward_full;
//        if (ad_name.equals("unityAds_full")) return Constants.enum_ad.unityAds_full;
//        if (ad_name.equals("cauly_full")) return Constants.enum_ad.cauly_full;

//        if (ad_name.equals("vungle_reward")) return Constants.enum_ad.vungle_reward;
//        if (ad_name.equals("vungle_full")) return Constants.enum_ad.vungle_full;
//        if (ad_name.equals("wad_full")) return Constants.enum_ad.wad_full;
        if (ad_name.equals("max_reward")) return Constants.enum_ad.max_reward;
        if (ad_name.equals("max_full")) return Constants.enum_ad.max_full;

        if (ad_name.equals("tnk_full")) return Constants.enum_ad.tnk_full;
        if (ad_name.equals("tnk_reward")) return Constants.enum_ad.tnk_reward;
//        if (ad_name.equals("adpc_full")) return enum_ad.adpc_full;
//        if (ad_name.equals("adpc_reward")) return enum_ad.adpc_reward;
//        if (ad_name.equals("adpc_reward_full")) return enum_ad.adpc_reward_full;

//        if (Build.VERSION.SDK_INT != 26) {
//            if (ad_name.equals("facebook_full")) return Constants.enum_ad.facebook_full;
//        }

        return Constants.enum_ad.none;
    }
}
