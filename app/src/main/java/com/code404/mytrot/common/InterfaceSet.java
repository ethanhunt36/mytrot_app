package com.code404.mytrot.common;

import android.content.DialogInterface;
import android.view.View;


import org.json.JSONArray;
import org.json.JSONObject;

public class InterfaceSet {
    public static final int Index0 = 0;
    public static final int Index1 = 1;
    public static final int Index2 = 2;
    public static final int Index3 = 3;


    public interface OnInAppPurchaseListener {
        public void onClick(String sku, JSONObject json, int order_no);
    }

    public interface OnZzimListener {
        public void onClick(int no, String target_member_no);
    }

    public interface OnClickViewListener {
        public void onClick(View v, int pos);
    }

    public interface OnCloseListener {
        public void onClose(DialogInterface dialog, int which) ;
    }

    public interface OnClickJsonListener {
        public void onClick(View v, int pos, JSONObject json);
    }

    public interface OnStringListener {
        public void onClose(DialogInterface dialog, int which, String str);
    }

    public interface OnCompleteListener {
        public void onComplete();
    }

    public interface OnAdFullManagerListener {
        public void setActiveAdText(Constants.enum_ad enumAd, boolean isActive);
        public void setVotePointSync(JSONObject userJson, int amount_ticket_ad, double point_amount);
    }



    public interface OnAdCompleteListener {
        public void onAdLoadSuccess();
        public void onAdLoadFail();
        public void onAdClose();
    }



    public interface OnAd2CompleteListener {
        public void onAdLoadSuccess();
        public void onAdLoadFail();
        public void onAdPlayComplete(Constants.enum_ad enumAd);
    }

    public interface OnObjectListener {
        public void onClose(DialogInterface dialog, int which, Object obj);
    }


    public interface OnCloseFavoriteListener {
        public void onClose(DialogInterface dialog, int which, JSONArray jsonArray);
    }

    public interface IScollState {
        public boolean isTop();

        public boolean isBottom();
    }

    public interface IBooleanListener {
        public void result(boolean value);
    }


    public interface IRefresh {
        public void refresh();
    }


    public interface OnChangeUserPushInfoListener {
        public void onChange(String info);
    }

    public interface AdFullViewCompleteListener {
        public void onAfterAction();
    }


}