package com.code404.mytrot;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.code404.mytrot.common.Constants;
import com.code404.mytrot.util.AppOpenAdManager;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.util.Map;


public class MyTrotApp extends Application implements Application.ActivityLifecycleCallbacks, LifecycleObserver {

    // SNS Login
    private static volatile MyTrotApp instance = null;
    private static volatile Activity currentActivity = null;

    public static boolean isInit = false;

    private AppOpenAdManager appOpenAdManager;

//    private Thread.UncaughtExceptionHandler androidDefaultUEH;
//    private UncaughtExceptionHandler uncaughtExceptionHandler;
//
//    public UncaughtExceptionHandler getUncaughtExceptionHandler() {
//        return uncaughtExceptionHandler;
//    }

    public static MyTrotApp getGlobalApplicationContext() {
        if (instance == null)
            throw new IllegalStateException("this application does not inherit com.kakao.GlobalApplication");
        return instance;
    }

    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        MyTrotApp.currentActivity = currentActivity;
    }

    public AppOpenAdManager getAppOpenAdManager() {
        return this.appOpenAdManager;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        int member_no = SPUtil.getInstance().getUserNo(this);

        this.registerActivityLifecycleCallbacks(this);

        instance = this;


        Constants.IsAdFullScreen = SPUtil.getInstance().getAdFullScreen(this);


//        try {
//            androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler();
//            uncaughtExceptionHandler = new UncaughtExceptionHandler();
//
//            Thread.setDefaultUncaughtExceptionHandler(uncaughtExceptionHandler);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        if (member_no < 1) {
            return;
        }


        initAdLoad();

    }


    public void initAdLoad() {

        new Thread(
                () -> {
                    MobileAds.initialize(this, new OnInitializationCompleteListener() {
                        @Override
                        public void onInitializationComplete(InitializationStatus initializationStatus) {
                            if (initializationStatus != null) {
                                Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
                                for (String adapter : statusMap.keySet()) {
                                    AdapterStatus status = statusMap.get(adapter);

                                    LogUtil.d(String.format("adapter name : %s, desc : %s, latancy : %d", adapter, status.getDescription(), status.getLatency()));
                                }
                            }
                        }
                    });
                })
                .start();
        try {
            appOpenAdManager = null;

            if (SPUtil.getInstance().getUseIntroYn(this).equals("Y")) {
                appOpenAdManager = new AppOpenAdManager();

                if (appOpenAdManager != null && appOpenAdManager.isShowingAd == true) {
                    appOpenAdManager.isShowingAd = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


//        try {
////            MobileAds.initialize(this, new OnInitializationCompleteListener() {
////                @Override
////                public void onInitializationComplete(InitializationStatus initializationStatus) {
////                    if (initializationStatus != null) {
////                        Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
////                        for (String adapter : statusMap.keySet()) {
////                            AdapterStatus status = statusMap.get(adapter);
////
////                            LogUtil.d(String.format("adapter name : %s, desc : %s, latancy : %d", adapter, status.getDescription(), status.getLatency()));
////                        }
////                    }
////                }
////            });
////
////            try {
////                appOpenAdManager = new AppOpenAdManager
////                        .Builder(this, Constants.KEY_AD_INTRO)
////                        .build();
////
////            } catch (Exception e) {
////                e.printStackTrace();
////            }
//
//
//
//
//
//
//            MobileAds.initialize(this, new OnInitializationCompleteListener() {
//                @Override
//                public void onInitializationComplete(InitializationStatus initializationStatus) {
//                    Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
//                    for (String adapterClass : statusMap.keySet()) {
//                        AdapterStatus status = statusMap.get(adapterClass);
//                        LogUtil.d(String.format(
//                                "Adapter name: %s, Description: %s, Latency: %d",
//                                adapterClass, status.getDescription(), status.getLatency()));
//                    }
//
//                    // Start loading ads here...
//                }
//            });
//
//
//            isInit = true;
//
////            MobileAds.initialize(
////                    this,
////                    new OnInitializationCompleteListener() {
////                        @Override
////                        public void onInitializationComplete(
////                                @NonNull InitializationStatus initializationStatus) {
////                        }
////                    });
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//        try {
//            appOpenAdManager = null;
//
//            if (SPUtil.getInstance().getUseIntroYn(this).equals("Y")) {
//                appOpenAdManager = new AppOpenAdManager();
//
//                if (appOpenAdManager != null && appOpenAdManager.isShowingAd == true) {
//                    appOpenAdManager.isShowingAd = false;
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
////        try {
////            WSdk.init(this, Constants.KEY_WAD_ID);
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
//
////        try {
////            if (SPUtil.getInstance().getUseVungleYn(this).equals("Y")) {
////                VungleAds.init(getApplicationContext(), Constants.KEY_VUNGLE_ID, new InitializationListener() {
////                    @Override
////                    public void onSuccess() {
////                        LogUtil.d("_txtAd, Vungle, onSuccess");
////                    }
////
////                    @Override
////                    public void onError(@NonNull VungleError vungleError) {
////                        LogUtil.d("_txtAd, Vungle, onError : " + vungleError.getErrorMessage());
////                    }
////                });
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
//
//        try {
//            if (Build.VERSION.SDK_INT != 26 && !AudienceNetworkAds.isInitialized(this)) {
//                AudienceNetworkAds
//                        .buildInitSettings(this)
//                        .withInitListener(new AudienceNetworkAds.InitListener() {
//                            @Override
//                            public void onInitialized(AudienceNetworkAds.InitResult initResult) {
//                                LogUtil.d("_txtAd", "AudienceNetworkAds initResult : " + (initResult == null ? "null" : initResult.toString()));
//                            }
//                        })
//                        .initialize();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
////        try {
////            if (SPUtil.getInstance().getUseUnityAdsYn(this).equals("Y") && Constants.DISPLAY_AD_TYPE_UNITY) {
////                // Initialize the SDK:
////                UnityAds.initialize(this, Constants.KEY_UNITY_GAME_ID, false);
////                LogUtil.d("_txtAd", "UnityAds.initialize E");
////            }
////        } catch (Exception e) {
////            e.printStackTrace();
////        }
//
//
//        try {
//            if (Constants.DISPLAY_AD_TYPE_MAX) {
//                AppLovinSdk.getInstance(MyTrotApp.this).setMediationProvider("max");
//                AppLovinSdk.initializeSdk(MyTrotApp.this, new AppLovinSdk.SdkInitializationListener() {
//                    @Override
//                    public void onSdkInitialized(AppLovinSdkConfiguration config) {
//                        LogUtil.d("AppLovinSdk, onSdkInitialized, config : " + (config == null ? "null" : config.toString()));
//                    }
//                });
//
//                AppLovinPrivacySettings.setHasUserConsent(true, MyTrotApp.this);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        try {
//            // v3.0.0 이상 버전
//            if (AdPopcornSSP.isInitialized(MyTrotApp.this)) {
//                // Already SSP SDK initialized
//                LogUtil.d("AdPopcornSSP, isInitialized");
//            } else {
//                AdPopcornSSP.init(MyTrotApp.this);
//                LogUtil.d("AdPopcornSSP, init");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        instance = null;
    }

    /**
     * LifecycleObserver method that shows the app open ad when the app moves to foreground.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    protected void onMoveToForeground() {
        // Show the ad (if available) when the app moves to foreground.
                        LogUtil.d("onMoveToForeground, appOpenAdManager : " + (appOpenAdManager == null ? "null" : "ok"));

        if (appOpenAdManager != null && SPUtil.getInstance().getUseIntroYn(this).equals("Y")) {
            appOpenAdManager.showAdIfAvailable(currentActivity);
        }
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        if (appOpenAdManager != null && !appOpenAdManager.isShowingAd) {
            currentActivity = activity;
        }
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }

//    public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
//        @Override
//        public void uncaughtException(Thread thread, Throwable ex) {
//            // 이곳에서 로그를 남긴다
//            LogUtil.e("UncaughtExceptionHandler : " + ex.toString());
//            android.os.Process.killProcess(android.os.Process.myPid());
//            System.exit(10);
//        }
//    }


}