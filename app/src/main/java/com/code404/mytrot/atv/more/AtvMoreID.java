package com.code404.mytrot.atv.more;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;


public class AtvMoreID extends AtvBase {
    private static String TAG = "[AtvMoreID]";


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        LogUtil.d(TAG, "intent[2], intent.getAction() : " + intent.getAction());

        if (Intent.ACTION_VIEW.equals(intent.getAction())) {    // Action 확인

            Uri uri = intent.getData(); // Uri 조회

            if (uri != null) {
                String getUri = uri.toString();
                String cmd = uri.getQueryParameter("cmd");    // key1 값 조회
                Log.d(TAG, "intent[2], getUri : " + getUri);  // callee://data?key1=value1&key2=value2
                Log.d(TAG, "intent[2], cmd : " + cmd);     // value2

                if ("mytrot_id".equals(cmd)) {
                    sendMytrotID(uri);
                    return;
                }
            }
        }
    }


    private void sendMytrotID(Uri uri) {

        String from_package = uri.getQueryParameter("from_package");
        String from_scheme = uri.getQueryParameter("from_scheme");

        String user_no_enc = SPUtil.getInstance().getUserNoEnc(this);

        String schemeUrl = from_scheme + "://action?cmd=res_mytrot_id&user_no_enc=" + user_no_enc;

        LogUtil.d(TAG, "schemeUrl : " + schemeUrl);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(schemeUrl));
        startActivity(intent);

        finishAffinity();

    }

    @Override
    protected void configureListener() {

    }

    @Override
    protected void setView() {

    }

    @Override
    protected void findView() {

    }

    @Override
    protected void init() {
        LogUtil.d(TAG, "intent, getIntent().getAction() : " + getIntent().getAction());

        if (Intent.ACTION_VIEW.equals(getIntent().getAction())) {    // Action 확인

            Uri uri = getIntent().getData(); // Uri 조회

            if (uri != null) {
                String getUri = uri.toString();
                String cmd = uri.getQueryParameter("cmd");    // key1 값 조회
                Log.d(TAG, "intent, getUri : " + getUri);  // callee://data?key1=value1&key2=value2
                Log.d(TAG, "intent, cmd : " + cmd);     // value2

                if ("mytrot_id".equals(cmd)) {
                    sendMytrotID(uri);
                    return;
                }
            }
        }
    }
}
