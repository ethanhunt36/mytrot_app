package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.bean.ProductBean;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.widget.ItemPayment;
import com.code404.mytrot.widget.ItemPaymentDev;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;

public class AtvTrade extends AtvBase {

    private static String TAG = "AtvTrade";


    private SeekBar _seekBar = null;
    private TextView _txtMyPoint = null;
    private TextView _txtTrade_P = null;
    private TextView _txtTrade_V = null;
    private EditText _edtPoint = null;
    private Button _btnTrade = null;


    private TextView _txtTrade_P_Heart = null;
    private TextView _txtTrade_V_Heart = null;
    private SeekBar _seekBar_Heart = null;
    private TextView _txtMyPoint_Heart = null;
    private EditText _edtPoint_Heart = null;
    private Button _btnTrade_Heart = null;


    private TextView _txtTrade_Blue = null;
    private TextView _txtTrade_Blue_Point = null;
    private SeekBar _seekBar_Blue = null;
    private TextView _txtMyPoint_Blue = null;
    private EditText _edtPoint_Blue = null;
    private TextView _txtBlue = null;
    private Button _btnTrade_Blue = null;


    private TextView _txtTrade_HV_Heart = null;
    private TextView _txtTrade_HV_Vote = null;
    private SeekBar _seekBar_HV = null;
    private TextView _txtMyPoint_HV = null;
    private EditText _edtPoint_HV = null;
    private TextView _txtPoint = null;
    private Button _btnTrade_HV = null;


    private static List<String> _purchaseTokenList = new ArrayList<String>();


    private BillingClient mBillingClient;

    private List<SkuDetails> _skuDetailsList;

    private int _order_no = -1;
    private JSONObject _productInfo = null;
    private int _price = -1;
    private String _sku = "";
    private String _pay_type = "A";

    private List<String> _skuList = new ArrayList<>();

    @Override
    protected boolean getBundle() {
        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_trade);
    }

    @Override
    protected void findView() {
        _seekBar = findViewById(R.id.seekBar);
        _txtMyPoint = findViewById(R.id.txtMyPoint);
        _txtTrade_P = findViewById(R.id.txtTrade_P);
        _txtTrade_V = findViewById(R.id.txtTrade_V);
        _btnTrade = findViewById(R.id.btnTrade);
        _edtPoint = findViewById(R.id.edtPoint);

        _txtTrade_P_Heart = (TextView) findViewById(R.id.txtTrade_P_Heart);
        _txtTrade_V_Heart = (TextView) findViewById(R.id.txtTrade_V_Heart);
        _seekBar_Heart = (SeekBar) findViewById(R.id.seekBar_Heart);
        _txtMyPoint_Heart = (TextView) findViewById(R.id.txtMyPoint_Heart);
        _edtPoint_Heart = (EditText) findViewById(R.id.edtPoint_Heart);
        _btnTrade_Heart = (Button) findViewById(R.id.btnTrade_Heart);


        _txtTrade_Blue = (TextView) findViewById(R.id.txtTrade_Blue);
        _txtTrade_Blue_Point = (TextView) findViewById(R.id.txtTrade_Blue_Point);
        _seekBar_Blue = (SeekBar) findViewById(R.id.seekBar_Blue);
        _txtMyPoint_Blue = (TextView) findViewById(R.id.txtMyPoint_Blue);
        _edtPoint_Blue = (EditText) findViewById(R.id.edtPoint_Blue);
        _txtBlue = (TextView) findViewById(R.id.txtBlue);
        _btnTrade_Blue = (Button) findViewById(R.id.btnTrade_Blue);


        // 하트 -> 투표권
        _txtTrade_HV_Heart = (TextView) findViewById(R.id.txtTrade_HV_Heart);
        _txtTrade_HV_Vote = (TextView) findViewById(R.id.txtTrade_HV_Vote);
        _seekBar_HV = (SeekBar) findViewById(R.id.seekBar_HV);
        _txtMyPoint_HV = (TextView) findViewById(R.id.txtMyPoint_HV);
        _edtPoint_HV = (EditText) findViewById(R.id.edtPoint_HV);
        _txtPoint = (TextView) findViewById(R.id.txtPoint);
        _btnTrade_HV = (Button) findViewById(R.id.btnTrade_HV);

    }

    @Override
    protected void init() {
        _txtTopTitle.setText("포인트 → 투표권 or 하트 교환");
        _baseTopBottom.setVisibility(View.GONE);

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
        int point = JSONUtil.getInteger(json, "point");
        int point_max = point - (point % 10);

        int blue = JSONUtil.getInteger(json, "blue");
        int blue_max = blue - (blue % 10);

        int heart = JSONUtil.getInteger(json, "heart");
        int heart_max = heart - (heart % 10);

        _seekBar.setMax(point);
        _txtMyPoint.setText(FormatUtil.toPriceFormat(point) + "P");

        _seekBar_Heart.setMax(point_max);
        _txtMyPoint_Heart.setText(FormatUtil.toPriceFormat(point_max) + "P");

        _seekBar_Blue.setMax(blue_max);
        _txtMyPoint_Blue.setText(FormatUtil.toPriceFormat(blue_max) + "쩜");

        _seekBar_HV.setMax(heart_max);
        _txtMyPoint_HV.setText(FormatUtil.toPriceFormat(heart_max) + "개");


        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            findViewById(R.id.base_blue).setVisibility(View.GONE);
        }
    }


    @Override
    protected void configureListener() {

        _seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                LogUtil.d("onProgressChanged, i : " + i + ", b : " + b);

                int vote_cnt = i * 2;
                _txtTrade_P.setText(FormatUtil.toPriceFormat(i) + "P");
                _txtTrade_V.setText(FormatUtil.toPriceFormat(vote_cnt) + "장");

                _edtPoint.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }
        });

        _edtPoint.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (FormatUtil.isNullorEmpty(_edtPoint.getText().toString()) == false) {
                    final int p = Integer.parseInt(_edtPoint.getText().toString());

                    if (_seekBar.getMax() < p) {
                        int max = _seekBar.getMax();
                        Alert.toastLong(getContext(), max + "P 까지만 입력할 수 있습니다.");

                        _seekBar.setProgress(max);
                        _edtPoint.setText(String.valueOf(max));
                        return;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (_seekBar.getMax() >= p) {
                                _seekBar.setProgress(p);
                                //_edtPoint.requestFocus();
                                _edtPoint.setSelection(_edtPoint.getText().length());
                            }
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        _btnTrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final int point = _seekBar.getProgress();
                int vote_cnt = point * 2;

                if (point < 10) {
                    new Alert().showAlert(getContext(), "10P 이상 부터 교환할 수 있습니다.");
                    return;
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            callApi_buy_vote_ticket(point);
                        }
                    }
                });
                alert.showAlert(getContext(), "포인트 " + FormatUtil.toPriceFormat(point) + "P를 투표권 " + FormatUtil.toPriceFormat(vote_cnt) + "장으로 교환하시겠습니까?", false, "확인", "취소");

            }
        });


        _seekBar_Heart.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                LogUtil.d("onProgressChanged, i : " + i + ", b : " + b);

                int heart_cnt = i / 10;
                _txtTrade_P_Heart.setText(FormatUtil.toPriceFormat(i) + "P");
                _txtTrade_V_Heart.setText(FormatUtil.toPriceFormat(heart_cnt) + "개");

                _edtPoint_Heart.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }
        });

        _edtPoint_Heart.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (FormatUtil.isNullorEmpty(_edtPoint_Heart.getText().toString()) == false) {
                    final int p = Integer.parseInt(_edtPoint_Heart.getText().toString());

                    if (_seekBar_Heart.getMax() < p) {
                        int max = _seekBar_Heart.getMax();
                        Alert.toastLong(getContext(), max + "P 까지만 입력할 수 있습니다.");

                        _seekBar_Heart.setProgress(max);
                        _edtPoint_Heart.setText(String.valueOf(max));
                        return;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (_seekBar_Heart.getMax() >= p) {
                                _seekBar_Heart.setProgress(p);
                                //_edtPoint.requestFocus();
                                _edtPoint_Heart.setSelection(_edtPoint_Heart.getText().length());
                            }
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        _btnTrade_Heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int point = _seekBar_Heart.getProgress();
                int heart_cnt = point / 10;
                if (point < 10) {
                    new Alert().showAlert(getContext(), "10P 이상 부터 교환할 수 있습니다.");
                    return;
                }

                final int point_final = heart_cnt * 10;

                _seekBar_Heart.setProgress(point_final);
                _edtPoint_Heart.setText(String.valueOf(point_final));

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            callApi_point_buy_heart(point_final);
                        }
                    }
                });
                alert.showAlert(getContext(), "포인트 " + FormatUtil.toPriceFormat(point_final) + "P를 하트 " + FormatUtil.toPriceFormat(heart_cnt) + "개로 교환하시겠습니까?", false, "확인", "취소");

            }
        });


        _seekBar_Blue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                LogUtil.d("onProgressChanged, i : " + i + ", b : " + b);

                int point_cnt = i / 20;
                _txtTrade_Blue.setText(FormatUtil.toPriceFormat(i) + "쩜");
                _txtTrade_Blue_Point.setText(FormatUtil.toPriceFormat(point_cnt) + "P");

                _edtPoint_Blue.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }
        });

        _edtPoint_Blue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (FormatUtil.isNullorEmpty(_edtPoint_Blue.getText().toString()) == false) {
                    final int p = Integer.parseInt(_edtPoint_Blue.getText().toString());

                    if (_seekBar_Blue.getMax() < p) {
                        int max = _seekBar_Blue.getMax();
                        Alert.toastLong(getContext(), max + "P 까지만 입력할 수 있습니다.");

                        _seekBar_Blue.setProgress(max);
                        _edtPoint_Blue.setText(String.valueOf(max));
                        return;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (_seekBar_Blue.getMax() >= p) {
                                _seekBar_Blue.setProgress(p);
                                //_edtBlue.requestFocus();
                                _edtPoint_Blue.setSelection(_edtPoint_Blue.getText().length());
                            }
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        _btnTrade_Blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int blue = _seekBar_Blue.getProgress();
                int point_cnt = blue / 20;
                if (blue < 20) {
                    new Alert().showAlert(getContext(), "20쩜 이상 부터 교환할 수 있습니다.");
                    return;
                }

                final int blue_final = point_cnt * 20;

                _seekBar_Blue.setProgress(blue_final);
                _edtPoint_Blue.setText(String.valueOf(blue_final));

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            callApi_buy_point_by_blue(blue_final);
                        }
                    }
                });
                alert.showAlert(getContext(), FormatUtil.toPriceFormat(blue_final) + "쩜을 포인트 " + FormatUtil.toPriceFormat(point_cnt) + "P로 교환하시겠습니까?", false, "확인", "취소");

            }
        });


        // 하트 -> 투표권
        _seekBar_HV.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                LogUtil.d("onProgressChanged, i : " + i + ", b : " + b);

                int vote_cnt = i * 20;
                _txtTrade_HV_Heart.setText(FormatUtil.toPriceFormat(i) + "개");
                _txtTrade_HV_Vote.setText(FormatUtil.toPriceFormat(vote_cnt) + "장");

                _edtPoint_HV.setText(String.valueOf(i));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                LogUtil.d("onStartTrackingTouch, " + seekBar.getProgress());
            }
        });

        _edtPoint_HV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (FormatUtil.isNullorEmpty(_edtPoint_HV.getText().toString()) == false) {
                    final int p = Integer.parseInt(_edtPoint_HV.getText().toString());

                    if (_seekBar_HV.getMax() < p) {
                        int max = _seekBar_HV.getMax();
                        Alert.toastLong(getContext(), max + "개 까지만 입력할 수 있습니다.");

                        _seekBar_HV.setProgress(max);
                        _edtPoint_HV.setText(String.valueOf(max));
                        return;
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (_seekBar_HV.getMax() >= p) {
                                _seekBar_HV.setProgress(p);
                                //_edtPoint.requestFocus();
                                _edtPoint_HV.setSelection(_edtPoint_HV.getText().length());
                            }
                        }
                    });
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        _btnTrade_HV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final int heart = _seekBar_HV.getProgress();

                if (heart < 10) {
                    new Alert().showAlert(getContext(), "하트 10개 이상 부터 교환할 수 있습니다.");
                    return;
                }

                int vote_cnt = heart * 20;

                _seekBar_HV.setProgress(heart);
                _edtPoint_HV.setText(String.valueOf(heart));

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            callApi_heart_buy_vote_ticket(heart);
                        }
                    }
                });
                alert.showAlert(getContext(), "하트 " + FormatUtil.toPriceFormat(heart) + "개를 투표권 " + FormatUtil.toPriceFormat(vote_cnt) + "장으로 교환하시겠습니까?", false, "확인", "취소");
            }
        });


    }


    private void callApi_buy_vote_ticket(int point_amount) {

        LogUtil.e("==========callApi_product_get_all : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.point_buy_vote_ticket(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , point_amount
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) resultMessage = "교환이 완료 되었습니다.";

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_point_buy_heart(int point_amount) {

        LogUtil.e("==========callApi_point_buy_heart : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.point_buy_heart(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , point_amount
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) resultMessage = "교환이 완료 되었습니다.";

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_heart_buy_vote_ticket(int heart_amount) {

        LogUtil.e("==========callApi_heart_buy_vote_ticket : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.heart_buy_vote_ticket(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , heart_amount
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) resultMessage = "교환이 완료 되었습니다.";

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_buy_point_by_blue(int blue_amount) {

        LogUtil.e("==========callApi_buy_point_by_blue : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.buy_point_by_blue(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , blue_amount
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) resultMessage = "교환이 완료 되었습니다.";

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
