package com.code404.mytrot.atv.etc;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.bean.ImageBean;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CodeUtil;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.SelectPhotoManager;
import com.google.gson.JsonObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AtvReport extends AtvBase implements SelectPhotoManager.PhotoSelectCallback {

    public static final String ACCUSE_TYPE_BOARD = "B";
    public static final String ACCUSE_TYPE_BOARD_COMMENT = "C";
    public static final String ACCUSE_TYPE_MAN = "M";

    private ScrollView _scrollView = null;
    private RelativeLayout _baseReason = null;
    private TextView _txtReason = null;
    private EditText _edtContent = null;
    private RelativeLayout _basePhone = null;
    private Button _btnFile = null;
    private com.code404.mytrot.view.SquareImageView _img01 = null;
    private com.code404.mytrot.view.SquareImageView _img02 = null;
    private com.code404.mytrot.view.SquareImageView _img03 = null;
    private RelativeLayout _base_bottom = null;
    private Button _btnConfirm = null;

    private View _basePhoto01 = null;
    private View _basePhoto02 = null;

    private ImageBean[] _imgBeanArr = new ImageBean[]{new ImageBean(), new ImageBean(), new ImageBean()};
    private SelectPhotoManager m_photoManger = null;

    private int _boardNo = 0;
    private int _reasonCode = 0;
    private String _accuse_type = AtvReport.ACCUSE_TYPE_BOARD;
    private String _target_member_no = "";
    private String _artist_board_id = "";

    private int _selectedPhotoIndex = 0;

    @Override
    protected boolean getBundle() {
        _target_member_no = getIntent().getStringExtra(Constants.EXTRAS_MEMBER_NO);
        _accuse_type = getIntent().getStringExtra(Constants.EXTRAS_ACCUSE_TYPE);
        _boardNo = getIntent().getIntExtra(Constants.EXTRAS_BOARD_NO, -1);
        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);

        if (FormatUtil.isNullorEmpty(_target_member_no)) return false;
        if (FormatUtil.isNullorEmpty(_accuse_type)) return false;

        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_report);
    }

    @Override
    protected void findView() {

        _scrollView = findViewById(R.id.scrollView);
        _baseReason = (RelativeLayout) findViewById(R.id.baseReason);
        _txtReason = (TextView) findViewById(R.id.txtReason);
        _edtContent = (EditText) findViewById(R.id.edtContent);
        _basePhone = (RelativeLayout) findViewById(R.id.basePhone);
        _btnFile = (Button) findViewById(R.id.btnFile);
        _img01 = (com.code404.mytrot.view.SquareImageView) findViewById(R.id.img01);
        _img02 = (com.code404.mytrot.view.SquareImageView) findViewById(R.id.img02);
        _img03 = (com.code404.mytrot.view.SquareImageView) findViewById(R.id.img03);
        _base_bottom = (RelativeLayout) findViewById(R.id.base_bottom);
        _btnConfirm = (Button) findViewById(R.id.btnConfirm);

        _basePhoto01 = findViewById(R.id.basePhoto01);
        _basePhoto02 = findViewById(R.id.basePhoto02);

    }

    @Override
    protected void init() {
        //_menuIndex = 4;

        LogUtil.intent(getIntent());

        if(_accuse_type.equals(AtvReport.ACCUSE_TYPE_MAN)) {
            _txtTopTitle.setText("회원 신고");
        } else {
            _txtTopTitle.setText("갤러리 신고");
            
            if(!FormatUtil.isNullorEmpty(_artist_board_id)){
                _txtTopTitle.setText("별방 신고");
            }
        }
        _baseTopBottom.setVisibility(View.GONE);

        if (m_photoManger == null) {
            m_photoManger = new SelectPhotoManager(this);
            m_photoManger.setPhotoSelectCallback(this);
        }

        _imgBeanArr[0].setImageView(_img01);
        _imgBeanArr[1].setImageView(_img02);
        _imgBeanArr[2].setImageView(_img03);

        _img01.setBackground(getDrawable(R.drawable._s_background_rounding));
        _img01.setClipToOutline(true);
        _img02.setBackground(getDrawable(R.drawable._s_background_rounding));
        _img02.setClipToOutline(true);
        _img03.setBackground(getDrawable(R.drawable._s_background_rounding));
        _img03.setClipToOutline(true);

        if (_accuse_type.equals(AtvReport.ACCUSE_TYPE_BOARD)) {
            _basePhoto01.setVisibility(View.GONE);
            _basePhoto02.setVisibility(View.GONE);
        }
    }

    @Override
    protected void configureListener() {

        _edtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("onClick");
                CommonUtil.scrollToView(_edtContent, _scrollView, 0);
            }
        });

        _btnFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelectedPhotoIndex();
                m_photoManger.doPickFromGallery();
            }
        });

        _img01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAttach(0);
            }
        });
        _img02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAttach(1);
            }
        });
        _img03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAttach(2);
            }
        });

        _baseReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (_accuse_type.equals(AtvReport.ACCUSE_TYPE_MAN)) {
//                    AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
//                    adb.setItems(CodeUtil.getReportCodeNameArr(), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            LogUtil.d("which : " + which);
//
//                            _reasonCode = CodeUtil.getReportCode(which);
//                            _txtReason.setText(CodeUtil.getReportCodeName(which));
//                            _txtReason.setTextColor(getResources().getColor(R.color.white));
//                        }
//                    });
//                    adb.show();
//                } else {
                    AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
                    adb.setItems(CodeUtil.getReportBoardCodeNameArr(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            LogUtil.d("which : " + which);

                            _reasonCode = CodeUtil.getReportBoardCode(which);
                            _txtReason.setText(CodeUtil.getReportBoardCodeName(which));
                            _txtReason.setTextColor(getResources().getColor(R.color.black));
                        }
                    });
                    adb.show();
//                }
            }
        });


        _btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_reasonCode < 1) {
                    new Alert().showAlert(getContext(), "신고사유를 선택해 주세요.");
                    return;
                }

                String cont = _edtContent.getText().toString();

//                if (FormatUtil.isNullorEmpty(cont)) {
//                    new Alert().showAlert(getContext(), "신고 내용을 입력해 주세요.");
//                    return;
//                }
                File file01 = null, file02 = null, file03 = null;
                int i = 0;
                for (ImageBean bean : _imgBeanArr) {
                    i++;
                    if (i == 1) {
                        file01 = bean.getFile();
                    } else if (i == 2) {
                        file02 = bean.getFile();
                    } else if (i == 3) {
                        file03 = bean.getFile();
                    }
                }

                callApi_accuse_ins(file01, file02, file03);
            }
        });
    }


    private void onAttach(final int index) {

        if (!_imgBeanArr[index].isExistsData()) {
            setSelectedPhotoIndex();
            m_photoManger.doPickFromGallery();
            return;
        }

        Alert alert = new Alert();
        alert.setOnCloseListener(
                new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            _imgBeanArr[index].remove();
                            reordenar();
                        }
                    }
                }
        );
        alert.showAlert(getContext(), "삭제 하시겠습니까?", false, "확인", "취소");
    }


    private void reordenar() {

        if (!_imgBeanArr[0].isExistsData()) {
            _imgBeanArr[0].setImageBitmap(_imgBeanArr[1].getBitmap());
            _imgBeanArr[0].setLocalUri(_imgBeanArr[1].getLocalUri());
            _imgBeanArr[0].setFile(_imgBeanArr[1].getFile());

            _imgBeanArr[1].setImageBitmap(_imgBeanArr[2].getBitmap());
            _imgBeanArr[1].setLocalUri(_imgBeanArr[2].getLocalUri());
            _imgBeanArr[1].setFile(_imgBeanArr[2].getFile());

            _imgBeanArr[2].remove();
        }
        if (!_imgBeanArr[1].isExistsData()) {
            _imgBeanArr[1].setImageBitmap(_imgBeanArr[2].getBitmap());
            _imgBeanArr[1].setLocalUri(_imgBeanArr[2].getLocalUri());
            _imgBeanArr[1].setFile(_imgBeanArr[2].getFile());

            _imgBeanArr[2].remove();
        }

        setSelectedPhotoIndex();
    }


    private void setSelectedPhotoIndex() {
        int count = 0;
        for (ImageBean i : _imgBeanArr) {
            if (i.isExistsData()) count++;
        }

        _selectedPhotoIndex = count;

        if(_selectedPhotoIndex > 2) _selectedPhotoIndex = 2;
    }


    private void callApi_accuse_ins(File file01, File file02, File file03) {

        LogUtil.e("==========callApi_msg_accept : start==========");


        RequestBody ssid = RequestBody.create(MediaType.parse("text/plain"), "accuse.ins");
        RequestBody member_no = RequestBody.create(MediaType.parse("text/plain"), SPUtil.getInstance().getUserNoEnc(getContext()));

        MultipartBody.Part pic01 = null;
        MultipartBody.Part pic02 = null;
        MultipartBody.Part pic03 = null;

        String msg = _edtContent.getText().toString();

        msg = FormatUtil.isNullorEmpty(msg) ? "빠른신고" : msg;

        RequestBody target_member_no = RequestBody.create(MediaType.parse("text/plain"), _target_member_no);
        RequestBody accuse_type = RequestBody.create(MediaType.parse("text/plain"), _accuse_type);
        RequestBody reasonCode = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_reasonCode));
        RequestBody edtContent = RequestBody.create(MediaType.parse("text/plain"), msg);
        RequestBody boardNo = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_boardNo));
        RequestBody artist_board_id = RequestBody.create(MediaType.parse("text/plain"), FormatUtil.isNullorEmpty(_artist_board_id) ? "" : _artist_board_id);

        if (file01 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file01);
            pic01 = MultipartBody.Part.createFormData("pic1", file01.getName(), requestFile);
        }
        if (file02 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file02);
            pic02 = MultipartBody.Part.createFormData("pic2", file02.getName(), requestFile);
        }
        if (file03 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file03);
            pic03 = MultipartBody.Part.createFormData("pic3", file03.getName(), requestFile);
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<JsonObject> call = apiInterface.accuse_ins(
                ssid
                , member_no
                , target_member_no
                , accuse_type
                , reasonCode

                , edtContent
                , boardNo

                , pic01
                , pic02
                , pic03

                , artist_board_id
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "신고 접수가 완료 되었습니다.\n\n최대한 빨리 확인/처리해서 회신 드리겠습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public void onSelectImageDone(Bitmap image, File file) {
        LogUtil.d("onSelectImageDone");

        if (image != null && file != null) {
            BitmapFactory.Options onlyBoundsOptions2 = CommonUtil.getBitmapFactoryFromUri(this, Uri.fromFile(file));

            int photo_min_size = 320;

            // 핸드폰 디스플레이 사이즈보다 작은 사진은 선택 못하도록 합니다.
            if (onlyBoundsOptions2.outWidth <= photo_min_size || onlyBoundsOptions2.outHeight <= photo_min_size) {
                Alert alert = new Alert();
                alert.showAlert(this, getString(R.string.please_photo_width_01));
                return;
            }
            _imgBeanArr[_selectedPhotoIndex].setImageBitmap(image);
            _imgBeanArr[_selectedPhotoIndex].setFile(file);

            _selectedPhotoIndex++;
        } else {
            _imgBeanArr[_selectedPhotoIndex].remove();
        }
    }

    @Override
    public void onFailedSelectImage(int errorCode, String err) {

    }

    @Override
    public void onDeleteImage() {

    }

    @Override
    public void onPermissionUpdated(int permission_type, boolean isGranted) {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);
        LogUtil.d("onActivityResult, data : " + data);
        LogUtil.intent("onActivityResult", data);

        if (resultCode == Activity.RESULT_OK) {

            // 갤러리에서 사진 선택 시.
            if (requestCode == SelectPhotoManager.PICK_FROM_FILE) {
                if (data.getData() == null) {
                    new Alert().showAlert(getContext(), "사진을 가져오는 동안 오류가 발생하였습니다.");
                } else {
                    String str = data.getData().toString();
                    _imgBeanArr[_selectedPhotoIndex].setLocalUri(str);
                    LogUtil.d("str : " + str);

                    String url = CommonUtil.getRealPathFromURI(this, data.getData());
                    LogUtil.d("url : " + url);

                    String mimeType = CommonUtil.getMimeType(url);
                    LogUtil.d("mimeType : " + mimeType);

                    if (!FormatUtil.isNullorEmpty(mimeType) && (!mimeType.toLowerCase().endsWith("jpeg") && !mimeType.toLowerCase().endsWith("jpg") && !mimeType.toLowerCase().endsWith("png"))) {
                        new Alert().showAlert(this, "사진은 jpg, png 형태의 파일만 등록하실 수 있습니다.");
                        return;
                    }
                }

            }
        }

        if (requestCode == UCrop.REQUEST_CROP || requestCode == SelectPhotoManager.PICK_FROM_FILE) {
            // m_photoManger null 인 경우가 발생한다.
            if (m_photoManger == null) {
                m_photoManger = new SelectPhotoManager(this);
            }
            m_photoManger.setPhotoSelectCallback(this);
            m_photoManger.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        if(m_photoManger != null) {
            m_photoManger.removeTempFile(getContext());
        }
        super.onDestroy();
    }
}
