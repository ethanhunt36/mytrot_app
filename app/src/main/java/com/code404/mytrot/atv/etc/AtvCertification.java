package com.code404.mytrot.atv.etc;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.ImageUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.MediaScanner;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;

import com.gun0912.tedpermission.TedPermission;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import retrofit2.Call;

public class AtvCertification extends AtvBase {

    private View _base_certi = null;
    private de.hdodenhof.circleimageview.CircleImageView _imgProfile = null;
    private TextView _txtArtist = null;
    private TextView _txtMyNick = null;
    private TextView _txtPlayCount = null;
    private TextView _txtVoteCount = null;
    private TextView _txtDonateCount = null;
    private TextView _txtBoardCount01 = null;
    private TextView _txtBoardCount02 = null;
    private TextView _txtStarRoomCount = null;
    private TextView _txtHeartCount = null;
    private TextView _txtDate = null;
    private TextView _txtDate2 = null;

    private Button _btnDownload = null;
    private Button _btnShare = null;

    private int _artist_no = 7;
    private String _artist_name = "";
    private String _url = "";
    private String _period = "today";
    private String _fileName = "MYTROT_" + FormatUtil.getCurrentDateHms() + ".jpg";

    private boolean _have_permission = false;

    @Override
    protected boolean getBundle() {

        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        _url = getIntent().getStringExtra(Constants.EXTRAS_URL);
        _period = getIntent().getStringExtra(Constants.EXTRAS_TYPE);


        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_certification);
    }

    @Override
    protected void findView() {
        _base_certi = findViewById(R.id.base_certi);
        _imgProfile = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.imgProfile);
        _txtArtist = (TextView) findViewById(R.id.txtArtist);
        _txtMyNick = (TextView) findViewById(R.id.txtMyNick);
        _txtPlayCount = (TextView) findViewById(R.id.txtPlayCount);
        _txtVoteCount = (TextView) findViewById(R.id.txtVoteCount);
        _txtDonateCount = (TextView) findViewById(R.id.txtDonateCount);
        _txtBoardCount01 = (TextView) findViewById(R.id.txtBoardCount01);
        _txtBoardCount02 = (TextView) findViewById(R.id.txtBoardCount02);
        _txtStarRoomCount = findViewById(R.id.txtStarRoomCount);
        _txtHeartCount = findViewById(R.id.txtHeartCount);
        _txtDate = (TextView) findViewById(R.id.txtDate);
        _txtDate2 = (TextView) findViewById(R.id.txtDate2);

        _btnDownload = findViewById(R.id.btnDownload);
        _btnShare = findViewById(R.id.btnShare);
    }

    @Override
    protected void init() {
        _txtTopTitle.setText("활동 인증서");

        _baseTopTitle.setVisibility(View.GONE);
        _baseTopBottom.setVisibility(View.GONE);

        callApi_member_get_cert_info();

        Picasso.with(getContext())
                .load(_url).fit()
                .placeholder(R.drawable.img_noimg)
                .into(_imgProfile);

        init_check_permission();

        setDataBinding(null);

        ViewGroup.LayoutParams params = _base_certi.getLayoutParams();
        int width = params.width;
        params.height = width;
        _base_certi.setLayoutParams(params);


//        _btnDownload.setVisibility(View.GONE);
//        _btnShare.setVisibility(View.GONE);
    }


    private void init_check_permission() {

        TedPermission.with(getContext()).setPermissionListener(new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                _have_permission = true;
            }

            @Override
            public void onPermissionDenied(List<String> deniedPermissions) {
                // Possibility of denied even if this app granted permissions
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    _have_permission = true;

                } else {
                    _have_permission = false;
                }
            }
        }).setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE).check();
    }


    @Override
    protected void configureListener() {
        _btnDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q && _have_permission == false) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            init_check_permission();
                        }
                    });
                    alert.showAlert(getContext(), "저장 권한이 없습니다.");
                    return;
                }

//                String path = Request_Capture(_base_certi, _fileName);
//                if (FormatUtil.isNullorEmpty(path)) {
//                    Alert.toastLong(getContext(), "저장 실패 했습니다.");
//                    return;
//                }

                try {
                    saveImage(_base_certi, _fileName, "mytrot", false);
                } catch (Exception e) {
                    Alert.toastLong(getContext(), "저장 실패했습니다. 핸드폰 캡쳐 기능을 이용해주세요.");
                    e.printStackTrace();
                }


                Alert.toastLong(getContext(), "저장 되었습니다.");
            }
        });

        _btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q && _have_permission == false) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            init_check_permission();
                        }
                    });
                    alert.showAlert(getContext(), "저장 권한이 없습니다.");
                    return;
                }

//                String path = Request_Capture(_base_certi, _fileName);
//                if (FormatUtil.isNullorEmpty(path)) {
//                    Alert.toastLong(getContext(), "저장 실패 했습니다.");
//                    return;
//                }
//                try {
//                    saveImage(_base_certi, _fileName, "mytrot", false);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }


//                String filename = "image.png";
//                Bitmap bitmap = BitmapFactory.decodeFile(path);

                _base_certi.buildDrawingCache(); //캐시 비트 맵 만들기
                Bitmap bitmap = _base_certi.getDrawingCache();

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                //Uri screenshotUri = Uri.parse(path);    // android image path
                Uri screenshotUri = ImageUtil.getImageUri(getContext(), bitmap);

                if (screenshotUri != null) {
                    sharingIntent.setType("image/*");
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
                    startActivity(Intent.createChooser(sharingIntent, "친구에게 공유하기"));
                } else {
                    new Alert().showAlert(getContext(), "공유하기 실패 했습니다.");
                }
            }
        });
    }


    private Uri saveImage(View view, @NonNull String name, @NonNull String folder, boolean inAppFolder) throws IOException {

        view.buildDrawingCache(); //캐시 비트 맵 만들기
        Bitmap bitmap = view.getDrawingCache();

        if (bitmap == null) {
            Alert.toastLong(getContext(), "저장 실패했습니다. 핸드폰 캡쳐 기능을 이용해주세요.");
            return null;
        }


        boolean saved;
        OutputStream fos;
        Uri imageUri;


        // inAppFolder 는 /Android/data/앱패키지명/files 폴더에 위치
        // 파일 명이 같을 경우 덮어쓰기로 됨
        if (inAppFolder) {
            File image = new File(getExternalFilesDir(null), name);
            fos = new FileOutputStream(image);
            imageUri = Uri.fromFile(image);
        } else {
            // 공용공간에 저장, DCIM의 folder에
            // 파일명이 같을 경우 (1) (2)로 넘버링 됨
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                ContentResolver resolver = getContext().getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, name);
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/" + folder);
                imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                fos = resolver.openOutputStream(imageUri);
            } else {
                String imagesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).toString() + File.separator + folder;

                File file = new File(imagesDir);
                if (!file.exists()) {
                    file.mkdir();
                }

                File image = new File(imagesDir, name);
                fos = new FileOutputStream(image);
                imageUri = Uri.fromFile(image);
            }
        }

        saved = bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        if (saved) {
            fos.flush();
            fos.close();
            return imageUri;
        }


        return null;
    }

    public String Request_Capture(View view, String title) {
        if (view == null) { //Null Point Exception ERROR 방지
            LogUtil.d("::::ERROR:::: view == NULL");
            return "";
        }

        /* 캡쳐 파일 저장 */
        view.buildDrawingCache(); //캐시 비트 맵 만들기
        Bitmap bitmap = view.getDrawingCache();
        FileOutputStream fos;
        OutputStream os;

        /* 저장할 폴더 Setting */
        //File uploadFolder = Environment.getExternalStoragePublicDirectory("/DCIM/Mytrot/"); //저장 경로 (File Type형 변수)
        File uploadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        if (!uploadFolder.exists()) { //만약 경로에 폴더가 없다면
            uploadFolder.mkdir(); //폴더 생성
        }

        /* 파일 저장 */
        //String Str_Path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/Mytrot/"; //저장 경로 (String Type 변수)
        String Str_Path = uploadFolder.getAbsolutePath();
        String file_name = title;
        String file_path = Str_Path + "/" + file_name;

        LogUtil.d("file_path : " + file_path);
        Uri imageUri;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

            try {
                ContentResolver resolver = getContext().getContentResolver();
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, file_name);
                contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg");
                contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES);
                imageUri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                os = resolver.openOutputStream(imageUri);

                boolean saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                if (saved) {
                    os.flush();
                    os.close();
                }
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            }


        } else {
            try {
                fos = new FileOutputStream(file_path); // 경로 + 제목 + .jpg로 FileOutputStream Setting
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //캡쳐 파일 미디어 스캔 (https://hongdroid.tistory.com/7)

        MediaScanner ms = MediaScanner.newInstance(getApplicationContext());
        //이미지 스캔해서 갤러리 업데이트
        // sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
        MediaScannerConnection.scanFile(getApplicationContext(), new String[]{file_path}, null, new MediaScannerConnection.OnScanCompletedListener() {
            @Override
            public void onScanCompleted(String s, Uri uri) {
                LogUtil.d("onScanCompleted, s : " + s + ", uri : " + uri);
            }
        });

        try {
            ms.mediaScanning(file_path);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.d("::::ERROR:::: " + e);
        }

        return file_path;
    }

    private void setDataBinding(JSONObject json) {
        JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());

        _txtPlayCount.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "pnt_play", 0)) + "회");
        _txtVoteCount.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "pnt_vote", 0)) + "장");
        _txtDonateCount.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "pnt_donate", 0)) + "P");
        _txtHeartCount.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "heart_donate", 0)) + "하트");

        _txtBoardCount01.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "pnt_board1", 0)) + "회");
        _txtBoardCount02.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "pnt_board2", 0)) + "회");
        _txtStarRoomCount.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "pnt_starroom", 0)) + "회");

        _txtArtist.setText(_artist_name);
        _txtMyNick.setText(JSONUtil.getString(jsonUser, "nick"));

        String date_st = JSONUtil.getString(json, "date_st");

        if (FormatUtil.isNullorEmpty(date_st)) date_st = FormatUtil.getCurrentDate();

        _txtDate.setText(date_st);
        _txtDate2.setText(date_st);
    }


    private void callApi_member_get_cert_info() {

        LogUtil.e("==========callApi_member_get_cert_info : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.member_get_cert_info(SPUtil.getInstance().getUserNoEnc(getContext()), _artist_no, _period);

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                setDataBinding(data);

                callApi_memberMission_check_mission_get_cert();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_memberMission_check_mission_get_cert() {

        LogUtil.e("==========callApi_memberMission_check_mission_get_cert : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.memberMission_check_mission_get_cert(SPUtil.getInstance().getUserNoEnc(getContext()));

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }


}
