package com.code404.mytrot.atv.star;

import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GMultiListView;
import com.code404.mytrot.view.SliderListView;
import com.code404.mytrot.view.SquareImageView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;

public class AtvStar extends AtvBase implements GMultiListView.IMakeView {

    private static final int REQ_BOARD_INFO = 7419;
    private static final int REQ_WRITE = 3333;

    private SliderListView _sliderList = null;
    private GMultiListView _multiList = null;

    private View _header = null;
    private TextView _txtFilter = null;
    private TextView _txtSeqRecent = null;
    private TextView _txtSeqComment = null;
    private TextView _txtSeqLike = null;

    private String _last_no = "-1";
    private String _sort = "recent";
    private int _artist_no = -1;
    private String _artist_name = "";
    private boolean _is_more_data = true;
    private boolean _is_call_api_ing = false;


    @Override
    protected boolean getBundle() {
        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        return true;
    }

    @Override
    protected void init() {
        _canFinish = true;

        _menuIndex = 3;
        _txtTopTitle.setText("갤러리");

        _txtFilter.setVisibility(View.VISIBLE);
        _multiList.setRowItemCounts(2);
        _multiList.setViewMaker(R.layout.row_star, this);


        if (!FormatUtil.isNullorEmpty(_artist_name)) {
            _txtFilter.setText(_artist_name + " X");
        }

        callApi_list("recent");
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_star);
    }

    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _multiList = findViewById(R.id.multiList);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_gallery, null);
        _multiList.addHeaderView(_header);

        _txtFilter = _header.findViewById(R.id.txtFilter);
        _txtSeqRecent = _header.findViewById(R.id.txtSeqRecent);
        _txtSeqComment = _header.findViewById(R.id.txtSeqComment);
        _txtSeqLike = _header.findViewById(R.id.txtSeqLike);
    }

    @Override
    protected void configureListener() {
        findViewById(R.id.btnWrite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AdViewUtil.goActionAfterAdView(AtvStar.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent intent = new Intent();
                        intent.setClass(getContext(), AtvStarWrite.class);
                        intent.putExtra(Constants.EXTRAS_TYPE, 2);
                        startActivityForResult(intent, REQ_WRITE);
                    }
                });


//                int boardEnterCount = SPUtil.getInstance().getBoardEnterCount(getContext());
//                int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(getContext());
//                LogUtil.d("boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);
//
//                if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null && _adFullOrgManager.canShowAdCount_Interstitial(true,false) > 0
//                        && boardEnterCount % repeatBoardWriteAd == 0) {
//                    _adFullOrgManager.setOnAdCompleteMissionListener(new InterfaceSet.OnAdCompleteListener() {
//                        @Override
//                        public void onAdLoadSuccess() {
//                            LogUtil.d("_txtAd, AtvStar, onAdLoadSuccess");
//                        }
//
//                        @Override
//                        public void onAdLoadFail() {
//                            LogUtil.d("_txtAd, AtvStar, onAdLoadFail");
//                        }
//
//                        @Override
//                        public void onAdClose() {
//                            LogUtil.d("_txtAd, AtvStar, onAdClose");
//
//                            _adFullOrgManager.setOnAdCompleteMissionListener(null);
//
//                            Intent intent = new Intent();
//                            intent.setClass(getContext(), AtvStarWrite.class);
//                            intent.putExtra(Constants.EXTRAS_TYPE, 2);
//                            startActivityForResult(intent, REQ_WRITE);
//                        }
//                    });
//                    _adFullOrgManager.showAdOrder_Interstitial();
//                    return;
//                }
//
//                Intent intent = new Intent();
//                intent.setClass(getContext(), AtvStarWrite.class);
//                intent.putExtra(Constants.EXTRAS_TYPE, 2);
//                startActivityForResult(intent, REQ_WRITE);
            }
        });

        _txtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_artist_no < 1) {
                    AlertPop alert = new AlertPop();
                    alert.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
                        @Override
                        public void onClick(View v, int pos, JSONObject json) {
                            if (json != null) {
                                _artist_no = JSONUtil.getInteger(json, "no");
                                String name = JSONUtil.getString(json, "name");
                                _txtFilter.setText(name + " X");
                            }

                            LogUtil.d("_artist_no : " + _artist_no);

                            callApi_list("recent");
                        }
                    });
                    alert.showVoteArtist(getContext());
                } else {
                    _artist_no = -1;
                    _txtFilter.setText("전체");
                    callApi_list("recent");
                }
            }
        });

        _txtSeqRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApi_list("recent");
            }
        });
        _txtSeqComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApi_list("comment");
            }
        });
        _txtSeqLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApi_list("popular");
            }
        });

        _multiList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _multiList.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _multiList.isBottom());
                LogUtil.d("_multiList.isTop() : " + _multiList.isTop());

                if (_multiList.isBottom()) {
                    LogUtil.d("_multiList.isBottom()>> : true");
                    callApi_boardlist(false);
                }
            }
        });
    }


    private void callApi_list(String sort) {
        _sort = sort;
        _is_more_data = true;
        _last_no = "-1";

        _txtSeqRecent.setBackground(null);
        _txtSeqComment.setBackground(null);
        _txtSeqLike.setBackground(null);


        if (_sort.equals("recent")) {
            _txtSeqRecent.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
            callApi_boardlist(true);
        }
        if (_sort.equals("comment")) {
            _txtSeqComment.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
            callApi_boardlist(true);
        }
        if (_sort.equals("popular")) {
            _txtSeqLike.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
            callApi_boardlist(true);
        }

    }

    private void callApi_boardlist(boolean isClear) {

        LogUtil.e("==========callApi_boardlist : start==========");

        if (isClear) {
            _is_more_data = true;
            _last_no = "-1";
            _multiList.removeAll();
        }

        if (_is_more_data == false || _is_call_api_ing == true) {
            return;
        }

        _is_call_api_ing = true;

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        int category = 2; // 갤러리
        Call<JsonObject> call = apiInterface.board_listing(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _sort,
                Constants.RECORD_SIZE,
                String.valueOf(_last_no),
                category,
                _artist_no < 1 ? "" : String.valueOf(_artist_no)
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                _is_call_api_ing = false;
                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list_data");

                if (list.length() < 1) {
                    _is_more_data = false;
                    return;
                }

                _last_no = JSONUtil.getString(data, "last_no");

                _multiList.addItems(list);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                _is_call_api_ing = false;
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GMultiListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final ArrayList<JSONObject> list = adapter.getItem(position);

        LogUtil.d("list.size() : " + list.size());


        JSONObject info01 = list.size() > 0 ? list.get(0) : null;
        JSONObject info02 = list.size() > 1 ? list.get(1) : null;
        //JSONObject info03 = list.size() > 2 ? list.get(2) : null;

        SquareImageView img01 = convertView.findViewById(R.id.img01);
        SquareImageView img02 = convertView.findViewById(R.id.img02);

        TextView txtInfo01 = convertView.findViewById(R.id.txtInfo01);
        TextView txtInfo02 = convertView.findViewById(R.id.txtInfo02);


        setViewBinding(img01, txtInfo01, info01);
        setViewBinding(img02, txtInfo02, info02);


        FrameLayout adViewContainer = convertView.findViewById(R.id.adViewContainer);

        if (Constants.DISPLAY_AD
                && Constants.DISPLAY_AD_ROW
                && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD
                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
        ) {
            adViewContainer.setVisibility(View.VISIBLE);

            AdView adViewNew = new AdView(getContext());
            adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
            adViewContainer.addView(adViewNew);

            AdRequest adRequest = new AdRequest.Builder().build();
            adViewNew.setAdSize(getAdSizeInfo());
            adViewNew.loadAd(adRequest);
        } else {
            adViewContainer.setVisibility(View.GONE);
        }


        return convertView;
    }

    private JSONObject _selectedJson = null;

    private void setViewBinding(SquareImageView img, TextView txt, final JSONObject info) {
        String url = JSONUtil.getString(info, "img01");

        if (FormatUtil.isNullorEmpty(url)) {
            img.setVisibility(View.INVISIBLE);
            txt.setVisibility(View.INVISIBLE);
            return;
        }

        if (!url.startsWith("http")) {
            url = Constants.IMAGE_URL + url;
        }

        // 모서리 라운딩 처리
        img.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        img.setClipToOutline(true);

        Picasso.with(getContext())
                .load(url)
                .fit()
                .placeholder(R.drawable.img_noimg)
                .into(img);

        img.setVisibility(View.VISIBLE);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _selectedJson = info;

                Intent i = new Intent();
                i.setClass(getContext(), AtvStarInfo.class);
                i.putExtra(Constants.EXTRAS_JSON_STRING, info.toString());

                startActivityForResult(i, REQ_BOARD_INFO);

            }
        });

        txt.setText(String.format("좋아요 %s | 댓글 %s | 조회 %s"
                , FormatUtil.toPriceFormat(JSONUtil.getString(info, "like_cnt"))
                , FormatUtil.toPriceFormat(JSONUtil.getString(info, "comment_cnt"))
                , FormatUtil.toPriceFormat(JSONUtil.getString(info, "read_cnt"))
        ));

        txt.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onResume() {
        super.onResume();

        AdViewUtil.ready(AtvStar.this, getContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);
        LogUtil.d("onActivityResult, data : " + data);
        LogUtil.intent("onActivityResult", data);

        try {
            if (resultCode == RESULT_OK) {
                if (requestCode == REQ_WRITE) {
                    callApi_list("recent");
                }
                if (requestCode == REQ_BOARD_INFO) {
                    JSONObject jsonNew = JSONUtil.createObject(data.getStringExtra(Constants.EXTRAS_JSON_STRING));
                    String del_yn = JSONUtil.getString(jsonNew, "del_yn");


                    int no = JSONUtil.getInteger(jsonNew, "no");
                    int read_cnt = JSONUtil.getInteger(jsonNew, "read_cnt");
                    int comment_cnt = JSONUtil.getInteger(jsonNew, "comment_cnt");
                    int like_cnt = JSONUtil.getInteger(jsonNew, "read_cnt");

                    LogUtil.d("onActivityResult, no : " + no);
                    LogUtil.d("onActivityResult, read_cnt : " + read_cnt);

                    LogUtil.json(jsonNew);


                    if ("Y".equals(del_yn)) {
                        callApi_list("recent");
                    } else if (_selectedJson != null) {
                        JSONUtil.puts(_selectedJson, "like_cnt", like_cnt);
                        JSONUtil.puts(_selectedJson, "comment_cnt", comment_cnt);
                        JSONUtil.puts(_selectedJson, "read_cnt", read_cnt);

                        _multiList.getAdapter().notifyDataSetChanged();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
