package com.code404.mytrot.atv.vod;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.VodBindUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.widget.ItemNativeAd;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.JsonObject;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Random;

import retrofit2.Call;

public class AtvVod extends YouTubeBaseActivity implements GListView.IMakeView {

    private static final String KEY_YOUTUBE = "MYTROT_YOUTUBE";

    enum enum_play_type {
        none, circle_repeat, vod_repeat
    }

    enum enum_random_type {
        none, random
    }

    private boolean _isCallStart = false;

    //    private YouTubePlayerView _youtube;
    private com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView _youtube2;


    private ImageButton _btnPlayAll = null;
    private ImageButton _btnPlayBefore = null;
    private ImageButton _btnPause = null;
    private ImageButton _btnPlay = null;
    private ImageButton _btnPlayNext = null;
    private ImageButton _btnPlayRandom = null;


    private com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer _youTubePlayer2 = null;

    private ItemNativeAd _itemNativeAd = null;

    private JSONArray _jsonVodList;
    private static int _current_position = -1;
    private GListView _list = null;

    private static enum_play_type _enum_play_type = enum_play_type.none;
    private static enum_random_type _enum_random_type = enum_random_type.none;


    private VodBindUtil _vodBindUtil = null;

    private int _playCount = 0;


    private String _type = "";
    private String _youtube_key = "";


    // 밝기 관련
    private WindowManager.LayoutParams params;
    private float brightness; // 밝기값은 float형으로 저장되어 있습니다.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.atv_vod);

            findView();
            init();
            initYoutube();

            configureListener();

            // 화면 정보 불러오기
            params = getWindow().getAttributes();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findView() {
//        _youtube = findViewById(R.id.viewYoutube);
        _youtube2 = findViewById(R.id.viewYoutube2);


        _itemNativeAd = findViewById(R.id.itemNativeAd);

        _list = findViewById(R.id.list);

//        _viewYoutube = (com.google.android.youtube.player.YouTubePlayerView) findViewById(R.id.viewYoutube);
        _btnPlayAll = (ImageButton) findViewById(R.id.btnPlayAll);
        _btnPlayBefore = (ImageButton) findViewById(R.id.btnPlayBefore);
        _btnPause = (ImageButton) findViewById(R.id.btnPause);
        _btnPlay = (ImageButton) findViewById(R.id.btnPlay);
        _btnPlayNext = (ImageButton) findViewById(R.id.btnPlayNext);
        _btnPlayRandom = (ImageButton) findViewById(R.id.btnPlayRandom);
    }


    private void configureListener() {
        _btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play();
            }
        });
        _btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (_youTubePlayer != null) {
//                    _youTubePlayer.pause();
//                    _btnPlay.setVisibility(View.VISIBLE);
//                    _btnPause.setVisibility(View.GONE);
//                }

                if (_youTubePlayer2 != null) {
                    _youTubePlayer2.pause();
                    _btnPlay.setVisibility(View.VISIBLE);
                    _btnPause.setVisibility(View.GONE);
                }
            }
        });
        _btnPlayBefore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNextPlayIndex(true, false);
                playVod();
            }
        });
        _btnPlayNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_isCallStart) {
                    int vod_no = getVodNo();
                    callApi_artistvod_play_end(vod_no);
                }

                setNextPlayIndex(true, true);
                playVod();
            }
        });
        _btnPlayRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btnPlayRandom.setSelected(!_btnPlayRandom.isSelected());
                if (_btnPlayRandom.isSelected()) {
                    _enum_random_type = enum_random_type.random;
                    Alert.toastShort(getContext(), "셔플을 사용합니다.");
                } else {
                    _enum_random_type = enum_random_type.none;
                    Alert.toastShort(getContext(), "셔플을 사용하지 않습니다.");
                }
            }
        });
        _btnPlayAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_enum_play_type == enum_play_type.none) {
                    _enum_play_type = enum_play_type.circle_repeat;
                    _btnPlayAll.setImageResource(R.drawable._s_btn_play_all);
                    Alert.toastShort(getContext(), "전체 음악을 반복합니다.");
                } else if (_enum_play_type == enum_play_type.circle_repeat) {
                    _enum_play_type = enum_play_type.vod_repeat;
                    _btnPlayAll.setImageResource(R.drawable._s_btn_play_one);
                    Alert.toastShort(getContext(), "현재 음악을 반복합니다.");
                } else if (_enum_play_type == enum_play_type.vod_repeat) {
                    _enum_play_type = enum_play_type.none;
                    _btnPlayAll.setImageResource(R.drawable._s_btn_play_none);
                    Alert.toastShort(getContext(), "반복을 사용하지 않습니다.");
                }
            }
        });
    }


    private void init() {
        _itemNativeAd.setVisibility(View.GONE);
        _vodBindUtil = new VodBindUtil();

        _jsonVodList = SPUtil.getInstance().getYoutubeJsonArray(getContext());


        _type = getIntent().getStringExtra(Constants.EXTRAS_TYPE);
        _youtube_key = getIntent().getStringExtra(Constants.EXTRAS_KEYWORD);

        _list.setViewMaker(R.layout.row_vod, this);
        _btnPause.setVisibility(View.GONE);

        if (_enum_play_type == enum_play_type.none) {
            _btnPlayAll.setImageResource(R.drawable._s_btn_play_none);
        }


        if (KEY_YOUTUBE.equals(_type)) {
            callApi_artistVod_get_youtube_list();
            return;
        }

        int vod_no = getIntent().getIntExtra(Constants.EXTRAS_VOD_NO, -1);


        LogUtil.intent(getIntent());

        LogUtil.d("vod_no : " + vod_no);

        for (int i = 0; i < _jsonVodList.length(); i++) {
            JSONObject json = JSONUtil.getJSONObject(_jsonVodList, i);
            String vod_no_col_name = JSONUtil.getString(json, "vod_no_col_name");
            int t_vod_no = JSONUtil.getInteger(json, "vod_no");
            int t_no = JSONUtil.getInteger(json, "no");

            int no = JSONUtil.getInteger(json, "vod_no".equals(vod_no_col_name) ? "vod_no" : "no");

            LogUtil.d("vod_no_col_name : " + vod_no_col_name);
            LogUtil.d("t_vod_no : " + t_vod_no);
            LogUtil.d("t_no : " + t_no);
            LogUtil.d("no : " + no);

            if (no == vod_no) {
                _current_position = i;
                break;
            }
        }


        _list.addItems(_jsonVodList);


    }


    private void initYoutube() {
        String key = SPUtil.getInstance().getVodKey(getContext());

//        _youtube.initialize(key, new YouTubePlayer.OnInitializedListener() {
//            @Override
//            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
//                LogUtil.d("youtube, onInitializationSuccess");
//
//                if (youTubePlayer != null) {
//                    _youTubePlayer = youTubePlayer;
//                }
//                if (!b) {
//                    playVod();
//                }
//
//                youTubePlayer.setPlayerStateChangeListener(new YouTubePlayer.PlayerStateChangeListener() {
//                    @Override
//                    public void onLoading() {
//                        LogUtil.d("youtube, onLoading");
//                    }
//
//                    @Override
//                    public void onLoaded(String s) {
//                        LogUtil.d("youtube, onLoaded, s : " + s);
//                        youTubePlayer.play();
//                    }
//
//                    @Override
//                    public void onAdStarted() {
//                        LogUtil.d("youtube, onAdStarted");
//                    }
//
//                    @Override
//                    public void onVideoStarted() {
//                        LogUtil.d("youtube, onVideoStarted");
//                        _btnPlay.setVisibility(View.GONE);
//                        _btnPause.setVisibility(View.VISIBLE);
//                    }
//
//                    @Override
//                    public void onVideoEnded() {
//                        LogUtil.d("youtube, onVideoEnded");
//
//                        if (SPUtil.getInstance().getIsLogin(getContext())) {
//                            int vod_no = getVodNo();
//                            callApi_artistvod_play_end(vod_no);
//                        }
//
//                        setNextPlayIndex(false, true);
//                        playVod();
//                    }
//
//                    @Override
//                    public void onError(YouTubePlayer.ErrorReason errorReason) {
//                        LogUtil.d("youtube, onError : " + errorReason.name());
//                        new Alert().showAlert(getContext(), "youtube, onError : " + errorReason.name());
//                    }
//                });
//            }
//
//            @Override
//            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//                try {
//                    LogUtil.d("youtube, onInitializationFailure, youTubeInitializationResult : " + youTubeInitializationResult.toString());
//
//                    Dialog dialog = youTubeInitializationResult.getErrorDialog(AtvVod.this, youTubeInitializationResult.ordinal());
//
//                    if (dialog != null) {
//                        dialog.show();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });


        _youtube2.initialize(new AbstractYouTubePlayerListener() {
            @Override
            public void onApiChange(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer) {
                super.onApiChange(youTubePlayer);

                LogUtil.d("_youtube2, onApiChange");
            }

            @Override
            public void onCurrentSecond(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, float second) {
                super.onCurrentSecond(youTubePlayer, second);
                LogUtil.d("_youtube2, onCurrentSecond");
            }

            @Override
            public void onError(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerError error) {
                super.onError(youTubePlayer, error);
                LogUtil.d("_youtube2, onError");
            }

            @Override
            public void onPlaybackQualityChange(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlaybackQuality playbackQuality) {
                super.onPlaybackQualityChange(youTubePlayer, playbackQuality);
                LogUtil.d("_youtube2, onPlaybackQualityChange");
            }

            @Override
            public void onPlaybackRateChange(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlaybackRate playbackRate) {
                super.onPlaybackRateChange(youTubePlayer, playbackRate);
                LogUtil.d("_youtube2, onPlaybackRateChange");
            }

            @Override
            public void onReady(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer) {
                super.onReady(youTubePlayer);

                LogUtil.d("_youtube2, onReady");

                if (youTubePlayer != null) {
                    _youTubePlayer2 = youTubePlayer;
                }
                playVod();

                _btnPlay.setVisibility(View.GONE);
                _btnPause.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStateChange(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState state) {
                super.onStateChange(youTubePlayer, state);
                LogUtil.d("_youtube2, onStateChange : " + state.name());

                if (state.name().equals("VIDEO_CUED")) {
                    // 재생 준비 완료
                    youTubePlayer.play();
                } else if (state.name().equals("ENDED")) {
                    // 재생 종료
                    if (SPUtil.getInstance().getIsLogin(getContext())) {
                        int vod_no = getVodNo();
                        callApi_artistvod_play_end(vod_no);
                    }

                    setNextPlayIndex(false, true);
                    playVod();
                }
            }

            @Override
            public void onVideoDuration(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, float duration) {
                super.onVideoDuration(youTubePlayer, duration);
                LogUtil.d("_youtube2, onVideoDuration");
            }

            @Override
            public void onVideoId(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, @NonNull String videoId) {
                super.onVideoId(youTubePlayer, videoId);
                LogUtil.d("_youtube2, onVideoId : " + videoId);
            }

            @Override
            public void onVideoLoadedFraction(@NonNull com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer youTubePlayer, float loadedFraction) {
                super.onVideoLoadedFraction(youTubePlayer, loadedFraction);
                LogUtil.d("_youtube2, onVideoLoadedFraction");
            }
        });

    }


    private void setNextPlayIndex(boolean isClick, boolean direct) {
        if (isClick == false) {
            if (_enum_play_type == enum_play_type.vod_repeat) {
                return;
            }
        }
        if (_enum_random_type == enum_random_type.random) {
            setRandomIndex();
        } else {
            if (direct) {
                _current_position++;
            } else {
                _current_position--;
                if (_current_position < 0) {
                    _current_position = _jsonVodList.length() - 1;
                }
            }
        }
    }


    private void setRandomIndex() {
        int length = _jsonVodList.length();

        if (length < 1) {
            return;
        }

        Random r = new Random();
        _current_position = r.nextInt(length);
    }

    private String getCurrentPlayID() {
        int length = _jsonVodList.length();

        if (length < 1) {
            return "";
        }

        if (_current_position < 0 || _current_position >= length) {
            _current_position = 0;
        }

        JSONObject json = JSONUtil.getJSONObject(_jsonVodList, _current_position);
        String youtube_id = JSONUtil.getString(json, "youtube_id");

        return youtube_id;
    }

    private Context getContext() {
        return AtvVod.this;
    }


    private void callApi_artistVod_get_youtube_list() {
        LogUtil.e("==========callApi_artistVod_get_youtube_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistVod_get_youtube_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                _jsonVodList = JSONUtil.getJSONArray(data, "list");

                _list.addItems(_jsonVodList);
                _list.setNoDataVisibility(_list.getCountAll() < 1);


                for (int i = 0; i < _jsonVodList.length(); i++) {
                    JSONObject jsonV = JSONUtil.getJSONObject(_jsonVodList, i);
                    String youtube_id = JSONUtil.getString(jsonV, "youtube_id");

                    if (youtube_id.equals(_youtube_key)) {
                        _current_position = i;
                        break;
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_artistvod_play_start(int vod_no) {
        LogUtil.e("==========callApi_artistvod_play_start : start==========");

        if (KEY_YOUTUBE.equals(_type)) {
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistvod_play_start(SPUtil.getInstance().getUserNoEnc(getContext()), vod_no);
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                LogUtil.d("onFailure");
            }
        });
        _isCallStart = true;
    }


    private void callApi_artistvod_play_end(int vod_no) {
        LogUtil.e("==========callApi_artistvod_play_end : start==========");

        if (KEY_YOUTUBE.equals(_type)) {
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistvod_play_end(SPUtil.getInstance().getUserNoEnc(getContext()), vod_no);
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                LogUtil.d("onFailure");
            }
        });
        _isCallStart = false;
    }


    private void setListBookmark(int position, int is_bookmark) {
        try {
            JSONObject json = _list.getItem(position);
            JSONUtil.puts(json, "is_bookmark", is_bookmark);
            _list.getAdapter().notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);


        if (KEY_YOUTUBE.equals(_type)) {
            TextView txtType = (TextView) convertView.findViewById(R.id.txtType);
            TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
            ImageView imgYoutube = (ImageView) convertView.findViewById(R.id.imgYoutube);
            TextView txtPlayCount = (TextView) convertView.findViewById(R.id.txtPlayCount);
            TextView txtPlayCountMy = (TextView) convertView.findViewById(R.id.txtPlayCountMy);
            TextView txtPlayMinute = (TextView) convertView.findViewById(R.id.txtPlayMinute);

            ImageButton btnHeart = (ImageButton) convertView.findViewById(R.id.btnHeart);
            View baseLine = (View) convertView.findViewById(R.id.baseLine);
            View adViewBody = convertView.findViewById(R.id.adViewBody);

            AdView adView = convertView.findViewById(R.id.adView);

            Button btnYoutube = convertView.findViewById(R.id.btnYoutube);

            String youtube_id = JSONUtil.getString(json, "youtube_id");

            txtType.setVisibility(View.GONE);
            adViewBody.setVisibility(View.GONE);
            txtPlayMinute.setVisibility(View.GONE);
            baseLine.setVisibility(View.VISIBLE);

            String title = JSONUtil.getString(json, "title");
            title = title.replace("by 마이트롯 광고펀딩", "").replace("마이트롯", "").replace("CM보드 광고", "");
            title = title.trim();

            txtName.setText(title);
            //txtPlayCount.setText(JSONUtil.getString(json, "title_show"));
            txtPlayCount.setVisibility(View.GONE);
            txtPlayCountMy.setVisibility(View.GONE);

            String img01 = JSONUtil.getStringUrl(json, "thumb_url");

            if (!FormatUtil.isNullorEmpty(img01)) {
                Picasso.with(getContext())
                        .load(img01)
                        .fit()
                        .placeholder(R.drawable.img_noimg)
                        .into(imgYoutube);
            }

            btnHeart.setVisibility(View.GONE);
            btnYoutube.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse("http://youtu.be/" + youtube_id));

                        getContext().startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

        } else {
            convertView = _vodBindUtil.setViewEvent(getContext(), json, _jsonVodList, convertView, position, _list.getAdapter(), _current_position, SPUtil.getInstance().getAdYoutube(getContext()).equals("Y") ? false : true);
        }

        View baseRowVod = convertView.findViewById(R.id.baseRowVod);

        String is_play = JSONUtil.getString(json, "is_play", "N");

        LogUtil.d("is_play : " + is_play);

        if (!FormatUtil.isNullorEmpty(is_play)) {
            baseRowVod.setBackgroundColor(is_play.equals("N") ? getResources().getColor(R.color.white) : getResources().getColor(R.color.play_row));
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Play
//                if (_youTubePlayer != null) {
//
//                    if (_isCallStart) {
//                        int vod_no = getVodNo();
//                        callApi_artistvod_play_end(vod_no);
//                    }
//
//                    _current_position = position;
//                    playVod();
//                }

                if (_youTubePlayer2 != null) {
//
                    if (_isCallStart) {
                        int vod_no = getVodNo();
                        callApi_artistvod_play_end(vod_no);
                    }

                    _current_position = position;
                    playVod();
                }
            }
        });

        return convertView;
    }


    private void play() {
        try {

//            if (_youTubePlayer != null) {
//                if (_youTubePlayer.isPlaying() == false) {
//                    _youTubePlayer.play();
//                    _btnPlay.setVisibility(View.GONE);
//                    _btnPause.setVisibility(View.VISIBLE);
//                }
//            }

            if (_youTubePlayer2 != null) {
                _youTubePlayer2.play();
                _btnPlay.setVisibility(View.GONE);
                _btnPause.setVisibility(View.VISIBLE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void playVod() {
        try {
            _playCount++;
            LogUtil.d("play, _playCount : " + _playCount);

            String youtubeID = getCurrentPlayID();
//            if (_youTubePlayer != null) {
//                _youTubePlayer.cueVideo(youtubeID);
//                _webView.loadUrl(Constants.getServerUrl() + "/vod/youtube.php?videoId=" + youtubeID);
//            }

            if (_youTubePlayer2 != null) {
                _youTubePlayer2.cueVideo(youtubeID, 0);
            }


            for (int i = 0; i < _jsonVodList.length(); i++) {
                JSONObject j = JSONUtil.getJSONObject(_jsonVodList, i);
                JSONUtil.puts(j, "is_play", "N");
            }
            JSONObject j = JSONUtil.getJSONObject(_jsonVodList, _current_position);
            JSONUtil.puts(j, "is_play", "Y");

            _list.getAdapter().notifyDataSetChanged();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (_list.getCountAll() < 20) {
                        _list.smoothScrollToPositionFromTop(_current_position, 0);
                    } else {
                        _list.setSelection(_current_position);
                    }
                }
            }, 200);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (SPUtil.getInstance().getIsLogin(getContext())) {
                        int vod_no = getVodNo();
                        callApi_artistvod_play_start(vod_no);
                    }
                }
            }, 400);

            LogUtil.d("_playCount : " + _playCount);

            if (_playCount > 1 && _playCount % 5 == 0 && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
                int lv = JSONUtil.getInteger(jsonUser, "lv", 0);
                int vod_time = JSONUtil.getInteger(jsonUser, "vod_time", 0);

                LogUtil.d("lv : " + lv + ", vod_time : " + vod_time);

                if (lv > 1 || vod_time > 30) {
                    _itemNativeAd.initAd();
                    _itemNativeAd.setVisibility(View.VISIBLE);
                }
            } else {
                _itemNativeAd.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private int getVodNo() {
        JSONObject json = JSONUtil.getJSONObject(_jsonVodList, _current_position);
        String vod_no_col_name = JSONUtil.getString(json, "vod_no_col_name");
        int vod_no = JSONUtil.getInteger(json, "vod_no".equals(vod_no_col_name) ? "vod_no" : "no");
        return vod_no;
    }


    protected void onResume() {
        super.onResume();

        try {
//            if (_youTubePlayer != null) {
//                if (_youTubePlayer.isPlaying() == false) {
//
//                }
//            }

            if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                // 화면 꺼짐 방지
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        // 기존 밝기 저장
//        brightness = params.screenBrightness;
//        // 최저 밝기로 설정
//        params.screenBrightness = 0.0f;
//        // 밝기 설정 적용
//        getWindow().setAttributes(params);
    }

    protected void onPause() {
        try {
            super.onPause();

//            if (_youTubePlayer != null && _youTubePlayer.isPlaying()) {
//                _youTubePlayer.pause();
//
//                _btnPlay.setVisibility(View.VISIBLE);
//                _btnPause.setVisibility(View.GONE);
//            }

            if (_youTubePlayer2 != null) {
                _youTubePlayer2.pause();

                _btnPlay.setVisibility(View.VISIBLE);
                _btnPause.setVisibility(View.GONE);
            }
            if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                LogUtil.d("화면 꺼짐 방지 해제");
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        // 기존 밝기로 변경
//        params.screenBrightness = brightness;
//        getWindow().setAttributes(params);
    }

    private void finishVod() {
        int vod_no = getVodNo();
        callApi_artistvod_play_end(vod_no);

        finish();
    }

    protected void onBack() {
        finishVod();
    }

    @Override
    public void onBackPressed() {
        if (_btnPlay.getVisibility() == View.VISIBLE) {
            finishVod();
            return;
        }
        String title = getString(R.string.alert_youtube);
        Alert alert = new Alert();
        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
            @Override
            public void onClose(DialogInterface dialog, int which) {
                if (which == Alert.BUTTON1) {
                    finishVod();
                }
            }
        });
        alert.showAlert(getContext(), title, true, "확인", "취소");
    }

    @Override
    protected void onDestroy() {
        try {
            super.onDestroy();
            if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                LogUtil.d("화면 꺼짐 방지 해제");
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        try {
            LogUtil.d("onConfigurationChanged");

            super.onConfigurationChanged(newConfig);

            if (_list != null && _list.getAdapter() != null) {
                _list.getAdapter().notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
