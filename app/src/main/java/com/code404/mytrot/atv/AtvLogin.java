package com.code404.mytrot.atv;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.code404.mytrot.MyTrotApp;
import com.code404.mytrot.R;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonObject;
import com.kakao.auth.ApprovalType;
import com.kakao.auth.AuthType;
import com.kakao.auth.IApplicationConfig;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.ISessionConfig;
import com.kakao.auth.KakaoAdapter;
import com.kakao.auth.KakaoSDK;
import com.kakao.auth.Session;
import com.kakao.network.ErrorResult;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.MeV2ResponseCallback;
import com.kakao.usermgmt.response.MeV2Response;
import com.kakao.util.exception.KakaoException;
import com.nhn.android.naverlogin.OAuthLogin;
import com.nhn.android.naverlogin.OAuthLoginHandler;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvLogin extends AtvBase {

    private static int REQUEST_CODE_SIGN_IN = 8787;

    private View _base_google = null;
    private View _base_kakao = null;
    private View _base_naver = null;
    private View _base_no_regist = null;

    private ImageView _img_logo = null;


    private SessionCallbackKakao callback_kakao;
    private OAuthLogin mOAuthLoginInstanceNaver;


    private TextView _txtQuestion = null;


    @Override
    protected void setView() {
        setView(R.layout.atv_login);
    }

    @Override
    protected void init() {
        _baseTopTitle.setVisibility(View.GONE);
        _baseTopBottom.setVisibility(View.GONE);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();


        _googleSignInClient = GoogleSignIn.getClient(getContext(), gso);


        _img_logo.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        _img_logo.setClipToOutline(true);

        setup_sns_environment();

//        String key = CommonUtil.getKeyHash(getContext());
//
//        LogUtil.d("key : " + key);

        _txtQuestion.setText(Html.fromHtml("<u> 문의하기 </u>"));

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _base_naver.setVisibility(View.GONE);
        }


        try {
            KakaoSDK.init(new KakaoSDKAdapter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private GoogleSignInClient _googleSignInClient = null;

    @Override
    protected void configureListener() {
        _base_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = _googleSignInClient.getSignInIntent();
                startActivityForResult(i, REQUEST_CODE_SIGN_IN);
            }
        });

        _base_kakao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginKakao();
            }
        });

        _base_naver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginNaver();
            }
        });

        _base_no_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // 안드로이드 아이디를 리프레쉬를 한번 하고.
                SPUtil.getInstance().refreshAndroidID(getContext());

                String uniqueID = SPUtil.getInstance().getAndroidID(getContext());
                LogUtil.d("uniqueID : " + uniqueID);
                callApi_member_get_by_email(uniqueID, "A", uniqueID);
            }
        });

        _txtQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Uri uri = Uri.parse("mailto:code404.app@gmail.com?subject=마이트롯문의&body=");

                    Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
                    startActivity(intent);
                } catch (Exception e) {

                }
            }
        });
    }


    @Override
    protected void findView() {
        _base_google = findViewById(R.id.base_google);
        _base_kakao = findViewById(R.id.base_kakao);
        _base_naver = findViewById(R.id.base_naver);
        _img_logo = findViewById(R.id.img_logo);
        _txtQuestion = findViewById(R.id.txtQuestion);
        _base_no_regist = findViewById(R.id.base_no_regist);
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount acccount = completedTask.getResult(ApiException.class);
            updateUI(acccount);
        } catch (ApiException e) {
            e.printStackTrace();
            updateUI(null);
        }
    }

    public void updateUI(GoogleSignInAccount account) {
        if (account == null) {
            LogUtil.d("updateUI, account : null");
            return;
        }

        String name = account.getDisplayName();
        String email = account.getEmail();
        String id = account.getId();

        LogUtil.d(String.format("updateUI, account, name : %s, email : %s, id : %s", name, email, id));

        callApi_member_get_by_sns_v2(email, "G", id);
    }


    ///////////////////////////
    enum enum_action_result_type {
        none,
        kakao,
        naver,
        payment
    }

    private enum_action_result_type _enum_action_result_type = enum_action_result_type.none;

    public void setup_sns_environment() {

        try {
            /*
             * 네이버 로그인 관련
             * */
            mOAuthLoginInstanceNaver = OAuthLogin.getInstance();

            mOAuthLoginInstanceNaver.showDevelopersLog(true);
            mOAuthLoginInstanceNaver.init(this, getString(R.string.naver_client_id), getString(R.string.naver_client_secret), getString(R.string.naver_client_name));

            /*
             * 카카오 로그인 관련
             * */
            callback_kakao = new SessionCallbackKakao();
            Session.getCurrentSession().addCallback(callback_kakao);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loginNaver() {
        try {
            _enum_action_result_type = enum_action_result_type.naver;
            mOAuthLoginInstanceNaver.startOauthLoginActivity(this, mOAuthLoginHandler);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loginKakao() {
        _enum_action_result_type = enum_action_result_type.kakao;
        Session.getCurrentSession().open(AuthType.KAKAO_LOGIN_ALL, this);
    }

    /**
     * Kakao
     */
    private class SessionCallbackKakao implements ISessionCallback {

        /*
         * access token을 성공적으로 발급 받아 valid access token을 가지고 있는 상태
         * */
        @Override
        public void onSessionOpened() {
            LogUtil.d("kakao onSessionOpened");

            // 사용자 정보 요청
            // 세션이 열려있더라도 카카오 로그인을 원하는 것이 아니면 Pass
            //if (DeliveryMan.temp_login_type == (LOGIN_TYPE_KAKAO)) {
            requestMe();
            //}

//            kakaoConnect();

        }

        /*
         * memory와 cache에 session 정보가 전혀 없는 상태. 일반적으로 로그인 버튼이 보이고 사용자가 클릭시 동의를 받아 access token 요청을 시도한다.
         * */
        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            if (exception != null) {
                LogUtil.d(exception.toString());
                LogUtil.d("kakao onSessionOpenFailed");

            }
        }
    }

    private void requestMe() {
        UserManagement.getInstance().me(new MeV2ResponseCallback() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                super.onFailure(errorResult);
                String message = "failed to get user info. msg=" + errorResult;
                LogUtil.d("kakao onFailure " + message);
            }

            @Override
            public void onFailureForUiThread(ErrorResult errorResult) {
                super.onFailureForUiThread(errorResult);
                String message = "failed to get user info. msg=" + errorResult;
                LogUtil.d("kakao onFailureForUiThread " + message);

                if (errorResult.getErrorCode() == -401) {
//                    Intent i = new Intent(cx_base, LoginActivity.class);
//                    startActivity(i);
//                    finishAffinity();

                }

            }

            @Override
            public void onSessionClosed(ErrorResult errorResult) {
                LogUtil.d("kakao onSessionClosed");
            }

            @Override
            public void onSuccess(MeV2Response result) {
                LogUtil.d(result.toString());
                String id = String.valueOf(result.getId());
                String email = result.getKakaoAccount() != null ? result.getKakaoAccount().getEmail() : "";

                LogUtil.d("kakao result : " + result.toString());
                LogUtil.d("kakao id : " + id);
                LogUtil.d("kakao email : " + email);

                if (FormatUtil.isNullorEmpty(email)) {
                    new Alert().showAlert(getContext(), "카카오톡에서 이메일을 수집하지 못했습니다.\n\n다른 SNS 로그인을 이용바랍니다.");
                    return;
                }

                if (!FormatUtil.isNullorEmpty(id) && !FormatUtil.isNullorEmpty(email)) {
                    //callApi_member_get_by_email(email, "K", id);
                    //callApi_member_get_by_sns("K", id);

                    callApi_member_get_by_sns_v2(email, "K", id);

                }

                //mBinding.webView.callScriptMethod("nativeSnsLoginResult", "K", id, "", "", sex);

            }

        });
    }


    /**
     * Naver
     */
    public OAuthLoginHandler mOAuthLoginHandler = new OAuthLoginHandler() {
        @Override
        public void run(boolean success) {
            try {
                if (success) {
                    String accessToken = mOAuthLoginInstanceNaver.getAccessToken(getContext());
                    String refreshToken = mOAuthLoginInstanceNaver.getRefreshToken(getContext());
                    long expiresAt = mOAuthLoginInstanceNaver.getExpiresAt(getContext());
                    String tokenType = mOAuthLoginInstanceNaver.getTokenType(getContext());
                    LogUtil.d("naver accessToken " + accessToken);
                    LogUtil.d("naver refreshToken " + refreshToken);
                    LogUtil.d("naver expiresAt " + expiresAt);
                    LogUtil.d("naver tokenType " + tokenType);

                    new RequestApiTaskNaver().execute();
//                mOauthAT.setText(accessToken);
//                mOauthRT.setText(refreshToken);
//                mOauthExpires.setText(String.valueOf(expiresAt));
//                mOauthTokenType.setText(tokenType);
//                mOAuthState.setText(mOAuthLoginInstanceNaver.getState(cx).toString());
                } else {
                    String errorCode = mOAuthLoginInstanceNaver.getLastErrorCode(getContext()).getCode();
                    String errorDesc = mOAuthLoginInstanceNaver.getLastErrorDesc(getContext());
                    LogUtil.d("naver errorCode:" + errorCode + ", errorDesc:" + errorDesc);
//                Toast.makeText(cx, "errorCode:" + errorCode + ", errorDesc:" + errorDesc, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    };

    private class RequestApiTaskNaver extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
//            mApiResultText.setText((String) "");

        }

        @Override
        protected String doInBackground(Void... params) {
            String url = "https://openapi.naver.com/v1/nid/me";
            String at = mOAuthLoginInstanceNaver.getAccessToken(getContext());
            return mOAuthLoginInstanceNaver.requestApi(getContext(), at, url);
        }

        protected void onPostExecute(String content) {
            //{"resultcode":"00","message":"success","response":{"id":"34835743","nickname":"\ub9c8\uc74c","gender":"M","birthday":"03-03"}}
            LogUtil.d("naver onPostExecute : " + content);

            String resultcode = "";
            String message = "";


            JSONObject result = null;
            try {
                result = new JSONObject(content);

                message = result.optString("message");
                resultcode = result.optString("resultcode");


            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (resultcode.equals("00")) {

                JSONObject JO_response = result.optJSONObject("response");

                String id = JO_response.optString("id");
                String email = JO_response.optString("email");
                String name = JO_response.optString("name");

                int sex = -1;
                if (JO_response.optString("gender").equals("M")) {
                    sex = 1;
                } else if (JO_response.optString("gender").equals("F")) {
                    sex = 2;
                }

                //checkEmailOrSNSidExist(0);


                LogUtil.d("naver, id : " + id);
                LogUtil.d("naver, email : " + email);
                LogUtil.d("naver, name : " + name);
                LogUtil.d("naver, sex : " + sex);

                //mBinding.webView.callScriptMethod("nativeSnsLoginResult", "N", id, email, name, sex);

                if (!FormatUtil.isNullorEmpty(id) && !FormatUtil.isNullorEmpty(email)) {
                    //callApi_member_get_by_email(email, "N", id);
                    //callApi_member_get_by_sns("N", id);
                    callApi_member_get_by_sns_v2(email, "N", id);
                }

            } else {
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());

                alert.setMessage(resultcode + " " + message);
                alert.setPositiveButton("확인",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                alert.show();
            }


        }
    }


    ///


    ///////////


    private void callApi_member_set_sns(final String login_type, final String sns_id) {

        LogUtil.e("==========callApi_regist : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_set_sns(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                login_type,
                sns_id
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


                if (!FormatUtil.isNullorEmpty(resultMessage)) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            callApi_member_get_by_no(true);
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");

                //new Alert().showAlert(getContext(), "관리자에게 문의 바랍니다.");
            }
        });

    }


    private void callApi_member_get_by_no(final boolean isCallPpobkki) {

        LogUtil.e("==========callApi_member_get_by_no : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_no(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                if (resultCode == 0) {

                    int member_no = JSONUtil.getInteger(userInfo, "no");
                    String member_no_enc = JSONUtil.getString(userInfo, "member_no");

                    SPUtil.getInstance().setIsLogin(getContext(), true);
                    SPUtil.getInstance().setUserNo(getContext(), member_no);
                    SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                    SPUtil.getInstance().setUserNoEnc(getContext(), member_no_enc);


                    Intent intent = new Intent(getContext(), AtvVote.class);
                    startActivity(intent);
                    finishAffinity();

                }

                if (isCallPpobkki) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    public void onDestroy() {

        super.onDestroy();

        Session.getCurrentSession().removeCallback(callback_kakao);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);

        if (REQUEST_CODE_SIGN_IN == requestCode) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else if (_enum_action_result_type == enum_action_result_type.kakao) {
            Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data);
        } else if (_enum_action_result_type == enum_action_result_type.naver) {
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

        _enum_action_result_type = enum_action_result_type.none;
    }


    private static class KakaoSDKAdapter extends KakaoAdapter {
        /**
         * Session Config에 대해서는 default값들이 존재한다.
         * 필요한 상황에서만 override해서 사용하면 됨.
         *
         * @return Session의 설정값.
         */
        @Override
        public ISessionConfig getSessionConfig() {
            return new ISessionConfig() {
                @Override
                public AuthType[] getAuthTypes() {
                    return new AuthType[]{AuthType.KAKAO_LOGIN_ALL};
                }

                @Override
                public boolean isUsingWebviewTimer() {
                    return false;
                }

                @Override
                public boolean isSecureMode() {
                    return false;
                }

                @Override
                public ApprovalType getApprovalType() {
                    return ApprovalType.INDIVIDUAL;
                }

                @Override
                public boolean isSaveFormData() {
                    return true;
                }
            };
        }

        @Override
        public IApplicationConfig getApplicationConfig() {
            return new IApplicationConfig() {
                @Override
                public Context getApplicationContext() {
                    return MyTrotApp.getGlobalApplicationContext();
                }
            };
        }
    }
}