package com.code404.mytrot.atv.event;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAction;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GMultiListView;
import com.code404.mytrot.view.SliderListView;
import com.code404.mytrot.widget.ItemPPRank;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import retrofit2.Call;

public class AtvEventPP extends AtvBase implements GMultiListView.IMakeView {

    private SliderListView _sliderList = null;

    // private GListView _list = null;
    private GMultiListView _multiList = null;
    private View _baseNoData = null;

    private View _header = null;

    private ItemPPRank _itemReward01 = null;
    private ItemPPRank _itemReward02 = null;
    private ItemPPRank _itemReward03 = null;
    private ItemPPRank _itemReward04 = null;
    private ItemPPRank _itemReward05 = null;

    private int _ppobkki_no = -1;

    // 광고를 시청했는지 여부
    private boolean _isViewAd = true;

    @Override
    protected boolean getBundle() {
        String isViewAd_YN = getIntent().getStringExtra(Constants.EXTRAS_TYPE);

        if ("N".equals(isViewAd_YN)) _isViewAd = false;

        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_list);
    }

    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        //_list = (GListView) findViewById(R.id.list);
        _multiList = findViewById(R.id.multiList);

        _baseNoData = findViewById(R.id.baseNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_pp, null);
        _multiList.addHeaderView(_header);


        _itemReward01 = (ItemPPRank) _header.findViewById(R.id.itemReward01);
        _itemReward02 = (ItemPPRank) _header.findViewById(R.id.itemReward02);
        _itemReward03 = (ItemPPRank) _header.findViewById(R.id.itemReward03);
        _itemReward04 = (ItemPPRank) _header.findViewById(R.id.itemReward04);
        _itemReward05 = (ItemPPRank) _header.findViewById(R.id.itemReward05);


    }

    private CountDownTimer _timer = null;

    @Override
    protected void init() {
        _txtTopTitle.setText("추억의 뽑기");
        _txtTopTitle.setVisibility(View.GONE);
        _imgTopTitle.setImageResource(R.drawable.txt_draw);
        _imgTopTitle.setVisibility(View.VISIBLE);
        _baseTopBottom.setVisibility(View.GONE);

        _multiList.setRowItemCounts(5);
        _multiList.setViewMaker(R.layout.row_pp, this);
        _multiList.setNoData(_baseNoData);

        _itemReward01.setRank(1);
        _itemReward02.setRank(2);
        _itemReward03.setRank(3);
        _itemReward04.setRank(4);
        _itemReward05.setRank(5);


        callApi_ppobkki_get_list(true);

        initTimer();
    }

    private void initTimer() {
        _timer = new CountDownTimer((1000L) * 60 * 60 * 4, 1000L) {
            @Override
            public void onTick(long millisUntilFinished) {
                // Do nothing
                LogUtil.d("onTick, millisUntilFinished : " + millisUntilFinished);

            }

            @Override
            public void onFinish() {

                LogUtil.d("onTick, onFinish");

            }
        };
    }


    @Override
    protected void configureListener() {


    }

    private void callApi_ppobkki_get_list(final boolean isClear) {

        LogUtil.e("==========callApi_ppobkki_get_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.ppobkki_get_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "N"
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONObject bonus = JSONUtil.getJSONObject(data, "bonus");

                String can_time_cheat = JSONUtil.getString(data, "can_time_cheat", "N");

                if (_timer == null) {
                    initTimer();
                }
                _timer.start();
                for (int i = 1; i <= 5; i++) {
                    JSONObject bonus_temp = JSONUtil.getJSONObject(bonus, String.valueOf(i));

                    switch (i) {
                        case 1:
                            _itemReward01.setData(bonus_temp);
                            break;
                        case 2:
                            _itemReward02.setData(bonus_temp);
                            break;
                        case 3:
                            _itemReward03.setData(bonus_temp);
                            break;
                        case 4:
                            _itemReward04.setData(bonus_temp);
                            break;
                        case 5:
                            _itemReward05.setData(bonus_temp);
                            break;
                    }
                }

                JSONArray list = JSONUtil.getJSONArray(data, "list");

                if (isClear) {
                    _multiList.removeAll();
                }


                _multiList.addItems(list);
                _multiList.setNoDataVisibility(_multiList.getCountAll() < 1);


                if (list.length() < 1) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), "진행중인 뽑기가 없습니다.\n\n잠시 후에 다시 이용해주세요.");
                    return;
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_ppobkki_play(int no) {

        LogUtil.e("==========callApi_ppobkki_play : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.ppobkki_play_v2(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , no
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(final int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {

                    JSONObject data = JSONUtil.getJSONObject(json, "data");
                    JSONObject bonus = JSONUtil.getJSONObject(data, "bonus");

                    JSONArray list = JSONUtil.getJSONArray(json, "list");

                    if (list != null && list.length() > 0) {
                        _multiList.removeAll();
                        _multiList.addItems(list);
                        _multiList.setNoDataVisibility(_multiList.getCountAll() < 1);
                    }

                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (resultCode == 1) {
                                finish();
                            }
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                int bonus_num = JSONUtil.getInteger(data, "bonus_num");


                int imgResourceID = -1;
                switch (bonus_num) {
                    case 1:
                        imgResourceID = R.drawable.ic_draw_rank_1;
                        break;
                    case 2:
                        imgResourceID = R.drawable.ic_draw_rank_2;
                        break;
                    case 3:
                        imgResourceID = R.drawable.ic_draw_rank_3;
                        break;
                    case 4:
                        imgResourceID = R.drawable.ic_draw_rank_4;
                        break;
                    case 5:
                        imgResourceID = R.drawable.ic_draw_rank_5;
                        break;
                }

                if (imgResourceID > -1) {
                    AlertAction alert = new AlertAction();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    String title = "추억의 뽑기";
                    String msg = resultMessage;
                    alert.showAlertAction(getContext(), title, msg, imgResourceID);
                } else {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GMultiListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

        final ArrayList<JSONObject> list = adapter.getItem(position);

        LogUtil.d("list.size() : " + list.size());

        JSONObject info01 = list.size() > 0 ? list.get(0) : null;
        JSONObject info02 = list.size() > 1 ? list.get(1) : null;
        JSONObject info03 = list.size() > 2 ? list.get(2) : null;
        JSONObject info04 = list.size() > 3 ? list.get(3) : null;
        JSONObject info05 = list.size() > 4 ? list.get(4) : null;

        ImageButton btnPP01 = convertView.findViewById(R.id.btnPP01);
        ImageButton btnPP02 = convertView.findViewById(R.id.btnPP02);
        ImageButton btnPP03 = convertView.findViewById(R.id.btnPP03);
        ImageButton btnPP04 = convertView.findViewById(R.id.btnPP04);
        ImageButton btnPP05 = convertView.findViewById(R.id.btnPP05);

        setViewBinding(btnPP01, info01);
        setViewBinding(btnPP02, info02);
        setViewBinding(btnPP03, info03);
        setViewBinding(btnPP04, info04);
        setViewBinding(btnPP05, info05);

        return convertView;
    }


    private void setViewBinding(ImageButton img, JSONObject info) {

        if (info == null) {
            img.setVisibility(View.INVISIBLE);
            return;
        }

        final int no = JSONUtil.getInteger(info, "no");
        final String use_yn = JSONUtil.getString(info, "use_yn");

        img.setSelected("Y".equals(use_yn));
        img.setVisibility(View.VISIBLE);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!"N".equals(use_yn)) {
                    new Alert().showAlert(getContext(), "다른 분이 이미 뽑았습니다.\n\n다른 뽑기를 선택해주세요.");
                    return;
                }

                _ppobkki_no = no;

                if (_isViewAd) {
                    callApi_ppobkki_play(no);
                    return;
                }


            }
        });
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }
    }
}
