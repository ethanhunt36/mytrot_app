package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.code404.mytrot.R;
import com.code404.mytrot.adapter.MyFragmentPagerAdapter;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.star.AtvStarInfo;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.frg.point.FrgPoint;
import com.code404.mytrot.frg.puzzle.FrgPuzzleRank;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvPuzzleRank extends AtvBase {


    private int _selectedIndex = -1;

    private MyFragmentPagerAdapter _adapter = null;
    private com.code404.mytrot.view.SlideTabView _slideTab = null;
    private androidx.viewpager.widget.ViewPager _pager = null;


    @Override
    protected void setView() {
        setView(R.layout.atv_puzzle_rank);
    }


    @Override
    protected void init() {
        _txtTopTitle.setText("퍼즐 게임 순위");
        _baseTopBottom.setVisibility(View.GONE);

        _adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());

        _slideTab.setTabViewCount(2);
        _slideTab.addTab("전체 순위");
        _slideTab.addTab("내 기록");
        _slideTab.setTabOn(0);

        FrgPuzzleRank ftg02 = new FrgPuzzleRank();
        FrgPuzzleRank ftg01 = new FrgPuzzleRank();

        ftg02.setType("RANK");
        ftg01.setType("MY");

        _adapter.addItem(ftg02);
        _adapter.addItem(ftg01);

        _pager.setAdapter(_adapter);
        _pager.setCurrentItem(0);
    }

    private void setPageTab(int pos) {
        _selectedIndex = pos;

        _pager.setCurrentItem(pos);
        _slideTab.setTabOn(pos);
        FrgBase frg = (FrgBase) _adapter.getItem(pos);

        if (frg != null) frg.refresh();
        LogUtil.d("pos ::: " + pos);
    }

    @Override
    protected void findView() {
        _slideTab = (com.code404.mytrot.view.SlideTabView) findViewById(R.id.slideTab);
        _pager = (androidx.viewpager.widget.ViewPager) findViewById(R.id.pager);
    }


    @Override
    protected void configureListener() {

        _btnTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopSearch();
            }
        });

        _slideTab.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    // mSlider.setSlideEnable(false);

                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    // if(mPager.getCurrentItem() == 0)
                    // {
                    // mSlider.setSlideEnable(true);
                    // }
                }
                return false;
            }
        });

        _slideTab.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
            @Override
            public void onClick(View v, int pos, JSONObject json) {
                setPageTab(pos);
            }
        });

        _pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int index) {
                _selectedIndex = index;

                LogUtil.d("onPageSelected : " + index);
                _slideTab.setTabOn(index);

                FrgBase frg = (FrgBase) _adapter.getItem(index);

                //if (frg != null) frg.refresh();
            }


            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // LogUtil.d("onPageScrolled : " + arg0);
            }


            @Override
            public void onPageScrollStateChanged(int index) {
                // LogUtil.d("onPageScrollStateChanged : " + index);
            }
        });
    }


}
