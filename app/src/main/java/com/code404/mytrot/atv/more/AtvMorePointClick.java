package com.code404.mytrot.atv.more;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SquareImageView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


public class AtvMorePointClick extends AtvBase implements GListView.IMakeView {

    private final static int _REQ_AUTO = 9988;
    private final static int _REQ_AUTO_CONTINUE = 8888;

    private GListView _list = null;
    private View _baseNoData = null;
    private View _header = null;


    private TextView _txtGubun = null;
    private TextView _txtSeq = null;
    private TextView _txtKeyword = null;
    private EditText _edtTitle = null;
    private Button _btnSearch = null;


    private String[] _adTypeArr = new String[]{"", "CPA", "CPC", "CPE", "CPS", "CPSNS"};
    private String[] _adTypeTextArr = new String[]{"전체", "CPA-특정 액션이 발생했을 시", "CPC-클릭만 해도", "CPE-앱 최초 접속 시", "CPS-상품 구매 시", "CPSNS-적립 방법 참여 완료 시"};


    private String[] _seqArr = new String[]{"", "point"};
    private String[] _seqTextArr = new String[]{"추천순", "포인트순"};


    private String[] _keywordArr = new String[]{"클릭", "참여", "맞추기"};

    private String _ad_type = "";
    private String _seq = "";
    //private String _ad_key = "";

    private String _auto_cpa = "";

    @Override
    protected void setView() {
        setView(R.layout.atv_alrim);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_pointclick, null);
        _list.addHeaderView(_header);


        _txtGubun = (TextView) _header.findViewById(R.id.txtGubun);
        _txtKeyword = (TextView) _header.findViewById(R.id.txtKeyword);
        _txtSeq = (TextView) _header.findViewById(R.id.txtSeq);

        _edtTitle = (EditText) _header.findViewById(R.id.edtTitle);
        _btnSearch = (Button) _header.findViewById(R.id.btnSearch);

    }

    @Override
    protected void init() {
        _txtTopTitle.setText("오퍼월 - 포인트클릭");

        _baseTopBottom.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_point_click, this);
        _list.setNoData(_baseNoData);

        setDataBinding();

        callApi_pointClick_get_list(true);
    }


    @Override
    protected void configureListener() {
        _txtTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvMorePoint.class);
                i.putExtra(Constants.EXTRAS_TYPE, "get");
                startActivity(i);
            }
        });

        _btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_pointClick_get_list(true);
            }
        });

        _txtGubun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {

                        if (which < 0) return;

                        _ad_type = _adTypeArr[which];

                        String ad_text = FormatUtil.isNullorEmpty(_ad_type) ? "전체" : _ad_type;

                        _txtGubun.setText(ad_text);

                        _txtKeyword.setText("키워드");
                        _edtTitle.setText("");

                        callApi_pointClick_get_list(true);
                    }
                });
                alert.showSeletItem(getContext(), _adTypeTextArr);
            }
        });


        _txtKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {

                        if (which < 0) return;

                        String keyword = _keywordArr[which];

                        _txtKeyword.setText(keyword);
                        _edtTitle.setText(keyword);

                        if (keyword.equals("맞추기")) {
                            _ad_type = "";
                            String ad_text = FormatUtil.isNullorEmpty(_ad_type) ? "전체" : _ad_type;
                            _txtGubun.setText(ad_text);
                        }

                        callApi_pointClick_get_list(true);
                    }
                });
                alert.showSeletItem(getContext(), _keywordArr);
            }
        });


        _txtSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {

                        if (which < 0) return;

                        _seq = _seqArr[which];
                        _txtSeq.setText(_seqTextArr[which]);

                        callApi_pointClick_get_list(true);
                    }
                });
                alert.showSeletItem(getContext(), _seqTextArr);
            }
        });

        _edtTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    callApi_pointClick_get_list(true);
                }
                return false;
            }
        });
    }


    private void setDataBinding() {
        JSONObject userJson = SPUtil.getInstance().getUserInfo(getContext());

        double point = 0;

        if (userJson != null) {
            point = JSONUtil.getDouble(userJson, "point", 0);
        }

        _txtTopRight.setText(FormatUtil.toPriceFormat(point) + "P");
        _txtTopRight.setVisibility(View.VISIBLE);
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

        JSONObject json = adapter.getItem(position);

        SquareImageView img = (com.code404.mytrot.view.SquareImageView) convertView.findViewById(R.id.img);
        TextView txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
        TextView txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        final String ad_key = JSONUtil.getString(json, "ad_key");
        String pic1 = JSONUtil.getStringUrl(json, "creative_list_img");
        String ad_type = JSONUtil.getString(json, "ad_type");
        String ad_name = JSONUtil.getString(json, "ad_name");
        String ad_type_sub_title = JSONUtil.getString(json, "ad_type_sub_title");

        int counts = JSONUtil.getInteger(json, "counts", -1);
        final double ad_profit = JSONUtil.getDouble(json, "ad_profit");

        LogUtil.d("ad_profit : " + ad_profit);

        if (!FormatUtil.isNullorEmpty(pic1)) {
            Picasso.with(getContext())
                    .load(pic1)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(img);
        }

        txtMessage.setText(ad_name);

        if (counts < 1)
            txtMessage.setTextColor(getResources().getColor(R.color.purpley));
        else
            txtMessage.setTextColor(getResources().getColor(R.color.black));

        txtDesc.setText(String.format("%s - %s", ad_type, ad_type_sub_title));

        txtPoint.setText(String.format("%sP", FormatUtil.toPriceFormat(ad_profit)));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if ("Y".equals(_auto_cpa) && "CPA".equals(ad_type) && (ad_name.contains("6자리") || ad_name.contains("4자리"))) {
//                    String msg = "본 캠페인은 자동으로 수행할 수 있습니다.\n\n성공 보수 1.5P를 지급하시고\n자동으로 수행 하시겠습니까?";
//                    Alert alert = new Alert();
//                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                        @Override
//                        public void onClose(DialogInterface dialog, int which) {
//                            if (which == Alert.BUTTON1) {
//                                Alert.toastLong(getContext(), "캠페인을 탐색합니다. 본화면으로 되돌아올때 까지 기다려주세요.");
//                                callApi_pointClick_check(ad_key, ad_name, ad_profit, true);
//                            } else {
//                                callApi_pointClick_check(ad_key, ad_name, ad_profit, false);
//                            }
//                        }
//                    });
//                    alert.showAlert(getContext(), msg, true, "네,자동수행해주세요.", "아니오,직접하겠습니다.");
//                    return;
//                }

                callApi_pointClick_check(ad_key, ad_name, ad_profit, false);
            }
        });


//        if (SPUtil.getInstance().getAutoViewNewspickYn(getContext()).equals("Y")) {
//            convertView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//                    alertAutoAdView(ad_key, ad_name, ad_profit, true);
//                    return false;
//                }
//            });
//        }


        return convertView;
    }

//    private void alertAutoAdView(String ad_key, String ad_name, double ad_profit, boolean isContinue) {
//        if (SPUtil.getInstance().getAutoViewNewspickYn(getContext()).equals("Y")) {
//            Alert alert = new Alert();
//            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                @Override
//                public void onClose(DialogInterface dialog, int which) {
//                    if (which == Alert.BUTTON1) {
//                        callApi_pointClick_check(ad_key, ad_name, ad_profit, true, isContinue);
//                    }
//                }
//            });
//            alert.showAlert(getContext(), "자동으로 CPA를 구독하시겠습니까?", false, "확인", "취소");
//        }
//    }


    private void callApi_pointClick_get_list(final boolean isClear) {

        LogUtil.e("==========callApi_pointClick_get_list : start==========");

        String keyword = _edtTitle.getText().toString();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.pointClick_get_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , SPUtil.getInstance().getAndroidID(getContext())
                , keyword
                , _ad_type
                , _seq
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray ad_list = JSONUtil.getJSONArray(data, "ad_list");

                _auto_cpa = JSONUtil.getString(data, "auto_cpa");
                if (isClear) {
                    _list.removeAll();
                }

                _list.addItems(ad_list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_pointClick_check(String ad_key, String ad_name, double ad_profit, final boolean isAuto) {
        callApi_pointClick_check(ad_key, ad_name, ad_profit, isAuto, false);
    }

    private void callApi_pointClick_check(String ad_key, String ad_name, double ad_profit, final boolean isAuto, boolean isContinue) {

        LogUtil.e("==========callApi_pointClick_check : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.pointClick_check(
                ad_key
                , SPUtil.getInstance().getUserNoEnc(getContext())
                , ad_profit
                , SPUtil.getInstance().getAndroidID(getContext())
                , isAuto ? "Y" : "N"
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    callApi_member_get_by_no();
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                int result_code = JSONUtil.getInteger(data, "result_code");

                String result_message = JSONUtil.getString(data, "result_message");
                String landing_url = JSONUtil.getString(data, "landing_url");
                String auto_cpa = JSONUtil.getString(data, "auto_cpa", "");

                String ad_participation = JSONUtil.getString(data, "ad_participation", "");


                if (!FormatUtil.isNullorEmpty(auto_cpa)) _auto_cpa = auto_cpa;

                if (result_code != 200) {

                    if (isAuto && isContinue) {
                        Alert.toastLong(getContext(), result_message);
//                        autoAutoNext();
                        return;
                    }

                    new Alert().showAlert(getContext(), result_message);
                    callApi_member_get_by_no();
                    return;
                }

                //if (isAuto == false) {

                    // 적립방법 안내 문구가 있으면, 팝업 오픈
                    if (!FormatUtil.isNullorEmpty(ad_participation)) {

                        Alert alert = new Alert();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(landing_url));
                                    startActivity(intent);
                                }
                            }
                        });
                        alert.showAlert(getContext(), ad_participation, false, "확인", "취소");

                    } else {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(landing_url));
                        startActivity(intent);
                    }


//                } else {
//
//
//                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_get_by_no() {

        LogUtil.e("==========callApi_member_get_by_no : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_no(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                if (resultCode == 0) {
                    SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                    setDataBinding();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                // ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        callApi_member_get_by_no();
    }


//    private void removeBeforeAd() {
//
//        if (FormatUtil.isNullorEmpty(_ad_key)) return;
//
//        for (int i = 0; i < _list.getAdapter().getCount(); i++) {
//            JSONObject json = _list.getAdapter().getItem(i);
//
//            String ad_key = JSONUtil.getString(json, "ad_key");
//
//            if (_ad_key.equals(ad_key)) {
//                _list.remove(json);
//                break;
//            }
//        }
//
//        _list.getAdapter().notifyDataSetChanged();
//    }


    Handler _handler = null;
    Runnable _runnable = null;

//    private void autoAutoNext() {
//
//        removeBeforeAd();
//
//        _runnable = new Runnable() {
//            @Override
//            public void run() {
//
//                _ad_key = "";
//                String ad_name = "";
//                double ad_profit = 0;
//                for (int i = 0; i < _list.getAdapter().getCount(); i++) {
//                    JSONObject json = _list.getAdapter().getItem(i);
//
//                    ad_name = JSONUtil.getString(json, "ad_name");
//
//                    if (ad_name.contains("6자리") || ad_name.contains("4자리")) {
//                        _ad_key = JSONUtil.getString(json, "ad_key");
//                        ad_profit = JSONUtil.getDouble(json, "ad_profit");
//
//                        break;
//                    }
//                }
//
//                if (FormatUtil.isNullorEmpty(_ad_key)) {
//                    new Alert().showAlert(getContext(), "오토 가능한 상품이 없습니다.");
//                    return;
//                }
//
//
//                callApi_pointClick_check(_ad_key, ad_name, ad_profit, true, true);
//            }
//        };
//        _handler = new Handler();
//        _handler.postDelayed(_runnable, 2000 + CommonUtil.getRandom());
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);

//        removeBeforeAd();
//
//        if (_REQ_AUTO_CONTINUE == requestCode && resultCode == RESULT_OK) {
//            autoAutoNext();
//        }
    }


    @Override
    protected void onDestroy() {

        if (_handler != null && _runnable != null) {
            _handler.removeCallbacks(_runnable);
        }

        super.onDestroy();
    }
}
