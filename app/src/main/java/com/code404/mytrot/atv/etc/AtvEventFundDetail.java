package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.vod.AtvVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvEventFundDetail extends AtvBase implements GListView.IMakeView {

    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;

    private View _header = null;
    private ImageView _img01 = null;
    private TextView _txtDesc = null;
    private Button _btnYoutube = null;
    private Button _btnShare = null;

    int _last_no = -1;

    private String _url = "";
    private String _artist_name = "";
    private int _artist_no = -1;
    private int _no = -1;
    private String _title = "";

    private String _myNick = "";
    private String _myUserNoEnc = "";
    private String _youtube_url = "";

    @Override
    protected boolean getBundle() {

        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        _url = getIntent().getStringExtra(Constants.EXTRAS_URL);
        _title = getIntent().getStringExtra(Constants.EXTRAS_TITLE);

        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _no = getIntent().getIntExtra(Constants.EXTRAS_NO, -1);

        if (_artist_no < 0) {
            String temp = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NO);
            if (!FormatUtil.isNullorEmpty(temp)) _artist_no = Integer.parseInt(temp);
        }
        if (_no < 0) {
            String temp = getIntent().getStringExtra(Constants.EXTRAS_NO);
            if (!FormatUtil.isNullorEmpty(temp)) _no = Integer.parseInt(temp);
        }

        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_alrim);
    }

    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);


        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_donate_detail, null);
        _txtDesc = _header.findViewById(R.id.txtDesc);
        _img01 = _header.findViewById(R.id.img01);
        _btnYoutube = _header.findViewById(R.id.btnYoutube);
        _btnShare = _header.findViewById(R.id.btnShare);

        _list.addHeaderView(_header);
    }

    @Override
    protected void init() {

        //_txtTopTitle.setText(String.format("%s - %s", _artist_name, _title));
        _txtTopTitle.setText(_title);
        _txtDesc.setText("");
        _baseTopBottom.setVisibility(View.GONE);
        _btnYoutube.setVisibility(View.GONE);
        _btnShare.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_point_rank, this);
        _list.setNoData(_baseNoData);

        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
        _myNick = JSONUtil.getString(userInfo, "nick");
        _myUserNoEnc = SPUtil.getInstance().getUserNoEnc(getContext());

        _img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        _img01.setClipToOutline(true);

        if (!FormatUtil.isNullorEmpty(_url)) {
            Picasso.with(getContext())
                    .load(_url)
                    //.fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img01);
            _img01.setVisibility(View.VISIBLE);
        } else {
            _img01.setVisibility(View.GONE);
        }

        callApi_eventFund_get(true);
    }


    @Override
    protected void configureListener() {
        _btnYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Uri url = Uri.parse(_youtube_url);
//                Intent intentCall = new Intent(Intent.ACTION_VIEW, url);
//                startActivity(intentCall);

                String youtube_key = _youtube_url.replace("https://youtu.be/", "").replace("https://www.youtube.com/watch?v=", "");

                Intent i = new Intent();
                i.setClass(getContext(), AtvVod.class);
                i.putExtra(Constants.EXTRAS_TYPE, "MYTROT_YOUTUBE");
                i.putExtra(Constants.EXTRAS_KEYWORD, youtube_key);
                //i.putExtra(Constants.EXTRAS_VOD_NO, vod_no);
                startActivity(i);
            }
        });

        _btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String text = "마이트롯 - " + _title + "\n\n" + _youtube_url;

                try {
                    Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, text);
                    Intent chooser = Intent.createChooser(intent, "친구에게 공유하기");
                    startActivity(chooser);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callApi_eventFund_get(final boolean isClear) {

        LogUtil.e("==========callApi_eventFund_get : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.eventFund_get(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _no
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "participant_list");

                _youtube_url = JSONUtil.getStringUrl(data, "video_url");

                _btnYoutube.setVisibility(FormatUtil.isNullorEmpty(_youtube_url) ? View.GONE : View.VISIBLE);
                _btnShare.setVisibility(FormatUtil.isNullorEmpty(_youtube_url) ? View.GONE : View.VISIBLE);

                // 2024.09.22
                // 심사중에는 유튜브영상,공유하기 버튼 안보이게 처리합니다.
                if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                    _btnYoutube.setVisibility(View.GONE);
                    _btnShare.setVisibility(View.GONE);
                }

                if (isClear) {
                    _list.removeAll();
                }

                _txtDesc.setText(Html.fromHtml(String.format("총 <b>%d명</b>의 트별님들이 참여해주셨습니다.", list.length())));

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        String userNo = JSONUtil.getString(json, "member_no");
        int mem_no = JSONUtil.getInteger(json, "mem_no");
        String nick = JSONUtil.getString(json, "member_nick").trim();

        if (FormatUtil.isNullorEmpty(nick)) nick = "팬" + mem_no;

        if (userNo.equals(_myUserNoEnc)) {
            nick = "<font color='#ff9900'><b>" + nick + "</b></font>";
        }

        txtRank.setText(String.valueOf(position + 1));
        txtNick.setText(Html.fromHtml(nick));
        txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "heart")) + " 하트");


        return convertView;
    }

}
