package com.code404.mytrot.atv.more;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.code404.mytrot.MyTrotApp;
import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.etc.AtvQuiz;
import com.code404.mytrot.atv.event.AtvEventPP;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.atv.web.AtvWeb;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAction;
import com.code404.mytrot.util.AlertAdSpecial;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.widget.ItemMyVph;
import com.code404.mytrot.widget.ItemNativeAd;
import com.code404.mytrot.widget.ItemNewspic;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.JsonObject;
import com.igaworks.adpopcorn.Adpopcorn;
import com.igaworks.adpopcorn.AdpopcornExtension;
import com.igaworks.adpopcorn.interfaces.IAPRewardInfoCallbackListener;
import com.nasmedia.nstation.NStation;
import com.nasmedia.nstation.ui.activity.NStationActivity;
import com.squareup.picasso.Picasso;
import com.tnkfactory.ad.TnkOfferwall;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

//import kr.newspic.offerwall.NewspicOfferwall;
//import kr.newspic.offerwall.NewspicOfferwall;
import kr.newspic.offerwall.NewspicOfferwall;
import retrofit2.Call;


public class AtvMoreChargeB extends AtvBase {

    private static String TAG = "AtvMoreCharge";
    private final static int _REQ_PPOBKKI = 9921;
    private final static int _REQ_AUTO = 9888;

    private RelativeLayout _baseAd = null;
    private ImageView _img_ad = null;

    private TextView _txtAdDesc01 = null;
    private TextView _txtTicket = null;
    private TextView _txtAdDesc02 = null;
    private RelativeLayout _baseEventPP = null;
    private View _baseEventPPBody = null;
    private ImageView _img_pp = null;
    private TextView _txtRemainCountPP = null;
    private TextView _txtRemainTimePP = null;
    private RelativeLayout _basePromo = null;
    private TextView _txtHintPromo = null;
    private View _baseNewspic = null;
    private TextView _txtHotIssue = null;
    private LinearLayout _base_list = null;

    private TextView _txtHearHow = null;
    private TextView _txtPpobkkiTitle = null;
    private TextView _txtPpobkkiDesc = null;

    private View _base_heart_ppobki = null;
    private ImageView _imgHeartPer = null;
    private TextView _txtHeartPpomkiPer = null;
    private TextView _txtHeartDesc = null;
    private TextView _txtHeartGetUser = null;
    private TextView _txtNewspicDesc02 = null;

    private ItemMyVph _item_my_vph = null;
    private ItemNativeAd _itemNativeAd = null;
    private View _baseCharge2 = null;
    private View _baseCharge3 = null;
    private View _baseCharge4 = null;
    private View _baseCharge5 = null;

    private View _adViewBody = null;

    private TextView _txtOfferwall = null;
    private TextView _txtOfferwall3 = null;
    private TextView _txtOfferwall4 = null;
    private TextView _txtOfferwall5 = null;


    private TextView _txtOfferwallDesc = null;
    private TextView _txtOfferwallDesc3 = null;
    private TextView _txtOfferwallDesc4 = null;
    private TextView _txtOfferwallDesc5 = null;

    private View _baseQuiz = null;

    private View _baseChargeMode = null;
    private Button _btnChargeVote = null;
    private Button _btnChargeBalance = null;
    private Button _btnChargeHeart = null;
    private TextView _txtDescChargeMode = null;

    private int _amount_ticket_ad = 1;
    private double _point_amount = 1;
    private Constants.enum_ad_type _enum_ad_type = Constants.enum_ad_type.none;
    private long _adClickTime = -1;
    private Handler _handlerTimer;
    private int _timerInterval = 500;

    // 현재 보유한 투표권 개수
    private int _vote_cnt = 0;
    private int _heart_per = 0;

    private int _ppobkki_cnt = 0;
    private boolean _is_view_newspic = false;

    private CountDownTimer _timer = null;
    private CountDownTimer _timerAd = null;
    private CountDownTimer _timerHeartGetUser = null;

    private long _diff = 0;
    private long _countCallByPpobkki = 0;

    private String _can_time_cheat = "N";
    private Date _can_apply_dttm = null;
    private Animation _animFadeIn, _animFadeOut;

    private int _count_ad_info_alert = 0;


    private List<ItemNewspic> _itemNewspicList = new ArrayList<ItemNewspic>();

    private TnkOfferwall _tnkOfferwall = null;

    @Override
    protected boolean getBundle() {

        _amount_ticket_ad = getIntent().getIntExtra(Constants.EXTRAS_AD_TICKET, 2);
        _point_amount = getIntent().getDoubleExtra(Constants.EXTRAS_AD_POINT, 0.5);

        return true;
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_more_charge);
    }

    @Override
    protected void configureListener() {

        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // 퀴즈
        _baseQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvQuiz.class);
                startActivity(i);
            }
        });


        // 무료충전소 - 오퍼월
        _baseCharge2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvMorePointClick.class);
                i.putExtra(Constants.EXTRAS_AD_TICKET, _amount_ticket_ad);
                i.putExtra(Constants.EXTRAS_AD_POINT, _point_amount);
                startActivityForResult(i, 9999);

//                if (Constants.DEBUG == false && Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null && _adFullOrgManager.canShowSimpleAdCount() > 0) {
//                    _adFullOrgManager.showSimpleAd();
//                }
                return;
            }
        });

        // 무료충전소 - 오퍼월3 - TNK 오퍼월
        _baseCharge3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _tnkOfferwall.startOfferwallActivity(getContext());
            }
        });

        // 무료충전소 - 오퍼월4 - 애드팝콘 오퍼월
        _baseCharge4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Adpopcorn.openOfferwall(getContext());
            }
        });

        _baseCharge5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), NStationActivity.class));
            }
        });


        _adFullOrgManager.setOnAdFullManagerListener(new InterfaceSet.OnAdFullManagerListener() {
            @Override
            public void setActiveAdText(Constants.enum_ad adName, boolean isActive) {
                LogUtil.d("_txtAd, FrgVote setActiveAdText, adName : " + adName + ", isActive " + isActive);
                setActiveAdTextDisplay(adName, isActive);
            }

            @Override
            public void setVotePointSync(final JSONObject userInfo, int amount_ticket_ad, double point_amount) {

                LogUtil.d("_txtAd, FrgVote setVotePointSync, amount_ticket_ad : " + amount_ticket_ad + ", point_amount " + point_amount);
                LogUtil.d("_txtAd, FrgVote setVotePointSync, getContext : " + (getContext() == null ? "null" : "ok"));

                if (amount_ticket_ad > 0) {
                    _amount_ticket_ad = amount_ticket_ad;
                }
                if (point_amount > 0) {
                    _point_amount = point_amount;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setMyVoteCountPoint(userInfo);

                        LogUtil.d("_txtAd, FrgVote setVotePointSync, runOnUiThread getContext : " + (getContext() == null ? "null" : "ok"));

                        SPUtil.getInstance().setAdTicketAmount(getContext(), _amount_ticket_ad);
                        SPUtil.getInstance().setAdPointAmount(getContext(), Float.valueOf(String.valueOf(_point_amount)));
                    }
                });

                if (_handlerTimer != null) {
                    _handlerTimer.sendEmptyMessageDelayed(0, 0);
                }
            }
        });


//        if (Constants.IS_AUTO_VIEW_NEWSPIC && SPUtil.getInstance().getAutoViewNewspickYn(getContext()).equals("Y")) {
//            _baseAd.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//
//                    alertAutoNewspic();
//                    return false;
//                }
//            });
//        }


        // 광고 보기
        _baseAd.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                long gap = getAdGap();
                long adViewGap = SPUtil.getInstance().getClickAdViewGap(getContext());

                LogUtil.d("_adClickTime : " + _adClickTime);
                LogUtil.d("System.currentTimeMillis() : " + System.currentTimeMillis());
                LogUtil.d("gap : " + gap);

                LogUtil.d("SPUtil.getInstance().getAdViewLimitYn() : " + SPUtil.getInstance().getAdViewLimitYn(getContext()));
                LogUtil.d("SPUtil.getInstance().getAdFullScreen() : " + SPUtil.getInstance().getAdFullScreen(getContext()));
                LogUtil.d("_try_load_ad_count : " + _try_load_ad_count + ", _fail_load_ad_count : " + Constants.TRY_MAX_COUNT_ADMANAGER);

                if ((SPUtil.getInstance().getAdViewLimitYn(getContext()).equals("Y") || SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false)
                        && _adClickTime > 0 && gap < adViewGap) {
                    String txtGap = adViewGap >= 60 ? (adViewGap / 60 + "분") : (adViewGap + "초");
                    Alert.toastLong(getContext(), "광고는 " + txtGap + " 이내 연속으로 시청할 수 없습니다.");
                    return;
                }


                String date = SPUtil.getInstance().getAdInfoDate(getContext());
                String currentDate = FormatUtil.getCurrentDate();

                String msg = "광고를 보면 보상을 받을 수 있습니다. 시청하시겠습니까?";


                if (!currentDate.equals(date) && _count_ad_info_alert < 1) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (which == Alert.BUTTON1) {


                                showAd_reward();
                            }
                        }
                    });
                    alert.showAlert(getContext(), msg, true, "확인", "취소");
                    _count_ad_info_alert++;

                    if (_count_ad_info_alert >= 1) {
                        SPUtil.getInstance().setAdInfoDate(getContext(), currentDate);
                    }
                } else {
                    showAd_reward();
                }

                if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                    callApi_logClick_add_log("CLK_AD_" + SPUtil.getInstance().getUserNo(getContext()));
                }
            }
        });


        // 뽑기
        _baseEventPP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                    callApi_logClick_add_log("CLK_PP_" + SPUtil.getInstance().getUserNo(getContext()));
                }

                onGotoPPomkki();
            }
        });

        // 홍보 프로모션
        _basePromo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvMorePromo.class);
                startActivity(i);
            }
        });

        _baseNewspic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _is_view_newspic = true;

                NewspicOfferwall.setTestMode(false);
                HashMap parameters = new HashMap<String, String>();
                //parameters.put("userId", String.valueOf(SPUtil.getInstance().getUserNo(getContext())));
                parameters.put("userId", SPUtil.getInstance().getUserNoEnc(getContext()));
                parameters.put("deviceName", SPUtil.getInstance().getAndroidID(getContext()));
                NewspicOfferwall.setParameters(parameters);
                NewspicOfferwall.openOfferwall(getContext());
            }
        });


        // 하트 뽑기권 사용
        _base_heart_ppobki.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_heart_per < 100) {
                    //new Alert().showAlert(getContext(), "하트 뽑기권을 100% 충전 후에 사용할 수 있습니다.\n\n'광고'를 시청하거나\n'오늘의HOT이슈'를 읽으면 충전할 수 있습니다.");
                    new Alert().showAlert(getContext(), "하트 뽑기권을 100% 충전 후에 사용할 수 있습니다.");
                    return;
                }

                String msg = "하트 뽑기권을 사용하시겠습니까?";
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            callApi_heart_open_heart_ticket();
                        }
                    }
                });
                alert.showAlert(getContext(), msg, false, "확인", "취소");
            }
        });


        _txtHearHow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String msg = "하트를 모아서 '광고펀딩'에 참여할 수 있어요.\n\n광고펀딩으로 지하철 광고를 최애가수에게 선물할 수 있습니다.";
                msg += "\n\n광고펀딩 화면으로 이동하시겠습니까?";

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            Intent intent = new Intent();
                            intent.setClass(getContext(), AtvVote.class);
                            intent.putExtra(Constants.EXTRAS_FRG_INDEX, 2);
                            startActivity(intent);

//                            if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null && _adFullOrgManager.canShowSimpleAdCount() > 0) {
//                                _adFullOrgManager.showSimpleAd();
//                            }

                            finishAffinity();


                        }
                    }
                });
                alert.showAlert(getContext(), msg, false, "확인", "취소");
            }
        });


        _btnChargeVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDataBinding_ChargeMode(enum_charge_mode.vote_only);
            }
        });
        _btnChargeBalance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDataBinding_ChargeMode(enum_charge_mode.point_only);
            }
        });
        _btnChargeHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDataBinding_ChargeMode(enum_charge_mode.heart_only);
            }
        });
    }

    private enum enum_charge_mode {point_only, vote_only, heart_only}

    ;


    private void showAd_reward() {
        boolean isShowAd = false;

        if (_adFullOrgManager != null && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            int ad_try_count = _adFullOrgManager.get_ad_try_count_increase();
            int repeatPpomkkiAd = SPUtil.getInstance().getRepeatPpomkkiAd(getContext());

            LogUtil.d(TAG, String.format("ad_try_count : %d, repeatPpomkkiAd : %d", ad_try_count, repeatPpomkkiAd));

            if (ad_try_count % repeatPpomkkiAd == (repeatPpomkkiAd - 1)) {
                AlertAdSpecial alert = new AlertAdSpecial();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {

                        if (which == Alert.BUTTON4) {
                            _adFullOrgManager.callApi_newspic_get_newspic_info("READ_NEWSPIC_C");
                        }

                        if (dialog != null) dialog.dismiss();
                    }
                });
                alert.showAdSpecial(AtvMoreChargeB.this, getAdSizeInfo());
                return;
            }

            if (_adFullOrgManager.isCanViewNewspic() && ad_try_count % repeatPpomkkiAd == (repeatPpomkkiAd / 2)) {
                _adFullOrgManager.callApi_newspic_get_newspic_info("READ_NEWSPIC_D");
                return;
            }
        }

        if (_adFullOrgManager != null && _adFullOrgManager.canShowAdCount(true) > 0) {
            _adFullOrgManager.setActivity(AtvMoreChargeB.this);
            _adFullOrgManager.setContext(getContext());
            _adFullOrgManager.show(getAdSizeInfo(), true);
            isShowAd = true;
//        } else if (_try_load_ad_count < Constants.TRY_MAX_COUNT_ADMANAGER) {
//            loadAutoShow(enum_ad_for_type.charge);
//            isShowAd = true;
        } else if (_adFullOrgManager != null) {
            _adFullOrgManager.setActivity(AtvMoreChargeB.this);
            _adFullOrgManager.setContext(getContext());
            _adFullOrgManager.show(getAdSizeInfo(), true);
            isShowAd = true;
        }

        final boolean isShowAdFinal = isShowAd;
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                initAdLoad_direct();

                if (isShowAdFinal) {
                    _adClickTime = System.currentTimeMillis();
                    SPUtil.getInstance().setClickAdView(getContext(), _adClickTime);
                }
            }
        }, 500);
    }


    private void setDataBinding_ChargeMode(enum_charge_mode enumChargeMode) {

        _btnChargeVote.setSelected(false);
        _btnChargeBalance.setSelected(false);
        _btnChargeHeart.setSelected(false);

        _btnChargeVote.setText("투표권 집중");
        _btnChargeBalance.setText("포인트 집중");
        _btnChargeHeart.setText("하트 집중");

        String msg = "";
        if (enumChargeMode == enum_charge_mode.vote_only) {
            _btnChargeVote.setSelected(true);
            _btnChargeVote.setText(Html.fromHtml("<b>투표권 집중</b>"));
            //msg = "* 오직 투표권만 집중해서 적립! <font color='red'>+2장 더!!<f/ont>";
            msg = "* 오직 투표권만 집중 적립!";
        } else if (enumChargeMode == enum_charge_mode.point_only) {
            _btnChargeBalance.setSelected(true);
            _btnChargeBalance.setText(Html.fromHtml("<b>포인트 집중</b>"));
            msg = "* 오직 포인트에 집중 적립";
        } else if (enumChargeMode == enum_charge_mode.heart_only) {
            _btnChargeHeart.setSelected(true);
            _btnChargeHeart.setText(Html.fromHtml("<b>하트 집중</b>"));
            //msg = "* 오직 하트충전만 빠르게 적립! <font color='red'>+2% 더!!</font>";
            msg = "* 오직 하트충전만 집중 적립!";
        }

        _txtDescChargeMode.setText(Html.fromHtml(msg));

        SPUtil.getInstance().setChargeMode(getContext(), enumChargeMode.name());
    }


    @Override
    protected void findView() {
        _txtHotIssue = findViewById(R.id.txtHotIssue);
        _baseAd = (RelativeLayout) findViewById(R.id.baseAd);

        _img_ad = (ImageView) findViewById(R.id.img_ad);
        _txtAdDesc01 = (TextView) findViewById(R.id.txtAdDesc01);
        _txtTicket = (TextView) findViewById(R.id.txtTicket);
        _txtAdDesc02 = (TextView) findViewById(R.id.txtAdDesc02);
        _baseEventPP = (RelativeLayout) findViewById(R.id.baseEventPP);
        _baseEventPPBody = findViewById(R.id.baseEventPPBody);
        _img_pp = (ImageView) findViewById(R.id.img_pp);
        _txtRemainCountPP = (TextView) findViewById(R.id.txtRemainCountPP);
        _txtRemainTimePP = (TextView) findViewById(R.id.txtRemainTimePP);
        _basePromo = (RelativeLayout) findViewById(R.id.basePromo);
        _txtHintPromo = (TextView) findViewById(R.id.txtHintPromo);
        _baseNewspic = findViewById(R.id.baseNewspic);
        _base_list = findViewById(R.id.base_list);
        _imgHeartPer = findViewById(R.id.imgHeartPer);
        _txtHeartPpomkiPer = findViewById(R.id.txtHeartPpomkiPer);
        _base_heart_ppobki = findViewById(R.id.base_heart_ppobki);
        _txtHearHow = findViewById(R.id.txtHearHow);
        _txtHeartDesc = findViewById(R.id.txtHeartDesc);
        _txtHeartGetUser = findViewById(R.id.txtHeartGetUser);

        _item_my_vph = findViewById(R.id.item_my_vph);
        _itemNativeAd = findViewById(R.id.itemNativeAd);
        _adViewBody = findViewById(R.id.adViewBody);

        _txtPpobkkiTitle = findViewById(R.id.txtPpobkkiTitle);
        _txtPpobkkiDesc = findViewById(R.id.txtPpobkkiDesc);
        _txtNewspicDesc02 = findViewById(R.id.txtNewspicDesc02);

        _txtOfferwall = findViewById(R.id.txtOfferwall);
        _baseCharge2 = findViewById(R.id.baseCharge2);

        _txtOfferwall3 = findViewById(R.id.txtOfferwall3);
        _baseCharge3 = findViewById(R.id.baseCharge3);

        _txtOfferwall4 = findViewById(R.id.txtOfferwall4);
        _baseCharge4 = findViewById(R.id.baseCharge4);

        _baseCharge5 = findViewById(R.id.baseCharge5);
        _txtOfferwall5 = findViewById(R.id.txtOfferwall5);


        _txtOfferwallDesc = findViewById(R.id.txtOfferwallDesc);
        _txtOfferwallDesc3 = findViewById(R.id.txtOfferwallDesc3);
        _txtOfferwallDesc4 = findViewById(R.id.txtOfferwallDesc4);
        _txtOfferwallDesc5 = findViewById(R.id.txtOfferwallDesc5);

        _baseChargeMode = findViewById(R.id.baseChargeMode);
        _btnChargeBalance = findViewById(R.id.btnChargeBalance);
        _btnChargeVote = findViewById(R.id.btnChargeVote);
        _btnChargeHeart = findViewById(R.id.btnChargeHeart);
        _txtDescChargeMode = findViewById(R.id.txtDescChargeMode);

        _baseQuiz = findViewById(R.id.baseQuiz);
    }


    @Override
    protected void init() {

        _txtTopTitle.setText("무료 충전소");
        _baseTopBottom.setVisibility(View.GONE);
        _txtHearHow.setText(Html.fromHtml("<u><b>하트를 모아서 무엇을 할 수 있나요?</b></u>"));
        _txtHeartGetUser.setText("");

//        initNewAdFullOrgManager();

        _adClickTime = SPUtil.getInstance().getClickAdView(getContext());

        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
        int lv = JSONUtil.getInteger(userInfo, "lv", 0);

        setMyVoteCountPoint(userInfo);
        setActiveAdTextDisplay(Constants.enum_ad.none, true);


        _basePromo.setVisibility(View.GONE);

        JSONObject boardPromoInfo = SPUtil.getInstance().getBoardPromoInfo(getContext());
        int reward_vote = JSONUtil.getInteger(boardPromoInfo, "reward_vote", -1);
        int reward_point = JSONUtil.getInteger(boardPromoInfo, "reward_point", -1);

        if (reward_vote > 0 && reward_point > 0) {
            String txt = String.format("인증샷을 등록하시면<br>투표권 <font color='red'>%d장</font>, 포인트 <font color='red'>%dP</font>를 드려요", reward_vote, reward_point);
            _txtHintPromo.setText(Html.fromHtml(txt));
        } else if (reward_vote > 0 && reward_point == 0) {
            String txt = String.format("인증샷을 등록하시면<br>투표권 <font color='red'>%d장</font>을 드려요", reward_vote);
            _txtHintPromo.setText(Html.fromHtml(txt));
        } else {
            _basePromo.setVisibility(View.GONE);
            _txtHintPromo.setVisibility(View.GONE);
        }

        _txtHotIssue.setVisibility(View.GONE);
        _adViewBody.setVisibility(View.GONE);

        initAdLoad();

        //initAdSimpleLoad();
        initAdHouse();


        callApi_ppobkki_get_list();

        _animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        _animFadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);

        String desc = "적립된 <font color='red'>쩜</font>은 <font color='red'>포인트</font>로 교환할 수 있어요.";
        _txtNewspicDesc02.setText(Html.fromHtml(desc));

        _baseNewspic.setVisibility(View.GONE);


        // 포인트클릭
        if (SPUtil.getInstance().getIsViewBaseCharge2Yn(getContext()).equals("Y")) {
            _baseCharge2.setVisibility(View.VISIBLE);
        } else {
            _baseCharge2.setVisibility(View.GONE);
        }

        // 티앤케이팩토리
        if (SPUtil.getInstance().getIsViewBaseCharge3Yn(getContext()).equals("Y")) {
            _baseCharge3.setVisibility(View.VISIBLE);
        } else {
            _baseCharge3.setVisibility(View.GONE);
        }

        // 애드팝콘
        if (SPUtil.getInstance().getIsViewBaseCharge4Yn(getContext()).equals("Y")) {
            _baseCharge4.setVisibility(View.VISIBLE);
        } else {
            _baseCharge4.setVisibility(View.GONE);
        }


        // NSTATION
        if (SPUtil.getInstance().getIsViewBaseCharge5Yn(getContext()).equals("Y")) {
            _baseCharge5.setVisibility(View.VISIBLE);
            callApi_nstation_maxreward();
        } else {
            _baseCharge5.setVisibility(View.GONE);
        }


        _tnkOfferwall = new TnkOfferwall(getContext());

        // 유저 식별 값 설정
        _tnkOfferwall.setUserName(SPUtil.getInstance().getUserNoEnc(getContext()));

        // Analytics Report를 사용 할 경우 적용 (앱 시작시 동작하는 activity에서 호출하시기 바랍니다.)
        //TnkSession.applicationStarted(getContext());

        // COPPA 설정 (true - ON / false - OFF)
        _tnkOfferwall.setCOPPA(false);

        // tnk 오퍼월 최대적립가능 포인트
        _tnkOfferwall.getEarnPoint(point -> {
            runOnUiThread(() -> {
                if (point > 0) {
                    String msg = String.format("최대 <font color='red'>%sP</font>까지 적립 가능", FormatUtil.toPriceFormat(point));
                    _txtOfferwall3.setText(Html.fromHtml(msg));
                    _txtOfferwall3.setVisibility(View.VISIBLE);
                    _txtOfferwallDesc3.setVisibility(View.GONE);
                } else {
                    _txtOfferwall3.setVisibility(View.GONE);
                }
            });
            return null;
        });

        // 애드팝콘 오퍼월 최대적립가능 포인트
        AdpopcornExtension.getEarnableTotalRewardInfo(this, new IAPRewardInfoCallbackListener() {

            @Override
            public void OnEarnableTotalRewardInfo(boolean queryResult, int totalCount, String totalReward) {
                // queryResult : 조회결과, totalCount : 참여 가능한 캠페인 수
                // totalReward : 적립 가능한 총 리워드

                if (queryResult) {
                    String msg = String.format("최대 <font color='red'>%sP</font>까지 적립 가능", totalReward);
                    _txtOfferwall4.setText(Html.fromHtml(msg));
                    _txtOfferwall4.setVisibility(View.VISIBLE);
                    _txtOfferwallDesc4.setVisibility(View.GONE);
                } else {
                    _txtOfferwall4.setVisibility(View.GONE);
                }
            }
        });

        String charge = SPUtil.getInstance().getChargeMode(getContext());


        // 심사중일때는
        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            //_baseCharge2.setVisibility(View.GONE);
            _basePromo.setVisibility(View.GONE);

            //- 기본적립으로 고정
            charge = "balance";
            _baseChargeMode.setVisibility(View.GONE);

//            // 광고버튼 안보이게
//            findViewById(R.id.baseAdEdge).setVisibility(View.GONE);
//
//            // 뽑기 안보이게
//            _baseEventPPBody.setVisibility(View.GONE);

            _base_heart_ppobki.setVisibility(View.GONE);
            _txtHearHow.setVisibility(View.GONE);
            _baseCharge5.setVisibility(View.GONE);

            _txtPpobkkiDesc.setText("1~5등까지 투표권,포인트를 받을 수 있습니다.");

            findViewById(R.id.base_offerwall).setVisibility(View.GONE);
        }


        if (charge.equals(enum_charge_mode.point_only.name()))
            setDataBinding_ChargeMode(enum_charge_mode.point_only);
        else if (charge.equals(enum_charge_mode.vote_only.name()))
            setDataBinding_ChargeMode(enum_charge_mode.vote_only);
        else if (charge.equals(enum_charge_mode.heart_only.name()))
            setDataBinding_ChargeMode(enum_charge_mode.heart_only);


    }


    private void initAdHouse() {
        if (Constants.DISPLAY_AD
                && Constants.DISPLAY_AD_HOUSE
                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
        ) {


            AdView adView = findViewById(R.id.adView);
            View baseAdLine = findViewById(R.id.baseAdLine);
            WebView webView = findViewById(R.id.webView);
            ImageView imgAd = findViewById(R.id.imgAd);
            View baseAdHouse = findViewById(R.id.baseAdHouse);

            baseAdLine.setVisibility(View.GONE);

            JSONArray list = SPUtil.getInstance().getAdBannerList(getContext());
            JSONObject jsonAd = JSONUtil.getJSONObject(list, 0);

            String img_url = JSONUtil.getStringUrl(jsonAd, "img_url");

            if (!FormatUtil.isNullorEmpty(img_url)) {
                final String click_url = JSONUtil.getStringUrl(jsonAd, "click_url");
                final String no = JSONUtil.getString(jsonAd, "no");

                _adViewBody.setVisibility(View.VISIBLE);
                Picasso.with(getContext())
                        .load(img_url)
                        //.fit()
                        .placeholder(R.drawable.img_noimg)
                        .into(imgAd);

                baseAdHouse.setVisibility(View.VISIBLE);

                adView.setVisibility(View.GONE);
                webView.setVisibility(View.GONE);

                imgAd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callApi_logClick_add_log(Constants.LOG_CLICK_MORE_AD_ROW + no);

                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(click_url));
                        startActivity(i);
                    }
                });
            }
        } else {
            _adViewBody.setVisibility(View.GONE);
        }


        // v8.0.0 이상
        Adpopcorn.setUserId(getContext(), SPUtil.getInstance().getUserNoEnc(this));


        try {
            //mkey,mckeyOfferwall,mckeyCarousel : Nstation에서 발급, userid : 참여 유저 식별값
            NStation.init(getContext(), Constants.NSTATION_M_KEY, Constants.NSTATION_MC_KEY, Constants.NSTATION_MC_KEY, SPUtil.getInstance().getUserNoEnc(this));
            NStation.setListType(NStation.TYPE_LIST_NATIVE);
            NStation.setBenefitsType(NStation.TYPE_BENEFITS_DISPLAY_LIST_TOP);
            NStation.setPrivacyPopup(true);
            NStation.setSystemRadioDialog(false);
            NStation.setFooterVisibility(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initAdLoad() {

//        // 오늘의핫이슈를 롱클릭으로 자동구독이 가능하다면, 광고 로딩은 하지 않습니다. Constants.DEBUG == false &&
//        if (Constants.DEBUG == true && SPUtil.getInstance().getAutoViewNewspickYn(getContext()).equals("Y"))
//            return;


        if (MyTrotApp.isInit == false) {
            new Thread(
                    () -> {
                        MobileAds.initialize(this, new OnInitializationCompleteListener() {
                            @Override
                            public void onInitializationComplete(InitializationStatus initializationStatus) {

                            }
                        });

                        MyTrotApp.isInit = true;
                    })
                    .start();
        }


        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            int count = _adFullOrgManager.canShowAdCount(true);
            if (count < 2) {
                _adFullOrgManager.setActivity(AtvMoreChargeB.this);
                _adFullOrgManager.setContext(getContext());
                _adFullOrgManager.initAd(Constants.enum_ad.none, _isFirstInit);
                _isFirstInit = false;
                _try_load_ad_count = 0;
            }
            return;
        }


        LogUtil.d("_txtAd, initAdLoad, _isStopAdLoading : " + _isStopAdLoading);
        LogUtil.d("_txtAd, initAdLoad, _try_load_ad_count : " + _try_load_ad_count);
        LogUtil.d("_txtAd, initAdLoad, _fail_load_ad_count : " + Constants.TRY_MAX_COUNT_ADMANAGER);
        LogUtil.d("_txtAd, initAdLoad, _adManager_success_count : " + _adManager_success_count);

        if (_isStopAdLoading) return;
//        if (_try_load_ad_count < Constants.TRY_MAX_COUNT_ADMANAGER && _adManager_success_count < Constants.SUCCESS_COUNT_ADMANAGER)
//            return;

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) return;


        int count = _adFullOrgManager.canShowAdCount(true);

        LogUtil.d("_txtAd, initAdLoad, count : " + count);

        if (count < 2) {
            _adFullOrgManager.setActivity(AtvMoreChargeB.this);
            _adFullOrgManager.setContext(getContext());
            _adFullOrgManager.initAd(Constants.enum_ad.none, _isFirstInit);
            _isFirstInit = false;
            _try_load_ad_count = 0;
        }

    }


    private void initAdLoad_direct() {
        if (_isStopAdLoading) return;
        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) return;

        int count = _adFullOrgManager.canShowAdCount(true);
        if (count < 1) {
            _adFullOrgManager.setActivity(AtvMoreChargeB.this);
            _adFullOrgManager.setContext(getContext());
            _adFullOrgManager.initAd(Constants.enum_ad.none, _isFirstInit);
        }
    }

    private void initTimer() {

        if (_timer == null) {

            int adLoadingTimeInterval = SPUtil.getInstance().getAdLoadingTimeInterval(getContext());
            _timer = new CountDownTimer((1000L) * 60 * 60 * 4, 1000L * adLoadingTimeInterval) {
                @Override
                public void onTick(long millisUntilFinished) {
                    int count = -1;

                    if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null) {
                        count = _adFullOrgManager.canShowAdCount(true);
                    }

                    LogUtil.d("onTick, millisUntilFinished : " + millisUntilFinished + ", count : " + count);
                    initAdLoad();
                }

                @Override
                public void onFinish() {
                    LogUtil.d("onTick, onFinish");
                }
            };

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (_timer != null) {
                        _timer.start();
                    }
                }
            }, 1000L * adLoadingTimeInterval);
        }

        if (_timerAd == null) {

            _timerAd = new CountDownTimer((1000L) * 60 * 60 * 4, 1000L) {
                @Override
                public void onTick(long millisUntilFinished) {
                    setActiveAdTextDisplay(Constants.enum_ad.none, true);
                    updateRemainTime();
                }

                @Override
                public void onFinish() {
                    LogUtil.d("onTick, onFinish");
                }
            };

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (_timerAd != null) {
                        _timerAd.start();
                    }
                }
            }, 100);
        }


        if (_timerHeartGetUser == null) {

            _timerHeartGetUser = new CountDownTimer((1000L) * 60 * 60 * 4, 1000L * 10) {
                @Override
                public void onTick(long millisUntilFinished) {
                    setDataBinding_heart_get_user();
                }

                @Override
                public void onFinish() {
                    LogUtil.d("onTick, onFinish");
                }
            };

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (_timerHeartGetUser != null) {
                        _timerHeartGetUser.start();
                    }
                }
            }, 1000L * 10);
        }
    }


//    private void alertAutoNewspic() {
//
//
//        String android_id_a = SPUtil.getInstance().getAndroidID(getContext());
//
//        LogUtil.d("android_id[1] : " + android_id_a);
//
//        SPUtil.getInstance().refreshAndroidID(getContext());
//
//        String android_id_b = SPUtil.getInstance().getAndroidID(getContext());
//
//        LogUtil.d("android_id[2] : " + android_id_b);
//
//        Alert.toastLong(getContext(), android_id_a + "\n" + android_id_b);
//
//
//        if (SPUtil.getInstance().getAutoViewNewspickYn(getContext()).equals("Y")) {
//            Alert alert = new Alert();
//            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                @Override
//                public void onClose(DialogInterface dialog, int which) {
//                    if (which == Alert.BUTTON1) {
//
//                        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//
//                        Intent i = new Intent();
//                        i.setClass(getContext(), AtvWeb.class);
//                        i.putExtra(Constants.EXTRAS_TYPE, "READ_NEWSPIC_X");
//                        i.putExtra(Constants.EXTRAS_NO, -1);
//                        i.putExtra(Constants.EXTRAS_URL, "");
//                        startActivityForResult(i, _REQ_AUTO);
//                    }
//                }
//            });
//            alert.showAlert(getContext(), "오늘의핫이슈를 보시겠습니까?", false, "확인", "취소");
//        }
//    }


    private JSONArray _newspic_list = null;
    private JSONArray _heart_get_user_list = null;

    private int _index_heart_get_user = 0;

    private int _index_newspic_view = 0;
    private static final int COUNT_NEWSPIC_VIEW = 20;

    private void setDataBinding_heart_get_user(JSONArray heart_get_user_list) {

        _index_heart_get_user = 0;
        _heart_get_user_list = heart_get_user_list;

        setDataBinding_heart_get_user();
    }


    private void setDataBinding_heart_get_user() {

        if (_heart_get_user_list == null || _heart_get_user_list.length() < 1) {
            _txtHeartGetUser.setVisibility(View.GONE);
            return;
        }
        if (_index_heart_get_user >= _heart_get_user_list.length()) _index_heart_get_user = 0;

        JSONObject json = JSONUtil.getJSONObject(_heart_get_user_list, _index_heart_get_user);

        if (json != null) {
            String nick = JSONUtil.getString(json, "nick");
//            String time_diff = JSONUtil.getString(json, "time_diff");
            String heart_amount = JSONUtil.getString(json, "heart_amount");

            String reg_dttm = JSONUtil.getString(json, "reg_dttm");
            String time_diff = FormatUtil.getHeartDateFormat(reg_dttm);

            String msg = String.format("<b>%s</b>, '%s'님이 하트 %s개 당첨되셨습니다.", time_diff, nick, heart_amount);

            if (!FormatUtil.isNullorEmpty(_txtHeartGetUser.getText().toString())) {
                _txtHeartGetUser.startAnimation(_animFadeOut);
                _txtHeartGetUser.startAnimation(_animFadeIn);
            }

            _txtHeartGetUser.setText(Html.fromHtml(msg));
            _txtHeartGetUser.setVisibility(View.VISIBLE);
            _index_heart_get_user++;
        }
    }


    private void setDataBinding_newspic(JSONArray newspic_list) {

        _newspic_list = newspic_list;
        _base_list.removeAllViews();
        _txtHotIssue.setVisibility(View.VISIBLE);

        if (newspic_list == null || newspic_list.length() < 1) {
//            ItemNoData itemNoData = new ItemNoData(getContext());
//            itemNoData.setText("매시각 정각에 새로운 뉴스가 채워집니다.\n다음 정각에 확인해주세요.");
//            _base_list.addView(itemNoData);
            _txtHotIssue.setVisibility(View.GONE);
            _base_list.setVisibility(View.GONE);
            return;
        }

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _txtHotIssue.setVisibility(View.GONE);
            _base_list.setVisibility(View.GONE);
            _baseNewspic.setVisibility(View.GONE);
            //_baseCharge2.setVisibility(View.GONE);

            return;
        }

        _itemNewspicList.clear();

        for (int i = 0; i < newspic_list.length(); i++) {
            JSONObject json = JSONUtil.getJSONObject(newspic_list, i);

            final String newspicUrl = JSONUtil.getStringUrl(json, "url");
            final int newspic_no = JSONUtil.getInteger(json, "no");

            final ItemNewspic item = new ItemNewspic(getContext());
            item.setData(json);
            item.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v, int pos) {

                    _index_newspic_view++;

//                    if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null
//                            && (_index_newspic_view % COUNT_NEWSPIC_VIEW == (COUNT_NEWSPIC_VIEW / 2))
//                            && _adFullOrgManager.canShowSimpleAdCount() > 0) {
//                        _adFullOrgManager.showSimpleAd();
//                    }


                    Intent i = new Intent();
                    i.setClass(getContext(), AtvWeb.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "READ_NEWSPIC_B");
                    i.putExtra(Constants.EXTRAS_NO, newspic_no);
                    i.putExtra(Constants.EXTRAS_URL, newspicUrl);

//                    if(Constants.DEBUG) {
//                        i.putExtra(Constants.EXTRAS_NO, 743434);
//                        i.putExtra(Constants.EXTRAS_URL, "http://ponong.co.kr/bbs/board.php?bo_table=free&wr_id=41422&decaffeine_link=170687939854");
//                    }

                    startActivity(i);

                    _newspicViewCount++;

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            item.setVisibility(View.GONE);
                        }
                    }, 500);
                }
            });

            _itemNewspicList.add(item);
            _base_list.addView(item);
        }
    }


    private long getAdGap() {
        long gap = _adClickTime < 1 ? 0 : (System.currentTimeMillis() - _adClickTime) / 1000;
        return gap;
    }

    private void setActiveAdTextDisplay(Constants.enum_ad adName, boolean isActive) {

        String txt = "";

        if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager == null) {
            LogUtil.d("setActiveAdTextDisplay, _adFullOrgManager is null");
            return;
        }


        int adCount = _adFullOrgManager.canShowAdCount(true);

//        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
//                && _try_load_ad_count < Constants.TRY_MAX_COUNT_ADMANAGER && _adManager_success_count < Constants.SUCCESS_COUNT_ADMANAGER) {
//            adCount++;
//        }

        txt = FormatUtil.getAdDesc(getContext(), _amount_ticket_ad, _point_amount, isActive, adCount);

        long gap = getAdGap();
        long adViewGap = SPUtil.getInstance().getClickAdViewGap(getContext());

        String msg = "동영상 광고";

        if (SPUtil.getInstance().getAdViewLimitYn(getContext()).equals("Y") || SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {

            if (Constants.DEBUG) msg += "(" + adCount + ")";
            if (gap > 0) {
                msg += (gap < adViewGap ? (", " + (adViewGap - gap) + "초 전") : "");
            }
        } else {
            if (Constants.DEBUG) msg += "(" + adCount + ")";
        }

        if (_txtAdDesc01 != null) {
            try {
                _txtAdDesc01.setText(Html.fromHtml(msg));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (_txtAdDesc02 != null && !FormatUtil.isNullorEmpty(txt)) {
            try {
                _txtAdDesc02.setText(Html.fromHtml(txt));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getRemainGap() {

        int gap = 111111;
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long hour, minute, second, curTotalSecs, diff;
        String txt = "";

        try {

            if (_diff > 0) {
                hour = _diff / 3600;
                minute = (_diff / 60) % 60;
                second = _diff % 60;

                txt = String.format("%02d시간 %02d분 %02d초", hour, minute, second);
            } else {
                txt = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return txt;
    }

    private int get_ad_counts() {
        int all_ad_count = _adFullOrgManager.canShowAdCount(true);
        int reward_ad_count = _adFullOrgManager.canShowAdCount_Reward();

        LogUtil.d("_txtAd, all_ad_count : " + all_ad_count);
        LogUtil.d("_txtAd, reward_ad_count : " + reward_ad_count);

        reward_ad_count++;
        return all_ad_count + reward_ad_count;
    }

    private int get_ad_counts_back_pressed() {
        int all_ad_count = _adFullOrgManager.canShowAdCount(false);
        int reward_ad_count = _adFullOrgManager.canShowAdCount_Reward();

        LogUtil.d("_txtAd, all_ad_count : " + all_ad_count);
        LogUtil.d("_txtAd, reward_ad_count : " + reward_ad_count);
        reward_ad_count++;
        return all_ad_count + reward_ad_count;
    }

    private int get_ad_counts_alreay_loading() {
        int all_ad_count = _adFullOrgManager.canShowAdCount(false);
        int reward_ad_count = _adFullOrgManager.canShowAdCount_Reward();

        LogUtil.d("_txtAd, all_ad_count : " + all_ad_count);
        LogUtil.d("_txtAd, reward_ad_count : " + reward_ad_count);
        return all_ad_count + reward_ad_count;
    }

    private void onGotoPPomkki() {
        String remainTime = getRemainGap();

        if ("Y".equals(_can_time_cheat) && _diff > 0 && _diff < 14340 && get_ad_counts_back_pressed() > 0 && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            if (ad_view_time_cheat() == true) {
                return;
            }
        }

        if (!FormatUtil.isNullorEmpty(remainTime) && _ppobkki_cnt < 1) {
            new Alert().showAlert(getContext(), remainTime + " 후에\n뽑기를 선택할 수 있습니다.\n\n잠시 후에 이용해주세요.");
            return;
        }

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            Intent i = new Intent();
            i.setClass(getContext(), AtvEventPP.class);
            startActivityForResult(i, _REQ_PPOBKKI);
            return;
        }

        final int all_ad_count = _adFullOrgManager.canShowAdCount(true);
        final int reward_ad_count = _adFullOrgManager.canShowAdCount_Reward();

        LogUtil.d("_txtAd, all_ad_count : " + all_ad_count);
        LogUtil.d("_txtAd, reward_ad_count : " + reward_ad_count);

        String msg = "";

        if (!FormatUtil.isNullorEmpty(remainTime) && _ppobkki_cnt > 0) {
            msg += String.format("추억의뽑기권이 %s장이 있습니다.\n\n한장을 사용하시면, ", FormatUtil.toPriceFormat(_ppobkki_cnt));
        }

        msg += "광고를 보고 뽑기 화면으로 이동합니다.\n\n광고를 시청하시겠습니까?";

        Alert alert = new Alert();
        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
            @Override
            public void onClose(DialogInterface dialog, int which) {

                if (which == Alert.BUTTON1) {


                    if (all_ad_count > 0 || reward_ad_count > 0) {
                        _adFullOrgManager.setOnAdCompleteListener(null);
                        _adFullOrgManager.setOnAdCompleteListener(new InterfaceSet.OnAdCompleteListener() {
                            @Override
                            public void onAdLoadSuccess() {
                                LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadSuccess");
                            }

                            @Override
                            public void onAdLoadFail() {
                                LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadFail");
                            }

                            @Override
                            public void onAdClose() {
                                _adFullOrgManager.setOnAdCompleteListener(null);
                                Intent i = new Intent();
                                i.setClass(getContext(), AtvEventPP.class);
                                startActivityForResult(i, _REQ_PPOBKKI);
                            }
                        });

                        if (reward_ad_count > 0)
                            _adFullOrgManager.showAdOrderReward(false);
                        else
                            _adFullOrgManager.show(getAdSizeInfo(), false);
//                    } else if (_try_load_ad_count < Constants.TRY_MAX_COUNT_ADMANAGER) {
//                        loadAutoShow(enum_ad_for_type.ppopkki);
                    } else {
                        //Alert.toastLong(getContext(), getString(R.string.alert_ad_no_fill));
                        Alert.toastLong(getContext(), "수신된 광고가 없는 관계로,\n바로 뽑기 화면으로 이동합니다.");
                        _adFullOrgManager.setOnAdCompleteListener(null);
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvEventPP.class);
                        startActivityForResult(i, _REQ_PPOBKKI);

//                        AdViewUtil.goActionAfterAdView(AtvMoreChargeB.this, getContext(), true, false, new InterfaceSet.AdFullViewCompleteListener() {
//                            @Override
//                            public void onAfterAction() {
//                                Intent i = new Intent();
//                                i.setClass(getContext(), AtvEventPP.class);
//                                startActivityForResult(i, _REQ_PPOBKKI);
//                            }
//                        });
                    }

                }
            }
        });
        alert.showAlert(getContext(), msg, false, "확인", "취소");
    }


    @Override
    public void onPause() {
        super.onPause();

        LogUtil.d("AtvMore, onPause");
        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }
        if (_timerAd != null) {
            _timerAd.cancel();
            _timerAd = null;
        }
        if (_timerHeartGetUser != null) {
            _timerHeartGetUser.cancel();
            _timerHeartGetUser = null;
        }
    }

    private int _newspicViewCount = 0;

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.d("FrgVote, onResume");


        if (_handlerTimer == null) {
            _handlerTimer = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    LogUtil.d("handleMessage");
                    JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
                    setMyVoteCountPoint(userInfo);

                    _item_my_vph.setMyVoteCountPoint();

                    setActiveAdTextDisplay(Constants.enum_ad.none, true);
                    //_handlerTimer.sendEmptyMessageDelayed(0, _timerInterval);
                }
            };
        }
        if (_handlerTimer != null) {
            _handlerTimer.sendEmptyMessageDelayed(0, _timerInterval);
        }

        initTimer();

        if (_newspic_list != null) {
            try {
                if (_itemNewspicList != null && _itemNewspicList.size() > 0) {
                    int lastestNo = AtvWeb._newspic_no;
                    for (int i = 0; i < _itemNewspicList.size(); i++) {
                        if (_itemNewspicList.get(i).getNewsNo() == lastestNo && _itemNewspicList.get(i).getVisibility() == View.VISIBLE) {
                            _newspicViewCount++;
                            _itemNewspicList.get(i).setVisibility(View.GONE);
                            if (Constants.DEBUG) Alert.toastLong(getContext(), "기존 데이터 삭제");
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (_newspicViewCount > 0 && _newspicViewCount % _newspic_list.length() == 0) {
                _newspicViewCount = 0;
                callApi_ppobkki_get_list();
            }
        }

        if (_is_view_newspic) {
            _is_view_newspic = false;
            callApi_ppobkki_get_list();
        }

    }

    private void setMyVoteCountPoint(JSONObject userJson) {
        String vote_dttm_remain = "";
        double point = 0;
        double heart = 0;

        LogUtil.d("_txtAd, FrgVote setMyVoteCountPoint, userJson : " + (userJson == null ? "null" : userJson.toString()));

        if (userJson != null) {
            _vote_cnt = JSONUtil.getInteger(userJson, "vote_cnt", 0);
            _ppobkki_cnt = JSONUtil.getInteger(userJson, "ppobkki_cnt", 0);

            point = JSONUtil.getDouble(userJson, "point", 0);
            heart = JSONUtil.getDouble(userJson, "heart", 0);
            _heart_per = JSONUtil.getInteger(userJson, "heart_per", 0);
            vote_dttm_remain = JSONUtil.getString(userJson, "vote_dttm_remain", "");
        }

        if (_heart_per > 100) {
            _heart_per = 100;
        }

        if (_heart_per >= 100) {
            _txtHeartDesc.setText(Html.fromHtml("<font color='#ea5351'><b>하트뽑기권을 사용해 주세요.</b></font>"));
        } else {
            _txtHeartDesc.setText("하트뽑기권을 100% 충전해서\n하트를 뽑아주세요.");
        }

        String msg = String.format("투표권 : %s장 | 포인트 : %sP | 하트 : %s개", FormatUtil.toPriceFormat(_vote_cnt), FormatUtil.toPriceFormat(point), FormatUtil.toPriceFormat(heart));
        //_txtTicket.setText("투표권 : " + FormatUtil.toPriceFormat(_vote_cnt) + "장 | 포인트 : " + FormatUtil.toPriceFormat(point) + " P");
        _txtTicket.setText(msg);
        _txtTicket.setVisibility(View.GONE);

        _item_my_vph.setMyVoteCountPoint();

        _txtHeartPpomkiPer.setText(Html.fromHtml("<font color='red'><b>" + _heart_per + "%" + "</b></font>"));

        if (_heart_per <= 0) _imgHeartPer.setImageResource(R.drawable.ic_heart_0);
        else if (_heart_per < 10) _imgHeartPer.setImageResource(R.drawable.ic_heart_1);
        else if (_heart_per < 20) _imgHeartPer.setImageResource(R.drawable.ic_heart_2);
        else if (_heart_per < 30) _imgHeartPer.setImageResource(R.drawable.ic_heart_3);
        else if (_heart_per < 40) _imgHeartPer.setImageResource(R.drawable.ic_heart_4);
        else if (_heart_per < 50) _imgHeartPer.setImageResource(R.drawable.ic_heart_5);
        else if (_heart_per < 60) _imgHeartPer.setImageResource(R.drawable.ic_heart_6);
        else if (_heart_per < 70) _imgHeartPer.setImageResource(R.drawable.ic_heart_7);
        else if (_heart_per < 80) _imgHeartPer.setImageResource(R.drawable.ic_heart_8);
        else if (_heart_per < 98) _imgHeartPer.setImageResource(R.drawable.ic_heart_9);
        else if (_heart_per <= 100) _imgHeartPer.setImageResource(R.drawable.ic_heart_10);

        int left = 20 * 3;
        int top = 10 * 3;
        _base_heart_ppobki.setBackgroundResource(_heart_per < 100 ? R.drawable._s_btn_violet_line : R.drawable._s_btn_red_line);
        _base_heart_ppobki.setPadding(left, top, left, top);
    }


    private void callApi_vote_my_vote_ticket_info() {
        LogUtil.e("==========callApi_vote_my_vote_ticket_info : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_my_vote_ticket_info(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    JSONObject userJson = JSONUtil.getJSONObject(json, "data");

                    setMyVoteCountPoint(userJson);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_logClick_add_log(final String code) {

        LogUtil.e("==========callApi_logClick_add_log : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.logClick_add_log(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , code
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    /**
     * 남은 시간을 업데이트 해준다.
     */
    private void updateRemainTime() {

        LogUtil.d("AtvMoreCharge, updateRemainTime, _diff : " + _diff);
        LogUtil.d("AtvMoreCharge, updateRemainTime, _can_apply_dttm : " + (_can_apply_dttm == null ? "null" : _can_apply_dttm.toString()));

        long hour, minute, second;

        try {
            _diff = FormatUtil.getRemainSecond(_can_apply_dttm);

            String txt = "";
            if (_diff > 0) {
                hour = _diff / 3600;
                minute = (_diff / 60) % 60;
                second = _diff % 60;

                //txt = String.format("남은 시간 : <font color='red'>%02d:%02d:%02d</font>", hour, minute, second);
                txt = String.format("<font color='#d4364f'><b>%02d:%02d:%02d</b></font>", hour, minute, second);
            } else if (get_ad_counts() > 0) {
                //txt = "<font color='red'>지금 바로 뽑을 수 있어요!!!</font>";
                txt = "<font color='#d4364f'><b>00:00:00</b></font>";
            }

            if (!FormatUtil.isNullorEmpty(txt)) {
                _txtRemainTimePP.setText(Html.fromHtml(txt));
                _txtRemainTimePP.setVisibility(View.VISIBLE);
            } else {
                _txtRemainTimePP.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callApi_ppobkki_run_time_cheat() {

        LogUtil.e("==========callApi_ppobkki_run_time_cheat : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.ppobkki_run_time_cheat(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(final int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {

                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                    return;
                }

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        callApi_ppobkki_get_list();
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_ppobkki_get_list() {

        _countCallByPpobkki++;

        LogUtil.e("==========callApi_ppobkki_get_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.ppobkki_get_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "Y"
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);


                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                _can_time_cheat = JSONUtil.getString(data, "can_time_cheat", "N");
                _ppobkki_cnt = JSONUtil.getInteger(data, "ppobkki_cnt", 0);

                LogUtil.d("AtvMoreCharge, _can_time_cheat : " + _can_time_cheat);

                JSONArray list = JSONUtil.getJSONArray(data, "list");
                JSONArray newspic_list = JSONUtil.getJSONArray(data, "newspic_list");
                JSONArray heart_get_user_list = JSONUtil.getJSONArray(data, "heart_get_user_list");


                _diff = JSONUtil.getInteger(data, "diff", -1);

                String can_apply_dttm = JSONUtil.getString(data, "can_apply_dttm", "");

                String is_view_ad_chimaegame = JSONUtil.getString(data, "is_view_ad_chimaegame", "N");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                try {
                    _can_apply_dttm = dateFormat.parse(can_apply_dttm);
                } catch (ParseException pe) {
                    pe.printStackTrace();
                }

                updateRemainTime();

                if (_timer == null) {
                    initTimer();
                }


                if (list == null || list.length() < 1) {
                    _baseEventPP.setVisibility(View.GONE);
                    return;
                }

                if (SPUtil.getInstance().isAvailableBoard(getContext()) && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                    _baseEventPP.setVisibility(View.VISIBLE);
                }

                _txtPpobkkiTitle.setText("추억의 뽑기(4시간 마다 1번)");
                if (_ppobkki_cnt > 0) {
                    _txtPpobkkiTitle.setText(String.format("추억의 뽑기(추가보유 : %d장)", _ppobkki_cnt));
                }
                int can_count = 0;
                if (list != null && list.length() > 0) {
                    for (int i = 0; i < list.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(list, i);
                        String use_yn = JSONUtil.getString(j, "use_yn");
                        if ("N".equals(use_yn)) can_count++;
                    }

                    _txtRemainCountPP.setText(String.format("총 %s장, 지금 %s장 남았어요."
                            , FormatUtil.toPriceFormat(list.length()), FormatUtil.toPriceFormat(can_count)));
                    _txtRemainCountPP.setVisibility(View.VISIBLE);
                }

                if ("Y".equals(_can_time_cheat) && _diff > 0 && _diff < 14340 && get_ad_counts_back_pressed() > 0 && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                    ad_view_time_cheat();
                }

                setDataBinding_heart_get_user(heart_get_user_list);
                setDataBinding_newspic(newspic_list);

                if (_countCallByPpobkki % 3 == 0) {
                    _itemNativeAd.initAd();
                }

                LogUtil.d("is_view_ad_chimaegame : " + is_view_ad_chimaegame);

                JSONArray adBannerList = SPUtil.getInstance().getAdBannerList(getContext());

                if (adBannerList.length() > 0) {
                    _adViewBody.setVisibility(is_view_ad_chimaegame.equals("Y") && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") ? View.VISIBLE : View.GONE);
                } else {
                    _adViewBody.setVisibility(View.GONE);
                }


                String txt_newspic_desc = JSONUtil.getString(data, "txt_newspic_desc");
                String is_view_newspic_offerwall = JSONUtil.getString(data, "is_view_newspic_offerwall");

                _txtNewspicDesc02.setText(Html.fromHtml(txt_newspic_desc));

                if (Constants.APP_DEVICE_ID.equals("MYTROT")) {
                    _baseNewspic.setVisibility("Y".equals(is_view_newspic_offerwall) ? View.VISIBLE : View.GONE);

                    if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                        _baseNewspic.setVisibility(View.GONE);
                    }

                    //_baseNewspic.setVisibility(View.GONE);
                }

                int ad_count = JSONUtil.getInteger(data, "ad_count");
                int ad_profit = JSONUtil.getInteger(data, "ad_profit");

                String msg = String.format("미션 <font color='red'>%s개</font>, 최대 <font color='red'>%sP</font>까지 적립 가능합니다.", FormatUtil.toPriceFormat(ad_count), FormatUtil.toPriceFormat(ad_profit));

                msg = String.format("최대 <font color='red'>%sP</font>까지 적립 가능", FormatUtil.toPriceFormat(ad_profit));
                _txtOfferwall.setText(Html.fromHtml(msg));
                _txtOfferwall.setVisibility(View.VISIBLE);

                _txtOfferwallDesc.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_get_by_no() {

        LogUtil.e("==========callApi_member_get_by_no : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_no(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                if (resultCode == 0) {
                    SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                    setMyVoteCountPoint(userInfo);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                // ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_heart_open_heart_ticket() {

        LogUtil.e("==========callApi_heart_open_heart_ticket : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.heart_open_heart_ticket(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);

                    callApi_member_get_by_no();

                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                final JSONObject jsonUser = JSONUtil.getJSONObject(data, "member");

                int heart_amount = JSONUtil.getInteger(data, "heart_amount");

                SPUtil.getInstance().setUserInfo(getContext(), jsonUser);

//                Alert alert = new Alert();
//                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                    @Override
//                    public void onClose(DialogInterface dialog, int which) {
//                        setMyVoteCountPoint(jsonUser);
//                    }
//                });
//                alert.showAlert(getContext(), resultMessage);

                final AlertAction alert = new AlertAction();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        setMyVoteCountPoint(jsonUser);

                        if (heart_amount >= 7) {
                            callApi_ppobkki_get_list();
                        }
                    }
                });
                alert.showAlertActionHeart(getContext());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        alert.setHeartStart(heart_amount, resultMessage);
                    }
                });

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_nstation_maxreward() {

        LogUtil.e("==========callApi_heart_open_heart_ticket : start==========");

        APIInterface apiInterface = APIClient.getClient_NStation().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.nstation_maxreward(
                Constants.NSTATION_M_KEY
                , Constants.NSTATION_MC_KEY
                , SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                int result = JSONUtil.getInteger(json, "result");
                long maxreward = JSONUtil.getLong(json, "maxreward");

                if (result == 200) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String msg = String.format("최대 <font color='red'>%sP</font>까지 적립 가능", FormatUtil.toPriceFormat(maxreward));
                            _txtOfferwall5.setText(Html.fromHtml(msg));
                            _txtOfferwall5.setVisibility(View.VISIBLE);

                            _txtOfferwallDesc5.setVisibility(View.GONE);
                        }
                    });
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }
        if (_timerAd != null) {
            _timerAd.cancel();
            _timerAd = null;
        }
        if (_timerHeartGetUser != null) {
            _timerHeartGetUser.cancel();
            _timerHeartGetUser = null;
        }
    }


    // 더 추가로 자동 로딩하지 않겠다는 플래그
    private boolean _isStopAdLoading = false;
    private boolean _isViewQuitAlert = false;

    @Override
    public void onBackPressed() {
        int count = get_ad_counts_alreay_loading();

//        if (count > 0 && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") && _isViewQuitAlert == false) {
//            String msg = "시청할 수 있는 광고가 조금 더 있습니다.\n잔여 광고를 모두 보시고 종료하시겠습니까?";
//
//            msg += "\n\n'아니요. 바로 종료'를 누르면 바로 종료합니다.";
//            msg += "\n\n'확인'을 누르면, 더이상 광고를 로딩하지 않습니다. 잔여 광고를 모두 보시고 종료해주세요.";
//            msg += "\n\n잔여 광고를 보시고, 원하시면, 이전화면으로 이동했다가 다시 무료충전소를 입장해주세요.";
//
//            Alert alert = new Alert();
//            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                @Override
//                public void onClose(DialogInterface dialog, int which) {
//                    if (which == Alert.BUTTON1) {
//                        _isStopAdLoading = true;
//                    } else {
//                        AtvMoreChargeB.super.onBackPressed();
//                    }
//                }
//            });
//            alert.showAlert(getContext(), msg, false, "확인", "아니요. 바로 종료");
//
//            _isViewQuitAlert = true;
//            return;
//        }

        super.onBackPressed();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);

        if (_REQ_PPOBKKI == requestCode) {
            callApi_ppobkki_get_list();
        } else if (_REQ_AUTO == requestCode && resultCode == RESULT_OK) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

                    Intent i = new Intent();
                    i.setClass(getContext(), AtvWeb.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "READ_NEWSPIC_X");
                    i.putExtra(Constants.EXTRAS_NO, -1);
                    i.putExtra(Constants.EXTRAS_URL, "");
                    startActivityForResult(i, _REQ_AUTO);
                }
            }, 2000 + CommonUtil.getRandom());
        }
    }


    //    private AdManager _adManager = new AdManager();
    private Dialog _dialogAd = null;

    private int _adManager_success_count = 0;
    private int _try_load_ad_count = 0;


    public enum enum_ad_for_type {charge, ppopkki, ppopkki_cheat}

//    private void loadAutoShow(enum_ad_for_type adForType) {
//
//        LogUtil.d(TAG, "_txtAd, loadAutoShow, _try_load_ad_count[1] : " + _try_load_ad_count);
//
//        if (_try_load_ad_count >= Constants.TRY_MAX_COUNT_ADMANAGER) {
//            ProgressDialogUtil.dismiss(_dialogAd);
//
//            if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
//                _adFullOrgManager.callApi_newspic_get_newspic_info("READ_NEWSPIC_D");
//            } else {
//                new Alert().showAlert(getContext(), "광고를 로딩하는데 실패했습니다.\n잠시 후에 다시 시도해주세요." + (Constants.DEBUG ? _try_load_ad_count : ""));
//                _try_load_ad_count = 0;
//            }
//
//            return;
//        }
//
//
//        if (_dialogAd == null || _dialogAd.isShowing() == false) {
//            _dialogAd = ProgressDialogUtil.show(getContext(), null);
//        }
//
//        if (_adManager.showAd()) {
//            return;
//        }
//
//        _adManager.ready(AtvMoreChargeB.this, getContext(), new InterfaceSet.OnAd2CompleteListener() {
//            @Override
//            public void onAdLoadSuccess() {
//                LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadSuccess");
//
//                ProgressDialogUtil.dismiss(_dialogAd);
//                _adManager.showAd();
//                _try_load_ad_count = 0;
//                _adManager_success_count++;
//
//
//                // 광고시청시간을 저장
//                //_adClickTime = System.currentTimeMillis();
//                //SPUtil.getInstance().setClickAdView(getContext(), _adClickTime);
//
//            }
//
//            @Override
//            public void onAdLoadFail() {
//                LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadFail");
//                ProgressDialogUtil.dismiss(_dialogAd);
//
//                if (_adManager.showAd())
//                    return;
//                _try_load_ad_count++;
//                loadAutoShow(adForType);
//            }
//
//            @Override
//            public void onAdPlayComplete(Constants.enum_ad enumAd) {
//                LogUtil.d("_txtAd, AtvMoreCharge, onAdPlayComplete");
//                ProgressDialogUtil.dismiss(_dialogAd);
//
//                if (adForType == enum_ad_for_type.charge) {
//                    if (_adFullOrgManager != null) {
//                        _adFullOrgManager.callApi_vote_put_ticket_ad_point(enumAd.name());
//                    }
//                } else if (adForType == enum_ad_for_type.ppopkki) {
//                    Intent i = new Intent();
//                    i.setClass(getContext(), AtvEventPP.class);
//                    startActivityForResult(i, _REQ_PPOBKKI);
//                } else if (adForType == enum_ad_for_type.ppopkki_cheat) {
//                    callApi_ppobkki_run_time_cheat();
//                }
//
//                // AdFuulOrgManager 광고 로딩 처리
//                initAdLoad();
//            }
//        });
//
//
//        LogUtil.d("_txtAd, loadAutoShow, _try_load_ad_count[2] : " + _try_load_ad_count);
//    }


    private boolean ad_view_time_cheat() {

        if (_baseEventPP == null || _baseEventPP.getVisibility() == View.GONE) {
            return false;
        }

        final int all_ad_count = _adFullOrgManager.canShowAdCount(true);
        final int reward_ad_count = _adFullOrgManager.canShowAdCount_Reward();

        LogUtil.d("_txtAd, ad_view_time_cheat all_ad_count : " + all_ad_count);
        LogUtil.d("_txtAd, ad_view_time_cheat reward_ad_count : " + reward_ad_count);


        if (all_ad_count == 0 && reward_ad_count == 0) {
            return false;
        }

        Alert alert = new Alert();
        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {

            @Override
            public void onClose(DialogInterface dialog, int which) {

                if (which == Alert.BUTTON1) {

//                    AdViewUtil.goActionAfterAdView(AtvMoreChargeB.this, getContext(), true, false, new InterfaceSet.AdFullViewCompleteListener() {
//                        @Override
//                        public void onAfterAction() {
//                            callApi_ppobkki_run_time_cheat();
//                        }
//                    });

//
                    if (all_ad_count > 0 || reward_ad_count > 0) {
                        _adFullOrgManager.setOnAdCompleteListener(null);
                        _adFullOrgManager.setOnAdCompleteListener(new InterfaceSet.OnAdCompleteListener() {
                            @Override
                            public void onAdLoadSuccess() {
                                LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadSuccess 2");
                            }

                            @Override
                            public void onAdLoadFail() {
                                LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadFail 2");
                            }

                            @Override
                            public void onAdClose() {
                                _adFullOrgManager.setOnAdCompleteListener(null);
                                LogUtil.d("_txtAd, AtvMoreCharge, onAdClose 2");
                                callApi_ppobkki_run_time_cheat();
                            }
                        });

                        if (reward_ad_count > 0)
                            _adFullOrgManager.showAdOrderReward(false);
                        else
                            _adFullOrgManager.showAdOrder_Interstitial();
//                        } else if (_try_load_ad_count < _fail_load_ad_count) {
//                            loadAutoShow(enum_ad_for_type.ppopkki_cheat);
                    } else {
                        //Alert.toastLong(getContext(), getString(R.string.alert_ad_no_fill));
                        callApi_ppobkki_run_time_cheat();
                    }

                }

            }
        });
        alert.showAlert(getContext(), "광고를 시청하면\n2시간 앞당길 수 있습니다.\n\n광고를 시청하시겠습니까?", false, "확인", "취소");

        return true;
    }

}
