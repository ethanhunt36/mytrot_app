package com.code404.mytrot.atv.star_room;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.etc.AtvReport;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertReport;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.widget.ItemNativeAd;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvStarRoomComment extends AtvBase implements GListView.IMakeView {

    private View _baseReport = null;

    private EditText _edtReply = null;
    private Button _btnSend = null;
    private TextView _txtDownload = null;

    private AdView _adView = null;
    private ItemNativeAd _itemNativeAd = null;
    private View _baseBoardLine = null;

    private GListView _list = null;

    private JSONObject _json = null;

    // 작성한 회원 번호
    private String _star_member_no = "";

    // 열람 중인 회원 번호
    private String _enter_member_no = "";
    private int _category = -1;

    private JSONArray _jsonArrayComment = null;


    private String _artist_board_id = "";

    private int _comment_no = -1;
    private int _board_no = -1;


    @Override
    protected boolean getBundle() {
        String jsonStr = getIntent().getStringExtra(Constants.EXTRAS_JSON_STRING);

        _jsonArrayComment = JSONUtil.createArray(jsonStr);

        LogUtil.jsonArray(_jsonArrayComment);

        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);
        String board_no = getIntent().getStringExtra(Constants.EXTRAS_BOARD_NO);
        String comment_no = getIntent().getStringExtra(Constants.EXTRAS_COMMENT_NO);

        if (!FormatUtil.isNullorEmpty(board_no)) _board_no = Integer.parseInt(board_no);
        if (!FormatUtil.isNullorEmpty(comment_no)) _comment_no = Integer.parseInt(comment_no);


        return true;
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_star_info);
    }

    @Override
    protected void findView() {
        _list = findViewById(R.id.list);

        _edtReply = (EditText) findViewById(R.id.edtReply);
        _btnSend = (Button) findViewById(R.id.btnSend);

    }


    @Override
    protected void init() {
        _txtTopTitle.setText("대댓글 작성");

        _baseTopBottom.setVisibility(View.GONE);
        _btnTopBack.setVisibility(View.VISIBLE);

        _txtTopRight.setVisibility(View.GONE);

        _btnTopRight.setVisibility(View.VISIBLE);

        _list.setViewMaker(R.layout.row_comment, this);


        _enter_member_no = SPUtil.getInstance().getUserNoEnc(getContext());

        setDataBinding();
    }


    @Override
    protected void configureListener() {

        _btnTopBack.setOnClickListener(null);
        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishInfo();
            }
        });

        _btnTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onReport();
            }
        });

        _baseReportRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertReport alert = new AlertReport();
                alert.setOnStringListener(new InterfaceSet.OnStringListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which, String str) {
                        if (which < 0)
                            return;
                        callApi_accuse_ins_direct(str, AtvReport.ACCUSE_TYPE_BOARD, -1);
                    }
                });

                alert.showReport(getContext());
            }
        });


        _btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cont = _edtReply.getText().toString().trim();

                if (FormatUtil.isNullorEmpty(cont)) {
                    new Alert().showAlert(getContext(), "내용을 입력해 주세요.");
                    return;
                }

                callApi_board_cmt_ins(cont);
            }
        });
    }

    private void onReport() {
        Alert alert = new Alert();
        String[] items = new String[]{"게시글 신고하기", "작성자의 글 모두 차단하기", "취소"};

        if (_star_member_no.equals(SPUtil.getInstance().getUserNoEnc(getContext()))) {
            items = new String[]{"삭제하기", "취소"};
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    LogUtil.d("onClose, which : " + which);

                    if (which == 0) {
                        // 삭제
                        Alert alert1 = new Alert();
                        alert1.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    callApi_board_del();
                                }
                            }
                        });
                        alert1.showAlert(getContext(), "삭제 하시겠습니까?");
                    } else if (which == 1) {
//                                // 수정
//
//                                if (_category == 1 || _category == 2) {
//                                    Intent intent = new Intent();
//                                    intent.setClass(getContext(), AtvStarWrite.class);
//                                    intent.putExtra(Constants.EXTRAS_JSON_STRING, _json.toString());
//                                    startActivity(intent);
//                                }
                    }
                }
            });
        } else {
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    LogUtil.d("onClose, which : " + which);
                    if (which == 0) {
                        // 신고하기
                        Intent intent = new Intent();
                        intent.setClass(getContext(), AtvReport.class);
                        intent.putExtra(Constants.EXTRAS_MEMBER_NO, _star_member_no);
                        intent.putExtra(Constants.EXTRAS_BOARD_NO, _board_no);
                        intent.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
                        intent.putExtra(Constants.EXTRAS_ACCUSE_TYPE, AtvReport.ACCUSE_TYPE_BOARD);
                        startActivity(intent);
                    } else if (which == 1) {
                        // 작성자의 글 모두 차단하기
                        Alert alert1 = new Alert();
                        alert1.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    callApi_memberBlock_block_by_board();
                                }
                            }
                        });
                        alert1.showAlert(getContext(), "작성자의 글을 모두 차단 하시겠습니까?");
                    }
                }
            });
        }

        alert.showSeletItem(getContext(), items);
    }

    private void setDataBinding() {


        _baseReportRight.setVisibility(View.GONE);
        _btnTopRight.setVisibility(View.GONE);


        _list.addItems(_jsonArrayComment);
    }


    private void callApi_board_del() {

        LogUtil.e("==========callApi_board_del : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.boardStar_del(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _board_no,
                _artist_board_id
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                JSONUtil.puts(_json, "del_yn", "Y");

                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_memberBlock_block_by_board() {

        LogUtil.e("==========callApi_board_del : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberBlock_block_by_board(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _board_no,
                _artist_board_id
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                JSONUtil.puts(_json, "del_yn", "Y");

                finishInfo();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_accuse_ins_direct(String cont, String accuse_type, int comment_no) {

        LogUtil.e("==========callApi_accuse_ins_direct : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.accuse_ins_direct(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _star_member_no,
                accuse_type,
                "99",
                cont,
                _board_no,
                _artist_board_id,
                comment_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "신고 접수가 완료 되었습니다.\n\n최대한 빨리 확인/처리해서 회신 드리겠습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        //finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_board_cmt_ins(String cont) {

        LogUtil.e("==========callApi_board_cmt_ins : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);


        Call<JsonObject> call = apiInterface.boardStar_cmt_ins(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _board_no,
                _comment_no,
                _artist_board_id,
                cont
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (!FormatUtil.isNullorEmpty(resultMessage)) {
                    Alert.toastLong(getContext(), resultMessage);
                }

                _edtReply.setText("");

                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_board_cmt_del(final int position, int comment_no) {

        LogUtil.e("==========callApi_board_cmt_del : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.boardStar_cmt_del(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                comment_no,
                _artist_board_id
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);

                    LogUtil.json(json);

                    JSONObject j = _list.getAdapter().getItem(position);
                    _list.getAdapter().remove(j);
                    _list.getAdapter().notifyDataSetChanged();

                    setResult(RESULT_OK);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finishInfo();
    }


    private void finishInfo() {
        try {
            if (_json != null) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_JSON_STRING, _json.toString());
                setResult(RESULT_OK, i);
                finish();
            } else {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        JSONObject member_info = JSONUtil.getJSONObject(json, "member_info");

        String IS_NO_DATA = JSONUtil.getString(json, "IS_NO_DATA");

        TextView txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
        TextView txtComment = (TextView) convertView.findViewById(R.id.txtComment);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtNoData = (TextView) convertView.findViewById(R.id.txtNoData);
        ImageButton btnOption = (ImageButton) convertView.findViewById(R.id.btnOption);
        ImageButton btnReportComment = (ImageButton) convertView.findViewById(R.id.btnReportComment);
        TextView img_cmt_cmt = convertView.findViewById(R.id.img_cmt_cmt);

        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);
        View baseLine = convertView.findViewById(R.id.baseLine);

        btnOption.setVisibility(View.GONE);
        btnReportComment.setVisibility(View.GONE);

        final int comment_no = JSONUtil.getInteger(json, "no");
        final int g0 = JSONUtil.getInteger(json, "g0");


        if (IS_NO_DATA.equals("Y")) {
            txtUserName.setVisibility(View.GONE);
            txtDate.setVisibility(View.GONE);
            txtComment.setVisibility(View.GONE);
            txtNoData.setText(JSONUtil.getString(json, "cont"));
            txtNoData.setVisibility(View.VISIBLE);
            adViewBody.setVisibility(View.GONE);
            img_cmt_cmt.setVisibility(View.GONE);
            //convertView.setBackgroundColor(getResources().getColor(R.color.pale_grey));
        } else {
            String member_no = JSONUtil.getString(member_info, "member_no");
            txtUserName.setText(JSONUtil.getString(member_info, "nick") + " Lv." + JSONUtil.getString(member_info, "lv"));
            String reg_dttm = JSONUtil.getString(json, "reg_dttm");
            txtDate.setText(FormatUtil.getReplyDateFormat(reg_dttm));

            txtUserName.setVisibility(View.VISIBLE);
            txtDate.setVisibility(View.VISIBLE);
            txtComment.setVisibility(View.VISIBLE);
            txtNoData.setVisibility(View.GONE);

            txtComment.setLinksClickable(true);
            txtComment.setMovementMethod(LinkMovementMethod.getInstance());
            txtComment.setText(Html.fromHtml(JSONUtil.getString(json, "cont")));

            img_cmt_cmt.setVisibility(comment_no == g0 ? View.GONE : View.VISIBLE);


            if (Constants.DISPLAY_AD
                    && Constants.DISPLAY_AD_ROW
                    && (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD)
                    && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
            ) {
                adViewBody.setVisibility(View.VISIBLE);
                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);

//                FrameLayout adViewContainer = convertView.findViewById(R.id.adViewContainer);
//                AdView adViewNew = new AdView(getContext());
//                adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
//                adViewContainer.addView(adViewNew);
//
//                AdRequest adRequest = new AdRequest.Builder().build();
//                adViewNew.setAdSize(getAdSizeInfo());
//                adViewNew.loadAd(adRequest);

            } else {
                adViewBody.setVisibility(View.GONE);
            }

            if (member_no.equals(_enter_member_no)) {
                btnOption.setVisibility(View.VISIBLE);
            } else {
                btnReportComment.setVisibility(View.VISIBLE);
            }

            //convertView.setBackgroundColor(getResources().getColor(R.color.white));
        }

        if (position >= adapter.getCount() - 1) {
            baseLine.setVisibility(View.INVISIBLE);
        } else {
            baseLine.setVisibility(View.VISIBLE);
        }

        btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] items = new String[]{"삭제하기", "취소"};
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        LogUtil.d("onClose, which : " + which);

                        if (which == 0) {
                            // 삭제
                            Alert alert1 = new Alert();
                            alert1.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                @Override
                                public void onClose(DialogInterface dialog, int which) {
                                    if (which == Alert.BUTTON1) {
                                        callApi_board_cmt_del(position, comment_no);
                                    }
                                }
                            });
                            alert1.showAlert(getContext(), "삭제 하시겠습니까?");
                        }
                    }
                });
                alert.showSeletItem(getContext(), items);
            }
        });

        btnReportComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertReport alert = new AlertReport();
                alert.setOnStringListener(new InterfaceSet.OnStringListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which, String str) {
                        if (which < 0)
                            return;
                        callApi_accuse_ins_direct(str, AtvReport.ACCUSE_TYPE_BOARD_COMMENT, comment_no);
                    }
                });

                alert.showReport(getContext());
            }
        });

        return convertView;
    }


}
