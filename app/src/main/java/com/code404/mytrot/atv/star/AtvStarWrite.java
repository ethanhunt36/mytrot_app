package com.code404.mytrot.atv.star;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAgree;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.SelectPhotoManager;
import com.google.gson.JsonObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.Console;
import java.io.File;

import javax.xml.transform.Result;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AtvStarWrite extends AtvBase implements SelectPhotoManager.PhotoSelectCallback {

    private ScrollView _scrollView = null;
    private ImageView _imgAttach = null;
    private EditText _edtContent = null;
    private View _baseArtist = null;
    private TextView _txtArtist = null;

    private TextView _txtTip01 = null;
    private Button _btnWrite = null;

    private TextView _txtTip03 = null;
    private ImageButton _btnCheck = null;

    private SelectPhotoManager m_photoManger = null;

    private File _file = null;
    private int _artist_no = -1;
    private int _category = 1;
    private String _artist_board_id = "";


    @Override
    protected boolean getBundle() {
        _category = getIntent().getIntExtra(Constants.EXTRAS_TYPE, -1);
        return _category > 0;
    }

    @Override
    protected void init() {

        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);

        //Alert.toastLong(getContext(), "_category : " + _category);

        _menuIndex = 1;
        _txtTopTitle.setText("갤러리 작성");
        _baseTopBottom.setVisibility(View.GONE);
        _imgAttach.setBackground(getDrawable(R.drawable._s_background_rounding));
        _imgAttach.setClipToOutline(true);

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
        int artist_no = JSONUtil.getInteger(json, "artist_no");
        String artist_name = JSONUtil.getString(json, "artist_name");

        // 최애가 있으면 가수 자동 선택
        if (artist_no > 0 && !FormatUtil.isNullorEmpty(artist_name)) {
            _artist_no = artist_no;
            _txtArtist.setText(artist_name);
        }

        if (m_photoManger == null) {
            m_photoManger = new SelectPhotoManager(this);
            m_photoManger.setPhotoSelectCallback(this);
        }

        if (!FormatUtil.isNullorEmpty(_artist_board_id)) {
            _txtTip01.setText("* 별방에서의 이미지는 필수가 아닌 선택입니다.");
            findViewById(R.id.baseArtistBody).setVisibility(View.GONE);
        }

        if (SPUtil.getInstance().getIsAgreeWriteBoard(getContext())) {
            _btnCheck.setSelected(true);
        }
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_star_write);
    }

    @Override
    protected void findView() {
        _scrollView = findViewById(R.id.scrollView);
        _imgAttach = (ImageView) findViewById(R.id.imgAttach);
        _edtContent = (EditText) findViewById(R.id.edtContent);
        _baseArtist = findViewById(R.id.baseArtist);
        _txtArtist = findViewById(R.id.txtArtist);
        _btnWrite = (Button) findViewById(R.id.btnWrite);
        _txtTip01 = findViewById(R.id.txtTip01);

        _txtTip03 = findViewById(R.id.txtTip03);
        _btnCheck = findViewById(R.id.btnCheck);
    }


    @Override
    protected void configureListener() {

        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackCheck();
            }
        });

        _baseArtist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertPop alert = new AlertPop();
                alert.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
                    @Override
                    public void onClick(View v, int pos, JSONObject json) {
                        if (json != null) {
                            _artist_no = JSONUtil.getInteger(json, "no");
                            String name = JSONUtil.getString(json, "name");
                            _txtArtist.setText(name);
                        }

                        LogUtil.d("_artist_no : " + _artist_no);
                    }
                });
                alert.showVoteArtist(getContext());
            }
        });

        _edtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("onClick");
                CommonUtil.scrollToView(_edtContent, _scrollView, 0);
            }
        });

        _imgAttach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LogUtil.d("_imgAttach");

                m_photoManger.doPickFromGallery();
            }
        });

        _btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertAgree alert = new AlertAgree();
                alert.showAgree(getContext());
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            _btnCheck.setSelected(true);
                        } else {
                            _btnCheck.setSelected(false);
                        }
                    }
                });
            }
        });

        _txtTip03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertAgree alert = new AlertAgree();
                alert.showAgree(getContext());
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            _btnCheck.setSelected(true);
                        } else {
                            _btnCheck.setSelected(false);
                        }
                    }
                });
            }
        });

        _edtContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                //if (b) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            _scrollView.scrollTo(0, 2000);
                        } catch (Exception e) {
                        }
                    }
                }, 500);

                //}
            }
        });

        _edtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            _scrollView.scrollTo(0, 2000);
                        } catch (Exception e) {
                        }
                    }
                }, 500);
            }
        });

        _btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = _edtContent.getText().toString();

                if (FormatUtil.isNullorEmpty(_artist_board_id)) {
                    if (_file == null) {
                        new Alert().showAlert(getContext(), "이미지를 선택해 주세요.");
                        return;
                    }

                    if (_artist_no < 1) {
                        new Alert().showAlert(getContext(), "가수를 선택해 주세요.");
                        return;
                    }
                }

                if (_btnCheck.isSelected() == false) {
                    new Alert().showAlert(getContext(), "사용자 제작 콘텐츠(UGC) 약관에 동의해주세요.");
                    return;
                }

                if (FormatUtil.isNullorEmpty(content)) {
                    new Alert().showAlert(getContext(), "내용을 입력해 주세요.");
                    return;
                }

                if (content.length() < 3) {
                    new Alert().showAlert(getContext(), "내용을 최소 3자 이상 입력해 주세요.");
                    return;
                }

                SPUtil.getInstance().setIsAgreeWriteBoard(getContext(), true);

                callApi_boardins();
            }
        });
    }


    private void callApi_boardins() {

        LogUtil.e("==========callApi_boardins : start==========");

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

        String nicks = JSONUtil.getString(json, "nick");

        String contents = _edtContent.getText().toString();

        _artist_board_id = !FormatUtil.isNullorEmpty(_artist_board_id) ? _artist_board_id : "";


        RequestBody ssid = RequestBody.create(MediaType.parse("text/plain"), "board.ins");
        RequestBody board_id = RequestBody.create(MediaType.parse("text/plain"), _artist_board_id);

        RequestBody member_no = RequestBody.create(MediaType.parse("text/plain"), SPUtil.getInstance().getUserNoEnc(getContext()));

        MultipartBody.Part pic01 = null;
        RequestBody nick = RequestBody.create(MediaType.parse("text/plain"), nicks);
        RequestBody content = RequestBody.create(MediaType.parse("text/plain"), contents);
        RequestBody category = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_category));
        RequestBody artist_no = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_artist_no));

        if (_file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), _file);
            pic01 = MultipartBody.Part.createFormData("pic1", _file.getName(), requestFile);
        }

        if (!FormatUtil.isNullorEmpty(_artist_board_id)) {
            ssid = RequestBody.create(MediaType.parse("text/plain"), "boardStar.ins");
        }

        LogUtil.d("_artist_no : " + _artist_no);
        LogUtil.d("_category : " + _category);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_ins(
                ssid
                , member_no
                , artist_no
                , category
                , nick
                , content
                , pic01
                , board_id
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "등록 되었습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        setResult(RESULT_OK);
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void onBackCheck() {
        String contents = _edtContent.getText().toString();

        if (!FormatUtil.isNullorEmpty(contents)) {
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialogAlert, int which) {
                    if (which == Alert.BUTTON1) {
                        finish();
                    }
                }
            });
            alert.showAlert(getContext(), "입력 중인 내용이 있습니다.\n\n취소 하시겠습니까?", true, "확인", "아니오, 계속 입력합니다.");
            return;
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        onBackCheck();
    }


    /**
     * 이미지 크랍 완료
     *
     * @param image
     * @param file
     */
    @Override
    public void onSelectImageDone(Bitmap image, File file) {

        LogUtil.d("onSelectImageDone");

        if (image != null && file != null) {
            BitmapFactory.Options onlyBoundsOptions2 = CommonUtil.getBitmapFactoryFromUri(getContext(), Uri.fromFile(file));
            LogUtil.d("options.outWidth : " + onlyBoundsOptions2.outWidth);
            LogUtil.d("options.outHeight : " + onlyBoundsOptions2.outHeight);

            int photo_min_size = 320;

            // 핸드폰 디스플레이 사이즈보다 작은 사진은 선택 못하도록 합니다.
            if (onlyBoundsOptions2.outWidth <= photo_min_size || onlyBoundsOptions2.outHeight <= photo_min_size) {
                Alert alert = new Alert();
                alert.showAlert(getContext(), getString(R.string.please_photo_width_01));
                return;
            }


            _imgAttach.setScaleType(ImageView.ScaleType.CENTER_CROP);
            _imgAttach.setImageBitmap(image);

            _file = file;
        }
    }

    @Override
    public void onFailedSelectImage(int errorCode, String err) {
        LogUtil.d("onFailedSelectImage");
    }

    @Override
    public void onDeleteImage() {
        LogUtil.d("onDeleteImage");
    }

    @Override
    public void onPermissionUpdated(int permission_type, boolean isGranted) {
        LogUtil.d("onPermissionUpdated");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);
        LogUtil.d("onActivityResult, data : " + data);
        LogUtil.intent("onActivityResult", data);


        if (resultCode == Activity.RESULT_OK) {

            // 갤러리에서 사진 선택 시.
            if (requestCode == SelectPhotoManager.PICK_FROM_FILE) {
                if (data.getData() == null) {
                    new Alert().showAlert(getContext(), "사진을 가져오는 동안 오류가 발생하였습니다. 앱을 재시작 후에 다시 시도해주세요.");
                } else {
                    String str = data.getData().toString();


                    LogUtil.d("onActivityResult, str : " + str);

                    String url = CommonUtil.getRealPathFromURI(getContext(), data.getData());
                    LogUtil.d("onActivityResult, url : " + url);

                    String mimeType = CommonUtil.getMimeType(url);
                    LogUtil.d("onActivityResult, mimeType : " + mimeType);

                    if (!FormatUtil.isNullorEmpty(mimeType) && (!mimeType.toLowerCase().endsWith("jpeg") && !mimeType.toLowerCase().endsWith("jpg") && !mimeType.toLowerCase().endsWith("png"))) {
                        new Alert().showAlert(getContext(), "사진은 jpg, png 형태의 파일만 등록하실 수 있습니다.");
                        return;
                    }
                }
            }
        }

        if (requestCode == UCrop.REQUEST_CROP || requestCode == SelectPhotoManager.PICK_FROM_FILE) {
            m_photoManger.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        if (m_photoManger != null) {
            m_photoManger.removeTempFile(getContext());
        }
        super.onDestroy();
    }
}
