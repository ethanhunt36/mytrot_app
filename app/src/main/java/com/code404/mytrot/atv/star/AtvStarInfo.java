package com.code404.mytrot.atv.star;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.etc.AtvReport;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertReport;
import com.code404.mytrot.util.BoardViewUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SquareImageView;
import com.code404.mytrot.widget.ItemNativeAd;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import retrofit2.Call;

public class AtvStarInfo extends AtvBase implements GListView.IMakeView {


    private View _header = null;
    private View _baseReport = null;

    private EditText _edtReply = null;
    private Button _btnSend = null;
    private TextView _txtDownload = null;

    private SquareImageView _img01 = null;
    private SquareImageView _img02 = null;
    private SquareImageView _img03 = null;

    private ImageView _imgHeart = null;
    private TextView _txtLike = null;
    private TextView _txtRead = null;
    private TextView _txtContent = null;
    private TextView _txtNick = null;
    private TextView _txtDate = null;

    private AdView _adView = null;
    private ItemNativeAd _itemNativeAd = null;
    private View _baseBoardLine = null;

    private GListView _list = null;

    private JSONObject _json = null;

    Bitmap _bitmap = null;

    // 작성한 회원 번호
    private String _star_member_no = "";

    // 열람 중인 회원 번호
    private String _enter_member_no = "";
    private int _category = -1;

    private JSONArray _jsonArrayComment = null;
    private BoardViewUtil _boardViewUtil = null;

    private int _board_no = -1;

    private String[] _report_items = new String[]{"다른 가수 비난",
            "욕설 및 불쾌한 내용",
            "관련 없는 광고성 내용",
            "음란하거나 선정적 내용",
            "기타 부적절한 내용"};

    private String[] _report_man_items = new String[]{"다른 가수 비난",
            "욕설 및 불쾌한 내용",
            "관련 없는 광고성 내용",
            "음란하거나 선정적 내용",
            "지속적인 도배글",
            "기타 부적절한 내용"};

    private boolean _have_permission = false;


    @Override
    protected boolean getBundle() {
        String jsonStr = getIntent().getStringExtra(Constants.EXTRAS_JSON_STRING);

        _json = JSONUtil.createObject(jsonStr);

        LogUtil.json(_json);

        _board_no = JSONUtil.getInteger(_json, "no");
        _star_member_no = JSONUtil.getString(_json, "member_no");
        _category = JSONUtil.getInteger(_json, "category", -1);

        LogUtil.d("_board_no : " + _board_no);
        int board_no = getIntent().getIntExtra(Constants.EXTRAS_BOARD_NO, 0);

        if (board_no > 0) {
            _board_no = board_no;
        }

        LogUtil.d("_board_no : " + _board_no);


        return _board_no > 0;
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_star_info);
    }

    @Override
    protected void findView() {
        _list = findViewById(R.id.list);

        _edtReply = (EditText) findViewById(R.id.edtReply);
        _btnSend = (Button) findViewById(R.id.btnSend);
        _header = View.inflate(getContext(), R.layout.row_star_info, null);
        _baseReport = _header.findViewById(R.id.baseReport);
        _img01 = _header.findViewById(R.id.img01);
        _img02 = _header.findViewById(R.id.img02);
        _img03 = _header.findViewById(R.id.img03);
        _imgHeart = (ImageView) _header.findViewById(R.id.imgHeart);
        _txtLike = (TextView) _header.findViewById(R.id.txtLike);
        _txtRead = (TextView) _header.findViewById(R.id.txtRead);
        _txtContent = (TextView) _header.findViewById(R.id.txtContent);

        _txtNick = (TextView) _header.findViewById(R.id.txtNick);
        _txtDate = (TextView) _header.findViewById(R.id.txtDate);
        _txtDownload = _header.findViewById(R.id.txtDownload);
        _baseBoardLine = _header.findViewById(R.id.baseBoardLine);

        _adView = _header.findViewById(R.id.adView);
        _itemNativeAd = _header.findViewById(R.id.itemNativeAd);
    }


    @Override
    protected void init() {
        _txtTopTitle.setText("갤러리");

        _baseTopBottom.setVisibility(View.GONE);
        _baseBoardLine.setVisibility(View.GONE);
        _btnTopBack.setVisibility(View.VISIBLE);

        _txtTopRight.setText("〮〮〮");
        _txtTopRight.setVisibility(View.VISIBLE);
        _txtTopRight.setVisibility(View.GONE);

        _btnTopRight.setImageResource(R.drawable.option);
        _btnTopRight.setBackgroundResource(R.drawable._s_btn_basic);
        _btnTopRight.setVisibility(View.VISIBLE);


        _list.addHeaderView(_header);
        _list.setViewMaker(R.layout.row_comment, this);


        _boardViewUtil = new BoardViewUtil();

        _enter_member_no = SPUtil.getInstance().getUserNoEnc(getContext());

        setDataBinding();

        init_check_permission();

        callApi_board_view(false);
    }

    private void executeDownload() {
        String img01 = JSONUtil.getStringUrl(_json, "img01");
        new ImageDownload().execute(img01);
    }

    @Override
    protected void configureListener() {

        _btnTopBack.setOnClickListener(null);
        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishInfo();
            }
        });

        _btnTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onReport();
            }
        });


        _txtDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogUtil.d("Build.VERSION.SDK_INT : " + Build.VERSION.SDK_INT);
                LogUtil.d("Build.VERSION_CODES.Q : " + Build.VERSION_CODES.Q);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q && _have_permission == false) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            init_check_permission();
                        }
                    });
                    alert.showAlert(getContext(), "저장 권한이 없습니다.");
                    return;
                }

                _bitmap = _img01.getDrawingCache();

                executeDownload();
                callApi_board_inc_download();
            }
        });

        _btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cont = _edtReply.getText().toString().trim();

                if (FormatUtil.isNullorEmpty(cont)) {
                    new Alert().showAlert(getContext(), "내용을 입력해 주세요.");
                    return;
                }

                callApi_board_cmt_ins(cont);
            }
        });

        // 회원 신고
        _baseReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertReport alert = new AlertReport();
                alert.setOnStringListener(new InterfaceSet.OnStringListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which, String str) {
                        if (which < 0)
                            return;
                        callApi_accuse_ins_direct(str, AtvReport.ACCUSE_TYPE_BOARD, -1);
                    }
                });

                alert.showReport(getContext());
            }
        });

        // 글 신고
        _baseReportRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertReport alert = new AlertReport();
                alert.setOnStringListener(new InterfaceSet.OnStringListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which, String str) {
                        if (which < 0)
                            return;
                        callApi_accuse_ins_direct(str, AtvReport.ACCUSE_TYPE_BOARD, -1);
                    }
                });

                alert.showReport(getContext());
            }
        });
    }


    private void init_check_permission() {

        TedPermission.with(getContext())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        _have_permission = true;
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        // Possibility of denied even if this app granted permissions
                        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                            _have_permission = true;

                        } else {
                            _have_permission = false;
                        }
                    }
                })
                .setPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .check();
    }

    private void onReport() {
        Alert alert = new Alert();
        String[] items = new String[]{"게시글 신고하기", "작성자의 글 모두 차단하기", "취소"};

        if (_star_member_no.equals(SPUtil.getInstance().getUserNoEnc(getContext()))) {
            items = new String[]{"삭제하기", "취소"};
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    LogUtil.d("onClose, which : " + which);

                    if (which == 0) {
                        // 삭제
                        Alert alert1 = new Alert();
                        alert1.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    callApi_board_del();
                                }
                            }
                        });
                        alert1.showAlert(getContext(), "삭제 하시겠습니까?");
                    } else if (which == 1) {
//                                // 수정
//
//                                if (_category == 1 || _category == 2) {
//                                    Intent intent = new Intent();
//                                    intent.setClass(getContext(), AtvStarWrite.class);
//                                    intent.putExtra(Constants.EXTRAS_JSON_STRING, _json.toString());
//                                    startActivity(intent);
//                                }
                    }
                }
            });
        } else {
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    LogUtil.d("onClose, which : " + which);
                    if (which == 0) {
                        // 신고하기
                        Intent intent = new Intent();
                        intent.setClass(getContext(), AtvReport.class);
                        intent.putExtra(Constants.EXTRAS_MEMBER_NO, _star_member_no);
                        intent.putExtra(Constants.EXTRAS_BOARD_NO, _board_no);
                        intent.putExtra(Constants.EXTRAS_ACCUSE_TYPE, AtvReport.ACCUSE_TYPE_BOARD);
                        startActivity(intent);
                    } else if (which == 1) {
                        // 작성자의 글 모두 차단하기
                        Alert alert1 = new Alert();
                        alert1.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    callApi_memberBlock_block_by_board();
                                }
                            }
                        });
                        alert1.showAlert(getContext(), "작성자의 글을 모두 차단 하시겠습니까?");
                    }
                }
            });
        }

        alert.showSeletItem(getContext(), items);


    }

    private void setDataBinding() {
        _header = _boardViewUtil.setViewEvent(getContext(), _json, _header, _list.getAdapter(), true);
        _category = JSONUtil.getInteger(_json, "category", 1);

        if (_category == 1) {
            _txtTopTitle.setText("응원글");
        } else if (_category == 2) {
            _txtTopTitle.setText("갤러리");
        } else if (_category == 3) {
            _txtTopTitle.setText("인증샷 상세");
        }

        if (_category == 2 && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            _txtDownload.setVisibility(View.VISIBLE);
        } else {
            _txtDownload.setVisibility(View.GONE);
        }


        if (!SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            AdRequest adRequest = new AdRequest.Builder().build();
            _adView.loadAd(adRequest);
            _adView.setVisibility(View.VISIBLE);

            LogUtil.d("_list.getCountAll() : " + _list.getCountAll());
            if (_list.getCountAll() > 1) {
                _adView.setVisibility(View.GONE);
                _itemNativeAd.setVisibility(View.VISIBLE);
                _itemNativeAd.initAd();
            } else {
                _adView.setVisibility(View.VISIBLE);
                _itemNativeAd.setVisibility(View.GONE);
            }
        } else {
            _adView.setVisibility(View.GONE);
        }

        _baseReportRight.setVisibility(View.GONE);

        if (_star_member_no.equals(SPUtil.getInstance().getUserNoEnc(getContext()))) {
            _baseReport.setVisibility(View.GONE);
            _btnTopRight.setVisibility(View.VISIBLE);
        } else {
            // 남의 컨텐츠
            _btnTopRight.setVisibility(View.GONE);
            //_baseReportRight.setVisibility(View.VISIBLE);
        }
    }


    private void callApi_board_view(final boolean afterComment) {

        LogUtil.e("==========callApi_board_view : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_view(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _board_no,
                -1,
                2000,
                "recent"
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                int is_ilike = JSONUtil.getInteger(data, "is_ilike");
                _json = JSONUtil.getJSONObject(data, "view_data");

                JSONObject memberJson = JSONUtil.getJSONObject(_json, "member_info");

                _star_member_no = JSONUtil.getString(memberJson, "member_no");

                JSONUtil.puts(_json, "is_ilike", is_ilike);

                JSONObject comment_data = JSONUtil.getJSONObject(data, "comment_data");

                _jsonArrayComment = JSONUtil.getJSONArray(comment_data, "list_data");

                if (_jsonArrayComment == null || _jsonArrayComment.length() < 1) {
                    _jsonArrayComment = new JSONArray();
                    JSONObject jsonRow = new JSONObject();
                    JSONUtil.puts(jsonRow, "cont", "첫 번째 댓글을 남겨주세요~");
                    JSONUtil.puts(jsonRow, "IS_NO_DATA", "Y");
                    _jsonArrayComment.put(jsonRow);
                }

                _list.addItems(_jsonArrayComment);

                setDataBinding();

                if (afterComment) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            _list.smoothScrollToPosition(1);
                        }
                    }, 500);
                }

                _baseBoardLine.setVisibility(_list.getCount() < 1 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_board_del() {

        LogUtil.e("==========callApi_board_del : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_del(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _board_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                JSONUtil.puts(_json, "del_yn", "Y");

                finishInfo();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_memberBlock_block_by_board() {

        LogUtil.e("==========callApi_board_del : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberBlock_block_by_board(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _board_no,
                ""
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                JSONUtil.puts(_json, "del_yn", "Y");

                finishInfo();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_board_cmt_ins(String cont) {

        LogUtil.e("==========callApi_board_cmt_ins : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        int board_no = JSONUtil.getInteger(_json, "no");

        Call<JsonObject> call = apiInterface.board_cmt_ins(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                board_no,
                cont
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (!FormatUtil.isNullorEmpty(resultMessage)) {
                    Alert.toastLong(getContext(), resultMessage);
                }

                if (resultCode != 0) {
                    return;
                }

                _edtReply.setText("");
                _list.removeAll();

                callApi_board_view(true);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_board_cmt_del(final int position, int comment_no) {

        LogUtil.e("==========callApi_board_cmt_del : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_cmt_del(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                comment_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);

                    LogUtil.json(json);

                    JSONObject j = _list.getAdapter().getItem(position);
                    _list.getAdapter().remove(j);
                    _list.getAdapter().notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_board_inc_download() {

        LogUtil.e("==========callApi_board_inc_download : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        int board_no = JSONUtil.getInteger(_json, "no");

        Call<JsonObject> call = apiInterface.board_inc_download(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                board_no
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                int download_cnt = JSONUtil.getInteger(_json, "download_cnt", 0);
                download_cnt++;
                JSONUtil.puts(_json, "download_cnt", download_cnt);

                setDataBinding();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_accuse_ins_direct(String cont, String accuse_type, int comment_no) {

        LogUtil.e("==========callApi_accuse_ins_direct : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.accuse_ins_direct(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _star_member_no,
                accuse_type,
                "99",
                cont,
                _board_no,
                "",
                comment_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "신고 접수가 완료 되었습니다.\n\n최대한 빨리 확인/처리해서 회신 드리겠습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        //finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finishInfo();
    }


    private void finishInfo() {
        try {
            if (_json != null) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_JSON_STRING, _json.toString());
                setResult(RESULT_OK, i);
                finish();
            } else {
                finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        JSONObject member_info = JSONUtil.getJSONObject(json, "member_info");

        String IS_NO_DATA = JSONUtil.getString(json, "IS_NO_DATA");

        TextView txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
        TextView txtComment = (TextView) convertView.findViewById(R.id.txtComment);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtNoData = (TextView) convertView.findViewById(R.id.txtNoData);
        ImageButton btnOption = (ImageButton) convertView.findViewById(R.id.btnOption);
        ImageButton btnReportComment = (ImageButton) convertView.findViewById(R.id.btnReportComment);

        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);
        View baseLine = convertView.findViewById(R.id.baseLine);

        btnOption.setVisibility(View.GONE);
        btnReportComment.setVisibility(View.GONE);

        final int comment_no = JSONUtil.getInteger(json, "no");

        if (IS_NO_DATA.equals("Y")) {
            txtUserName.setVisibility(View.GONE);
            txtDate.setVisibility(View.GONE);
            txtComment.setVisibility(View.GONE);
            txtNoData.setText(JSONUtil.getString(json, "cont"));
            txtNoData.setVisibility(View.VISIBLE);
            adViewBody.setVisibility(View.GONE);

            //convertView.setBackgroundColor(getResources().getColor(R.color.pale_grey));
        } else {
            String member_no = JSONUtil.getString(member_info, "member_no");

            txtUserName.setText(JSONUtil.getString(member_info, "nick")
                    + (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") ? (" Lv." + JSONUtil.getString(member_info, "lv")) : ""));


            String reg_dttm = JSONUtil.getString(json, "reg_dttm");
            String cont = JSONUtil.getString(json, "cont");

            cont = cont.replaceAll("\n", "<br>");

            LogUtil.d("cont : " + cont);

            txtDate.setText(FormatUtil.getReplyDateFormat(reg_dttm));

            txtUserName.setVisibility(View.VISIBLE);
            txtDate.setVisibility(View.VISIBLE);
            txtComment.setVisibility(View.VISIBLE);
            txtNoData.setVisibility(View.GONE);

            txtComment.setLinksClickable(true);
            txtComment.setMovementMethod(LinkMovementMethod.getInstance());
            txtComment.setText(Html.fromHtml(cont));

            if (Constants.DISPLAY_AD
                    && Constants.DISPLAY_AD_ROW
                    && (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD)
                    && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                    && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
            ) {
                adViewBody.setVisibility(View.VISIBLE);
                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);

//                FrameLayout adViewContainer = convertView.findViewById(R.id.adViewContainer);
//                AdView adViewNew = new AdView(getContext());
//                adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
//                adViewContainer.addView(adViewNew);
//
//                AdRequest adRequest = new AdRequest.Builder().build();
//                adViewNew.setAdSize(getAdSizeInfo());
//                adViewNew.loadAd(adRequest);

            } else {
                adViewBody.setVisibility(View.GONE);
            }

            if (member_no.equals(_enter_member_no)) {
                btnOption.setVisibility(View.VISIBLE);
            } else {
                btnReportComment.setVisibility(View.VISIBLE);
            }

            //convertView.setBackgroundColor(getResources().getColor(R.color.white));
        }

//        if (position > adapter.getCount() - 1) {
//            baseLine.setVisibility(View.INVISIBLE);
//        } else {
        baseLine.setVisibility(View.VISIBLE);
//        }

        btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] items = new String[]{"삭제하기", "취소"};
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        LogUtil.d("onClose, which : " + which);

                        if (which == 0) {
                            // 삭제
                            Alert alert1 = new Alert();
                            alert1.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                @Override
                                public void onClose(DialogInterface dialog, int which) {
                                    if (which == Alert.BUTTON1) {
                                        callApi_board_cmt_del(position, comment_no);
                                    }
                                }
                            });
                            alert1.showAlert(getContext(), "삭제 하시겠습니까?");
                        }
                    }
                });
                alert.showSeletItem(getContext(), items);
            }
        });

        btnReportComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertReport alert = new AlertReport();
                alert.setOnStringListener(new InterfaceSet.OnStringListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which, String str) {
                        if (which < 0)
                            return;
                        callApi_accuse_ins_direct(str, AtvReport.ACCUSE_TYPE_BOARD_COMMENT, comment_no);
                    }
                });

                alert.showReport(getContext());
            }
        });

        return convertView;
    }


    /**
     * url 이미지 다운로드
     */
    private class ImageDownload extends AsyncTask<String, Void, Void> {


        private String _file_path = "";


        @Override
        protected Void doInBackground(String... params) {

            /* 저장할 폴더 Setting */
            //File uploadFolder = Environment.getExternalStoragePublicDirectory("/DCIM/Mytrot/"); //저장 경로 (File Type형 변수)
            File uploadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);


            if (!uploadFolder.exists()) { //만약 경로에 폴더가 없다면
                uploadFolder.mkdir(); //폴더 생성
            }

            String title = "MYTROT_" + FormatUtil.getCurrentDateHms() + ".jpg";

            /* 파일 저장 */
            //String Str_Path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/Mytrot/"; //저장 경로 (String Type 변수)
            String Str_Path = uploadFolder.getAbsolutePath();


            _file_path = Str_Path + "/" + title;

            LogUtil.d("_file_path : " + _file_path);


            //웹 서버 쪽 파일이 있는 경로
            String fileUrl = params[0];


            try {
                URL imgUrl = new URL(fileUrl);
                //서버와 접속하는 클라이언트 객체 생성
                HttpURLConnection conn = (HttpURLConnection) imgUrl.openConnection();
                int len = conn.getContentLength();
                byte[] tmpByte = new byte[len];
                //입력 스트림을 구한다
                InputStream is = conn.getInputStream();
                File file = new File(_file_path);
                //파일 저장 스트림 생성
                FileOutputStream fos = new FileOutputStream(file);
                int read;
                //입력 스트림을 파일로 저장
                for (; ; ) {
                    read = is.read(tmpByte);
                    if (read <= 0) {
                        break;
                    }
                    fos.write(tmpByte, 0, read); //file 생성
                }
                is.close();
                fos.close();
                conn.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
                LogUtil.e(e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            File file = new File(_file_path);

            //이미지 스캔해서 갤러리 업데이트
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)));
            MediaScannerConnection.scanFile(getApplicationContext(), new String[]{file.getAbsolutePath()}, null
                    , new MediaScannerConnection.OnScanCompletedListener() {
                        @Override
                        public void onScanCompleted(String s, Uri uri) {
                            LogUtil.d("onScanCompleted, s : " + s + ", uri : " + uri);
                        }
                    });
            Alert.toastLong(getContext(), "갤러리에 다운로드 되었습니다.");
        }


    }

}
