package com.code404.mytrot.atv.main;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.artist.AtvArtistVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertSearch;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import retrofit2.Call;


public class AtvChart extends AtvBase implements GListView.IMakeView {

    private View _header = null;
    private GListView _list = null;

    private TextView _txtSeqAll = null;
    private TextView _txtSeqM = null;
    private TextView _txtSeqF = null;

    private int _last_no = -1;
    private String _gender = "";
    @Override
    protected void setView() {
        setView(R.layout.atv_chart);
    }


    @Override
    protected void init() {
        _menuIndex = 0;
        _canFinish = true;
        _txtTopTitle.setText("트롯차트");
        _btnTopBack.setVisibility(View.GONE);


        _btnTopRight.setImageResource(R.drawable._s_btn_find);
        _btnTopRight.setVisibility(View.VISIBLE);

        if(SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _btnTopRight.setVisibility(View.GONE);
        }

        _list.setViewMaker(R.layout.row_rank, this);

        if (!Constants.APP_DEVICE_ID.equals("MYTROT") && !SPUtil.getInstance().getIsLogin(getContext())) {
            String uniqueID = SPUtil.getInstance().getAndroidID(getContext());
            LogUtil.d("uniqueID : " + uniqueID);
            callApi_member_get_by_email(uniqueID, "A", uniqueID);
        }



        if(SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            String mainToastDate = SPUtil.getInstance().getMainToastDate(getContext());
            String todayDate = FormatUtil.getCurrentDate();

            if (!todayDate.equals(mainToastDate)) {
                String msg = "트롯차트의 점수 집계 방식에 대해\n안내 말씀 드립니다.\n\n\n";
                msg += "총점 = 재생X4 + 투표X3 + 응원글X2 + 좋아요\n\n\n";
                msg += "좋아하는 가수의 노래를 많이 듣고,\n투표하고, 응원글도 남기고,\n응원글에 '좋아요' 많이 눌러주세요~";
                new Alert().showAlert(getContext(), msg, true);

                SPUtil.getInstance().setMainToastDate(getContext(), todayDate);
            }
        }

        String gender = SPUtil.getInstance().getRankGender(getContext());

        setBtnTab(gender);

        callApi_artist_ranking(true);
    }

    @Override
    protected void findView() {
        _list = findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_chart, null);
        _list.addHeaderView(_header);

        _txtSeqAll = _header.findViewById(R.id.txtSeqAll);
        _txtSeqM = _header.findViewById(R.id.txtSeqM);
        _txtSeqF = _header.findViewById(R.id.txtSeqF);
    }


    private void setBtnTab(String gender) {
        _gender = gender;

        _txtSeqAll.setBackground(null);
        _txtSeqM.setBackground(null);
        _txtSeqF.setBackground(null);

        if (_gender.equals("")) {
            _txtSeqAll.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        } else if (_gender.equals("M")) {
            _txtSeqM.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        } else if (_gender.equals("F")) {
            _txtSeqF.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        }

        SPUtil.getInstance().setRankGender(getContext(), _gender);
    }


    @Override
    protected void configureListener() {

        _btnTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopSearch();
            }
        });


        _txtSeqAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnTab("");
                callApi_artist_ranking(true);
            }
        });

        _txtSeqM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnTab("M");
                callApi_artist_ranking(true);
            }
        });

        _txtSeqF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnTab("F");
                callApi_artist_ranking(true);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        // callApi_common_check_app_version();


    }


    private void callApi_artist_ranking(final boolean isClear) {
        LogUtil.e("==========callApi_artist_ranking : start==========");

        if(isClear) {
            _last_no = -1;
        }

        final long start_time = System.currentTimeMillis();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_ranking(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , _last_no
                , "rank1"
                , _gender
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                long end_time = System.currentTimeMillis();

                LogUtil.d("artist.ranking1  : " + (end_time - start_time) / 1000.0);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                if (jsonList != null) {
                    if (isClear) {
                        _list.removeAll();
                    }

                    for(int i = 0; i < jsonList.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(jsonList, i);

                        JSONUtil.puts(j, "rank1", i+1);
                    }

                    _list.addItems(jsonList);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");

            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        LinearLayout baseLeft = (LinearLayout) convertView.findViewById(R.id.baseLeft);
        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
        de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
        LinearLayout baseRight = (LinearLayout) convertView.findViewById(R.id.baseRight);
        TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
        TextView txtArtistPoint = (TextView) convertView.findViewById(R.id.txtArtistPoint);
        TextView txtPlayCount = (TextView) convertView.findViewById(R.id.txtPlayCount);
        TextView txtPlayCountMy = (TextView) convertView.findViewById(R.id.txtPlayCountMy);
        TextView txtVoteCount = (TextView) convertView.findViewById(R.id.txtVoteCount);
        TextView txtVoteCountMy = (TextView) convertView.findViewById(R.id.txtVoteCountMy);
        TextView txtBoardCount = (TextView) convertView.findViewById(R.id.txtBoardCount);
        TextView txtBoardCountMy = (TextView) convertView.findViewById(R.id.txtBoardCountMy);
        TextView txtLikeCount = (TextView) convertView.findViewById(R.id.txtLikeCount);
        TextView txtLikeCountMy = (TextView) convertView.findViewById(R.id.txtLikeCountMy);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);


        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);

        if (Constants.DISPLAY_AD
                && Constants.DISPLAY_AD_ROW
                && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD
                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            adViewBody.setVisibility(View.VISIBLE);
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        } else {
            adViewBody.setVisibility(View.GONE);
        }

        String pic1 = JSONUtil.getStringUrl(json, "pic");
        String rank = JSONUtil.getString(json, "rank");
        LogUtil.d("position : " + position + ", rank : " + rank);
        LogUtil.d("pic1 : " + pic1);

        txtRank.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "rank1")));
        txtArtist.setText(JSONUtil.getString(json, "name"));

        txtArtistPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "rank1_score")));

        txtPlayCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "play_cnt", "0")));
        txtPlayCountMy.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_play_cnt", "0")));

        txtVoteCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "vote_cnt", "0")));
        txtVoteCountMy.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_vote_cnt", "0")));

        txtBoardCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "bbs_cnt", "0")));
        txtBoardCountMy.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_bbs_cnt", "0")));

        txtLikeCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "like_cnt", "0")));
        txtLikeCountMy.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_like_cnt", "0")));


        if(!FormatUtil.isNullorEmpty(pic1)) {
            Picasso.with(getContext())
                    .load(pic1)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent();
//                i.setClass(getContext(), AtvArtistVod.class);
//                i.putExtra(Constants.EXTRAS_JSON_STRING, json.toString());
//                i.putExtra(Constants.EXTRAS_ARTIST_NO, JSONUtil.getInteger(json, "no"));
//                i.putExtra(Constants.EXTRAS_TITLE, JSONUtil.getString(json, "name"));
//                startActivity(i);
//            }
//        });

        return convertView;
    }


}
