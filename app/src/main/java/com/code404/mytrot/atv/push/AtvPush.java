package com.code404.mytrot.atv.push;

import android.content.Intent;
import android.os.Bundle;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.AtvIntro;
import com.code404.mytrot.util.LogUtil;


public class AtvPush extends AtvBase {


    @Override
    protected void setView() {
        setView(R.layout.atv_intro);
    }

    @Override
    protected void findView() {

    }

    @Override
    protected void init() {

        LogUtil.d("AtvPush------------------------------------------------------");
        LogUtil.intent(getIntent());
        LogUtil.d("------------------------------------------------------");

        if (getIntent().getExtras() == null) {
            Intent intent = new Intent(this, AtvIntro.class);
            startActivity(intent);
        } else {
            Bundle data = getIntent().getExtras().getBundle("push");
            Intent intent = new Intent(this, AtvIntro.class);
            intent.putExtra("push", data);
            intent.setAction("start_from_push");
            startActivity(intent);
        }
    }

    @Override
    protected void configureListener() {

    }

}
