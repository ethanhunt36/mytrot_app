package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.star.AtvStarInfo;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvAlrim extends AtvBase implements GListView.IMakeView {

    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;

    int _last_no = -1;


    @Override
    protected void setView() {
        setView(R.layout.atv_alrim);
    }

    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
    }

    @Override
    protected void init() {
        _txtTopTitle.setText("알림");
        _baseTopBottom.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_alrim, this);
        _list.setNoData(_baseNoData);

        _txtTopRight.setText("전체삭제");
        _txtTopRight.setVisibility(View.GONE);

        callApi_alram_listing(true);
    }


    @Override
    protected void configureListener() {

        _txtTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if(which == Alert.BUTTON1){
                            callApi_alram_delete();
                        }
                    }
                });
                alert.showAlert(getContext(), "전체 삭제하시겠습니까?", false, "확인", "취소");
            }
        });

        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_alram_listing(false);
                }
            }
        });
    }

    private void callApi_alram_listing(final boolean isClear) {

        LogUtil.e("==========callApi_alram_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.alram_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _last_no
                , Constants.RECORD_SIZE
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list_data");

                if (isClear) {
                    _list.removeAll();
                }

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");

                    _txtTopRight.setVisibility(View.VISIBLE);
                }



                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_alram_delete() {

        LogUtil.e("==========callApi_alram_delete : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.alram_delete(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                _txtTopRight.setVisibility(View.GONE);

                _list.removeAll();
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        com.code404.mytrot.view.SquareImageView imgProfile = convertView.findViewById(R.id.imgProfile);
        TextView txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);


        String cont = JSONUtil.getString(json, "cont");

        cont = cont.replaceAll("\n", "<br>");


        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        reg_dttm = reg_dttm.substring(0, 16);

        final int type = JSONUtil.getInteger(json, "type");
        final int board_no = JSONUtil.getInteger(json, "board_no");
        final int msg_room_no = JSONUtil.getInteger(json, "msg_room_no");
        final String other_member_no = JSONUtil.getString(json, "other_member_no");

        JSONObject memberInfo = JSONUtil.getJSONObject(json, "other_member_info");
        String pic1 = JSONUtil.getStringUrl(memberInfo, "pic1");

        txtMessage.setLinksClickable(true);
        txtMessage.setMovementMethod(LinkMovementMethod.getInstance());
        txtMessage.setText(Html.fromHtml(cont));
        txtDate.setText(reg_dttm);

        imgProfile.setVisibility(View.GONE);


        LogUtil.d("board_no : " + board_no + ", type : " + type);

        txtMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogUtil.d("setOnClickListener, board_no : " + board_no + ", type : " + type);

                Intent i = new Intent();
                switch (type) {
                    case 40:     // 댓글
                    case 41:     // 대댓글
                        i.setClass(getContext(), AtvStarInfo.class);
                        i.putExtra(Constants.EXTRAS_BOARD_NO, board_no);
                        startActivity(i);
                        break;

                }

            }
        });

        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogUtil.d("setOnClickListener, board_no : " + board_no + ", type : " + type);

                Intent i = new Intent();
                switch (type) {
                    case 40:     // 댓글
                    case 41:     // 대댓글
                        i.setClass(getContext(), AtvStarInfo.class);
                        i.putExtra(Constants.EXTRAS_BOARD_NO, board_no);
                        startActivity(i);
                        break;

                }

            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LogUtil.d("setOnClickListener, board_no : " + board_no + ", type : " + type);

                Intent i = new Intent();
                switch (type) {
                    case 40:     // 댓글
                    case 41:     // 대댓글
                        i.setClass(getContext(), AtvStarInfo.class);
                        i.putExtra(Constants.EXTRAS_BOARD_NO, board_no);
                        startActivity(i);
                        break;

                }

            }
        });

        return convertView;
    }

}
