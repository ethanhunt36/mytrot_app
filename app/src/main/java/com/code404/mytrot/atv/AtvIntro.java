package com.code404.mytrot.atv;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.splashscreen.SplashScreen;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.code404.mytrot.MyTrotApp;
import com.code404.mytrot.R;
import com.code404.mytrot.atv.etc.AtvQuiz;
import com.code404.mytrot.atv.main.AtvChart;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AppOpenAdManager;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.UUID;

import retrofit2.Call;


public class AtvIntro extends AtvBase {

    private AppOpenAdManager appOpenAdManager;


    private boolean isAdShown = false;
    private boolean isAdDismissed = false;
    private boolean isLoadCompleted = false;
    private boolean _isCallApiFailed = true;


    private ImageView _imgIntro = null;


    private static String TAG = "[AtvIntro]";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SplashScreen splashScreen = SplashScreen.installSplashScreen(this);


        super.onCreate(savedInstanceState);


        splashScreen.setKeepOnScreenCondition(() -> false);

    }

    @Override
    protected void findView() {

        _imgIntro = findViewById(R.id.imgIntro);
    }


    @Override
    protected void configureListener() {


    }

    @Override
    protected void setView() {
        setView(R.layout.atv_intro);
    }


    @Override
    protected void init() {
        _baseTopTitle.setVisibility(View.GONE);
        _baseTopBottom.setVisibility(View.GONE);


        appOpenAdManager = null;


        String token = SPUtil.getInstance().getRegistPushKey(getContext());

        LogUtil.d("_txtAd, token : " + token);

        int token_save_date = SPUtil.getInstance().getRegistPushKeyDate(getContext());
        LogUtil.d("_txtAd, token_save_date : " + token_save_date);

        int today = Integer.parseInt(FormatUtil.getCurrentDate().replace("-", ""));


        LogUtil.d("_txtAd, today : " + today);


//        Glide.with(this).load(R.raw.intro_240807).into(_imgIntro);


//        String uniqueID = UUID.randomUUID().toString();
//        LogUtil.d("_txtAd, uniqueID : " + uniqueID);
//
//        String uuid = CommonUtil.getDeviceUuid(getContext());
//        LogUtil.d("_txtAd, uniqueID : " + uniqueID);

//        String key = CommonUtil.getKeyHash(getContext());
//        LogUtil.d("key : " + key);
//        if (true) {
//            return;
//        }

        if (SPUtil.getInstance().getUseIntroYn(getContext()).equals("Y")) {
            appOpenAdManager = ((MyTrotApp) getApplication()).getAppOpenAdManager();

            LogUtil.d("AppOpenAdManager.showAdIfAvailable exec");
            appOpenAdManager.showAdIfAvailable(AtvIntro.this);

            if (appOpenAdManager != null && appOpenAdManager.isShowingAd == true) {
                appOpenAdManager.isShowingAd = false;
            }
        }


        if (FormatUtil.isNullorEmpty(token) || (today - token_save_date > 15)) {
            FirebaseMessaging.getInstance()
                    .getToken()
                    .addOnSuccessListener(AtvIntro.this, new OnSuccessListener<String>() {
                        @Override
                        public void onSuccess(String s) {
                            String newToken = s;
                            Log.e("fcm token : ", newToken);
                            SPUtil.getInstance().setRegistPushKey(getContext(), newToken);
                            SPUtil.getInstance().setRegistPushKeyDate(getContext(), today);
                        }
                    });


            FirebaseMessaging.getInstance().subscribeToTopic("mytrot_event")
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            String msg = "Subscribed";
                            if (!task.isSuccessful()) {
                                msg = "Subscribe failed";
                            }
                            LogUtil.d(TAG, msg);
                        }
                    });
        }

        if (Constants.DEBUG) {
            Alert.toastShort(getContext(), Constants.IS_REAL_SEVER ? "실서버" : "개발서버");
        }


        AdViewUtil.ready(AtvIntro.this, getContext(), true);



    }


    private void load_intro_ad() {
        LogUtil.d(TAG, "load_intro_ad, start");

        // Wait for 5 seconds
        CountDownTimer timer = new CountDownTimer(4500L, 1000L) {
            @Override
            public void onTick(long millisUntilFinished) {
                // Do nothing
                LogUtil.d(TAG, "onTick, millisUntilFinished : " + millisUntilFinished);
            }

            @Override
            public void onFinish() {

                LogUtil.d(TAG, "onTick, onFinish");

                LogUtil.d(TAG, "onTick, onFinish. isAdShown : " + isAdShown);
                LogUtil.d(TAG, "onTick, onFinish. isAdDismissed : " + isAdDismissed);

                if (appOpenAdManager != null) {
                    LogUtil.d(TAG, "onTick, onFinish. isShowingAd : " + appOpenAdManager.isShowingAd);
                    LogUtil.d(TAG, "onTick, onFinish. isLoadingAd : " + appOpenAdManager.isLoadingAd);
                }


                isLoadCompleted = true;

                if (appOpenAdManager != null && appOpenAdManager.isLoadingAd && SPUtil.getInstance().getUseIntroYn(getContext()).equals("Y")) {
                    appOpenAdManager.showAdIfAvailable(
                            AtvIntro.this,
                            new AppOpenAdManager.OnShowAdCompleteListener() {
                                @Override
                                public void onShowAdComplete() {
                                    gotoVote();
                                }
                            });
                } else {

                    if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {

                        if (Constants.DEBUG) {
                            LogUtil.d(TAG, "onTick, goActionAfterAdView");
                            Alert.toastLong(getContext(), "goActionAfterAdView");
                        }

                        AdViewUtil.goActionAfterAdView(AtvIntro.this, getContext(), true, true, new InterfaceSet.AdFullViewCompleteListener() {
                            @Override
                            public void onAfterAction() {
                                gotoVote();
                            }
                        });

                    } else {

                        LogUtil.d(TAG, "onTick, Handler gotoVote");

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                gotoVote();
                            }
                        }, 500);
                    }
                }
            }
        };
        timer.start();


        LogUtil.d(TAG, "load_intro_ad, end");
    }


    private void gotoVote() {

        LogUtil.d(TAG, "gotoVote");

        String user_enc = SPUtil.getInstance().getUserNoEnc(getContext());

        Intent intent = new Intent(getContext(), AtvVote.class);

        if (FormatUtil.isNullorEmpty(user_enc)) {
            intent = new Intent(getContext(), AtvLogin.class);
        }

//        // 퀴즈 화면으로 임시!!!
//        if(Constants.DEBUG) {
//            intent = new Intent(getContext(),AtvQuiz.class);
//        }

        intent.setAction("start_app");
        if (getIntent().getExtras() != null) {
            Bundle data = getIntent().getExtras().getBundle("push");
            if (data != null) {
                intent.putExtra("push", data);
                intent.setAction("start_from_push");
            }
        }
        startActivity(intent);
        finishAffinity();
    }


    private void goToNextDelay() {
        String date = SPUtil.getInstance().getAttendDate(getContext());
        String currentDate = FormatUtil.getCurrentDate();

        if (currentDate.equals(date)
                && Constants.DISPLAY_AD == true
                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {

            _isShowAd = true;
            int member_no = SPUtil.getInstance().getUserNo(getContext());
            LogUtil.d("goToNext, member_no : " + member_no);

            if (member_no > 0) {
                callApi_member_login_after(false);
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    load_intro_ad();
                }
            }, 500);

        } else {

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    goToNext();

                }
            }, 100);
        }
    }


    private void goToNext() {
        int member_no = SPUtil.getInstance().getUserNo(getContext());
        LogUtil.d("goToNext, member_no : " + member_no);
        if (member_no > 0) {
            callApi_member_login_after(true);
            return;
        }

        Intent i = new Intent();
        i.setClass(getContext(), AtvLogin.class);
        startActivity(i);
        finishAffinity();
    }

    private void callApi_common_check_app_version() {
        LogUtil.e("==========callApi_common_check_app_version : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.common_check_app_version(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.APP_TYPE_ID
                , CommonUtil.getCurrentVersion(getContext())
                , Constants.APP_DEVICE_ID
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);
                _isCallApiFailed = false;

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


                JSONObject app_data = JSONUtil.getJSONObject(json, "data");

                String new_app_version = JSONUtil.getString(app_data, "new_app_version");
                String new_must_update = JSONUtil.getString(app_data, "new_must_update");


                JSONObject config_info = JSONUtil.getJSONObject(app_data, "config_info");
                SPUtil.getInstance().setAppConfig(getContext(), config_info);

                if (!FormatUtil.isNullorEmpty(new_app_version) && !FormatUtil.isNullorEmpty(new_must_update)) {

                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            try {
                                if (which == Alert.BUTTON1) {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(Constants.MARKET_URL));
                                    startActivity(i);
                                    finishAffinity();
                                } else {
                                    goToNext();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                goToNext();
                            }
                        }
                    });

                    String msg = "플레이스토어에서 앱을 업데이트 받아주세요.";
                    if (new_must_update.equals("N")) {
                        msg += "\n\n업데이트 받으시겠습니까?";
                    }

                    if (new_must_update.equals("Y")) {
                        alert.showAlert(getContext(), msg);
                    } else {
                        alert.showAlert(getContext(), msg, false, "확인", "취소");
                    }
                    return;
                }

                goToNextDelay();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");

                _isCallApiFailed = true;
            }
        });
    }


    private void callApi_member_login_after(final boolean gotoMove) {
        LogUtil.e("==========callApi_member_login_after : start==========");

        LogUtil.e("callApi_member_login_after, gotoMove : " + gotoMove);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_login_after(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                Constants.APP_TYPE_ID,
                Constants.APP_DEVICE_ID,
                CommonUtil.getCurrentVersion(getContext()),
                SPUtil.getInstance().getRegistPushKey(getContext())
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


                LogUtil.d("AtvIntro------------------------------------------------------");
                LogUtil.intent(getIntent());
                LogUtil.d("------------------------------------------------------");

                if (gotoMove) {
                    gotoVote();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private boolean _isShowAd = false;

    @Override
    protected void onResume() {

        try {
            super.onResume();

            if (_isShowAd == false) {

                if (CommonUtil.isNetworkOn(getContext()) == false) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finishAffinity();
                        }
                    });
                    alert.showAlert(getContext(), "인터넷 연결을 확인해주세요.\n\nLTE 혹은 Wifi 를 사용해주세요.");
                }


                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        callApi_common_check_app_version();

                    }
                }, 100);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        LogUtil.d(TAG, "onDestroy");


        try {

            if (appOpenAdManager != null) {
                LogUtil.d(TAG, "onDestroy, appOpenAdManager.isShowingAd : " + appOpenAdManager.isShowingAd);
            }

            if (appOpenAdManager != null && appOpenAdManager.isShowingAd == false) {
                // 인트로 광고를 안봤어도 본걸로 칩니다.
                appOpenAdManager.isShowingAd = true;
            }
        } catch (Exception e) {

        }
    }

}
