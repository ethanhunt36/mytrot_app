package com.code404.mytrot.atv.more;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.bean.ImageBean;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.SelectPhotoManager;
import com.code404.mytrot.view.SquareImageView;
import com.google.gson.JsonObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import javax.xml.transform.Result;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AtvMoreReq extends AtvBase implements SelectPhotoManager.PhotoSelectCallback {

    private String _category = "1";

    private int _selectedPhotoIndex = -1;

    private EditText _edtContents = null;
    private Button _btnCancel = null;
    private Button _btnConfirm = null;
    private View _base_tip = null;
    private TextView _txtTip = null;
    private TextView _txtGotoSearch = null;

    private ImageBean[] _imgBeanArr = new ImageBean[]{new ImageBean(), new ImageBean(), new ImageBean()};

    private View _baseFiles = null;
    private SquareImageView _imgAttach01 = null;
    private SquareImageView _imgAttach02 = null;
    private SquareImageView _imgAttach03 = null;

    private SelectPhotoManager m_photoManger = null;


    @Override
    protected boolean getBundle() {

        _category = getIntent().getStringExtra(Constants.EXTRAS_TYPE);

        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_req);
    }


    @Override
    protected void init() {
        _btnTopBack.setVisibility(View.VISIBLE);
        _btnTopRight.setVisibility(View.GONE);
        _baseTopBottom.setVisibility(View.GONE);

        _txtGotoSearch.setText(Html.fromHtml("<b><u>검색 바로 가기</u></b>"));

        if (_category.equals("1")) {
            _txtTopTitle.setText("트롯 영상 추가해 주세요.");
            _baseFiles.setVisibility(View.GONE);
        } else {
            _txtTopTitle.setText("문의하기");
            _baseFiles.setVisibility(View.VISIBLE);
            _base_tip.setVisibility(View.GONE);

            _edtContents.setHint("문의하실 내용을 입력해 주세요.");

            _imgAttach01.setBackground(getDrawable(R.drawable._s_background_rounding));
            _imgAttach01.setClipToOutline(true);
            _imgAttach02.setBackground(getDrawable(R.drawable._s_background_rounding));
            _imgAttach02.setClipToOutline(true);
            _imgAttach03.setBackground(getDrawable(R.drawable._s_background_rounding));
            _imgAttach03.setClipToOutline(true);

            _imgBeanArr[0].setImageView(_imgAttach01);
            _imgBeanArr[1].setImageView(_imgAttach02);
            _imgBeanArr[2].setImageView(_imgAttach03);

            if (m_photoManger == null) {
                m_photoManger = new SelectPhotoManager(this);
                m_photoManger.setPhotoSelectCallback(this);
            }
        }



    }


    @Override
    protected void findView() {
        _edtContents = (EditText) findViewById(R.id.edtContents);
        _btnCancel = (Button) findViewById(R.id.btnCancel);
        _btnConfirm = (Button) findViewById(R.id.btnConfirm);
        _base_tip = findViewById(R.id.base_tip);
        _txtTip = findViewById(R.id.txtTip);
        _txtGotoSearch = findViewById(R.id.txtGotoSearch);

        _imgAttach01 = findViewById(R.id.imgAttach01);
        _imgAttach02 = findViewById(R.id.imgAttach02);
        _imgAttach03 = findViewById(R.id.imgAttach03);

        _baseFiles = findViewById(R.id.baseFiles);
    }


    @Override
    protected void configureListener() {

        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackCheck();
            }
        });

        _txtGotoSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contents = _edtContents.getText().toString();

                if (!FormatUtil.isNullorEmpty(contents)) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialogAlert, int which) {
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), "입력 중인 내용이 있습니다.\n\n검색 화면으로 이동하시겠습니까?\n\n입력 중인 내용은 삭제됩니다.", true, "이동", "취소");
                    return;
                }

                setResult(RESULT_OK);
                finish();

            }
        });

        _btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String contents = _edtContents.getText().toString();

                if (FormatUtil.isNullorEmpty(contents)) {
                    new Alert().showAlert(getContext(), "내용을 입력해 주세요.");
                    return;
                }

                File file01 = null, file02 = null, file03 = null;

                if(_category.equals("2")) {

                    String state01 = ImageBean.STATUS_STAY, state02 = ImageBean.STATUS_STAY, state03 = ImageBean.STATUS_STAY;
                    int i = 0;
                    for (ImageBean bean : _imgBeanArr) {
                        i++;
                        if (i == 1) {
                            file01 = bean.getFile();
                            state01 = bean.getStatus();
                        } else if (i == 2) {
                            file02 = bean.getFile();
                            state02 = bean.getStatus();
                        } else if (i == 3) {
                            file03 = bean.getFile();
                            state03 = bean.getStatus();
                        }
                    }

//                    if (_imgBeanArr[0].isExistsData() == false) {
//                        new Alert().showAlert(getContext(), "첫번째 사진을 등록해 주세요.");
//                        return;
//                    }

                    ArrayList<String> list = new ArrayList<String>();

                    for (ImageBean b : _imgBeanArr) {
                        if (b.getFile() != null && !FormatUtil.isNullorEmpty(b.getLocalUri())) {

                            if (!list.contains(b.getLocalUri())) {
                                list.add(b.getLocalUri());
                            } else {
                                new Alert().showAlert(getContext(), "중복되는 사진이 있습니다.\n동일한 사진은 1장만 등록해 주세요.");
                                return;
                            }
                        }
                    }
                }

                callApi_shuttle_put(contents, file01, file02, file03);
            }
        });

        _btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackCheck();
            }
        });

        _imgAttach01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoSelected(0);
            }
        });
        _imgAttach02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoSelected(1);
            }
        });
        _imgAttach03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoSelected(2);
            }
        });
    }


    private void onBackCheck(){
        String contents = _edtContents.getText().toString();

        if (!FormatUtil.isNullorEmpty(contents)) {
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialogAlert, int which) {
                    if (which == Alert.BUTTON1) {
                        finish();
                    }
                }
            });
            alert.showAlert(getContext(), "입력 중인 내용이 있습니다.\n\n취소 하시겠습니까?", true, "확인", "아니오, 계속 입력합니다.");
            return;
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        onBackCheck();
    }

    private void callApi_shuttle_put(String cont, File file01, File file02, File file03) {


        RequestBody ssid = RequestBody.create(MediaType.parse("text/plain"), "shuttle.put");
        RequestBody member_no = RequestBody.create(MediaType.parse("text/plain"), SPUtil.getInstance().getUserNoEnc(getContext()));
        RequestBody category = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_category));
        RequestBody conts = RequestBody.create(MediaType.parse("text/plain"), cont);

        MultipartBody.Part pic01 = null;
        MultipartBody.Part pic02 = null;
        MultipartBody.Part pic03 = null;

        if (file01 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file01);
            pic01 = MultipartBody.Part.createFormData("pic1", file01.getName(), requestFile);
        }
        if (file02 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file02);
            pic02 = MultipartBody.Part.createFormData("pic2", file02.getName(), requestFile);
        }
        if (file03 != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file03);
            pic03 = MultipartBody.Part.createFormData("pic3", file03.getName(), requestFile);
        }

        LogUtil.e("==========callApi_shuttle_put : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.shuttle_put(
                ssid
                , member_no
                , category
                , conts
                , pic01
                , pic02
                , pic03
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    if (_category.equals("1")) resultMessage = "트롯 영상 추가 요청이 접수 되었습니다.";
                    if (_category.equals("2"))
                        resultMessage = "문의 내용이 등록되었습니다.\n\n관리자가 확인 후 회신 드릴 예정입니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void onPhotoSelected(final int index) {

        _selectedPhotoIndex = index;

        System.out.println("onPhotoSelected, _selectedPhotoIndex : " + _selectedPhotoIndex);

        if (m_photoManger == null) {
            m_photoManger = new SelectPhotoManager(AtvMoreReq.this);
            m_photoManger.setPhotoSelectCallback(this);
        }

        String[] items = new String[]{
                "앨범에서 선택", "취소"
        };

        if (_imgBeanArr[index].isExistsData()) {
            items = new String[]{
                    "앨범에서 선택", "삭제", "취소"
            };
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
        adb.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    // 앨범에서 선택
                    m_photoManger.setIsCropFreestyle(true);
                    m_photoManger.doPickFromGallery();
                } else if (which == 1) {
                    // 삭제
                    _imgBeanArr[_selectedPhotoIndex].remove();
                }
            }
        });
        adb.show();
    }

    /**
     * 이미지 크랍 완료
     *
     * @param image
     * @param file
     */
    @Override
    public void onSelectImageDone(Bitmap image, File file) {

        LogUtil.d("onSelectImageDone");

        if (image != null && file != null) {
            BitmapFactory.Options onlyBoundsOptions2 = CommonUtil.getBitmapFactoryFromUri(getContext(), Uri.fromFile(file));
            LogUtil.d("options.outWidth : " + onlyBoundsOptions2.outWidth);
            LogUtil.d("options.outHeight : " + onlyBoundsOptions2.outHeight);

            int photo_min_size = 320;

            // 핸드폰 디스플레이 사이즈보다 작은 사진은 선택 못하도록 합니다.
            if (onlyBoundsOptions2.outWidth <= photo_min_size || onlyBoundsOptions2.outHeight <= photo_min_size) {
                Alert alert = new Alert();
                alert.showAlert(getContext(), getString(R.string.please_photo_width_01));
                return;
            }

            _imgBeanArr[_selectedPhotoIndex].setImageBitmap(image);
            _imgBeanArr[_selectedPhotoIndex].setFile(file);
        } else {
            _imgBeanArr[_selectedPhotoIndex].remove();
        }
    }

    @Override
    public void onFailedSelectImage(int errorCode, String err) {
        LogUtil.d("onFailedSelectImage");
    }

    @Override
    public void onDeleteImage() {
        LogUtil.d("onDeleteImage");
    }

    @Override
    public void onPermissionUpdated(int permission_type, boolean isGranted) {
        LogUtil.d("onPermissionUpdated");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);
        LogUtil.d("onActivityResult, data : " + data);
        LogUtil.intent("onActivityResult", data);


        if (resultCode == Activity.RESULT_OK) {

            // 갤러리에서 사진 선택 시.
            if (requestCode == SelectPhotoManager.PICK_FROM_FILE) {
                if (data.getData() == null) {
                    new Alert().showAlert(getContext(), "사진을 가져오는 동안 오류가 발생하였습니다. 앱을 재시작 후에 다시 시도해주세요.");
                } else {
                    if (_selectedPhotoIndex < 0) {
                        new Alert().showAlert(this, "사진을 다시 선택해주세요.");
                        return;
                    }

                    String str = data.getData().toString();


                    LogUtil.d("onActivityResult, str : " + str);

                    String url = CommonUtil.getRealPathFromURI(getContext(), data.getData());
                    LogUtil.d("onActivityResult, url : " + url);

                    String mimeType = CommonUtil.getMimeType(url);
                    LogUtil.d("onActivityResult, mimeType : " + mimeType);

                    if (!FormatUtil.isNullorEmpty(mimeType) && (!mimeType.toLowerCase().endsWith("jpeg") && !mimeType.toLowerCase().endsWith("jpg") && !mimeType.toLowerCase().endsWith("png"))) {
                        new Alert().showAlert(getContext(), "사진은 jpg, png 형태의 파일만 등록하실 수 있습니다.");
                        return;
                    }
                }
            }
        }

        if (requestCode == UCrop.REQUEST_CROP || requestCode == SelectPhotoManager.PICK_FROM_FILE) {
            // m_photoManger null 인 경우가 발생한다.
            if (m_photoManger == null) {
                m_photoManger = new SelectPhotoManager(this);
            }
            m_photoManger.setPhotoSelectCallback(this);
            m_photoManger.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        if (m_photoManger != null) {
            m_photoManger.removeTempFile(getContext());
        }
        super.onDestroy();
    }
}
