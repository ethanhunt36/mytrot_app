package com.code404.mytrot.atv.star_room;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.SelectPhotoManager;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.GMultiListView;
import com.code404.mytrot.view.SliderListView;
import com.code404.mytrot.widget.ItemPicArtist;
import com.google.gson.JsonObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


public class AtvStarRoomPic extends AtvBase implements GMultiListView.IMakeView, SelectPhotoManager.PhotoSelectCallback  {

    private GMultiListView _multiList = null;

    private View _header = null;
    private TextView _txtFilter = null;
    private TextView _txtSeqRecent = null;
    private TextView _txtSeqComment = null;
    private TextView _txtSeqLike = null;

    private View _base_help = null;
    private TextView _txtTip = null;

    private Button _btnWrite = null;

    private View _baseNoData = null;

    private String _artist_name = "";
    private int _artist_no = -1;
    private String _artist_board_id = "";
    private JSONArray _artist_pic_list = null;

    private SelectPhotoManager m_photoManger = null;
    private File _file = null;

    private String _member_no_enc = "";

    @Override
    protected boolean getBundle() {

        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);

        String str = getIntent().getStringExtra(Constants.EXTRAS_JSON_STRING);
        if (!FormatUtil.isNullorEmpty(str)) {
            _artist_pic_list = JSONUtil.createArray(str);
        }
        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_starroom_pic);
    }


    @Override
    protected void init() {
        _member_no_enc = SPUtil.getInstance().getUserNoEnc(getContext());


        _txtFilter.setVisibility(View.GONE);
        _txtSeqComment.setVisibility(View.GONE);

        _txtSeqLike.setText("인기순");

        _txtTopTitle.setText(_artist_name + " 프로필 사진");

        _baseTopBottom.setVisibility(View.GONE);

        _multiList.setRowItemCounts(3);
        _multiList.setViewMaker(R.layout.row_artist_pic, this);
        _multiList.setNoData(_baseNoData);

        if (_artist_pic_list != null) {
            _multiList.addItems(_artist_pic_list);
        }

        if (m_photoManger == null) {
            m_photoManger = new SelectPhotoManager(this);
            m_photoManger.setPhotoSelectCallback(this);
        }

        callApi_artist_artist_pic_list("recent");
    }


    @Override
    protected void findView() {
        _multiList = findViewById(R.id.multiList);
        _baseNoData = findViewById(R.id.baseNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_gallery, null);
        _multiList.addHeaderView(_header);

        _txtFilter = _header.findViewById(R.id.txtFilter);
        _txtSeqRecent = _header.findViewById(R.id.txtSeqRecent);
        _txtSeqComment = _header.findViewById(R.id.txtSeqComment);
        _txtSeqLike = _header.findViewById(R.id.txtSeqLike);

        _base_help = _header.findViewById(R.id.base_help);
        _txtTip = _header.findViewById(R.id.txtTip);

        _btnWrite = findViewById(R.id.btnWrite);
    }


    @Override
    protected void configureListener() {
        _btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_photoManger.doPickFromGallery();
            }
        });

        _txtSeqRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_artist_artist_pic_list("recent");
            }
        });

        _txtSeqLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_artist_artist_pic_list("like");
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public View makeView(GMultiListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final ArrayList<JSONObject> list = adapter.getItem(position);

        LogUtil.d("list.size() : " + list.size());

        JSONObject info01 = list.size() > 0 ? list.get(0) : null;
        JSONObject info02 = list.size() > 1 ? list.get(1) : null;
        JSONObject info03 = list.size() > 2 ? list.get(2) : null;

        ItemPicArtist img01 = convertView.findViewById(R.id.img01);
        ItemPicArtist img02 = convertView.findViewById(R.id.img02);
        ItemPicArtist img03 = convertView.findViewById(R.id.img03);

        setData(img01, info01);
        setData(img02, info02);
        setData(img03, info03);

        return convertView;
    }


    private void setData(ItemPicArtist item, JSONObject json){

        int artist_pic_no = JSONUtil.getInteger(json, "no");
        String member_no = JSONUtil.getString(json, "member_no");

        LogUtil.d("****************************************************");
        LogUtil.d("member_no : " + member_no);
        LogUtil.d("member_no_enc : " + _member_no_enc);

        item.setData(json);

        item.setVisibility(json != null ? View.VISIBLE : View.INVISIBLE);
        item.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            callApi_artist_artist_pic_del(pos);
                        }
                    }
                });
                alert.showAlert(getContext(), "삭제하시겠습니까?", true, "확인", "취소");
            }
        });
    }




    private void callApi_artist_artist_pic_del(int artist_pic_no) {
        LogUtil.e("==========callApi_artist_artist_pic_del : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_artist_pic_del(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
                , artist_pic_no
        );


        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                callApi_artist_artist_pic_list(_type);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

private String _type = "";

    private void callApi_artist_artist_pic_list(String type) {
        LogUtil.e("==========callApi_artist_artist_pic_like : start==========");

        if ("recent".equals(type)) {
            _txtSeqRecent.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
            _txtSeqLike.setBackground(null);
        } else {
            _txtSeqRecent.setBackground(null);
            _txtSeqLike.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        }

        _type = type;

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_artist_pic_list_recent(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
        );

        if (type.equals("like")) {
            call = apiInterface.artist_artist_pic_list_like(
                    SPUtil.getInstance().getUserNoEnc(getContext())
                    , _artist_no
            );
        }

        _multiList.removeAll();

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");

                _artist_pic_list = JSONUtil.getJSONArray(jsonData, "list");
                _multiList.addItems(_artist_pic_list);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }



    private void callApi_artist_artist_pic_ins() {

        LogUtil.e("==========callApi_artist_artist_pic_ins : start==========");

        RequestBody ssid = RequestBody.create(MediaType.parse("text/plain"), "artist.artist_pic_ins");
        RequestBody member_no = RequestBody.create(MediaType.parse("text/plain"), SPUtil.getInstance().getUserNoEnc(getContext()));

        MultipartBody.Part pic01 = null;
        RequestBody artist_no = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_artist_no));

        if (_file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), _file);
            pic01 = MultipartBody.Part.createFormData("pic1", _file.getName(), requestFile);
        }

        LogUtil.d("_artist_no : " + _artist_no);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_artist_pic_ins(
                ssid
                , member_no
                , artist_no
                , pic01
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "등록 되었습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        callApi_artist_artist_pic_list("recent");
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }



    @Override
    public void onSelectImageDone(Bitmap image, File file) {
        LogUtil.d("onSelectImageDone");

        if (image != null && file != null) {
            BitmapFactory.Options onlyBoundsOptions2 = CommonUtil.getBitmapFactoryFromUri(getContext(), Uri.fromFile(file));
            LogUtil.d("options.outWidth : " + onlyBoundsOptions2.outWidth);
            LogUtil.d("options.outHeight : " + onlyBoundsOptions2.outHeight);

            int photo_min_size = 320;

            // 핸드폰 디스플레이 사이즈보다 작은 사진은 선택 못하도록 합니다.
            if (onlyBoundsOptions2.outWidth <= photo_min_size || onlyBoundsOptions2.outHeight <= photo_min_size) {
                Alert alert = new Alert();
                alert.showAlert(getContext(), getString(R.string.please_photo_width_01));
                return;
            }


            //_imgAttach.setScaleType(ImageView.ScaleType.CENTER_CROP);
            //_imgAttach.setImageBitmap(image);

            _file = file;
            callApi_artist_artist_pic_ins();
        }
    }

    @Override
    public void onFailedSelectImage(int errorCode, String err) {
        LogUtil.d("onFailedSelectImage");
    }

    @Override
    public void onDeleteImage() {
        LogUtil.d("onDeleteImage");
    }

    @Override
    public void onPermissionUpdated(int permission_type, boolean isGranted) {
        LogUtil.d("onPermissionUpdated");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);
        LogUtil.d("onActivityResult, data : " + data);
        LogUtil.intent("onActivityResult", data);


        if (resultCode == Activity.RESULT_OK) {

            // 갤러리에서 사진 선택 시.
            if (requestCode == SelectPhotoManager.PICK_FROM_FILE) {
                if (data.getData() == null) {
                    new Alert().showAlert(getContext(), "사진을 가져오는 동안 오류가 발생하였습니다. 앱을 재시작 후에 다시 시도해주세요.");
                } else {
                    String str = data.getData().toString();


                    LogUtil.d("onActivityResult, str : " + str);

                    String url = CommonUtil.getRealPathFromURI(getContext(), data.getData());
                    LogUtil.d("onActivityResult, url : " + url);

                    String mimeType = CommonUtil.getMimeType(url);
                    LogUtil.d("onActivityResult, mimeType : " + mimeType);

                    if (!FormatUtil.isNullorEmpty(mimeType) && (!mimeType.toLowerCase().endsWith("jpeg") && !mimeType.toLowerCase().endsWith("jpg") && !mimeType.toLowerCase().endsWith("png"))) {
                        new Alert().showAlert(getContext(), "사진은 jpg, png 형태의 파일만 등록하실 수 있습니다.");
                        return;
                    }
                }
            }
        }

        if (requestCode == UCrop.REQUEST_CROP || requestCode == SelectPhotoManager.PICK_FROM_FILE) {
            m_photoManger.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        if (m_photoManger != null) {
            m_photoManger.removeTempFile(getContext());
        }
        super.onDestroy();
    }
}
