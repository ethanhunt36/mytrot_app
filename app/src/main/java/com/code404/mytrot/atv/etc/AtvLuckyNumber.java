package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.code404.mytrot.widget.ItemLuckyBall;
import com.code404.mytrot.widget.ItemNativeAd;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;

public class AtvLuckyNumber extends AtvBase implements GListView.IMakeView {

    private View _header = null;
    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;

    // header
    private TextView _txtDesc03 = null;
    private LinearLayout _baseLucky = null;
    private LinearLayout _baseLuckyBody = null;
    private TextView _txtDate = null;
    private LinearLayout _baseRow01 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num01 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num02 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num03 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num04 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num05 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num06 = null;
    private LinearLayout _baseRow02 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num07 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num08 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num09 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num10 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num11 = null;
    private com.code404.mytrot.widget.ItemLuckyBall _num12 = null;

    private ItemNativeAd _itemNativeAd = null;
    private ArrayList<ItemLuckyBall> _ballList = new ArrayList<ItemLuckyBall>();

    //

    @Override
    protected void setView() {
        setView(R.layout.atv_lucky_num);
    }

    @Override
    protected void findView() {
        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_lucky, null);

        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        _list.addHeaderView(_header);


        _baseLucky = (LinearLayout) _header.findViewById(R.id.baseLucky);
        _baseLuckyBody = (LinearLayout) _header.findViewById(R.id.baseLuckyBody);
        _txtDate = (TextView) _header.findViewById(R.id.txtDate);
        _baseRow01 = (LinearLayout) _header.findViewById(R.id.baseRow01);
        _num01 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num01);
        _num02 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num02);
        _num03 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num03);
        _num04 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num04);
        _num05 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num05);
        _num06 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num06);
        _baseRow02 = (LinearLayout) _header.findViewById(R.id.baseRow02);
        _num07 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num07);
        _num08 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num08);
        _num09 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num09);
        _num10 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num10);
        _num11 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num11);
        _num12 = (com.code404.mytrot.widget.ItemLuckyBall) _header.findViewById(R.id.num12);
        _txtDesc03 = _header.findViewById(R.id.txtDesc03);
        _itemNativeAd = _header.findViewById(R.id.itemNativeAd);

        _ballList.add(_num01);
        _ballList.add(_num02);
        _ballList.add(_num03);
        _ballList.add(_num04);
        _ballList.add(_num05);
        _ballList.add(_num06);
        _ballList.add(_num07);
        _ballList.add(_num08);
        _ballList.add(_num09);
        _ballList.add(_num10);
        _ballList.add(_num11);
        _ballList.add(_num12);


    }

    @Override
    protected void init() {
        _txtTopTitle.setText("행운의 번호");
        _baseTopBottom.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_lucky_num, this);
        _list.setNoData(_baseNoData);

        _baseLuckyBody.setBackgroundResource(R.drawable._s_btn_orange);
        _baseLucky.setVisibility(View.GONE);
        _itemNativeAd.setVisibility(View.VISIBLE);
        _itemNativeAd.initAd();
        _txtDesc03.setVisibility(View.GONE);
        callApi_luckyNumber_get_today_number_list();
    }


    @Override
    protected void configureListener() {


    }

    private void callApi_luckyNumber_get_today_number_list() {

        LogUtil.e("==========callApi_luckyNumber_get_today_number_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.luckyNumber_get_today_number_list(
                SPUtil.getInstance().getUserNoEnc(getContext())

        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list");
                JSONArray duplicate_list2 = JSONUtil.getJSONArray(data, "duplicate_list2");


                if (duplicate_list2 != null && duplicate_list2.length() >= 12) {

                    setDuplicateNumber(duplicate_list2);
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void setDuplicateNumber(JSONArray duplicate_list2) {

        for (int i = 0; i < duplicate_list2.length(); i++) {

            if (i >= 12) break;

            JSONObject json = JSONUtil.getJSONObject(duplicate_list2, i);

            _ballList.get(i).setData(json);
        }

        _txtDate.setText("중복 출현 번호");
        _txtDate.setVisibility(View.GONE);
        _baseLucky.setVisibility(View.VISIBLE);
        _itemNativeAd.setVisibility(View.VISIBLE);
        _txtDesc03.setVisibility(View.VISIBLE);
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        LinearLayout adViewBody = (LinearLayout) convertView.findViewById(R.id.adViewBody);

        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);

        com.code404.mytrot.widget.ItemLuckyBall num01 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num01);
        com.code404.mytrot.widget.ItemLuckyBall num02 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num02);
        com.code404.mytrot.widget.ItemLuckyBall num03 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num03);
        com.code404.mytrot.widget.ItemLuckyBall num04 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num04);
        com.code404.mytrot.widget.ItemLuckyBall num05 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num05);
        com.code404.mytrot.widget.ItemLuckyBall num06 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num06);
        com.code404.mytrot.widget.ItemLuckyBall num07 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num07);
        com.code404.mytrot.widget.ItemLuckyBall num08 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num08);
        com.code404.mytrot.widget.ItemLuckyBall num09 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num09);
        com.code404.mytrot.widget.ItemLuckyBall num10 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num10);
        com.code404.mytrot.widget.ItemLuckyBall num11 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num11);
        com.code404.mytrot.widget.ItemLuckyBall num12 = (com.code404.mytrot.widget.ItemLuckyBall) convertView.findViewById(R.id.num12);

        String dates = JSONUtil.getString(json, "dates");
        String dateDay = FormatUtil.getDateDay(dates, "yyyy-MM-dd");
        txtDate.setText(String.format("%s(%s)", dates, dateDay));

        num01.setData(JSONUtil.getInteger(json, "num01"));
        num02.setData(JSONUtil.getInteger(json, "num02"));
        num03.setData(JSONUtil.getInteger(json, "num03"));
        num04.setData(JSONUtil.getInteger(json, "num04"));
        num05.setData(JSONUtil.getInteger(json, "num05"));
        num06.setData(JSONUtil.getInteger(json, "num06"));
        num07.setData(JSONUtil.getInteger(json, "num07"));
        num08.setData(JSONUtil.getInteger(json, "num08"));
        num09.setData(JSONUtil.getInteger(json, "num09"));
        num10.setData(JSONUtil.getInteger(json, "num10"));
        num11.setData(JSONUtil.getInteger(json, "num11"));
        num12.setData(JSONUtil.getInteger(json, "num12"));

        return convertView;
    }
}
