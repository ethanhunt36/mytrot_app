package com.code404.mytrot.atv.more;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.AtvLogin;
import com.code404.mytrot.atv.etc.AtvCertification;
import com.code404.mytrot.atv.etc.AtvPayment;
import com.code404.mytrot.atv.etc.AtvPuzzleRank;
import com.code404.mytrot.atv.etc.AtvQuiz;
import com.code404.mytrot.atv.etc.AtvTrade;
import com.code404.mytrot.atv.like.AtvLike;
import com.code404.mytrot.atv.star_room.AtvStarRoomMain;
import com.code404.mytrot.atv.web.AtvWeb;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.CodeUtil;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.widget.ItemMyVph;
//import com.fsn.cauly.CaulyAdView;
//import com.fsn.cauly.CaulyAdView;
import com.google.android.gms.ads.AdRequest;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;


public class AtvMore extends AtvBase {


    private static int _REQ_SHUTTLE = 9922;


    private LinearLayout _baseUserInfo = null;
    private TextView _txtNick = null;
    private TextView _txtPhone = null;
    private TextView _txtPlayCount = null;
    private TextView _txtPlayTime = null;
    private TextView _txtEmail = null;
    private TextView _txtArtist = null;

    private View _baseCharge = null;
    private View _baseCerti = null;
    private View _baseBlog = null;
    private View _baseInputRcmd = null;
    private View _baseFavorite = null;


    //    private RelativeLayout _baseReview = null;
    private RelativeLayout _baseShare = null;

    private Button _btnChangeNick = null;
    private Button _btnArtist = null;
    private ImageView _imgSns = null;

    //    private TextView _txtHintReview = null;
    private TextView _txtHintShare = null;
    private TextView _txtHintShare02 = null;

    //    private TextView _txtVoteCount = null;
    private TextView _txtVotedCount = null;
    private TextView _txtBoardCount = null;
    private TextView _txtLikePutCount = null;
    private TextView _txtLikeGutCount = null;

    //    private TextView _txtPoint = null;
    private TextView _txtPointGet = null;
    private TextView _txtPointUse = null;

    private TextView _txtAutoTicketName = null;
    private TextView _txtAutoTicketDesc = null;
    private TextView _txtHintPromo = null;

//    private int cnt_addVoteTicketReview = -1;
//    private int cnt_addVoteTicketShare = -1;


    private View _baseReqVod = null;
    private TextView _txtReqVodQuestion = null;
    private View _baseMore = null;
    private TextView _txtMore = null;
    private TextView _txtNoticeNew = null;
    //    private View _baseNoAd = null;
    private TextView _txtRemoveAd = null;

    private RelativeLayout _baseAd = null;
    private ImageView _img_ad = null;
    private TextView _txtAdDesc01 = null;
    private TextView _txtAdDesc02 = null;

    private RelativeLayout _basePay = null;
    private ImageView _img_trade = null;
    private TextView _txtPayDesc01 = null;
    private TextView _txtPayDesc02 = null;

    private View _baseTrade = null;

    private WebView _webView = null;
    //    private View _baseEventPP = null;
//    private TextView _txtRemainTimePP = null;
//    private TextView _txtRemainCountPP = null;
    private TextView _txtVersion = null;


    private TextView _txtLevel = null;
    private TextView _txtExpStart = null;
    private TextView _txtExpEnd = null;
    private TextView _txtWidraw = null;
    private TextView _txtLogout = null;

    private View _baseStarRoom = null;
    private TextView _txtStarRoom = null;
    private RelativeLayout _base_ad_major = null;

    private ItemMyVph _item_my_vph = null;


    private int _order_no = -1;
    private JSONObject _productInfo = null;


    private View _baseAdView = null;
    private com.google.android.gms.ads.AdView _adView = null;
//    private CaulyAdView _adViewCauly = null;
    private int _price = -1;
    private String _sku = "";


    private int _vote_cnt = 0;
    private int _amount_ticket_ad = 1;
    private double _point_amount = 1;

    private int _heart_recommend = -1;
    private int _heart_recommend_rcv = -1;

    @Override
    protected void setView() {
        setView(R.layout.atv_more);
    }


    @Override
    protected void init() {

        _adClickTime = SPUtil.getInstance().getClickAdView(getContext());

        _menuIndex = 4;
        _canFinish = true;
        _txtTopTitle.setText("더보기");

        _txtAdDesc01.setText("동영상 광고");
        //_txtPayDesc01.setText("유료 충전소");
        _txtWidraw.setText(Html.fromHtml("<u> 탈퇴하기 </u>"));
        _txtLogout.setText(Html.fromHtml("<u> 로그아웃 </u>"));

        _btnTopBack.setVisibility(View.GONE);

        _baseMore.setVisibility(View.GONE);

//        _adViewCauly.reload();

        JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
        int lv = JSONUtil.getInteger(jsonUser, "lv", 0);

        _baseUserInfo.setVisibility(View.VISIBLE);
        setDataBinding();


//        if (!SPUtil.getInstance().getIsLogin(getContext())) {
//            _baseLogin.setVisibility(View.VISIBLE);
//            _baseUserInfo.setVisibility(View.GONE);
//            setup_sns_environment();
//        } else {
//            //_baseLogin.setVisibility(View.GONE);
//            _baseLogin.setVisibility(View.VISIBLE);
//            _baseUserInfo.setVisibility(View.VISIBLE);
//            setup_sns_environment();
//            setDataBinding();
//        }


        JSONObject json = SPUtil.getInstance().getAdRemoveInfo(getContext());

        String sku = JSONUtil.getString(json, "sku");
        String price = JSONUtil.getString(json, "price");

        LogUtil.d("sku : " + sku);
        LogUtil.d("price : " + price);

        // 서버에서 광고 오픈 여부에 따라 투표권 지급 문구 show
        if (SPUtil.getInstance().getReviewRewardYn(getContext()).equals("Y")) {
            //callApi_logClick_my_action_info(Constants.LOG_CLICK_REVIEW);
            //callApi_logClick_my_action_info(Constants.LOG_CLICK_SHARE);
        } else {
            //_txtHintReview.setVisibility(View.GONE);
            _txtHintShare.setVisibility(View.GONE);
        }


        if (
//                Constants.DEBUG == false
//                &&
                (!SPUtil.getInstance().getUsePromotionYn(getContext()).equals("Y")
                        || !SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
                        || lv < SPUtil.getInstance().getAvailableBoardLevel(getContext())
                )
        ) {
            findViewById(R.id.basePromo).setVisibility(View.GONE);
        }


        //
        JSONObject boardPromoInfo = SPUtil.getInstance().getBoardPromoInfo(getContext());
        int reward_vote = JSONUtil.getInteger(boardPromoInfo, "reward_vote", -1);
        int reward_point = JSONUtil.getInteger(boardPromoInfo, "reward_point", -1);

        if (reward_vote > 0 && reward_point > 0) {
            String txt = String.format("인증샷을 등록하시면<br>투표권 <font color='red'>%d장</font>, 포인트 <font color='red'>%dP</font>를 드려요", reward_vote, reward_point);
            _txtHintPromo.setText(Html.fromHtml(txt));
        } else if (reward_vote > 0 && reward_point == 0) {
            String txt = String.format("인증샷을 등록하시면<br>투표권 <font color='red'>%d장</font>을 드려요", reward_vote);
            _txtHintPromo.setText(Html.fromHtml(txt));
        } else {
            findViewById(R.id.basePromo).setVisibility(View.GONE);
            _txtHintPromo.setVisibility(View.GONE);
        }


        String login_type = JSONUtil.getString(jsonUser, "login_type");
        String phone = JSONUtil.getString(jsonUser, "phone");
        String put_rcmd_code = JSONUtil.getString(jsonUser, "put_rcmd_code");
        int reg_days = JSONUtil.getInteger(jsonUser, "reg_days");

        _baseInputRcmd.setVisibility(reg_days <= 15 && FormatUtil.isNullorEmpty(put_rcmd_code) ? View.VISIBLE : View.GONE);

        //
        int enterMoreCount = SPUtil.getInstance().getEnterMoreCount(getContext());

        String extra_type = getIntent().getStringExtra(Constants.EXTRAS_TYPE);

        if (!FormatUtil.isNullorEmpty(extra_type) && extra_type.equals("GET_CERTI")) {
            onAuthCertification();
        } else if (!Constants.APP_DEVICE_ID.equals("MYTROT") && enterMoreCount % 10 == 1 && FormatUtil.isNullorEmpty(phone) && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            String msg = "핸드폰 번호를 인증 하시겠습니까?\n\n인증을 해두시면 나중에 재설치 시 핸드폰 번호로 기존 정보를 불러올 수 있습니다.";
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    if (which == Alert.BUTTON1) {
                        AlertPop alert = new AlertPop();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                callApi_member_get_by_no(false);
                            }
                        });
                        alert.showAuthPhone(getContext());
                    }
                }
            });
            alert.showAlert(getContext(), msg, false, "확인", "취소");
        } else if (enterMoreCount % 10 == 1 && reg_days <= 15 && FormatUtil.isNullorEmpty(put_rcmd_code) && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            String msg = "추천인 코드를 입력하지 않았습니다.\n\n지인에게 받은 추천인코드가 있다면 입력해주세요.";
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    if (which == Alert.BUTTON1) {
                        onClickRcmd();
                    }
                }
            });
            alert.showAlert(getContext(), msg, false, "입력하기", "취소");
        }

        enterMoreCount++;
        SPUtil.getInstance().setEnterMoreCount(getContext(), enterMoreCount);

        callApi_memberItem_get_all();

        _baseAd.setVisibility(View.GONE);


        _amount_ticket_ad = SPUtil.getInstance().getAdTicketAmount(getContext());
        _point_amount = SPUtil.getInstance().getAdPointAmount(getContext());


        if (enterMoreCount % 2 == 0 && SPUtil.getInstance().getUseCoupangYn(getContext()).equals("Y")) {
            String adUrl = CodeUtil.getCoupangUrl();
            String html = "<html><center>[AD]</center></html>";
            html = html.replace("[AD]", adUrl);

            _webView.loadData(html, "text/html", "utf-8");
            _webView.setVisibility(View.VISIBLE);
//            _adViewCauly.setVisibility(View.GONE);
        } else {
//            _adViewCauly.setVisibility(View.VISIBLE);
            _webView.setVisibility(View.GONE);
        }

        _txtVersion.setText(CommonUtil.getCurrentVersion(getContext()));

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _item_my_vph.setVisibility(View.GONE);
//            _baseReview.setVisibility(View.GONE);
            _baseShare.setVisibility(View.GONE);
//            _adViewCauly.setVisibility(View.GONE);
            _webView.setVisibility(View.GONE);
            _baseFavorite.setVisibility(View.GONE);
            _baseCerti.setVisibility(View.GONE);
            _txtReqVodQuestion.setText("문의하기");

            //_baseCharge.setVisibility(View.GONE);
            _baseInputRcmd.setVisibility(View.GONE);

            TextView txtTradeDesc01_01 = findViewById(R.id.txtTradeDesc01_01);
            TextView txtTradeDesc02 = findViewById(R.id.txtTradeDesc02);
            TextView txtChargeDesc02 = findViewById(R.id.txtChargeDesc02);

            txtTradeDesc01_01.setText("(포인트, 투표권, 하트)");
            txtTradeDesc02.setText("포인트를 투표권 or 하트로 교환할 수 있어요.");

        }

        if (Constants.IS_ONE_STORE) {
            _basePay.setVisibility(View.GONE);
        }


        if (!Constants.APP_DEVICE_ID.equals("MYTROT")) {
            _txtLogout.setVisibility(View.GONE);
        }



    }


    @Override
    protected void findView() {
        _baseMore = findViewById(R.id.baseUserMore);
        _txtMore = findViewById(R.id.txtMore);

        _txtNick = findViewById(R.id.txtNick);
        _imgSns = findViewById(R.id.imgSns);
        _txtPhone = findViewById(R.id.txtPhone);
        _btnChangeNick = findViewById(R.id.btnChangeNick);
        _btnArtist = findViewById(R.id.btnArtist);

        _baseUserInfo = (LinearLayout) findViewById(R.id.baseUserInfo);
        _txtPlayCount = (TextView) findViewById(R.id.txtPlayCount);
        _txtPlayTime = (TextView) findViewById(R.id.txtPlayTime);
        _txtEmail = (TextView) findViewById(R.id.txtEmail);
        _txtArtist = (TextView) findViewById(R.id.txtArtist);
        //_btnLogout = (Button) findViewById(R.id.btnLogout);
//        _baseReview = (RelativeLayout) findViewById(R.id.baseReview);
        _baseShare = (RelativeLayout) findViewById(R.id.baseShare);
        _baseReqVod = findViewById(R.id.baseReqVod);
        _txtReqVodQuestion = findViewById(R.id.txtReqVodQuestion);
        _baseAdView = findViewById(R.id.baseAdView);
        _adView = findViewById(R.id.adView);
//        _adViewCauly = findViewById(R.id.adViewCauly);


//        _txtHintReview = findViewById(R.id.txtHintReview);
        _txtHintShare = findViewById(R.id.txtHintShare);
        _txtHintShare02 = findViewById(R.id.txtHintShare02);

//        _txtVoteCount = (TextView) findViewById(R.id.txtVoteCount);
        _txtVotedCount = (TextView) findViewById(R.id.txtVotedCount);
        _txtBoardCount = (TextView) findViewById(R.id.txtBoardCount);
        _txtLikePutCount = (TextView) findViewById(R.id.txtLikePutCount);
        _txtLikeGutCount = (TextView) findViewById(R.id.txtLikeGutCount);

//        _txtPoint = findViewById(R.id.txtPoint);
        _txtPointGet = findViewById(R.id.txtPointGet);
        _txtPointUse = findViewById(R.id.txtPointUse);

        _txtNoticeNew = findViewById(R.id.txtNoticeNew);


        _txtHintPromo = findViewById(R.id.txtHintPromo);

        _baseCerti = findViewById(R.id.baseCerti);
        _baseBlog = findViewById(R.id.baseBlog);

        _baseAd = (RelativeLayout) findViewById(R.id.baseAd);
        _img_ad = (ImageView) findViewById(R.id.img_ad);
        _txtAdDesc01 = (TextView) findViewById(R.id.txtAdDesc01);
        _txtAdDesc02 = (TextView) findViewById(R.id.txtAdDesc02);

        _basePay = (RelativeLayout) findViewById(R.id.basePay);
        _img_trade = (ImageView) findViewById(R.id.img_trade);
        _txtPayDesc01 = (TextView) findViewById(R.id.txtPayDesc01);
        _txtPayDesc02 = (TextView) findViewById(R.id.txtPayDesc02);

        _baseCharge = findViewById(R.id.baseCharge);
        _baseTrade = findViewById(R.id.baseTrade);

        _baseInputRcmd = findViewById(R.id.baseInputRcmd);

        _webView = findViewById(R.id.webView);
//        _baseEventPP = findViewById(R.id.baseEventPP);
//        _txtRemainTimePP = findViewById(R.id.txtRemainTimePP);
//        _txtRemainCountPP = findViewById(R.id.txtRemainCountPP);

        _txtVersion = findViewById(R.id.txtVersion);

        _txtLevel = findViewById(R.id.txtLevel);
        _txtExpStart = findViewById(R.id.txtExpStart);
        _txtExpEnd = findViewById(R.id.txtExpEnd);

        _baseStarRoom = findViewById(R.id.baseStarRoom);
        _txtStarRoom = findViewById(R.id.txtStarRoom);

        _txtWidraw = findViewById(R.id.txtWidraw);
        _txtLogout = findViewById(R.id.txtLogout);
        _base_ad_major = findViewById(R.id.base_ad_major);


        _item_my_vph = findViewById(R.id.item_my_vph);
        _baseFavorite = findViewById(R.id.baseFavorite);


    }


    private void showArtistPop() {

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
        String login_type = JSONUtil.getString(json, "login_type");
        String phone = JSONUtil.getString(json, "phone");

        if (login_type.equals("A") && FormatUtil.isNullorEmpty(phone)) {
            new Alert().showAlert(getContext(), "핸드폰 인증 혹은 네이버/카카오톡 연결 후에 이용해주세요.");
            return;
        }

        AlertPop alert = new AlertPop();
        alert.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
            @Override
            public void onClick(View v, int pos, JSONObject json) {
                if (json != null) {
                    int artist_no = JSONUtil.getInteger(json, "no");
                    String name = JSONUtil.getString(json, "name");


                    callApi_member_update_favorite_artist(artist_no, name);

                    // 최애 맨위로 체크
                    SPUtil.getInstance().setCheckedFavoriteVote(getContext(), true);
                    SPUtil.getInstance().setCheckedFavoriteDonate(getContext(), true);
                    SPUtil.getInstance().setCheckedFavoritePopupArtist(getContext(), true);
                }
            }
        });
        alert.showVoteArtist(getContext(), true, "", false);
    }

    private long _adClickTime = -1;


    @Override
    protected void configureListener() {


        _baseInputRcmd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickRcmd();
            }
        });



//        _baseEventPP.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onGotoPPomkki();
//            }
//        });

        _txtLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            SPUtil.getInstance().logout(getContext());
                            Intent i = new Intent();
                            i.setClass(getContext(), AtvLogin.class);
                            startActivity(i);
                            finishAffinity();
                        }
                    }
                });
                alert.showAlert(getContext(), "로그아웃 하시겠습니까?", true, "확인", "취소");
            }
        });

        _txtWidraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "마이트롯을 사용하시면서 불편하셨던 내용은 문의하기를 통해 글을 남겨주세요.\n불편해소를 위해 운영팀이 최대한 지원하겠습니다.\n\n탈퇴하시겠습니까?";

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            String msg = "탈퇴를 하시면 모든 데이터가 삭제됩니다.\n\n투표권/포인트를 모두 사용하셨는지 확인해주세요.\n\n정말 탈퇴하시겠습니까?";

                            Alert alert = new Alert();
                            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                @Override
                                public void onClose(DialogInterface dialog, int which) {
                                    if (which == Alert.BUTTON1) {
                                        callApi_member_out();
                                    }
                                }
                            });
                            alert.showAlert(getContext(), msg, true, "탈퇴하기", "취소");
                        }
                    }
                });
                alert.showAlert(getContext(), msg, true, "확인", "취소");
            }
        });

        _baseStarRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

                int artist_no = JSONUtil.getInteger(json, "artist_no");
                String artist_name = JSONUtil.getString(json, "artist_name");
                String artist_board_id = JSONUtil.getString(json, "artist_board_id");

                if (artist_no < 1) {
                    new Alert().showAlert(getContext(), getString(R.string.txt_set_artist));
                    return;
                }

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
                        i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
                        i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, artist_board_id);
                        i.setClass(getContext(), AtvStarRoomMain.class);
                        startActivity(i);
                    }
                });


            }
        });


        // 무료충전소
        _baseCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvMoreChargeB.class);
                i.putExtra(Constants.EXTRAS_AD_TICKET, _amount_ticket_ad);
                i.putExtra(Constants.EXTRAS_AD_POINT, _point_amount);
                startActivity(i);

//                if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null && _adFullOrgManager.canShowSimpleAdCount() > 0) {
//                    _adFullOrgManager.showSimpleAd();
//                }
                return;
            }
        });


        _baseCerti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        onAuthCertification();
                    }
                });
            }
        });


        _baseBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvWeb.class);
                i.putExtra(Constants.EXTRAS_TYPE, "blog");
                startActivity(i);

                callApi_memberMission_check_mission_view_blog();
            }
        });

        _txtMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _baseMore.setVisibility(_baseMore.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);
                _txtMore.setText(_baseMore.getVisibility() == View.GONE ? "더보기" : "닫기");
            }
        });

        _btnArtist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int a_no = SPUtil.getInstance().getUserFavoriteArtistNo(getContext());

                if (a_no > 0) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (which == Alert.BUTTON1) {
                                callApi_member_update_favorite_artist(-1, "");
                            } else {


                                showArtistPop();
                            }
                        }
                    });
                    alert.showAlert(getContext(), "최애 가수를 초기화 하시겠습니까?", true, "확인", "취소");
                } else {


                    showArtistPop();
                }
            }
        });

        _basePay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvPayment.class);
                startActivity(i);
            }
        });

        _baseTrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvTrade.class);
                startActivity(i);
            }
        });

//        _baseReview.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (cnt_addVoteTicketReview < Constants.ADD_TICKET_REVIEW) {
//                    callApi_vote_put_ticket_ad(Constants.TICKET_REVIEW_COUNT);
//                    cnt_addVoteTicketReview++;
//                }
//
//                callApi_logClick_add_log(Constants.LOG_CLICK_REVIEW_01);
//
//                openReviewStoreOrApi(false);
//            }
//        });


        _baseShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (cnt_addVoteTicketShare < Constants.ADD_TICKET_SHARE) {
//                    callApi_vote_put_ticket_ad(Constants.TICKET_SHARE_COUNT);
//                    cnt_addVoteTicketShare++;
//                }

                callApi_logClick_add_log(Constants.LOG_CLICK_SHARE);

                String rcmd_code = SPUtil.getInstance().getRcmdCode(getContext());

                String msg00 = "트로트가 매일 추가되는 마이트롯!\\n\\n평생 완전 무료!\\n\\n가장 좋아하는 가수에게 투표하고, 기부도 참여해요!"
                        + "\n\n" +
                        "앱 설치 후 추천인코드 입력시 %dP 지급!!\n" +
                        "\n다른 사람을 초대하면 추가 %dP 지급!!";

                String msg01 = "친구야! 이거 대박이다. " +
                        "\n\n" +
                        "이거 함 설치해봐라. 좋아하는 가수 노래도 듣고, 투표하고 기부도 참여할 수 있다." +
                        "\n\n" +
                        "네가 가입하고 지금 주는 추천인 코드 입력하면 넌 %dP를 받고, 난 소개비 명목으로 %dP를 준데!!" +
                        "\n\n" +
                        "얼른 설치해라. ";

                String msg02 = "마이트롯!\n" +
                        "\n" +
                        "노래듣고 응원하며\n내 가수 기부천사 만들기!" +
                        "\n\n" +
                        "앱 설치 후 추천인코드 입력시 %dP 지급!!\n" +
                        "\n다른 사람을 초대하면 추가 %dP 지급!!";

                String msg03 = "트로트 좋아하는 사람에게 딱!!\n" +
                        "\n" +
                        "드디어 찾았다!! \n\n" +
                        "앱 설치 후 추천인 코드만 입력하면 %dP!\n" +
                        "다른 분께 소개하면 추가 %dP!";

                String[] msgArr = new String[]{msg00, msg01, msg02, msg03};
                int r = new Random().nextInt(msgArr.length);
                LogUtil.d("r : " + r);
                if (r >= msgArr.length) r = msgArr.length - 1;
                if (r < 0) r = 0;
                String msg = msgArr[r];

                msg = msg +
                        "\n\n" +
                        "단 추천인코드를 반드시 입력 바랍니다." +
                        "\n\n" +
                        "추천인코드 : %s" +
                        "\n\n" +
                        "다운로드 : %s";

                final String text = String.format(msg, _heart_recommend, _heart_recommend_rcv, rcmd_code, Constants.MARKET_WEB_URL);

                String alertMsg = "추천인 코드와 간단한 소개 문구가\n클립보드에 복사되었습니다.\n\nSMS, 카카오톡 등을 통해서 지인에게 전달해주세요.\n\n초대받은 친구가 '마이트롯'에 가입하면\n두분 모두에게 포인트를 드립니다.\n\n(친구 : %dP, 본인 : %dP)";
                alertMsg = String.format(alertMsg, _heart_recommend, _heart_recommend_rcv);

                CommonUtil.copyClipboard(getContext(), text);

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {

                        if (which == Alert.BUTTON1) {
                            try {
                                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                                intent.setType("text/plain");
                                intent.putExtra(Intent.EXTRA_TEXT, text);
                                Intent chooser = Intent.createChooser(intent, "친구에게 공유하기");
                                startActivity(chooser);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                alert.showAlert(getContext(), alertMsg);

            }
        });


        _baseReqVod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SPUtil.getInstance().getIsLogin(getContext()) == false) {
                    new Alert().showAlert(getContext(), "로그인 후에 이용 하실 수 있습니다.");
                    return;
                }


                if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                    Intent i = new Intent();
                    i.setClass(getContext(), AtvMoreReq.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "2");
                    startActivity(i);

                    return;
                }


                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Alert alert = new Alert();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    Intent i = new Intent();
                                    i.setClass(getContext(), AtvMoreReq.class);
                                    i.putExtra(Constants.EXTRAS_TYPE, "1");
                                    startActivityForResult(i, _REQ_SHUTTLE);
                                } else if (which == 1) {
                                    Intent i = new Intent();
                                    i.setClass(getContext(), AtvMoreReq.class);
                                    i.putExtra(Constants.EXTRAS_TYPE, "2");
                                    startActivity(i);
                                }
                            }
                        });
                        alert.showSeletItem(getContext(), new String[]{"영상 추가 요청", "문의하기"});
                    }
                });

            }
        });

        // 닉네임 변경
        _btnChangeNick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertPop alert = new AlertPop();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        setDataBinding();
                    }
                });
                alert.showChangeNick(getContext());
            }
        });

        findViewById(R.id.baseNotice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreNotice.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "notice");
                        startActivity(i);
                    }
                });
            }
        });

        findViewById(R.id.baseFaq).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreNotice.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "faq");
                        startActivity(i);
                    }
                });
            }
        });

//        // 다른 앱 더보기
//        findViewById(R.id.baseMoreApp).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                callApi_logClick_add_log(Constants.LOG_CLICK_MORE_APP);
//
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse("https://play.google.com/store/search?q=pub:code404&c=apps"));
//                startActivity(i);
//            }
//        });

        // 영상 보관함
        _baseFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvLike.class);
                        startActivity(i);
                    }
                });
            }
        });

        // 이용약관
        findViewById(R.id.baseAgree).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvWeb.class);
                i.putExtra(Constants.EXTRAS_TYPE, "agree");
                startActivity(i);
            }
        });

        // 레벨 순위
        findViewById(R.id.baseLevel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreLevel.class);
                        startActivity(i);
                    }
                });
            }
        });

        // 투표권 적립/사용 내역
        findViewById(R.id.baseVoteHistory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreVote.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "get");
                        startActivity(i);
                    }
                });
            }
        });

        // 포인트 적립/기부 내역
        findViewById(R.id.basePointHistory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMorePoint.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "get");
                        startActivity(i);
                    }
                });
            }
        });


        // 투표권 순위
        findViewById(R.id.baseVoteRank).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreRankVote.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "use");
                        startActivity(i);
                    }
                });
            }
        });

        // 포인트 순위
        findViewById(R.id.basePointRank).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreRank.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "use");
                        startActivity(i);
                    }
                });
            }
        });


        // 하트 적립/기부 내역
        findViewById(R.id.baseHeartHistory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreHeart.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "get");
                        startActivity(i);
                    }
                });
            }
        });


        // 하트 순위
        findViewById(R.id.baseHeartRank).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreRankHeart.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "use");
                        startActivity(i);
                    }
                });
            }
        });


        // 핸드폰 인증
        findViewById(R.id.btnAuth).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertPop alert = new AlertPop();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        callApi_member_get_by_no(false);
                    }
                });
                alert.showAuthPhone(getContext());
            }
        });

        // 홍보 프로모션
        findViewById(R.id.basePromo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMorePromo.class);
                        startActivity(i);
                    }
                });
            }
        });

//        `// 버전 정보
//        findViewById(R.id.baseVersion).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
//                    gotoStore();
//                }
//            }
//        });`

        // 알림 설정
        findViewById(R.id.baseSettingAlram).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMoreAlramSet.class);
                        startActivity(i);
                    }
                });

            }
        });

        // 퍼즐 게임
        findViewById(R.id.basePuzzle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String artist_name = SPUtil.getInstance().getUserFavoriteArtistName(getContext());

                if (FormatUtil.isNullorEmpty(artist_name)) {
                    new Alert().showAlert(getContext(), getString(R.string.txt_set_artist));
                    return;
                }

                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvWeb.class);
                        i.putExtra(Constants.EXTRAS_TYPE, "puzzle");
                        startActivity(i);
                    }
                });

            }
        });

        // 퍼즐 게임 순위
        findViewById(R.id.basePuzzleRank).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AdViewUtil.goActionAfterAdView(AtvMore.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvPuzzleRank.class);
                        startActivity(i);
                    }
                });
            }
        });
    }


    @Override
    public void onPause() {
        super.onPause();

        LogUtil.d("AtvMore, onPause");
        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }
        if (_timerAd != null) {
            _timerAd.cancel();
            _timerAd = null;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.d("AtvMore, onResume");
        try {
            callApi_member_get_by_no(false);

            if (_txtNoticeNew != null) {
                if (SPUtil.getInstance().getLastestNoticeNo(getContext()) > SPUtil.getInstance().getLastestNoticeNoViewed(getContext())) {
                    _txtNoticeNew.setVisibility(View.VISIBLE);
                } else {
                    _txtNoticeNew.setVisibility(View.GONE);
                }
            }

            AdViewUtil.ready(AtvMore.this, getContext());
        } catch (Exception e) {

        }
    }


    // 마이트롯 인증서 발급
    private void onAuthCertification() {
        AlertPop alert = new AlertPop();
        alert.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
            @Override
            public void onClick(View v, int pos, final JSONObject json) {
                if (json != null) {
                    String[] items = new String[]{"오늘", "어제"};
                    final String[] keys = new String[]{"today", "yesterday"};

                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            try {
                                String period = keys[which];
                                //String period = "today";

                                int artist_no = JSONUtil.getInteger(json, "no");
                                String artist_name = JSONUtil.getString(json, "name");
                                String url = JSONUtil.getStringUrl(json, "pic");

                                Intent i = new Intent();
                                i.setClass(getContext(), AtvCertification.class);
                                i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
                                i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
                                i.putExtra(Constants.EXTRAS_URL, url);
                                i.putExtra(Constants.EXTRAS_TYPE, period);
                                startActivity(i);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    alert.showSeletItem(getContext(), items);
                }
            }
        });
        alert.showVoteArtist(getContext(), true, "인증서를 발급할 가수를 선택해주세요", true);
    }

    private long getAdGap() {
        long gap = _adClickTime < 1 ? 0 : (System.currentTimeMillis() - _adClickTime) / 1000;
        return gap;
    }


    private void onClickRcmd() {
        JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
        String phone = JSONUtil.getString(jsonUser, "phone");
        String login_type = JSONUtil.getString(jsonUser, "login_type");

        //if (FormatUtil.isNullorEmpty(phone) && (!login_type.equals("N") && !login_type.equals("K") && !login_type.equals("G"))) {
        if (FormatUtil.isNullorEmpty(phone)) {
            new Alert().showAlert(getContext(), "핸드폰 인증 후에 이용 하실 수 있습니다.");
            return;
        }

        String put_rcmd_code = JSONUtil.getString(jsonUser, "put_rcmd_code");

        if (!FormatUtil.isNullorEmpty(put_rcmd_code)) {
            new Alert().showAlert(getContext(), "추천인 코드를 이미 등록 하셨습니다.");
            return;
        }

        AlertPop alert = new AlertPop();
        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
            @Override
            public void onClose(DialogInterface dialog, int which) {
                if (which == Alert.BUTTON1) {
                    callApi_member_get_by_no(false);
                    _baseInputRcmd.setVisibility(View.GONE);
                }
            }
        });
        alert.showInputRcmd(getContext());
    }


    private void setMyVoteCountPoint(JSONObject userJson) {
        String vote_dttm_remain = "";
        double point = 0;
        double heart = 0;

        if (userJson != null) {
            _vote_cnt = JSONUtil.getInteger(userJson, "vote_cnt", 0);
            point = JSONUtil.getDouble(userJson, "point", 0);
            heart = JSONUtil.getDouble(userJson, "heart", 0);
            vote_dttm_remain = JSONUtil.getString(userJson, "vote_dttm_remain", "");
        }
        if (_vote_cnt < 1 && (FormatUtil.isNullorEmpty(vote_dttm_remain) || vote_dttm_remain.equals("00:00:00"))) {
            callApi_vote_put_ticket_init();
        }

//        _txtMyVote.setText(FormatUtil.toPriceFormat(_vote_cnt) + "장");
//        _txtMyPoint.setText(FormatUtil.toPriceFormat(point) + "P");
//        _txtMyHeart.setText(FormatUtil.toPriceFormat(heart) + "개");
    }


    private boolean isContainsItem(String sku) {

        for (String s : _myItemList) {
            if (s.contains(sku)) {
                return true;
            }
        }

        return false;
    }


    private void setDataBinding() {

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

        LogUtil.json(json);

        setMyVoteCountPoint(json);

        String login_type = JSONUtil.getString(json, "login_type");
        String phone = JSONUtil.getString(json, "phone");
        String rcmd_code = SPUtil.getInstance().getRcmdCode(getContext());

        int vod_cnt = JSONUtil.getInteger(json, "vod_cnt");
        int vod_time = JSONUtil.getInteger(json, "vod_time");
        int lv = JSONUtil.getInteger(json, "lv", 0);

        String artist_name = JSONUtil.getString(json, "artist_name");
        int artist_pan_count = JSONUtil.getInteger(json, "artist_pan_count", 0);

        if (!FormatUtil.isNullorEmpty(artist_name)) {
            String pan = String.format("%s, 팬 : %s명", artist_name, FormatUtil.toPriceFormat(artist_pan_count));
            _txtArtist.setText(pan);
            _txtStarRoom.setText(artist_name + " 별방 입장");
        } else {
            _txtArtist.setText("최애가수를 선택해주세요.");
            _txtStarRoom.setText("별방 입장");
        }

        _txtEmail.setText(JSONUtil.getString(json, "email"));
        _txtPhone.setText(!FormatUtil.isNullorEmpty(phone) ? phone : "미인증 상태입니다.");

        if (login_type.equals("K")) {
            _txtEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_ktalk, 0, 0, 0);
        } else if (login_type.equals("N")) {
            _txtEmail.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_naver, 0, 0, 0);
        } else {
            _txtEmail.setVisibility(View.GONE);
        }

        if (login_type.equals("E") || login_type.equals("A")) {
            _imgSns.setVisibility(View.GONE);
        } else if (login_type.equals("K") || login_type.equals("N") || login_type.equals("G")) {
            if (login_type.equals("N"))
                _imgSns.setImageResource(R.drawable.ic_naver);
            if (login_type.equals("K"))
                _imgSns.setImageResource(R.drawable.ic_ktalk);
            if (login_type.equals("G"))
                _imgSns.setImageResource(R.drawable.ic_google);
            _imgSns.setVisibility(View.VISIBLE);
        }

        int hour = vod_time / (60 * 60);
        int minute = (vod_time / 60) % 60;

        _txtNick.setText(JSONUtil.getString(json, "nick"));


        _txtLevel.setText("Lv." + lv);
        _txtExpStart.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "lv_pnt")));
        _txtExpEnd.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "exp_end")));

        _txtPlayCount.setText(FormatUtil.toPriceFormat(vod_cnt) + "회");
        _txtPlayTime.setText(FormatUtil.twoNumber(hour) + "시간 " + FormatUtil.twoNumber(minute) + "분");

//        _txtVoteCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "vote_cnt")) + "장");
        _txtVotedCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "voted_cnt")) + "장");

        _txtBoardCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "board_write_cnt")) + "회");
        _txtLikeGutCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "get_like_cnt")) + "회");
        _txtLikePutCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "put_like_cnt")) + "회");

        //_txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "point")) + " P");
        _txtPointGet.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "point_get")) + "P");
        _txtPointUse.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "point_use")) + "P");

        if (SPUtil.getInstance().isAvailableBoard(getContext())) {
            _baseStarRoom.setVisibility(View.VISIBLE);
            _baseBlog.setVisibility(View.VISIBLE);
        } else {
            _baseStarRoom.setVisibility(View.GONE);
            _baseBlog.setVisibility(View.GONE);
        }

        if (_item_my_vph != null) {
            _item_my_vph.setMyVoteCountPoint();
        }
    }


//    private void callApi_logClick_my_action_info(final String code) {
//
//        LogUtil.e("==========callApi_logClick_my_action_info : start==========");
//        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
//
//        Call<JsonObject> call = apiInterface.logClick_my_action_info(
//                SPUtil.getInstance().getUserNoEnc(getContext())
//                , code
//        );
//        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
//
//        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
//            @Override
//            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
//                super.onResponse(resultCode, resultMessage, json);
//
//                //ProgressDialogUtil.dismiss(dialog);
//
//                LogUtil.d("resultCode : " + resultCode);
//                LogUtil.d("resultMessage : " + resultMessage);
//                LogUtil.json(json);
//
//                JSONObject data = JSONUtil.getJSONObject(json, "data");
//
//                int cnt = JSONUtil.getInteger(data, "cnt", 0);
//
//                LogUtil.d("code : " + code + ", cnt : " + cnt);
//
//                if (code.equals(Constants.LOG_CLICK_REVIEW)) {
//                    cnt_addVoteTicketReview = cnt;
//                }
//                if (code.equals(Constants.LOG_CLICK_SHARE)) {
//                    cnt_addVoteTicketShare = cnt;
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                super.onFailure(call, t);
//                //ProgressDialogUtil.dismiss(dialog);
//                LogUtil.d("onFailure");
//            }
//        });
//    }

    private void callApi_vote_put_ticket_ad(final int amount) {
        LogUtil.e("==========callApi_vote_put_ticket_ad : start==========");

        if (!SPUtil.getInstance().getReviewRewardYn(getContext()).equals("Y")) {
            LogUtil.e("callApi_vote_put_ticket_ad, pass");
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_put_ticket_ad(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , amount
                , ""
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "투표권 " + amount + "장이 충전되었습니다.";
                }

                //Alert.toastLong(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_logClick_add_log(final String code) {

        LogUtil.e("==========callApi_logClick_add_log : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.logClick_add_log(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , code
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_out() {
        LogUtil.e("==========callApi_member_out : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_out(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        SPUtil.getInstance().logout(getContext());

                        finishAffinity();
                    }
                });
                alert.showAlert(getContext(), resultMessage, false, "확인");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_get_by_no(final boolean isCallPpobkki) {

        LogUtil.e("==========callApi_member_get_by_no : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_no(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                if (resultCode == 0) {
                    SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                    setDataBinding();
                }

                if (isCallPpobkki) {

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                // ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_update_favorite_artist(int artist_no, final String name) {

        LogUtil.e("==========callApi_member_update_favorite_artist : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_update_favorite_artist(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , artist_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                if (resultCode == 0) {
                    SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                    setDataBinding();
                }

                if (!FormatUtil.isNullorEmpty(name)) {
                    _txtArtist.setText(name);
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private List<String> _myItemList = new ArrayList<String>();


    private void callApi_memberItem_get_all() {

        _myItemList = new ArrayList<String>();
        LogUtil.e("==========callApi_memberItem_get_all : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberItem_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "P"
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


                JSONArray dataArr = JSONUtil.getJSONArray(json, "data");

                if (dataArr != null) {
                    for (int i = 0; i < dataArr.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(dataArr, i);

                        String sku = JSONUtil.getString(j, "sku");

                        _myItemList.add(sku);
                    }
                }

                if (Constants.DISPLAY_AD && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                    if (isContainsItem("remove_ad")) {
                        SPUtil.getInstance().setIsHaveAdRemoveItem(getContext(), true);
                        _baseAdView.setVisibility(View.GONE);
//                        _adViewCauly.setVisibility(View.GONE);
                    } else {
                        SPUtil.getInstance().setIsHaveAdRemoveItem(getContext(), false);
                        _baseAdView.setVisibility(View.VISIBLE);
                        AdRequest adRequest = new AdRequest.Builder().build();
                        _adView.loadAd(adRequest);
                    }

                    SPUtil.getInstance().setIsHaveBoosterItem(getContext(), isContainsItem("booster"));

                    int enterMoreCount = SPUtil.getInstance().getEnterMoreCount(getContext());

                    if (enterMoreCount % 80 == 15 && !isContainsItem("auto_ticket") && Constants.IS_ONE_STORE == false) {
                        String msg = "1시간 마다 투표권을 200장씩 충전해주는 상품이 있습니다.\n\n확인해보시겠습니까?";
                        Alert alert = new Alert();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    //showPopupAutoTicket();

                                    Intent i = new Intent();
                                    i.setClass(getContext(), AtvPayment.class);
                                    startActivity(i);
                                }
                            }
                        });
                        alert.showAlert(getContext(), msg, false, "확인", "취소");
                    }
                } else {
                    _baseAdView.setVisibility(View.GONE);
//                    _adViewCauly.setVisibility(View.GONE);
                }

                String rcmd_code = SPUtil.getInstance().getRcmdCode(getContext());

                LogUtil.d("rcmd_code : " + rcmd_code);

                if (_heart_recommend < 0 || _heart_recommend_rcv < 0) {
                    callApi_member_get_rcmd_code();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_memberMission_check_mission_view_blog() {

        LogUtil.e("==========callApi_memberMission_check_mission_view_blog : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.memberMission_check_mission_view_blog(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

    private void callApi_vote_put_ticket_init() {
        LogUtil.e("==========callApi_vote_put_ticket_init : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_put_ticket_init(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    callApi_vote_my_vote_ticket_info();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_vote_my_vote_ticket_info() {
        LogUtil.e("==========callApi_vote_my_vote_ticket_info : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_my_vote_ticket_info(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    JSONObject userJson = JSONUtil.getJSONObject(json, "data");

                    setMyVoteCountPoint(userJson);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_get_rcmd_code() {
        LogUtil.e("==========callApi_member_get_rcmd_code : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_rcmd_code(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                String rcmd_code = JSONUtil.getString(data, "rcmd_code");

                int rcmd_cnt = JSONUtil.getInteger(data, "rcmd_cnt", 0);

                _heart_recommend = JSONUtil.getInteger(data, "point", 0);
                _heart_recommend_rcv = JSONUtil.getInteger(data, "point_target", 0);

                if (!FormatUtil.isNullorEmpty(rcmd_code)) {
                    SPUtil.getInstance().setRcmdCode(getContext(), rcmd_code);
                    String msg = "초대하기 (추천인 코드 : <font color='#d4364f'>" + rcmd_code + "</font>)";

                    if (rcmd_cnt >= 0) {
                        msg += "<br>내 추천인 코드를 입력한 회원 수 : " + rcmd_cnt + "명";
                    }

                    _txtHintShare02.setText(Html.fromHtml(msg));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private CountDownTimer _timer = null;
    private CountDownTimer _timerAd = null;


    // 인앱 결제


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }
        if (_timerAd != null) {
            _timerAd.cancel();
            _timerAd = null;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);

        if (_REQ_SHUTTLE == requestCode) {
            if (resultCode == RESULT_OK) {
                showPopSearch();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
