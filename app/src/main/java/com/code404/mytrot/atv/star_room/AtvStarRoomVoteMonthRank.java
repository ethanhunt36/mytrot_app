package com.code404.mytrot.atv.star_room;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.more.AtvMoreReq;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;


public class AtvStarRoomVoteMonthRank extends AtvBase implements GListView.IMakeView {

    // starroom, vote, donate, vod


    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;

    private View _header = null;
    private TextView _txtFilter = null;
    private TextView _txtSeqAll = null;
    private TextView _txtSeqM = null;
    private TextView _txtSeqF = null;


    private int _artist_no = -1;
    private int _last_no = -1;
    private String _term = "month";
    private String _gender = "";
    private String _ym = "202201";
    private String _ymd = "";

    private String[] _items = null;
    private JSONArray _rank_list = null;
    private JSONArray _ym_list = null;
    private JSONArray _ymd_list = null;

    @Override
    protected boolean getBundle() {

        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);

        _term = getIntent().getStringExtra(Constants.EXTRAS_TYPE);

        if (FormatUtil.isNullorEmpty(_term)) _term = "month";

        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_alrim);
    }


    @Override
    protected void init() {
        _txtTopTitle.setText("월별 투표 순위");
        _ym = FormatUtil.getCurrentYm();
        _ymd = "";

        if (_term.equals("week")) {
            _txtTopTitle.setText("주별 투표 순위");
            _ym = "";
            _ymd = "";
        }

        _baseTopBottom.setVisibility(View.GONE);
        _txtFilter.setVisibility(View.VISIBLE);

        setDatabinding();
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);


        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_starroom_vote_rank, null);
        _list.addHeaderView(_header);

        _txtFilter = _header.findViewById(R.id.txtFilter);
        _txtSeqAll = _header.findViewById(R.id.txtSeqAll);
        _txtSeqF = _header.findViewById(R.id.txtSeqF);
        _txtSeqM = _header.findViewById(R.id.txtSeqM);


        _list.setViewMaker(R.layout.row_vote, this);
    }


    @Override
    protected void configureListener() {

        _txtSeqAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGender("");
                filtering();
            }
        });

        _txtSeqF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGender("F");
                filtering();
            }
        });

        _txtSeqM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGender("M");
                filtering();
            }
        });

        _txtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (_term.equals("month")) {
                    _items = new String[_ym_list.length()];

                    for (int i = 0; i < _ym_list.length(); i++) {
                        JSONObject json = JSONUtil.getJSONObject(_ym_list, i);
                        String ym = JSONUtil.getString(json, "ym");
                        _items[i] = ym.substring(0, 4) + "년 " + ym.substring(4, 6) + "월";
                    }

                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (which > -1) {
                                JSONObject json = JSONUtil.getJSONObject(_ym_list, which);
                                _ym = JSONUtil.getString(json, "ym");

                                setDatabinding();
                            }
                        }
                    });
                    alert.showSeletItem(getContext(), _items);
                } else {
                    _items = new String[_ymd_list.length()];

                    for (int i = 0; i < _ymd_list.length(); i++) {
                        JSONObject json = JSONUtil.getJSONObject(_ymd_list, i);
                        String text = JSONUtil.getString(json, "text");
                        _items[i] = text;
                    }

                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (which > -1) {
                                JSONObject json = JSONUtil.getJSONObject(_ymd_list, which);
                                _ymd = JSONUtil.getString(json, "ymd");

                                setDatabinding();
                            }
                        }
                    });
                    alert.showSeletItem(getContext(), _items);
                }

            }
        });
    }


    private void setGender(String gender) {
        _gender = gender;

        _txtSeqAll.setBackground(null);
        _txtSeqF.setBackground(null);
        _txtSeqM.setBackground(null);

        if (gender.equals("F")) {
            _txtSeqF.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        } else if (gender.equals("M")) {
            _txtSeqM.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        } else {
            _txtSeqAll.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        }


    }


    private void filtering() {
        JSONArray list = new JSONArray();
        int rank = 0;
        for (int i = 0; i < _rank_list.length(); i++) {
            JSONObject json = JSONUtil.getJSONObject(_rank_list, i);
            String gender = JSONUtil.getString(json, "artist_gender");

            if (FormatUtil.isNullorEmpty(_gender) || _gender.equals(gender)) {
                rank++;
                JSONUtil.puts(json, "rank", rank);
                list.put(json);
            }
        }
        _list.removeAll();
        _list.addItems(list);
    }


    private void setDatabinding() {
        setFilter();
        //setGender("");
        callApi_vote_ranking_get_artist();
    }

    private void setFilter() {
        if (_term.equals("month")) {
            _txtFilter.setText(_ym.substring(0, 4) + "년 " + _ym.substring(4, 6) + "월");
        } else {
            if (_ymd_list != null) {
                for (int i = 0; i < _ymd_list.length(); i++) {
                    JSONObject j = JSONUtil.getJSONObject(_ymd_list, i);
                    String ym = JSONUtil.getString(j, "ymd");

                    if (_ymd.equals(ym)) {
                        String text = JSONUtil.getString(j, "text");
                        _txtFilter.setText(text);
                        break;
                    }
                }
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        LinearLayout baseLeft = (LinearLayout) convertView.findViewById(R.id.baseLeft);
        de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
        TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
        TextView txtVoteCount = (TextView) convertView.findViewById(R.id.txtVoteCount);
        TextView txtMyCount = (TextView) convertView.findViewById(R.id.txtMyCount);

        View baseLine = (View) convertView.findViewById(R.id.baseLine);
        View baseMyCount = convertView.findViewById(R.id.baseMyCount);
        View adViewBody = convertView.findViewById(R.id.adViewBody);

        ImageView img_rank = convertView.findViewById(R.id.img_rank);
        TextView txtFix = convertView.findViewById(R.id.txtFix);

        img_rank.setVisibility(View.GONE);
        txtFix.setVisibility(View.GONE);

        final String artist_name = JSONUtil.getString(json, "artist_name");
        String pic1 = JSONUtil.getStringUrl(json, "pic");
        final int artist_no = JSONUtil.getInteger(json, "artist_no", 0);

        //int vote_cnt = JSONUtil.getInteger(json, "vote_cnt", 0);
        int vote_cnt = JSONUtil.getInteger(json, "total_vote_sum", 0);
        int rank = JSONUtil.getInteger(json, "rank", 0);


        LogUtil.d("pic1 : " + pic1);

        baseMyCount.setVisibility(View.GONE);

        String name = (rank) + ". " + artist_name;

        if (rank <= 5) {
            name = artist_name;
        }
        if (artist_no == _artist_no) {
            name = "<font color='#ff9900'><b>" + name + "</b></font>";
        }

//        int target_vote_cnt = 3000000;
//        int target_vote_cnt_min = 1000000;
//
//        if (vote_cnt >= target_vote_cnt && _term.equals("month")
//                && Integer.parseInt(_ym) >= 202405
//                && Integer.parseInt(_ym) <= 202501) {
//            double per = vote_cnt * 100.0 / target_vote_cnt;
//
//            txtFix.setText("300만표 달성(" + String.format("%.2f", per) + "%) - 지하철광고 확정");
//            txtFix.setVisibility(View.VISIBLE);
//        } else if (vote_cnt >= target_vote_cnt_min && _term.equals("month")) {
//
//            int a = (int) (vote_cnt / target_vote_cnt_min);
//            txtFix.setText(a + "00만표 달성");
//            txtFix.setVisibility(View.VISIBLE);
//        }

        txtArtist.setText(Html.fromHtml(name));

        if (rank <= 5) {
            img_rank.setVisibility(View.VISIBLE);

            switch (rank) {
                case 1:
                    img_rank.setImageResource(R.drawable.ic_rank_1);
                    break;
                case 2:
                    img_rank.setImageResource(R.drawable.ic_rank_2);
                    break;
                case 3:
                    img_rank.setImageResource(R.drawable.ic_rank_3);
                    break;
                case 4:
                    img_rank.setImageResource(R.drawable.ic_rank_4);
                    break;
                case 5:
                    img_rank.setImageResource(R.drawable.ic_rank_5);
                    break;
            }
        } else {
            img_rank.setVisibility(View.GONE);
        }

        txtVoteCount.setText(FormatUtil.toPriceFormat(vote_cnt));
        if (!FormatUtil.isNullorEmpty(pic1)) {
            Picasso.with(getContext())
                    .load(pic1)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

        adViewBody.setVisibility(View.GONE);

        return convertView;
    }

    private void callApi_vote_ranking_get_artist() {
        _list.removeAll();

        LogUtil.e("==========callApi_vote_ranking_get_artist : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_ranking_get_artist(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "USE"
                , Constants.RECORD_SIZE_MAX
                , _last_no
                , _term
                , _ym
                , _ymd
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    _rank_list = JSONUtil.getJSONArray(jsonData, "list");

                    for (int i = 0; i < _rank_list.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(_rank_list, i);
                        JSONUtil.puts(j, "rank", i + 1);
                    }

                    if (_term.equals("month"))
                        _ym_list = JSONUtil.getJSONArray(jsonData, "valid_ym");
                    else
                        _ymd_list = JSONUtil.getJSONArray(jsonData, "valid_ymd");

                    //_list.addItems(_rank_list);
                    filtering();

                    if (FormatUtil.isNullorEmpty(_ymd) && _ymd_list != null && _ymd_list.length() > 0) {
                        JSONObject j = JSONUtil.getJSONObject(_ymd_list, 0);
                        _ymd = JSONUtil.getString(j, "ymd");
                        setFilter();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

}
