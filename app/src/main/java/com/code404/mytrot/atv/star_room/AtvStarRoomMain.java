package com.code404.mytrot.atv.star_room;

import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.code404.mytrot.R;
import com.code404.mytrot.adapter.MyFragmentPagerAdapter;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.artist.AtvArtist;
import com.code404.mytrot.atv.star.AtvStar;
import com.code404.mytrot.atv.star.AtvVsInfo;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.vote.FrgBoard;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.SquareImageView;
import com.code404.mytrot.widget.ItemPicLike;
import com.code404.mytrot.widget.ItemStarRoom;
import com.code404.mytrot.widget.ItemStarRoomTop;
import com.code404.mytrot.widget.ItemStarroomSummary;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


// 별방
public class AtvStarRoomMain extends AtvBase {

    private int _artist_no = -1;
    private String _artist_name = "";
    private String _artist_board_id = "";

    private int _like_cnt = 5;
    private int _comment_cnt = 5;

    private String _myNick = "";
    // 프로필 사진 이미지
    private JSONArray _artist_pic_list = null;

    private SquareImageView _img01 = null;
    private TextView _txtTip = null;
    private LinearLayout _baseImgList = null;
    private LinearLayout _baseStarRoomBody = null;

    private TextView _txtMore = null;
    private TextView _txtMore2 = null;
    private TextView _txt_keyword = null;

    private ItemStarRoomTop _item_rank_01 = null;
    private ItemStarRoomTop _item_rank_02 = null;
    private ItemStarRoomTop _item_rank_03 = null;
    private ItemStarRoomTop _item_rank_04 = null;

    private ItemStarRoomTop _item_rank_05 = null;
    private ItemStarRoomTop _item_rank_06 = null;

    private ItemStarroomSummary _btn01 = null;
    private ItemStarroomSummary _btn01_2 = null;
    private ItemStarroomSummary _btn02 = null;
    private ItemStarroomSummary _btn03 = null;
    private ItemStarroomSummary _btn04 = null;
    private ItemStarroomSummary _btn05 = null;
    private ItemStarroomSummary _btn06 = null;


    @Override
    protected boolean getBundle() {
        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);

        if (_artist_no < 1 || FormatUtil.isNullorEmpty(_artist_name) || FormatUtil.isNullorEmpty(_artist_board_id)) {
            JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

            int artist_no = JSONUtil.getInteger(json, "artist_no");
            String artist_name = JSONUtil.getString(json, "artist_name");
            String artist_board_id = JSONUtil.getString(json, "artist_board_id");

            if (artist_no > 0) _artist_no = artist_no;
            if (!FormatUtil.isNullorEmpty(artist_name)) _artist_name = artist_name;
            if (!FormatUtil.isNullorEmpty(artist_board_id)) _artist_board_id = artist_board_id;
        }

        return _artist_no > 0;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_star_room_main);
    }


    @Override
    protected void init() {
        _txtTopTitle.setText(_artist_name + " 별방");
        _btnTopBack.setVisibility(View.VISIBLE);
        _btnTopRight.setVisibility(View.GONE);
        _baseTopBottom.setVisibility(View.GONE);

        _item_rank_05.setTitle("이번달 응원글 Top3");
        _item_rank_06.setTitle("이번달 갤러리 Top3");

        _item_rank_01.setTitle("이번달 별방 Top3");
        _item_rank_02.setTitle("이번달 영상재생 Top3");
        _item_rank_03.setTitle("이번달 투표 Top3");
        _item_rank_04.setTitle("이번달 기부 Top3");


        _btn01.setTitle("월별 투표 순위");
        _btn01_2.setTitle("주별 투표 순위");
        _btn02.setTitle("누적 기부 포인트");
        _btn03.setTitle("응원글");
        _btn04.setTitle("갤러리");
        _btn05.setTitle("별방");
        _btn06.setTitle("영상");

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

        _myNick = JSONUtil.getString(json, "nick");
    }


    @Override
    protected void findView() {
        _img01 = findViewById(R.id.img01);
        _txtTip = findViewById(R.id.txtTip);
        _baseImgList = findViewById(R.id.baseImgList);
        _baseStarRoomBody = findViewById(R.id.baseStarRoomBody);

        _txtMore = findViewById(R.id.txtMore);
        _txtMore2 = findViewById(R.id.txtMore2);
        _txt_keyword = findViewById(R.id.txt_keyword);

        _item_rank_01 = (ItemStarRoomTop) findViewById(R.id.item_rank_01);
        _item_rank_02 = (ItemStarRoomTop) findViewById(R.id.item_rank_02);
        _item_rank_03 = (ItemStarRoomTop) findViewById(R.id.item_rank_03);
        _item_rank_04 = (ItemStarRoomTop) findViewById(R.id.item_rank_04);
        _item_rank_05 = (ItemStarRoomTop) findViewById(R.id.item_rank_05);
        _item_rank_06 = (ItemStarRoomTop) findViewById(R.id.item_rank_06);

        _btn01 = findViewById(R.id.btn01);
        _btn01_2 = findViewById(R.id.btn01_2);
        _btn02 = findViewById(R.id.btn02);
        _btn03 = findViewById(R.id.btn03);
        _btn04 = findViewById(R.id.btn04);
        _btn05 = findViewById(R.id.btn05);
        _btn06 = findViewById(R.id.btn06);
    }


    @Override
    protected void configureListener() {

        _txtTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg = "1. 갤러리와 별방에 이미지를 등록해주세요. \n" +
                        "좋아요 " + _like_cnt + "개, 댓글 " + _comment_cnt + "개가 등록되면 메인 사진 후보에 등록됩니다.\n추후에 별방 이야기의 이미지도 추가될 예정입니다.\n" +
                        "\n" +
                        "2. 후보 사진 중에 맘에 드는 사진을 좋아요 눌러주세요. 1인 1회만 좋아요 가능합니다.\n" +
                        "\n" +
                        "3. 매시각마다 좋아요를 가장 많이 받은 Top10 중에 한장이 랜덤으로 메인 사진으로 등록됩니다.\n" +
                        "\n" +
                        "4. 후보 사진은 무제한으로 등록 가능하지만, 중복사진이나 양호하지 않은 사진은 관리자가 삭제할 수 있습니다.\n" +
                        "\n" +
                        "5. 매달 1일에는 후보 사진 Top10 를 제외한 나머지 후보 사진은 자동으로 삭제 됩니다. ";

                new Alert().showAlert(getContext(), msg);
            }
        });

        _txtMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                i.putExtra(Constants.EXTRAS_JSON_STRING, _artist_pic_list == null ? "" : _artist_pic_list.toString());
                i.setClass(getContext(), AtvStarRoomPic.class);
                startActivity(i);
            }
        });

        _txtMore2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                i.setClass(getContext(), AtvStarRoomKeyword.class);
                startActivity(i);
            }
        });

        findViewById(R.id.baseStarRoom).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
                i.putExtra(Constants.EXTRAS_TYPE, 1);
                i.setClass(getContext(), AtvStarRoom.class);
                startActivity(i);
            }
        });

        findViewById(R.id.baseStarRoomSecret).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
                int lv = JSONUtil.getInteger(jsonUser, "lv", 0);
                int artist_reg_days = JSONUtil.getInteger(jsonUser, "artist_reg_days", 0);
                int artist_reg_hours = JSONUtil.getInteger(jsonUser, "artist_reg_hours", 0);

                if (lv < 2 || artist_reg_hours < 48) {
                    String msg = "별방 비밀 이야기는\n레벨2, 별방 가입 2일(48시간) 경과 후 부터 입장이 가능합니다.";
                    msg += "\n\n현재 레벨 : Lv." + lv;
                    msg += "\n별방 가입 : " + artist_reg_hours + "시간 경과";
                    new Alert().showAlert(getContext(), msg);
                    return;
                }

                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
                i.putExtra(Constants.EXTRAS_TYPE, 99);
                i.setClass(getContext(), AtvStarRoom.class);
                startActivity(i);
            }
        });

        _item_rank_05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToStarRoomRank(AtvStarRoomRank.TYPE_CHEERUP);
            }
        });
        _item_rank_06.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToStarRoomRank(AtvStarRoomRank.TYPE_GALLERY);
            }
        });

        _item_rank_01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToStarRoomRank(AtvStarRoomRank.TYPE_STARROOM);
            }
        });
        _item_rank_02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToStarRoomRank(AtvStarRoomRank.TYPE_VOD);
            }
        });
        _item_rank_03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToStarRoomRank(AtvStarRoomRank.TYPE_VOTE);
            }
        });
        _item_rank_04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                moveToStarRoomRank(AtvStarRoomRank.TYPE_DONATE);
            }
        });

        _btn01.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                i.putExtra(Constants.EXTRAS_TYPE, "month");
                i.setClass(getContext(), AtvStarRoomVoteMonthRank.class);
                startActivity(i);
            }
        });
        _btn01_2.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                i.putExtra(Constants.EXTRAS_TYPE, "week");
                i.setClass(getContext(), AtvStarRoomVoteMonthRank.class);
                startActivity(i);
            }
        });
        _btn02.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Intent intent = new Intent();
                intent.setClass(getContext(), AtvVote.class);
                intent.putExtra(Constants.EXTRAS_FRG_INDEX, 1);
                startActivity(intent);
                finishAffinity();
            }
        });
        _btn03.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Intent intent = new Intent();
                intent.setClass(getContext(), AtvVote.class);
                intent.putExtra(Constants.EXTRAS_FRG_INDEX, 2);
                startActivity(intent);
                finishAffinity();
            }
        });
        _btn04.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Intent intent = new Intent();
                intent.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                intent.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                intent.setClass(getContext(), AtvStar.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        _btn05.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, _artist_name);
                i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
                i.setClass(getContext(), AtvStarRoom.class);
                startActivity(i);
            }
        });
        _btn06.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                Intent intent = new Intent();
                intent.setClass(getContext(), AtvArtist.class);
                startActivity(intent);
                finishAffinity();
            }
        });
    }


    private void moveToStarRoomRank(String type) {
        Intent i = new Intent();
        i.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
        i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
        i.putExtra(Constants.EXTRAS_TYPE, type);
        i.setClass(getContext(), AtvStarRoomRank.class);
        startActivity(i);
    }


    @Override
    protected void onResume() {
        super.onResume();

        callApi_boardStar_get_starroom_main_data();
    }


    private void callApi_boardStar_get_starroom_main_data() {
        LogUtil.e("==========callApi_boardStar_get_starroom_main_data : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.boardStar_get_starroom_main_data(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                JSONObject artist_pic_info = JSONUtil.getJSONObject(data, "list_artist_pic");
                JSONArray artist_pic_list = JSONUtil.getJSONArray(artist_pic_info, "list");

                JSONObject last_board_star = JSONUtil.getJSONObject(data, "last_board_star");
                JSONArray board_list = JSONUtil.getJSONArray(last_board_star, "list_data");

                _like_cnt = JSONUtil.getInteger(data, "like_cnt");
                _comment_cnt = JSONUtil.getInteger(data, "comment_cnt");

                JSONObject top_cheerup = JSONUtil.getJSONObject(data, "top_cheerup");
                JSONObject top_gallery = JSONUtil.getJSONObject(data, "top_gallery");

                JSONObject top_starroom = JSONUtil.getJSONObject(data, "top_starroom");
                JSONObject top_vod = JSONUtil.getJSONObject(data, "top_vod");
                JSONObject top_vote = JSONUtil.getJSONObject(data, "top_vote");
                JSONObject top_donate = JSONUtil.getJSONObject(data, "top_donate");

                JSONArray top_cheerup_list = JSONUtil.getJSONArray(top_cheerup, "list");
                JSONArray top_gallery_list = JSONUtil.getJSONArray(top_gallery, "list");

                JSONArray top_starroom_list = JSONUtil.getJSONArray(top_starroom, "list");
                JSONArray top_vod_list = JSONUtil.getJSONArray(top_vod, "list");
                JSONArray top_vote_list = JSONUtil.getJSONArray(top_vote, "list");
                JSONArray top_donate_list = JSONUtil.getJSONArray(top_donate, "list");


                JSONObject vote_artist = JSONUtil.getJSONObject(data, "vote_artist");
                JSONObject vote_artist_week = JSONUtil.getJSONObject(data, "vote_artist_week");
                JSONObject donate_done = JSONUtil.getJSONObject(data, "donate_done");

                JSONObject board_cate1_info = JSONUtil.getJSONObject(data, "board_cate1_info");
                JSONObject board_cate2_info = JSONUtil.getJSONObject(data, "board_cate2_info");
                JSONObject board_starroom = JSONUtil.getJSONObject(data, "board_starroom");

                int artist_vod_cnt = JSONUtil.getInteger(data, "artist_vod_cnt");
                String starroom_secret_yn = JSONUtil.getString(data, "starroom_secret_yn");

                int pic_count = JSONUtil.getInteger(data, "pic_count");
                int keyword_count = JSONUtil.getInteger(data, "keyword_count");

                String keyword = JSONUtil.getString(data, "keyword");

                if("N".equals(starroom_secret_yn)) {
                    findViewById(R.id.baseStarRoomSecret).setVisibility(View.GONE);
                }

                if(FormatUtil.isNullorEmpty(keyword)){
                    keyword = "가수님의 키워드를 입력해주세요.";
                }


                _txtMore.setText(Html.fromHtml(String.format("월간 프로필 사진(<font color='red'>%d</font>)", pic_count)));
                _txtMore2.setText(Html.fromHtml(String.format("월간 키워드(<font color='red'>%d</font>)", keyword_count)));
                _txt_keyword.setText(Html.fromHtml("<b>" + keyword + "</b>"));

                setDataBinding_Pic(artist_pic_list);
                setDataBinding_Board(board_list);

                setDataBinding_Vote(vote_artist);
                setDataBinding_Vote_Week(vote_artist_week);
                setDataBinding_Donate(donate_done);

                setDataBinding_Board1(board_cate1_info);
                setDataBinding_Board2(board_cate2_info);
                setDataBinding_StarRoom(board_starroom);
                setDataBinding_Vod(artist_vod_cnt);

                _item_rank_05.setData(top_cheerup_list, _myNick);
                _item_rank_06.setData(top_gallery_list, _myNick);

                _item_rank_01.setData(top_starroom_list, _myNick);
                _item_rank_02.setData(top_vod_list, _myNick);
                _item_rank_03.setData(top_vote_list, _myNick);
                _item_rank_04.setData(top_donate_list, _myNick);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void setDataBinding_Pic(JSONArray list) {

        _artist_pic_list = list;

        _baseImgList.removeAllViews();

        // 모서리 라운딩 처리
        _img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        _img01.setClipToOutline(true);



        for (int i = 0; i < list.length(); i++) {
            final JSONObject json = JSONUtil.getJSONObject(list, i);
            String main_yn = JSONUtil.getString(json, "main_yn");

            if (main_yn.equals("Y")) {
                String path = JSONUtil.getStringUrl(json, "path");

                Picasso.with(getContext())
                        .load(path)
                        .fit()
                        .placeholder(R.drawable.img_noimg)
                        .into(_img01);
            }

//            ItemPicLike item = new ItemPicLike(getContext());
//            item.setData(json);
//
//            _baseImgList.addView(item);
        }

        _baseImgList.setVisibility(View.GONE);
    }

    private void setDataBinding_Board(JSONArray list) {
        if (list.length() > 0) {
            _baseStarRoomBody.removeAllViews();
        }
        for (int i = 0; i < list.length(); i++) {
            final JSONObject json = JSONUtil.getJSONObject(list, i);

            ItemStarRoom item = new ItemStarRoom(getContext());
            item.setData(json);

            if (i == list.length() - 1) {
                item.setVisibleRowLine(false);
            }

            item.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
                @Override
                public void onClick(View v, int pos) {
                    Intent i = new Intent();
                    i.setClass(getContext(), AtvStarRoomInfo.class);
                    i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
                    i.putExtra(Constants.EXTRAS_JSON_STRING, json.toString());

                    startActivity(i);
                }
            });

            _baseStarRoomBody.addView(item);
        }
    }


    private void setDataBinding_Vote(JSONObject json) {
        int rank = JSONUtil.getInteger(json, "rank", 0);
        String year_month = JSONUtil.getString(json, "year_month", "");

        String desc = String.format("%s : <font color='red'>%s</font>위", year_month, String.valueOf(rank));
        _btn01.setDesc(desc);
    }

    private void setDataBinding_Vote_Week(JSONObject json) {
        int rank = JSONUtil.getInteger(json, "rank", 0);
        String year_month_day = JSONUtil.getString(json, "year_month_day", "");

        String desc = String.format("%s : <font color='red'>%s</font>위", year_month_day, String.valueOf(rank));
        _btn01_2.setDesc(desc);
    }

    private void setDataBinding_Donate(JSONObject json) {
        int donate_cur_point = JSONUtil.getInteger(json, "donate_cur_point", 0);
        int counts = JSONUtil.getInteger(json, "counts", 0);
        String desc = String.format("<font color='red'>%s</font>P (<font color='red'>%s</font>회)", FormatUtil.toPriceFormat(donate_cur_point), String.valueOf(counts));

        _btn02.setDesc(desc);
    }

    private void setDataBinding_Board1(JSONObject json) {
        int cnt_all = JSONUtil.getInteger(json, "cnt_all", 0);
        int cnt = JSONUtil.getInteger(json, "cnt", 0);
        String desc = String.format("누적 : <font color='red'>%s</font>건, 이번달 : <font color='red'>%s</font>건", FormatUtil.toPriceFormat(cnt_all), FormatUtil.toPriceFormat(cnt));

        _btn03.setDesc(desc);
    }

    private void setDataBinding_Board2(JSONObject json) {
        int cnt_all = JSONUtil.getInteger(json, "cnt_all", 0);
        int cnt = JSONUtil.getInteger(json, "cnt", 0);
        String desc = String.format("누적 : <font color='red'>%s</font>건, 이번달 : <font color='red'>%s</font>건", FormatUtil.toPriceFormat(cnt_all), FormatUtil.toPriceFormat(cnt));

        _btn04.setDesc(desc);
    }

    private void setDataBinding_StarRoom(JSONObject json) {
        int cnt_all = JSONUtil.getInteger(json, "cnt_all", 0);
        int cnt = JSONUtil.getInteger(json, "cnt", 0);
        String desc = String.format("누적 : <font color='red'>%s</font>건, 이번달 : <font color='red'>%s</font>건", FormatUtil.toPriceFormat(cnt_all), FormatUtil.toPriceFormat(cnt));

        _btn05.setDesc(desc);
    }

    private void setDataBinding_Vod(int count) {
        String desc = String.format("누적 : <font color='red'>%s</font>건", FormatUtil.toPriceFormat(count));

        _btn06.setDesc(desc);
    }
}
