package com.code404.mytrot.atv.star_room;

import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.code404.mytrot.view.SquareImageView;
import com.code404.mytrot.widget.ItemPicLike;
import com.code404.mytrot.widget.ItemStarRoom;
import com.code404.mytrot.widget.ItemStarRoomTop;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


public class AtvStarRoomRank extends AtvBase implements GListView.IMakeView {

    // starroom, vote, donate, vod

    public static String TYPE_CHEERUP = "cheerup";
    public static String TYPE_GALLERY = "gallery";
    public static String TYPE_STARROOM = "starroom";
    public static String TYPE_VOTE = "vote";
    public static String TYPE_DONATE = "donate";
    public static String TYPE_VOD = "vod";

    private int _artist_no = -1;
    private String _artist_board_id = "";
    private String _type = "";
    private String _myUserNoEnc = "";
    private String _myNick = "";
    private String _category = "";

    private String _ssid = "starroom.ranking_get_all";
    private View _header = null;
    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;
    private TextView _txtUpdateDate = null;
    private TextView _txtMyRank = null;


    @Override
    protected boolean getBundle() {

        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);

        _type = getIntent().getStringExtra(Constants.EXTRAS_TYPE);
        LogUtil.d("_type : " + _type);
        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_alrim);
    }


    @Override
    protected void init() {

        if (_type.equals(TYPE_CHEERUP)) {
            _txtTopTitle.setText("응원글 작성 랭킹");
            _ssid = "board.ranking_get_all";
            _category = "1";
        } else if (_type.equals(TYPE_GALLERY)) {
            _txtTopTitle.setText("갤러리 작성 랭킹");
            _ssid = "board.ranking_get_all";
            _category = "2";
        } else if (_type.equals(TYPE_STARROOM)) {
            _txtTopTitle.setText("별방 작성 랭킹");
            _ssid = "starroom.ranking_get_all";
        } else if (_type.equals(TYPE_VOTE)) {
            _txtTopTitle.setText("투표 랭킹");
            _ssid = "vote.ranking_get_all";
        } else if (_type.equals(TYPE_DONATE)) {
            _txtTopTitle.setText("기부 랭킹");
            _ssid = "donate.ranking_get_all";
        } else if (_type.equals(TYPE_VOD)) {
            _txtTopTitle.setText("영상 재생 랭킹");
            _ssid = "artistVod.ranking_get_all";
        }

        _baseTopBottom.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_point_rank, this);
        _list.setNoData(_baseNoData);

        _txtMyRank.setText("");

        _txtNoData.setText("등록된 랭킹 정보가 없습니다.");

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
        _myUserNoEnc = SPUtil.getInstance().getUserNoEnc(getContext());
        _myNick = JSONUtil.getString(json, "nick").trim();

        callApi_starroom_rank_type();
    }


    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);


        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_rank, null);
        _list.addHeaderView(_header);

        _txtUpdateDate = _header.findViewById(R.id.txtUpdateDate);
        _txtMyRank = _header.findViewById(R.id.txtMyRank);

        findViewById(R.id.baseHeader).setVisibility(View.GONE);
    }


    @Override
    protected void configureListener() {

    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    private void callApi_starroom_rank_type() {
        LogUtil.e("==========callApi_starroom_rank_type : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);


        Call<JsonObject> call = apiInterface.starroom_rank_type(
                _ssid
                , SPUtil.getInstance().getUserNoEnc(getContext())
                , _category
                , "month"
                , "USE"
                , Constants.RECORD_SIZE_MAX
                , _artist_no
                , _artist_board_id
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONObject my_ranking = JSONUtil.getJSONObject(data, "my_ranking");
                JSONArray list = JSONUtil.getJSONArray(data, "list");

                _list.addItems(list);

                if (list.length() > 0) {
                    JSONObject jsonRow01 = JSONUtil.getJSONObject(list, 0);
                    String reg_dttm = JSONUtil.getString(jsonRow01, "reg_dttm");

                    reg_dttm = FormatUtil.convertDateAmPm(reg_dttm, "yyyy-MM-dd a hh:mm");
                    String desc = "최근 업데이트 : " + reg_dttm;
                    _txtUpdateDate.setText(desc);
                }

                setDataBinding(my_ranking);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void setDataBinding(JSONObject json) {
        int rank = JSONUtil.getInteger(json, "rank", 0);

        String txt = "";
        if (rank > 0) {
            txt = _myNick + " <font color='red'><b>" + rank + "위</b></font>";
        } else {
            txt = _myNick + " <font color='red'>등록된 순위가 없습니다.</font>";
        }

        _txtMyRank.setText(Html.fromHtml(txt));
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        String userNo = JSONUtil.getString(json, "member_no");
        String nick = JSONUtil.getString(json, "nick").trim();
        String lv = JSONUtil.getString(json, "lv");

        String nick_lv = nick + " Lv." + lv;

        if (userNo.equals(_myUserNoEnc)) {
            nick_lv = "<font color='#ff9900'><b>" + nick_lv + "</b></font>";
        }

        txtRank.setText(JSONUtil.getString(json, "rank"));
        txtNick.setText(Html.fromHtml(nick_lv));

        if (_type.equals(TYPE_CHEERUP)) {
            txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "cheerup_sum")) + "회");
        } else if (_type.equals(TYPE_GALLERY)) {
            txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "gallery_sum")) + "회");
        } else if (_type.equals(TYPE_STARROOM)) {
            txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "starroom_sum")) + "회");
        } else if (_type.equals(TYPE_VOTE)) {
            txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "vote_sum")) + "장");
        } else if (_type.equals(TYPE_DONATE)) {
            txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "donate_sum")) + "P");
        } else if (_type.equals(TYPE_VOD)) {
            txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "vod_sum")) + "회");
        }


        return convertView;
    }
}
