package com.code404.mytrot.atv.more;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;

public class AtvMoreAlramSet extends AtvBase {

    private ImageButton _btnAll = null;
    private ImageButton _btnArtist = null;
    private ImageButton _btnBoard = null;
    private ImageButton _btnEvent = null;
    //private ImageButton _btnRequest = null;
    private ImageButton _btnEtc = null;

    private Button _btnConfirm = null;


    @Override
    protected void setView() {
        setView(R.layout.atv_more_alram_set);
    }

    @Override
    protected void findView() {
        _btnAll = (ImageButton) findViewById(R.id.btnAll);
        _btnArtist = (ImageButton) findViewById(R.id.btnArtist);
        _btnBoard = (ImageButton) findViewById(R.id.btnBoard);
        _btnEvent = (ImageButton) findViewById(R.id.btnEvent);
        //_btnRequest = (ImageButton) findViewById(R.id.btnRequest);
        _btnEtc = (ImageButton) findViewById(R.id.btnEtc);

        _btnConfirm = (Button) findViewById(R.id.btnConfirm);
    }

    @Override
    protected void init() {
        _txtTopTitle.setText("알림 설정");
        _baseTopBottom.setVisibility(View.GONE);

        callApi_memberAlram_get();
    }

    private void checkAll() {
        //if (_btnArtist.isSelected() && _btnBoard.isSelected() && _btnEvent.isSelected() && _btnRequest.isSelected() && _btnEtc.isSelected()) {
        if (_btnArtist.isSelected() && _btnBoard.isSelected() && _btnEvent.isSelected()  && _btnEtc.isSelected()) {
            _btnAll.setSelected(true);
        } else {
            _btnAll.setSelected(false);
        }
    }

    @Override
    protected void configureListener() {
        _btnAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean check = !_btnAll.isSelected();
                _btnAll.setSelected(check);

                _btnArtist.setSelected(check);
                _btnBoard.setSelected(check);
                _btnEvent.setSelected(check);
                //_btnRequest.setSelected(check);
                _btnEtc.setSelected(check);
            }
        });

        _btnArtist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btnArtist.setSelected(!_btnArtist.isSelected());
                checkAll();
            }
        });
        _btnBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btnBoard.setSelected(!_btnBoard.isSelected());
                checkAll();
            }
        });
        _btnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btnEvent.setSelected(!_btnEvent.isSelected());
                checkAll();
            }
        });
//        _btnRequest.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                _btnRequest.setSelected(!_btnRequest.isSelected());
//                checkAll();
//            }
//        });
        _btnEtc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _btnEtc.setSelected(!_btnEtc.isSelected());
                checkAll();
            }
        });

        _btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_memberAlram_set_all();
            }
        });
    }


    private void callApi_memberAlram_get() {

        LogUtil.e("==========callApi_memberAlram_get : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberAlram_get(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject info = JSONUtil.getJSONObject(json, "data");

                String favorite_yn = JSONUtil.getString(info, "favorite_yn");
                String etc_yn = JSONUtil.getString(info, "etc_yn");
                String admin_yn = JSONUtil.getString(info, "admin_yn");
                String event_yn = JSONUtil.getString(info, "event_yn");
                String community_yn = JSONUtil.getString(info, "community_yn");

                if (favorite_yn.equals("Y")) {
                    _btnArtist.setSelected(true);
                }
                if (etc_yn.equals("Y")) {
                    _btnEtc.setSelected(true);
                }
//                if (admin_yn.equals("Y")) {
//                    _btnRequest.setSelected(true);
//                }
                if (event_yn.equals("Y")) {
                    _btnEvent.setSelected(true);
                }
                if (community_yn.equals("Y")) {
                    _btnBoard.setSelected(true);
                }

                checkAll();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_memberAlram_set_all() {

        LogUtil.e("==========callApi_memberAlram_set_all : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberAlram_set_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _btnArtist.isSelected() ? "Y" : "N"
                , _btnBoard.isSelected() ? "Y" : "N"
                , _btnEvent.isSelected() ? "Y" : "N"
                //, _btnRequest.isSelected() ? "Y" : "N"
                , "Y"
                , _btnEtc.isSelected() ? "Y" : "N"
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "저장 되었습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

}
