package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvDonateDetail extends AtvBase implements GListView.IMakeView {

    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;

    private View _header = null;
    private ImageView _img01 = null;
    private TextView _txtDesc = null;

    int _last_no = -1;

    private String _url = "";
    private String _artist_name = "";
    private int _artist_no = -1;
    private int _donate_nth = -1;
    private int _donate_goal_no = -1;

    private String _myNick = "";
    private String _myUserNoEnc = "";

    @Override
    protected boolean getBundle() {

        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        _url = getIntent().getStringExtra(Constants.EXTRAS_URL);

        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _donate_nth = getIntent().getIntExtra(Constants.EXTRAS_DONATE_NTH, -1);
        _donate_goal_no = getIntent().getIntExtra(Constants.EXTRAS_DONATE_GOAL_NO, -1);


        if (_artist_no < 0) {
            String temp = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NO);
            if (!FormatUtil.isNullorEmpty(temp)) _artist_no = Integer.parseInt(temp);
        }
        if (_donate_nth < 0) {
            String temp = getIntent().getStringExtra(Constants.EXTRAS_DONATE_NTH);
            if (!FormatUtil.isNullorEmpty(temp)) _donate_nth = Integer.parseInt(temp);
        }
        if (_donate_goal_no < 0) {
            String temp = getIntent().getStringExtra(Constants.EXTRAS_DONATE_GOAL_NO);
            if (!FormatUtil.isNullorEmpty(temp)) _donate_goal_no = Integer.parseInt(temp);
        }
        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_alrim);
    }

    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_donate_detail, null);
        _txtDesc = _header.findViewById(R.id.txtDesc);
        _img01 = _header.findViewById(R.id.img01);
        _list.addHeaderView(_header);
    }

    @Override
    protected void init() {
        if (_donate_goal_no > 0) {
            _txtTopTitle.setText(String.format("%s 님의 %d차 기부 회원", _artist_name, _donate_nth));
        } else {
            _txtTopTitle.setText(String.format("%s 님의 1차 ~ %d차 기부 회원", _artist_name, _donate_nth));
        }
        _baseTopBottom.setVisibility(View.GONE);
        _txtDesc.setText("");
        _list.setViewMaker(R.layout.row_point_rank, this);
        _list.setNoData(_baseNoData);

        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
        _myNick = JSONUtil.getString(userInfo, "nick");
        _myUserNoEnc = SPUtil.getInstance().getUserNoEnc(getContext());

        _img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        _img01.setClipToOutline(true);

        if (!FormatUtil.isNullorEmpty(_url)) {
            Picasso.with(getContext())
                    .load(_url)
                    //.fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img01);
            _img01.setVisibility(View.VISIBLE);
        } else {
            _img01.setVisibility(View.GONE);
        }

        callApi_donate_member_list(true);
    }


    @Override
    protected void configureListener() {


    }

    private void callApi_donate_member_list(final boolean isClear) {

        LogUtil.e("==========callApi_donate_member_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.donate_member_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
                , _donate_goal_no
                , Constants.RECORD_SIZE_MAX
                , _last_no
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list_data");

                if (isClear) {
                    _list.removeAll();
                }

                _txtDesc.setText(Html.fromHtml(String.format("총 <b>%d명</b>의 트별님들이 참여해주셨습니다.", list.length())));

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        String userNo = JSONUtil.getString(json, "member_no");
        int mem_no = JSONUtil.getInteger(json, "mem_no");
        String nick = JSONUtil.getString(json, "member_nick").trim();

        if (FormatUtil.isNullorEmpty(nick)) nick = "팬" + mem_no;

        if (userNo.equals(_myUserNoEnc)) {
            nick = "<font color='#ff9900'><b>" + nick + "</b></font>";
        }

        txtRank.setText(String.valueOf(position + 1));
        txtNick.setText(Html.fromHtml(nick));
        txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getInteger(json, "sum_point")) + " P");


        return convertView;
    }

}
