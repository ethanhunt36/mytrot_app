package com.code404.mytrot.atv.more;

import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.web.AtvWeb;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.code404.mytrot.view.SquareImageView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvMoreNotice extends AtvBase implements GListView.IMakeView {

    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;

    private View _header = null;
    private TextView _txtSeqAll = null;
    private TextView _txtSeq01 = null;
    private TextView _txtSeq02 = null;
    private TextView _txtSeq03 = null;
    private TextView _txtSeq04 = null;

    private boolean _is_more_data = true;
    private int _category = 0;
    private int _last_no = -1;


    private String _type = "notice";
    private int _open_no = -1;


    @Override
    protected boolean getBundle() {
        _type = getIntent().getStringExtra(Constants.EXTRAS_TYPE);
        _open_no = getIntent().getIntExtra(Constants.EXTRAS_NO, -1);

        return !FormatUtil.isNullorEmpty(_type);
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_alrim);
    }

    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        if (_type.equals("notice")) {
            // 헤더
            _header = InflateUtil.inflate(getContext(), R.layout.header_notice, null);

            _txtSeqAll = _header.findViewById(R.id.txtSeqAll);
            _txtSeq01 = _header.findViewById(R.id.txtSeq01);
            _txtSeq02 = _header.findViewById(R.id.txtSeq02);
            _txtSeq03 = _header.findViewById(R.id.txtSeq03);
            _txtSeq04 = _header.findViewById(R.id.txtSeq04);

            _list.addHeaderView(_header);
        }
    }

    @Override
    protected void init() {

        _txtTopTitle.setText(_type.equals("notice") ? "공지사항" : "FAQ");
        _btnTopBack.setVisibility(View.VISIBLE);
        _baseTopBottom.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_notice, this);
        _list.setNoData(_baseNoData);

        callApi_notice_listing(true);
    }

    @Override
    protected void configureListener() {

        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_notice_listing(false);
                }
            }
        });


        if (_type.equals("notice")) {
            _txtSeqAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callApi_notice(0);
                }
            });
            _txtSeq01.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callApi_notice(1);
                }
            });
            _txtSeq02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callApi_notice(2);
                }
            });
            _txtSeq03.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callApi_notice(3);
                }
            });
            _txtSeq04.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callApi_notice(4);
                }
            });
        }
    }


    private void callApi_notice(int category) {
        _category = category;

        _txtSeqAll.setBackground(_category == 0 ? getContext().getDrawable(R.drawable.bg_typebox_ent) : null);
        _txtSeq01.setBackground(_category == 1 ? getContext().getDrawable(R.drawable.bg_typebox_ent) : null);
        _txtSeq02.setBackground(_category == 2 ? getContext().getDrawable(R.drawable.bg_typebox_ent) : null);
        _txtSeq03.setBackground(_category == 3 ? getContext().getDrawable(R.drawable.bg_typebox_ent) : null);
        _txtSeq04.setBackground(_category == 4 ? getContext().getDrawable(R.drawable.bg_typebox_ent) : null);

        callApi_notice_listing(true);
    }


    private void callApi_notice_listing(final boolean isClear) {

        if (isClear) {
            _is_more_data = true;
            _last_no = -1;
        }

        if (_is_more_data == false) {
            return;
        }

        LogUtil.e("==========callApi_notice_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        String ssid = _type.equals("notice") ? "notice.listing" : "faq.listing";

        call = apiInterface.notice_listing(
                ssid
                , SPUtil.getInstance().getUserNoEnc(getContext())
                , _category
                , _last_no
                , Constants.RECORD_SIZE
                , "Y"
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list_data");

                JSONArray blog_list = JSONUtil.getJSONArray(data, "blog_data");

                if (isClear) {
                    _list.removeAll();
                }

                if (_last_no < 1 && _type.equals("notice")) {
                    JSONObject info = JSONUtil.getJSONObject(list, 0);
                    int lastest_no = JSONUtil.getInteger(info, "no");
                    SPUtil.getInstance().setLastestNoticeNoViewed(getContext(), lastest_no);
                }

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                if (list.length() < 1) {
                    _is_more_data = false;
                    return;
                }

                if (blog_list != null && blog_list.length() > 0) {
                    _list.addItems(blog_list);
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);


                if (_open_no > 0) {
                    for (int i = 0; i < list.length(); i++) {
                        JSONObject info = JSONUtil.getJSONObject(list, i);

                        int no = JSONUtil.getInteger(info, "no");
                        if (no == _open_no) {

                            final int index = i;


                            _list.smoothScrollByOffset(index);
                            JSONUtil.puts(info, "IS_OPEN", "Y");
                            JSONUtil.put(list, info, index);
                            _list.getAdapter().notifyDataSetChanged();


                            callApi_notice_read(no);
                            
                            break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_notice_read(int no) {

        LogUtil.e("==========callApi_notice_read : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        String ssid = _type.equals("notice") ? "notice.read" : "faq.read";

        call = apiInterface.notice_read(
                ssid
                , SPUtil.getInstance().getUserNoEnc(getContext())
                , no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //rogressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(final GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        RelativeLayout baseTitle = (RelativeLayout) convertView.findViewById(R.id.baseTitle);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        ImageView imgArrow = (ImageView) convertView.findViewById(R.id.imgArrow);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);
        final LinearLayout baseDetail = (LinearLayout) convertView.findViewById(R.id.baseDetail);
        TextView txtContent = (TextView) convertView.findViewById(R.id.txtContent);
        SquareImageView imgContent = convertView.findViewById(R.id.imgContent);

        final String IS_OPEN = JSONUtil.getString(json, "IS_OPEN", "N");
        final int no = JSONUtil.getInteger(json, "no", 0);
        final int b_no = JSONUtil.getInteger(json, "b_no", 0);

        String img_path = JSONUtil.getString(json, "img_path");
        final String url = JSONUtil.getString(json, "url");

        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        if (reg_dttm.length() > 10) {
            reg_dttm = reg_dttm.substring(0, 10);
        } else {
            reg_dttm = FormatUtil.getCurrentDate();
        }
        reg_dttm = reg_dttm.replace("-", ".");

        String cont = JSONUtil.getString(json, "cont");
        cont = cont.replaceAll("\n", "<br>");

        LogUtil.d("cont : " + cont);


        baseDetail.setVisibility(IS_OPEN.equals("Y") ? View.VISIBLE : View.GONE);

        txtTitle.setText(Html.fromHtml(JSONUtil.getString(json, "subject")));
        txtContent.setLinksClickable(true);
        txtContent.setMovementMethod(LinkMovementMethod.getInstance());
        txtContent.setText(Html.fromHtml(cont));
        txtDate.setText(reg_dttm);

        imgArrow.setImageResource(IS_OPEN.equals("Y") ? R.drawable.btn_img_up : R.drawable.btn_img_down);

        if (!FormatUtil.isNullorEmpty(img_path)) {
            Picasso.with(getContext())
                    .load(img_path)
                    //.fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgContent);
            imgContent.setVisibility(View.VISIBLE);
        } else {
            imgContent.setVisibility(View.GONE);
        }

        baseTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (IS_OPEN.equals("N")) {
                    callApi_notice_read(no);
                }

                if (b_no > 0 || !FormatUtil.isNullorEmpty(url)) {
                    Intent i = new Intent();
                    i.setClass(getContext(), AtvWeb.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "blog");
                    i.putExtra(Constants.EXTRAS_URL, url);
                    startActivity(i);
                } else {
                    try {
                        JSONUtil.puts(json, "IS_OPEN", "Y".endsWith(IS_OPEN) ? "N" : "Y");
                        adapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        return convertView;
    }

}
