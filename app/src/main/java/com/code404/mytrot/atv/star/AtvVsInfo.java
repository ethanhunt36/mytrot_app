package com.code404.mytrot.atv.star;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.vod.AtvVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.code404.mytrot.widget.ItemVsVod;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvVsInfo extends AtvBase implements GListView.IMakeView {


    private View _header = null;

    private EditText _edtReply = null;
    private Button _btnSend = null;


    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;

    private TextView _txtTip01 = null;
    private TextView _txtTip02 = null;
    private TextView _txtRead = null;
    private TextView _txtTitle = null;
    private TextView _txtNick = null;
    private TextView _txtContent = null;
    private TextView _txtPicCount = null;
    private TextView _txtViewVod = null;
    private ItemVsVod _itemVsVod_A = null;
    private ItemVsVod _itemVsVod_B = null;


    // 작성한 회원 번호
    private String _star_member_no = "";

    // 열람 중인 회원 번호
    private String _enter_member_no = "";

    private JSONArray _jsonArrayComment = null;
    JSONArray _vod_items = null;

    int _my_pick_item_no = -1;
    int _board_no = -1;

    private boolean _is_display_ad = false;

    @Override
    protected boolean getBundle() {
        _board_no = getIntent().getIntExtra(Constants.EXTRAS_BOARD_NO, 0);

        return _board_no > 0;
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_vs_info);
    }

    @Override
    protected void findView() {
        _edtReply = (EditText) findViewById(R.id.edtReply);
        _btnSend = (Button) findViewById(R.id.btnSend);

        _header = View.inflate(getContext(), R.layout.row_vs_info, null);

        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        _txtTip01 = (TextView) _header.findViewById(R.id.txtTip01);
        _txtTip02 = (TextView) _header.findViewById(R.id.txtTip02);
        _txtRead = (TextView) _header.findViewById(R.id.txtRead);
        _txtTitle = (TextView) _header.findViewById(R.id.txtTitle);
        _txtNick = (TextView) _header.findViewById(R.id.txtNick);
        _txtContent = (TextView) _header.findViewById(R.id.txtContent);
        _txtPicCount = (TextView) _header.findViewById(R.id.txtPicCount);
        _txtViewVod = (TextView) _header.findViewById(R.id.txtViewVod);
        _itemVsVod_A = (ItemVsVod) _header.findViewById(R.id.itemVsVod_A);
        _itemVsVod_B = (ItemVsVod) _header.findViewById(R.id.itemVsVod_B);


    }

    @Override
    protected void init() {
        _txtTopTitle.setText("VS");
        _baseTopBottom.setVisibility(View.GONE);
        _txtTip01.setVisibility(View.GONE);

        _list.addHeaderView(_header);

        _list.setViewMaker(R.layout.row_comment, this);
        //_list.setNoData(_baseNoData);

        _enter_member_no = SPUtil.getInstance().getUserNoEnc(getContext());

        setDataBinding(null);
        callApi_vs_view(false);
    }


    @Override
    protected void configureListener() {

        _itemVsVod_A.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                int item_no = pos;
                onVoteVs(item_no);
            }
        });

        _itemVsVod_B.setOnClickViewListener(new InterfaceSet.OnClickViewListener() {
            @Override
            public void onClick(View v, int pos) {
                int item_no = pos;
                onVoteVs(item_no);
            }
        });

        _btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cont = _edtReply.getText().toString().trim();

                if (FormatUtil.isNullorEmpty(cont)) {
                    new Alert().showAlert(getContext(), "내용을 입력해 주세요.");
                    return;
                }

                callApi_board_cmt_ins(cont);
            }
        });

        _txtViewVod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AdViewUtil.goActionAfterAdView(AtvVsInfo.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        SPUtil.getInstance().setYoutubeJsonArray(getContext(), _vod_items);

                        SPUtil.getInstance().setBoardEnterShort(getContext());

                        Intent i = new Intent();
                        i.setClass(getContext(), AtvVod.class);
                        //i.putExtra(Constants.EXTRAS_VOD_LIST_JSON_STRING, jsonList.toString());
                        i.putExtra(Constants.EXTRAS_VOD_NO, -1);
                        i.putExtra(Constants.EXTRAS_TITLE, "VS");
                        startActivity(i);
                    }
                });


            }
        });

        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    //callApi_alram_listing(false);
                }
            }
        });
    }


    private void onVoteVs(final int item_no) {
        if (_my_pick_item_no == item_no) {
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    if (which == Alert.BUTTON1) {
                        callApi_vs_inc_pick(item_no);
                    }
                }
            });
            alert.showAlert(getContext(), "VS 투표를 취소하시겠습니까?", false, "투표 취소하기", "닫기");
            return;
        }

        if (_my_pick_item_no > 0) {
            new Alert().showAlert(getContext(), "투표하셨던 내역을 먼저 취소해주세요.");
            return;
        }

        callApi_vs_inc_pick(item_no);
    }


    private void setDataBinding(JSONObject json) {

        JSONObject member_info = JSONUtil.getJSONObject(json, "member_info");
        JSONObject my_pick = JSONUtil.getJSONObject(json, "my_pick");

        _my_pick_item_no = JSONUtil.getInteger(my_pick, "item_no", -1);

        int is_hot = JSONUtil.getInteger(json, "is_hot", 0);
        int read_cnt = JSONUtil.getInteger(json, "read_cnt", 0);
        int vs_pick_cnt = JSONUtil.getInteger(json, "vs_pick_cnt", 0);

        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        _txtTip01.setVisibility(is_hot == 1 ? View.VISIBLE : View.GONE);
        _txtRead.setText(String.format("%s명 조회 | %s", FormatUtil.toPriceFormat(read_cnt), FormatUtil.getReplyDateFormat(reg_dttm)));
        _txtTitle.setText(JSONUtil.getString(json, "title"));

        //_txtNick.setText(JSONUtil.getString(json, "nick") + " Lv." + JSONUtil.getString(json, "lv"));
        _txtNick.setText(JSONUtil.getString(member_info, "nick") + " Lv." + JSONUtil.getString(member_info, "lv"));

        _txtContent.setText(Html.fromHtml(JSONUtil.getString(json, "cont")));

        _txtPicCount.setText(FormatUtil.toPriceFormat(vs_pick_cnt) + "명");

        _vod_items = JSONUtil.getJSONArray(json, "item");

        JSONObject itemA = JSONUtil.getJSONObject(_vod_items, 0);
        JSONObject itemB = JSONUtil.getJSONObject(_vod_items, 1);

        _itemVsVod_A.setData(itemA, vs_pick_cnt, _my_pick_item_no);
        _itemVsVod_B.setData(itemB, vs_pick_cnt, _my_pick_item_no);
    }


    private void callApi_vs_inc_pick(int item_no) {
        LogUtil.e("==========callApi_vs_inc_pick : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vs_inc_pick(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _board_no
                , item_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    callApi_vs_view(true);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);

                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_vs_view(final boolean isClear) {
        LogUtil.e("==========callApi_vs_view : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vs_view(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _board_no
                , Constants.RECORD_SIZE_MAX
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    if (isClear) {
                        _list.removeAll();
                    }

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject data = JSONUtil.getJSONObject(json, "data");

                    JSONObject view_data = JSONUtil.getJSONObject(data, "view_data");
                    JSONObject comment_data = JSONUtil.getJSONObject(data, "comment_data");
                    _jsonArrayComment = JSONUtil.getJSONArray(comment_data, "list_data");

                    setDataBinding(view_data);

                    if (_jsonArrayComment == null || _jsonArrayComment.length() < 1) {
                        _jsonArrayComment = new JSONArray();
                        JSONObject jsonRow = new JSONObject();
                        JSONUtil.puts(jsonRow, "cont", "첫 번째 댓글을 남겨주세요~");
                        JSONUtil.puts(jsonRow, "IS_NO_DATA", "Y");
                        _jsonArrayComment.put(jsonRow);
                    }
                    _list.addItems(_jsonArrayComment);
                    //_list.setNoDataVisibility(_list.getCountAll() < 1);

                    LogUtil.d("jsonList, size : " + _jsonArrayComment.length());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);

                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_board_cmt_ins(String cont) {

        LogUtil.e("==========callApi_board_cmt_ins : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);


        Call<JsonObject> call = apiInterface.board_cmt_ins(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                _board_no,
                cont
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (!FormatUtil.isNullorEmpty(resultMessage)) {
                    Alert.toastLong(getContext(), resultMessage);
                }

                if (resultCode != 0) {
                    return;
                }

                _edtReply.setText("");
                _list.removeAll();

                callApi_vs_view(true);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_board_cmt_del(final int position, int comment_no) {

        LogUtil.e("==========callApi_board_cmt_del : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_cmt_del(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                comment_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);

                    LogUtil.json(json);

                    JSONObject j = _list.getAdapter().getItem(position);
                    _list.getAdapter().remove(j);
                    _list.getAdapter().notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        JSONObject member_info = JSONUtil.getJSONObject(json, "member_info");

        String IS_NO_DATA = JSONUtil.getString(json, "IS_NO_DATA");

        TextView txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
        TextView txtComment = (TextView) convertView.findViewById(R.id.txtComment);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtNoData = (TextView) convertView.findViewById(R.id.txtNoData);
        ImageButton btnOption = (ImageButton) convertView.findViewById(R.id.btnOption);

        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);
        View baseLine = convertView.findViewById(R.id.baseLine);

        btnOption.setVisibility(View.GONE);

        final int comment_no = JSONUtil.getInteger(json, "no");

        if (IS_NO_DATA.equals("Y")) {
            txtUserName.setVisibility(View.GONE);
            txtDate.setVisibility(View.GONE);
            txtComment.setVisibility(View.GONE);
            txtNoData.setText(JSONUtil.getString(json, "cont"));
            txtNoData.setVisibility(View.VISIBLE);
            adViewBody.setVisibility(View.GONE);

            //convertView.setBackgroundColor(getResources().getColor(R.color.pale_grey));
        } else {
            String member_no = JSONUtil.getString(member_info, "member_no");
            txtUserName.setText(JSONUtil.getString(member_info, "nick") + " Lv." + JSONUtil.getString(member_info, "lv"));
            String reg_dttm = JSONUtil.getString(json, "reg_dttm");
            txtDate.setText(FormatUtil.getReplyDateFormat(reg_dttm));

            txtUserName.setVisibility(View.VISIBLE);
            txtDate.setVisibility(View.VISIBLE);
            txtComment.setVisibility(View.VISIBLE);
            txtNoData.setVisibility(View.GONE);

            txtComment.setLinksClickable(true);
            txtComment.setMovementMethod(LinkMovementMethod.getInstance());
            txtComment.setText(Html.fromHtml(JSONUtil.getString(json, "cont")));

            if (Constants.DISPLAY_AD
                    && Constants.DISPLAY_AD_ROW
                    && _is_display_ad
                    && (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD)
                    && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
            ) {
                adViewBody.setVisibility(View.VISIBLE);
                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);

//                FrameLayout adViewContainer = convertView.findViewById(R.id.adViewContainer);
//                AdView adViewNew = new AdView(getContext());
//                adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
//                adViewContainer.addView(adViewNew);
//
//                AdRequest adRequest = new AdRequest.Builder().build();
//                adViewNew.setAdSize(getAdSizeInfo());
//                adViewNew.loadAd(adRequest);

            } else {
                adViewBody.setVisibility(View.GONE);
            }

            if (member_no.equals(_enter_member_no)) {
                btnOption.setVisibility(View.VISIBLE);
            }

            //convertView.setBackgroundColor(getResources().getColor(R.color.white));
        }

        if (position >= adapter.getCount() - 1) {
            baseLine.setVisibility(View.INVISIBLE);
        } else {
            baseLine.setVisibility(View.VISIBLE);
        }

        btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] items = new String[]{"삭제하기", "취소"};
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        LogUtil.d("onClose, which : " + which);

                        if (which == 0) {
                            // 삭제
                            Alert alert1 = new Alert();
                            alert1.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                @Override
                                public void onClose(DialogInterface dialog, int which) {
                                    if (which == Alert.BUTTON1) {
                                        callApi_board_cmt_del(position, comment_no);
                                    }
                                }
                            });
                            alert1.showAlert(getContext(), "삭제 하시겠습니까?");
                        }
                    }
                });
                alert.showSeletItem(getContext(), items);
            }
        });

        return convertView;
    }

}
