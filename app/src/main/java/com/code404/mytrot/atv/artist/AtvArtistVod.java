package com.code404.mytrot.atv.artist;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.like.AtvLike;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.vod.AtvVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.VodBindUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


public class AtvArtistVod extends AtvBase implements GListView.IMakeView {

    private static String TAG = "AtvArtistVod";

    private GListView _list = null;
    private View _baseNoData = null;

    private View _header = null;
    private TextView _txtSeqRecent = null;
    private TextView _txtSeqName = null;
    private TextView _txtSeqPlay = null;
    private TextView _txtSeqScore = null;

    private TextView _txtGotoFavorite = null;

    private int _artist_no = -1;
    private String _title = "";
    private String _keyword = "";
    private String _sort = "recent";

    private VodBindUtil _vodBindUtil = null;

    JSONArray _jsonVodList = null;

    @Override
    protected boolean getBundle() {
        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _title = getIntent().getStringExtra(Constants.EXTRAS_TITLE);
        _keyword = getIntent().getStringExtra(Constants.EXTRAS_KEYWORD);

        LogUtil.d(TAG, "_artist_no : " + _artist_no);
        LogUtil.d(TAG, "_title : " + _title);
        LogUtil.d(TAG, "_keyword : " + _keyword);
        return true;
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_artist_vod);
    }


    @Override
    protected void init() {
        _menuIndex = 1;

        _txtTopTitle.setText(_title);
        _btnTopBack.setVisibility(View.GONE);
        _txtSeqScore.setVisibility(View.VISIBLE);

        _txtGotoFavorite.setVisibility(View.VISIBLE);

        _list.setNoData(_baseNoData);
        _list.setViewMaker(R.layout.row_vod, this);

        _vodBindUtil = new VodBindUtil();


        _txtSeqScore.setVisibility(View.GONE);

        AdViewUtil.ready(AtvArtistVod.this, getContext());
    }

    @Override
    protected void findView() {
        _list = findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_trot, null);

        _txtSeqRecent = _header.findViewById(R.id.txtSeqRecent);
        _txtSeqName = _header.findViewById(R.id.txtSeqName);
        _txtSeqPlay = _header.findViewById(R.id.txtSeqPlay);
        _txtSeqScore = _header.findViewById(R.id.txtSeqScore);

        _txtGotoFavorite = _header.findViewById(R.id.txtGotoFavorite);
        _list.addHeaderView(_header);
    }


    @Override
    protected void configureListener() {


        _txtGotoFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdViewUtil.goActionAfterAdView(AtvArtistVod.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvLike.class);
                        startActivity(i);
                    }
                });
            }
        });

        _txtSeqRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "recent";
                _txtSeqRecent.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqName.setBackground(null);
                _txtSeqPlay.setBackground(null);
                _txtSeqScore.setBackground(null);
                callApi_artistvod_listing();
            }
        });

        _txtSeqName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "title";
                _txtSeqName.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqRecent.setBackground(null);
                _txtSeqPlay.setBackground(null);
                _txtSeqScore.setBackground(null);
                callApi_artistvod_listing();
            }
        });

        _txtSeqPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "play";
                _txtSeqPlay.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqRecent.setBackground(null);
                _txtSeqName.setBackground(null);
                _txtSeqScore.setBackground(null);
                callApi_artistvod_listing();
            }
        });

        _txtSeqScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "editor_score";
                _txtSeqScore.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqRecent.setBackground(null);
                _txtSeqName.setBackground(null);
                _txtSeqPlay.setBackground(null);
                callApi_artistvod_listing();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        callApi_artistvod_listing();
    }


    private void callApi_artistvod_listing() {
        LogUtil.e("==========callApi_artistvod_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        String keyword = !FormatUtil.isNullorEmpty(_keyword) ? _keyword.trim() : "";
        String srch_artist_name = "";
        String srch_vod_name = "";

        if (!FormatUtil.isNullorEmpty(keyword) && keyword.contains(" ") && keyword.split(" ").length == 2) {
            String[] temps = keyword.split(" ");
            srch_artist_name = temps[0];
            srch_vod_name = temps[1];
        }

        Call<JsonObject> call = apiInterface.artistvod_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
                , keyword
                , _sort
                , Constants.RECORD_SIZE_MAX
                , "-1"
                , srch_artist_name
                , srch_vod_name
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                _jsonVodList = JSONUtil.getJSONArray(jsonData, "list_data");

                if (_list != null) {
                    _list.removeAll();
                    _list.addItems(_jsonVodList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);
                    //_baseNoData.setVisibility(_jsonVodList.length() > 0 ? View.GONE : View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);
        final int vod_no = JSONUtil.getInteger(json, "no");

        LogUtil.json(json);
        convertView = _vodBindUtil.setViewEvent(getContext(), json, _jsonVodList, convertView, position, _list.getAdapter(), -1, true);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String todayDate = FormatUtil.getCurrentDate();
                String alertDate = SPUtil.getInstance().getYoutubeAlertDate(getContext());

                if (!todayDate.equals(alertDate)) {

                    AlertPop alert = new AlertPop();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            SPUtil.getInstance().setYoutubeAlertDate(getContext(), todayDate);

                            goVod(vod_no);
                        }
                    });
                    alert.showYoutudePolicy(getContext());

                } else {
                    goVod(vod_no);
                }
            }
        });

        return convertView;
    }

    private void goVod(int vod_no) {


        AdViewUtil.goActionAfterAdView(this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
            @Override
            public void onAfterAction() {
                SPUtil.getInstance().setYoutubeJsonArray(getContext(), _jsonVodList);

                SPUtil.getInstance().setBoardEnterShort(getContext());

                Intent i = new Intent();
                i.setClass(getContext(), AtvVod.class);
                //i.putExtra(Constants.EXTRAS_VOD_LIST_JSON_STRING, _jsonVodList.toString());
                i.putExtra(Constants.EXTRAS_VOD_NO, vod_no);
                startActivity(i);
            }
        });
    }
}
