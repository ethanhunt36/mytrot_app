package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.SkuDetails;
import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.widget.ItemNativeAd;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;

public class AtvQuiz extends AtvBase {

    private static String TAG = "AtvQuiz";

    private View _baseVote = null;
    private TextView _txtVoteCount = null;
    private View _baseHint = null;
    private TextView _txtHintPoint = null;
    private TextView _txtRemainQuiz = null;
    private View _baseQuizCharge = null;
    private TextView _txtPlus = null;
    private TextView _txtSubject = null;
    private TextView _txtQTitle = null;
    private Button _btnA01 = null;
    private Button _btnA02 = null;
    private Button _btnA03 = null;
    private Button _btnA04 = null;
    private Button _btnA05 = null;
    private Button _btnA06 = null;
    private TextView _txtDesc = null;
    private Button _btn01 = null;
    private Button _btn02 = null;
    private Button _btn03 = null;
    private Button _btn04 = null;
    private Button _btn05 = null;
    private Button _btn06 = null;
    private Button _btn11 = null;
    private Button _btn12 = null;
    private Button _btn13 = null;
    private Button _btn14 = null;
    private Button _btn15 = null;
    private Button _btn16 = null;


    private List<Button> _btnAnswerList = new ArrayList<>();
    private List<Button> _btnChoiceList = new ArrayList<>();


    private int _quiz_no = -1;
    private int _remain_count = -1;
    private int _quiz_max_count = -1;
    private int _point_hint = -1;
    private int _unit_correct_ad = 2;

    private String _answer = "";

    private boolean _isAlertHint = false;


    private String _use_ad_charge = "";
    private String _msg_quiz_end = "";


    @Override
    protected boolean getBundle() {
        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_quiz);
    }

    @Override
    protected void findView() {


        _baseVote = findViewById(R.id.baseVote);
        _txtVoteCount = (TextView) findViewById(R.id.txtVoteCount);
        _baseHint = findViewById(R.id.baseHint);
        _txtRemainQuiz = (TextView) findViewById(R.id.txtRemainQuiz);
        _baseQuizCharge = findViewById(R.id.baseQuizCharge);
        _txtSubject = (TextView) findViewById(R.id.txtSubject);
        _txtQTitle = (TextView) findViewById(R.id.txtQTitle);
        _btnA01 = (Button) findViewById(R.id.btnA01);
        _btnA02 = (Button) findViewById(R.id.btnA02);
        _btnA03 = (Button) findViewById(R.id.btnA03);
        _btnA04 = (Button) findViewById(R.id.btnA04);
        _btnA05 = (Button) findViewById(R.id.btnA05);
        _btnA06 = (Button) findViewById(R.id.btnA06);
        _txtDesc = (TextView) findViewById(R.id.txtDesc);
        _btn01 = (Button) findViewById(R.id.btn01);
        _btn02 = (Button) findViewById(R.id.btn02);
        _btn03 = (Button) findViewById(R.id.btn03);
        _btn04 = (Button) findViewById(R.id.btn04);
        _btn05 = (Button) findViewById(R.id.btn05);
        _btn06 = (Button) findViewById(R.id.btn06);
        _btn11 = (Button) findViewById(R.id.btn11);
        _btn12 = (Button) findViewById(R.id.btn12);
        _btn13 = (Button) findViewById(R.id.btn13);
        _btn14 = (Button) findViewById(R.id.btn14);
        _btn15 = (Button) findViewById(R.id.btn15);
        _btn16 = (Button) findViewById(R.id.btn16);

        _txtPlus = findViewById(R.id.txtPlus);
        _txtHintPoint = findViewById(R.id.txtHintPoint);

        _itemNativeAd = findViewById(R.id.itemNativeAd);

    }

    @Override
    protected void init() {
        _txtTopTitle.setText("트로트 퀴즈");
        _baseTopBottom.setVisibility(View.GONE);

        _btnAnswerList.add(_btnA01);
        _btnAnswerList.add(_btnA02);
        _btnAnswerList.add(_btnA03);
        _btnAnswerList.add(_btnA04);
        _btnAnswerList.add(_btnA05);
        _btnAnswerList.add(_btnA06);

        _btnChoiceList.add(_btn01);
        _btnChoiceList.add(_btn02);
        _btnChoiceList.add(_btn03);
        _btnChoiceList.add(_btn04);
        _btnChoiceList.add(_btn05);
        _btnChoiceList.add(_btn06);

        _btnChoiceList.add(_btn11);
        _btnChoiceList.add(_btn12);
        _btnChoiceList.add(_btn13);
        _btnChoiceList.add(_btn14);
        _btnChoiceList.add(_btn15);
        _btnChoiceList.add(_btn16);


        callApi_zQuiz_get_info();

        showNativeAd(true);
    }


    private View _baseBottom = null;
    private ItemNativeAd _itemNativeAd = null;

    private void showNativeAd(boolean isFirstInit) {

        LogUtil.d("showNativeAd, _remain_count : " + _remain_count);
        FrameLayout adViewContainer = findViewById(R.id.adViewContainer);

        if (_remain_count % 10 == 5 || isFirstInit) {

            //_baseBottom.setVisibility(View.VISIBLE);
            adViewContainer.setVisibility(View.GONE);

            _itemNativeAd.setVisibility(View.VISIBLE);
            _itemNativeAd.initAd();


        } else if (_remain_count % 10 == 0) {


            _itemNativeAd.setVisibility(View.GONE);
            adViewContainer.setVisibility(View.VISIBLE);

            AdView adViewNew = new AdView(getContext());
            adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
            adViewContainer.addView(adViewNew);

            AdRequest adRequest = new AdRequest.Builder().build();
            adViewNew.setAdSize(getAdSizeInfo());
            adViewNew.loadAd(adRequest);


        }
    }


    @Override
    protected void configureListener() {

        for (Button b : _btnAnswerList) {
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String a = b.getText().toString();

                    b.setText("");

                    for (Button btnC : _btnChoiceList) {
                        if (btnC.getVisibility() == View.VISIBLE && !FormatUtil.isNullorEmpty(btnC.getText().toString())) {
                            if (btnC.getText().toString().equals(a)) {
                                // 힌트 미선택 처리
                                btnC.setSelected(false);
                                btnC.setTextColor(getResources().getColor(R.color.black));
                                break;
                            }
                        }
                    }
                }
            });
        }


        // 보기 클릭
        for (Button b : _btnChoiceList) {
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (_remain_count < 1) {
                        //Alert.toastLong(getContext(), "광고를 보고 퀴즈를 충전해주세요.");
                        checkEndQuizOrCharge();
                        return;
                    }

                    LogUtil.d("(b.isSelected() : " + (b.isSelected()));

                    if (b.isSelected() == true) {
                        return;
                    }

                    String t = b.getText().toString();

                    for (Button btnA : _btnAnswerList) {
                        if (btnA.getVisibility() == View.VISIBLE
                                && FormatUtil.isNullorEmpty(btnA.getText().toString())) {


                            btnA.setText(t);

                            // 힌트 선택 처리
                            b.setSelected(true);
                            b.setTextColor(getResources().getColor(R.color.white_f8));
                            checkAnswerCorret();
                            break;


                        }
                    }
                }
            });
        }

        //
        _baseHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (FormatUtil.isNullorEmpty(_answer)) {
                    new Alert().showAlert(getContext(), "이용할 수 없습니다.");
                    return;
                }

                if (_answer.length() == 1) {
                    new Alert().showAlert(getContext(), "답이 한 글자라서, 이용할 수 없습니다.");
                    return;
                }

                if (_isAlertHint == false) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (which == Alert.BUTTON1) {
                                callApi_zQuiz_use_hint();
                                _isAlertHint = true;
                            }
                        }
                    });
                    alert.showAlert(getContext(), "힌트 사용 시 " + _point_hint + "P가 사용됩니다.\n힌트를 사용하시겠습니까?", false, "확인", "취소");
                    return;
                }

                callApi_zQuiz_use_hint();
            }
        });


        // 퀴즈 충전
        _baseQuizCharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_remain_count >= 2) {
                    Alert.toastLong(getContext(), "퀴즈가 아직 남아 있습니다.");
                    return;
                }

                if (_try_charge_count < 2 && AdViewUtil.isReadyCheck(AtvQuiz.this, getContext()) == false) {
                    Alert.toastLong(getContext(), "광고가 로딩되지 않았습니다. 나중에 다시 시도해주세요.");
                    _try_charge_count++;
                    return;
                }

                if (_try_charge_count >= 2) {
                    callApi_zQuiz_charge_quiz();
                    _try_charge_count = 0;
                    return;
                }

                AdViewUtil.goActionAfterAdView(AtvQuiz.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {

                        LogUtil.d("_baseHint, onAfterAction");
                        // 광고 재로딩
                        AdViewUtil.ready(AtvQuiz.this, getContext(), true);

                        callApi_zQuiz_charge_quiz();
                    }
                });
            }
        });
    }


    private int _try_charge_count = 0;


    private void checkAnswerCorret() {

        boolean isInputAll = true;

        String inputAnswer = "";
        for (Button b : _btnAnswerList) {

            if (b.getVisibility() == View.GONE) continue;

            if (FormatUtil.isNullorEmpty(b.getText().toString())) {
                isInputAll = false;
                break;
            }

            inputAnswer += b.getText().toString();
        }

        LogUtil.d("_answer : " + _answer);
        LogUtil.d("inputAnswer : " + inputAnswer);

        if (isInputAll == false) return;

        if (!_answer.equals(inputAnswer)) {
            Alert.toastLong(getContext(), "오답입니다.");
            AdViewUtil.ready(AtvQuiz.this, getContext(), true);
            return;
        }


        Alert.toastLong(getContext(), "정답입니다.");

        callApi_zQuiz_correct(_quiz_no, inputAnswer);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    private void setDataBinding(JSONObject json) {

        JSONObject info = JSONUtil.getJSONObject(json, "info");
        JSONObject member_info = JSONUtil.getJSONObject(json, "m_info");

        String ans_random = JSONUtil.getString(json, "ans_random");

        _use_ad_charge = JSONUtil.getString(json, "use_ad_charge");
        _msg_quiz_end = JSONUtil.getString(json, "msg_quiz_end");
        _remain_count = JSONUtil.getInteger(json, "remain_count", 20);
        _quiz_max_count = JSONUtil.getInteger(json, "quiz_max_count", 20);
        _unit_correct_ad = JSONUtil.getInteger(json, "unit_correct_ad", 2);
        _point_hint = JSONUtil.getInteger(json, "point_hint", 1);

        if (_remain_count < 1) {
            //Alert.toastLong(getContext(), "광고를 보고 퀴즈를 충전해주세요.");
            if (!checkEndQuizOrCharge()) return;
        }

        showNativeAd(false);

        _txtHintPoint.setText(_point_hint + "P");


        String title = JSONUtil.getString(info, "title");

        _quiz_no = JSONUtil.getInteger(info, "no", -1);
        _answer = JSONUtil.getString(info, "answer");


        if (_quiz_no < 1 || FormatUtil.isNullorEmpty(_answer)) {
//            Alert alert = new Alert();
//            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                @Override
//                public void onClose(DialogInterface dialog, int which) {
//                    finish();
//                }
//            });
//            alert.showAlert(getContext(), "등록된 퀴즈가 없습니다.\n다음에 다시 이용해주세요.");
            if (!checkEndQuizOrCharge()) return;
        }

        String[] answerArr = CommonUtil.convertStringToArray(_answer);

        for (String a : answerArr) {
            ans_random = ans_random.replace(a, "");
        }


        int size = _btnChoiceList.size();

        ans_random = _answer + ans_random;

        LogUtil.d("size : " + size);
        LogUtil.d("ans_random : " + ans_random);
        ans_random = ans_random.substring(0, size);
        LogUtil.d("ans_random : " + ans_random);

        String[] ans_randomArr = CommonUtil.convertStringToArray(ans_random);

        // 배열을 List로 변환
        List<String> list = Arrays.asList(ans_randomArr);
        Collections.shuffle(list);
        ans_randomArr = list.toArray(new String[0]);


        _txtQTitle.setText(title);
        _txtPlus.setText("+" + _quiz_max_count + "개");

        for (Button b : _btnAnswerList) {
            b.setVisibility(View.GONE);
        }

        for (int i = 0; i < answerArr.length; i++) {
            if (_btnAnswerList.size() > i) {
                Button b = _btnAnswerList.get(i);
                b.setVisibility(View.VISIBLE);
                b.setText("");
            }
        }


        for (Button b : _btnChoiceList) {
            b.setVisibility(View.GONE);
        }

        for (int i = 0; i < ans_randomArr.length; i++) {
            if (i < _btnChoiceList.size()) {
                Button b = _btnChoiceList.get(i);
                b.setVisibility(View.VISIBLE);
                b.setText(ans_randomArr[i]);
                // 힌트 미선택 처리
                b.setSelected(false);
                b.setTextColor(getResources().getColor(R.color.black));
            }
        }


        // 회원정보

        int vote_cnt = JSONUtil.getInteger(member_info, "vote_cnt", 0);

        _txtVoteCount.setText("투표권 " + FormatUtil.toPriceFormat(vote_cnt) + "장");
        _txtRemainQuiz.setText("퀴즈 " + (_remain_count) + "개");

        _baseQuizCharge.setVisibility(_use_ad_charge.equals("Y") ? View.VISIBLE : View.GONE);
    }


    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.d("AtvQuiz, onResume");
        try {
            AdViewUtil.ready(AtvQuiz.this, getContext(), true);
        } catch (Exception e) {

        }
    }


    private void callApi_zQuiz_get_info() {

        LogUtil.e("==========callApi_zQuiz_get_info : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.zQuiz_get_info(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    //new Alert().showAlert(getContext(), resultMessage);
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");


                setDataBinding(data);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_zQuiz_correct(int quiz_no, String answer) {

        LogUtil.e("========== callApi_zQuiz_correct : start ==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.zQuiz_correct(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , quiz_no
                , answer
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    //new Alert().showAlert(getContext(), resultMessage);
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                    return;
                }

                LogUtil.d("callApi_zQuiz_correct, _remain_count : " + _remain_count);
                LogUtil.d("callApi_zQuiz_correct, AdViewUtil : " + AdViewUtil.isReadyCheck(AtvQuiz.this, getContext()));

                LogUtil.d("callApi_zQuiz_correct, _remain_count : " + _remain_count);
                LogUtil.d("callApi_zQuiz_correct, _unit_correct_ad : " + _unit_correct_ad);


                JSONObject data = JSONUtil.getJSONObject(json, "data");


                if (_remain_count == 1) {
                    if (AdViewUtil.isReadyCheck(AtvQuiz.this, getContext())) {
                        setDataBinding(data);

                        _remain_count = 0;
                        _txtRemainQuiz.setText("퀴즈 " + (_remain_count) + "개");


                        checkEndQuizOrCharge();

                        return;
                    } else {
                        new Alert().showAlert(getContext(), "로딩된 광고가 없어서 자동으로 퀴즈 충전이 완료 되었습니다.");
                    }
                }

                setDataBinding(data);

                if (_remain_count % _unit_correct_ad == 0 && AdViewUtil.isReadyCheck(AtvQuiz.this, getContext())) {
                    AdViewUtil.goActionAfterAdView(AtvQuiz.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                        @Override
                        public void onAfterAction() {
                            AdViewUtil.ready(AtvQuiz.this, getContext(), true);
                        }
                    });
                } else if (AdViewUtil.isReadyCheck(AtvQuiz.this, getContext()) == false) {
                    AdViewUtil.ready(AtvQuiz.this, getContext(), true);
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private boolean checkEndQuizOrCharge() {
        if (_use_ad_charge.equals("Y")) {
            new Alert().showAlert(getContext(), _msg_quiz_end);
            return true;
        } else {
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.showAlert(getContext(), _msg_quiz_end);
            return false;
        }
    }


    private void callApi_zQuiz_charge_quiz() {

        LogUtil.e("========== callApi_zQuiz_charge_quiz : start ==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.zQuiz_charge_quiz(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    //new Alert().showAlert(getContext(), resultMessage);
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                setDataBinding(data);

                AdViewUtil.goActionAfterAdView(AtvQuiz.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        AdViewUtil.ready(AtvQuiz.this, getContext(), true);
                    }
                });
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_zQuiz_use_hint() {

        LogUtil.e("========== callApi_zQuiz_use_hint : start ==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.zQuiz_use_hint(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    //new Alert().showAlert(getContext(), resultMessage);
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONObject userJson = JSONUtil.getJSONObject(data, "m_info");

                SPUtil.getInstance().setUserInfo(getContext(), userJson);

                String msg = _answer;

                msg = FormatUtil.insertRandomAsterisks(msg, 1);

                new Alert().showAlert(getContext(), msg);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }
}
