package com.code404.mytrot.atv.web;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.AtvIntro;
import com.code404.mytrot.atv.etc.AtvPuzzleRank;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AppOpenAdManager;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.widget.ItemNativeAd;
//import com.fsn.cauly.CaulyAdView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;

public class AtvWeb extends AtvBase {

    private WebView _webView = null;
    private String _type = "";
    private String _url = "";

    private WebViewInterface _webViewInterface = null;

    private View _baseBottom = null;
    private ItemNativeAd _itemNativeAd = null;
//    private CaulyAdView _adViewCauly = null;

    private View _baseBottomAdAuto = null;
    private View _baseClose = null;
    private ImageButton _btnClose = null;
    private TextView _txtCountDown = null;

    private boolean _isPageFinish = false;
    private boolean _isPageScroll = false;
    private boolean _isAfterSeconds = false;
    private boolean _isFinishCountDown = false;

    private static List<Integer> _newspic_no_list = new ArrayList<Integer>();

    public static int _newspic_no = -1;
    public static boolean _newspic_x = false;

    private Handler _handler = null;
    private Runnable _runnable = null;
    private Runnable _runnableMoveAtv = null;

    private Handler _countDownhandler = new Handler();
    private Runnable _countDownRunnable = null;

    private static int _viewCount = 0;

    public static int _failedHour = -1;

    private int _wait_seconds = 13;


    private String _prevUrl = "http://ponong.co.kr/bbs/board.php?bo_table=free";

    @Override
    protected boolean getBundle() {

        _type = getIntent().getStringExtra(Constants.EXTRAS_TYPE);
        _url = getIntent().getStringExtra(Constants.EXTRAS_URL);

        _newspic_no = getIntent().getIntExtra(Constants.EXTRAS_NO, 0);

        LogUtil.d("AtvWeb, _type : " + _type);
        LogUtil.d("AtvWeb, _newspic_no : " + _newspic_no);
        LogUtil.d("AtvWeb, _url : " + _url);

        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    protected void setView() {
        setView(R.layout.atv_web);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void init() {

        _btnTopBack.setVisibility(View.VISIBLE);
        _baseTopBottom.setVisibility(View.GONE);
        _baseBottomAdAuto.setVisibility(View.GONE);
//        _adViewCauly.setVisibility(View.GONE);
        _itemNativeAd.setVisibility(View.GONE);

        _txtCountDown.setText(String.valueOf(_wait_seconds));

        LogUtil.d("_webView : " + (_webView == null ? "null" : "ok"));

        WebSettings settings = _webView.getSettings();
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(false);

        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        //settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        settings.setLoadWithOverviewMode(true);

        settings.setDomStorageEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        if (android.os.Build.VERSION.SDK_INT >= 11) {
            initWeb_SDK11();
        }

        _webView.setScrollbarFadingEnabled(true);
        _webView.setHorizontalScrollBarEnabled(false);
        _webView.setVerticalScrollBarEnabled(true);


        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            _webView.getSettings().setTextZoom(100);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            _webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }


        if (_type.startsWith("READ_NEWSPIC") && !_url.contains("adid") && !_url.contains("ponong")) {
            _url += "&adid=" + SPUtil.getInstance().getAndroidID(getContext()) + "&packagename=com.code404.mytrot";
        }

        LogUtil.d("AtvWeb", "_type  : " + _type);
        LogUtil.d("AtvWeb", "url  : " + _url);


        if ("agree".equals(_type)) {
            _txtTopTitle.setText("이용 약관");
            _webView.loadUrl(Constants.getServerUrl() + "/agree/agree_use.html");
        } else if ("blog".equals(_type)) {
            _txtTopTitle.setText("마이트롯 블로그");

            if (!FormatUtil.isNullorEmpty(_url)) {
                _webView.loadUrl(_url);
            } else {
                _url = SPUtil.getInstance().getBlogUrl(getContext());
                _webView.loadUrl(_url);
            }
        } else if ("puzzle".equals(_type)) {
            _txtTopTitle.setText("퍼즐 게임");

            showNativeAd();

            _url = Constants.getServerUrl() + "/game/slidepuzzle/index.htm?member_no=" + SPUtil.getInstance().getUserNoEnc(getContext()) + "&level=3";

            _webView.loadUrl(_url);

            _webViewInterface = new WebViewInterface(AtvWeb.this, _webView);
            _webView.addJavascriptInterface(_webViewInterface, "mytrot");

        } else if ("READ_PODONG".equals(_type)) {
            _txtTopTitle.setText("유머글");
            _webView.loadUrl(_url, getPonongRefererHeader());
        } else if ("READ_NEWSPIC_A".equals(_type) || "READ_NEWSPIC_B".equals(_type)) {
            _baseClose.setVisibility(View.VISIBLE);


            _baseTopTitle.setVisibility(View.GONE);


            _webView.loadUrl(_url, getPonongRefererHeader());
            initCountDown();
        } else if ("READ_NEWSPIC_C".equals(_type) || "READ_NEWSPIC_D".equals(_type) || "READ_NEWSPIC_X".equals(_type)) {
            _baseClose.setVisibility(View.VISIBLE);
            _baseTopTitle.setVisibility(View.GONE);

            if (_newspic_no > 0 && !FormatUtil.isNullorEmpty(_url)) {

                LogUtil.d("url  : " + _url);

                _webView.loadUrl(_url, getPonongRefererHeader());
                initCountDown();
            } else {
                callApi_newspic_get_newspic_info();
            }
        } else if ("READ_NEWSPIC_ATTEND".equals(_type)) {
            _baseClose.setVisibility(View.VISIBLE);
            initCountDown();
            _baseTopTitle.setVisibility(View.GONE);
            callApi_newspic_get_newspic_info();
        }

        if (("blog".equals(_type) || "READ_PODONG".equals(_type) || _type.startsWith("READ_NEWSPIC"))) {

            _viewCount++;


            if (Constants.DEBUG) {
                if (SPUtil.getInstance().getAutoViewNewspickYn(getContext()).equals("Y")) {
                    _baseBottomAdAuto.setBackgroundColor(getColor(R.color.violet_blue));
                    _baseBottom.setBackgroundColor(getColor(R.color.violet_blue));
                }
                _baseBottomAdAuto.setVisibility(View.VISIBLE);

                showNativeAd();

                if (Constants.IS_VIEW_AD_GOOGLE) {
                    if (_viewCount % 2 == 0) {
                        // 카울리 배너
//                        _adViewCauly.setVisibility(View.VISIBLE);
                    } else {
                        // 구글 배너
                        FrameLayout adViewContainer = findViewById(R.id.adViewContainer);

                        adViewContainer.setVisibility(View.VISIBLE);

                        AdView adViewNew = new AdView(getContext());
                        adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
                        adViewContainer.addView(adViewNew);

                        AdRequest adRequest = new AdRequest.Builder().build();
                        adViewNew.setAdSize(getAdSizeInfo());
                        adViewNew.loadAd(adRequest);
                    }
                } else {
                    // 카울리 배너
//                    _adViewCauly.setVisibility(View.VISIBLE);
                }
            }
        }

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                _isAfterSeconds = true;
            }
        }, 8000);
    }


    private Map<String, String> getPonongRefererHeader() {
        Map<String, String> headers = new HashMap<String, String>();

        LogUtil.d("AtvWeb", "_prevUrl : " + _prevUrl);

        headers.put("Referer", _prevUrl);

        return headers;
    }


    private void initAuto() {

        if ("READ_NEWSPIC_X".equals(_type) && _newspic_no > 0) {

            // 화면 꺼짐 방지
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            _newspic_x = true;

            _handler = new Handler();

            _runnableMoveAtv = new Runnable() {
                @Override
                public void run() {

                    LogUtil.d("Runnable, _runnableMoveAtv");

                    setResult(RESULT_OK);
                    finish();

                }
            };

            _runnable = new Runnable() {
                @Override
                public void run() {

                    LogUtil.d("Runnable, _runnable[1]");

                    _webView.scrollTo(0, 4000);

                    _handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            LogUtil.d("Runnable, _runnable[2]");
                            _webView.scrollTo(0, 9000);
                        }
                    }, 1500);

                    _handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            LogUtil.d("Runnable, _runnable[3]");
                            _webView.scrollTo(0, 999000);
                        }
                    }, (_wait_seconds - 7) * 1000);

                    _handler.postDelayed(_runnableMoveAtv, _wait_seconds * 1000 + CommonUtil.getRandom());
                }
            };

            _handler.postDelayed(_runnable, 4000 + CommonUtil.getRandom());


        }
    }


    private void showNativeAd() {
        _baseBottom.setVisibility(View.VISIBLE);
        _itemNativeAd.setVisibility(View.VISIBLE);
        _itemNativeAd.initAd();
    }


    private void callApi_newspic_complete() {

        if (isFinishing() || isDestroyed()) {
            if (Constants.DEBUG) {
                Alert.toastShort(getContext(), "isFinishing() : " + isFinishing() + ", isDestroyed() : " + isDestroyed());
            }
            return;
        }

        if (_isPageScroll == false) {
            Alert.toastShort(getContext(), "화면 맨아래까지 스크롤을 움직여야 적립이 됩니다.");
            return;
        }

        String ad_type = Constants.enum_ad.newspic_a.name();
        if ("READ_NEWSPIC_B".equals(_type))
            ad_type = Constants.enum_ad.newspic_b.name();
        if ("READ_NEWSPIC_C".equals(_type))
            ad_type = Constants.enum_ad.newspic_c.name();
        if ("READ_NEWSPIC_D".equals(_type))
            ad_type = Constants.enum_ad.newspic_d.name();
        if ("READ_NEWSPIC_X".equals(_type))
            ad_type = Constants.enum_ad.newspic_x.name();

        callApi_vote_put_ticket_ad_point(ad_type, _newspic_no);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBackPressed() {
        //super.onBack();

        removeHandler();

        if ("READ_PODONG".equals(_type) || "READ_NEWSPIC_A".equals(_type) || "READ_NEWSPIC_B".equals(_type) || "READ_NEWSPIC_C".equals(_type) || "READ_NEWSPIC_D".equals(_type) || "READ_NEWSPIC_X".equals(_type) || "READ_NEWSPIC_ATTEND".equals(_type)) {
            String url = _webView.getUrl();
            LogUtil.d("_webView.canGoBack() : " + _webView.canGoBack() + ", url : " + url);

            if (_webView.canGoBack() && url.startsWith("http")) {
                _webView.goBack();
            } else {
                super.onBackPressed();
            }
            return;
        }


        super.onBackPressed();
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void removeHandler() {

        if (_webView != null) {
            _webView.setOnScrollChangeListener(null);
        }

        if (_handler != null) {
            if (_runnable != null) _handler.removeCallbacks(_runnable);
            if (_runnableMoveAtv != null) _handler.removeCallbacks(_runnableMoveAtv);

            _handler.removeCallbacksAndMessages(null);

            _handler = null;
            _runnable = null;
            _runnableMoveAtv = null;
        }

        if (_countDownhandler != null) {
            if (_countDownRunnable != null) _countDownhandler.removeCallbacks(_countDownRunnable);

            _countDownhandler = null;
            _countDownRunnable = null;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onBack() {
        super.onBack();

        removeHandler();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onDestroy() {
        removeHandler();
        super.onDestroy();
    }


    @Override
    protected void findView() {
        _webView = (WebView) findViewById(R.id.webView);
        _itemNativeAd = findViewById(R.id.itemNativeAd);
        _baseBottom = findViewById(R.id.baseBottom);
        _baseBottomAdAuto = findViewById(R.id.baseBottomAdAuto);
//        _adViewCauly = findViewById(R.id.adViewCauly);
        _btnClose = findViewById(R.id.btnClose);
        _baseClose = findViewById(R.id.baseClose);
        _txtCountDown = findViewById(R.id.txtCountDown);
    }


    @Override
    protected void configureListener() {
        _btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _newspic_x = false;
                finish();
            }
        });


        _webView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {

                int contentHeight = _webView.getContentHeight();
                int scroll_y = i1;

                LogUtil.d(String.format("onScrollChange i : %d, scroll_y : %d, i : %d, i : %d, _webView.getContentHeight() : %d", i, i1, i2, i3, contentHeight));
                LogUtil.d("onScrollChange : _isPageFinish : " + _isPageFinish + ", _isAfterSeconds : " + _isAfterSeconds + ", _isPageScroll : " + _isPageScroll + ", _isFinishCountDown : " + _isFinishCountDown);
                LogUtil.d("onScrollChange : contentHeight : " + contentHeight);
                LogUtil.d("onScrollChange : scroll_y : " + scroll_y);
                if (_isPageScroll == false && contentHeight < scroll_y) {
                    _isPageScroll = true;
                }

                if (("READ_NEWSPIC_A".equals(_type) || "READ_NEWSPIC_B".equals(_type) || "READ_NEWSPIC_C".equals(_type)
                        || "READ_NEWSPIC_D".equals(_type) || "READ_NEWSPIC_X".equals(_type))
                        && _isPageFinish
                        //&& _isAfterSeconds
                        && _isPageScroll
                        && _isFinishCountDown
                        && contentHeight < scroll_y) {


                    if (_isFinishCountDown && isFinishing() == false && isDestroyed() == false) {
                        callApi_newspic_complete();
                    }
                }
            }
        });


        _webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                //return super.shouldOverrideUrlLoading(view, request);

                String url = request.getUrl().toString();

                LogUtil.d("webview", "shouldOverrideUrlLoading");
                LogUtil.d("webview", "url : " + url);

                if (url.startsWith("coupang://")) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    } catch (ActivityNotFoundException e) {
                        Intent webIntent = new Intent(Intent.ACTION_VIEW);
                        webIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.coupang.mobile"));
                        if (webIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(webIntent);
                        }
                    }

                    finish();

                    return true;
                } else if (url.contains("www.instagram.com")) {
                    try {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(i);
                    } catch (ActivityNotFoundException e) {
                        Intent webIntent = new Intent(Intent.ACTION_VIEW);
                        webIntent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.instagram.android"));
                        if (webIntent.resolveActivity(getPackageManager()) != null) {
                            startActivity(webIntent);
                        }
                    }
                    return true;
                }

                Map<String, String> headers = new HashMap<String, String>();

                LogUtil.d("AtvWeb", "_prevUrl : " + _prevUrl);

                view.loadUrl(url, getPonongRefererHeader());

                if (url.contains("ponong.co.kr")) _prevUrl = url;

                return true;
            }

            @Override
            public void onPageStarted(final WebView view, final String url, Bitmap favicon) {
                LogUtil.d("webview", "onPageStarted");
            }

            @Override
            public void onPageFinished(final WebView view, String url) {
                LogUtil.d("webview", "inside onPageFinished");
                _isPageFinish = true;
            }


            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {

                try {
                    LogUtil.d("webview", "onReceivedError");
                    LogUtil.d("webview", "onReceivedError, _url : " + _url);
                    LogUtil.d("webview", "onReceivedError, failingUrl : " + failingUrl);

                    if (!failingUrl.startsWith("mailto:")) {
                    }

                    if (failingUrl.startsWith("coupang://")) {
                        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(_url));
                        getContext().startActivity(i);
                        //Alert.toastLong(getContext(), "뉴스픽에서 삭제된 기사입니다.\n기사를 재로딩합니다.");
                        //callApi_newspic_report();
                    }

                    if (failingUrl.startsWith("intent://")) {
                        try {
                            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(failingUrl));
                            getContext().startActivity(i);
                        } catch (Exception ex) {
                            Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(_url));
                            getContext().startActivity(i);
                        }
                    }

                    if (!failingUrl.startsWith("http")) {
                        //Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(_url));
                        //getContext().startActivity(i);
                        finish();
                    }

                    _failedHour = FormatUtil.getCurrentHour();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void initCountDown() {

        if (_url.contains("ponong")) _wait_seconds = SPUtil.getInstance().getSecondsViewAdpick(getContext());

        _countDownRunnable = new Runnable() {
            @Override
            public void run() {
                CountDownTimer timer = new CountDownTimer(_wait_seconds * 1000 + 990, 1000L) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        // Do nothing
                        LogUtil.d("onTick, millisUntilFinished : " + millisUntilFinished);
                        _isFinishCountDown = false;
                        _txtCountDown.setText(String.valueOf((int) millisUntilFinished / 1000));
                    }

                    @Override
                    public void onFinish() {

                        LogUtil.d("onTick, onFinish");

                        _txtCountDown.setText("0");
                        _isFinishCountDown = true;
                        callApi_newspic_complete();


                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                _txtCountDown.setVisibility(View.GONE);
                            }
                        }, 1000);
                    }
                };
                timer.start();
            }
        };

        _countDownhandler.postDelayed(_countDownRunnable, 1000);
    }


    @SuppressLint("NewApi")
    private void initWeb_SDK11() {
        WebSettings settings = _webView.getSettings();

        _webView.setMotionEventSplittingEnabled(true);

        // settings.setEnableSmoothTransition(true);
        settings.setAllowFileAccess(true);
        if (android.os.Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(true);
        }
    }


    public class WebViewInterface {

        private WebView mWebView;
        private Activity mContext;
        private final Handler handler = new Handler();


        public WebViewInterface(Activity activity, WebView view) {
            mWebView = view;
            mContext = activity;
        }

        @JavascriptInterface
        public void finish_puzzle(String duration, String moveCnt) {

            String msg = String.format("%s회 이동하여 %s초 만에 성공하였습니다.", moveCnt, duration);

            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    finish();

                    Intent i = new Intent();
                    i.setClass(getContext(), AtvPuzzleRank.class);
                    startActivity(i);
                }
            });
            alert.showAlert(getContext(), msg);
        }
    }


    private void callApi_vote_put_ticket_ad_point(final String ad_type, int newspic_no) {

        if (CommonUtil.checkDoubleTab()) {
            LogUtil.e("_txtAd, callApi_vote_put_ticket_ad_point, 이벤트 중복 발생");
            return;
        }

        if (_newspic_no_list == null) {
            _newspic_no_list = new ArrayList<Integer>();
        }

        if (newspic_no > 0 && _newspic_no_list.contains(newspic_no)) {
            LogUtil.e("_txtAd, _newspic_no_list contains : " + newspic_no);
            return;
        }

        _newspic_no_list.add(newspic_no);

        LogUtil.e("==========callApi_vote_put_ticket_ad_point : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_put_ticket_ad_point(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , ad_type
                , SPUtil.getInstance().getIsHaveBoosterItem(getContext()) ? "Y" : "N"
                , SPUtil.getInstance().getAndroidID(getContext())
                , 0
                , newspic_no
                , SPUtil.getInstance().getChargeMode(getContext())
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                try {
                    LogUtil.json(json);


                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    if (resultCode == 0) {
                        JSONObject data = JSONUtil.getJSONObject(json, "data");
                        JSONObject userInfo = JSONUtil.getJSONObject(data, "member");

                        LogUtil.json(userInfo);

                        SPUtil.getInstance().setUserInfo(getContext(), userInfo);

                        final int amount_ticket_ad = JSONUtil.getInteger(data, "amount_ticket_ad", -1);
                        final double point_amount = JSONUtil.getDouble(data, "point_amount", -1);

                        Alert.toastLong(getContext(), resultMessage);

                        //setMyVoteCountPoint(userInfo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_newspic_get_newspic_info() {
        LogUtil.e("==========callApi_newspic_get_newspic_info : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.newspic_get_newspic_info(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                try {
                    LogUtil.json(json);
                    JSONObject data = JSONUtil.getJSONObject(json, "data");


                    _newspic_no = JSONUtil.getInteger(data, "newspic_no");
                    _url = JSONUtil.getStringUrl(data, "newspic_url");

                    if (_newspic_no_list == null) {
                        _newspic_no_list = new ArrayList<Integer>();
                    }

                    if (FormatUtil.isNullorEmpty(_url)) {
                        String msg = "볼 수 있는 오늘의핫이슈 기사가 없습니다.\n다음 정각 시각에 새로운 기사가 충전될 예정입니다.";
                        Alert alert = new Alert();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        alert.showAlert(getContext(), msg);
                        return;
                    }


                    if (!_url.contains("adid")) {
                        _url += "&adid=" + SPUtil.getInstance().getAndroidID(getContext()) + "&packagename=com.code404.mytrot";
                    }

//                    if (_url.contains("ponong.co.kr")) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_url));
//                        startActivity(intent);
//
//                        Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                callApi_vote_put_ticket_ad_point("ponong", _newspic_no);
//                                finish();
//                            }
//                        }, 1000);
//                        return;
//                    }

                    _webView.loadUrl(_url, getPonongRefererHeader());

                    initCountDown();
                    initAuto();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_newspic_report() {
        LogUtil.e("==========callApi_newspic_report : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.newspic_report(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _newspic_no
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                try {

                    finish();

                    Intent i = new Intent();
                    i.setClass(getContext(), AtvWeb.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "READ_NEWSPIC_C");
                    i.putExtra(Constants.EXTRAS_NO, -1);
                    i.putExtra(Constants.EXTRAS_URL, "");
                    startActivity(i);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

}
