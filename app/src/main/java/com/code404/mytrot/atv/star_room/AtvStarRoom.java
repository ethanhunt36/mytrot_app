package com.code404.mytrot.atv.star_room;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.star.AtvStar;
import com.code404.mytrot.atv.star.AtvStarWrite;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SquareImageView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


// 별방
public class AtvStarRoom extends AtvBase implements GListView.IMakeView {

    private int _artist_no = -1;
    private String _artist_name = "";
    private String _artist_board_id = "";


    private static final int REQ_BOARD_INFO = 7419;
    private static final int REQ_WRITE = 3333;

    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;

    private String _last_no = "-1";
    private String _sort = "recent";

    private boolean _is_call_api_ing = false;

    JSONArray _jsonVodList = null;
    private boolean _is_more_data = true;

    private int _category = 1;


    @Override
    protected boolean getBundle() {
        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);
        _category = getIntent().getIntExtra(Constants.EXTRAS_TYPE, -1);

        if (_artist_no < 1 || FormatUtil.isNullorEmpty(_artist_name) || FormatUtil.isNullorEmpty(_artist_board_id)) {
            JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

            int artist_no = JSONUtil.getInteger(json, "artist_no");
            String artist_name = JSONUtil.getString(json, "artist_name");
            String artist_board_id = JSONUtil.getString(json, "artist_board_id");

            if (artist_no > 0) _artist_no = artist_no;
            if (!FormatUtil.isNullorEmpty(artist_name)) _artist_name = artist_name;
            if (!FormatUtil.isNullorEmpty(artist_board_id)) _artist_board_id = artist_board_id;
        }

        return _artist_no > 0;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_borad);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = findViewById(R.id.txtNoData);

        _list.setNoData(_baseNoData);
    }


    @Override
    protected void init() {

        if(_category == 1) {
            _txtTopTitle.setText("별방 오픈 이야기");
            _txtNoData.setText("별방에서 첫 오픈 이야기를 들려주세요.");
        }
        if(_category == 99) {
            _txtTopTitle.setText("별방 비밀 이야기");
            _txtNoData.setText("별방에서 첫 비밀 이야기를 들려주세요.");
        }

        _baseTopBottom.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_board, this);

        callApi_board_listing(true);
    }


    private void callApi_list(String sort) {
        try {
            _sort = sort;
            _is_more_data = true;
            _last_no = "-1";
            callApi_board_listing(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void configureListener() {

        // 응원글 작성
        findViewById(R.id.btnWrite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AdViewUtil.goActionAfterAdView(AtvStarRoom.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
                        intent.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
                        intent.putExtra(Constants.EXTRAS_TYPE, _category);
                        intent.setClass(getContext(), AtvStarWrite.class);
                        startActivityForResult(intent, REQ_WRITE);
                    }
                });


//                int boardEnterCount = SPUtil.getInstance().getBoardEnterCount(getContext());
//                int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(getContext());
//                LogUtil.d("boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);
//
//               if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null && _adFullOrgManager.canShowAdCount_Interstitial(true,false) > 0
//                        && boardEnterCount % repeatBoardWriteAd == 0) {
//                    _adFullOrgManager.setOnAdCompleteMissionListener(new InterfaceSet.OnAdCompleteListener() {
//                        @Override
//                        public void onAdLoadSuccess() {
//                            LogUtil.d("_txtAd, AtvStar, onAdLoadSuccess");
//                        }
//
//                        @Override
//                        public void onAdLoadFail() {
//                            LogUtil.d("_txtAd, AtvStar, onAdLoadFail");
//                        }
//
//                        @Override
//                        public void onAdClose() {
//                            LogUtil.d("_txtAd, AtvStar, onAdClose");
//
//                            _adFullOrgManager.setOnAdCompleteMissionListener(null);
//
//                            Intent intent = new Intent();
//                            intent.setClass(getContext(), AtvStarWrite.class);
//                            intent.putExtra(Constants.EXTRAS_TYPE, _category);
//                            startActivityForResult(intent, REQ_WRITE);
//                        }
//                    });
//                    _adFullOrgManager.showAdOrder_Interstitial();
//                    return;
//                }
//
//                Intent intent = new Intent();
//                intent.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
//                intent.putExtra(Constants.EXTRAS_ARTIST_NO, _artist_no);
//                intent.putExtra(Constants.EXTRAS_TYPE, _category);
//                intent.setClass(getContext(), AtvStarWrite.class);
//                startActivityForResult(intent, REQ_WRITE);

            }
        });


        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom() && !_sort.equals("st")) {
                    callApi_board_listing(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // callApi_board_listing(true);

        AdViewUtil.ready(AtvStarRoom.this, getContext());
    }


    private void callApi_board_listing(final boolean isClear) {
        LogUtil.e("==========callApi_board_listing : start==========");

        if (_is_more_data == false || _is_call_api_ing == true) {
            return;
        }
        _is_call_api_ing = true;

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.boardStar_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_board_id
                , _category
                , Constants.RECORD_SIZE
                , _last_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    _is_call_api_ing = false;
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    _jsonVodList = JSONUtil.getJSONArray(jsonData, "list_data");

                    _last_no = JSONUtil.getString(jsonData, "last_no");

                    if (isClear) {
                        _list.removeAll();
                    }

                    if (_jsonVodList.length() < 1) {
                        _is_more_data = false;
                        return;
                    }

                    _list.addItems(_jsonVodList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);

                    if (isClear) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                _list.smoothScrollToPosition(0);
                            }
                        }, 500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ;
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                _is_call_api_ing = false;
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_board_inc_like(final int position, final int board_no) {
        LogUtil.e("==========callApi_board_inc_like : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.boardStar_inc_like(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , board_no
                , _artist_board_id
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

//                try {
//                    if (resultCode == 0) {
//                        String data = JSONUtil.getString(json, "data");
//
//                        JSONObject jsonRow = _list.getItem(position);
//
//                        int like_cnt = JSONUtil.getInteger(jsonRow, "like_cnt");
//                        int my_like_cnt = 0;
//                        if (data.equals("inc")) {
//                            like_cnt++;
//                            my_like_cnt = 1;
//                        } else {
//                            like_cnt--;
//                        }
//
//                        JSONUtil.puts(jsonRow, "like_cnt", like_cnt);
//                        JSONUtil.puts(jsonRow, "my_like_cnt", my_like_cnt);
//
//                        _list.getAdapter().notifyDataSetChanged();
//                    } else {
//                        if (!FormatUtil.isNullorEmpty(resultMessage)) {
//                            new Alert().showAlert(getContext(), resultMessage);
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private int _position = -1;

    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);


        final int bbs_no = JSONUtil.getInteger(json, "no");

        de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
        SquareImageView img01 = convertView.findViewById(R.id.img01);
        TextView txtNickname = (TextView) convertView.findViewById(R.id.txtNickname);
        TextView txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtLike = (TextView) convertView.findViewById(R.id.txtLike);
        TextView txtHate = (TextView) convertView.findViewById(R.id.txtHate);
        TextView txtComment = (TextView) convertView.findViewById(R.id.txtComment);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);
        View baseLeft = (View) convertView.findViewById(R.id.baseLeft);
        View baseRight = (View) convertView.findViewById(R.id.baseRight);


        String img01Url = JSONUtil.getStringUrl(json, "img01");
        String artist_name = JSONUtil.getString(json, "artist_name");

        int my_like_cnt = JSONUtil.getInteger(json, "my_like_cnt", 0);
        int board_no = JSONUtil.getInteger(json, "board_no");

        LogUtil.d("my_like_cnt : " + my_like_cnt);
        LogUtil.d("img01Url : " + img01Url);

        baseLeft.setVisibility(View.GONE);
        baseRight.setVisibility(View.VISIBLE);

        String cont = JSONUtil.getString(json, "cont");

        cont = FormatUtil.replaceTag(cont);
        String reg_dttm = JSONUtil.getString(json, "reg_dttm");

        txtNickname.setText(JSONUtil.getString(json, "nick") + " Lv." + JSONUtil.getString(json, "lv"));
        txtMessage.setText(Html.fromHtml(cont));
        txtDate.setText(FormatUtil.convertDateAmPmRemoveToday(reg_dttm));

        String strLike = "";
        if (my_like_cnt > 0) {
            strLike = "좋아요 <b>" + FormatUtil.toPriceFormat(JSONUtil.getString(json, "like_cnt")) + "<b>";
            txtLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bt_heart_on, 0, 0, 0);
        } else {
            strLike = "좋아요 " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "like_cnt"));
            txtLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bt_heart_off, 0, 0, 0);
        }

        txtLike.setText(Html.fromHtml(strLike));
        txtHate.setText("싫어요 " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "hate_cnt")));
        txtComment.setText("댓글 " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "comment_cnt")));

        if (!FormatUtil.isNullorEmpty(img01Url)) {
            // 모서리 라운딩 처리
            img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
            img01.setClipToOutline(true);

            Picasso.with(getContext())
                    .load(img01Url)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(img01);
            img01.setVisibility(View.VISIBLE);
        } else {
            //Picasso.with(getContext()).load(R.drawable.img_noimg).fit();
            img01.setVisibility(View.GONE);
        }

        txtHate.setVisibility(View.GONE);

        View adViewBody = convertView.findViewById(R.id.adViewBody);

        AdView adView = convertView.findViewById(R.id.adView);

        if (Constants.DISPLAY_AD
                && Constants.DISPLAY_AD_ROW
                && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD
                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
            // && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
        ) {
            adViewBody.setVisibility(View.VISIBLE);
//                AdRequest adRequest = new AdRequest.Builder().build();
//
//                LogUtil.d("getAdSize, adView.getAdSize() : " + (adView.getAdSize() == null ? "null" : adView.getAdSize().toString()));
//
//                if(adView.getAdSize() == null) {
//                    adView.setAdSize(getAdSizeInfo());
//                }
//                adView.loadAd(adRequest);

            FrameLayout adViewContainer = convertView.findViewById(R.id.adViewContainer);
            AdView adViewNew = new AdView(getContext());
            adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
            adViewContainer.addView(adViewNew);

            AdRequest adRequest = new AdRequest.Builder().build();
            adViewNew.setAdSize(getAdSizeInfo());
            adViewNew.loadAd(adRequest);
        } else {
            adViewBody.setVisibility(View.GONE);
        }

        txtLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    callApi_board_inc_like(position, bbs_no);

                    // api 호출 후 ui 는 즉각적으로 처리
                    JSONObject jsonRow = _list.getItem(position);

                    int like_cnt = JSONUtil.getInteger(jsonRow, "like_cnt");
                    int my_like_cnt = JSONUtil.getInteger(jsonRow, "my_like_cnt", 0);
                    if (my_like_cnt < 1) {
                        like_cnt++;
                        my_like_cnt = 1;
                    } else {
                        like_cnt--;
                        my_like_cnt = 0;
                    }

                    JSONUtil.puts(jsonRow, "like_cnt", like_cnt);
                    JSONUtil.puts(jsonRow, "my_like_cnt", my_like_cnt);

                    _list.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {

                }
            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _position = position;

                Intent i = new Intent();
                i.setClass(getContext(), AtvStarRoomInfo.class);
                i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, _artist_board_id);
                i.putExtra(Constants.EXTRAS_JSON_STRING, json.toString());

                startActivityForResult(i, REQ_BOARD_INFO);
            }
        });


        return convertView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("FrgBoard, onActivityResult, requestCode : " + requestCode);
        LogUtil.d("FrgBoard, onActivityResult, resultCode : " + resultCode);
        LogUtil.intent(data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_WRITE) {
                callApi_list("recent");
            } else if (requestCode == REQ_BOARD_INFO) {
                try {
                    JSONObject jsonNew = JSONUtil.createObject(data.getStringExtra(Constants.EXTRAS_JSON_STRING));

                    JSONObject json = _list.getAdapter().getItem(_position);

                    int is_ilike = JSONUtil.getInteger(jsonNew, "is_ilike");
                    int like_cnt = JSONUtil.getInteger(jsonNew, "like_cnt");
                    int comment_cnt = JSONUtil.getInteger(jsonNew, "comment_cnt");

                    String del_yn = JSONUtil.getString(jsonNew, "del_yn");

                    if ("Y".equals(del_yn)) {
                        _list.getAdapter().remove(json);
                    } else {
                        JSONUtil.puts(json, "is_ilike", is_ilike);
                        JSONUtil.puts(json, "my_like_cnt", is_ilike);
                        JSONUtil.puts(json, "like_cnt", like_cnt);
                        JSONUtil.puts(json, "comment_cnt", comment_cnt);
                    }

                    _list.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
