package com.code404.mytrot.atv.vote;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.code404.mytrot.R;
import com.code404.mytrot.adapter.MyFragmentPagerAdapter;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.attend.AtvAttend;
import com.code404.mytrot.atv.etc.AtvAlrim;
import com.code404.mytrot.atv.more.AtvMoreChargeB;
import com.code404.mytrot.atv.more.AtvMoreNotice;
import com.code404.mytrot.atv.more.AtvMorePromo;
import com.code404.mytrot.atv.star.AtvStarInfo;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.frg.vote.FrgBoard;
import com.code404.mytrot.frg.vote.FrgDonate;
import com.code404.mytrot.frg.vote.FrgEventFund;
import com.code404.mytrot.frg.vote.FrgMission;
import com.code404.mytrot.frg.vote.FrgVote;
import com.code404.mytrot.frg.vote.FrgVs;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.android.gms.tasks.Task;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.gson.JsonObject;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;


public class AtvVote extends AtvBase {

    private int _selectedIndex = -1;

    private MyFragmentPagerAdapter _adapter = null;
    private com.code404.mytrot.view.SlideTabView _slideTab = null;
    private ViewPager _pager = null;

    private static int _tryAttend = 0;
    private final static int MY_REQUEST_CODE = 0;

    private int _frg_index = 0;

    private boolean _isOpenBoard = true;

    // 버전체크 알림창 띄운 적이 있는지
    private static boolean _isCheckVersion = false;

    @Override
    protected boolean getBundle() {

        _frg_index = getIntent().getIntExtra(Constants.EXTRAS_FRG_INDEX, 0);


        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_artist);
    }


    @Override
    protected void init() {


        _menuIndex = 2;
        _canFinish = true;
        _txtTopTitle.setText("마이트롯");

        _btnTopBack.setVisibility(View.VISIBLE);
        _btnTopBack.setImageResource(R.drawable.bt_top_alim_over);

        _btnTopRight.setVisibility(View.GONE);

        _txtTopRight.setText("출석부");
        _txtTopRight.setVisibility(View.VISIBLE);

        _adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());


        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
        int lv = JSONUtil.getInteger(json, "lv");

        if (!SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") || lv < SPUtil.getInstance().getAvailableBoardLevel(getContext())) {
            _isOpenBoard = false;
        }

        _slideTab.setTabViewCount(_isOpenBoard ? 6 : 3);

        _slideTab.addTab("투표");

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            _slideTab.addTab("기부");

        } else {
            _slideTab.addTab("기부펀딩");
        }

        if (_isOpenBoard) {
            _slideTab.addTab("광고");
            _slideTab.addTab("응원");
            _slideTab.addTab("VS");
        }

        _slideTab.addTab("미션");

        _slideTab.setTabOn(_frg_index);


        FrgVote frgVote = new FrgVote();
        FrgDonate frgDonate = new FrgDonate();
        FrgBoard frgBoard = new FrgBoard();
        FrgVs frgVs = new FrgVs();
        FrgMission frgMission = new FrgMission();
        FrgEventFund frgEventFund = new FrgEventFund();

        _adapter.addItem(frgVote);
        _adapter.addItem(frgDonate);

        if (_isOpenBoard) {
            _adapter.addItem(frgEventFund);
            _adapter.addItem(frgBoard);
            _adapter.addItem(frgVs);
        }
        _adapter.addItem(frgMission);


        _pager.setAdapter(_adapter);
        _pager.setCurrentItem(_frg_index);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (AtvVote.this.isFinishing()) {
                    return;
                }

                String date = SPUtil.getInstance().getAttendDate(getContext());
                String currentDate = FormatUtil.getCurrentDate();

                int enterHomeCount = SPUtil.getInstance().getEnterHomeCount(getContext());


                if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                    if (!currentDate.equals(date)) {

                        if (_tryAttend < 2) {

                            AdViewUtil.ready(AtvVote.this, getContext(), true);

                            _adFullOrgManager.setActivity(AtvVote.this);
                            _adFullOrgManager.setContext(getContext());
                            _adFullOrgManager.initAd(Constants.enum_ad.none, _isFirstInit);
//
//
                            String msg = getString(R.string.txt_attend);
                            Alert alert = new Alert();
                            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                @Override
                                public void onClose(DialogInterface dialog, int which) {
                                    if (which == Alert.BUTTON1) {
                                        onPlayAdAttence();
                                    } else if (which == Alert.BUTTON2) {
                                        gotoAttend();
                                    } else {

                                    }
                                }
                            });
                            alert.showAlert(getContext(), msg, false, "광고시청", "바로이동", "취소");
                        }

                        callApi_common_check_app_version();
                    } else if (enterHomeCount % 20 == 1) {
                        String msg = "오늘의 미션을 확인해보시겠습니까?";
                        Alert alert = new Alert();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    setPageTab(_isOpenBoard ? 5 : 3);
                                }
                            }
                        });
                        alert.showAlert(getContext(), msg, false, "확인", "취소");
                    } else if (enterHomeCount % 20 == 5) {
                        if (SPUtil.getInstance().getLastestNoticeNo(getContext()) > SPUtil.getInstance().getLastestNoticeNoViewed(getContext())) {

                            String msg = "새로 등록된 공지사항이 있습니다. \n\n확인해 보시겠습니까?";
                            Alert alert = new Alert();
                            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                @Override
                                public void onClose(DialogInterface dialog, int which) {
                                    if (which == Alert.BUTTON1) {
                                        Intent i = new Intent();
                                        i.setClass(getContext(), AtvMoreNotice.class);
                                        i.putExtra(Constants.EXTRAS_TYPE, "notice");
                                        startActivity(i);
                                    }
                                }
                            });
                            alert.showAlert(getContext(), msg, false, "확인", "취소");
                        }
                    }
                }

                enterHomeCount++;
                SPUtil.getInstance().setEnterHomeCount(getContext(), enterHomeCount);
            }
        }, 1000);


        callApi_memberMission_get();

        initPush();

        init_check_permission();

        init_version_check();
    }


    private void init_version_check() {

        LogUtil.d("init_version_check : " + SPUtil.getInstance().getAdFullScreen(getContext()));
        LogUtil.d("init_version_check : " + _isCheckVersion);

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false || _isCheckVersion)
            return;


        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());


        // 업데이트를 체크하는데 사용되는 인텐트를 리턴한다.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> { // appUpdateManager이 추가되는데 성공하면 발생하는 이벤트
            LogUtil.d("init_version_check", "appUpdateInfo.updateAvailability() : " + appUpdateInfo.updateAvailability());

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE // UpdateAvailability.UPDATE_AVAILABLE == 2 이면 앱 true
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) { // 허용된 타입의 앱 업데이트이면 실행 (AppUpdateType.IMMEDIATE || AppUpdateType.FLEXIBLE)
                // 업데이트가 가능하고, 상위 버전 코드의 앱이 존재하면 업데이트를 실행한다.
                //requestUpdate (appUpdateInfo);


                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        try {
                            if (which == Alert.BUTTON1) {
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(Constants.MARKET_URL));
                                startActivity(i);
                                finishAffinity();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                String msg = "플레이스토어에서 앱을 업데이트 받아주세요.";
                msg += "\n\n업데이트 받으시겠습니까?";

                alert.showAlert(getContext(), msg, false, "확인", "취소");


            }
        }).addOnFailureListener(e -> {
            //mHandler.sendEmptyMessage(MSG_INTENT_CHK);

            LogUtil.d("init_version_check", "실패");
        });

        _isCheckVersion = true;
    }


    private void requestUpdate(AppUpdateInfo appUpdateInfo) {
        try {

            AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(getApplicationContext());
            appUpdateManager.startUpdateFlowForResult(
                    // 'getAppUpdateInfo()' 에 의해 리턴된 인텐트
                    appUpdateInfo,
                    // 'AppUpdateType.FLEXIBLE': 사용자에게 업데이트 여부를 물은 후 업데이트 실행 가능
                    // 'AppUpdateType.IMMEDIATE': 사용자가 수락해야만 하는 업데이트 창을 보여줌
                    AppUpdateType.IMMEDIATE,
                    // 현재 업데이트 요청을 만든 액티비티, 여기선 MainActivity.
                    this,
                    // onActivityResult 에서 사용될 REQUEST_CODE.
                    MY_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private boolean _have_permission = false;


    private void init_check_permission() {
        TedPermission.with(getContext())
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        _have_permission = true;
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        // Possibility of denied even if this app granted permissions
                        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {
                            _have_permission = true;

                        } else {
                            _have_permission = false;
                        }
                    }
                })
                .setPermissions(Manifest.permission.POST_NOTIFICATIONS)
                .check();
    }

    /**
     * 푸시를 받아서 앱이 실행된 경우
     */
    private void initPush() {
        LogUtil.d("AtvMain------------------------------------------------------");
        LogUtil.intent(getIntent());
        LogUtil.d("------------------------------------------------------");


        if ("start_from_push".equals(getIntent().getAction())) {
            if (getIntent().getExtras() != null) {
                Bundle data = getIntent().getExtras().getBundle("push");
                if (data != null) {
                    String title = data.getString("subject");
                    String type = data.getString("type");
                    String message = data.getString("content");

                    LogUtil.d("title : " + title);
                    LogUtil.d("type : " + type);
                    LogUtil.d("message : " + message);

                    int push_type = !FormatUtil.isNullorEmpty(type) ? Integer.parseInt(type) : 0;

                    movePageFromPush(push_type, data);
                }
            }
        }
    }


    private void movePageFromPush(int push_type, Bundle data) {

        String title = data.getString("subject");
        String type = data.getString("type");
        String message = data.getString("content");
        String other_member_no = data.getString("other_member_no");
        String board_no = data.getString("board_no");
//        String msg_room_no = data.getString("msg_room_no");

        LogUtil.d("movePageFromPush, push_type : " + push_type);
        LogUtil.d("movePageFromPush, title : " + title);
        LogUtil.d("movePageFromPush, type : " + type);
        LogUtil.d("movePageFromPush, message : " + message);
        LogUtil.d("movePageFromPush, other_member_no : " + other_member_no);
        LogUtil.d("movePageFromPush, board_no : " + board_no);
//        LogUtil.d("movePageFromPush, msg_room_no : " + msg_room_no);

        int board_no_int = !FormatUtil.isNullorEmpty(board_no) ? Integer.parseInt(board_no) : 0;

        Intent i = new Intent();
        switch (push_type) {
            case 40:     // 댓글
            case 41:     // 대댓글
                i.setClass(getContext(), AtvStarInfo.class);
                i.putExtra(Constants.EXTRAS_BOARD_NO, board_no_int);
                startActivity(i);
                break;
        }
    }


    private void setPageTab(int pos) {
        try {
            _selectedIndex = pos;

            _pager.setCurrentItem(pos);
            _slideTab.setTabOn(pos);
            FrgBase frg = (FrgBase) _adapter.getItem(pos);

            if (frg != null) frg.refresh();
            LogUtil.d("pos ::: " + pos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void findView() {
        _slideTab = (com.code404.mytrot.view.SlideTabView) findViewById(R.id.slideTab);
        _pager = (ViewPager) findViewById(R.id.pager);
    }


    @Override
    protected void configureListener() {

        _btnTopBack.setVisibility(View.VISIBLE);
        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvAlrim.class);
                startActivity(i);

                _txtAlrimNew.setVisibility(View.GONE);
            }
        });

        _txtTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String date = SPUtil.getInstance().getAttendDate(getContext());
                String currentDate = FormatUtil.getCurrentDate();


                if (currentDate.equals(date) || SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                    gotoAttend();
                    return;
                }

                AdViewUtil.ready(AtvVote.this, getContext(), true);

                String msg = getString(R.string.txt_attend);
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            onPlayAdAttence();
                        } else if (which == Alert.BUTTON2) {
                            gotoAttend();
                        } else {

                        }
                    }
                });
                alert.showAlert(getContext(), msg, false, "광고시청", "바로이동", "취소");
            }
        });


        _slideTab.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    // mSlider.setSlideEnable(false);

                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    // if(mPager.getCurrentItem() == 0)
                    // {
                    // mSlider.setSlideEnable(true);
                    // }
                }
                return false;
            }
        });

        _slideTab.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
            @Override
            public void onClick(View v, int pos, JSONObject json) {
                setPageTab(pos);
            }
        });

        _pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int index) {
                _selectedIndex = index;

                LogUtil.d("onPageSelected : " + index);
                _slideTab.setTabOn(index);

                FrgBase frg = (FrgBase) _adapter.getItem(index);

//// 임시 테스트입니다. 배포 시 주석 풀어줘
//                if (_selectedIndex == 0 || _selectedIndex == 1 || _selectedIndex == 3) {
//                    if (frg != null) frg.refresh();
//                }
            }


            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // LogUtil.d("onPageScrolled : " + arg0);
            }


            @Override
            public void onPageScrollStateChanged(int index) {
                // LogUtil.d("onPageScrollStateChanged : " + index);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    /**
     * 각종 설정 서버 동기화 목적
     */
    private void callApi_common_check_app_version() {
        LogUtil.e("==========callApi_common_check_app_version : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.common_check_app_version(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.APP_TYPE_ID
                , CommonUtil.getCurrentVersion(getContext())
                , Constants.APP_DEVICE_ID
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject app_data = JSONUtil.getJSONObject(json, "data");

                JSONObject config_info = JSONUtil.getJSONObject(app_data, "config_info");
                SPUtil.getInstance().setAppConfig(getContext(), config_info);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private boolean _isViewAttendAd = false;

    private void callApi_attendance_attendance(final InterfaceSet.OnCompleteListener listener) {
        LogUtil.e("==========callApi_attendance_attendance : start==========");

        // 출석체크한 날짜 저장
        SPUtil.getInstance().setAttendDate(getContext(), FormatUtil.getCurrentDate());

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.attendance_attendance(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                try {
                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    //Alert.toastLong(getContext(), resultMessage);

                    if (listener != null) {
                        listener.onComplete();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_memberMission_get() {
        LogUtil.e("==========callApi_memberMission_get : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberMission_get(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject data = JSONUtil.getJSONObject(json, "data");

                    String reward_yn = JSONUtil.getString(data, "reward_yn");
                    String can_reward_yn = JSONUtil.getString(data, "can_reward_yn");
                    int push_unread_cnt = JSONUtil.getInteger(data, "push_unread_cnt");

                    LogUtil.d("push_unread_cnt : " + push_unread_cnt);

                    if ("Y".equals(can_reward_yn) && "N".equals(reward_yn)) {
                        Alert alert = new Alert();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                                     @Override
                                                     public void onClose(DialogInterface dialog, int which) {
                                                         setPageTab(_isOpenBoard ? 5 : 3);
                                                     }
                                                 }
                        );
                        alert.showAlert(getContext(), "미션 100% 달성하였습니다.\n\n미션 보상을 받으세요.");
                    }

                    _txtAlrimNew.setVisibility(push_unread_cnt > 0 ? View.VISIBLE : View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private static int _REQ_ATTENDANCE = 9922;

    private void gotoAttend() {
        gotoAttend(false);
    }

    private void gotoAttend(boolean needAd) {
        Intent i = new Intent();
        i.setClass(getContext(), AtvAttend.class);

        //startActivity(i);
        if (needAd)
            startActivityForResult(i, _REQ_ATTENDANCE);
        else
            startActivity(i);

    }

    private void onPlayAdAttence() {

        if (AdViewUtil.isReadyCheck(getActivity(), getContext())) {
            AdViewUtil.goActionAfterAdView(AtvVote.this, getContext(), true, false, new InterfaceSet.AdFullViewCompleteListener() {
                @Override
                public void onAfterAction() {
                    callApi_attendance_attendance(new InterfaceSet.OnCompleteListener() {
                        @Override
                        public void onComplete() {
                            gotoAttend();
                        }
                    });
                }
            });
        } else {
            gotoAttend(true);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);


        if (_REQ_ATTENDANCE == requestCode && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {

            int all_ad_count = _adFullOrgManager.canShowAdCount(false);
            int reward_ad_count = _adFullOrgManager.canShowAdCount_Reward();
            int full_ad_count = _adFullOrgManager.canShowAdCount_Interstitial(false, false);


            LogUtil.d("onActivityResult, all_ad_count : " + all_ad_count);
            LogUtil.d("onActivityResult, reward_ad_count : " + reward_ad_count);
            LogUtil.d("onActivityResult, full_ad_count : " + full_ad_count);

            if (all_ad_count > 0 || reward_ad_count > 0) {
                _adFullOrgManager.setOnAdCompleteListener(null);
                _adFullOrgManager.setOnAdCompleteListener(new InterfaceSet.OnAdCompleteListener() {
                    @Override
                    public void onAdLoadSuccess() {
                        LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadSuccess 2");
                    }

                    @Override
                    public void onAdLoadFail() {
                        LogUtil.d("_txtAd, AtvMoreCharge, onAdLoadFail 2");
                    }

                    @Override
                    public void onAdClose() {
                        _adFullOrgManager.setOnAdCompleteListener(null);
                        LogUtil.d("_txtAd, AtvMoreCharge, onAdClose 2");

                    }
                });

                if (full_ad_count > 0)
                    _adFullOrgManager.showAdOrder_Interstitial();
                else
                    _adFullOrgManager.showAdOrderReward(false);
            }
        } else {

        }
    }

}
