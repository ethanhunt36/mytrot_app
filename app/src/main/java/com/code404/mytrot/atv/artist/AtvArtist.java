package com.code404.mytrot.atv.artist;

import android.view.MotionEvent;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.code404.mytrot.R;
import com.code404.mytrot.adapter.MyFragmentPagerAdapter;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.frg.artist.FrgArtist;
import com.code404.mytrot.frg.artist.FrgTrot;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;

import org.json.JSONObject;


public class AtvArtist extends AtvBase {

    private int _selectedIndex = -1;

    private MyFragmentPagerAdapter _adapter = null;
    private com.code404.mytrot.view.SlideTabView _slideTab = null;
    private androidx.viewpager.widget.ViewPager _pager = null;

    @Override
    protected void setView() {
        setView(R.layout.atv_artist);
    }


    @Override
    protected void init() {
        _menuIndex = 1;
        _canFinish = true;
        _txtTopTitle.setText("영상");
        _btnTopBack.setVisibility(View.GONE);

        _btnTopRight.setImageResource(R.drawable._s_btn_find);
        _btnTopRight.setVisibility(View.VISIBLE);

        _adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());

        _slideTab.setTabViewCount(2);


        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
        int artist_no = JSONUtil.getInteger(json, "artist_no");
        String artist_name = JSONUtil.getString(json, "artist_name");

        if (Constants.APP_DEVICE_ID.equals("MYTROT") && artist_no > 0) {
            _slideTab.setTabViewCount(3);
            _slideTab.addTab("최애 영상");
        }


        _slideTab.addTab("전체 영상");
        _slideTab.addTab("가수별 영상");
        _slideTab.setTabOn(0);


        FrgArtist ftg01 = new FrgArtist();
        FrgTrot ftg02 = new FrgTrot();


        if (Constants.APP_DEVICE_ID.equals("MYTROT") && artist_no > 0) {
            FrgTrot ftg00 = new FrgTrot();
            ftg00.setArtistNo(artist_no);
            _adapter.addItem(ftg00);
        }

        _adapter.addItem(ftg02);
        _adapter.addItem(ftg01);

        _pager.setAdapter(_adapter);
        _pager.setCurrentItem(0);
    }

    private void setPageTab(int pos) {
        _selectedIndex = pos;

        _pager.setCurrentItem(pos);
        _slideTab.setTabOn(pos);
        FrgBase frg = (FrgBase) _adapter.getItem(pos);

        if (frg != null) frg.refresh();
        LogUtil.d("pos ::: " + pos);
    }

    @Override
    protected void findView() {
        _slideTab = (com.code404.mytrot.view.SlideTabView) findViewById(R.id.slideTab);
        _pager = (androidx.viewpager.widget.ViewPager) findViewById(R.id.pager);
    }


    @Override
    protected void configureListener() {

        _btnTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopSearch();
            }
        });

        _slideTab.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    // mSlider.setSlideEnable(false);

                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    // if(mPager.getCurrentItem() == 0)
                    // {
                    // mSlider.setSlideEnable(true);
                    // }
                }
                return false;
            }
        });

        _slideTab.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
            @Override
            public void onClick(View v, int pos, JSONObject json) {
                setPageTab(pos);
            }
        });

        _pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int index) {
                _selectedIndex = index;

                LogUtil.d("onPageSelected : " + index);
                _slideTab.setTabOn(index);

                FrgBase frg = (FrgBase) _adapter.getItem(index);

                //if (frg != null) frg.refresh();
            }


            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // LogUtil.d("onPageScrolled : " + arg0);
            }


            @Override
            public void onPageScrollStateChanged(int index) {
                // LogUtil.d("onPageScrollStateChanged : " + index);
            }
        });
    }


}
