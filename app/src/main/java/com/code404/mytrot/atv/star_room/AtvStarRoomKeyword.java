package com.code404.mytrot.atv.star_room;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.SelectPhotoManager;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;


public class AtvStarRoomKeyword extends AtvBase implements GListView.IMakeView {

    private GListView _list = null;

    private View _header = null;
    private TextView _txtFilter = null;
    private TextView _txtSeqRecent = null;
    private TextView _txtSeqComment = null;
    private TextView _txtSeqLike = null;

    private Button _btnWrite = null;

    private View _baseNoData = null;

    private String _artist_name = "";
    private int _artist_no = -1;
    private String _artist_board_id = "";
    private JSONArray _keyword_list = null;




    @Override
    protected boolean getBundle() {

        _artist_name = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_NAME);
        _artist_no = getIntent().getIntExtra(Constants.EXTRAS_ARTIST_NO, -1);
        _artist_board_id = getIntent().getStringExtra(Constants.EXTRAS_ARTIST_BOARD_ID);


        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_starroom_pic);
    }


    @Override
    protected void init() {
        _txtFilter.setVisibility(View.GONE);
        _txtSeqComment.setVisibility(View.GONE);
        _txtSeqLike.setText("인기순");
        _txtTopTitle.setText(_artist_name + " 월간 키워드");

        _baseTopBottom.setVisibility(View.GONE);
        _btnWrite.setText("월간 키워드 등록하기");

        _list.setViewMaker(R.layout.row_artist_keyword, this);

        callApi_artistKeyword_list("recent");
    }


    @Override
    protected void findView() {
        _list = findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _list.setNoData(_baseNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_gallery, null);
        _list.addHeaderView(_header);

        _txtFilter = _header.findViewById(R.id.txtFilter);
        _txtSeqRecent = _header.findViewById(R.id.txtSeqRecent);
        _txtSeqComment = _header.findViewById(R.id.txtSeqComment);
        _txtSeqLike = _header.findViewById(R.id.txtSeqLike);

        _btnWrite = findViewById(R.id.btnWrite);

        _header.findViewById(R.id.base_help).setVisibility(View.VISIBLE);
    }


    @Override
    protected void configureListener() {
        _btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertPop alert = new AlertPop();
                alert.setOnStringListener(new InterfaceSet.OnStringListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which, String str) {
                        if(which == Alert.BUTTON1) {
                            callApi_artistKeyword_add(str);
                        }

                        if(dialog != null) dialog.dismiss();
                    }
                });
                alert.showArtistKeyword(getContext());
            }
        });

        _txtSeqRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_artistKeyword_list("recent");
            }
        });

        _txtSeqLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_artistKeyword_list("like");
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    private void callApi_artistKeyword_list(String type) {
        LogUtil.e("==========callApi_artistKeyword_list : start==========");

        if ("recent".equals(type)) {
            _txtSeqRecent.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
            _txtSeqLike.setBackground(null);
        } else {
            _txtSeqRecent.setBackground(null);
            _txtSeqLike.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistKeyword_list_recent(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
                , 1
                , Constants.RECORD_SIZE_MAX
        );

        if (type.equals("like")) {
            call = apiInterface.artistKeyword_list_pop(
                    SPUtil.getInstance().getUserNoEnc(getContext())
                    , _artist_no
                    , 1
                    , Constants.RECORD_SIZE_MAX
            );
        }

        _list.removeAll();

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");

                _keyword_list = JSONUtil.getJSONArray(jsonData, "data");
                _list.addItems(_keyword_list);

                _list.setNoDataVisibility(_keyword_list.length() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }



    private void callApi_artistKeyword_add(String keyword) {
        LogUtil.e("==========callApi_artistKeyword_add : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistKeyword_add(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
                , keyword
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                Alert.toastLong(getContext(), "등록되었습니다.");
                callApi_artistKeyword_list("recent");
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_artistKeyword_like(int keyword_no) {
        LogUtil.e("==========callApi_artistKeyword_like : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistKeyword_like(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , keyword_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_artistKeyword_unlike(int keyword_no) {
        LogUtil.e("==========callApi_artistKeyword_unlike : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistKeyword_unlike(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , keyword_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        TextView txtKeyword = convertView.findViewById(R.id.txtKeyword);
        TextView txtNick = convertView.findViewById(R.id.txtNick);
        TextView txtLikeCount = convertView.findViewById(R.id.txtLikeCount);

        final int keyword_no = JSONUtil.getInteger(json, "no");
        final int my_like_cnt = JSONUtil.getInteger(json, "my_like_cnt");
        final int like_cnt = JSONUtil.getInteger(json, "like_cnt");

        String nick = JSONUtil.getString(json, "nick");
        String lv = JSONUtil.getString(json, "lv");

        txtKeyword.setText(JSONUtil.getString(json, "keyword"));
        txtNick.setText(String.format("%s Lv.%s", nick, lv));
        txtLikeCount.setSelected(my_like_cnt > 0);
        txtLikeCount.setText("+" + like_cnt);


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int like_count = like_cnt;
                if (my_like_cnt < 1) {
                    callApi_artistKeyword_like(keyword_no);

                    like_count++;
                    JSONUtil.puts(json, "my_like_cnt", 1);
                }
                else {
                    callApi_artistKeyword_unlike(keyword_no);

                    like_count--;
                    JSONUtil.puts(json, "my_like_cnt", 0);
                }

                JSONUtil.puts(json, "like_cnt", like_count);

                JSONUtil.put(_keyword_list, json, position);
                adapter.notifyDataSetChanged();
            }
        });

        return convertView;
    }
}
