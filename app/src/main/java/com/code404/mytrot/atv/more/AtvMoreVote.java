package com.code404.mytrot.atv.more;

import android.view.MotionEvent;
import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.code404.mytrot.R;
import com.code404.mytrot.adapter.MyFragmentPagerAdapter;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.frg.etc.FrgVoteHistory;
import com.code404.mytrot.util.LogUtil;

import org.json.JSONObject;

public class AtvMoreVote extends AtvBase {

    private int _selectedIndex = -1;

    private MyFragmentPagerAdapter _adapter = null;
    private com.code404.mytrot.view.SlideTabView _slideTab = null;
    private ViewPager _pager = null;

    @Override
    protected void setView() {
        setView(R.layout.atv_artist);
    }


    @Override
    protected void init() {

        _txtTopTitle.setText("투표권 내역");
        _btnTopBack.setVisibility(View.VISIBLE);
        _btnTopRight.setVisibility(View.GONE);
        _baseTopBottom.setVisibility(View.GONE);

        _adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());

        _slideTab.setTabViewCount(2);

        _slideTab.addTab("적립");
        _slideTab.addTab("사용");
        _slideTab.setTabOn(0);

        FrgVoteHistory ftg01 = new FrgVoteHistory();
        FrgVoteHistory ftg02 = new FrgVoteHistory();

        ftg01.setType("GET");
        ftg02.setType("USE");

        _adapter.addItem(ftg01);
        _adapter.addItem(ftg02);

        _pager.setAdapter(_adapter);
        _pager.setCurrentItem(0);
    }

    private void setPageTab(int pos) {
        _selectedIndex = pos;

        _pager.setCurrentItem(pos);
        _slideTab.setTabOn(pos);
        FrgBase frg = (FrgBase) _adapter.getItem(pos);

        if (frg != null) frg.refresh();
        LogUtil.d("pos ::: " + pos);
    }

    @Override
    protected void findView() {
        _slideTab = (com.code404.mytrot.view.SlideTabView) findViewById(R.id.slideTab);
        _pager = (ViewPager) findViewById(R.id.pager);
    }


    @Override
    protected void configureListener() {

        _btnTopRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopSearch();
            }
        });

        _slideTab.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    // mSlider.setSlideEnable(false);

                } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    // if(mPager.getCurrentItem() == 0)
                    // {
                    // mSlider.setSlideEnable(true);
                    // }
                }
                return false;
            }
        });

        _slideTab.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
            @Override
            public void onClick(View v, int pos, JSONObject json) {
                setPageTab(pos);
            }
        });

        _pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int index) {
                _selectedIndex = index;

                LogUtil.d("onPageSelected : " + index);
                _slideTab.setTabOn(index);

                FrgBase frg = (FrgBase) _adapter.getItem(index);

                //if (frg != null) frg.refresh();
            }


            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // LogUtil.d("onPageScrolled : " + arg0);
            }


            @Override
            public void onPageScrollStateChanged(int index) {
                // LogUtil.d("onPageScrollStateChanged : " + index);
            }
        });
    }


}
