package com.code404.mytrot.atv.more;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.star.AtvStarInfo;
import com.code404.mytrot.atv.vod.AtvVod;
import com.code404.mytrot.atv.web.AtvWeb;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class AtvMorePromo extends AtvBase implements GListView.IMakeView {

    private static final int REQ_WRITE = 3333;
    private static final int REQ_BOARD_INFO = 7419;

    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;
    private ImageButton _btnWrite = null;

    private boolean _is_more_data = true;
    private int _last_no = -1;


    @Override
    protected void setView() {
        setView(R.layout.atv_more_promo);
    }

    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        _btnWrite = findViewById(R.id.btnWrite);
    }

    @Override
    protected void init() {

        _txtTopTitle.setText("마이트롯 인증샷 게시판");
        _btnTopBack.setVisibility(View.VISIBLE);
        _baseTopBottom.setVisibility(View.GONE);

        _list.setViewMaker(R.layout.row_promo, this);
        _list.setNoData(_baseNoData);

        callApi_boardPromo_listing(true);
    }

    @Override
    protected void configureListener() {

        _btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //SPUtil.getInstance().setBoardEnterShort(getContext());

                AdViewUtil.goActionAfterAdView(AtvMorePromo.this, getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvMorePromoWrite.class);
                        startActivityForResult(i, REQ_WRITE);
                    }
                });

            }
        });


        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_boardPromo_listing(false);
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

        AdViewUtil.ready(AtvMorePromo.this, getContext());
    }

    private void callApi_boardPromo_listing(final boolean isClear) {

        if (isClear) {
            _last_no = -1;
            _is_more_data = true;
        }

        if (_is_more_data == false) {
            return;
        }

        LogUtil.e("==========callApi_boardPromo_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.boardPromo_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _last_no
                , Constants.RECORD_SIZE
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list_data");

                if (isClear) {
                    _list.removeAll();
                }

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                if (list.length() < 1) {
                    _is_more_data = false;
                    return;
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private int _position = -1;

    @Override
    public View makeView(final GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);


        com.code404.mytrot.view.SquareImageView img01 = (com.code404.mytrot.view.SquareImageView) convertView.findViewById(R.id.img01);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtEtc = (TextView) convertView.findViewById(R.id.txtEtc);

        int is_hot = JSONUtil.getInteger(json, "is_hot", 0);
        String img01Url = JSONUtil.getStringUrl(json, "img01");
        String title = JSONUtil.getString(json, "title");
        String nick = JSONUtil.getString(json, "nick");

        if(is_hot == 1){
            title = "<font color='#D81B60'>Best</font> " + title;
        }

        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        reg_dttm = reg_dttm.substring(0, 10);
        reg_dttm = reg_dttm.replace("-", ".");

        // 모서리 라운딩 처리
        img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        img01.setClipToOutline(true);

        if (!FormatUtil.isNullorEmpty(img01Url)) {
            Picasso.with(getContext())
                    .load(img01Url)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(img01);
            img01.setVisibility(View.VISIBLE);
        } else {
            img01.setVisibility(View.GONE);
        }


        txtTitle.setText(Html.fromHtml(title));
        txtNick.setText(nick);
        txtDate.setText(reg_dttm);

        txtEtc.setText(String.format("좋아요 %s | 댓글 %s | 조회 %s"
                , FormatUtil.toPriceFormat(JSONUtil.getString(json, "like_cnt"))
                , FormatUtil.toPriceFormat(JSONUtil.getString(json, "comment_cnt"))
                , FormatUtil.toPriceFormat(JSONUtil.getString(json, "read_cnt"))
        ));


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _position = position;

                Intent i = new Intent();
                i.setClass(getContext(), AtvStarInfo.class);
                i.putExtra(Constants.EXTRAS_JSON_STRING, json.toString());

                startActivityForResult(i, REQ_BOARD_INFO);
            }
        });

        return convertView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);
        LogUtil.d("onActivityResult, data : " + data);
        LogUtil.intent("onActivityResult", data);

        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_WRITE) {
                callApi_boardPromo_listing(true);
            }
            if (requestCode == REQ_BOARD_INFO) {
                try {
                    JSONObject jsonNew = JSONUtil.createObject(data.getStringExtra(Constants.EXTRAS_JSON_STRING));

                    JSONObject json = _list.getAdapter().getItem(_position);

                    int is_ilike = JSONUtil.getInteger(jsonNew, "is_ilike");
                    int like_cnt = JSONUtil.getInteger(jsonNew, "like_cnt");
                    int comment_cnt = JSONUtil.getInteger(jsonNew, "comment_cnt");
                    String del_yn = JSONUtil.getString(jsonNew, "del_yn");

                    if ("Y".equals(del_yn)) {
                        _list.getAdapter().remove(json);
                    } else {
                        JSONUtil.puts(json, "is_ilike", is_ilike);
                        JSONUtil.puts(json, "my_like_cnt", is_ilike);
                        JSONUtil.puts(json, "like_cnt", like_cnt);
                        JSONUtil.puts(json, "comment_cnt", comment_cnt);
                    }

                    _list.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
