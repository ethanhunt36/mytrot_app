package com.code404.mytrot.atv;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.artist.AtvArtist;
import com.code404.mytrot.atv.artist.AtvArtistVod;
import com.code404.mytrot.atv.main.AtvChart;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.star.AtvStar;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdFullOrgManager;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertQuit;
import com.code404.mytrot.util.AlertSearch;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.android.gms.ads.AdSize;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;


public abstract class AtvBase extends FragmentActivity {


    protected boolean _canFinish = false;
    protected int _menuIndex = 0;
    protected TextView _txtTopTitle = null;
    protected ImageView _imgTopTitle = null;
    protected ImageButton _btnTopBack = null;
    protected TextView _txtAlrimNew = null;
    protected ImageButton _btnTopRight = null;
    protected View _baseReportRight = null;
    protected TextView _txtTopRight = null;
    protected TextView _txtTopHeartCount = null;
    protected TextView _txtCountAlrim = null;
    private LinearLayout mBaseLinear = null;
    protected View _baseTopTitle = null;
    protected View _baseTopBottom = null;


    //    protected static AdFullManager _adFullManager = null;
    // 카울리 때문에 static 으로 안합니다.
    // 04.26 에 다시 일반으로 변경... 1.8.90 에 적용
    // 카울리 효율이 떨어져서 다시 static 으로 변경 2023.01.23
    protected static AdFullOrgManager _adFullOrgManager = null;
    protected boolean _isFirstInit = true;

    View baseMenu01 = null;
    View baseMenu02 = null;
    View baseMenu03 = null;
    View baseMenu04 = null;
    View baseMenu05 = null;


    TextView _txtMenu01 = null;
    TextView _txtMenu02 = null;
    TextView _txtMenu03 = null;
    TextView _txtMenu04 = null;
    TextView _txtMenu05 = null;

    private static int _quit_try_count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.atv_base);


            int member_no = SPUtil.getInstance().getUserNo(getContext());

            if (member_no > 0) {
                APIClient.setMemberNo(member_no);
            }

            mBaseLinear = findViewById(R.id.baseLinear);

            setView();

            baseFindViewByID();
            baseConfigureListener();

            getAdSize();


            if (getBundle()) {
                findView();

                try {
                    if (Constants.IS_VIEW_SEQ_AD) {
//                        if (_adFullManager != null) {
//                            _adFullManager.setOnAdCompleteListener(null);
//                            _adFullManager.setOnAdCompleteMissionListener(null);
//                        }
//                        if (_adFullManager == null) {
//                            _adFullManager = new AdFullManager(AtvBase.this, getContext());
//                        }
//                        _adFullManager.setActivity(AtvBase.this);
//                        _adFullManager.setContext(getContext());
//                        _adFullManager.setAdSize(getAdSizeInfo());
//                        _adFullManager.setOnAdCompleteListener(null);
//                        _adFullManager.setOnAdCompleteMissionListener(null);

                    } else {
                        if (_adFullOrgManager != null) {
                            _adFullOrgManager.setOnAdCompleteListener(null);
                            _adFullOrgManager.setOnAdCompleteMissionListener(null);
                        }
                        if (_adFullOrgManager == null) {
                            _adFullOrgManager = new AdFullOrgManager(AtvBase.this, getContext());
                        } else if (_adFullOrgManager.canShowAdCount(false) == 0) {
                            _adFullOrgManager = new AdFullOrgManager(AtvBase.this, getContext());
                        }

                        _adFullOrgManager.setActivity(AtvBase.this);
                        _adFullOrgManager.setContext(getContext());
//                    _adFullOrgManager.setAdSize(getAdSizeInfo());

                        _adFullOrgManager.removeCauly();
                        _adFullOrgManager.setOnAdCompleteListener(null);
                        _adFullOrgManager.setOnAdCompleteMissionListener(null);
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }

                init();

                if (_canFinish) {
                    _btnTopBack.setVisibility(View.GONE);
                }

                configureListener();
            } else {
                getBundleFail();
            }
            initMenuBottom();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    protected void initNewAdFullOrgManager() {
//        if(_adFullOrgManager != null && _adFullOrgManager.isReadyCauly()) {
//            _adFullOrgManager = new AdFullOrgManager(AtvBase.this, getContext());
//            _adFullOrgManager.setActivity(AtvBase.this);
//            _adFullOrgManager.setContext(getContext());
//            _adFullOrgManager.removeCauly();
//            _adFullOrgManager.setOnAdCompleteListener(null);
//            _adFullOrgManager.setOnAdCompleteMissionListener(null);
//        }
//        _adFullOrgManager.setIsCaulyStatic(false);
//    }

    private void baseFindViewByID() {

        _txtTopTitle = findViewById(R.id.txtTopTitle);
        _imgTopTitle = findViewById(R.id.imgTopTitle);
        _btnTopRight = findViewById(R.id.btnTopRight);
        _btnTopBack = findViewById(R.id.btnTopBack);
        _txtAlrimNew = findViewById(R.id.txtAlrimNew);
        _baseTopTitle = findViewById(R.id.baseTopTitle);
        _baseTopBottom = findViewById(R.id.baseTopBottom);
        _txtTopHeartCount = findViewById(R.id.txtTopHeartCount);
        _txtTopRight = findViewById(R.id.txtTopRight);
        _baseReportRight = findViewById(R.id.baseReportRight);

        _txtCountAlrim = findViewById(R.id.txtCountAlrim);

        baseMenu01 = findViewById(R.id.baseMenu01);
        baseMenu02 = findViewById(R.id.baseMenu02);
        baseMenu03 = findViewById(R.id.baseMenu03);
        baseMenu04 = findViewById(R.id.baseMenu04);
        baseMenu05 = findViewById(R.id.baseMenu05);

        _txtMenu01 = findViewById(R.id.txtMenu01);
        _txtMenu02 = findViewById(R.id.txtMenu02);
        _txtMenu03 = findViewById(R.id.txtMenu03);
        _txtMenu04 = findViewById(R.id.txtMenu04);
        _txtMenu05 = findViewById(R.id.txtMenu05);


        _btnTopBack.setVisibility(View.VISIBLE);
        _btnTopRight.setVisibility(View.GONE);
        _txtTopHeartCount.setVisibility(View.GONE);

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
        int lv = JSONUtil.getInteger(json, "lv");

        if (lv < SPUtil.getInstance().getAvailableBoardLevel(getContext())) {
            baseMenu04.setVisibility(View.GONE);
        }

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            baseMenu02.setVisibility(View.GONE);
        }
    }

    private void baseConfigureListener() {
        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBack();
            }
        });
    }

    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        LogUtil.d("getAdSize, adWidth : " + adWidth);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth);
    }

    private AdSize _adSize = null;

    protected AdSize getAdSizeInfo() {

        if (_adSize == null) _adSize = getAdSize();

        LogUtil.d("getAdSizeInfo,  " + _adSize.toString());

        return _adSize;
    }

    private void initMenuBottom() {

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

        final String pause_yn = JSONUtil.getString(json, "pause_yn");

        baseMenu01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getContext(), AtvChart.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        baseMenu02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getContext(), AtvArtist.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        baseMenu03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                //intent.setClass(getContext(), AtvLike.class);
                intent.setClass(getContext(), AtvVote.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        baseMenu04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getContext(), AtvStar.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        baseMenu05.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getContext(), AtvMore.class);
                startActivity(intent);
                finishAffinity();
            }
        });
        switch (_menuIndex) {
            case 0:
                //imgMenu01.setImageResource(R.drawable.navi_01_on);
                _txtMenu01.setTextColor(getResources().getColor(R.color.purpley));
                _txtMenu01.setText(Html.fromHtml("<b>" + _txtMenu01.getText().toString() + "</b>"));
                break;
            case 1:
                //imgMenu02.setImageResource(R.drawable.navi_02_on);
                _txtMenu02.setTextColor(getResources().getColor(R.color.purpley));
                _txtMenu02.setText(Html.fromHtml("<b>" + _txtMenu02.getText().toString() + "</b>"));
                break;
            case 2:
                //imgMenu03.setImageResource(R.drawable.navi_03_on);
                _txtMenu03.setTextColor(getResources().getColor(R.color.purpley));
                _txtMenu03.setText(Html.fromHtml("<b>" + _txtMenu03.getText().toString() + "</b>"));
                break;
            case 3:
                //imgMenu04.setImageResource(R.drawable.navi_04_on);
                _txtMenu04.setTextColor(getResources().getColor(R.color.purpley));
                _txtMenu04.setText(Html.fromHtml("<b>" + _txtMenu04.getText().toString() + "</b>"));
                break;
            case 4:
                //imgMenu04.setImageResource(R.drawable.navi_04_on);
                _txtMenu05.setTextColor(getResources().getColor(R.color.purpley));
                _txtMenu05.setText(Html.fromHtml("<b>" + _txtMenu05.getText().toString() + "</b>"));
                break;

            default:
                _txtMenu03.setTextColor(getResources().getColor(R.color.purpley));
                _txtMenu03.setText(Html.fromHtml("<b>" + _txtMenu03.getText().toString() + "</b>"));
                break;
        }
    }

    public Context getContext() {
        return this;
    }

    public Activity getActivity() {
        return this;
    }


    /**
     * 컨텐츠 화면 구성
     *
     * @param layoutResId
     */
    protected void setView(int layoutResId) {
        if (layoutResId > 0) {
            View view = InflateUtil.inflate(this, layoutResId, null);

            if (view != null) {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
                mBaseLinear.addView(view, layoutParams);
            }
        }
    }

    /**
     * 각종 이벤트 리스너 추가
     */
    protected abstract void configureListener();

    protected abstract void setView();

    /**
     * 리소스 찾기
     */
    protected abstract void findView();

    protected boolean getBundle() {
        return true;
    }

    protected void getBundleFail() {
        Alert.toastShort(this, "다시 시도해 주세요.");
        finish();
    }


    /**
     * 화면 초기화
     */
    protected abstract void init();


    @Override
    protected void onResume() {
        try {
            //  화면 이동 시 자동 애니메이션 제거
            this.overridePendingTransition(0, 0);
            super.onResume();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        try {
            super.onPause();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onBack() {
        finish();
    }


    /**
     * BACK key 핸들러
     */
    Handler m_hndBackKey = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0)
                _isFinish = false;

            _quit_try_count++;
        }
    };
    private boolean _isFinish = false;


    @Override
    public void onBackPressed() {
        if (_canFinish == false) {
            super.onBackPressed();
            return;
        }

        if (_menuIndex != 2) {
            Intent intent = new Intent();
            intent.setClass(getContext(), AtvVote.class);
            startActivity(intent);
            finishAffinity();
            return;
        }

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            AlertQuit alert = new AlertQuit();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    if (which == Alert.BUTTON1) {
                        // 종료
//                    _adFullManager = null;
                        _adFullOrgManager = null;
                        finishAffinity();
                    } else if (which == Alert.BUTTON2) {
                        // 응원하기
                        openReviewStoreOrApi(true);
                    } else if (which == Alert.BUTTON3) {
                        // 치매사냥 플레이스토어 이동
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(Constants.AD_CHIMEA_GAME_URL));
                        startActivity(i);
                        finishAffinity();
                    }
                    LogUtil.d("which : " + which);
                }
            });

            //alert.showQuit(getContext());

            if (_quit_try_count % 3 == 0) {
                alert.showQuit(getContext());
            } else if (_quit_try_count % 3 == 1) {
                alert.showQuitReview(getContext());
            } else {
                if (_isFinish == false) {
                    _isFinish = true;
                    Toast.makeText(this, "'뒤로' 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
                    m_hndBackKey.sendEmptyMessageDelayed(0, 2000);
                } else {
                    finishAffinity();
                }
                return;
            }
        } else {
            if (_isFinish == false) {
                _isFinish = true;
                Toast.makeText(this, "'뒤로' 버튼을 한번 더 누르면 종료됩니다.", Toast.LENGTH_SHORT).show();
                m_hndBackKey.sendEmptyMessageDelayed(0, 2000);
            } else {
                finishAffinity();
            }
            return;
        }

        _quit_try_count++;
    }

    protected void callApi_login(String email, String pw, final boolean needCallLoginAfter) {
        callApi_login(email, pw, "E", "", needCallLoginAfter);
    }

    protected void callApi_login(String email, String pw, String login_type, String sns_id, final boolean needCallLoginAfter) {

        LogUtil.e("==========callApi_login : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_login_v2(
                email, pw, login_type, sns_id
                , Constants.APP_DEVICE_ID
        );


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");
                int member_no = JSONUtil.getInteger(userInfo, "no");
                String member_no_enc = JSONUtil.getString(userInfo, "member_no");

                SPUtil.getInstance().setIsLogin(getContext(), true);
                SPUtil.getInstance().setUserNo(getContext(), member_no);
                SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                SPUtil.getInstance().setUserNoEnc(getContext(), member_no_enc);


                if (needCallLoginAfter == false) {
                    // 메인 화면으로 이동
                    Intent intent = new Intent();
                    intent.setClass(getContext(), AtvMore.class);
                    startActivity(intent);
                    finishAffinity();
                } else {
                    callApi_member_login_after();
                }

            }
        });
    }


    protected void callApi_member_get_by_sns_v2(final String email, final String login_type, final String sns_id) {

        LogUtil.e("==========callApi_member_get_by_sns_v2 : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_sns_v2(
                sns_id
                , login_type
                , Constants.APP_DEVICE_ID
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                String member_no = JSONUtil.getString(userInfo, "member_no");
                String device_id = JSONUtil.getString(userInfo, "device_id");
                String db_sns_id = JSONUtil.getString(userInfo, "sns_id");
                String db_login_type = JSONUtil.getString(userInfo, "login_type");

                LogUtil.d("member_no : " + member_no);
                LogUtil.d("device_id : " + device_id);
                LogUtil.d("db_sns_id : " + db_sns_id);
                LogUtil.d("db_login_type : " + db_login_type);

                LogUtil.d("sns_id : " + sns_id);


                if (!FormatUtil.isNullorEmpty(member_no) && db_sns_id.equals(sns_id) && device_id.equals(Constants.APP_DEVICE_ID)) {
                    callApi_login(email, "", login_type, sns_id, null);
                } else {
                    callApi_regist(email, login_type, sns_id);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_regist(final String email, final String login_type, final String sns_id) {

        LogUtil.e("==========callApi_regist : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_add(
                email,
                "",
                login_type,
                sns_id,
                Constants.APP_TYPE_ID,
                Constants.APP_DEVICE_ID,
                CommonUtil.getCurrentVersion(getContext()),
                SPUtil.getInstance().getRegistPushKey(getContext())
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                callApi_login(email, "", login_type, sns_id, null);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");

                //new Alert().showAlert(getContext(), "관리자에게 문의 바랍니다.");
            }
        });

    }


    protected void callApi_login(String email, String pw, String login_type, String sns_id, final InterfaceSet.OnCompleteListener listener) {

        LogUtil.e("==========callApi_login : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_login_v2(
                email, pw, login_type, sns_id, Constants.APP_DEVICE_ID
        );


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");
                int member_no = JSONUtil.getInteger(userInfo, "no");
                String member_no_enc = JSONUtil.getString(userInfo, "member_no");

                SPUtil.getInstance().setIsLogin(getContext(), true);
                SPUtil.getInstance().setUserNo(getContext(), member_no);
                SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                SPUtil.getInstance().setUserNoEnc(getContext(), member_no_enc);

                if (listener != null) {
                    listener.onComplete();
                } else {
                    Intent intent = new Intent(getContext(), AtvVote.class);
                    startActivity(intent);
                    finishAffinity();
                }
            }
        });
    }


    private void callApi_member_login_after() {
        LogUtil.e("==========callApi_member_login_after : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_login_after(
                SPUtil.getInstance().getUserNoEnc(getContext()),
                Constants.APP_TYPE_ID,
                Constants.APP_DEVICE_ID,
                CommonUtil.getCurrentVersion(getContext()),
                SPUtil.getInstance().getRegistPushKey(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                LogUtil.d("AtvIntro------------------------------------------------------");
                LogUtil.intent(getIntent());
                LogUtil.d("------------------------------------------------------");

                Intent intent = new Intent(getContext(), AtvMore.class);
                if (resultCode == 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                    intent.putExtra(Constants.EXTRAS_MSG, resultMessage);
                }
                startActivity(intent);
                finishAffinity();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    protected void callApi_member_get_by_email(final String email, final String login_type, final String sns_id) {

        LogUtil.e("==========callApi_member_get_by_email : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_email(
                email
                , Constants.APP_DEVICE_ID
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                String member_no = JSONUtil.getString(userInfo, "member_no");
                String db_sns_id = JSONUtil.getString(userInfo, "sns_id");
                String db_login_type = JSONUtil.getString(userInfo, "login_type");

                LogUtil.d("member_no : " + member_no);
                LogUtil.d("db_sns_id : " + db_sns_id);
                LogUtil.d("db_login_type : " + db_login_type);

                LogUtil.d("sns_id : " + sns_id);


                if (!FormatUtil.isNullorEmpty(member_no)) {
                    if (db_sns_id.equals(sns_id)) {
                        callApi_login(email, "", login_type, sns_id, null);
                    } else {
                        // new Alert().showAlert(getContext(), "이미 가입된 " + CodeUtil.getSnsName(db_login_type) + " 계정이 있습니다.\n확인 바랍니다.");
                    }
                } else {
                    callApi_regist_non_sns(email, login_type, sns_id);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_regist_non_sns(final String email, final String login_type, final String sns_id) {

        LogUtil.e("==========callApi_regist : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_add(
                email,
                "",
                login_type,
                sns_id,
                Constants.APP_TYPE_ID,
                Constants.APP_DEVICE_ID,
                CommonUtil.getCurrentVersion(getContext()),
                SPUtil.getInstance().getRegistPushKey(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                callApi_login(email, "", "A", email, null);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");

                //new Alert().showAlert(getContext(), "관리자에게 문의 바랍니다.");
            }
        });

    }

    protected void showPopSearch() {
        AlertSearch alert = new AlertSearch();
        alert.setOnStringListener(new InterfaceSet.OnStringListener() {
            @Override
            public void onClose(DialogInterface dialog, int which, String str) {
                if (which == Alert.BUTTON1) {
                    Intent i = new Intent();
                    i.setClass(getContext(), AtvArtistVod.class);
                    i.putExtra(Constants.EXTRAS_ARTIST_NO, -1);
                    i.putExtra(Constants.EXTRAS_TITLE, str);
                    i.putExtra(Constants.EXTRAS_KEYWORD, str);
                    startActivity(i);
                }
            }
        });
        alert.show(getContext());
    }

    //private ReviewManager _manager = null;

    protected void openReviewStoreOrApi(boolean isClose) {
      //  _manager = ReviewManagerFactory.create(getContext());

        if (isClose) {
            gotoStore();
            return;
        }
        String reviewType = SPUtil.getInstance().getReviewType(getContext());

//        if ("native".equals(reviewType)) {
//            Task<ReviewInfo> request = _manager.requestReviewFlow();
//            request.addOnCompleteListener(new OnCompleteListener<ReviewInfo>() {
//                @Override
//                public void onComplete(@NonNull Task<ReviewInfo> task) {
//                    if (task.isSuccessful()) {
//                        // We can get the ReviewInfo object
//                        ReviewInfo reviewInfo = task.getResult();
//                        LogUtil.d("Successful");
//                        LogUtil.d("reviewInfo : " + reviewInfo.toString());
//
//                        Task<Void> flow = _manager.launchReviewFlow(AtvBase.this, reviewInfo);
//                        flow.addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                LogUtil.d("task : " + (task == null ? "null" : "ok ,, " + task.toString()));
//
//                                task.getResult();
//                            }
//                        });
//                    } else {
//                        // There was some problem, continue regardless of the result.
//                        LogUtil.d("failed");
//                    }
//                }
//            });
//        } else {
            gotoStore();
//        }
    }

    protected void gotoStore() {
        Intent i = new Intent(android.content.Intent.ACTION_VIEW);
        i.setData(Uri.parse(Constants.MARKET_URL));
        startActivity(i);
    }


}
