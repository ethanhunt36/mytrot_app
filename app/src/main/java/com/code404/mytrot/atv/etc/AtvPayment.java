package com.code404.mytrot.atv.etc;

import android.app.Dialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.BaseKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.UiThread;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryRecord;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.bean.ProductBean;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.widget.ItemPayment;
import com.code404.mytrot.widget.ItemPaymentDev;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;

public class AtvPayment extends AtvBase implements PurchasesUpdatedListener {

    private static String TAG = "AtvPayment_TEST";

    private LinearLayout _baseProduct_VoteTicket = null;
    private LinearLayout _baseProduct_H = null;
    private LinearLayout _baseProduct_Vote = null;
    private LinearLayout _baseProduct_VoteBooster = null;
    private LinearLayout _baseProduct_Package = null;
    private LinearLayout _baseProduct_PackageBody = null;


    private LinearLayout _base_etc = null;

//    private LinearLayout _baseProduct_Ad = null;


    private static List<String> _purchaseTokenList = new ArrayList<String>();


    private BillingClient mBillingClient;

    private List<SkuDetails> _skuDetailsList;

    private int _order_no = -1;
    private JSONObject _productInfo = null;
    private int _price = -1;
    private String _sku = "";
    private String _pay_type = "A";

    private List<String> _skuList = new ArrayList<>();


    private ImageView _imgArrow00 = null;
    private ImageView _imgArrow01 = null;

    @Override
    protected boolean getBundle() {
        return true;
    }

    @Override
    protected void setView() {
        setView(R.layout.atv_payment);
    }

    @Override
    protected void findView() {

        _baseProduct_VoteTicket = findViewById(R.id.baseProduct_VoteTicket);
        _baseProduct_H = findViewById(R.id.baseProduct_H);
        _baseProduct_Vote = findViewById(R.id.baseProduct_Vote);
        _baseProduct_VoteBooster = findViewById(R.id.baseProduct_VoteBooster);
        _baseProduct_Package = findViewById(R.id.baseProduct_Package);
        _baseProduct_PackageBody = findViewById(R.id.baseProduct_PackageBody);
//        _baseProduct_Ad = findViewById(R.id.baseProduct_Ad);

        _imgArrow00 = findViewById(R.id.imgArrow00);
        _imgArrow01 = findViewById(R.id.imgArrow01);

        _base_etc = findViewById(R.id.base_etc);

    }

    @Override
    protected void init() {
        _txtTopTitle.setText("상품 결제");
        _baseTopBottom.setVisibility(View.GONE);

        _baseProduct_VoteTicket.removeAllViews();
        _baseProduct_H.removeAllViews();
        _baseProduct_Vote.removeAllViews();
        _baseProduct_VoteBooster.removeAllViews();
        _baseProduct_Package.removeAllViews();
        //_baseProduct_Ad.removeAllViews();


        //_baseProduct_PackageBody.setVisibility(View.GONE);


//        setBodyConfigureListener(findViewById(R.id.imgArrow03), findViewById(R.id.base_body_03));


        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _base_etc.setVisibility(View.GONE);
            _imgArrow00.setVisibility(View.GONE);
            _imgArrow01.setVisibility(View.GONE);
        } else {
            setBodyConfigureListener(findViewById(R.id.imgArrow00), findViewById(R.id.base_body_00));
            setBodyConfigureListener(findViewById(R.id.imgArrow01), findViewById(R.id.base_body_01));
            setBodyConfigureListener(findViewById(R.id.imgArrow02), findViewById(R.id.base_body_02));
            setBodyConfigureListener(findViewById(R.id.imgArrow02_booster), findViewById(R.id.base_body_02_booster));
            setBodyConfigureListener(findViewById(R.id.imgArrow02_package), findViewById(R.id.base_body_02_package));
        }

        callApi_product_get_all();
    }

    public void initBillingProcessor() {
        LogUtil.d(TAG, "initBillingProcessor");
        mBillingClient = BillingClient.newBuilder(this).setListener(this)
                .enablePendingPurchases().build();
        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(@NonNull BillingResult billingResult) {

                LogUtil.d(TAG, "onBillingSetupFinished");

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    LogUtil.d(TAG, "onBillingSetupFinished, OK");
                    //checkPurchase(SKU_REMOVE_AD); // 아이템 구매 여부 확인, 원하는 곳에 쓰기 바람

//                    skuList.add(SKU_REMOVE_AD);
//                    skuList.add(SKU_DONATE);

                    SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
                    params.setSkusList(_skuList).setType(BillingClient.SkuType.INAPP); // 구독 아이템을 가져오려면 구독으로 변경
                    mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
                        @Override
                        public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) { // Process the result.

                            _skuDetailsList = null;

                            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
                                LogUtil.d(TAG, "onSkuDetailsResponse, OK");

                                _skuDetailsList = skuDetailsList;

                                for (SkuDetails skuDetails : skuDetailsList) { // detail 정보를 가지고 결제를 요청함
                                    String sku = skuDetails.getSku();
                                    String price = skuDetails.getPrice();

                                    LogUtil.d(TAG, "sku : " + sku + ", price : " + price);
//                                    if (SKU_REMOVE_AD.equals(sku)) {
//                                        skuDetailsRemoveAd = skuDetails;
//                                    } else if (SKU_DONATE.equals(sku)) {
//                                        skuDetailsDonate = skuDetails;
//                                        // purchase(skuDetailsDonate) // 아이템 구매 , 원하는 곳에 쓰기 바람
//                                    }
                                }
                            } else {
                                LogUtil.d(TAG, "onSkuDetailsResponse, FAIL");
                            }
                        }
                    });

                    checkPurchaseHistory();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                LogUtil.d(TAG, "onBillingServiceDisconnected");
            }
        });

    }


    private void checkPurchaseHistory() {

        LogUtil.d(TAG, "checkPurchaseHistory start ");

        if (_purchaseTokenList.size() > 0) {
            LogUtil.d(TAG, "checkPurchaseHistory skip ");
            return;
        }


        // 최근에 결제한 아이템 가져오기 (인앱)
        mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.INAPP, new PurchaseHistoryResponseListener() {
            @Override
            public void onPurchaseHistoryResponse(@NonNull BillingResult billingResult, @Nullable List<PurchaseHistoryRecord> list) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && list != null) {
                    for (PurchaseHistoryRecord purchaseHistoryRecord : list) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                        Date timeInDate = new Date(purchaseHistoryRecord.getPurchaseTime());
                        String timeInFormat = sdf.format(timeInDate);

                        LogUtil.d(TAG, "checkPurchaseHistory , " + timeInFormat + " , list Data : " + purchaseHistoryRecord);

                        String token = purchaseHistoryRecord.getPurchaseToken();

                        LogUtil.d(TAG, "checkPurchaseHistory , token : " + token);

                        _purchaseTokenList.add(token);

                        ConsumeParams consumeParams = ConsumeParams.newBuilder().setPurchaseToken(token).build();
                        mBillingClient.consumeAsync(consumeParams, _consumeListener);
                    }
                }
            }
        });
    }


    @Override
    protected void configureListener() {

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            findViewById(R.id.base_title_00).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBodyConfigureListener(findViewById(R.id.imgArrow00), findViewById(R.id.base_body_00));
                }
            });
            findViewById(R.id.base_title_01).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBodyConfigureListener(findViewById(R.id.imgArrow01), findViewById(R.id.base_body_01));
                }
            });
            findViewById(R.id.base_title_02).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBodyConfigureListener(findViewById(R.id.imgArrow02), findViewById(R.id.base_body_02));
                }
            });
            findViewById(R.id.base_title_02_booster).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBodyConfigureListener(findViewById(R.id.imgArrow02_booster), findViewById(R.id.base_body_02_booster));
                }
            });
            findViewById(R.id.base_title_02_package).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBodyConfigureListener(findViewById(R.id.imgArrow02_package), findViewById(R.id.base_body_02_package));
                }
            });
        }
//        findViewById(R.id.base_title_03).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                setBodyConfigureListener(findViewById(R.id.imgArrow03), findViewById(R.id.base_body_03));
//            }
//        });

    }


    private void setBodyConfigureListener(ImageView imgArrow, View body) {

        if (body == null) {
            LogUtil.d("body is null");
            return;
        }

        if (body.getVisibility() == View.GONE) {
            imgArrow.setImageResource(R.drawable.btn_img_up);
            body.setVisibility(View.VISIBLE);
        } else {
            imgArrow.setImageResource(R.drawable.btn_img_down);
            body.setVisibility(View.GONE);
        }
    }

    private void callApi_product_get_all() {

        LogUtil.e("==========callApi_product_get_all : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.product_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                callApi_memberItem_get_all();

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONArray list = JSONUtil.getJSONArray(json, "data");
                _skuList = new ArrayList<>();
                for (int i = 0; i < list.length(); i++) {
                    final int idx = i;
                    final JSONObject j = JSONUtil.getJSONObject(list, i);

                    String product_type = JSONUtil.getString(j, "product_type");
                    String sku = JSONUtil.getString(j, "sku");

                    _skuList.add(sku);

                    ItemPaymentDev itemDev = null;
                    ItemPayment itemAd = null;
                    ItemPayment item = null;

                    if (product_type.equals("H")) {
                        itemDev = new ItemPaymentDev(getContext());
                        itemDev.setData(j);
                        _baseProduct_H.addView(itemDev);
                    } else if (product_type.equals("P")) {
//                        if (sku.contains("remove_ad")) {
//                            itemAd = new ItemPayment(getContext());
//                            itemAd.setData(j);
//                            _baseProduct_Ad.addView(itemAd);
//                        } else
                        if (sku.startsWith("auto_ticket")) {
                            item = new ItemPayment(getContext());
                            item.setData(j);
                            _baseProduct_Vote.addView(item);
                        } else if (sku.startsWith("booster")) {
                            item = new ItemPayment(getContext());
                            item.setData(j);
                            _baseProduct_VoteBooster.addView(item);
                        } else if (sku.startsWith("auto_booster")) {
                            item = new ItemPayment(getContext());
                            item.setData(j);
                            _baseProduct_Package.addView(item);
                            //_baseProduct_PackageBody.setVisibility(View.VISIBLE);
                        } else if (sku.startsWith("vote_ticket")) {
                            item = new ItemPayment(getContext());
                            item.setData(j);
                            _baseProduct_VoteTicket.addView(item);

                        }
                    }

                    if (itemDev != null) {
                        itemDev.setOnClickViewListener(new InterfaceSet.OnClickJsonListener() {
                            @Override
                            public void onClick(View v, int pos, JSONObject json) {
                                _pay_type = "A";
                                _productInfo = json;
                                LogUtil.json(_productInfo);
                                int product_no = JSONUtil.getInteger(_productInfo, "no");
                                _price = JSONUtil.getInteger(_productInfo, "price");
                                _sku = JSONUtil.getString(_productInfo, "sku");

                                callApi_pay_order_add(product_no, _price, _sku);
                            }
                        });
                    }
                    if (itemAd != null) {
                        itemAd.setOnClickViewListener(new InterfaceSet.OnClickJsonListener() {
                            @Override
                            public void onClick(View v, int pos, JSONObject json) {

                                if (isStartsWithItem("remove_ad") != null) {
                                    new Alert().showAlert(getContext(), "광고제거상품은 이미 보유중입니다.");
                                    return;
                                }
                                _pay_type = "A";
                                _productInfo = json;
                                LogUtil.json(_productInfo);
                                final int product_no = JSONUtil.getInteger(_productInfo, "no");
                                _price = JSONUtil.getInteger(_productInfo, "price");
                                _sku = JSONUtil.getString(_productInfo, "sku");

                                JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
                                int point = JSONUtil.getInteger(jsonUser, "point");


                                if (point >= _price && canPurchasePoint(_sku)) {
                                    showPaymentPoint(product_no, point);
                                } else {
                                    callApi_pay_order_add(product_no, _price, _sku);
                                }
                            }
                        });
                    }
                    if (item != null) {
                        item.setOnClickViewListener(new InterfaceSet.OnClickJsonListener() {
                            @Override
                            public void onClick(View v, int pos, JSONObject json) {
                                ItemPayment itemPayment = (ItemPayment) v;
                                String sku = itemPayment.getSKU();

                                ProductBean bean = null;

                                if (sku.startsWith("auto_ticket") || sku.startsWith("booster") || sku.startsWith("auto_booster")) {
                                    bean = isStartsWithItem("auto_booster");
                                    if (bean != null) {
                                        String msg = String.format("'자동충전/부스터 패키지' 상품을 이미 보유중입니다.\n\n유효기간 종료 후에 구매 바랍니다.\n\n\n상품명 : %s\n\n유효기간 : %s", bean.description, bean.expired_dttm);
                                        new Alert().showAlert(getContext(), msg);
                                        return;
                                    }
                                }

                                if (sku.startsWith("auto_ticket") || sku.startsWith("auto_booster")) {
                                    bean = isStartsWithItem("auto_ticket");
                                    if (bean != null) {
                                        String msg = String.format("'투표권 자동 충전' 상품을 이미 보유중입니다.\n\n유효기간 종료 후에 구매 바랍니다.\n\n\n상품명 : %s\n\n유효기간 : %s", bean.description, bean.expired_dttm);
                                        new Alert().showAlert(getContext(), msg);
                                        return;
                                    }
                                }

                                if (sku.startsWith("booster") || sku.startsWith("auto_booster")) {
                                    bean = isStartsWithItem("booster");
                                    if (bean != null) {
                                        String msg = String.format("'투표권 X2 부스터' 상품을 이미 보유중입니다.\n\n유효기간 종료 후에 구매 바랍니다.\n\n\n상품명 : %s\n\n유효기간 : %s", bean.description, bean.expired_dttm);
                                        new Alert().showAlert(getContext(), msg);
                                        return;
                                    }
                                }


                                _pay_type = "A";
                                _productInfo = json;
                                LogUtil.json(_productInfo);
                                final int product_no = JSONUtil.getInteger(_productInfo, "no");
                                _price = JSONUtil.getInteger(_productInfo, "price");
                                _sku = JSONUtil.getString(_productInfo, "sku");

                                JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
                                int point = JSONUtil.getInteger(jsonUser, "point");


                                if (point >= _price && canPurchasePoint(_sku)) {
                                    showPaymentPoint(product_no, point);
                                } else {
                                    callApi_pay_order_add(product_no, _price, _sku);
                                }
                            }
                        });
                    }
                }

                initBillingProcessor();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private boolean canPurchasePoint(String sku) {
        if (sku.startsWith("auto_ticket") || sku.startsWith("booster") || sku.startsWith("auto_booster")) {
            return true;
        }
        return false;
    }


    private void showPaymentPoint(int product_no, int point) {
        String msg = String.format("일반 결제 대신에 보유하고 있는\n포인트 %sP로 구매할 수 있습니다.\n\n상품 가격(포인트) : %sP\n\n포인트로 구매하시겠습니까?"
                , FormatUtil.toPriceFormat(point)
                , FormatUtil.toPriceFormat(_price));

        AlertPop alert = new AlertPop();
        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
            @Override
            public void onClose(DialogInterface dialog, int which) {
                if (which == Alert.BUTTON1) {
                    // 포인트 결제
                    _pay_type = "P";
                    callApi_pay_order_add(product_no, _price, _sku);
                } else if (which == Alert.BUTTON2) {
                    // 인앱 결제
                    callApi_pay_order_add(product_no, _price, _sku);
                }
            }
        });
        alert.showPaymentPoint(getContext(), msg);
    }


    private void callApi_pay_order_add(int product_no, int price, final String sku) {

        LogUtil.e("==========callApi_pay_order_add : start==========");
        LogUtil.d("callApi_pay_order_add, product_no : " + product_no);
        LogUtil.d("callApi_pay_order_add, price : " + price);
        LogUtil.d("callApi_pay_order_add, sku : " + sku);
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.pay_order_add(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , product_no
                , price
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                _order_no = JSONUtil.getInteger(json, "data");

                if (_pay_type.equals("A")) {

                    // 결제 시도
                    //_bp.purchase(AtvPayment.this, sku);

                    SkuDetails skuDetails = getSkuDetails(sku);

                    if (skuDetails != null) {
                        purchase(skuDetails);
                    }

                } else if (_pay_type.equals("P")) {
                    callApi_pay_sales_add(sku, "", "");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_pay_sales_add(String sku_complete, String order_id, String token) {

        LogUtil.e("==========callApi_pay_sales_add : start==========");


        int orderNo = _order_no;
//        JSONObject productInfo = _productInfo;
//        int price = JSONUtil.getInteger(productInfo, "price");
//        final String sku = JSONUtil.getString(productInfo, "sku");

        LogUtil.d("sku_complete : " + sku_complete);
        LogUtil.d("order_id : " + order_id);
        LogUtil.d("token : " + token);

        LogUtil.d("sku : " + _sku);
        LogUtil.d("price : " + _price);


        if (!sku_complete.equals(_sku)) {
            new Alert().showAlert(getContext(), "상품 정보가 일치하지 않습니다. \n시스템 관리자에게 문의 바랍니다.");
            return;
        }

        if (CommonUtil.checkDoubleTab()) {
            LogUtil.e("callApi_pay_sales_add, 이벤트 중복 발생");
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.pay_sales_add(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , orderNo
                , _price
                , _pay_type
                , sku_complete
                , order_id
                , token
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                    return;
                }

                LogUtil.d("_sku : " + _sku);
                LogUtil.d("resultMessage : " + resultMessage);

                if (FormatUtil.isNullorEmpty(resultMessage)) {

                    resultMessage = "결제되었습니다.";

                    if (_sku.contains("donation")) {
                        resultMessage = "감사합니다.\n후원하기 완료 되었습니다.";
                    } else if (_sku.contains("remove_ad") || _sku.contains("android.test")) {
                        resultMessage = "감사합니다.\n앞으로 광고는 모두 제거됩니다.";
                    } else if (_sku.startsWith("auto_ticket")) {
                        resultMessage = "감사합니다.\n\n앞으로 1시간 마다 자동으로 투표권이 충전됩니다.";
                    } else if (_sku.startsWith("booster")) {
                        resultMessage = "감사합니다.\n\n'광고를 시청하고 투표권&포인트 충전' 시에 투표권을 2배로 충전해드립니다.";
                    } else if (_sku.startsWith("auto_booster")) {
                        resultMessage = "감사합니다.\n\n'앞으로 1시간 마다 자동으로 투표권이 충전되며, 광고를 시청하고 투표권&포인트 충전' 시에 투표권을 2배로 충전해드립니다.";
                    } else if (_sku.startsWith("vote_ticket")) {
                        resultMessage = "감사합니다.\n\n투표권이 충전되었습니다.\n[더보기 - 투표권내역]에서 내역을 확인하실 수 있습니다.";
                    }
                }

                if (_sku.contains("remove_ad")) {
                    // 광고 제거 저장
                    SPUtil.getInstance().setIsHaveAdRemoveItem(getContext(), true);
                }
                if (_sku.contains("booster")) {
                    // 부스터 상품 저장
                    SPUtil.getInstance().setIsHaveBoosterItem(getContext(), true);
                }

                LogUtil.d("resultMessage : " + resultMessage);

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);

                //callApi_member_get_by_no();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mBillingClient != null) {
            mBillingClient.endConnection();
            mBillingClient = null;
        }
    }


    private List<ProductBean> _myItemList = new ArrayList<ProductBean>();

    private List<String> _myItemSkuList = new ArrayList<String>();


    private void callApi_memberItem_get_all() {

        _myItemList = new ArrayList<ProductBean>();
        LogUtil.e("==========callApi_memberItem_get_all : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberItem_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "P"
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONArray dataArr = JSONUtil.getJSONArray(json, "data");

                if (dataArr != null) {
                    for (int i = 0; i < dataArr.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(dataArr, i);

                        ProductBean bean = new ProductBean();
                        bean.no = JSONUtil.getInteger(j, "no");
                        bean.product_no = JSONUtil.getInteger(j, "product_no");
                        bean.expired_dttm = JSONUtil.getString(j, "expired_dttm");
                        bean.description = JSONUtil.getString(j, "description");
                        bean.sku = JSONUtil.getString(j, "sku");

                        _myItemList.add(bean);
                    }
                }

                callApi_memberItem_get_all_all();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_memberItem_get_all_all() {

        _myItemSkuList = new ArrayList<String>();

        LogUtil.e("==========callApi_memberItem_get_all : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberItem_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , ""
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


                JSONArray dataArr = JSONUtil.getJSONArray(json, "data");

                if (dataArr != null) {
                    for (int i = 0; i < dataArr.length(); i++) {
                        try {
                            JSONObject j = JSONUtil.getJSONObject(dataArr, i);

                            String sku = JSONUtil.getString(j, "sku");

                            LogUtil.e("sku : " + sku);

                            if (!_myItemSkuList.contains(sku)) {
                                _myItemSkuList.add(sku);

                                // 소비 처리
//                                boolean result_consumePurchase = _bp.consumePurchase(sku);

//                                LogUtil.e("sku : " + sku + ", consumePurchase, result_consumePurchase : " + result_consumePurchase);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private ProductBean isStartsWithItem(String sku) {
        ProductBean bean = null;

        for (ProductBean b : _myItemList) {
            LogUtil.d("isStartsWithItem, b : " + b.sku);
            if (b.sku.startsWith(sku)) {
                bean = b;
            }
            if (b.sku.equals(sku)) {
                bean = b;
                break;
            }
        }

        return bean;
    }

    ConsumeResponseListener _consumeListener = new ConsumeResponseListener() {
        @Override
        public void onConsumeResponse(BillingResult billingResult, String purchaseToken) {
            if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                // Handle the success of the consume operation. // For example, increase the number of coins inside the user's basket.
                LogUtil.d(TAG, "onConsumeResponse : OK");

            } else {
                LogUtil.d(TAG, "onConsumeResponse : fail");
            }
        }
    };

    private void handlePurchase(Purchase purchase) {

        LogUtil.d(TAG, "handlePurchase : purchase.getOrderId : " + purchase.getOrderId());
        LogUtil.d(TAG, "handlePurchase : purchase.getPurchaseToken : " + purchase.getPurchaseToken());
        LogUtil.d(TAG, "handlePurchase : purchase.getPurchaseTime : " + purchase.getPurchaseTime());

        ConsumeParams consumeParams = ConsumeParams.newBuilder().setPurchaseToken(purchase.getPurchaseToken()).build();
        mBillingClient.consumeAsync(consumeParams, _consumeListener);


        //callApi_pay_sales_add(purchase.getSku(), purchase.getOrderId(), purchase.getPurchaseToken());
        ArrayList<String> skus = purchase.getSkus();
        String sku = "";

        if (skus != null && skus.size() > 0) {
            sku = skus.get(0);
        }
        callApi_pay_sales_add(sku, purchase.getOrderId(), purchase.getPurchaseToken());
    }

    private void purchase(SkuDetails details) {

        LogUtil.d(TAG, "purchase : details : " + details);

        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(details)
                .build();
        BillingResult responseCode = mBillingClient.launchBillingFlow(this, flowParams);
    }

//    private boolean checkPurchase(String skuName) {
//
//        LogUtil.d(TAG, "checkPurchase : skuName : " + skuName);
//
//        mBillingClient.queryPurchasesAsync();
//        Purchase.PurchasesResult purchasesResult = mBillingClient.queryPurchases(BillingClient.SkuType.INAPP);
//        for (Purchase purchase : purchasesResult.getPurchasesList()) {
//
//            ArrayList<String> skus = purchase.getSkus();
//            String sku = "";
//
//            if (skus != null && skus.size() > 0) {
//                sku = skus.get(0);
//            }
//
//            if (skuName.equals(sku) && purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
//                return true;
//            }
//        }
//        return false;
//    }


    @Override
    public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> purchases) {

        LogUtil.d(TAG, "onPurchasesUpdated : billingResult : " + billingResult);
        LogUtil.d(TAG, "onPurchasesUpdated : purchases : " + purchases);

        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && purchases != null) { // 결제성공
            for (Purchase purchase : purchases) {
                //if(purchase.getSku().equals(SKU_DONATE)){ // 재구매 가능하도록 요청
                handlePurchase(purchase);
                //}
            }
        } else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) { // 결제 취소
            // Handle an error caused by a user cancelling the purchase flow.
        } else {
            // Handle any other error codes.
        }
    }


    private SkuDetails getSkuDetails(String sku) {

        if (_skuDetailsList == null) return null;

        SkuDetails skuDetails = null;

        for (SkuDetails s : _skuDetailsList) {
            if (s.getSku().equals(sku)) {
                skuDetails = s;
                break;
            }
        }

        return skuDetails;
    }


}
