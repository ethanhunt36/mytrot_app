package com.code404.mytrot.atv.more;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.AtvBase;
import com.code404.mytrot.atv.star.AtvStarInfo;
import com.code404.mytrot.bean.ImageBean;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAgree;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.CommonUtil;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.SelectPhotoManager;
import com.google.gson.JsonObject;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AtvMorePromoWrite extends AtvBase implements SelectPhotoManager.PhotoSelectCallback {

    private ImageBean[] _imgBeanArr = new ImageBean[]{new ImageBean(), new ImageBean(), new ImageBean()};
    private int _selectedPhotoIndex = -1;

    private ScrollView _scrollView = null;
    private EditText _edtTitle = null;
    private Button _btnTitle = null;
    private TextView _txtExample = null;
    private TextView _txtExample02 = null;
    private TextView _txtTip03 = null;
    private ImageButton _btnCheck = null;

    private com.code404.mytrot.view.SquareImageView _imgAttach01 = null;
    private com.code404.mytrot.view.SquareImageView _imgAttach02 = null;
    private com.code404.mytrot.view.SquareImageView _imgAttach03 = null;

    private TextView _txtAttach01 = null;

    private EditText _edtContent = null;
    private EditText _edtUrl = null;
    private Button _btnWrite = null;

    private int _sns_no = -1;


    private SelectPhotoManager m_photoManger = null;

    private boolean _have_permission = false;

    @Override
    protected void setView() {
        setView(R.layout.atv_more_promo_write);
    }

    @Override
    protected void init() {
        _menuIndex = 1;
        _txtTopTitle.setText("인증샷 등록");
        _baseTopBottom.setVisibility(View.GONE);
        _imgAttach01.setBackground(getDrawable(R.drawable._s_background_rounding));
        _imgAttach01.setClipToOutline(true);
        _imgAttach02.setBackground(getDrawable(R.drawable._s_background_rounding));
        _imgAttach02.setClipToOutline(true);
        _imgAttach03.setBackground(getDrawable(R.drawable._s_background_rounding));
        _imgAttach03.setClipToOutline(true);

        _imgBeanArr[0].setImageView(_imgAttach01);
        _imgBeanArr[1].setImageView(_imgAttach02);
        _imgBeanArr[2].setImageView(_imgAttach03);

        if (m_photoManger == null) {
            m_photoManger = new SelectPhotoManager(this);
            m_photoManger.setPhotoSelectCallback(this);
        }

        _txtExample.setText(Html.fromHtml("- <u><font color='#FF6300'>스크린샷 예시를 반드시 확인</font></u>해주시고, 업로드 바랍니다."));
        _txtExample02.setText(Html.fromHtml("- <u><font color='#FF6300'>마이트롯 활동 인증서를 반드시 포함해서 인증해주세요.</font></u> 스크린샷에 포함되어 있어야 보상이 지급됩니다."));

        final String todayDate = FormatUtil.getCurrentDate();
        String alertDate = SPUtil.getInstance().getMarketingAlertDate(getContext());


        //init_check_permission();

        if (!todayDate.equals(alertDate)) {
            JSONArray list = SPUtil.getInstance().getMarketingExJsonArray(getContext());

            if (list != null && list.length() > 0) {
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            showAlert();
                        }
                    }
                });
                alert.showAlert(getContext(), "스크린샷 예시를 확인해보시겠습니까?", true, "확인", "취소");

                SPUtil.getInstance().setMarketingAlertDate(getContext(), todayDate);
            }
        }

        if (SPUtil.getInstance().getIsAgreeWriteBoard(getContext())) {
            _btnCheck.setSelected(true);
        }
    }


    @Override
    protected void findView() {
        _scrollView = findViewById(R.id.scrollView);
        _edtTitle = (EditText) findViewById(R.id.edtTitle);
        _btnTitle = findViewById(R.id.btnTitle);
        _imgAttach01 = (com.code404.mytrot.view.SquareImageView) findViewById(R.id.imgAttach01);
        _imgAttach02 = (com.code404.mytrot.view.SquareImageView) findViewById(R.id.imgAttach02);
        _imgAttach03 = (com.code404.mytrot.view.SquareImageView) findViewById(R.id.imgAttach03);
        _edtContent = (EditText) findViewById(R.id.edtContent);
        _edtUrl = (EditText) findViewById(R.id.edtUrl);
        _btnWrite = (Button) findViewById(R.id.btnWrite);
        _txtExample = findViewById(R.id.txtExample);
        _txtExample02 = findViewById(R.id.txtExample02);

        _txtAttach01 = findViewById(R.id.txtAttach01);
        _txtTip03 = findViewById(R.id.txtTip03);
        _btnCheck = findViewById(R.id.btnCheck);
    }


    @Override
    protected void configureListener() {
        _btnTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertPop alert = new AlertPop();
                alert.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
                    @Override
                    public void onClick(View v, int pos, JSONObject json) {
                        LogUtil.d("pos : " + pos);

                        if(pos >= 0) {
                            String sns_name = JSONUtil.getString(json, "sns_name");
                            _btnTitle.setText(sns_name);
                            _edtTitle.setText(sns_name);

                            _sns_no = JSONUtil.getInteger(json, "no");

                        } else if(pos == -10) {
                            AlertPop alertReq = new AlertPop();
                            alertReq.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                @Override
                                public void onClose(DialogInterface dialog, int which) {

                                }
                            });
                            alertReq.showReqRegistSns(getContext());
                        }
                    }
                });
                alert.showSnsList(getContext());
            }
        });

        _txtExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlert();
            }
        });

        _imgAttach01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoSelected(0);
            }
        });
        _imgAttach02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoSelected(1);
            }
        });
        _imgAttach03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPhotoSelected(2);
            }
        });

        _btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertAgree alert = new AlertAgree();
                alert.showAgree(getContext());
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            _btnCheck.setSelected(true);
                        } else {
                            _btnCheck.setSelected(false);
                        }
                    }
                });
            }
        });

        _txtTip03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertAgree alert = new AlertAgree();
                alert.showAgree(getContext());
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            _btnCheck.setSelected(true);
                        } else {
                            _btnCheck.setSelected(false);
                        }
                    }
                });
            }
        });

        _edtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUtil.d("onClick");
                CommonUtil.scrollToView(_edtContent, _scrollView, 0);
            }
        });

        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackCheck();
            }
        });

        _btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = _edtTitle.getText().toString();
                String content = _edtContent.getText().toString();


                if (FormatUtil.isNullorEmpty(title)) {
                    new Alert().showAlert(getContext(), "마이트롯 인증글을 등록하신 SNS 커뮤니티 이름을 선택해 주세요.");
                    return;
                }

//                if (title.length() < 3) {
//                    new Alert().showAlert(getContext(), "마이트롯을 인증해주신 SNS 커뮤니티 이름을 최소 3자 이상 입력해 주세요.");
//                    return;
//                }

//                if (FormatUtil.isNullorEmpty(content)) {
//                    new Alert().showAlert(getContext(), "마이트롯을 인증해주신 내용을 간략히 입력해 주세요.");
//                    return;
//                }
//
//                if (content.length() < 5) {
//                    new Alert().showAlert(getContext(), "마이트롯을 인증해주신 내용을 간략히(5자 이상) 입력해 주세요.");
//                    return;
//                }

                if (_btnCheck.isSelected() == false) {
                    new Alert().showAlert(getContext(), "사용자 제작 콘텐츠(UGC) 약관에 동의해주세요.");
                    return;
                }

//                if (_file == null) {
//                    new Alert().showAlert(getContext(), "이미지를 선택해 주세요.");
//                    return;
//                }

                File file01 = null, file02 = null, file03 = null;
                String state01 = ImageBean.STATUS_STAY, state02 = ImageBean.STATUS_STAY, state03 = ImageBean.STATUS_STAY;
                int i = 0;
                for (ImageBean bean : _imgBeanArr) {
                    i++;
                    if (i == 1) {
                        file01 = bean.getFile();
                        state01 = bean.getStatus();
                    } else if (i == 2) {
                        file02 = bean.getFile();
                        state02 = bean.getStatus();
                    } else if (i == 3) {
                        file03 = bean.getFile();
                        state03 = bean.getStatus();
                    }
                }

                if (_imgBeanArr[0].isExistsData() == false) {
                    new Alert().showAlert(getContext(), "첫번째 사진을 등록해 주세요.");
                    return;
                }

                ArrayList<String> list = new ArrayList<String>();

                for (ImageBean b : _imgBeanArr) {
                    if (b.getFile() != null && !FormatUtil.isNullorEmpty(b.getLocalUri())) {

                        if (!list.contains(b.getLocalUri())) {
                            list.add(b.getLocalUri());
                        } else {
                            new Alert().showAlert(getContext(), "중복되는 사진이 있습니다.\n동일한 사진은 1장만 등록해 주세요.");
                            return;
                        }
                    }
                }

                callApi_boardPromo_ins(file01, file02, file03);
            }
        });
    }


    private void showAlert() {
        final JSONArray list = SPUtil.getInstance().getMarketingExJsonArray(getContext());

        if (list != null && list.length() > 0) {
            Alert alert = new Alert();

            String[] items = new String[list.length()];

            for (int i = 0; i < list.length(); i++) {
                JSONObject j = JSONUtil.getJSONObject(list, i);
                items[i] = JSONUtil.getString(j, "title");
            }

            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {
                    LogUtil.d("w : " + which);

                    if (which > -1) {
                        JSONObject j = JSONUtil.getJSONObject(list, which);
                        if (j != null) {
                            Intent i = new Intent();
                            i.setClass(getContext(), AtvStarInfo.class);
                            i.putExtra(Constants.EXTRAS_JSON_STRING, j.toString());

                            startActivity(i);
                        }
                    }
                }
            });

            alert.showSeletItem(getContext(), items);
        }
    }

    private void onBackCheck() {
        String contents = _edtContent.getText().toString();

        if (!FormatUtil.isNullorEmpty(contents)) {
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialogAlert, int which) {
                    if (which == Alert.BUTTON1) {
                        finish();
                    }
                }
            });
            alert.showAlert(getContext(), "입력 중인 내용이 있습니다.\n\n취소 하시겠습니까?", true, "확인", "아니오, 계속 입력합니다.");
            return;
        }

        finish();
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        onBackCheck();
    }

    private void onPhotoSelected(final int index) {

        _selectedPhotoIndex = index;

        System.out.println("onPhotoSelected");

        if (m_photoManger == null) {
            m_photoManger = new SelectPhotoManager(this);
            m_photoManger.setPhotoSelectCallback(this);
        }

        String[] items = new String[]{
                "앨범에서 선택", "취소"
        };

        if (_imgBeanArr[index].isExistsData()) {
            items = new String[]{
                    "앨범에서 선택", "삭제", "취소"
            };
        }

        AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
        adb.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    // 앨범에서 선택
                    m_photoManger.setIsCrop(false);
                    m_photoManger.setIsCropFreestyle(true);
                    m_photoManger.doPickFromGallery();
                } else if (which == 1) {
                    // 삭제
                    _imgBeanArr[_selectedPhotoIndex].remove();
                    _txtAttach01.setVisibility(View.VISIBLE);
                }
            }
        });
        adb.show();
    }

    private void callApi_boardPromo_ins(File file01, File file02, File file03) {

        LogUtil.e("==========callApi_boardPromo_ins : start==========");

        JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

        String nicks = JSONUtil.getString(json, "nick");

        String titles = _edtTitle.getText().toString();
        String contents = _edtContent.getText().toString();
        String urls = _edtUrl.getText().toString();

        if (FormatUtil.isNullorEmpty(contents)) {
            contents = titles;
        }

        if (file01 == null) {
            LogUtil.d("file01 is null");
        }
        if (file02 == null) {
            LogUtil.d("file02 is null");
        }
        if (file03 == null) {
            LogUtil.d("file03 is null");
        }

        RequestBody ssid = RequestBody.create(MediaType.parse("text/plain"), "boardPromo.ins");
        RequestBody member_no = RequestBody.create(MediaType.parse("text/plain"), SPUtil.getInstance().getUserNoEnc(getContext()));

        MultipartBody.Part pic01 = null;
        MultipartBody.Part pic02 = null;
        MultipartBody.Part pic03 = null;
        RequestBody nick = RequestBody.create(MediaType.parse("text/plain"), nicks);
        RequestBody title = RequestBody.create(MediaType.parse("text/plain"), titles);
        RequestBody title_v2 = RequestBody.create(MediaType.parse("text/plain"), titles);
        RequestBody content = RequestBody.create(MediaType.parse("text/plain"), contents);
        RequestBody link = RequestBody.create(MediaType.parse("text/plain"), urls);
        RequestBody sns_no = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(_sns_no));


        if (file01 != null) {
            LogUtil.d("file01  : " + file01.getAbsolutePath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file01);
            pic01 = MultipartBody.Part.createFormData("pic1", file01.getName(), requestFile);
        }
        if (file02 != null) {
            LogUtil.d("file02  : " + file02.getAbsolutePath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file02);
            pic02 = MultipartBody.Part.createFormData("pic2", file02.getName(), requestFile);
        }
        if (file03 != null) {
            LogUtil.d("file03  : " + file03.getAbsolutePath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file03);
            pic03 = MultipartBody.Part.createFormData("pic3", file03.getName(), requestFile);
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.boardPromo_ins(
                ssid
                , member_no
                , nick
                , title
                , title_v2
                , content
                , link
                , sns_no
                , pic01
                , pic02
                , pic03
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "관리자 확인 후, 투표권과 포인트를 적립해 드리겠습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        setResult(RESULT_OK);
                        finish();
                    }
                });
                alert.showAlert(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    /**
     * 이미지 크랍 완료
     *
     * @param image
     * @param file
     */
    @Override
    public void onSelectImageDone(Bitmap image, File file) {

        LogUtil.d("onSelectImageDone");

        if (image != null && file != null) {
            BitmapFactory.Options onlyBoundsOptions2 = CommonUtil.getBitmapFactoryFromUri(getContext(), Uri.fromFile(file));
            LogUtil.d("options.outWidth : " + onlyBoundsOptions2.outWidth);
            LogUtil.d("options.outHeight : " + onlyBoundsOptions2.outHeight);

            int photo_min_size = 320;

            // 핸드폰 디스플레이 사이즈보다 작은 사진은 선택 못하도록 합니다.
            if (onlyBoundsOptions2.outWidth <= photo_min_size || onlyBoundsOptions2.outHeight <= photo_min_size) {
                Alert alert = new Alert();
                alert.showAlert(getContext(), getString(R.string.please_photo_width_01));
                return;
            }

            _imgBeanArr[_selectedPhotoIndex].setImageBitmap(image, ImageView.ScaleType.CENTER_INSIDE);
            _imgBeanArr[_selectedPhotoIndex].setFile(file);

            _txtAttach01.setVisibility(View.GONE);
        } else {
            _imgBeanArr[_selectedPhotoIndex].remove();
            _txtAttach01.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFailedSelectImage(int errorCode, String err) {
        LogUtil.d("onFailedSelectImage");

        _txtAttach01.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDeleteImage() {
        LogUtil.d("onDeleteImage");
    }

    @Override
    public void onPermissionUpdated(int permission_type, boolean isGranted) {
        LogUtil.d("onPermissionUpdated");
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("onActivityResult, requestCode : " + requestCode);
        LogUtil.d("onActivityResult, resultCode : " + resultCode);
        LogUtil.d("onActivityResult, data : " + data);
        LogUtil.intent("onActivityResult", data);


        if (resultCode == Activity.RESULT_OK) {

            // 갤러리에서 사진 선택 시.
            if (requestCode == SelectPhotoManager.PICK_FROM_FILE) {
                if (data.getData() == null) {
                    new Alert().showAlert(getContext(), "사진을 가져오는 동안 오류가 발생하였습니다. 앱을 재시작 후에 다시 시도해주세요.");
                } else {
                    if (_selectedPhotoIndex < 0) {
                        new Alert().showAlert(this, "사진을 다시 선택해주세요.");
                        return;
                    }

                    String str = data.getData().toString();


                    LogUtil.d("onActivityResult, str : " + str);

                    String url = CommonUtil.getRealPathFromURI(getContext(), data.getData());
                    LogUtil.d("onActivityResult, url : " + url);

                    String mimeType = CommonUtil.getMimeType(url);
                    LogUtil.d("onActivityResult, mimeType : " + mimeType);

                    if (!FormatUtil.isNullorEmpty(mimeType) && (!mimeType.toLowerCase().endsWith("jpeg") && !mimeType.toLowerCase().endsWith("jpg") && !mimeType.toLowerCase().endsWith("png"))) {
                        new Alert().showAlert(getContext(), "사진은 jpg, png 형태의 파일만 등록하실 수 있습니다.");
                        return;
                    }
                }
            }
        }

        if (requestCode == UCrop.REQUEST_CROP || requestCode == SelectPhotoManager.PICK_FROM_FILE) {
            // m_photoManger null 인 경우가 발생한다.
            if (m_photoManger == null) {
                m_photoManger = new SelectPhotoManager(this);
            }
            m_photoManger.setPhotoSelectCallback(this);
            m_photoManger.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onDestroy() {
        if (m_photoManger != null) {
            m_photoManger.removeTempFile(getContext());
        }
        super.onDestroy();
    }
}
