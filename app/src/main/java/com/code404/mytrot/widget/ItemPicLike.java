package com.code404.mytrot.widget;

import android.app.Dialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class ItemPicLike extends LinearLayout {


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;


    private ImageView _img01;
    private TextView _txtHeart;


    public ItemPicLike(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemPicLike(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemPicLike(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_pic_like, this);

        _img01 = findViewById(R.id.img01);
        _txtHeart = findViewById(R.id.txtHeart);
    }

    protected void listeners() {
        _img01.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi();
            }
        });
    }

    private void callApi() {
        if (_txtHeart.isSelected() == false) {
            _like_cnt++;
            callApi_artist_artist_pic_like();
            _txtHeart.setSelected(true);
            _txtHeart.setText("+" + _like_cnt);
        } else {
            _like_cnt--;
            callApi_artist_artist_pic_unlike();
            _txtHeart.setSelected(false);
            _txtHeart.setText("+" + _like_cnt);
        }
    }

    private int _artist_pic_no = -1;
    private int _like_cnt = 0;

    public void setData(JSONObject json) {
        _artist_pic_no = JSONUtil.getInteger(json, "no");
        _like_cnt = JSONUtil.getInteger(json, "like_cnt");

        String imgA = JSONUtil.getStringUrl(json, "path");
        String like_yn = JSONUtil.getString(json, "like_yn");

        if (!FormatUtil.isNullorEmpty(imgA)) {
            // 모서리 라운딩 처리
            _img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
            _img01.setClipToOutline(true);

            Picasso.with(getContext())
                    .load(imgA)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img01);
        } else {
            Picasso.with(getContext()).load(R.drawable.img_noimg).fit().into(_img01);
        }

        _txtHeart.setSelected(like_yn.equals("Y"));
        _txtHeart.setText("+" + _like_cnt);
    }


    private void callApi_artist_artist_pic_like() {
        LogUtil.e("==========callApi_artist_artist_pic_like : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_artist_pic_like(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_pic_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_artist_artist_pic_unlike() {
        LogUtil.e("==========callApi_artist_artist_pic_unlike : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_artist_pic_unlike(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_pic_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }
}
