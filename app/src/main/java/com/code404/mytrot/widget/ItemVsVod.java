package com.code404.mytrot.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

public class ItemVsVod extends LinearLayout {


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;


    private ImageView _img01;
    private TextView _txtDesc;
    private View _baseChoice;
    private RadioButton _radio;
    private TextView _txtCount;


    private int _item_no = -1;
    private int _my_pick_item_no = -2;


    public ItemVsVod(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemVsVod(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemVsVod(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_vs_vod, this);

        _img01 = findViewById(R.id.img01);
        _txtDesc = findViewById(R.id.txtDesc);
        _baseChoice = findViewById(R.id.baseChoice);

        _radio = findViewById(R.id.radioCount);
        _txtCount = findViewById(R.id.txtCount);
    }

    protected void listeners() {
        _radio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClickViewListener != null) {
                    mOnClickViewListener.onClick(_baseChoice, _item_no);
                }
                setCheckStatus();
            }
        });
        _baseChoice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClickViewListener != null) {
                    mOnClickViewListener.onClick(_baseChoice, _item_no);
                }
                setCheckStatus();
            }
        });
    }


    public void setData(JSONObject json, int vs_pick_cnt, int my_pick_item_no) {
        _item_no = JSONUtil.getInteger(json, "no");
        _my_pick_item_no = my_pick_item_no;
        String imgA = JSONUtil.getStringUrl(json, "img01");
        String title = JSONUtil.getString(json, "title");
        String subject = JSONUtil.getString(json, "subject");
        int pick_cnt = JSONUtil.getInteger(json, "pick_cnt");

        if (!FormatUtil.isNullorEmpty(imgA)) {
            Picasso.with(getContext())
                    .load(imgA)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img01);
        } else {
            Picasso.with(getContext()).load(R.drawable.img_noimg).fit().into(_img01);
        }

        _txtDesc.setText(subject);

        if (vs_pick_cnt < 1) {
            _txtCount.setText("0% (0명)");
        } else {
            int per = pick_cnt * 100 / vs_pick_cnt;
            _txtCount.setText(per + "% (" + pick_cnt + "명)");
        }

        setCheckStatus();
    }

    private void setCheckStatus(){
        _radio.setChecked(false);
        _baseChoice.setSelected(false);

        if(_item_no == _my_pick_item_no) {
            _radio.setChecked(true);
            _baseChoice.setSelected(true);
        }
    }
}
