package com.code404.mytrot.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;

import org.json.JSONObject;

public class ItemPayment extends LinearLayout {

    private RelativeLayout _btnPay = null;
    private TextView _txtName = null;
    private TextView _txtDesc = null;
    private TextView _txtPrice = null;
    private ImageView _imgAd = null;
    private ImageView _imgTradeVote = null;

    JSONObject _json = null;


    private InterfaceSet.OnClickJsonListener mOnClickJsonListener = null;


    public ItemPayment(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemPayment(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemPayment(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickJsonListener l) {
        mOnClickJsonListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_payment, this);


        _btnPay = (RelativeLayout) findViewById(R.id.btnPay);
        _txtName = (TextView) findViewById(R.id.txtName);
        _txtDesc = findViewById(R.id.txtDesc);
        _txtPrice = (TextView) findViewById(R.id.txtPrice);
        _imgAd = findViewById(R.id.img_ad);
        _imgTradeVote = findViewById(R.id.img_trade_vote);

    }

    protected void listeners() {
        _btnPay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickJsonListener != null) {
                    mOnClickJsonListener.onClick(ItemPayment.this, -1, _json);
                }
            }
        });
    }

    public String getSKU() {
        String sku = JSONUtil.getString(_json, "sku");
        return sku;
    }

    public void setData(JSONObject json) {
        _json = json;

        String sku = JSONUtil.getString(json, "sku");

        if (sku.contains("remove_ad")) {
            _imgAd.setVisibility(View.VISIBLE);
            _imgTradeVote.setVisibility(View.GONE);
        } else if (sku.contains("auto_ticket")) {
            _imgAd.setVisibility(View.GONE);
            _imgTradeVote.setVisibility(View.VISIBLE);
        }

        String name = JSONUtil.getString(json, "name");
        int price = JSONUtil.getInteger(json, "price", 0);
        int a_price = JSONUtil.getInteger(json, "a_price", 0);
        int b_price = JSONUtil.getInteger(json, "b_price", 0);

        int vote_cnt = JSONUtil.getInteger(json, "vote_cnt", 0);
        int bonus_vote_cnt = JSONUtil.getInteger(json, "bonus_vote_cnt", 0);

        _txtDesc.setVisibility(View.GONE);

        if(sku.startsWith("auto_booster")) {
            String desc = "%s원 + %s원 = <strike>%s원</strike>";

            desc = String.format(desc, FormatUtil.toPriceFormat(a_price), FormatUtil.toPriceFormat(b_price), FormatUtil.toPriceFormat(a_price + b_price));
            _txtDesc.setText(Html.fromHtml(desc));
            _txtDesc.setVisibility(View.VISIBLE);
        }
        else if(sku.startsWith("vote_ticket")) {
            //String desc = "+ 보너스 %s장 = <font color='blue'><b>총 %s장</b></font>";
            String desc = "+ 보너스 %s장<br>= <b>총 %s장</b>";

            desc = String.format(desc, FormatUtil.toPriceFormat(bonus_vote_cnt), FormatUtil.toPriceFormat(vote_cnt + bonus_vote_cnt));
            _txtDesc.setText(Html.fromHtml(desc));
            _txtDesc.setVisibility(View.VISIBLE);
        }

        _txtName.setText(Html.fromHtml(name));
        _txtPrice.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "price")) + "원");
    }

}
