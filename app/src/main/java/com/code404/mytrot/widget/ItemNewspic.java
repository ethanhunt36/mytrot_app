package com.code404.mytrot.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

public class ItemNewspic extends LinearLayout {

    private TextView _txtMessage = null;
    private TextView _txtAuthor = null;
    private TextView _txtDate = null;
    private ImageView _img01 = null;

    private int _news_no = -1;


    public int getNewsNo(){
        return _news_no;
    }


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;


    public ItemNewspic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemNewspic(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemNewspic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_newspic, this);

        _txtMessage = findViewById(R.id.txtMessage);
        _txtAuthor = findViewById(R.id.txtAuthor);
        _txtDate = findViewById(R.id.txtDate);
        _img01 = findViewById(R.id.img01);
    }

    protected void listeners() {
        findViewById(R.id.baseBody).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClickViewListener != null) {
                    mOnClickViewListener.onClick(view, -1);
                }
            }
        });
    }


    public void setData(JSONObject json) {
        String img01 = JSONUtil.getStringUrl(json, "img_url");
        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        String author = JSONUtil.getString(json, "author");
        String category = JSONUtil.getString(json, "category");

        _news_no = JSONUtil.getInteger(json, "no");

        if (!FormatUtil.isNullorEmpty(reg_dttm)) reg_dttm = reg_dttm.substring(0, 10);

        String msg = !FormatUtil.isNullorEmpty(author) ? author + " > " + category : "";

        _txtMessage.setText(JSONUtil.getString(json, "title"));
        _txtAuthor.setText(msg);
        _txtDate.setText(reg_dttm);

        // 모서리 라운딩 처리
        _img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        _img01.setClipToOutline(true);
        //_img01.setScaleType(ImageView.ScaleType.CENTER_CROP);
        _img01.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        if (!FormatUtil.isNullorEmpty(img01)) {
            Picasso.with(getContext())
                    .load(img01)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img01);
            _img01.setVisibility(View.VISIBLE);
        } else {
            _img01.setVisibility(View.GONE);
        }
    }


}
