package com.code404.mytrot.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import retrofit2.Call;

public class ItemStarRoom extends LinearLayout {

    private TextView _txtMessage = null;
    private TextView _txtNickname = null;
    private TextView _txtLike = null;
    private TextView _txtComment = null;
    private LinearLayout _baseRight = null;
    private com.code404.mytrot.view.SquareImageView _img01 = null;
    private View _baseLine = null;


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;


    public ItemStarRoom(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemStarRoom(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemStarRoom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setVisibleRowLine(boolean isShow) {
        _baseLine.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_star_room, this);

        _txtMessage = (TextView) findViewById(R.id.txtMessage);
        _txtNickname = (TextView) findViewById(R.id.txtNickname);
        _txtLike = (TextView) findViewById(R.id.txtLike);
        _txtComment = (TextView) findViewById(R.id.txtComment);
        _baseRight = (LinearLayout) findViewById(R.id.baseRight);
        _img01 = (com.code404.mytrot.view.SquareImageView) findViewById(R.id.img01);
        _baseLine = (View) findViewById(R.id.baseLine);
    }

    protected void listeners() {
        findViewById(R.id.baseBody).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClickViewListener != null) {
                    mOnClickViewListener.onClick(view, -1);
                }
            }
        });
    }


    public void setData(JSONObject json) {
        String img01 = JSONUtil.getStringUrl(json, "img01");

        _txtMessage.setText(JSONUtil.getString(json, "cont"));
        _txtNickname.setText(JSONUtil.getString(json, "member_nick") + " Lv." + JSONUtil.getString(json, "member_lv"));

        _txtLike.setText("좋아요 " + JSONUtil.getString(json, "like_cnt"));
        _txtComment.setText("댓글 " + JSONUtil.getString(json, "comment_cnt"));

        // 모서리 라운딩 처리
        _img01.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
        _img01.setClipToOutline(true);

        if (!FormatUtil.isNullorEmpty(img01)) {
            Picasso.with(getContext())
                    .load(img01)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img01);
            _img01.setVisibility(View.VISIBLE);
        } else {
            _img01.setVisibility(View.GONE);
        }
    }


}
