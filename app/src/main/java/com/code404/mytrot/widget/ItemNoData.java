package com.code404.mytrot.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

public class ItemNoData extends LinearLayout {


    private TextView _txtNoData = null;

    public ItemNoData(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();

    }

    public ItemNoData(Context context) {
        super(context);
        init();

    }

    public ItemNoData(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }


    protected void init() {
        inflate(getContext(), R.layout.item_nodata, this);

        _txtNoData = findViewById(R.id.txtNoData);

    }


    public void setText(String msg) {
        _txtNoData.setText(msg);
    }
}
