package com.code404.mytrot.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAction;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class ItemMyVph extends LinearLayout {
    private TextView _txtMyVote = null;
    private TextView _txtMyPoint = null;
    private TextView _txtMyHeart = null;
    private View _base_vph_vote = null;
    private View _base_vph_point = null;
    private View _base_vph_heart = null;
    private TextView _txtTip01 = null;

    public ItemMyVph(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemMyVph(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemMyVph(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    protected void init() {
        inflate(getContext(), R.layout.layout_my_vph, this);


        _txtMyVote = findViewById(R.id.txtMyVote);
        _txtMyPoint = findViewById(R.id.txtMyPoint);
        _txtMyHeart = findViewById(R.id.txtMyHeart);


        _base_vph_vote = findViewById(R.id.base_vph_vote);
        _base_vph_point = findViewById(R.id.base_vph_point);
        _base_vph_heart = findViewById(R.id.base_vph_heart);


        _txtTip01 = findViewById(R.id.txtTip01);


        setMyVoteCountPoint();

        _txtTip01.setText(Html.fromHtml("<font color='red'><b>Tip.</b></font> 상단의 투표권, 포인트, 하트를 누르면<br>최애가수에게 바로 사용할 수 있어요."));
    }


    private void listeners() {
        _txtTip01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _txtTip01.setVisibility(View.GONE);
            }
        });

        // 투표권
        _base_vph_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int artist_no = SPUtil.getInstance().getUserFavoriteArtistNo(getContext());
                String artist_name = SPUtil.getInstance().getUserFavoriteArtistName(getContext());
                String artist_pic = SPUtil.getInstance().getUserFavoriteArtistPic(getContext());

                if (artist_no < 1 || FormatUtil.isNullorEmpty(artist_name)) {
                    new Alert().showAlert(getContext(), getContext().getString(R.string.txt_set_artist));
                    return;
                }

                callApi_artist_vote_listing(artist_no, true, new InterfaceSet.OnClickJsonListener() {
                    @Override
                    public void onClick(View v, int pos, JSONObject json) {

                        int vote_cnt_month = JSONUtil.getInteger(json, "vote_cnt_month", 0);
                        int my_vote_cnt = JSONUtil.getInteger(json, "my_vote_cnt", 0);

                        AlertPop alertPop = new AlertPop();
                        alertPop.setOnStringListener(new InterfaceSet.OnStringListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which, String str) {
                                if (which == Alert.BUTTON1) {
                                    int vote_cnt = Integer.parseInt(str);
                                    callApi_vote_do_vote(artist_no, artist_name, vote_cnt);
                                }
                            }
                        });
                        alertPop.showVote(getContext(), artist_no, artist_name, artist_pic, vote_cnt_month, my_vote_cnt);
                    }
                });
            }
        });

        // 포인트
        _base_vph_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int artist_no = SPUtil.getInstance().getUserFavoriteArtistNo(getContext());
                String artist_name = SPUtil.getInstance().getUserFavoriteArtistName(getContext());
                String artist_pic = SPUtil.getInstance().getUserFavoriteArtistPic(getContext());

                LogUtil.d("artist_no : " + artist_no);
                LogUtil.d("artist_name : " + artist_name);
                LogUtil.d("artist_pic : " + artist_pic);

                if (artist_no < 1 || FormatUtil.isNullorEmpty(artist_name)) {
                    new Alert().showAlert(getContext(), getContext().getString(R.string.txt_set_artist));
                    return;
                }

                callApi_donate_ing_list(artist_no, true, new InterfaceSet.OnClickJsonListener() {
                    @Override
                    public void onClick(View v, int pos, JSONObject json) {
                        final int donate_nth = JSONUtil.getInteger(json, "donate_nth", 0);
                        final int donate_cur_point = JSONUtil.getInteger(json, "donate_cur_point", 0);
                        final int donate_goal_point = JSONUtil.getInteger(json, "donate_goal_point", 0);

                        AlertPop alert = new AlertPop();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                //callApi_donate_ing_list(true, true);

                                setMyVoteCountPoint();
                            }
                        });
                        alert.showDonate(getContext(), artist_no, artist_name, artist_pic, donate_nth, donate_goal_point, donate_cur_point);
                    }
                });
            }
        });

        // 하트
        _base_vph_heart.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                int artist_no = SPUtil.getInstance().getUserFavoriteArtistNo(getContext());
                String artist_name = SPUtil.getInstance().getUserFavoriteArtistName(getContext());
                String artist_pic = SPUtil.getInstance().getUserFavoriteArtistPic(getContext());

                LogUtil.d("artist_no : " + artist_no);
                LogUtil.d("artist_name : " + artist_name);
                LogUtil.d("artist_pic : " + artist_pic);

                if (artist_no < 1 || FormatUtil.isNullorEmpty(artist_name)) {
                    new Alert().showAlert(getContext(), getContext().getString(R.string.txt_set_artist));
                    return;
                }

                callApi_eventFund_get_list(artist_no, true, new InterfaceSet.OnClickJsonListener() {
                    @Override
                    public void onClick(View v, int pos, JSONObject json) {

                        final int event_fund_no = JSONUtil.getInteger(json, "no", 0);
                        final String subject = JSONUtil.getString(json, "subject");
                        final int artist_no = JSONUtil.getInteger(json, "artist_no", 0);
                        int heart_goal = JSONUtil.getInteger(json, "heart_goal", 0);
                        int heart_cur = JSONUtil.getInteger(json, "heart_cur", 0);

                        AlertPop alert = new AlertPop();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                //callApi_eventFund_get_list(true);

                                setMyVoteCountPoint();
                            }
                        });
                        alert.showEventFund(getContext(), event_fund_no, artist_no, subject, artist_pic, heart_goal, heart_cur);
                    }
                });


            }
        });

    }


    public void setMyVoteCountPoint() {
        JSONObject userJson = SPUtil.getInstance().getUserInfo(getContext());

        double vote_cnt = 0;
        double point = 0;
        double heart = 0;

        if (userJson != null) {
            vote_cnt = JSONUtil.getInteger(userJson, "vote_cnt", 0);
            point = JSONUtil.getDouble(userJson, "point", 0);
            heart = JSONUtil.getDouble(userJson, "heart", 0);
        }

        _txtMyVote.setText(FormatUtil.toPriceFormat(vote_cnt) + "장");
        _txtMyPoint.setText(FormatUtil.toPriceFormat(point) + "P");
        _txtMyHeart.setText(FormatUtil.toPriceFormat(heart) + "개");
    }

    private void callApi_vote_do_vote(final int artist_no, final String artist_name, final int vote_cnt) {
        LogUtil.e("==========callApi_vote_do_vote : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_do_vote(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , artist_no
                , vote_cnt
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    if (FormatUtil.isNullorEmpty(resultMessage)) {
                        resultMessage = String.format("%s 님에게 투표권 %d장이 투표 되었습니다.", artist_name, vote_cnt);
                    }

                    if (!FormatUtil.isNullorEmpty(resultMessage)) {
                        AlertAction alert = new AlertAction();
                        String title = "투표 완료";
                        String msg = resultMessage;
                        int imgResourceID = FormatUtil.getCurrentMinute() % 2 == 0 ? R.drawable.img_pop_vote_1 : R.drawable.img_pop_vote_2;
                        alert.showAlertAction(getContext(), title, msg, imgResourceID);
                    }

                    JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
                    int f_vote_cnt = JSONUtil.getInteger(userInfo, "vote_cnt");
                    f_vote_cnt = f_vote_cnt - vote_cnt;

                    JSONUtil.puts(userInfo, "vote_cnt", f_vote_cnt);
                    SPUtil.getInstance().setUserInfo(getContext(), userInfo);
                    setMyVoteCountPoint();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    Dialog _dialog = null;


    private void callApi_artist_vote_listing(final int f_artist_no, boolean isShowProgress, final InterfaceSet.OnClickJsonListener listener) {


        LogUtil.e("==========callApi_artist_vote_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_vote_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , -1
                , "vote_cnt_month"
                , ""
                , "N"
                , ""
                , ""
        );

        if (isShowProgress) _dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                if (isShowProgress) ProgressDialogUtil.dismiss(_dialog);

                try {
                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                    if (f_artist_no > 0) {
                        for (int i = 0; i < jsonList.length(); i++) {
                            JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                            int no = JSONUtil.getInteger(j, "no");

                            if (f_artist_no == no) {
                                if (listener != null) {
                                    listener.onClick(null, i, j);
                                }
                                return;
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                if (isShowProgress) ProgressDialogUtil.dismiss(_dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_donate_ing_list(final int f_artist_no, boolean isShowProgress, final InterfaceSet.OnClickJsonListener listener) {

        LogUtil.e("==========callApi_donate_ing_list : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.donate_ing_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , -1
        );

        if (isShowProgress) _dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(_dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");


                    if (f_artist_no > 0) {
                        for (int i = 0; i < jsonList.length(); i++) {
                            JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                            int no = JSONUtil.getInteger(j, "no");

                            if (f_artist_no == no) {
                                if (listener != null) {
                                    listener.onClick(null, i, j);
                                }
                                return;
                            }
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(_dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_eventFund_get_list(final int f_artist_no, boolean isShowProgress, final InterfaceSet.OnClickJsonListener listener) {


        LogUtil.e("==========callApi_eventFund_get_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.eventFund_get_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "S"
                , Constants.RECORD_SIZE_MAX
                , 0
        );

        if (isShowProgress) _dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(_dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");


                    JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());

                    List<JSONObject> list = new ArrayList<JSONObject>();

                    for (int i = 0; i < jsonList.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                        int no = JSONUtil.getInteger(j, "artist_no");

                        if (no == f_artist_no) {
                            if (listener != null) {
                                listener.onClick(null, i, j);
                            }
                            break;
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(_dialog);
                LogUtil.d("onFailure");
            }
        });
    }
}
