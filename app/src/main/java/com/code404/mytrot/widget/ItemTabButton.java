package com.code404.mytrot.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;

public class ItemTabButton extends LinearLayout {

    private int _index = -1;

    private Button _btnTab = null;
    private TextView _txtMenu = null;
    private TextView _txtCountMsg = null;
    private View _baseLine = null;

    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;


    public ItemTabButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemTabButton(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemTabButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_tab_button, this);

        _btnTab = (Button) findViewById(R.id.btnTab);
        _txtMenu = (TextView) findViewById(R.id.txtMenu);
        _txtCountMsg = (TextView) findViewById(R.id.txtCountMsg);
        _baseLine = (View) findViewById(R.id.baseLine);


    }

    protected void listeners() {
        _btnTab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickViewListener != null) {
                    mOnClickViewListener.onClick(v, _index);
                }
            }
        });
    }

    public void setIndex(int index) {
        _index = index;
    }


    public void setTitle(String title) {
        setTitle(title, 0);
    }

    public void setTitle(String title, int count) {
        _btnTab.setText("");
        _txtMenu.setText(title);
        _txtCountMsg.setText(String.valueOf(count));
        _txtCountMsg.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
    }

    public void setCount(int count) {
        _txtCountMsg.setText(String.valueOf(count));
        _txtCountMsg.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
    }

    private int mColor_over = 0xff222222;
    private ColorStateList mTabColorList = null;

    public void setSelected(boolean selected) {

        int[][] states = new int[][]
                {new int[]
                        {android.R.attr.state_pressed}, new int[]
                        {android.R.attr.state_selected}, new int[]
                        {0}};
        int[] colors = new int[]
                {0xffffffff, 0xffffffff, 0xff6a7285};
        mTabColorList = new ColorStateList(states, colors);
        if (selected) {
            _txtMenu.setTextColor(mColor_over);
            _txtMenu.setTextSize(15);
            _baseLine.setVisibility(View.VISIBLE);
        } else {
            _txtMenu.setTextColor(mTabColorList);
            _txtMenu.setTextSize(14);
            _baseLine.setVisibility(View.GONE);
        }
    }

}
