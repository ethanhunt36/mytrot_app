package com.code404.mytrot.widget;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ItemStarRoomTop extends LinearLayout {


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;


    private TextView _txtTitle;
    private TextView _txtRank01;
    private TextView _txtRank02;
    private TextView _txtRank03;
    private TextView _txtDate ;


    public ItemStarRoomTop(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemStarRoomTop(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemStarRoomTop(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_starroom_top, this);

        _txtTitle = findViewById(R.id.txtTitle);
        _txtRank01 = findViewById(R.id.txtRank01);
        _txtRank02 = findViewById(R.id.txtRank02);
        _txtRank03 = findViewById(R.id.txtRank03);
        _txtDate = findViewById(R.id.txtDate);
    }

    protected void listeners() {

    }

    public void setTitle(String title) {
        _txtTitle.setText(title);

        _txtRank01.setText("1. 없음");
        _txtRank02.setText("2. 없음");
        _txtRank03.setText("3. 없음");
    }


    public void setData(JSONArray jsonArr, String myNick) {

        JSONObject json = null;
        List<TextView> list = new ArrayList<TextView>();
        list.add(_txtRank01);
        list.add(_txtRank02);
        list.add(_txtRank03);

        String reg_dttm = "";
        for (int i = 0; i < jsonArr.length(); i++) {
            json = JSONUtil.getJSONObject(jsonArr, i);

            String nick = JSONUtil.getString(json, "nick");
            String lv = JSONUtil.getString(json, "lv");
            reg_dttm = JSONUtil.getString(json, "reg_dttm");

            String txt = String.format("%d. %s Lv.%s", i + 1, nick, lv);
            if (myNick.equals(nick)) {
                txt = "<font color='#ff9900'><b>" + txt + "</b></font>";
            }
            list.get(i).setText(Html.fromHtml(txt));
        }

        if(!FormatUtil.isNullorEmpty(reg_dttm) && reg_dttm.length() > 16){
            reg_dttm = reg_dttm.substring(0, 16);
        } else {
            reg_dttm = "-";
        }

        _txtDate.setText("최근 업데이트\n" + reg_dttm);
    }
}
