package com.code404.mytrot.widget;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

public class ItemStarroomSummary extends LinearLayout {


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;

    private TextView _txtTitle = null;
    private TextView _txtDesc = null;

    public ItemStarroomSummary(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemStarroomSummary(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemStarroomSummary(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_starroom_summary, this);

        _txtTitle = findViewById(R.id.txtTitle);
        _txtDesc = findViewById(R.id.txtDesc);

        _txtDesc.setText("");
    }

    protected void listeners() {
        findViewById(R.id.baseBody).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClickViewListener != null) {
                    mOnClickViewListener.onClick(view, -1);
                }
            }
        });
    }


    public void setTitle(String title) {
        _txtTitle.setText(title);
    }

    public void setDesc(String desc) {
        _txtDesc.setText(Html.fromHtml(desc));
    }


    public void setData(JSONObject json) {

    }

}
