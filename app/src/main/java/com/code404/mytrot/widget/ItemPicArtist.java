package com.code404.mytrot.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.SquareImageView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import retrofit2.Call;

public class ItemPicArtist extends LinearLayout {


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;

    private SquareImageView _imgProfile;
    private TextView _txtTitle = null;
    private TextView _txtHeart = null;
    private TextView _txtNick = null;
    private ImageButton _btnDelete = null;



    public ItemPicArtist(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemPicArtist(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemPicArtist(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_pic_artist, this);

        _imgProfile = (SquareImageView) findViewById(R.id.imgProfile);
        _txtTitle = findViewById(R.id.txtNick);
        _txtHeart = findViewById(R.id.txtHeart);
        _txtNick = findViewById(R.id.txtNick);
        _btnDelete = findViewById(R.id.btnDelete);
    }

    protected void listeners() {
        _imgProfile.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi();
            }
        });

        _btnDelete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnClickViewListener != null) {
                    mOnClickViewListener.onClick(view, _artist_pic_no);
                }
            }
        });
    }


    private int _artist_pic_no = -1;
    private int _like_cnt = 0;

    public void setData(JSONObject json) {
        _artist_pic_no = JSONUtil.getInteger(json, "no");
        _like_cnt = JSONUtil.getInteger(json, "like_cnt");


        String imgA = JSONUtil.getStringUrl(json, "path");
        String like_yn = JSONUtil.getString(json, "like_yn");
        String member_no = JSONUtil.getString(json, "member_no");
        String member_no_enc = SPUtil.getInstance().getUserNoEnc(getContext());
        String member_nick = JSONUtil.getString(json, "member_nick");
        String member_lv = JSONUtil.getString(json, "member_lv");

        _btnDelete.setVisibility(member_no.equals(member_no_enc) ? View.VISIBLE : View.GONE);

        if (!FormatUtil.isNullorEmpty(imgA)) {
            // 모서리 라운딩 처리
            _imgProfile.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
            _imgProfile.setClipToOutline(true);

            Picasso.with(getContext())
                    .load(imgA)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_imgProfile);
        } else {
            Picasso.with(getContext()).load(R.drawable.img_noimg).fit().into(_imgProfile);
        }

        _txtHeart.setSelected(like_yn.equals("Y"));
        _txtHeart.setText("+" + _like_cnt);

        _txtNick.setText("익명");

        if (!FormatUtil.isNullorEmpty(member_nick)) {
            _txtNick.setText(member_nick + " Lv." + member_lv);
        }
    }


    private void callApi() {
        if (_txtHeart.isSelected() == false) {
            _like_cnt++;
            callApi_artist_artist_pic_like();
            _txtHeart.setSelected(true);
            _txtHeart.setText("+" + _like_cnt);
        } else {
            _like_cnt--;
            callApi_artist_artist_pic_unlike();
            _txtHeart.setSelected(false);
            _txtHeart.setText("+" + _like_cnt);
        }



    }


    private void callApi_artist_artist_pic_like() {
        LogUtil.e("==========callApi_artist_artist_pic_like : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_artist_pic_like(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_pic_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_artist_artist_pic_unlike() {
        LogUtil.e("==========callApi_artist_artist_pic_unlike : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_artist_pic_unlike(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_pic_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }
}
