package com.code404.mytrot.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;

import org.json.JSONObject;

public class ItemLuckyBall extends RelativeLayout {

    private RelativeLayout _baseBall = null;
    private TextView _txtBall = null;
    private TextView _txtCount = null;

    JSONObject _json = null;


    public ItemLuckyBall(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        //listeners();
    }

    public ItemLuckyBall(Context context) {
        super(context);
        init();
        //listeners();
    }

    public ItemLuckyBall(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        //listeners();
    }


    protected void init() {
        inflate(getContext(), R.layout.item_lucky_ball, this);


        _baseBall = (RelativeLayout) findViewById(R.id.baseBall);
        _txtBall = (TextView) findViewById(R.id.txtBall);
        _txtCount = findViewById(R.id.txtCount);

    }

    public void setData(int num) {

        if (num <= 10) _baseBall.setBackgroundResource(R.drawable.bg_ball1);
        else if (num <= 20) _baseBall.setBackgroundResource(R.drawable.bg_ball2);
        else if (num <= 30) _baseBall.setBackgroundResource(R.drawable.bg_ball3);
        else if (num <= 40) _baseBall.setBackgroundResource(R.drawable.bg_ball4);
        else if (num <= 45) _baseBall.setBackgroundResource(R.drawable.bg_ball5);

        _txtBall.setText(String.valueOf(num));
    }


    public void setData(JSONObject json) {
        int num = JSONUtil.getInteger(json, "num");
        int count = JSONUtil.getInteger(json, "count");

        setData(num);

        _txtCount.setText("X" + count);
        _txtCount.setVisibility(View.VISIBLE);
    }

//    public void setData(JSONObject json) {
//        _json = json;
//
//        _txtName.setText(JSONUtil.getString(json, "name"));
//        _txtPrice.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "price")) + "원");
//    }

}
