package com.code404.mytrot.widget;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdRevenueListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.nativeAds.MaxNativeAdListener;
import com.applovin.mediation.nativeAds.MaxNativeAdLoader;
import com.applovin.mediation.nativeAds.MaxNativeAdView;
import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
//import com.fsn.cauly.CaulyAdInfo;
//import com.fsn.cauly.CaulyNativeAdInfoBuilder;
//import com.fsn.cauly.CaulyNativeAdView;
//import com.fsn.cauly.CaulyNativeAdViewListener;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.android.gms.ads.nativead.NativeAdOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static com.google.android.gms.ads.nativead.NativeAdOptions.ADCHOICES_TOP_LEFT;
import static com.google.android.gms.ads.nativead.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_SQUARE;

import androidx.annotation.NonNull;

public class ItemNativeAd extends LinearLayout {

    private static String TAG = "ItemNativeAd";

    private static String AD_CAULY_BIG = "cauly_big";
    private static String AD_CAULY_SMALL = "cauly_small";
    private static String AD_GOOGLE = "google";
    private static String AD_MAX = "max";

    private List<String> _ad_type_list = new ArrayList<String>();
    private Queue<String> _ad_queue = new LinkedList();
    private com.google.android.ads.nativetemplates.TemplateView _adNativeView = null;
    private AdRequest _adRequest = null;
    private String _ad_id = "";
    private AdLoader _adLoader = null;

    private MaxNativeAdLoader nativeAdLoader;
    private MaxAd loadedNativeAd;
    private RelativeLayout _base_ad_etc = null;

    public ItemNativeAd(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemNativeAd(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemNativeAd(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    protected void init() {

        LogUtil.d(TAG, "initAd");

        inflate(getContext(), R.layout.item_native_ad, this);

        _adNativeView = findViewById(R.id.adNativeView);
        _base_ad_etc = findViewById(R.id.base_ad_etc);

        _adNativeView.setVisibility(View.GONE);


        if (this.getVisibility() == View.GONE) {
            LogUtil.d(TAG, "this.getVisibility() == View.GONE");
            return;
        }

        initAd();
    }

    protected void listeners() {

    }

    public void setData(JSONObject json) {

    }


    public void initAd() {

        LogUtil.d(TAG, "initAd, Start");

        _ad_type_list.add(AD_MAX);
        _ad_type_list.add(AD_CAULY_BIG);
        _ad_type_list.add(AD_CAULY_SMALL);
        _ad_type_list.add(AD_GOOGLE);
//
        Collections.shuffle(_ad_type_list);

        for (String s : _ad_type_list) {
            LogUtil.d(TAG, "ad_type : " + s);
            _ad_queue.offer(s);
        }

        _adNativeView.setVisibility(View.GONE);
        _base_ad_etc.setVisibility(View.GONE);

        if (SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                || SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false
                || Constants.DISPLAY_AD == false ) {
            LogUtil.d(TAG, "getIsHaveAdRemoveItem : true");
            return;
        }

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            showOrder();
        } else {
            loadAd_Google();
        }

        LogUtil.d(TAG, "initAd, End");
    }


    private void showOrder() {
        LogUtil.d(TAG, "showOrder");

        try {
            String ad_type = _ad_queue.poll();

            if (FormatUtil.isNullorEmpty(ad_type)) return;

//            if (AD_CAULY_BIG.equals(ad_type)) loadAd_Cauly_Big();
//            if (AD_CAULY_SMALL.equals(ad_type)) loadAd_Cauly_Small();
            if (AD_MAX.equals(ad_type)) loadAd_Max();
            if (AD_GOOGLE.equals(ad_type)) loadAd_Google();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void loadAd_Max() {
        LogUtil.d(TAG, "loadAd_Max");

        nativeAdLoader = new MaxNativeAdLoader(Constants.KEY_MAX_NATIVE_SMALL_ID, getContext());
        nativeAdLoader.setRevenueListener(new MaxAdRevenueListener() {
            @Override
            public void onAdRevenuePaid(MaxAd ad) {
                LogUtil.d(TAG, "loadAd_Max, onAdRevenuePaid");
            }
        });
        nativeAdLoader.setNativeAdListener(new MaxNativeAdListener() {
            @Override
            public void onNativeAdLoaded(final MaxNativeAdView nativeAdView, final MaxAd nativeAd) {
                LogUtil.d(TAG, "loadAd_Max, onNativeAdLoaded, nativeAdView : " + (nativeAdView == null ? "null" : nativeAdView.toString()));
                LogUtil.d(TAG, "loadAd_Max, onNativeAdLoaded, nativeAd : " + (nativeAd == null ? "null" : nativeAd.toString()));

                try {
                    // Clean up any pre-existing native ad to prevent memory leaks.
                    if (loadedNativeAd != null) {
                        nativeAdLoader.destroy(loadedNativeAd);
                    }

                    LogUtil.d(TAG, "loadAd_Max, onNativeAdLoaded[1]");
                    // Save ad for cleanup.
                    loadedNativeAd = nativeAd;

                    if (nativeAdView != null) {
                        _base_ad_etc.removeAllViews();
                        _base_ad_etc.addView(nativeAdView);


                        _adNativeView.setVisibility(View.GONE);
                        _base_ad_etc.setVisibility(View.VISIBLE);
                    }

                    LogUtil.d(TAG, "loadAd_Max, onNativeAdLoaded[2]");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (nativeAdView == null) {
                        LogUtil.d(TAG, "loadAd_Max, onNativeAdLoaded[3]");
                        showOrder();
                    }
                }


            }

            @Override
            public void onNativeAdLoadFailed(final String adUnitId, final MaxError error) {
                // Native ad load failed.
                // AppLovin recommends retrying with exponentially higher delays up to a maximum delay.
                LogUtil.d(TAG, "loadAd_Max, onNativeAdLoadFailed");

                showOrder();
            }
        });

        nativeAdLoader.loadAd();
    }


//    private void loadAd_Cauly_Small() {
//
//        LogUtil.d(TAG, "loadAd_Cauly_Small");
//
//        CaulyAdInfo adInfo = new CaulyNativeAdInfoBuilder(Constants.KEY_CAULY_AD_ID)
//                .layoutID(R.layout.activity_native_view)
//                .iconImageID(R.id.icon)
//                .titleID(R.id.title)
//                .subtitleID(R.id.subtitle)
//                .sponsorPosition(R.id.sponsor, CaulyAdInfo.Direction.CENTER)
//                .build();
//        CaulyNativeAdView nativeAd = new CaulyNativeAdView(getContext());
//        nativeAd.setAdInfo(adInfo);
//        nativeAd.setAdViewListener(new CaulyNativeAdViewListener() {
//            @Override
//            public void onReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, boolean b) {
//                LogUtil.d(TAG, "loadAd_Cauly_Small, onReceiveNativeAd, b : " + b);
//
//                caulyNativeAdView.attachToView(_base_ad_etc);
//
//                _adNativeView.setVisibility(View.GONE);
//                _base_ad_etc.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onFailedToReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, int i, String s) {
//                LogUtil.d(TAG, "loadAd_Cauly_Small, onFailedToReceiveNativeAd, i : " + i + ", s : " + s);
//
//                showOrder();
//            }
//        });
//        nativeAd.request();
//    }
//
//    private void loadAd_Cauly_Big() {
//
//        LogUtil.d(TAG, "loadAd_Cauly_Big");
//
//        CaulyAdInfo adInfo = new CaulyNativeAdInfoBuilder(Constants.KEY_CAULY_AD_ID)
//                .layoutID(R.layout.activity_native_cardlistview) // 네이티브애드에 보여질 디자인을 작성하여 등록한다.
//                .mainImageID(R.id.image)                        // 메인 이미지 등록
//                .iconImageID(R.id.icon)                            // 아이콘 등록
//                .titleID(R.id.title)                            // 제목 등록
//                .subtitleID(R.id.subtitle)                        // 부제목 등록
//                .textID(R.id.description)                        //자세한 설명 등록
//                .mainImageOrientation(CaulyAdInfo.Orientation.PORTRAIT)
//                .sponsorPosition(R.id.sponsor, CaulyAdInfo.Direction.LEFT)
//                .build();
//        CaulyNativeAdView nativeView = new CaulyNativeAdView(getContext());
//        nativeView.setAdInfo(adInfo);
//        nativeView.setAdViewListener(new CaulyNativeAdViewListener() {
//            @Override
//            public void onReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, boolean b) {
//                LogUtil.d(TAG, "loadAd_Cauly_Big, onReceiveNativeAd, b : " + b);
//
//                caulyNativeAdView.attachToView(_base_ad_etc);
//
//                _adNativeView.setVisibility(View.GONE);
//                _base_ad_etc.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public void onFailedToReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, int i, String s) {
//                LogUtil.d(TAG, "loadAd_Cauly_Big, onFailedToReceiveNativeAd, i : " + i + ", s : " + s);
//                showOrder();
//            }
//        });
//        nativeView.request();
//    }


    private void loadAd_Google() {

        LogUtil.d(TAG, "loadAd_Google");

        LogUtil.e("loadAd");
        LogUtil.e("loadAd, _adLoader : " + (_adLoader == null ? "null" : "ok"));
        LogUtil.e("loadAd, _adRequest : " + (_adRequest == null ? "null" : "ok"));

        _ad_id = Constants.KEY_AD_NATIVE;
        _adRequest = new AdRequest.Builder()
                .build();

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                LogUtil.d(TAG, "MobileAds, onInitializationComplete");
            }
        });

        LogUtil.d(TAG, "ItemNativeAd, TRY");

        _adLoader = new AdLoader.Builder(getContext(), _ad_id)
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        LogUtil.d(TAG, "ItemNativeAd, onUnifiedNativeAdLoaded, _adLoader.isLoading() : " + _adLoader.isLoading());
                        // some code that displays the ad.

                        if (_adLoader.isLoading()) {

                            _adNativeView.setVisibility(View.VISIBLE);
                            _base_ad_etc.setVisibility(View.GONE);

                            try {
                                ColorDrawable c = new ColorDrawable();
                                c.setColor(getContext().getResources().getColor(R.color.white));
                                NativeTemplateStyle styles = new
                                        NativeTemplateStyle.Builder().withMainBackgroundColor(c).build();
                                _adNativeView.setStyles(styles);
                                _adNativeView.setNativeAd(nativeAd);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            // The AdLoader has finished loading ads.
                            LogUtil.d(TAG, "ItemNativeAd, FAIL");
                        }
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdClicked() {
                        super.onAdClicked();
                        LogUtil.d("NativeAd, onAdClicked");
                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        LogUtil.d("NativeAd, onAdClosed");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        LogUtil.d("NativeAd, onAdFailedToLoad");
                        LogUtil.d("NativeAd, onAdFailedToLoad, loadAdError : " + loadAdError.toString());

                        showOrder();
                    }

                    @Override
                    public void onAdImpression() {
                        super.onAdImpression();
                        LogUtil.d("NativeAd, onAdImpression");
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        LogUtil.d("NativeAd, onAdLoaded");
                    }

                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                        LogUtil.d("NativeAd, onAdOpened");
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        .setReturnUrlsForImageAssets(false)
                        .setAdChoicesPlacement(ADCHOICES_TOP_LEFT)
                        .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_SQUARE)
                        .build()
                )
                .build();


        _adLoader.loadAds(_adRequest, 2);
    }


}
