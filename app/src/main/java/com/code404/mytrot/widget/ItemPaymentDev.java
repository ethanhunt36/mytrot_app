package com.code404.mytrot.widget;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;

import org.json.JSONObject;

public class ItemPaymentDev extends LinearLayout {

    private View _btnPay = null;
    private TextView _txtName = null;
    private TextView _txtPrice = null;
    private TextView _txtDesc = null;

    JSONObject _json = null;


    private InterfaceSet.OnClickJsonListener mOnClickJsonListener = null;


    public ItemPaymentDev(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemPaymentDev(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemPaymentDev(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickJsonListener l) {
        mOnClickJsonListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_payment_dev, this);


        _btnPay = findViewById(R.id.btnPay);
        _txtName = (TextView) findViewById(R.id.txtName);
        _txtPrice = (TextView) findViewById(R.id.txtPrice);
        _txtDesc = findViewById(R.id.txtDesc);

    }

    protected void listeners() {
        _btnPay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnClickJsonListener != null) {
                    mOnClickJsonListener.onClick(ItemPaymentDev.this, -1, _json);
                }
            }
        });
    }

    public void setData(JSONObject json) {
        _json = json;

        _txtName.setText(JSONUtil.getString(json, "description"));
        _txtPrice.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "price")) + "원");

        _txtDesc.setText("");
        _txtDesc.setVisibility(View.GONE);

        int vote_cnt = JSONUtil.getInteger(json, "vote_cnt");
        int point = JSONUtil.getInteger(json, "point");

        int bonus_vote_cnt = JSONUtil.getInteger(json, "bonus_vote_cnt");
        int bonus_point = JSONUtil.getInteger(json, "bonus_point");

        String tag = JSONUtil.getString(json, "tag");

        if (!FormatUtil.isNullorEmpty(tag)) tag = String.format("<b>[%s]</b> ", tag);

        if (vote_cnt > 0 && point > 0) {
            String msg = String.format("투표권 <font color='#d4364f'>%s장</font>과 포인트 <font color='#d4364f'>%sP</font>를 선물로 드려요.", FormatUtil.toPriceFormat(vote_cnt), FormatUtil.toPriceFormat(point));
            String bonus_msg = "";
            if (bonus_vote_cnt > 0 && bonus_point > 0) {
                bonus_msg += "<br>* " + tag + "<font color='#d4364f'>보너스</font> 투표권 <font color='#d4364f'><b>" + FormatUtil.toPriceFormat(bonus_vote_cnt) + "장</b></font>과 포인트 <font color='#d4364f'><b>" + FormatUtil.toPriceFormat(bonus_point) + "P</b></font> 추가 *";
            } else if (bonus_vote_cnt > 0) {
                bonus_msg += "<br>* " + tag + "<font color='#d4364f'>보너스</font> 투표권 <font color='#d4364f'><b>" + FormatUtil.toPriceFormat(bonus_vote_cnt) + "장</b></font> 추가 *";
            } else if (bonus_point > 0) {
                bonus_msg += "<br>* " + tag + "<font color='#d4364f'>보너스</font> 포인트 <font color='#d4364f'><b>" + FormatUtil.toPriceFormat(bonus_point) + "P</b></font> 추가 *";
            }

            _txtDesc.setText(Html.fromHtml(msg + bonus_msg));
            _txtDesc.setVisibility(View.VISIBLE);
        }
    }

}
