package com.code404.mytrot.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.util.JSONUtil;

import org.json.JSONObject;

public class ItemPPRank extends LinearLayout {

    private int _rank = -1;

    private LinearLayout _baseTop = null;
    private TextView _txtReward = null;
    private LinearLayout _baseFoot = null;
    private TextView _txtCount = null;


    private InterfaceSet.OnClickViewListener mOnClickViewListener = null;


    public ItemPPRank(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        listeners();
    }

    public ItemPPRank(Context context) {
        super(context);
        init();
        listeners();
    }

    public ItemPPRank(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        listeners();
    }


    public void setOnClickViewListener(InterfaceSet.OnClickViewListener l) {
        mOnClickViewListener = l;
    }

    protected void init() {
        inflate(getContext(), R.layout.item_pp_rank, this);


        _baseTop = (LinearLayout) findViewById(R.id.baseTop);
        _txtReward = (TextView) findViewById(R.id.txtReward);
        _baseFoot = (LinearLayout) findViewById(R.id.baseFoot);
        _txtCount = (TextView) findViewById(R.id.txtCount);

    }

    protected void listeners() {

    }

    public void setRank(int rank) {
        _rank = rank;

        switch (_rank) {
            case 1:
                _baseTop.setBackgroundResource(R.drawable.bg_prize_1_top);
                _baseFoot.setBackgroundResource(R.drawable.bg_prize_1_foot);
                _txtReward.setTextColor(getResources().getColor(R.color.faded_red_two));
                _txtCount.setTextColor(getResources().getColor(R.color.faded_red_two));
                break;
            case 2:
                _baseTop.setBackgroundResource(R.drawable.bg_prize_2_top);
                _baseFoot.setBackgroundResource(R.drawable.bg_prize_2_foot);
                _txtReward.setTextColor(getResources().getColor(R.color.squash));
                _txtCount.setTextColor(getResources().getColor(R.color.squash));
                break;
            case 3:
                _baseTop.setBackgroundResource(R.drawable.bg_prize_3_top);
                _baseFoot.setBackgroundResource(R.drawable.bg_prize_3_foot);
                _txtReward.setTextColor(getResources().getColor(R.color.dark_sky_blue));
                _txtCount.setTextColor(getResources().getColor(R.color.dark_sky_blue));
                break;
            case 4:
                _baseTop.setBackgroundResource(R.drawable.bg_prize_4_top);
                _baseFoot.setBackgroundResource(R.drawable.bg_prize_4_foot);
                _txtReward.setTextColor(getResources().getColor(R.color.brownish_grey));
                _txtCount.setTextColor(getResources().getColor(R.color.brownish_grey));
                break;
            case 5:
                _baseTop.setBackgroundResource(R.drawable.bg_prize_5_top);
                _baseFoot.setBackgroundResource(R.drawable.bg_prize_5_foot);
                _txtReward.setTextColor(getResources().getColor(R.color.brownish_grey));
                _txtCount.setTextColor(getResources().getColor(R.color.brownish_grey));
                break;
        }
    }

    public void setData(JSONObject json) {

        String msg = String.format("투표권\n%d장\n포인트\n%dP", JSONUtil.getInteger(json, "vote_cnt"),JSONUtil.getInteger(json, "point"));

        _txtReward.setText(msg);
        _txtCount.setText(JSONUtil.getInteger(json, "cnt") + "장");

    }
}
