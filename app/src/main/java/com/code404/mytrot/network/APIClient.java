package com.code404.mytrot.network;

import com.code404.mytrot.common.Constants;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class APIClient {
    private static Retrofit retrofit = null;

    private static int _member_no = -1;

    private static int _time_out = 10 * 1000;

    public static void setMemberNo(int member_no) {
        _member_no = member_no;
    }

    public static Retrofit getClient() {
        if (!Constants.IS_REAL_SEVER) {
            return getClient(Constants.SERVER_URL_DEBUG);
        } else {
            return getClient(Constants.SERVER_URL);
        }
    }


    public static Retrofit getClient_NStation() {
        return getClient("https://api.nstation.co.kr/");
    }


    public static Retrofit getClient(String serverUrl) {

        OkHttpClient client;
        if (Constants.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            client = new OkHttpClient.Builder()
                    .addInterceptor(interceptor)
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request.Builder builder = chain.request().newBuilder();
                            builder.header("api-token", makeApiToken());
                            return chain.proceed(builder.build());
                        }
                    })
                    .readTimeout(_time_out, TimeUnit.SECONDS)
                    .connectTimeout(_time_out, TimeUnit.SECONDS)
                    .build();
        } else {
            client = new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request.Builder builder = chain.request().newBuilder();
                            builder.header("api-token", makeApiToken());
                            return chain.proceed(builder.build());
                        }
                    })
                    .readTimeout(_time_out, TimeUnit.SECONDS)
                    .connectTimeout(_time_out, TimeUnit.SECONDS)
                    .build();
        }

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        retrofit = new Retrofit.Builder()
                .baseUrl(serverUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();

        return retrofit;
    }


    private static String makeApiToken() {

        String api_key = "mytrot!key&2021*new";

        long seconds = System.currentTimeMillis() / 1000;

        seconds = seconds - seconds % 100;

        String data = api_key + "_" + _member_no + "_" + seconds;

        String hash = bin2hex(getHash(data));

        LogUtil.d("makeApiToken, hash : " + data + " ----- " + hash);

        return hash;
    }

    private static byte[] getHash(String password) {
        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e1) {

        }
        if (digest != null) {
            digest.reset();
            return digest.digest(password.getBytes());
        } else {
            return null;
        }


    }

    private static String bin2hex(byte[] data) {
        if (data == null) {
            return "";
        } else {
            return String.format("%0" + (data.length * 2) + "X", new BigInteger(1, data));
        }

    }
}
