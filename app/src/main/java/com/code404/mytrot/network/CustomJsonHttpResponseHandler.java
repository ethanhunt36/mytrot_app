package com.code404.mytrot.network;

import android.content.Context;

import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomJsonHttpResponseHandler implements Callback<JsonObject> {
    private Context cx;
    private String api;

    public CustomJsonHttpResponseHandler(Context context, String api) {
        this.cx = context;
        this.api = api;
    }



    public void onResponse(int resultCode, String resultMessage, JSONObject json)  {


    }

    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
        try {
            if(response != null && response.body() != null) {
                JSONObject json = new JSONObject(response.body().toString());

                int resultCode = JSONUtil.getInteger(json, "err");
                String resultMessage = JSONUtil.getString(json, "msg");

                if(!FormatUtil.isNullorEmpty(resultMessage)){
                    resultMessage = resultMessage.replace("\\n", "\n");
                }

                onResponse(resultCode, resultMessage, json);
            } else {
                onResponse(-2000, "API", new JSONObject());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<JsonObject> call, Throwable t) {

    }
}
