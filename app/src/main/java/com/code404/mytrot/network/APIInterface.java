package com.code404.mytrot.network;

import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;


public interface APIInterface {


//    @Multipart
//    @Headers({"X-Naver-Client-Id: UjLOkv0oHa8QXmXfIsi5", "X-Naver-Client-Secret:9EcJ2B78JV"})
//    @POST("/v1/vision/face")
//    Call<JsonObject> naverFaceDetect(
//            @Part MultipartBody.Part fileBody
//    );
//
//    @Multipart
//    @Headers("Authorization: KakaoAK 294b15551f734e5bb27915673c6998b6")
//    @POST("/v1/vision/face/detect")
//    Call<JsonObject> kakaoFaceDetect(
//            @Part MultipartBody.Part fileBody
//    );

    // 버전 체크
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=common.check_app_version")
    Call<JsonObject> common_check_app_version(
            @Field("member_no") String member_no,
            @Field("device_type_id") String device_type_id,
            @Field("app_version") String app_version,
            @Field("device_id") String device_id
    );


//    // 파일 업로드
//    @Multipart
//    @POST("/_api/index.php?ssid=common.upload")
//    Call<JsonObject> imageUpload(
//            @Part("ssid") RequestBody ssid
//            , @Part MultipartBody.Part file
//    );

//    // 프로필 파일 업로드
//    @Multipart
//    @POST("/_api/index.php?ssid=Member.set_pic")
//    Call<JsonObject> member_set_pic(
//            @Part("ssid") RequestBody ssid
//            , @Part("member_no") RequestBody member_no
//            , @Part("state[]") RequestBody state01
//            , @Part("state[]") RequestBody state02
//            , @Part("state[]") RequestBody state03
//            , @Part("state[]") RequestBody state04
//            , @Part MultipartBody.Part file01
//            , @Part MultipartBody.Part file02
//            , @Part MultipartBody.Part file03
//            , @Part MultipartBody.Part file04
//
//    );


    // 회원 정보 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.get_by_no")
    Call<JsonObject> member_get_by_no(
            @Field("member_no") String member_no
    );

    // 이메일로 회원 정보 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.get_by_email")
    Call<JsonObject> member_get_by_email(
            @Field("email") String email
            , @Field("device_id") String device_id
    );

    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.get_by_sns_v2")
    Call<JsonObject> member_get_by_sns_v2(
            @Field("sns_id") String sns_id
            , @Field("login_type") String login_type
            , @Field("device_id") String device_id
    );

    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.set_sns")
    Call<JsonObject> member_set_sns(
            @Field("member_no") String member_no
            , @Field("login_type") String login_type
            , @Field("sns_id") String sns_id
    );


    // 회원 가입
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=Member.add")
    Call<JsonObject> member_add(
            @Field("email") String email,
            @Field("pw") String pw,
            @Field("login_type") String login_type,
            @Field("sns_id") String sns_id,
            @Field("device_type_id") String device_type_id,
            @Field("device_id") String device_id,
            @Field("app_version") String app_version,
            @Field("registration_key") String registration_key
    );


    // 회원 로그인
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=Member.login_v2")
    Call<JsonObject> member_login_v2(
            @Field("email") String email,
            @Field("pw") String pw,
            @Field("login_type") String login_type,
            @Field("sns_id") String sns_id,
            @Field("device_id") String device_id
    );


    // 회원 자동 로그인 기록 남기기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=Member.login_after")
    Call<JsonObject> member_login_after(
            @Field("member_no") String member_no,
            @Field("device_type_id") String device_type_id,
            @Field("device_id") String device_id,
            @Field("app_version") String app_version,
            @Field("registration_key") String registration_key
    );


    // 상품 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=product.get_all")
    Call<JsonObject> product_get_all(
            @Field("member_no") String member_no
    );


    // 상품 주문
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=pay.order_add")
    Call<JsonObject> pay_order_add(
            @Field("member_no") String member_no,
            @Field("product_no") int product_no,
            @Field("price") int price
    );


    // 상품 주문 -> 결제 완료
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=pay.sales_add")
    Call<JsonObject> pay_sales_add(
            @Field("member_no") String member_no
            , @Field("order_no") int order_no
            , @Field("price") int price
            , @Field("pay_type") String pay_type        // H:하트로결제, A:스토어,I:아이폰
            , @Field("sku") String sku
            , @Field("order_id") String order_id
            , @Field("token") String token
    );


//    // 내 결제 내역 조회
//    @FormUrlEncoded
//    @POST("/_api/index.php?ssid=pay.sales_get_all")
//    Call<JsonObject> pay_sales_get_all(
//            @Field("member_no") String member_no
//            , @Field("sku") String sku
//    );


//    // 회원 - 알림 설정 정보 조회
//    @FormUrlEncoded
//    @POST("/_api/index.php?ssid=memberAlram.get")
//    Call<JsonObject> memberAlram_get(
//            @Field("member_no") String member_no
//    );

//    // 회원 - 알림 설정 정보 저장
//    @FormUrlEncoded
//    @POST("/_api/index.php?ssid=memberAlram.set")
//    Call<JsonObject> memberAlram_set(
//            @Field("member_no") String member_no
//            , @Field("match_yn") String match_yn
//            , @Field("request_yn") String request_yn
//            , @Field("msg_yn") String msg_yn
//            , @Field("profile_yn") String profile_yn
//
//    );


//    // 회원 정보 수정
//    @FormUrlEncoded
//    @POST("/_api/index.php?ssid=Member.mod")
//    Call<JsonObject> member_mod(
//            @Field("member_no") String member_no
//            , @Field("nick") String nick
//            , @Field("intro") String intro
//            , @Field("area_cd") int area_cd
//            , @Field("job") String job
//            , @Field("height") String height
//            , @Field("weight") String weight
//            , @Field("body_code") int body_code
//    );


    // 공지/FAQ 리스트
    @FormUrlEncoded
    @POST("/_api/index.php")
    Call<JsonObject> notice_listing(
            @Field("ssid") String ssid
            , @Field("member_no") String member_no
            , @Field("category") int category
            , @Field("last_no") int last_no
            , @Field("record") int record
            , @Field("is_html") String is_html
    );

    // 공지/FAQ 읽음 처리
    @FormUrlEncoded
    @POST("/_api/index.php")
    Call<JsonObject> notice_read(
            @Field("ssid") String ssid
            , @Field("member_no") String member_no
            , @Field("no") int no
    );


    ////////////////////////////////////////////////////////////

    // 가수 랭킹
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.ranking1")
    Call<JsonObject> artist_ranking(
            @Field("member_no") String member_no
            , @Field("record") int record
            , @Field("last_no") int last_no
            , @Field("sort") String sort
            , @Field("gender") String gender
    );

    // 가수 투표 후보 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.vote_listing")
    Call<JsonObject> artist_vote_listing(
            @Field("member_no") String member_no
            , @Field("record") int record
            , @Field("last_no") int last_no
            , @Field("sort") String sort
            , @Field("gender") String gender
            , @Field("banner_yn") String banner_yn

            , @Field("srch_type") String srch_type
            , @Field("srch_text") String srch_text
    );

//    // 가수 리스트
//    @FormUrlEncoded
//    @POST("/_api/index.php?ssid=artist.listing")
//    Call<JsonObject> artist_listing(
//            @Field("member_no") String member_no
//            , @Field("record") int record
//            , @Field("last_no") int last_no
//            , @Field("sort") String sort
//    );

    // 영상 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistVod.listing")
    Call<JsonObject> artistvod_listing(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("keyword") String keyword
            , @Field("sort") String sort
            , @Field("record") int record
            , @Field("last_no") String last_no
            , @Field("srch_artist_name") String srch_artist_name
            , @Field("srch_vod_name") String srch_vod_name
    );


    // 영상 즐겨찾기 추가
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=MemberBookmark.set")
    Call<JsonObject> memberBookmark_set(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("vod_no") int record
    );

    // 영상 즐겨찾기 삭제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=MemberBookmark.del")
    Call<JsonObject> memberBookmark_del(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("vod_no") int record
    );

    // 영상재생시작
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistVod.play_start")
    Call<JsonObject> artistvod_play_start(
            @Field("member_no") String member_no
            , @Field("artist_vod_no") int artist_vod_no
    );

    // 영상재생완료
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistVod.play_end")
    Call<JsonObject> artistvod_play_end(
            @Field("member_no") String member_no
            , @Field("artist_vod_no") int artist_vod_no
    );


    // 즐찾 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=MemberBookmark.get_all")
    Call<JsonObject> memberBookmark_get_all(
            @Field("member_no") String member_no
            , @Field("gb") String gb
            , @Field("artist_no") int artist_no
            , @Field("record") int record
            , @Field("last_no") int last_no

    );


    // vod 등록 요청
    @Multipart
    @POST("/_api/index.php?ssid=shuttle.put")
    Call<JsonObject> shuttle_put(
            @Part("ssid") RequestBody ssid
            , @Part("member_no") RequestBody member_no
            , @Part("category") RequestBody category
            , @Part("cont") RequestBody cont
            , @Part MultipartBody.Part file01
            , @Part MultipartBody.Part file02
            , @Part MultipartBody.Part file03
    );

    // 투표하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.do_vote")
    Call<JsonObject> vote_do_vote(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("vote_cnt") int vote_cnt
    );

    // 투표권 정보 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.my_vote_ticket_info")
    Call<JsonObject> vote_my_vote_ticket_info(
            @Field("member_no") String member_no
    );

    // 투표권 지급 받기 (최초)
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.put_ticket_init")
    Call<JsonObject> vote_put_ticket_init(
            @Field("member_no") String member_no
    );

    // 투표권 지급 (광고)
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.put_ticket_ad")
    Call<JsonObject> vote_put_ticket_ad(
            @Field("member_no") String member_no
            , @Field("amount") int amount
            , @Field("ad_type") String ad_type
    );


    // 응원글 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.listing")
    Call<JsonObject> board_listing(
            @Field("member_no") String member_no
            , @Field("sort") String sort
            , @Field("record") int record
            , @Field("last_no") String last_no
            , @Field("category") int category
            , @Field("srch_artist_no") String srch_artist_no
    );

    // 응원글 리스트 - 별방
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.listing")
    Call<JsonObject> boardStar_listing(
            @Field("member_no") String member_no
            , @Field("board_id") String board_id
            , @Field("category") int category
            , @Field("record") int record
            , @Field("last_no") String last_no
    );

    // 응원글 통계
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.stat")
    Call<JsonObject> board_artist_stat(
            @Field("member_no") String member_no
            , @Field("record") int record
    );


    // 응원글 작성
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.ins")
    Call<JsonObject> board_ins(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("nick") String name
            , @Field("cont") String cont
    );


    // 갤러리 Multipart
    @Multipart
    @POST("/_api/index.php?ssid=board.ins")
    Call<JsonObject> board_ins(
            @Part("ssid") RequestBody ssid
            , @Part("member_no") RequestBody member_no
            , @Part("artist_no") RequestBody artist_no
            , @Part("category") RequestBody category
            , @Part("nick") RequestBody nick
            , @Part("cont") RequestBody cont
            , @Part MultipartBody.Part file01
            , @Part("board_id") RequestBody board_id
    );

    // 홍보인증 Multipart
    @Multipart
    @POST("/_api/index.php?ssid=boardPromo.ins")
    Call<JsonObject> boardPromo_ins(
            @Part("ssid") RequestBody ssid
            , @Part("member_no") RequestBody member_no
            , @Part("nick") RequestBody nick
            , @Part("title") RequestBody title
            , @Part("title_v2") RequestBody title_v2
            , @Part("cont") RequestBody cont

            , @Part("link") RequestBody link
            , @Part("sns_no") RequestBody sns_no

            , @Part MultipartBody.Part file01
            , @Part MultipartBody.Part file02
            , @Part MultipartBody.Part file03
    );


    // 응원글 좋아요
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.inc_like")
    Call<JsonObject> board_inc_like(
            @Field("member_no") String member_no
            , @Field("board_no") int board_no
    );

    // 별방글 좋아요
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.inc_like")
    Call<JsonObject> boardStar_inc_like(
            @Field("member_no") String member_no
            , @Field("board_no") int board_no
            , @Field("board_id") String board_id
    );

    // 프로모션 좋아요
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardPromo.inc_like")
    Call<JsonObject> boardPromo_inc_like(
            @Field("member_no") String member_no
            , @Field("board_no") int board_no
    );

    // 응원글 싫어요
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.inc_hate")
    Call<JsonObject> board_inc_hate(
            @Field("member_no") String member_no
            , @Field("board_no") int board_no
    );


    // 클릭 로그
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=logClick.add_log")
    Call<JsonObject> logClick_add_log(
            @Field("member_no") String member_no
            , @Field("action_code") String action_code
    );

    // 클릭 로그 회수 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=logClick.my_action_info")
    Call<JsonObject> logClick_my_action_info(
            @Field("member_no") String member_no
            , @Field("action_code") String action_code
    );


    // 게시판 삭제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.del")
    Call<JsonObject> board_del(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no
    );

    // 별방 삭제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.del")
    Call<JsonObject> boardStar_del(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no,
            @Field("board_id") String board_id
    );


    // 갤러리 상세정보
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.view")
    Call<JsonObject> board_view(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no,
            @Field("last_no") int last_no,
            @Field("record") int record,
            @Field("sort") String sort
    );

    // 별방 상세정보
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.view")
    Call<JsonObject> boardStar_view(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no,
            @Field("board_id") String board_id,
            @Field("last_no") int last_no,
            @Field("record") int record,
            @Field("sort") String sort
    );


    // 댓글 입력
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.cmt_ins")
    Call<JsonObject> board_cmt_ins(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no,
            @Field("cont") String cont
    );

    // 별방 댓글 입력
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.cmt_ins")
    Call<JsonObject> boardStar_cmt_ins(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no,
            @Field("comment_no") int comment_no,
            @Field("board_id") String board_id,
            @Field("cont") String cont
    );

    // 댓글 삭제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.cmt_del")
    Call<JsonObject> board_cmt_del(
            @Field("member_no") String member_no,
            @Field("comment_no") int comment_no
    );

    // 별방 댓글 삭제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.cmt_del")
    Call<JsonObject> boardStar_cmt_del(
            @Field("member_no") String member_no,
            @Field("comment_no") int comment_no,
            @Field("board_id") String board_id
    );


    // 게시글 다운로드 +1
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=board.inc_download")
    Call<JsonObject> board_inc_download(
            @Field("member_no") String member_no
            , @Field("board_no") int board_no
    );

    // 별방 다운로드 +1
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.inc_download")
    Call<JsonObject> boardStar_inc_download(
            @Field("member_no") String member_no
            , @Field("board_no") int board_no
            , @Field("board_id") String board_id
    );

    // 광고 시청 후, 투표권 & 포인트 지금
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.put_ticket_ad_point_v2")
    Call<JsonObject> vote_put_ticket_ad_point(
            @Field("member_no") String member_no
            , @Field("ad_type") String ad_type
            , @Field("is_have_booster") String is_have_booster
            , @Field("device_random_id") String device_random_id
            , @Field("ad_view_count") int ad_view_count
            , @Field("newspic_no") int newspic_no

            , @Field("charge_mode") String charge_mode
    );


    // 기부하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=donate.donate")
    Call<JsonObject> donate_donate(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("point") long point
    );

    // 포인트 적립/사용 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=MemberPointHistory.listing")
    Call<JsonObject> memberPointHistory_listing(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
    );

    // 투표권 적립/사용 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.get_all_history")
    Call<JsonObject> vote_get_all_history(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
    );

    // 하트 적립/사용 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=MemberHeartHistory.listing")
    Call<JsonObject> memberHeartHistory_listing(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
    );

    // 레벨 순위 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.get_level_top_n")
    Call<JsonObject> member_get_level_top_n(
            @Field("member_no") String member_no
    );


    // 출석하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=attendance.attendance_v2")
    Call<JsonObject> attendance_attendance(
            @Field("member_no") String member_no
    );

    // 내 출석 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=attendance.listing")
    Call<JsonObject> attendance_listing(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
    );

    // 출석 랭킹
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=attendance.ranking")
    Call<JsonObject> attendance_ranking(
            @Field("member_no") String member_no
            , @Field("record") int record
            , @Field("last_no") String last_no
    );

    // 닉네임 변경
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.set_update_nick_v2")
    Call<JsonObject> member_set_nick(
            @Field("member_no") String member_no
            , @Field("nick") String nick
    );

    // 인증번호 요청
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.send_auth_pin")
    Call<JsonObject> member_send_auth_pin(
            @Field("phone") String phone
    );

    // 인증번호 확인
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.verify_auth_pin")
    Call<JsonObject> member_verify_auth_pin(
            @Field("phone") String phone
            , @Field("pin") String pin
            , @Field("device_id") String device_id
    );

    // 계정 선택
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.choice")
    Call<JsonObject> member_choice(
            @Field("member_no") String member_no
            , @Field("phone") String phone
            , @Field("device_id") String device_id
            , @Field("app_version") String app_version
            , @Field("registration_key") String registration_key
    );


    // 신고 - 회원/밤스타
    @Multipart
    @POST("/_api/index.php?ssid=accuse.ins")
    Call<JsonObject> accuse_ins(
            @Part("ssid") RequestBody ssid
            , @Part("member_no") RequestBody member_no
            , @Part("target_member_no") RequestBody target_member_no
            , @Part("accuse_type") RequestBody accuse_type                  // M:회원신고, B:게시글신고,
            , @Part("accuse_reason") RequestBody accuse_reason              // 신고 사유, 80:사진/글 불량, 81:사진/글 광고, 99:기타

            , @Part("cont") RequestBody cont
            , @Part("board_no") RequestBody board_no
            , @Part MultipartBody.Part file01
            , @Part MultipartBody.Part file02
            , @Part MultipartBody.Part file03

            , @Part("board_id") RequestBody board_id
    );

    // 신고 - 회원/밤스타
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=accuse.ins")
    Call<JsonObject> accuse_ins_direct(
            @Field("member_no") String member_no
            , @Field("target_member_no") String target_member_no
            , @Field("accuse_type") String accuse_type                  // M:회원신고, B:게시글신고,
            , @Field("accuse_reason") String accuse_reason              // 신고 사유, 80:사진/글 불량, 81:사진/글 광고, 99:기타

            , @Field("cont") String cont
            , @Field("board_no") int board_no
            , @Field("board_id") String board_id
            , @Field("comment_no") int comment_no

    );


    // 투표권 적립 / 기부 랭킹
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.ranking_get_all")
    Call<JsonObject> vote_ranking_get_all(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
            , @Field("term") String term
    );

    // 포인트 적립 / 기부 랭킹
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=point.ranking_get_all")
    Call<JsonObject> point_ranking_get_all(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
            , @Field("term") String term
    );


    // 하트 적립 / 기부 랭킹
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=heart.ranking_get_all")
    Call<JsonObject> heart_ranking_get_all(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
            , @Field("term") String term
    );

    // 내 보유 아이템 리스트 가져오기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberItem.get_all")
    Call<JsonObject> memberItem_get_all(
            @Field("member_no") String member_no
            , @Field("product_type") String product_type    // 기간제:P,하트:H
    );


    // 홍보인증리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardPromo.listing")
    Call<JsonObject> boardPromo_listing(
            @Field("member_no") String member_no
            , @Field("last_no") int last_no
            , @Field("record") int record
    );

    // 알림 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=alram.listing")
    Call<JsonObject> alram_listing(
            @Field("member_no") String member_no
            , @Field("last_no") int last_no
            , @Field("record") int record
    );

    // 알림 삭제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=alram.delete")
    Call<JsonObject> alram_delete(
            @Field("member_no") String member_no
    );


    // 최애 가수 설정
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.update_favorite_artist_v2")
    Call<JsonObject> member_update_favorite_artist(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
    );


    // 기부 진행중 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=donate.ing_list")
    Call<JsonObject> donate_ing_list(
            @Field("member_no") String member_no
            , @Field("record") int record
            , @Field("last_no") int last_no
    );

    // 기부 완료 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=donate.done_list")
    Call<JsonObject> donate_done_list(
            @Field("member_no") String member_no
            , @Field("record") int record
            , @Field("last_no") int last_no
    );

    // 기부 완료 통계 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=donate.done_st_list")
    Call<JsonObject> donate_done_st_list(
            @Field("member_no") String member_no
    );


    // 기부 참여 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=donate.member_list")
    Call<JsonObject> donate_member_list(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("donate_goal_no") int donate_goal_no
            , @Field("record") int record
            , @Field("last_no") int last_no
    );


    // 오늘의 미션 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.get")
    Call<JsonObject> memberMission_get(
            @Field("member_no") String member_no
    );


    // 오늘의 미션 보상 받기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.bonus")
    Call<JsonObject> memberMission_bonus(
            @Field("member_no") String member_no
    );

    // 포동 미션 1회 체크
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.check_mission_podong")
    Call<JsonObject> memberMission_check_mission_podong(
            @Field("member_no") String member_no
    );

    // 동영상 광고 시청 미션 달성
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.check_mission_ad_movie")
    Call<JsonObject> memberMission_check_mission_ad_movie(
            @Field("member_no") String member_no
    );

    // 활동 인증서 발급
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.check_mission_get_cert")
    Call<JsonObject> memberMission_check_mission_get_cert(
            @Field("member_no") String member_no
    );

    // 마이트롯 블로그 방문
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.check_mission_view_blog")
    Call<JsonObject> memberMission_check_mission_view_blog(
            @Field("member_no") String member_no
    );

    // 마이트롯 인스타그램 방문
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.check_mission_view_instar")
    Call<JsonObject> memberMission_check_mission_view_instar(
            @Field("member_no") String member_no
    );


    // 마이트롯 유튜브 방문
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberMission.check_mission_view_youtube")
    Call<JsonObject> memberMission_check_mission_view_youtube(
            @Field("member_no") String member_no
    );


    // 오늘의 행운 번호 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=luckyNumber.get_today_number_list")
    Call<JsonObject> luckyNumber_get_today_number_list(
            @Field("member_no") String member_no
    );


    // 인증서 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.get_cert_info")
    Call<JsonObject> member_get_cert_info(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("period") String period
    );

    // 포인트로 투표권 교환
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=point.buy_vote_ticket")
    Call<JsonObject> point_buy_vote_ticket(
            @Field("member_no") String member_no
            , @Field("point_amount") int point_amount
    );

    // 포인트로 하트 교환
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=point.buy_heart")
    Call<JsonObject> point_buy_heart(
            @Field("member_no") String member_no
            , @Field("point_amount") int point_amount
    );

    // 하트로 투표권 교환
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=heart.buy_vote_ticket")
    Call<JsonObject> heart_buy_vote_ticket(
            @Field("member_no") String member_no
            , @Field("heart_amount") int heart_amount
    );

    // 쩜으로 포인트 교환
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=point.buy_point_by_blue")
    Call<JsonObject> buy_point_by_blue(
            @Field("member_no") String member_no
            , @Field("blue_amount") int blue_amount
    );


    // 나의 추천인코드 가져오기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.get_rcmd_code")
    Call<JsonObject> member_get_rcmd_code(
            @Field("member_no") String member_no
    );


    // 나의 추천인코드 입력
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.put_rcmd_code")
    Call<JsonObject> member_put_rcmd_code(
            @Field("member_no") String member_no
            , @Field("rcmd_code") String rcmd_code
    );


    // 뽑기 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=ppobkki.get_list")
    Call<JsonObject> ppobkki_get_list(
            @Field("member_no") String member_no
            , @Field("newspic_yn") String newspic_yn
    );

    // 뽑기 처리
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=ppobkki.play_v2")
    Call<JsonObject> ppobkki_play_v2(
            @Field("member_no") String member_no
            , @Field("no") int no
    );


    // 타임치트키 사용
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=ppobkki.run_time_cheat")
    Call<JsonObject> ppobkki_run_time_cheat(
            @Field("member_no") String member_no
    );


    // 특수광고 뽑기 준비
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=ppobkkiAd.get_ready")
    Call<JsonObject> ppobkkiAd_get_ready(
            @Field("member_no") String member_no
    );

    // 특수광고 뽑기 처리
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=ppobkkiAd.play")
    Call<JsonObject> ppobkkiAd_play(
            @Field("member_no") String member_no
            , @Field("no") int no
    );


    // vs 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vs.listing")
    Call<JsonObject> vs_listing(
            @Field("member_no") String member_no
            , @Field("record") int record
            , @Field("last_no") int last_no
            , @Field("sort") String sort
    );

    // vs 정보 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vs.view")
    Call<JsonObject> vs_view(
            @Field("member_no") String member_no
            , @Field("board_no") int board_no
            , @Field("record") int record
    );


    // 회원 - 알림 설정 정보 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberAlarm.get")
    Call<JsonObject> memberAlram_get(
            @Field("member_no") String member_no
    );

    // 회원 - 알림 설정 정보 저장
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberAlarm.set_all")
    Call<JsonObject> memberAlram_set_all(
            @Field("member_no") String member_no
            , @Field("favorite_yn") String favorite_yn
            , @Field("community_yn") String community_yn
            , @Field("event_yn") String event_yn
            , @Field("admin_yn") String admin_yn
            , @Field("etc_yn") String etc_yn
    );


    // 작성자의 글 모두 차단하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=memberBlock.block_by_board")
    Call<JsonObject> memberBlock_block_by_board(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no,
            @Field("board_id") String board_id
    );


    // 별방 메인 데이타 가져오기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=boardStar.get_starroom_main_data")
    Call<JsonObject> boardStar_get_starroom_main_data(
            @Field("member_no") String member_no,
            @Field("artist_no") int artist_no
    );

    // 별방 메인 - 사진 좋아요
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.artist_pic_like")
    Call<JsonObject> artist_artist_pic_like(
            @Field("member_no") String member_no,
            @Field("artist_pic_no") int artist_pic_no
    );


    // 별방 메인 - 사진 좋아요 취소
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.artist_pic_unlike")
    Call<JsonObject> artist_artist_pic_unlike(
            @Field("member_no") String member_no,
            @Field("artist_pic_no") int artist_pic_no
    );


    // 별방 메인 - 프로필 후보 사진 리스트 (인기순)
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.artist_pic_list_like")
    Call<JsonObject> artist_artist_pic_list_like(
            @Field("member_no") String member_no,
            @Field("artist_no") int artist_pic_no
    );

    // 별방 메인 - 프로필 후보 사진 리스트 (최신순)
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.artist_pic_list_recent")
    Call<JsonObject> artist_artist_pic_list_recent(
            @Field("member_no") String member_no,
            @Field("artist_no") int artist_pic_no
    );


    // 별방 메인 - 키워드 리스트 (최신순)
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistKeyword.list_recent")
    Call<JsonObject> artistKeyword_list_recent(
            @Field("member_no") String member_no,
            @Field("artist_no") int artist_no,
            @Field("page") int page,
            @Field("record") int record
    );

    // 별방 메인 - 키워드 리스트 (인기순)
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistKeyword.list_pop")
    Call<JsonObject> artistKeyword_list_pop(
            @Field("member_no") String member_no,
            @Field("artist_no") int artist_no,
            @Field("page") int page,
            @Field("record") int record
    );

    // 별방 메인 - 키워드 좋아요
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistKeyword.like")
    Call<JsonObject> artistKeyword_like(
            @Field("member_no") String member_no,
            @Field("keyword_no") int keyword_no
    );


    // 별방 메인 - 키워드 좋아요 취소
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistKeyword.unlike")
    Call<JsonObject> artistKeyword_unlike(
            @Field("member_no") String member_no,
            @Field("keyword_no") int keyword_no
    );

    // 별방 종류별 랭킹
    @FormUrlEncoded
    @POST("/_api/index.php")
    Call<JsonObject> starroom_rank_type(
            @Field("ssid") String ssid,
            @Field("member_no") String member_no,
            @Field("category") String category,
            @Field("term") String term,
            @Field("gubun") String gubun,
            @Field("record") int record,
            @Field("artist_no") int artist_no,
            @Field("board_id") String board_id
    );

    // 탈퇴하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=member.out")
    Call<JsonObject> member_out(
            @Field("member_no") String member_no
    );

    // vs 투표하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vs.inc_pick")
    Call<JsonObject> vs_inc_pick(
            @Field("member_no") String member_no,
            @Field("board_no") int board_no,
            @Field("item_no") int item_no
    );


    // 퍼즐 랭킹
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=SlidePuzzle.ranking")
    Call<JsonObject> slidePuzzle_ranking(
            @Field("member_no") String member_no
            , @Field("lv") int lv
    );

    // 개인 기록
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=SlidePuzzle.rank1")
    Call<JsonObject> slidePuzzle_ranking1(
            @Field("member_no") String member_no
            , @Field("lv") int lv
            , @Field("page") int page
            , @Field("pagenum") int pagenum
    );


    // 프로필 후보 사진 등록
    @Multipart
    @POST("/_api/index.php?ssid=artist.artist_pic_ins")
    Call<JsonObject> artist_artist_pic_ins(
            @Part("ssid") RequestBody ssid
            , @Part("member_no") RequestBody member_no
            , @Part("artist_no") RequestBody artist_no
            , @Part MultipartBody.Part file01
    );

    // 프로필 후보 사진 삭제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artist.artist_pic_del")
    Call<JsonObject> artist_artist_pic_del(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("artist_pic_no") int artist_pic_no
    );

    // 가수 대표 키워드 등록
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistKeyword.add")
    Call<JsonObject> artistKeyword_add(
            @Field("member_no") String member_no
            , @Field("artist_no") int artist_no
            , @Field("keyword") String keyword
    );


    // 투표권 적립/기부에 대한 주간/월간 순위
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=vote.ranking_get_artist")
    Call<JsonObject> vote_ranking_get_artist(
            @Field("member_no") String member_no
            , @Field("gubun") String gubun
            , @Field("record") int record
            , @Field("last_no") int last_no
            , @Field("term") String term
            , @Field("ym") String ym
            , @Field("ymd") String ymd
    );


    // 랜덤으로 뉴스 기사 불러오기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=newspic.get_newspic_info")
    Call<JsonObject> newspic_get_newspic_info(
            @Field("member_no") String member_no
    );

    // 로딩이 안되는 기사 삭제하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=newspic.report")
    Call<JsonObject> newspic_report(
            @Field("member_no") String member_no
            , @Field("newspic_no") int newspic_no
    );


    // 광고펀딩 리스트 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=eventFund.get_list")
    Call<JsonObject> eventFund_get_list(
            @Field("member_no") String member_no
            , @Field("state") String state
            , @Field("record") int record
            , @Field("last_no") int last_no
    );

    // 광고펀딩 상세 정보
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=eventFund.get")
    Call<JsonObject> eventFund_get(
            @Field("member_no") String member_no
            , @Field("no") int no
    );


    // 광고펀딩 하트 사용하기
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=eventFund.fund")
    Call<JsonObject> eventFund_fund(
            @Field("member_no") String member_no
            , @Field("no") int no
            , @Field("heart") long heart
    );


    // 하트 뽑기권 사용
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=heart.open_heart_ticket_v2")
    Call<JsonObject> heart_open_heart_ticket(
            @Field("member_no") String member_no
    );

//    // 배너 리스트 조회
//    @FormUrlEncoded
//    @POST("/_api/index.php?ssid=banner.listing")
//    Call<JsonObject> banner_listing(
//            @Field("member_no") String member_no
//            , @Field("dp_code") String dp_code
//    );


    // 배너 클릭 처리
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=banner.click")
    Call<JsonObject> banner_click(
            @Field("member_no") String member_no
            , @Field("no") int no
    );


    // PointClick 오퍼웰 데이터 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=pointClick.get_list")
    Call<JsonObject> pointClick_get_list(
            @Field("member_no") String member_no
            , @Field("device_ifa") String device_ifa
            , @Field("keyword") String keyword
            , @Field("ad_type") String ad_type
            , @Field("seq") String seq
    );

    // PointClick 오퍼웰 사용 가능 체크
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=pointClick.check")
    Call<JsonObject> pointClick_check(
            @Field("ad_key") String ad_key
            , @Field("member_no") String member_no
            , @Field("ad_profit") Double ad_profit
            , @Field("device_ifa") String device_ifa
            , @Field("auto_yn") String auto_yn
    );


    // 홍보 인증 커뮤니티 SNS 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=snsList.get_list")
    Call<JsonObject> snsList_get_list(
            @Field("member_no") String member_no
            , @Field("srch_text") String srch_text
            , @Field("sort") String sort
            , @Field("favorite_yn") String favorite_yn
            , @Field("last_no") int last_no
            , @Field("record") int record
    );


    // 홍보 인증 커뮤니티 SNS 등록
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=snsList.ins")
    Call<JsonObject> snsList_ins(
            @Field("member_no") String member_no
            , @Field("sns_name") String sns_name
            , @Field("sns_url") String sns_url
    );


    // 홍보 인증 커뮤니티 즐겨찾기 등록/해제
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=snsList.set_favorite")
    Call<JsonObject> snsList_set_favorite(
            @Field("member_no") String member_no
            , @Field("sns_no") int sns_no
            , @Field("favorite_yn") String favorite_yn      // Y , N
    );


    // 유튜브 업로드된 영상 리스트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=artistVod.get_youtube_list")
    Call<JsonObject> artistVod_get_youtube_list(
            @Field("member_no") String member_no
    );


    // 엔스테이션 적립 최대 포인트
    @FormUrlEncoded
    @POST("/v1/maxreward")
    Call<JsonObject> nstation_maxreward(
            @Field("mkey") int mkey
            , @Field("mckey") int mckey
            , @Field("userid") String userid
    );


    /*
    퀴즈 관련
     */
    // 퀴즈 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=zQuiz.get_info")
    Call<JsonObject> zQuiz_get_info(
            @Field("member_no") String member_no
    );


    // 퀴즈 조회
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=zQuiz.correct")
    Call<JsonObject> zQuiz_correct(
            @Field("member_no") String member_no
            , @Field("z_quiz_no") int z_quiz_no
            , @Field("answer") String answer
    );

    // 퀴즈 충전
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=zQuiz.charge_quiz")
    Call<JsonObject> zQuiz_charge_quiz(
            @Field("member_no") String member_no
    );

    // 퀴즈 힌트
    @FormUrlEncoded
    @POST("/_api/index.php?ssid=zQuiz.use_hint")
    Call<JsonObject> zQuiz_use_hint(
            @Field("member_no") String member_no
    );
}
