package com.code404.mytrot.frg.vote;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.code404.mytrot.R;
import com.code404.mytrot.adapter.MyFragmentPagerAdapter;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.more.AtvMoreChargeB;
import com.code404.mytrot.atv.star_room.AtvStarRoom;
import com.code404.mytrot.atv.star_room.AtvStarRoomMain;
import com.code404.mytrot.atv.star_room.AtvStarRoomVoteMonthRank;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.frg.banner.FrgBanner;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAction;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

@SuppressLint("ValidFragment")
public class FrgVote extends FrgBase implements GListView.IMakeView {

    private MyFragmentPagerAdapter _adapter = null;
    private ViewPager _pager = null;

    private GListView _list = null;

    private View _header = null;


    private View _base_vote_top = null;
    private TextView _txtTicket = null;
    private View _baseAd = null;
    private TextView _txtAdDesc01 = null;
    private TextView _txtAdDesc02 = null;
    private TextView _txtAdDesc03 = null;

    private View _base_vote_bottom = null;

    private Button _btnCheckFavorite = null;


    private TextView _txtSeq = null;
    private TextView _txtGender = null;

    private ImageView _btnMore = null;

    private int _amount_ticket_ad = 1;
    private double _point_amount = 1;

    private int _selectedIndex = 0;

    private int _last_no = -1;
    private String _sort = "vote_cnt";
    private String _gender = "";

    private String _last_month = "";

//    private AdFullManager _adFullManager = null;


    private Handler _handlerTimer;
    private int _timerInterval = 3000;

    // 현재 보유한 투표권 개수
    private int _vote_cnt = 0;

    private CountDownTimer _timer = null;
    private CountDownTimer _timerAd = null;

    private long _adClickTime = -1;


    private List<ImageView> _dot_list = new ArrayList<ImageView>();

    @SuppressLint("ValidFragment")
    public FrgVote() {

    }


    @Override
    protected void setView() {
        setView(R.layout.frg_vote);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_vote, null);
        _list.addHeaderView(_header);

        _base_vote_top = _header.findViewById(R.id.base_vote_top);
        _base_vote_bottom = _header.findViewById(R.id.base_vote_bottom);
        _txtTicket = _header.findViewById(R.id.txtTicket);
        _baseAd = _header.findViewById(R.id.baseAd);
        _txtAdDesc01 = _header.findViewById(R.id.txtAdDesc01);
        _txtAdDesc02 = _header.findViewById(R.id.txtAdDesc02);
        _txtAdDesc03 = _header.findViewById(R.id.txtAdDesc03);

        _btnCheckFavorite = _header.findViewById(R.id.btnCheckFavorite);
        _pager = _header.findViewById(R.id.pager);


        _txtSeq = _header.findViewById(R.id.txtSeq);
        _txtGender = _header.findViewById(R.id.txtGender);

        _dot_list = new ArrayList<ImageView>();

        _dot_list.add(_header.findViewById(R.id.dot_01));
        _dot_list.add(_header.findViewById(R.id.dot_02));
        _dot_list.add(_header.findViewById(R.id.dot_03));
        _dot_list.add(_header.findViewById(R.id.dot_04));
        _dot_list.add(_header.findViewById(R.id.dot_05));
        _dot_list.add(_header.findViewById(R.id.dot_06));
        _dot_list.add(_header.findViewById(R.id.dot_07));
        _dot_list.add(_header.findViewById(R.id.dot_08));
        _dot_list.add(_header.findViewById(R.id.dot_09));
        _dot_list.add(_header.findViewById(R.id.dot_10));

        _btnMore = (ImageView) findViewById(R.id.btnMore);
    }


    @Override
    protected void init() {

        _header.setVisibility(View.GONE);


        if (_adapter == null) {
            _adapter = new MyFragmentPagerAdapter(getChildFragmentManager());
        }

        _adClickTime = SPUtil.getInstance().getClickAdView(getContext());

        initBtn();

        LogUtil.d("_txt, init => initAd");

        _list.setViewMaker(R.layout.row_vote, this);

        boolean checked = SPUtil.getInstance().getCheckedFavoriteVote(getContext());

        _btnCheckFavorite.setSelected(checked);

        callApi_artist_vote_listing(true);

        setActiveAdTextDisplay(Constants.enum_ad.none, true);

        for (ImageView i : _dot_list) {
            i.setVisibility(View.GONE);
        }

//        _txtAdDesc03.setText(Html.fromHtml("동영상광고는 '<u><b><font color='red'>더보기 - 무료충전소</font></b></u>'에서 시청해주세요."));
        _txtAdDesc03.setText(Html.fromHtml("<u><b><font color='red'>더보기 - 무료충전소</font></b></u>에서 투표권,포인트,하트 적립받으세요."));

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _txtAdDesc03.setVisibility(View.GONE);
            _btnMore.setVisibility(View.GONE);
        }
    }


//    private void initTimer() {
//
//
//        if (_timerAd == null) {
//
//            _timerAd = new CountDownTimer((1000L) * 60 * 60 * 4, 1000L) {
//                @Override
//                public void onTick(long millisUntilFinished) {
//                    setActiveAdTextDisplay(Constants.enum_ad.none, true);
//
//                    JSONObject userJson = SPUtil.getInstance().getUserInfo(getContext());
//                    setMyVoteCountPoint(userJson);
//                }
//
//                @Override
//                public void onFinish() {
//                    LogUtil.d("onTick, onFinish");
//                }
//            };
//
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (_timerAd != null) {
//                        _timerAd.start();
//                    }
//                }
//            }, 100);
//        }
//
//    }


    private void initBtn() {
//        JSONObject json = SPUtil.getInstance().getBtnInfo(getContext());
//
//        String btn_week = JSONUtil.getString(json, "btn_week");
//        String btn_month = JSONUtil.getString(json, "btn_month");
//
//        String default_sort = JSONUtil.getString(json, "default_sort", "vote_cnt");
//
//
        JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
        int lv = JSONUtil.getInteger(jsonUser, "lv", 0);
        int availableBoardLevel = SPUtil.getInstance().getAvailableBoardLevel(getContext());
        LogUtil.d("_baseStarRoom, lv : " + lv + ", availableBoardLevel : " + availableBoardLevel);


        _txtAdDesc01.setText("동영상 광고");


        String default_sort = SPUtil.getInstance().getRankSeq(getContext());
        String default_gender = SPUtil.getInstance().getRankGender(getContext());

        setBtnTab(default_sort);
        setBtnTab_Gender(default_gender);
    }


    private long getAdGap() {
        long gap = _adClickTime < 1 ? 0 : (System.currentTimeMillis() - _adClickTime) / 1000;
        return gap;
    }


    private void setActiveAdTextDisplay(Constants.enum_ad adName, boolean isActive) {


        String txt = "";


        int adCount = 1;

        txt = FormatUtil.getAdDesc(getContext(), _amount_ticket_ad, _point_amount, isActive, adCount);

        long gap = getAdGap();
        long adViewGap = SPUtil.getInstance().getClickAdViewGap(getContext());

        String msg = "동영상 광고";

        //LogUtil.d("FrgVote, setActiveAdTextDisplay, adName : " + adName + ", isActive : " + isActive + ", adCount : " + adCount + ", gap : " + gap);

        if (SPUtil.getInstance().getAdViewLimitYn(getContext()).equals("Y") || SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
//            msg = "동영상 광고<font color='gray'>(" + (adViewGap / 60) + "분당 1회 가능)</font>";
//
//            if (adViewGap < 60) {
//                msg = "동영상 광고<font color='gray'>(" + adViewGap + "초당 1회 가능)</font>";
//            }

            if (Constants.DEBUG) msg += "(" + adCount + ")";
            if (gap > 0) {
                msg += (gap < adViewGap ? (", " + (adViewGap - gap) + "초 전") : "");
            }
        } else {
            if (Constants.DEBUG) msg += "(" + adCount + ")";
        }

        _txtAdDesc01.setText(Html.fromHtml(msg));

        if (!FormatUtil.isNullorEmpty(txt)) {
            _txtAdDesc02.setText(Html.fromHtml(txt));
        }
    }


    @Override
    public void refresh() {
        try {
            LogUtil.d("ssid : frgVote, refresh");
            if (_list != null) {
                _list.removeAll();
            }
            _last_no = -1;
            callApi_artist_vote_listing(true);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {

        _txtAdDesc03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvMoreChargeB.class);
                i.putExtra(Constants.EXTRAS_AD_TICKET, _amount_ticket_ad);
                i.putExtra(Constants.EXTRAS_AD_POINT, _point_amount);
                startActivity(i);
            }
        });


        _btnCheckFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _btnCheckFavorite.setSelected(!_btnCheckFavorite.isSelected());

                boolean checked = _btnCheckFavorite.isSelected();

                SPUtil.getInstance().setCheckedFavoriteVote(getContext(), checked);

                JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
                int artist_no = JSONUtil.getInteger(json, "artist_no");

                if (artist_no < 1) {
                    new Alert().showAlert(getContext(), getString(R.string.txt_set_artist));
                    return;
                }

                callApi_artist_vote_listing(true);
            }
        });


        _txtSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] items = new String[]{"주간", "월간", "누적"};
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        LogUtil.d("which : " + which);

                        if (which == 0) {
                            setBtnTab("vote_cnt_week");
                        } else if (which == 1) {
                            setBtnTab("vote_cnt_month");
                        } else if (which == 2) {
                            setBtnTab("vote_cnt");
                        }
                        callApi_artist_vote_listing(true);
                    }
                });
                alert.showSeletItem(getContext(), items);
            }
        });

        _txtGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] items = new String[]{"전체성별", "남자", "여자"};
                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        LogUtil.d("which : " + which);

                        if (which == 0) {
                            setBtnTab_Gender("");
                        } else if (which == 1) {
                            setBtnTab_Gender("M");
                        } else if (which == 2) {
                            setBtnTab_Gender("F");
                        }
                        callApi_artist_vote_listing(true);
                    }
                });
                alert.showSeletItem(getContext(), items);
            }
        });

        _baseAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent();
                i.setClass(getContext(), AtvMoreChargeB.class);
                i.putExtra(Constants.EXTRAS_AD_TICKET, _amount_ticket_ad);
                i.putExtra(Constants.EXTRAS_AD_POINT, _point_amount);
                startActivity(i);
            }
        });


        _pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int index) {
                _selectedIndex = index;

                LogUtil.d("onPageSelected : " + index);

                FrgBase frg = (FrgBase) _adapter.getItem(index);

                for (int i = 0; i < _dot_list.size(); i++) {
                    _dot_list.get(i).setImageResource(index == i ? R.drawable.btn_tradebar_over : R.drawable.btn_tradebar);
                }
            }


            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // LogUtil.d("onPageScrolled : " + arg0);
            }


            @Override
            public void onPageScrollStateChanged(int index) {
                // LogUtil.d("onPageScrollStateChanged : " + index);
            }
        });


        _btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                _btnMore.setVisibility(View.GONE);

                JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

                final int artist_no = JSONUtil.getInteger(json, "artist_no");
                final String artist_name = JSONUtil.getString(json, "artist_name");
                final String artist_board_id = JSONUtil.getString(json, "artist_board_id");


                AlertPop alertPop = new AlertPop();
                alertPop.showFloatingVote(getContext());
                alertPop.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {

                        _btnMore.setVisibility(View.VISIBLE);

                        if (which == Alert.BUTTON1 || which == Alert.BUTTON2) {
                            Intent i = new Intent();
                            i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
                            i.putExtra(Constants.EXTRAS_TYPE, "month");

                            if (which == Alert.BUTTON2) {
                                i.putExtra(Constants.EXTRAS_TYPE, "week");
                            }

                            i.setClass(getContext(), AtvStarRoomVoteMonthRank.class);
                            startActivity(i);
                        }
                        if (which == Alert.BUTTON3) {

                            if (artist_no < 1) {
                                Alert alert = new Alert();
                                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                                    @Override
                                    public void onClose(DialogInterface dialog, int which) {
                                        Intent intent = new Intent();
                                        intent.setClass(getContext(), AtvMore.class);
                                        startActivity(intent);
                                        getActivity().finishAffinity();
                                    }
                                });
                                alert.showAlert(getContext(), getString(R.string.txt_set_artist));
                                return;
                            }

                            Intent i = new Intent();
                            i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
                            i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
                            i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, artist_board_id);
                            i.setClass(getContext(), AtvStarRoomMain.class);
                            startActivity(i);
                        }
                    }
                });
            }
        });
    }


    private void setBtnTab(String sort) {
        _sort = sort;
        if (_sort.equals("vote_cnt_week")) {
            _txtSeq.setText("주간");
        } else if (_sort.equals("vote_cnt_month")) {
            _txtSeq.setText("월간");
        } else if (_sort.equals("vote_cnt")) {
            _txtSeq.setText("누적");
        }

        SPUtil.getInstance().setRankSeq(getContext(), sort);
    }


    private void setBtnTab_Gender(String gender) {
        _gender = gender;
        if (_gender.equals("")) {
            _txtGender.setText("전체성별");
        } else if (_gender.equals("M")) {
            _txtGender.setText("남자");
        } else if (_gender.equals("F")) {
            _txtGender.setText("여자");
        }
        SPUtil.getInstance().setRankGender(getContext(), gender);
    }


    @Override
    public void onPause() {
        super.onPause();
        LogUtil.d("FrgVote, onPause");
        if (_handlerTimer != null) {
            _handlerTimer.removeCallbacksAndMessages(null);
            _handlerTimer = null;
        }

        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }

        if (_timerAd != null) {
            _timerAd.cancel();
            _timerAd = null;
        }

        LogUtil.d("FrgVote, onPause");
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtil.d("FrgVote, onResume");
        if (_handlerTimer == null) {
            _handlerTimer = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    LogUtil.d("handleMessage");
                    JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
                    setMyVoteCountPoint(userInfo);

                    setActiveAdTextDisplay(Constants.enum_ad.none, true);
                    //_handlerTimer.sendEmptyMessageDelayed(0, _timerInterval);
                }
            };
        }
        if (_handlerTimer != null) {
            _handlerTimer.sendEmptyMessageDelayed(0, _timerInterval);
        }

        //initTimer();
    }


    private void callApi_artist_vote_listing(final boolean isClear) {

        if (isClear) {
            _last_no = -1;
        }

        LogUtil.e("==========callApi_artist_vote_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_vote_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , _last_no
                , _sort
                , _gender
                , "Y"
                , ""
                , ""
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");

                    _last_month = JSONUtil.getString(jsonData, "last_month");

                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                    if (isClear) {
                        _list.removeAll();
                    }
                    if (jsonList.length() > 0) {
                        _last_no = JSONUtil.getInteger(jsonData, "last_no");
                    }

                    for (int i = 0; i < jsonList.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                        JSONUtil.puts(j, "rank", i + 1);
                    }

                    boolean checked = _btnCheckFavorite.isSelected();

                    JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
                    int artist_no = JSONUtil.getInteger(jsonUser, "artist_no");

                    if (checked && artist_no > 0) {
                        // 최애 맨위로.
                        List<JSONObject> list = new ArrayList<JSONObject>();

                        for (int i = 0; i < jsonList.length(); i++) {
                            JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                            int no = JSONUtil.getInteger(j, "no");
                            list.add(j);

                            if (i > 0 && artist_no == no) {
                                //list.remove(i);
                                list.add(0, j);
                            }
                        }

                        jsonList = new JSONArray();

                        for (int i = 0; i < list.size(); i++) {
                            jsonList.put(i, list.get(i));
                        }
                    }


                    _header.setVisibility(View.VISIBLE);
                    _list.addItems(jsonList);

                    _amount_ticket_ad = JSONUtil.getInteger(jsonData, "amount_ticket_ad", -1);
                    _point_amount = JSONUtil.getDouble(jsonData, "point_amount", -1);

                    SPUtil.getInstance().setAdTicketAmount(getContext(), _amount_ticket_ad);
                    SPUtil.getInstance().setAdPointAmount(getContext(), Float.valueOf(String.valueOf(_point_amount)));

                    setActiveAdTextDisplay(Constants.enum_ad.none, false);

                    callApi_vote_my_vote_ticket_info();

                    JSONArray banner_data = JSONUtil.getJSONArray(jsonData, "banner_data");
                    setDataBinding_Banner(banner_data);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_vote_do_vote(final int position, final int artist_no, final String artist_name, final int vote_cnt) {
        LogUtil.e("==========callApi_vote_do_vote : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_do_vote(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , artist_no
                , vote_cnt
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    if (FormatUtil.isNullorEmpty(resultMessage)) {
                        resultMessage = String.format("%s 님에게 투표권 %d장이 투표 되었습니다.", artist_name, vote_cnt);
                    }

                    if (!FormatUtil.isNullorEmpty(resultMessage)) {
                        AlertAction alert = new AlertAction();
                        String title = "투표 완료";
                        //String msg = resultMessage.replace("님에게", "님에게\n");
                        String msg = resultMessage;
                        int imgResourceID = FormatUtil.getCurrentMinute() % 2 == 0 ? R.drawable.img_pop_vote_1 : R.drawable.img_pop_vote_2;
                        alert.showAlertAction(getContext(), title, msg, imgResourceID);
                    }

                    voteUp(position, artist_no, vote_cnt);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_vote_my_vote_ticket_info() {
        LogUtil.e("==========callApi_vote_my_vote_ticket_info : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_my_vote_ticket_info(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    JSONObject userJson = JSONUtil.getJSONObject(json, "data");

                    setMyVoteCountPoint(userJson);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_vote_put_ticket_init() {
        LogUtil.e("==========callApi_vote_put_ticket_init : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_put_ticket_init(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }

                    callApi_vote_my_vote_ticket_info();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


//    private void callApi_banner_listing() {
//        LogUtil.e("==========callApi_vote_do_vote : start==========");
//
//        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
//
//        Call<JsonObject> call = apiInterface.banner_listing(
//                SPUtil.getInstance().getUserNoEnc(getContext())
//                , "home"
//        );
//
//        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);
//        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
//            @Override
//            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
//                super.onResponse(resultCode, resultMessage, json);
//
//                try {
//                    ProgressDialogUtil.dismiss(dialog);
//
//                    LogUtil.d("resultCode : " + resultCode);
//                    LogUtil.d("resultMessage : " + resultMessage);
//                    LogUtil.json(json);
//
//                    if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
//                        new Alert().showAlert(getContext(), resultMessage);
//                        return;
//                    }
//
//                    JSONObject data = JSONUtil.getJSONObject(json, "data");
//                    JSONArray list = JSONUtil.getJSONArray(data, "list_data");
//
//                    setDataBinding_Banner(list);
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                super.onFailure(call, t);
//                ProgressDialogUtil.dismiss(dialog);
//                LogUtil.d("onFailure");
//            }
//        });
//    }


    private void setMyVoteCountPoint(JSONObject userJson) {
        String vote_dttm_remain = "";
        double point = 0;

        LogUtil.d("_txtAd, FrgVote setMyVoteCountPoint, userJson : " + (userJson == null ? "null" : userJson.toString()));

        if (userJson != null) {
            _vote_cnt = JSONUtil.getInteger(userJson, "vote_cnt", 0);
            point = JSONUtil.getDouble(userJson, "point", 0);
            vote_dttm_remain = JSONUtil.getString(userJson, "vote_dttm_remain", "");
        }
        if (_vote_cnt < 1 && (FormatUtil.isNullorEmpty(vote_dttm_remain) || vote_dttm_remain.equals("00:00:00"))) {
            callApi_vote_put_ticket_init();
        }

        _txtTicket.setText("투표권 : " + FormatUtil.toPriceFormat(_vote_cnt) + "장 | 포인트 : " + FormatUtil.toPriceFormat(point) + " P");
    }


    private void setDataBinding_Banner(JSONArray list) {

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _base_vote_top.setVisibility(View.GONE);
            _base_vote_bottom.setVisibility(View.VISIBLE);
            _pager.setVisibility(View.GONE);
            return;
        }

        _selectedIndex = 0;

        try {

            for (int i = 0; i < list.length(); i++) {
                JSONObject j = JSONUtil.getJSONObject(list, i);

                FrgBanner frgBanner01 = new FrgBanner();
                frgBanner01.setData(j);

                _dot_list.get(i).setVisibility(View.VISIBLE);
                _dot_list.get(i).setImageResource(i == 0 ? R.drawable.btn_tradebar_over : R.drawable.btn_tradebar);
                _adapter.addItem(frgBanner01);
            }

            // 배너광고가 1개라면, dot 이미지는 안보이게 처리합니다.
            if (list.length() == 1) {
                _dot_list.get(0).setVisibility(View.GONE);
            }

            _pager.setAdapter(_adapter);
            _pager.setCurrentItem(_selectedIndex);
            _pager.setVisibility(View.VISIBLE);

            _base_vote_top.setVisibility(list.length() == 0 ? View.VISIBLE : View.GONE);
            _base_vote_bottom.setVisibility(list.length() > 0 ? View.VISIBLE : View.GONE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void voteUp(int position, int artist_no, int voted_cnt) {
        final JSONObject json = _list.getItem(position);

        int vote_cnt = JSONUtil.getInteger(json, "vote_cnt", 0);
        int vote_cnt_week = JSONUtil.getInteger(json, "vote_cnt_week", 0);
        int vote_cnt_month = JSONUtil.getInteger(json, "vote_cnt_month", 0);
        int my_vote_cnt = JSONUtil.getInteger(json, "my_vote_cnt", 0);

        vote_cnt = vote_cnt + voted_cnt;
        vote_cnt_week = vote_cnt_week + voted_cnt;
        vote_cnt_month = vote_cnt_month + voted_cnt;

        my_vote_cnt = my_vote_cnt + voted_cnt;

        JSONUtil.puts(json, "vote_cnt", vote_cnt);
        JSONUtil.puts(json, "vote_cnt_week", vote_cnt_week);
        JSONUtil.puts(json, "vote_cnt_month", vote_cnt_month);
        JSONUtil.puts(json, "my_vote_cnt", my_vote_cnt);

        try {
            _list.getAdapter().notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

        _vote_cnt = _vote_cnt - voted_cnt;


        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
        JSONUtil.puts(userInfo, "vote_cnt", _vote_cnt);
        SPUtil.getInstance().setUserInfo(getContext(), userInfo);
        setMyVoteCountPoint(userInfo);
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        LinearLayout baseLeft = (LinearLayout) convertView.findViewById(R.id.baseLeft);
        de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
        TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
        View baseMyCount = convertView.findViewById(R.id.baseMyCount);


        TextView txtVoteCount = (TextView) convertView.findViewById(R.id.txtVoteCount);
        TextView txtMyCount = (TextView) convertView.findViewById(R.id.txtMyCount);

        View baseRank = convertView.findViewById(R.id.baseRank);
        TextView txtRankTitle = (TextView) convertView.findViewById(R.id.txtRankTitle);
        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);

        ImageView img_rank = convertView.findViewById(R.id.img_rank);
        TextView txtFix = convertView.findViewById(R.id.txtFix);


        img_rank.setVisibility(View.GONE);
        txtFix.setVisibility(View.GONE);
        baseMyCount.setVisibility(View.GONE);

        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        final String artist_name = JSONUtil.getString(json, "name");
        final String pic1 = JSONUtil.getStringUrl(json, "pic");
        final int artist_no = JSONUtil.getInteger(json, "no", 0);

        //int vote_cnt = JSONUtil.getInteger(json, "vote_cnt", 0);
        int vote_cnt_month = JSONUtil.getInteger(json, "vote_cnt_month", 0);
        int vote_cnt = JSONUtil.getInteger(json, _sort, 0);
        int my_vote_cnt = JSONUtil.getInteger(json, "my_vote_cnt", 0);
        int rank = JSONUtil.getInteger(json, "rank", 0);

        int last_week_rank = JSONUtil.getInteger(json, "last_week_rank", 0);
        int last_month_rank = JSONUtil.getInteger(json, "last_month_rank", 0);


        baseRank.setVisibility(View.GONE);

        LogUtil.d("pic1 : " + pic1);

        String name = (rank) + ". " + artist_name;
        String nameSub = "";
        if (_sort.equals("vote_cnt_week") || _sort.equals("vote_cnt_month")) {

            int last_rank = _sort.equals("vote_cnt_week") ? last_week_rank : last_month_rank;
            String last_str = _sort.equals("vote_cnt_week") ? "지난주" : "지난달";

            if (FormatUtil.isNullorEmpty(_gender)) {
                int gap = rank - last_rank;
                String gapStr = gap == 0 ? "-" : "<font color='red'>↑" + String.valueOf(Math.abs(gap)) + "위</font>";
                if (gap > 0) gapStr = "<font color='blue'>↓" + gap + "위</font>";
                // ↓
                // ↑

                txtRankTitle.setText(last_str);

                if (last_rank != 0)
                    txtRank.setText(Html.fromHtml(last_rank + "위(" + (gapStr) + ")"));
                else
                    txtRank.setText("-");

                baseRank.setVisibility(View.VISIBLE);
            }
//            else {
//
//                int gap = last_gender_ranking - rank ;
//                String gapStr = gap == 0 ? "-" : String.valueOf(gap);
//                if(gap > 0) gapStr = "+" + gap;
//
//                nameSub = _last_month.substring(5, 7) + "월 : " + last_gender_ranking + "위(" + (gapStr) + ")";
//            }
        }

//        int target_vote_cnt = 3000000;
//        if (_sort.equals("vote_cnt_month")) {
//            if (vote_cnt >= target_vote_cnt) {
//                //int man = (vote_cnt - (vote_cnt % 1000000)) / 10000;
//                double per = vote_cnt * 100.0 / target_vote_cnt;
//
//                txtFix.setText("300만표 달성(" + String.format("%.2f", per) + "%) - 지하철광고 확정");
//                txtFix.setVisibility(View.VISIBLE);
//            }
//        }

        if (rank <= 5) {
            txtArtist.setText(artist_name);
            img_rank.setVisibility(View.VISIBLE);

            switch (rank) {
                case 1:
                    img_rank.setImageResource(R.drawable.ic_rank_1);
                    break;
                case 2:
                    img_rank.setImageResource(R.drawable.ic_rank_2);
                    break;
                case 3:
                    img_rank.setImageResource(R.drawable.ic_rank_3);
                    break;
                case 4:
                    img_rank.setImageResource(R.drawable.ic_rank_4);
                    break;
                case 5:
                    img_rank.setImageResource(R.drawable.ic_rank_5);
                    break;
            }
        } else {
            txtArtist.setText(name);
            img_rank.setVisibility(View.GONE);
        }

        txtVoteCount.setText(FormatUtil.toPriceFormat(vote_cnt));
        txtMyCount.setText(FormatUtil.toPriceFormat(my_vote_cnt));

        if (my_vote_cnt > 0) {
            baseMyCount.setVisibility(View.VISIBLE);
        }

//        View adViewBody = convertView.findViewById(R.id.adViewBody);
//        AdView adView = convertView.findViewById(R.id.adView);
//
//        if (Constants.DISPLAY_AD && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())) {
//            adViewBody.setVisibility(View.VISIBLE);
//            AdRequest adRequest = new AdRequest.Builder().build();
//            adView.loadAd(adRequest);
//        } else {
//            adViewBody.setVisibility(View.GONE);
//        }

        if (!FormatUtil.isNullorEmpty(pic1)) {
            Picasso.with(getContext())
                    .load(pic1)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);
        WebView webView = convertView.findViewById(R.id.webView);
        ImageView imgAd = convertView.findViewById(R.id.imgAd);
        View baseAdHouse = convertView.findViewById(R.id.baseAdHouse);

        baseAdHouse.setVisibility(View.GONE);
        adViewBody.setVisibility(View.GONE);

//        if (Constants.DISPLAY_AD
//                && Constants.DISPLAY_AD_ROW
//                && (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD || position % Constants.AD_UNIT == Constants.AD_UNIT_MOD_02)
//                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
//                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
//        ) {
//
//            if (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD) {
//
//                JSONArray list = SPUtil.getInstance().getAdBannerList(getContext());
//                JSONObject jsonAd = JSONUtil.getJSONObject(list, 0);
//
//                String img_url = JSONUtil.getStringUrl(jsonAd, "img_url");
//                final String click_url = JSONUtil.getStringUrl(jsonAd, "click_url");
//                final String no = JSONUtil.getString(jsonAd, "no");
//
//                adViewBody.setVisibility(View.VISIBLE);
//                Picasso.with(getContext())
//                        .load(img_url)
//                        .fit()
//                        .placeholder(R.drawable.img_noimg)
//                        .into(imgAd);
//
//                baseAdHouse.setVisibility(View.VISIBLE);
//
//                adView.setVisibility(View.GONE);
//                webView.setVisibility(View.GONE);
//
//                imgAd.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        callApi_logClick_add_log(Constants.LOG_CLICK_MORE_AD_ROW + no);
//
//                        Intent i = new Intent(Intent.ACTION_VIEW);
//                        i.setData(Uri.parse(click_url));
//                        startActivity(i);
//                    }
//                });
//
//            } else if (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD_02) {
//                adViewBody.setVisibility(View.VISIBLE);
//                AdRequest adRequest = new AdRequest.Builder().build();
//                adView.loadAd(adRequest);
//                adView.setVisibility(View.VISIBLE);
//                webView.setVisibility(View.GONE);
//            } else {
//                adViewBody.setVisibility(View.GONE);
//            }
//
//        } else {
//            adViewBody.setVisibility(View.GONE);
//        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_vote_cnt <= 1) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (which == Alert.BUTTON1) {
                                callApi_vote_do_vote(position, artist_no, artist_name, 1);
                            }
                        }
                    });
                    alert.showAlert(getContext(), artist_name + " 님에게 투표하시겠습니까?", true, "확인", "취소");
                } else {
                    AlertPop alertPop = new AlertPop();
                    alertPop.setOnStringListener(new InterfaceSet.OnStringListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which, String str) {
                            if (which == Alert.BUTTON1) {
                                int vote_cnt = Integer.parseInt(str);
                                callApi_vote_do_vote(position, artist_no, artist_name, vote_cnt);
                            }
                        }
                    });
                    alertPop.showVote(getContext(), artist_no, artist_name, pic1, vote_cnt_month, my_vote_cnt);
                }
            }
        });

        return convertView;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (_timer != null) {
            _timer.cancel();
            _timer = null;
        }

        if (_timerAd != null) {
            _timerAd.cancel();
            _timerAd = null;
        }
    }

}
