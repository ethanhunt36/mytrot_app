package com.code404.mytrot.frg.attend;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgAttend extends FrgBase implements GListView.IMakeView {

    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;

    private View _header = null;


    private String _last_no = "-1";

    JSONArray _jsonVodList = null;
    private boolean _is_more_data = true;

    @SuppressLint("ValidFragment")
    public FrgAttend() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_attend);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);
        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_attend, null);
    }


    @Override
    protected void init() {
        _list.addHeaderView(_header);
        _list.setViewMaker(R.layout.row_attend, this);
        _list.setNoData(_baseNoData);

        _txtNoData.setText("랭킹은 잠시 후에 확인 부탁 드립니다.");

        callApi_attendance_ranking(true);
    }


    @Override
    public void refresh() {
        _is_more_data = true;
        _last_no = "-1";

        if (_list != null) {
            _list.removeAll();
        }

        callApi_attendance_ranking(true);
    }


    @Override
    protected void configureListener() {

//        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//
//                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
//
//                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));
//
//                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
//                LogUtil.d("_multiList.isTop() : " + _list.isTop());
//
//                if (_list.isBottom()) {
//                    callApi_attendance_ranking(false);
//                }
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // callApi_board_listing(true);
    }


    private void callApi_attendance_ranking(final boolean isClear) {
        LogUtil.e("==========callApi_attendance_ranking : start==========");

        if (_is_more_data == false) {
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.attendance_ranking(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_TOP100
                , _last_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    _jsonVodList = JSONUtil.getJSONArray(jsonData, "list_data");


                    _last_no = JSONUtil.getString(jsonData, "last_no", "-1");

                    if (isClear) {
                        _list.removeAll();
                    }

                    if (_jsonVodList.length() < 1) {
                        _is_more_data = false;
                        return;
                    }

                    _list.addItems(_jsonVodList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);

                    if (isClear) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                _list.smoothScrollToPosition(0);
                            }
                        }, 500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ;
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtCombo = (TextView) convertView.findViewById(R.id.txtCombo);


        txtTitle.setText((position + 1) + ". " + JSONUtil.getString(json, "nick"));
        txtCombo.setText(JSONUtil.getString(json, "message"));

        return convertView;
    }
}
