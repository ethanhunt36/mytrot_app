package com.code404.mytrot.frg.vote;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.etc.AtvLuckyNumber;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.more.AtvMoreChargeB;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.atv.web.AtvWeb;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAction;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgMission extends FrgBase implements GListView.IMakeView {


    private GListView _list = null;

    private View _header = null;
    private TextView _txtMissionDesc = null;

    private String _last_no = "-1";
    private String _reward_yn = "";
    private String _podong_url = "";

    private String _youtube_url = "";
    private String _instar_url = "";


    private Button _btnReceive = null;

    private boolean _isLoading = false;
    private String _action_code = "";

    private int _countFailedAdView = 0;

    // 포인트클릭 가능한지.
    private JSONObject _pc_info = null;

    @SuppressLint("ValidFragment")
    public FrgMission() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_mission);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _btnReceive = (Button) findViewById(R.id.btnReceive);
        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_mission, null);

        _txtMissionDesc = _header.findViewById(R.id.txtMissionDesc);

        _list.addHeaderView(_header);

        _list.setViewMaker(R.layout.row_mission, this);
    }


    @Override
    protected void init() {
        LogUtil.d("FrgMission, init");

        _txtMissionDesc.setText("");


    }


    @Override
    public void refresh() {
        try {
            LogUtil.d("FrgMission, refresh");

            _last_no = "-1";

            callApi_memberMission_get(true);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {
        _btnReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_memberMission_bonus();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();

        LogUtil.d("FrgMission, onResume");

        callApi_memberMission_get(true);


    }


    private void callApi_memberMission_get(final boolean isClear) {

        if (_isLoading == true) return;

        _isLoading = true;

        LogUtil.e("==========callApi_memberMission_get : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberMission_get(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    _isLoading = false;
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject data = JSONUtil.getJSONObject(json, "data");

                    int success_cnt = JSONUtil.getInteger(data, "success_cnt", -1);
                    JSONArray jsonList = JSONUtil.getJSONArray(data, "list");

                    JSONObject rewardInfo = JSONUtil.getJSONObject(data, "reward");

                    _reward_yn = JSONUtil.getString(data, "reward_yn");
                    String can_reward_yn = JSONUtil.getString(data, "can_reward_yn");
                    _podong_url = JSONUtil.getString(data, "podong_url");
                    _youtube_url = JSONUtil.getString(data, "youtube_url");
                    _instar_url = JSONUtil.getString(data, "instar_url");


                    _pc_info = JSONUtil.getJSONObject(data, "pc_info");

                    if (_reward_yn.equals("Y")) {
                        setBtnText();
                    }

                    if (_txtMissionDesc != null) {

                        int reward_vote = JSONUtil.getInteger(rewardInfo, "reward_vote");
                        int reward_point = JSONUtil.getInteger(rewardInfo, "reward_point");

                        String txt = "오늘의 미션 모두를 100% 달성 시<br>" + String.format("<b>투표권 <font color='red'>%d장</font></b>과 <b>포인트 <font color='red'>%dP</font></b>를 선물로 드려요~", reward_vote, reward_point);

                        if (reward_vote > 0 && reward_point == 0) {
                            txt = "오늘의 미션 모두를 100% 달성 시<br>" + String.format("<b>투표권 <font color='red'>%d장</font></b>을 선물로 드려요~", reward_vote);
                        }

                        if (success_cnt > 0) {
                            txt += "<br>오늘 <font color='red'><b>" + FormatUtil.toPriceFormat(success_cnt) + "명</b></font>이 미션 달성 보상을 받았습니다.";
                        }

                        _txtMissionDesc.setText(Html.fromHtml(txt));

                        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                            _txtMissionDesc.setText("");
                            _txtMissionDesc.setPadding(0, 0, 0, 0);
                            _txtMissionDesc.setLineSpacing(0, 0);
                        }
                    }

                    if (isClear && _list != null) {
                        _list.removeAll();
                    }

                    _list.addItems(jsonList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);

                    LogUtil.d("jsonList, size : " + jsonList.length());


//                    if("Y".equals(can_reward_yn) && "N".equals(_reward_yn)) {
//                        new Alert().showAlert(getContext(), "미션 100% 달성하였습니다.\n\n미션 보상을 받으시겠습니까?");
//                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);

                _isLoading = false;
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_memberMission_check_mission_view_instar() {

        LogUtil.e("==========callApi_memberMission_check_mission_view_instar : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.memberMission_check_mission_view_instar(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }


    private void callApi_memberMission_check_mission_view_youtube() {

        LogUtil.e("==========callApi_memberMission_check_mission_view_youtube : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.memberMission_check_mission_view_youtube(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

    private void callApi_memberMission_check_mission_view_blog() {

        LogUtil.e("==========callApi_memberMission_check_mission_view_blog : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.memberMission_check_mission_view_blog(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
            }
        });
    }

    private void callApi_memberMission_bonus() {
        LogUtil.e("==========callApi_memberMission_bonus : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberMission_bonus(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject data = JSONUtil.getJSONObject(json, "data");
                    String reward_yn = JSONUtil.getString(data, "reward_yn");

                    if ("Y".equals(reward_yn)) {
                        _reward_yn = reward_yn;
                        setBtnText();

                        callApi_memberMission_get(true);

                        AlertAction alert = new AlertAction();
                        String title = "미션 100% 달성";
                        String msg = resultMessage;
                        int imgResourceID = FormatUtil.getCurrentMinute() % 2 == 0 ? R.drawable.img_pop_point_1 : R.drawable.img_pop_point_2;
                        alert.showAlertAction(getContext(), title, msg, imgResourceID);

                    } else {
                        if (!FormatUtil.isNullorEmpty(resultMessage)) {
                            new Alert().showAlert(getContext(), resultMessage);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_memberMission_check_mission_podong() {
        LogUtil.e("==========callApi_memberMission_check_mission_podong : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberMission_check_mission_podong(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_memberMission_check_mission_ad_movie() {
        LogUtil.e("==========callApi_memberMission_check_mission_ad_movie : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberMission_check_mission_ad_movie(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);
                ProgressDialogUtil.dismiss(dialog);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        refresh();

                    }
                }, 400);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");

                refresh();
            }
        });
    }

    private void setBtnText() {
        if (_reward_yn.equals("Y")) {
            _btnReceive.setText("미션 보상 받기 완료");
            _btnReceive.setTextColor(getResources().getColor(R.color.black));
        }
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        View baseRow = convertView.findViewById(R.id.baseRow);
        TextView txtMission = convertView.findViewById(R.id.txtMission);
        TextView txtMissionStatus = convertView.findViewById(R.id.txtMissionStatus);

        final String code = JSONUtil.getString(json, "code");
        String name = JSONUtil.getString(json, "name");
        String unit = JSONUtil.getString(json, "unit");
        int stat = JSONUtil.getInteger(json, "stat", 0);
        int cnt = JSONUtil.getInteger(json, "cnt", 0);
        final String complete_yn = JSONUtil.getString(json, "complete_yn");

        name = name + " - " + cnt + unit;

        txtMission.setText(name);

        String status = "";

        if (stat >= cnt || complete_yn.equals("Y")) {
            status = "완료";
            txtMissionStatus.setText(status);
            txtMissionStatus.setBackgroundResource(R.drawable.bg_typebox_sing);
            txtMissionStatus.setTextColor(getResources().getColor(R.color.violet_blue));
            baseRow.setBackgroundResource(R.drawable.btn_gray);
        } else {
            status = String.format("%d / %d", stat, cnt);
            txtMissionStatus.setText(status);
            txtMissionStatus.setBackgroundResource(R.drawable.bg_typebox_ent);
            txtMissionStatus.setTextColor(getResources().getColor(R.color.tangerine));
            baseRow.setBackgroundResource(R.drawable.btn_orange);
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _action_code = "";
                if ("READ_PODONG".equals(code) || "READ_LUCKY".equals(code) || "READ_AD_MOVIE".equals(code)) {
                    _action_code = code;
                    AdViewUtil.goActionAfterAdView(getActivity(), getContext(), true, false, new InterfaceSet.AdFullViewCompleteListener() {
                        @Override
                        public void onAfterAction() {
                            action_after_ad();
                        }
                    });
                } else if ("READ_BLOG".equals(code)) {
                    Intent i = new Intent();
                    i.setClass(getContext(), AtvWeb.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "blog");
                    startActivity(i);
                    callApi_memberMission_check_mission_view_blog();
                } else if ("READ_INSTAR".equals(code)) {
                    String instar_url = "https://www.instagram.com/mytrot.app/";

                    if(!FormatUtil.isNullorEmpty(_instar_url)) instar_url = _instar_url;

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(instar_url));
                    intent.setPackage("com.instagram.android");
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(instar_url)));
                    }

                    callApi_memberMission_check_mission_view_instar();
                } else if ("READ_YOUTUBE".equals(code)) {
                    String youtube_url = "https://www.youtube.com/@code404-mytrot/videos";

                    Calendar now = Calendar.getInstance();
                    int second = now.get(Calendar.SECOND);

                    if (second % 2 == 0)
                        youtube_url = "https://www.youtube.com/@code404-mytrot/shorts";

                    if(!FormatUtil.isNullorEmpty(_youtube_url)) youtube_url = _youtube_url;

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtube_url));
                    intent.setPackage("com.google.android.youtube");
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtube_url)));
                    }

                    callApi_memberMission_check_mission_view_youtube();
                } else if ("GET_CERTI".equals(code)) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.EXTRAS_TYPE, "GET_CERTI");
                    intent.setClass(getContext(), AtvMore.class);
                    startActivity(intent);
                    getActivity().finishAffinity();
                } else if ("PPOBKKI".equals(code)) {
                    Intent i = new Intent();
//                    i.setClass(getContext(), AtvEventPP.class);
//                    i.putExtra(Constants.EXTRAS_TYPE, "N");
                    i.setClass(getContext(), AtvMoreChargeB.class);
                    startActivity(i);
                } else if ("VOTE".equals(code)) {
                    Intent intent = new Intent();
                    intent.setClass(getContext(), AtvVote.class);
                    startActivity(intent);
                } else if ("GET_AD_BONUS".equals(code)) {
                    Intent intent = new Intent();
                    intent.setClass(getContext(), AtvMoreChargeB.class);
                    startActivity(intent);
                } else if ("DONATION".equals(code)) {
                    Intent intent = new Intent();
                    intent.setClass(getContext(), AtvVote.class);
                    intent.putExtra(Constants.EXTRAS_FRG_INDEX, 1);
                    startActivity(intent);
                } else if ("CHEERUP_WRTE".equals(code)) {
                    Intent intent = new Intent();
                    intent.setClass(getContext(), AtvVote.class);
                    intent.putExtra(Constants.EXTRAS_FRG_INDEX, 3);
                    startActivity(intent);
                } else if ("ATTENDANCE".equals(code) && complete_yn.equals("N")) {
//                    Intent intent = new Intent();
//                    intent.setClass(getContext(), AtvVote.class);
//                    startActivity(intent);

                    new Alert().showAlert(getContext(), "화면 상단, 오른쪽의 '출석부' 버튼을 눌러주세요.");
                }
            }
        });

        return convertView;
    }


    private void action_after_ad() {

        LogUtil.d("_txtAd, action_after_ad, _countFailedAdView : " + _countFailedAdView);

        if ("READ_PODONG".equals(_action_code)) {
            gotoPodong();
        } else if ("READ_LUCKY".equals(_action_code)) {
            gotoLucky();
        } else if ("READ_AD_MOVIE".equals(_action_code)) {
            checkMissionAd();
        }
    }


    private void gotoPodong() {
        callApi_memberMission_check_mission_podong();

        String ad_key = JSONUtil.getString(_pc_info, "ad_key");
        int ad_profit = JSONUtil.getInteger(_pc_info, "ad_profit");


        if (!FormatUtil.isNullorEmpty(ad_key) && ad_profit > 0) {
            callApi_pointClick_check(ad_key, ad_profit);
            return;
        }


        gotoPodongReal();
    }


    private void gotoPodongReal() {
        int member_no = SPUtil.getInstance().getUserNo(getContext());
        String url = _podong_url.replace("[mt_member_no]", String.valueOf(member_no));
        Intent i = new Intent();
        i.setClass(getContext(), AtvWeb.class);
        i.putExtra(Constants.EXTRAS_TYPE, "READ_PODONG");
        i.putExtra(Constants.EXTRAS_URL, url);
        startActivity(i);
    }


    private void callApi_pointClick_check(String ad_key, double ad_profit) {

        LogUtil.e("==========callApi_pointClick_check : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.pointClick_check(
                ad_key
                , SPUtil.getInstance().getUserNoEnc(getContext())
                , ad_profit
                , SPUtil.getInstance().getAndroidID(getContext())
                , "N"
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    gotoPodongReal();
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                int result_code = JSONUtil.getInteger(data, "result_code");

                String result_message = JSONUtil.getString(data, "result_message");
                String landing_url = JSONUtil.getString(data, "landing_url");
                String auto_cpa = JSONUtil.getString(data, "auto_cpa", "");

                String ad_participation = JSONUtil.getString(data, "ad_participation", "");


                if (result_code != 200) {
                    gotoPodongReal();
                    return;
                }


                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(landing_url));
                startActivity(intent);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void gotoLucky() {
        Intent i = new Intent();
        i.setClass(getContext(), AtvLuckyNumber.class);
        startActivity(i);
    }


    private void checkMissionAd() {
        callApi_memberMission_check_mission_ad_movie();

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.intent(data);
    }
}
