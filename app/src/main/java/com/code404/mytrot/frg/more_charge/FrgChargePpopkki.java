package com.code404.mytrot.frg.more_charge;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;

import org.json.JSONArray;
import org.json.JSONObject;


@SuppressLint("ValidFragment")
public class FrgChargePpopkki extends FrgBase  {

    private TextView _txtHeartGetUser = null;



    private Animation _animFadeIn, _animFadeOut;
    private CountDownTimer _timerHeartGetUser = null;


    @SuppressLint("ValidFragment")
    public FrgChargePpopkki() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_charge_ppopkki);
    }


    @Override
    protected void findView() {
        _txtHeartGetUser = (TextView) findViewById(R.id.txtHeartGetUser);
    }


    @Override
    protected void init() {
        LogUtil.d("FrgVs, init");
        _txtHeartGetUser.setText("");


        _animFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
        _animFadeOut = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
    }


    @Override
    public void refresh() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {

    }



    @Override
    public void onResume() {
        super.onResume();

        LogUtil.d("FrgVs, onResume");


    }


    @Override
    public void onPause() {
        super.onPause();


        if (_timerHeartGetUser != null) {
            _timerHeartGetUser.cancel();
            _timerHeartGetUser = null;
        }
    }


    private void initTimer() {



        if (_timerHeartGetUser == null) {

            _timerHeartGetUser = new CountDownTimer((1000L) * 60 * 60 * 4, 1000L * 10) {
                @Override
                public void onTick(long millisUntilFinished) {
                    setDataBinding_heart_get_user();
                }

                @Override
                public void onFinish() {
                    LogUtil.d("onTick, onFinish");
                }
            };

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (_timerHeartGetUser != null) {
                        _timerHeartGetUser.start();
                    }
                }
            }, 1000L * 10);
        }
    }



    private JSONArray _newspic_list = null;
    private JSONArray _heart_get_user_list = null;

    private int _index_heart_get_user = 0;

    private int _index_newspic_view = 0;
    private static final int COUNT_NEWSPIC_VIEW = 20;

    private void setDataBinding_heart_get_user(JSONArray heart_get_user_list) {

        _index_heart_get_user = 0;
        _heart_get_user_list = heart_get_user_list;

        setDataBinding_heart_get_user();
    }

    private void setDataBinding_heart_get_user() {

        if (_heart_get_user_list == null || _heart_get_user_list.length() < 1) {
            _txtHeartGetUser.setVisibility(View.GONE);
            return;
        }
        if (_index_heart_get_user >= _heart_get_user_list.length()) _index_heart_get_user = 0;

        JSONObject json = JSONUtil.getJSONObject(_heart_get_user_list, _index_heart_get_user);

        if (json != null) {
            String nick = JSONUtil.getString(json, "nick");
//            String time_diff = JSONUtil.getString(json, "time_diff");
            String heart_amount = JSONUtil.getString(json, "heart_amount");

            String reg_dttm = JSONUtil.getString(json, "reg_dttm");
            String time_diff = FormatUtil.getHeartDateFormat(reg_dttm);

            String msg = String.format("<b>%s</b>, '%s'님이 하트 %s개 당첨되셨습니다.", time_diff, nick, heart_amount);

            if (!FormatUtil.isNullorEmpty(_txtHeartGetUser.getText().toString())) {
                _txtHeartGetUser.startAnimation(_animFadeOut);
                _txtHeartGetUser.startAnimation(_animFadeIn);
            }

            _txtHeartGetUser.setText(Html.fromHtml(msg));
            _txtHeartGetUser.setVisibility(View.VISIBLE);
            _index_heart_get_user++;
        }
    }
}
