package com.code404.mytrot.frg.etc;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgVoteHistory extends FrgBase implements GListView.IMakeView {

    private String _type = "GET";
    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;
    private int _last_no = -1;

    @SuppressLint("ValidFragment")
    public FrgVoteHistory() {

    }

    public void setType(String type) {
        _type = type;
        _last_no = -1;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_point);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);
    }


    @Override
    protected void init() {
        _list.setViewMaker(R.layout.row_point, this);
        _list.setNoData(_baseNoData);
        _txtNoData.setText("내역이 없습니다.");
    }


    @Override
    public void refresh() {
        if (_list == null) {
            findView();
        }
        _last_no = -1;
        _list.removeAll();
        callApi_vote_get_all_history(true);
    }


    @Override
    protected void configureListener() {
        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_vote_get_all_history(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        callApi_vote_get_all_history(true);
    }

    private void callApi_vote_get_all_history(final boolean isClear) {

        LogUtil.e("==========callApi_vote_get_all_history : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;


        call = apiInterface.vote_get_all_history(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _type
                , Constants.RECORD_SIZE
                , _last_no
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list");

                if (isClear) {
                    _list.removeAll();
                }

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(final GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        RelativeLayout baseTitle = (RelativeLayout) convertView.findViewById(R.id.baseTitle);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtGubun = (TextView) convertView.findViewById(R.id.txtGubun);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        String title = "";
        String subject = JSONUtil.getString(json, "subject");
        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        int amount = JSONUtil.getInteger(json, "amount");
        int amount_before = JSONUtil.getInteger(json, "amount_before");
        int amount_after = JSONUtil.getInteger(json, "amount_after");

        reg_dttm = reg_dttm.substring(0, 16);


        txtTitle.setText(subject);
        txtDate.setText(reg_dttm);
        txtGubun.setText("");
        //txtGubun.setText(String.format("%s -> %s", FormatUtil.toPriceFormat(amount_before), FormatUtil.toPriceFormat(amount_after)));
        txtPoint.setText(FormatUtil.toPriceFormat(amount) + " 장");
        return convertView;
    }


}
