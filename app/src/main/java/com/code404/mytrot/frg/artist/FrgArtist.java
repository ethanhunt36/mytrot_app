package com.code404.mytrot.frg.artist;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.artist.AtvArtistVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgArtist extends FrgBase implements GListView.IMakeView {

    private GListView _list = null;

    private View _header = null;

    private int _last_no = -1;


    @SuppressLint("ValidFragment")
    public FrgArtist() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_artist);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_artist, null);
        _list.addHeaderView(_header);
    }


    @Override
    protected void init() {
        _list.setViewMaker(R.layout.row_artist, this);
    }


    @Override
    public void refresh() {
        try {
            if (_list != null) _list.removeAll();
            _last_no = -1;
            callApi_artist_vote_listing(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {
        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_artist_vote_listing(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        callApi_artist_vote_listing(false);
    }

    private void callApi_artist_vote_listing(final boolean isClear) {
        LogUtil.e("==========callApi_artist_vote_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artist_vote_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , _last_no
                , "vod_cnt"
                , ""
                , "N"
                , ""
                , ""
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                if (jsonList.length() > 0) {
                    _last_no = JSONUtil.getInteger(jsonData, "last_no");
                }

                if (_list != null) {
                    if (isClear) {
                        _list.removeAll();
                    }
                    _list.addItems(jsonList);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        LinearLayout baseLeft = (LinearLayout) convertView.findViewById(R.id.baseLeft);
        de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
        TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
        TextView txtTrotCount = (TextView) convertView.findViewById(R.id.txtTrotCount);
        TextView txtEntCount = (TextView) convertView.findViewById(R.id.txtEntCount);
        TextView txtEtcCount = (TextView) convertView.findViewById(R.id.txtEtcCount);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        txtArtist.setText(JSONUtil.getString(json, "name"));
        txtTrotCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "vod_tro_cnt")));
        txtEntCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "vod_ent_cnt")));
        txtEtcCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "vod_etc_cnt")));

        String pic1 = JSONUtil.getStringUrl(json, "pic");

        LogUtil.d("pic1 : " + pic1);

        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);

        if (Constants.DISPLAY_AD
                && Constants.DISPLAY_AD_ROW
                && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD
                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
            adViewBody.setVisibility(View.VISIBLE);
            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
        } else {
            adViewBody.setVisibility(View.GONE);
        }

        if (!FormatUtil.isNullorEmpty(pic1)) {
            Picasso.with(getContext())
                    .load(pic1)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent i = new Intent();
                i.setClass(getContext(), AtvArtistVod.class);
                i.putExtra(Constants.EXTRAS_JSON_STRING, json.toString());
                i.putExtra(Constants.EXTRAS_ARTIST_NO, JSONUtil.getInteger(json, "no"));
                i.putExtra(Constants.EXTRAS_TITLE, JSONUtil.getString(json, "name"));
                startActivity(i);


            }
        });

        return convertView;
    }



}
