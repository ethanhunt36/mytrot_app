package com.code404.mytrot.frg.vote;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.star.AtvStarInfo;
import com.code404.mytrot.atv.star.AtvStarWrite;
import com.code404.mytrot.atv.star_room.AtvStarRoomMain;
import com.code404.mytrot.atv.vote.AtvVote;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgBoard extends FrgBase implements GListView.IMakeView {

    private static final int REQ_BOARD_INFO = 7419;

    private GListView _list = null;

    private View _header = null;
    private TextView _txtSeqRecent = null;
    private TextView _txtSeqName = null;
    private TextView _txtSeqPlay = null;
    private TextView _txtST = null;
    private TextView _txtFilter = null;
    private TextView _txtTip = null;

    private String _last_no = "-1";
    private String _sort = "recent";
    private int _artist_no = -1;
    private boolean _is_call_api_ing = false;

    JSONArray _jsonVodList = null;
    private boolean _is_more_data = true;

    private int _category = 1;

    @SuppressLint("ValidFragment")
    public FrgBoard() {
    }


    public void setStarRoom(int artistNo) {
        _artist_no = artistNo;
        _category = 6;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_borad);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_trot, null);

        _txtSeqRecent = _header.findViewById(R.id.txtSeqRecent);
        _txtSeqName = _header.findViewById(R.id.txtSeqName);
        _txtSeqPlay = _header.findViewById(R.id.txtSeqPlay);
        _txtST = _header.findViewById(R.id.txtST);
        _txtFilter = _header.findViewById(R.id.txtFilter);
        _txtTip = _header.findViewById(R.id.txtTip);

        _list.addHeaderView(_header);
    }


    @Override
    protected void init() {

//        _artist_no = SPUtil.getInstance().getUserFavoriteArtistNo(getContext());
//
//        if (_artist_no > 0) {
//            String name = SPUtil.getInstance().getUserFavoriteArtistName(getContext());
//            _txtFilter.setText(name + " X");
//        }

        _list.setViewMaker(R.layout.row_board, this);
        _txtFilter.setVisibility(View.VISIBLE);

        if (_category == 6) {
            _txtFilter.setVisibility(View.GONE);
        }

        _txtSeqName.setVisibility(View.GONE);
        _txtST.setVisibility(View.VISIBLE);

        _txtTip.setText(Html.fromHtml("최애가수 팬들끼리만 이야기할 수 있는 별방이 생겼어요.<br><u><b><font color='#e49115'>바로 이동하기</font></b></u>"));
        _txtTip.setVisibility(View.VISIBLE);

        callApi_board_listing(true);
    }


    @Override
    public void refresh() {
        try {
            _is_more_data = true;
            _last_no = "-1";

            if (_list != null) {
                _list.removeAll();
            }

            callApi_list(_sort);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void callApi_list(String sort) {
        try {
            _sort = sort;
            _is_more_data = true;
            _last_no = "-1";
            _txtSeqRecent.setBackground(null);
            _txtSeqName.setBackground(null);
            _txtSeqPlay.setBackground(null);
            _txtST.setBackground(null);

            if (_sort.equals("recent")) {
                _txtSeqRecent.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _list.setViewMaker(R.layout.row_board, this);
                callApi_board_listing(true);
            }
            if (_sort.equals("popular")) {
                _txtSeqPlay.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _list.setViewMaker(R.layout.row_board, this);
                callApi_board_listing(true);
            }
            if (_sort.equals("st")) {
                _txtST.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _list.setViewMaker(R.layout.row_board_st, this);
                callApi_board_artist_stat(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void configureListener() {

        _txtTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject json = SPUtil.getInstance().getUserInfo(getContext());

                int artist_no = JSONUtil.getInteger(json, "artist_no");
                String artist_name = JSONUtil.getString(json, "artist_name");
                String artist_board_id = JSONUtil.getString(json, "artist_board_id");

                if (artist_no < 1) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            intent.setClass(getContext(), AtvMore.class);
                            startActivity(intent);
                            getActivity().finishAffinity();
                        }
                    });
                    alert.showAlert(getContext(), getString(R.string.txt_set_artist));
                    return;
                }

                Intent i = new Intent();
                i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
                i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
                i.putExtra(Constants.EXTRAS_ARTIST_BOARD_ID, artist_board_id);
                i.setClass(getContext(), AtvStarRoomMain.class);
                startActivity(i);
            }
        });

        // 응원글 작성
        findViewById(R.id.btnWrite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AdViewUtil.goActionAfterAdView(getActivity(), getContext(), false, true, new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        AlertPop alert = new AlertPop();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                _artist_no = -1;
                                _txtFilter.setText("전체");

                                callApi_list("recent");
                            }
                        });
                        alert.showWriteBoard(getContext());
                    }
                });


//                int boardEnterCount = SPUtil.getInstance().getBoardEnterCount(getContext());
//                int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(getContext());
//                LogUtil.d("boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);
//
//                if (Constants.IS_VIEW_SEQ_AD == false && _adFullOrgManager != null && _adFullOrgManager.canShowAdCount_Interstitial(true,false) > 0
//                        && boardEnterCount % repeatBoardWriteAd == 0) {
//                    _adFullOrgManager.setOnAdCompleteMissionListener(new InterfaceSet.OnAdCompleteListener() {
//                        @Override
//                        public void onAdLoadSuccess() {
//                            LogUtil.d("_txtAd, AtvStar, onAdLoadSuccess");
//                        }
//
//                        @Override
//                        public void onAdLoadFail() {
//                            LogUtil.d("_txtAd, AtvStar, onAdLoadFail");
//                        }
//
//                        @Override
//                        public void onAdClose() {
//                            LogUtil.d("_txtAd, AtvStar, onAdClose");
//
//                            _adFullOrgManager.setOnAdCompleteMissionListener(null);
//
//                            AlertPop alert = new AlertPop();
//                            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                                @Override
//                                public void onClose(DialogInterface dialog, int which) {
//                                    _artist_no = -1;
//                                    _txtFilter.setText("전체");
//
//                                    callApi_list("recent");
//                                }
//                            });
//                            alert.showWriteBoard(getContext());
//                        }
//                    });
//                    _adFullOrgManager.showAdOrder_Interstitial();
//                    return;
//                }
//
//                AlertPop alert = new AlertPop();
//                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                    @Override
//                    public void onClose(DialogInterface dialog, int which) {
//                        _artist_no = -1;
//                        _txtFilter.setText("전체");
//
//                        callApi_list("recent");
//                    }
//                });
//                alert.showWriteBoard(getContext());
            }
        });


        _txtFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_artist_no < 1) {
                    AlertPop alert = new AlertPop();
                    alert.setOnClickJsonListener(new InterfaceSet.OnClickJsonListener() {
                        @Override
                        public void onClick(View v, int pos, JSONObject json) {
                            if (json != null) {
                                _artist_no = JSONUtil.getInteger(json, "no");
                                String name = JSONUtil.getString(json, "name");
                                _txtFilter.setText(name + " X");
                            }

                            LogUtil.d("_artist_no : " + _artist_no);

                            callApi_list("recent");
                        }
                    });
                    alert.showVoteArtist(getContext());
                } else {
                    _artist_no = -1;
                    _txtFilter.setText("전체");
                    callApi_list("recent");
                }
            }
        });

        _txtSeqRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _txtFilter.setVisibility(View.VISIBLE);
                callApi_list("recent");
            }
        });


        _txtSeqPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _txtFilter.setVisibility(View.VISIBLE);
                callApi_list("popular");
            }
        });


        _txtST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _artist_no = -1;
                _txtFilter.setVisibility(View.GONE);
                callApi_list("st");
            }
        });

        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom() && !_sort.equals("st")) {
                    callApi_board_listing(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // callApi_board_listing(true);
    }


    private void callApi_board_listing(final boolean isClear) {
        LogUtil.e("==========callApi_board_listing : start==========");

        if (_is_more_data == false || _is_call_api_ing == true) {
            return;
        }
        _is_call_api_ing = true;

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _sort
                , Constants.RECORD_SIZE
                , _last_no
                , _category
                , _artist_no < 1 ? "" : String.valueOf(_artist_no)
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    _is_call_api_ing = false;
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    _jsonVodList = JSONUtil.getJSONArray(jsonData, "list_data");

                    _last_no = JSONUtil.getString(jsonData, "last_no");

                    if (isClear) {
                        _list.removeAll();
                    }

                    if (_jsonVodList.length() < 1) {
                        _is_more_data = false;
                        return;
                    }

                    _list.addItems(_jsonVodList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);

                    if (isClear) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                _list.smoothScrollToPosition(0);
                            }
                        }, 500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                _is_call_api_ing = false;
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_board_artist_stat(final boolean isClear) {
        LogUtil.e("==========callApi_board_artist_stat : start==========");

        if (_is_more_data == false) {
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_artist_stat(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    _jsonVodList = JSONUtil.getJSONArray(jsonData, "list_data");

                    _last_no = JSONUtil.getString(jsonData, "last_no");

                    if (isClear) {
                        _list.removeAll();
                    }

                    if (_jsonVodList.length() < 1) {
                        _is_more_data = false;
                        return;
                    }

                    _list.addItems(_jsonVodList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);

                    if (isClear) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                _list.smoothScrollToPosition(0);
                            }
                        }, 500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ;
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_board_inc_like(final int position, final int board_no) {
        LogUtil.e("==========callApi_board_inc_like : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_inc_like(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , board_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

//                try {
//                    if (resultCode == 0) {
//                        String data = JSONUtil.getString(json, "data");
//
//                        JSONObject jsonRow = _list.getItem(position);
//
//                        int like_cnt = JSONUtil.getInteger(jsonRow, "like_cnt");
//                        int my_like_cnt = 0;
//                        if (data.equals("inc")) {
//                            like_cnt++;
//                            my_like_cnt = 1;
//                        } else {
//                            like_cnt--;
//                        }
//
//                        JSONUtil.puts(jsonRow, "like_cnt", like_cnt);
//                        JSONUtil.puts(jsonRow, "my_like_cnt", my_like_cnt);
//
//                        _list.getAdapter().notifyDataSetChanged();
//                    } else {
//                        if (!FormatUtil.isNullorEmpty(resultMessage)) {
//                            new Alert().showAlert(getContext(), resultMessage);
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_board_inc_hate(final int position, final int board_no) {
        LogUtil.e("==========callApi_board_inc_hate : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_inc_hate(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , board_no
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                try {
                    if (resultCode == 0) {
                        String data = JSONUtil.getString(json, "data");

                        JSONObject jsonRow = _list.getItem(position);

                        int hate_cnt = JSONUtil.getInteger(jsonRow, "hate_cnt");

                        if (data.equals("inc")) {
                            hate_cnt++;
                        } else {
                            hate_cnt--;
                        }

                        JSONUtil.puts(jsonRow, "hate_cnt", hate_cnt);

                        _list.getAdapter().notifyDataSetChanged();
                    } else {
                        if (!FormatUtil.isNullorEmpty(resultMessage)) {
                            new Alert().showAlert(getContext(), resultMessage);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private int _position = -1;

    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        if (!_sort.equals("st")) {

            final int bbs_no = JSONUtil.getInteger(json, "no");

            de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
            TextView txtNickname = (TextView) convertView.findViewById(R.id.txtNickname);
            TextView txtMessage = (TextView) convertView.findViewById(R.id.txtMessage);
            TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            TextView txtLike = (TextView) convertView.findViewById(R.id.txtLike);
            TextView txtHate = (TextView) convertView.findViewById(R.id.txtHate);
            TextView txtComment = (TextView) convertView.findViewById(R.id.txtComment);
            View baseLine = (View) convertView.findViewById(R.id.baseLine);


            String artist_pic = JSONUtil.getStringUrl(json, "artist_pic");
            String artist_name = JSONUtil.getString(json, "artist_name");

            int my_like_cnt = JSONUtil.getInteger(json, "my_like_cnt", 0);
            int board_no = JSONUtil.getInteger(json, "board_no");
            LogUtil.d("my_like_cnt : " + my_like_cnt);


            String cont = JSONUtil.getString(json, "cont");

            cont = FormatUtil.replaceTag(cont);
            String reg_dttm = JSONUtil.getString(json, "reg_dttm");

            if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                txtNickname.setText(JSONUtil.getString(json, "nick") + " Lv." + JSONUtil.getString(json, "lv"));
            } else {
                txtNickname.setText(JSONUtil.getString(json, "nick"));
            }
            txtMessage.setText(Html.fromHtml(cont));
            txtDate.setText(FormatUtil.convertDateAmPmRemoveToday(reg_dttm));

            String strLike = "";
            if (my_like_cnt > 0) {
                strLike = "좋아요 <b>" + FormatUtil.toPriceFormat(JSONUtil.getString(json, "like_cnt")) + "<b>";
                txtLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bt_heart_on, 0, 0, 0);
            } else {
                strLike = "좋아요 " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "like_cnt"));
                txtLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bt_heart_off, 0, 0, 0);
            }

            txtLike.setText(Html.fromHtml(strLike));
            txtHate.setText("싫어요 " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "hate_cnt")));
            txtComment.setText("댓글 " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "comment_cnt")));

            if (!FormatUtil.isNullorEmpty(artist_pic)) {
                Picasso.with(getContext())
                        .load(artist_pic)
                        .fit()
                        .placeholder(R.drawable.img_noimg)
                        .into(imgProfile);
            } else {
                Picasso.with(getContext()).load(R.drawable.img_noimg).fit();
            }

            txtHate.setVisibility(View.GONE);

            View adViewBody = convertView.findViewById(R.id.adViewBody);
            AdView adView = convertView.findViewById(R.id.adView);
            WebView webView = convertView.findViewById(R.id.webView);
            ImageView imgAd = convertView.findViewById(R.id.imgAd);
            View baseAdHouse = convertView.findViewById(R.id.baseAdHouse);

            baseAdHouse.setVisibility(View.GONE);

            if (Constants.DISPLAY_AD
//                    && Constants.DISPLAY_AD_ROW
                    && Constants.DISPLAY_AD_HOUSE
                    && (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD || position % Constants.AD_UNIT == Constants.AD_UNIT_MOD_02)
                    && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                    && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
            ) {

                if (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD) {

                    JSONArray list = SPUtil.getInstance().getAdBannerList(getContext());
                    JSONObject jsonAd = JSONUtil.getJSONObject(list, 0);

                    String img_url = JSONUtil.getStringUrl(jsonAd, "img_url");

                    if (!FormatUtil.isNullorEmpty(img_url)) {
                        final String click_url = JSONUtil.getStringUrl(jsonAd, "click_url");
                        final String no = JSONUtil.getString(jsonAd, "no");

                        adViewBody.setVisibility(View.VISIBLE);
                        Picasso.with(getContext())
                                .load(img_url)
                                //.fit()
                                .placeholder(R.drawable.img_noimg)
                                .into(imgAd);

                        baseAdHouse.setVisibility(View.VISIBLE);

                        adView.setVisibility(View.GONE);
                        webView.setVisibility(View.GONE);

                        imgAd.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                callApi_logClick_add_log(Constants.LOG_CLICK_MORE_AD_ROW + no);

                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(click_url));
                                startActivity(i);
                            }
                        });
                    } else {
                        adViewBody.setVisibility(View.GONE);
                    }
//                } else if (position % Constants.AD_UNIT == Constants.AD_UNIT_MOD_02) {
//                    adViewBody.setVisibility(View.VISIBLE);
//                    AdRequest adRequest = new AdRequest.Builder().build();
//                    adView.loadAd(adRequest);
//                    adView.setVisibility(View.VISIBLE);
//                    webView.setVisibility(View.GONE);
                } else {
                    adViewBody.setVisibility(View.GONE);
                }

            } else {
                adViewBody.setVisibility(View.GONE);
            }

            txtLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        callApi_board_inc_like(position, bbs_no);

                        // api 호출 후 ui 는 즉각적으로 처리
                        JSONObject jsonRow = _list.getItem(position);

                        int like_cnt = JSONUtil.getInteger(jsonRow, "like_cnt");
                        int my_like_cnt = JSONUtil.getInteger(jsonRow, "my_like_cnt", 0);
                        if (my_like_cnt < 1) {
                            like_cnt++;
                            my_like_cnt = 1;
                        } else {
                            like_cnt--;
                            my_like_cnt = 0;
                        }

                        JSONUtil.puts(jsonRow, "like_cnt", like_cnt);
                        JSONUtil.puts(jsonRow, "my_like_cnt", my_like_cnt);

                        _list.getAdapter().notifyDataSetChanged();
                    } catch (Exception e) {

                    }
                }
            });

            txtHate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callApi_board_inc_hate(position, bbs_no);
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    _position = position;

                    Intent i = new Intent();
                    i.setClass(getContext(), AtvStarInfo.class);
                    i.putExtra(Constants.EXTRAS_JSON_STRING, json.toString());

                    startActivityForResult(i, REQ_BOARD_INFO);
                }
            });
        } else {
            TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
            de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
            TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
            TextView txtBoardCount = (TextView) convertView.findViewById(R.id.txtBoardCount);
            TextView txtBoardCountMy = (TextView) convertView.findViewById(R.id.txtBoardCountMy);
            TextView txtLikeCount = (TextView) convertView.findViewById(R.id.txtLikeCount);
            TextView txtLikeCountMy = (TextView) convertView.findViewById(R.id.txtLikeCountMy);
            TextView txtHateCount = (TextView) convertView.findViewById(R.id.txtHateCount);
            TextView txtHateCountMy = (TextView) convertView.findViewById(R.id.txtHateCountMy);
            View baseLine = (View) convertView.findViewById(R.id.baseLine);

            txtHateCount.setVisibility(View.GONE);
            txtHateCountMy.setVisibility(View.GONE);

            View adViewBody = convertView.findViewById(R.id.adViewBody);
            AdView adView = convertView.findViewById(R.id.adView);

            if (Constants.DISPLAY_AD
                    && Constants.DISPLAY_AD_ROW
                    && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD
                    && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                    && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                adViewBody.setVisibility(View.VISIBLE);
//                AdRequest adRequest = new AdRequest.Builder().build();
//                adView.loadAd(adRequest);
                FrameLayout adViewContainer = convertView.findViewById(R.id.adViewContainer);
                AdView adViewNew = new AdView(getContext());
                adViewNew.setAdUnitId(Constants.KEY_AD_BANNER);
                adViewContainer.addView(adViewNew);

                AdRequest adRequest = new AdRequest.Builder().build();
                adViewNew.setAdSize(getAdSizeInfo());
                adViewNew.loadAd(adRequest);
            } else {
                adViewBody.setVisibility(View.GONE);
            }


            final int artist_no = JSONUtil.getInteger(json, "no");
            final String artist_name = JSONUtil.getString(json, "name");
            String pic1 = JSONUtil.getStringUrl(json, "pic");
            String rank = JSONUtil.getString(json, "rank");

            if (FormatUtil.isNullorEmpty(pic1)) {
                pic1 = JSONUtil.getStringUrl(json, "artist_pic");
            }

            LogUtil.d("position : " + position + ", rank : " + rank);
            LogUtil.d("pic1 : " + pic1);

            //txtRank.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "rank")));
            txtRank.setText(FormatUtil.toPriceFormat(position + 1));
            txtArtist.setText(artist_name);

            txtBoardCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "bbs_cnt", "0")));
            txtBoardCountMy.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_bbs_cnt", "0")));

            txtLikeCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "like_cnt", "0")));
            txtLikeCountMy.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_like_cnt", "0")));

            txtHateCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "hate_cnt", "0")));
            txtHateCountMy.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_hate_cnt", "0")));

            if (!FormatUtil.isNullorEmpty(pic1)) {
                Picasso.with(getContext())
                        .load(pic1)
                        .fit()
                        .placeholder(R.drawable.img_noimg)
                        .into(imgProfile);
                imgProfile.setVisibility(View.VISIBLE);
            } else {
                imgProfile.setVisibility(View.GONE);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _artist_no = artist_no;
                    _txtFilter.setText(artist_name + " X");
                    _txtFilter.setVisibility(View.VISIBLE);
                    callApi_list("recent");
                }
            });
        }

        return convertView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        LogUtil.d("FrgBoard, onActivityResult, requestCode : " + requestCode);
        LogUtil.d("FrgBoard, onActivityResult, resultCode : " + resultCode);
        LogUtil.intent(data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_BOARD_INFO) {
                try {
                    JSONObject jsonNew = JSONUtil.createObject(data.getStringExtra(Constants.EXTRAS_JSON_STRING));

                    JSONObject json = _list.getAdapter().getItem(_position);

                    int is_ilike = JSONUtil.getInteger(jsonNew, "is_ilike");
                    int like_cnt = JSONUtil.getInteger(jsonNew, "like_cnt");
                    int comment_cnt = JSONUtil.getInteger(jsonNew, "comment_cnt");

                    String del_yn = JSONUtil.getString(jsonNew, "del_yn");

                    if ("Y".equals(del_yn)) {
                        _list.getAdapter().remove(json);
                    } else {
                        JSONUtil.puts(json, "is_ilike", is_ilike);
                        JSONUtil.puts(json, "my_like_cnt", is_ilike);
                        JSONUtil.puts(json, "like_cnt", like_cnt);
                        JSONUtil.puts(json, "comment_cnt", comment_cnt);
                    }

                    _list.getAdapter().notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
