package com.code404.mytrot.frg.like;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.vod.AtvVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgLikeArtist extends FrgBase implements GListView.IMakeView {

    private String _gb = "A";


    private View _header = null;
    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;


    @SuppressLint("ValidFragment")
    public FrgLikeArtist() {

    }

    public void setGb(String gb) {
        _gb = gb;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_like_artist);
    }


    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
    }


    @Override
    protected void init() {
        _list.setViewMaker(R.layout.row_like_artist, this);
        _list.setNoData(_baseNoData);
    }


    @Override
    public void refresh() {
        _list.removeAll();
        callApi_memberBookmark_get_all();
    }


    @Override
    protected void configureListener() {

    }

    @Override
    public void onResume() {
        super.onResume();
        callApi_memberBookmark_get_all();
    }

    private void callApi_memberBookmark_get_all() {
        LogUtil.e("==========callApi_memberBookmark_get_all : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberBookmark_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _gb
                , -1
                , Constants.RECORD_SIZE_MAX
                , -1
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list");

                _list.removeAll();
                _list.addItems(jsonList);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_memberBookmark_get_all_for_artist(int artist_no, final String artist_name) {
        LogUtil.e("==========callApi_memberBookmark_get_all_for_artist : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberBookmark_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "V"
                , artist_no
                , Constants.RECORD_SIZE_MAX
                , -1
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list");

                SPUtil.getInstance().setYoutubeJsonArray(getContext(), jsonList);


                Intent i = new Intent();
                i.setClass(getContext(), AtvVod.class);
                //i.putExtra(Constants.EXTRAS_VOD_LIST_JSON_STRING, jsonList.toString());
                i.putExtra(Constants.EXTRAS_VOD_NO, -1);
                i.putExtra(Constants.EXTRAS_TITLE, artist_name);
                startActivity(i);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        LinearLayout baseLeft = (LinearLayout) convertView.findViewById(R.id.baseLeft);
        de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
        TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
        TextView txtLikeCount = (TextView) convertView.findViewById(R.id.txtLikeCount);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        txtArtist.setText(JSONUtil.getString(json, "name"));
        txtLikeCount.setText(FormatUtil.toPriceFormat(JSONUtil.getString(json, "cnt")));

        String artist_pic = JSONUtil.getStringUrl(json, "artist_pic");

        Picasso.with(getContext())
                .load(artist_pic)
                .fit()
                .placeholder(R.drawable.img_noimg)
                .into(imgProfile);


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int artist_no = JSONUtil.getInteger(json, "artist_no");
                String name = JSONUtil.getString(json, "name");
                callApi_memberBookmark_get_all_for_artist(artist_no, name);
            }
        });

        return convertView;
    }
}
