package com.code404.mytrot.frg.heart;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgHeartRank extends FrgBase implements GListView.IMakeView {

    private String _type = "GET";
    private GListView _list = null;

    private View _header = null;
    private TextView _txtUpdateDate = null;
    private TextView _txtSeqWeek = null;
    private TextView _txtSeqMonth = null;
    private TextView _txtSeqAll = null;
    private TextView _txtMyRank = null;

    private int _last_no = 0;
    private String _term = "week";
    private String _myNick = "";
    private String _myUserNoEnc = "";

    @SuppressLint("ValidFragment")
    public FrgHeartRank() {

    }

    public void setType(String type) {
        _type = type;
        _last_no = 0;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_point);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_rank, null);
        _list.addHeaderView(_header);

        _txtUpdateDate = _header.findViewById(R.id.txtUpdateDate);
        _txtSeqWeek = _header.findViewById(R.id.txtSeqWeek);
        _txtSeqMonth = _header.findViewById(R.id.txtSeqMonth);
        _txtSeqAll = _header.findViewById(R.id.txtSeqAll);
        _txtMyRank = _header.findViewById(R.id.txtMyRank);
    }


    @Override
    protected void init() {
        _list.setViewMaker(R.layout.row_point_rank, this);

        _txtMyRank.setText("");

        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
        _myNick = JSONUtil.getString(userInfo, "nick");
        _myUserNoEnc = SPUtil.getInstance().getUserNoEnc(getContext());

        initBtn();
    }

    private void initBtn(){
        JSONObject json = SPUtil.getInstance().getBtnInfo(getContext());

        String btn_week = JSONUtil.getString(json, "btn_week");
        String btn_month = JSONUtil.getString(json, "btn_month");

        if("N".equals(btn_week)) _txtSeqWeek.setVisibility(View.GONE);
        if("N".equals(btn_month)) _txtSeqMonth.setVisibility(View.GONE);

        _txtSeqAll.setVisibility(View.GONE);
    }


    @Override
    public void refresh() {
        if (_list == null) {
            findView();
        }
        _last_no = 0;
        _list.removeAll();
        callApi_heart_ranking_get_all(true);
    }


    @Override
    protected void configureListener() {

        _txtSeqWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _term = "week";
                _last_no = 0;
                _txtSeqWeek.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqMonth.setBackground(null);
                _txtSeqAll.setBackground(null);
                callApi_heart_ranking_get_all(true);
            }
        });
        _txtSeqMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _term = "month";
                _last_no = 0;
                _txtSeqMonth.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqWeek.setBackground(null);
                _txtSeqAll.setBackground(null);
                callApi_heart_ranking_get_all(true);
            }
        });
        _txtSeqAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _term = "all";
                _last_no = 0;
                _txtSeqAll.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqMonth.setBackground(null);
                _txtSeqWeek.setBackground(null);
                callApi_heart_ranking_get_all(true);
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        callApi_heart_ranking_get_all(true);
    }

    private void callApi_heart_ranking_get_all(final boolean isClear) {

        LogUtil.e("==========callApi_heart_ranking_get_all : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;


        call = apiInterface.heart_ranking_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _type
                , Constants.RECORD_SIZE_TOP100
                , _last_no
                , _term
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list");

                if (isClear) {
                    _list.removeAll();
                }

                JSONObject my_ranking = JSONUtil.getJSONObject(data, "my_ranking");

                _last_no = JSONUtil.getInteger(data, "last_no", 0);

                _list.addItems(list);
                //_list.setNoDataVisibility(_list.getCountAll() < 1);

                if(list.length() > 0) {
                    JSONObject jsonRow01 = JSONUtil.getJSONObject(list, 0);
                    String reg_dttm = JSONUtil.getString(jsonRow01, "reg_dttm");
                    reg_dttm = FormatUtil.convertDateAmPm(reg_dttm, "yyyy-MM-dd a hh:mm");
                    _txtUpdateDate.setText("최근 업데이트 : " + reg_dttm);
                }

                setDataBinding(my_ranking);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void setDataBinding(JSONObject json) {
        int rank = JSONUtil.getInteger(json, "rank", 0);

        String txt = "";
        if(rank > 0) {
            txt = _myNick + " <font color='red'><b>" + rank + "위</b></font>";
        } else {
            txt = _myNick + " <font color='red'>등록된 순위가 없습니다.</font>";
        }

        _txtMyRank.setText(Html.fromHtml(txt));
    }


    @Override
    public View makeView(final GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        String userNo = JSONUtil.getString(json, "member_no");
        String nick = JSONUtil.getString(json, "nick").trim();
        String lv = JSONUtil.getString(json, "lv");

        nick = nick + " Lv." + lv;

        if (userNo.equals(_myUserNoEnc)) {
            nick = "<font color='#ff9900'><b>" + nick + "</b></font>";
        }

        txtRank.setText(JSONUtil.getString(json, "rank"));
        txtNick.setText(Html.fromHtml(nick));
        txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getDouble(json, "heart_sum")) + " 개");

        return convertView;
    }


}
