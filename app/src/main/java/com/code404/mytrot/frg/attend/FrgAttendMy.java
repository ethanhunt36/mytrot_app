package com.code404.mytrot.frg.attend;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgAttendMy extends FrgBase implements GListView.IMakeView {

    private GListView _list = null;

    private View _header = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;

    private TextView _txtInfo01 = null;
    private TextView _txtInfo02 = null;

    private int _last_no = -1;

    JSONArray _jsonVodList = null;
    private boolean _is_more_data = true;

    private int _bonus_vote_cnt = -1;

    @SuppressLint("ValidFragment")
    public FrgAttendMy() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_attend);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_attend_my, null);

        _txtInfo01 = _header.findViewById(R.id.txtInfo01);
        _txtInfo02 = _header.findViewById(R.id.txtInfo02);
    }


    @Override
    protected void init() {
        _list.addHeaderView(_header);
        _list.setViewMaker(R.layout.row_attend_my, this);
        _list.setNoData(_baseNoData);
        _txtInfo01.setText("");
        _txtInfo02.setText("");
        _txtNoData.setText("출석한 내역이 없습니다.");

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            _txtInfo02.setVisibility(View.GONE);
        }

        callApi_attendance_listing(true);
    }


    @Override
    public void refresh() {
        _is_more_data = true;
        _last_no = -1;

        if (_list != null) {
            _list.removeAll();
        }

        callApi_attendance_listing(true);
    }


    @Override
    protected void configureListener() {

        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_attendance_listing(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // callApi_board_listing(true);
    }




    private void callApi_attendance_listing(final boolean isClear) {
        LogUtil.e("==========callApi_attendance_listing : start==========");

        if (_is_more_data == false) {
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.attendance_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , "vote_cnt"
                , Constants.RECORD_SIZE
                , _last_no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    _jsonVodList = JSONUtil.getJSONArray(jsonData, "list_data");

                    int point = JSONUtil.getInteger(jsonData, "point", 0);
                    _bonus_vote_cnt = JSONUtil.getInteger(jsonData, "bonus_vote_cnt", 0);

                    int combo = -1;
                    if (_last_no < 1) {
                        JSONObject jsonRow = JSONUtil.getJSONObject(_jsonVodList, 0);
                        combo = JSONUtil.getInteger(jsonRow, "combo", 0);

                        _header.setVisibility(combo < 1 ? View.GONE : View.VISIBLE);

                        if (combo > 0) {
                            _txtInfo01.setText("현재 " + combo + "일 연속 출석 기록중입니다.");
                            _txtInfo02.setText((((combo / 10) + 1) * 10) + "일 연속 출석하시면 투표권 " + _bonus_vote_cnt + "장을 드립니다.");
                        }
                    }

                    _last_no = JSONUtil.getInteger(jsonData, "last_no", -1);

                    if (isClear) {
                        _list.removeAll();
                    }

                    if (_jsonVodList.length() < 1) {
                        _is_more_data = false;
                        return;
                    }

                    _list.addItems(_jsonVodList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);

                    if (isClear) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                _list.smoothScrollToPosition(0);
                            }
                        }, 500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        reg_dttm = reg_dttm.length() > 16 ? reg_dttm.substring(0, 16) : reg_dttm;
        double point = JSONUtil.getDouble(json, "point");
        double vote_cnt = JSONUtil.getDouble(json, "vote_cnt");

        txtTitle.setText(JSONUtil.getString(json, "message"));
        txtDate.setText(reg_dttm);

        if (point > 0)
            txtPoint.setText(FormatUtil.toPriceFormat(point) + " P");
        else
            txtPoint.setText("투표권 " + FormatUtil.toPriceFormat(vote_cnt) + " 장");

        return convertView;
    }
}
