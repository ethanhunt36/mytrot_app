package com.code404.mytrot.frg.more_charge;

import android.annotation.SuppressLint;

import com.code404.mytrot.R;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.util.LogUtil;


@SuppressLint("ValidFragment")
public class FrgChargeNews extends FrgBase  {



    @SuppressLint("ValidFragment")
    public FrgChargeNews() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_charge_news);
    }


    @Override
    protected void findView() {

    }


    @Override
    protected void init() {
        LogUtil.d("FrgVs, init");

    }


    @Override
    public void refresh() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {

    }



    @Override
    public void onResume() {
        super.onResume();

        LogUtil.d("FrgVs, onResume");


    }



}
