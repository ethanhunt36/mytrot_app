package com.code404.mytrot.frg.puzzle;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgPuzzleRank extends FrgBase implements GListView.IMakeView {

    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;

    int _last_no = -1;

    private String _member_no = "";
    private String _my_artist_name = "";
    private String _type = "MY";

    @SuppressLint("ValidFragment")
    public FrgPuzzleRank() {

    }


    public void setType(String type) {
        _type = type;
        _last_no = -1;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_like_artist);
    }


    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);
    }


    @Override
    protected void init() {
        _list.setViewMaker(R.layout.row_puzzle_rank, this);
        _list.setNoData(_baseNoData);
        _txtNoData.setText("내역이 없습니다.");
        _member_no = SPUtil.getInstance().getUserNoEnc(getContext());
        _my_artist_name = SPUtil.getInstance().getUserFavoriteArtistName(getContext());
        callApi();
    }


    @Override
    public void refresh() {
        try {
            if (_list == null) {
                findView();
            }
            _last_no = -1;
            if (_list != null) {
                _list.removeAll();
            }
            callApi();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {

    }

    @Override
    public void onResume() {
        super.onResume();
        callApi();
    }

    private void callApi() {
        if (_type.equals("MY"))
            callApi_slidePuzzle_ranking_my(true);
        else
            callApi_slidePuzzle_ranking(true);
    }

    private void callApi_slidePuzzle_ranking(final boolean isClear) {

        LogUtil.e("==========callApi_slidePuzzle_ranking : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.slidePuzzle_ranking(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , 3
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONArray list = JSONUtil.getJSONArray(json, "data");

                if (isClear) {
                    _list.removeAll();
                }

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_slidePuzzle_ranking_my(final boolean isClear) {

        LogUtil.e("==========callApi_slidePuzzle_ranking : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;

        call = apiInterface.slidePuzzle_ranking1(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , 3
                , 1
                , Constants.RECORD_SIZE_MAX
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONArray list = JSONUtil.getJSONArray(json, "data");

                if (isClear) {
                    _list.removeAll();
                }

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        String ranking = JSONUtil.getString(json, "ranking");
        String artist_name = JSONUtil.getString(json, "artist_name");
        String member_nick = JSONUtil.getString(json, "member_nick");
        String member_lv = JSONUtil.getString(json, "member_lv");
        String duration = JSONUtil.getString(json, "duration");
        String reg_date = JSONUtil.getString(json, "reg_date");

        String member_no = JSONUtil.getString(json, "member_no");
        String nick = artist_name + " | " + member_nick + " Lv." + member_lv;

        if (_type.equals("RANK")) {
            if (_my_artist_name.equals(artist_name)) {
                nick = nick.replace(artist_name, "<font color='#ff9900'><b>" + artist_name + "</b></font>");
            }
            if (member_no.equals(_member_no)) {
                nick = nick.replace(member_nick + " Lv." + member_lv, "<font color='#ff9900'><b>" + member_nick + " Lv." + member_lv + "</b></font>");
            }
        }

        txtNick.setText(Html.fromHtml(nick));
        txtRank.setText(ranking);
        txtPoint.setText(duration + "초");

        return convertView;
    }


}
