package com.code404.mytrot.frg.like;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.vod.AtvVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.VodBindUtil;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.SliderListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgLikeVod extends FrgBase implements GListView.IMakeView {

    private String _gb = "V";
    private JSONArray _jsonVodList = null;

    private VodBindUtil _vodBindUtil = null;
    private View _header = null;
    private SliderListView _sliderList = null;
    private GListView _list = null;
    private View _baseNoData = null;


    @SuppressLint("ValidFragment")
    public FrgLikeVod(){

    }

    public void setGb(String gb){
        _gb = gb;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_like_artist);
    }


    @Override
    protected void findView() {
        _sliderList = (SliderListView) findViewById(R.id.sliderList);
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
    }


    @Override
    protected void init() {
        _vodBindUtil = new VodBindUtil();
        _list.setViewMaker(R.layout.row_vod, this);
        _list.setNoData(_baseNoData);
    }


    @Override
    public void refresh() {
        _list.removeAll();
        callApi_memberBookmark_get_all();
    }


    @Override
    protected void configureListener() {

    }

    @Override
    public void onResume() {
        super.onResume();
        callApi_memberBookmark_get_all();
    }

    private void callApi_memberBookmark_get_all() {
        LogUtil.e("==========callApi_memberBookmark_get_all : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberBookmark_get_all(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _gb
                , -1
                , Constants.RECORD_SIZE_MAX
                , -1
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                _jsonVodList = JSONUtil.getJSONArray(jsonData, "list");

                _list.removeAll();
                _list.addItems(_jsonVodList);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        final int vod_no = JSONUtil.getInteger(json, "no");

        //LogUtil.json(json);
        convertView = _vodBindUtil.setViewEvent(getContext(), json, _jsonVodList, convertView, position, _list.getAdapter(), -1, true);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String name = "보관함 전체 재생";

                SPUtil.getInstance().setYoutubeJsonArray(getContext(), _jsonVodList);

                SPUtil.getInstance().setBoardEnterShort(getContext());

                AdViewUtil.goActionAfterAdView(getActivity(), getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvVod.class);
                        //i.putExtra(Constants.EXTRAS_VOD_LIST_JSON_STRING, _jsonVodList.toString());
                        i.putExtra(Constants.EXTRAS_VOD_NO, vod_no);
                        i.putExtra(Constants.EXTRAS_TITLE, name);
                        startActivity(i);
                    }
                });


            }
        });
        return convertView;
    }

}
