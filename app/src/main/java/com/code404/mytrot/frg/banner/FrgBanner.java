package com.code404.mytrot.frg.banner;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.artist.AtvArtistVod;
import com.code404.mytrot.atv.etc.AtvDonateDetail;
import com.code404.mytrot.atv.etc.AtvEventFundDetail;
import com.code404.mytrot.atv.more.AtvMoreNotice;
import com.code404.mytrot.atv.star_room.AtvStarRoomMain;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.SquareImageView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgBanner extends FrgBase {

    private static String TAG = "FrgBanner";


    private RelativeLayout _base_bg = null;
    private ImageView _img_bg = null;
    private SquareImageView _img_ad = null;
    private TextView _txt_title = null;
    private TextView _txt_desc = null;
    private Button _btn_go = null;

    private JSONObject _json = null;

    @SuppressLint("ValidFragment")
    public FrgBanner() {
        LogUtil.d("FrgBanner create");
    }


    @Override
    protected void setView() {
        LogUtil.d("FrgBanner setView");
        setView(R.layout.frg_banner);
    }


    @Override
    protected void findView() {
        LogUtil.d("FrgBanner findView");
        _base_bg = (RelativeLayout) findViewById(R.id.base_bg);
        _img_bg = (ImageView) findViewById(R.id.img_bg);
        _img_ad = (SquareImageView) findViewById(R.id.img_ad);
        _txt_title = (TextView) findViewById(R.id.txt_title);
        _txt_desc = (TextView) findViewById(R.id.txt_desc);
        _btn_go = (Button) findViewById(R.id.btn_go);
        LogUtil.d("FrgBanner findView2");
    }


    @Override
    protected void init() {
        LogUtil.d("FrgBanner init");

        final int no = JSONUtil.getInteger(_json, "no");
        String path_bg = JSONUtil.getString(_json, "path_bg");
        String path_left = JSONUtil.getString(_json, "path_left");
        String text_1 = JSONUtil.getString(_json, "text_1");
        String text_2 = JSONUtil.getString(_json, "text_2");
        String btn_text = JSONUtil.getString(_json, "btn_text");
        String btn_type = JSONUtil.getString(_json, "btn_type");
        final String target_type = JSONUtil.getString(_json, "target_type");
        final String target_info = JSONUtil.getString(_json, "target_info");

        _img_bg.setVisibility(View.INVISIBLE);
        _img_ad.setVisibility(View.GONE);

        if (!FormatUtil.isNullorEmpty(path_bg) && _img_bg != null) {
            _img_bg.setVisibility(View.VISIBLE);
            Picasso.with(getContext())
                    .load(path_bg)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img_bg);
        }

        if (!FormatUtil.isNullorEmpty(path_left) && _img_ad != null) {

            _img_ad.setBackground(getContext().getDrawable(R.drawable._s_background_rounding));
            _img_ad.setClipToOutline(true);
            _img_ad.setVisibility(View.VISIBLE);
            Picasso.with(getContext())
                    .load(path_left)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(_img_ad);
        }

        _txt_title.setText(text_1);
        _txt_desc.setText(text_2);

        _btn_go.setText(btn_text);
        _btn_go.setBackgroundColor(Color.parseColor(btn_type));


        _img_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go_banner(target_type, target_info, no);
            }
        });
        _btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                go_banner(target_type, target_info, no);
            }
        });
    }


    @Override
    public void refresh() {

    }


    @Override
    protected void configureListener() {


    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public void setData(JSONObject json) {
        _json = json;
    }


    private void go_banner(String target_type, String target_info, int no) {
        callApi_banner_click(no);

        if (target_type.equals("vod")) {
            // 영상
            HashMap<String, String> map = FormatUtil.getQueryStringData(target_info);

            String artist_no = map.get("artist_no");
            String artist_name = map.get("artist_name");
            String artist_board_id = map.get("artist_board_id");

            LogUtil.d(TAG, "map : " + map.toString());

            LogUtil.d(TAG, "artist_no : " + artist_no);
            LogUtil.d(TAG, "artist_name : " + artist_name);
            LogUtil.d(TAG, "artist_board_id : " + artist_board_id);

            Intent i = new Intent();
            i.setClass(getContext(), AtvArtistVod.class);
            i.putExtra(Constants.EXTRAS_ARTIST_NO, -1);
            i.putExtra(Constants.EXTRAS_TITLE, artist_name);
            i.putExtra(Constants.EXTRAS_KEYWORD, artist_name);
            startActivity(i);
        } else if (target_type.equals("notice")) {
            // 공지사항
            HashMap<String, String> map = FormatUtil.getQueryStringData(target_info);

            String notice_no = map.get("no");

            LogUtil.d(TAG, "map : " + map.toString());

            LogUtil.d(TAG, "no : " + no);

            Intent i = new Intent();
            i.setClass(getContext(), AtvMoreNotice.class);
            i.putExtra(Constants.EXTRAS_TYPE, "notice");
            i.putExtra(Constants.EXTRAS_NO, Integer.parseInt(notice_no));
            startActivity(i);
        } else if (target_type.equals("event_fund")) {
            // 광고펀딩 완료
            HashMap<String, String> map = FormatUtil.getQueryStringData(target_info);

            String artist_no = map.get("artist_no");
            String artist_name = map.get("artist_name");
            String subject = map.get("subject");
            String img_url = map.get("img_url");
            String event_fund_no = map.get("event_fund_no");


            LogUtil.d(TAG, "map : " + map.toString());


            Intent i = new Intent();
            i.setClass(getContext(), AtvEventFundDetail.class);
            i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
            i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
            i.putExtra(Constants.EXTRAS_TITLE, subject);
            i.putExtra(Constants.EXTRAS_URL, img_url);
            i.putExtra(Constants.EXTRAS_NO, event_fund_no);
            startActivity(i);
        } else if (target_type.equals("donate")) {
            // 기부 완료
            HashMap<String, String> map = FormatUtil.getQueryStringData(target_info);

            String artist_no = map.get("artist_no");
            String artist_name = map.get("artist_name");
            String donate_nth = map.get("donate_nth");
            String donate_goal_no = map.get("donate_goal_no");
            String img01 = map.get("img01");


            LogUtil.d(TAG, "map : " + map.toString());


            Intent i = new Intent();
            i.setClass(getContext(), AtvDonateDetail.class);
            i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
            i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
            i.putExtra(Constants.EXTRAS_DONATE_NTH, donate_nth);
            i.putExtra(Constants.EXTRAS_DONATE_GOAL_NO, donate_goal_no);
            i.putExtra(Constants.EXTRAS_URL, img01);
            startActivity(i);
        } else if (target_type.equals("web")) {
            Uri url = Uri.parse(target_info);
            Intent intentCall = new Intent(Intent.ACTION_VIEW, url);
            startActivity(intentCall);
        }
    }


    private void callApi_banner_click(int no) {

        LogUtil.e("==========callApi_notice_read : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;


        call = apiInterface.banner_click(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //rogressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
