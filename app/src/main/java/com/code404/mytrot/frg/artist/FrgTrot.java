package com.code404.mytrot.frg.artist;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.artist.AtvArtist;
import com.code404.mytrot.atv.artist.AtvArtistVod;
import com.code404.mytrot.atv.like.AtvLike;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.vod.AtvVod;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.AdViewUtil;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.util.VodBindUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgTrot extends FrgBase implements GListView.IMakeView {

    private GListView _list = null;

    private View _header = null;
    private TextView _txtSeqRecent = null;
    private TextView _txtSeqName = null;
    private TextView _txtSeqPlay = null;
    private TextView _txtSeqScore = null;

    private TextView _txtGotoFavorite = null;

    private String _last_no = "-1";
    private String _sort = "recent";
    private VodBindUtil _vodBindUtil = null;
    private JSONArray _jsonVodList = null;
    private int _artist_no = -1;

    private boolean _is_more_data = true;

    @SuppressLint("ValidFragment")
    public FrgTrot() {
    }


    public void setArtistNo(int artist_no) {
        _artist_no = artist_no;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_artist);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        try {
            // 헤더
            _header = InflateUtil.inflate(getContext(), R.layout.header_trot, null);

            if (_header != null) {
                _txtSeqRecent = _header.findViewById(R.id.txtSeqRecent);
                _txtSeqName = _header.findViewById(R.id.txtSeqName);
                _txtSeqPlay = _header.findViewById(R.id.txtSeqPlay);
                _txtSeqScore = _header.findViewById(R.id.txtSeqScore);

                _txtGotoFavorite = _header.findViewById(R.id.txtGotoFavorite);

                _list.addHeaderView(_header);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void init() {
        _txtSeqScore.setVisibility(View.VISIBLE);

        _list.setViewMaker(R.layout.row_vod, this);
        _vodBindUtil = new VodBindUtil();


        _txtSeqScore.setVisibility(View.GONE);
        _txtGotoFavorite.setVisibility(View.VISIBLE);

        callApi_artistvod_listing(true);

        //initAd_Base();
    }


    @Override
    public void refresh() {

        try {
            _is_more_data = true;
            _last_no = "-1";

            if (_list == null) {
                findView();
            }
            if (_list != null) {
                _list.removeAll();
            }
            callApi_artistvod_listing(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {

        _txtGotoFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdViewUtil.goActionAfterAdView(getActivity(), getContext(), new InterfaceSet.AdFullViewCompleteListener() {
                    @Override
                    public void onAfterAction() {
                        Intent i = new Intent();
                        i.setClass(getContext(), AtvLike.class);
                        startActivity(i);
                    }
                });
            }
        });


        _txtSeqRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "recent";
                _is_more_data = true;
                _last_no = "-1";
                _txtSeqRecent.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqName.setBackground(null);
                _txtSeqPlay.setBackground(null);
                _txtSeqScore.setBackground(null);
                callApi_artistvod_listing(true);
            }
        });

        _txtSeqName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "title";
                _is_more_data = true;
                _last_no = "-1";
                _txtSeqName.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqRecent.setBackground(null);
                _txtSeqPlay.setBackground(null);
                _txtSeqScore.setBackground(null);
                callApi_artistvod_listing(true);
            }
        });

        _txtSeqPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "play";
                _is_more_data = true;
                _last_no = "-1";
                _txtSeqPlay.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqRecent.setBackground(null);
                _txtSeqName.setBackground(null);
                _txtSeqScore.setBackground(null);
                callApi_artistvod_listing(true);
            }
        });
        _txtSeqScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _sort = "editor_score";
                _is_more_data = true;
                _last_no = "-1";
                _txtSeqScore.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqRecent.setBackground(null);
                _txtSeqName.setBackground(null);
                _txtSeqPlay.setBackground(null);
                callApi_artistvod_listing(true);
            }
        });

        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_artistvod_listing(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        //callApi_artistvod_listing();
    }

    private void callApi_artistvod_listing(final boolean isClear) {
        LogUtil.e("==========callApi_artistvod_listing : start==========");

        if (_is_more_data == false) {
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.artistvod_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _artist_no
                , ""
                , _sort
                , _artist_no < 1 ? Constants.RECORD_SIZE : Constants.RECORD_SIZE_MAX
                , _last_no
                , ""
                , ""
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    _jsonVodList = JSONUtil.getJSONArray(jsonData, "list_data");

                    if (isClear && _list != null) {
                        _list.removeAll();
                    }

                    if (_jsonVodList.length() < 1) {
                        _is_more_data = false;
                        return;
                    }

                    _last_no = JSONUtil.getString(jsonData, "last_no", "");

                    if (_list != null) {
                        _list.addItems(_jsonVodList);
                        _list.setNoDataVisibility(_list.getCountAll() < 1);
                    }

                    if (isClear) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(_list!= null) {
                                    _list.smoothScrollToPosition(0);
                                }
                            }
                        }, 500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);
        final int vod_no = JSONUtil.getInteger(json, "no");

        LogUtil.json(json);
        convertView = _vodBindUtil.setViewEvent(getContext(), json, _jsonVodList, convertView, position, _list.getAdapter(), -1, true);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String todayDate = FormatUtil.getCurrentDate();
                String alertDate = SPUtil.getInstance().getYoutubeAlertDate(getContext());

                if (!todayDate.equals(alertDate)) {

                    AlertPop alert = new AlertPop();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            SPUtil.getInstance().setYoutubeAlertDate(getContext(), todayDate);
                            goVod(vod_no);
                        }
                    });
                    alert.showYoutudePolicy(getContext());

                } else {
                    goVod(vod_no);
                }
            }
        });

        return convertView;
    }

    private void goVod(int vod_no) {

        LogUtil.d("vod_no : " + vod_no);
        LogUtil.jsonArray(_jsonVodList);

        SPUtil.getInstance().setYoutubeJsonArray(getContext(), _jsonVodList);

        AdViewUtil.goActionAfterAdView(getActivity(), getContext(), new InterfaceSet.AdFullViewCompleteListener() {
            @Override
            public void onAfterAction() {

                SPUtil.getInstance().setBoardEnterShort(getContext());

                Intent intent = new Intent();
                intent.setClass(getContext(), AtvVod.class);
                //intent.putExtra(Constants.EXTRAS_VOD_LIST_JSON_STRING, _jsonVodList.toString());
                intent.putExtra(Constants.EXTRAS_VOD_NO, vod_no);
                intent.putExtra(Constants.EXTRAS_TITLE, "");


                startActivity(intent);
            }
        });


    }
}
