package com.code404.mytrot.frg.vote;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.SystemClock;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.etc.AtvDonateDetail;
import com.code404.mytrot.atv.etc.AtvEventFundDetail;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgEventFund extends FrgBase implements GListView.IMakeView {


    private GListView _list = null;
    private View _baseNoData = null;
    private View _header = null;


    private TextView _txtDesc = null;
    private Button _btnCheckFavorite = null;
    private View _base_header_tab = null;
    private TextView _txtSeqIng = null;
    private TextView _txtSeqDone = null;

    private TextView _txtSummary = null;

    private String _status = "S";

    private boolean _isHiddenDesc = false;

    private boolean _isAdFullScreen = false;



    @SuppressLint("ValidFragment")
    public FrgEventFund() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_event_fund);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_event_fund, null);
        _list.addHeaderView(_header);

        _txtDesc = _header.findViewById(R.id.txtDesc);
        _btnCheckFavorite = _header.findViewById(R.id.btnCheckFavorite);
        _txtSeqIng = _header.findViewById(R.id.txtSeqIng);
        _txtSeqDone = _header.findViewById(R.id.txtSeqDone);
        _base_header_tab = _header.findViewById(R.id.base_header_tab);

        _txtSummary = _header.findViewById(R.id.txtSummary);
    }


    @Override
    protected void init() {

        _list.setNoData(_baseNoData);
        _list.setViewMaker(R.layout.row_event_fund, this);

        _txtSummary.setText("집계중입니다");

        boolean checked = SPUtil.getInstance().getCheckedFavoriteVote(getContext());

        _btnCheckFavorite.setSelected(checked);

        _isAdFullScreen = SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y");

        callApi_eventFund_get_list(true);
    }


    @Override
    public void refresh() {
        try {
            if (_list != null) {
                _list.removeAll();
            }

            callApi_eventFund_get_list(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {
        _btnCheckFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _btnCheckFavorite.setSelected(!_btnCheckFavorite.isSelected());

                boolean checked = _btnCheckFavorite.isSelected();

                SPUtil.getInstance().setCheckedFavoriteVote(getContext(), checked);

                JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
                int artist_no = JSONUtil.getInteger(json, "artist_no");

                if (artist_no < 1) {
                    new Alert().showAlert(getContext(), getString(R.string.txt_set_artist));
                    return;
                }

                callApi_eventFund_get_list(true);
            }
        });

        _txtSeqIng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _status = "S";
                _txtSeqIng.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqDone.setBackground(null);
                callApi_eventFund_get_list(true);
            }
        });

        _txtSeqDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _status = "E";
                _txtSeqIng.setBackground(null);
                _txtSeqDone.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                callApi_eventFund_get_list(true);
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        LinearLayout baseLeft = (LinearLayout) convertView.findViewById(R.id.baseLeft);
        CircleImageView imgProfile = convertView.findViewById(R.id.imgProfile);
        TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
        LinearLayout baseRight = (LinearLayout) convertView.findViewById(R.id.baseRight);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtTarget = (TextView) convertView.findViewById(R.id.txtTarget);
        TextView txtPer = (TextView) convertView.findViewById(R.id.txtPer);
        TextView txtCurrent = (TextView) convertView.findViewById(R.id.txtCurrent);
        TextView txtMembers = (TextView) convertView.findViewById(R.id.txtMembers);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        final String artist_name = JSONUtil.getString(json, "artist_name");
        String subject_01 = JSONUtil.getString(json, "subject");


        if(_isAdFullScreen == false) {
            subject_01 = subject_01.replace("광고 게시", "지하철 광고");
        }

        final String subject = subject_01;

        final String artist_pic = JSONUtil.getStringUrl(json, "artist_pic");
        final String img_url = JSONUtil.getStringUrl(json, "img_url");
        final int event_fund_no = JSONUtil.getInteger(json, "no", 0);
        final int artist_no = JSONUtil.getInteger(json, "artist_no", 0);
        int heart_goal = JSONUtil.getInteger(json, "heart_goal", 0);
        int heart_cur = JSONUtil.getInteger(json, "heart_cur", 0);
        int participant_cnt = JSONUtil.getInteger(json, "participant_cnt", 0);
        double per = heart_cur > 0 ? (heart_cur * 1.0 / heart_goal) * 100.0 : 0;

        final String state = JSONUtil.getString(json, "state");

        if (!FormatUtil.isNullorEmpty(artist_pic)) {
            Picasso.with(getContext())
                    .load(artist_pic)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

        txtArtist.setText(artist_name);

        txtTitle.setText(subject);
        txtTarget.setText(String.format("하트 %s개 목표", FormatUtil.toPriceFormat(heart_goal)));
        txtPer.setText(String.format("%.2f", per) + "%");
        txtCurrent.setText(String.format("현재 하트 %s개", FormatUtil.toPriceFormat(heart_cur)));
        txtMembers.setText(String.format("%s명 참여", FormatUtil.toPriceFormat(participant_cnt)));
        txtMembers.setVisibility(View.GONE);
        txtPer.setVisibility(View.VISIBLE);

        if ("E".equals(state)) {
            // 완료된 경우.
            int year = FormatUtil.getCurrentYear();

            String ad_start_dttm = JSONUtil.getString(json, "ad_start_dttm");
            String ad_end_dttm = JSONUtil.getString(json, "ad_end_dttm");

            if (!FormatUtil.isNullorEmpty(ad_start_dttm) && !FormatUtil.isNullorEmpty(ad_end_dttm)) {

                int reg_year = Integer.parseInt(ad_start_dttm.substring(0, 4));

                if (year == reg_year) {
                    ad_start_dttm = ad_start_dttm.substring(5, 10).replace("-", ".");
                    ad_end_dttm = ad_end_dttm.substring(5, 10).replace("-", ".");
                } else {
                    ad_start_dttm = ad_start_dttm.substring(0, 10).replace("-", ".");
                    ad_end_dttm = ad_end_dttm.substring(5, 10).replace("-", ".");
                }

                txtTarget.setText(String.format("기간 : %s ~ %s", ad_start_dttm, ad_end_dttm));

            } else {
                txtTarget.setText("시작일 협의중");
            }

            txtCurrent.setText(String.format("하트 %s개 달성", FormatUtil.toPriceFormat(heart_cur)));
            txtPer.setVisibility(View.GONE);
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("E".equals(state)) {
                    Intent i = new Intent();
                    i.setClass(getContext(), AtvEventFundDetail.class);
                    i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no);
                    i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
                    i.putExtra(Constants.EXTRAS_TITLE, subject);
                    i.putExtra(Constants.EXTRAS_URL, img_url);
                    i.putExtra(Constants.EXTRAS_NO, event_fund_no);
                    startActivity(i);
                } else if ("S".equals(state)) {
                    AlertPop alert = new AlertPop();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            callApi_eventFund_get_list(true);
                        }
                    });
                    alert.showEventFund(getContext(), event_fund_no, artist_no, subject, artist_pic, heart_goal, heart_cur);
                }
            }
        });

        return convertView;
    }

    private int _last_no = 0;

    private void callApi_eventFund_get_list(final boolean isClear) {

        if (isClear) {
            _last_no = 0;
        }

        LogUtil.e("==========callApi_eventFund_get_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.eventFund_get_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _status
                , Constants.RECORD_SIZE_MAX
                , _last_no
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                    JSONObject summaryData = JSONUtil.getJSONObject(jsonData, "data_summary");

                    if (isClear) {
                        _list.removeAll();
                    }
                    if (jsonList.length() > 0) {
                        _last_no = JSONUtil.getInteger(jsonData, "last_no");
                    }

                    String is_view_header = JSONUtil.getString(jsonData, "is_view_header");

                    _base_header_tab.setVisibility("Y".equals(is_view_header) ? View.VISIBLE : View.GONE);

                    boolean checked = _btnCheckFavorite.isSelected();

                    JSONObject jsonUser = SPUtil.getInstance().getUserInfo(getContext());
                    int artist_no = JSONUtil.getInteger(jsonUser, "artist_no", 0);

                    boolean isConstainsArtist = false;
                    if (artist_no > 0) {
                        // 최애 맨위로.
                        List<JSONObject> list = new ArrayList<JSONObject>();

                        for (int i = 0; i < jsonList.length(); i++) {
                            JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                            int no = JSONUtil.getInteger(j, "artist_no");
                            list.add(j);

                            if (artist_no == no) {
                                isConstainsArtist = true;
                                _isHiddenDesc = true;
                            }
                            if (checked && i > 0 && artist_no == no) {
                                //list.remove(i);
                                list.add(0, j);
                            }
                        }

                        jsonList = new JSONArray();

                        for (int i = 0; i < list.size(); i++) {
                            jsonList.put(i, list.get(i));
                        }
                    }

                    _txtDesc.setVisibility(isConstainsArtist ? View.GONE : View.VISIBLE);

                    int counts = JSONUtil.getInteger(summaryData, "counts");
                    int hearts = JSONUtil.getInteger(summaryData, "hearts");

                    _txtSummary.setText(String.format("%d회 (%s하트)", counts, FormatUtil.toPriceFormat(hearts)));

                    if (_isHiddenDesc) {
                        _txtDesc.setVisibility(View.GONE);
                    }

                    _list.addItems(jsonList);
                    //_list.setNoDataVisibility(jsonList.length() < 1);
                    //_baseNoData.setVisibility(jsonList.length() < 1 ? View.VISIBLE : View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }
}

