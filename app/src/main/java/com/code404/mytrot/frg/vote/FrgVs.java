package com.code404.mytrot.frg.vote;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.etc.AtvDonateDetail;
import com.code404.mytrot.atv.etc.AtvLuckyNumber;
import com.code404.mytrot.atv.event.AtvEventPP;
import com.code404.mytrot.atv.more.AtvMore;
import com.code404.mytrot.atv.star.AtvVsInfo;
import com.code404.mytrot.atv.web.AtvWeb;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertAction;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgVs extends FrgBase implements GListView.IMakeView {

    private View _header = null;
    private TextView _txtSeq1 = null;
    private TextView _txtSeq2 = null;
    private TextView _txtSeq3 = null;
    private TextView _txtSeq4 = null;

    private GListView _list = null;
    private boolean _isLoading = false;
    private int _last_no = -1;
    private String _sort = "";

    @SuppressLint("ValidFragment")
    public FrgVs() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_vs);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_vs, null);
        _list.addHeaderView(_header);

        _txtSeq1 = _header.findViewById(R.id.txtSeq1);
        _txtSeq2 = _header.findViewById(R.id.txtSeq2);
        _txtSeq3 = _header.findViewById(R.id.txtSeq3);
        _txtSeq4 = _header.findViewById(R.id.txtSeq4);

        _list.setViewMaker(R.layout.row_vs, this);
    }


    @Override
    protected void init() {
        LogUtil.d("FrgVs, init");

    }


    @Override
    public void refresh() {
        try {
            LogUtil.d("FrgVs, refresh");
            callApi_vs_listing(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {
        _txtSeq1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnTab("");
                callApi_vs_listing(true);
            }
        });
        _txtSeq2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnTab("vs_pick_cnt");
                callApi_vs_listing(true);
            }
        });
        _txtSeq3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnTab("comment");
                callApi_vs_listing(true);
            }
        });
        _txtSeq4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setBtnTab("read_cnt");
                callApi_vs_listing(true);
            }
        });
    }


    private void setBtnTab(String sort) {
        _sort = sort;

        _txtSeq1.setBackground(null);
        _txtSeq2.setBackground(null);
        _txtSeq3.setBackground(null);
        _txtSeq4.setBackground(null);

        if (_sort.equals("")) {
            _txtSeq1.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        } else if (_sort.equals("vs_pick_cnt")) {
            _txtSeq2.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        } else if (_sort.equals("comment")) {
            _txtSeq3.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        }else if (_sort.equals("read_cnt")) {
            _txtSeq4.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
        }

    }



    @Override
    public void onResume() {
        super.onResume();

        LogUtil.d("FrgVs, onResume");

        callApi_vs_listing(true);
    }


    private void callApi_vs_listing(final boolean isClear) {

        if (_isLoading == true) return;

        _isLoading = true;

        if (isClear) {
            _last_no = -1;
        }

        LogUtil.e("==========callApi_vs_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vs_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , _last_no
                , _sort
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {
                    _isLoading = false;
                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject data = JSONUtil.getJSONObject(json, "data");

                    JSONArray jsonList = JSONUtil.getJSONArray(data, "list_data");


                    if (isClear && _list != null) {
                        _list.removeAll();
                    }

                    _last_no = JSONUtil.getInteger(data, "last_no");

                    _list.addItems(jsonList);
                    _list.setNoDataVisibility(_list.getCountAll() < 1);

                    LogUtil.d("jsonList, size : " + jsonList.length());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);

                _isLoading = false;
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        final int no = JSONUtil.getInteger(json, "no");

        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        ImageView imgYoutubeA = (ImageView) convertView.findViewById(R.id.imgYoutubeA);
        ImageView imgYoutubeB = (ImageView) convertView.findViewById(R.id.imgYoutubeB);
        TextView txtDesc01 = (TextView) convertView.findViewById(R.id.txtDesc01);
        TextView txtDesc02 = (TextView) convertView.findViewById(R.id.txtDesc02);

        txtTitle.setText(JSONUtil.getString(json, "title"));


        JSONArray items = JSONUtil.getJSONArray(json, "item");

        JSONObject itemA = JSONUtil.getJSONObject(items, 0);
        JSONObject itemB = JSONUtil.getJSONObject(items, 1);

        String imgA = JSONUtil.getStringUrl(itemA, "img01");
        String imgB = JSONUtil.getStringUrl(itemB, "img01");

        int vs_pick_cnt = JSONUtil.getInteger(json, "vs_pick_cnt");
        int comment = JSONUtil.getInteger(json, "comment_cnt");
        int read_cnt = JSONUtil.getInteger(json, "read_cnt");
        String reg_dttm = JSONUtil.getString(json, "reg_dttm");

        txtDesc01.setText(String.format("투표 %s명 | 댓글 %s | 조회수 %s", FormatUtil.toPriceFormat(vs_pick_cnt), FormatUtil.toPriceFormat(comment), FormatUtil.toPriceFormat(read_cnt)));
        txtDesc02.setText(FormatUtil.getReplyDateFormat(reg_dttm));

        if (!FormatUtil.isNullorEmpty(imgA)) {
            Picasso.with(getContext())
                    .load(imgA)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgYoutubeA);
        } else {
            Picasso.with(getContext()).load(R.drawable.img_noimg).fit().into(imgYoutubeA);
        }

        if (!FormatUtil.isNullorEmpty(imgB)) {
            Picasso.with(getContext())
                    .load(imgB)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgYoutubeB);
        } else {
            Picasso.with(getContext()).load(R.drawable.img_noimg).fit().into(imgYoutubeB);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setClass(getContext(), AtvVsInfo.class);
                i.putExtra(Constants.EXTRAS_BOARD_NO, no);
                startActivity(i);
            }
        });

        return convertView;
    }


}
