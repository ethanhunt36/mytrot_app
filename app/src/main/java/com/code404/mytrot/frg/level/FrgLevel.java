package com.code404.mytrot.frg.level;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgLevel extends FrgBase implements GListView.IMakeView {

    private String _type = "GET";

    private View _header = null;

    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;


    private TextView _txtMyRank = null;

    private String _myUserNoEnc = "";
    private String _myNick = "";

    @SuppressLint("ValidFragment")
    public FrgLevel() {

    }


    @Override
    protected void setView() {
        setView(R.layout.frg_point);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_level, null);
        _list.addHeaderView(_header);

        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);

        _txtMyRank = _header.findViewById(R.id.txtMyRank);
    }


    @Override
    protected void init() {
        _list.setViewMaker(R.layout.row_point_rank, this);
        _list.setNoData(_baseNoData);
        _txtNoData.setText("내역이 없습니다.");

        _txtMyRank.setText("");

        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
        _myNick = JSONUtil.getString(userInfo, "nick");
        _myUserNoEnc = SPUtil.getInstance().getUserNoEnc(getContext());

        setDataBinding(null);
    }


    @Override
    public void refresh() {
        if (_list == null) {
            findView();
        }
        _list.removeAll();
        callApi_member_get_level_top_n(true);
    }


    @Override
    protected void configureListener() {

    }

    @Override
    public void onResume() {
        super.onResume();
        callApi_member_get_level_top_n(true);
    }


    private void setDataBinding(JSONObject json) {
        int rank = JSONUtil.getInteger(json, "rank", 0);

        String txt = "";
        if (rank > 0) {
            txt = _myNick + " <font color='red'><b>" + FormatUtil.toPriceFormat(rank) + "위</b></font>";
        } else {
            txt = _myNick + " <font color='red'>등록된 순위가 없습니다.</font>";
        }

        _txtMyRank.setText(Html.fromHtml(txt));
    }


    private void callApi_member_get_level_top_n(final boolean isClear) {

        LogUtil.e("==========callApi_memberPointHistory_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;


        call = apiInterface.member_get_level_top_n(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list");

                JSONObject my_ranking = JSONUtil.getJSONObject(data, "my_ranking");

                if (isClear) {
                    _list.removeAll();
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);

                setDataBinding(my_ranking);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(final GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        TextView txtRank = (TextView) convertView.findViewById(R.id.txtRank);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);

        String userNo = JSONUtil.getString(json, "member_no");
        String nick = JSONUtil.getString(json, "nick").trim();
        String lv = JSONUtil.getString(json, "lv").trim();

        nick = String.format("%s Lv.%s", nick, lv);

        if (userNo.equals(_myUserNoEnc)) {
            nick = String.format("<font color='#ff9900'><b>%s Lv.%s</b></font>", nick, lv);
        }

        txtRank.setText(JSONUtil.getString(json, "rank"));
        txtNick.setText(Html.fromHtml(nick));
        txtPoint.setText(FormatUtil.toPriceFormat(JSONUtil.getDouble(json, "lv_pnt")) + "점");

        return convertView;
    }


}
