package com.code404.mytrot.frg.more_charge;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.star.AtvVsInfo;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgChargeAd extends FrgBase  {



    private com.code404.mytrot.widget.ItemMyVph _item_my_vph = null;
    private RelativeLayout _baseAd = null;
    private ImageView _img_ad = null;
    private TextView _txtAdDesc01 = null;
    private TextView _txtTicket = null;
    private TextView _txtAdDesc02 = null;
    private LinearLayout _baseChargeMode = null;
    private Button _btnChargeVote = null;
    private Button _btnChargeBalance = null;
    private Button _btnChargeHeart = null;
    private TextView _txtDescChargeMode = null;

    private Handler _handlerTimer;


    @SuppressLint("ValidFragment")
    public FrgChargeAd() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_charge_ad);
    }


    @Override
    protected void findView() {



        _item_my_vph = (com.code404.mytrot.widget.ItemMyVph) findViewById(R.id.item_my_vph);
        _baseAd = (RelativeLayout) findViewById(R.id.baseAd);
        _img_ad = (ImageView) findViewById(R.id.img_ad);
        _txtAdDesc01 = (TextView) findViewById(R.id.txtAdDesc01);
        _txtTicket = (TextView) findViewById(R.id.txtTicket);
        _txtAdDesc02 = (TextView) findViewById(R.id.txtAdDesc02);
        _baseChargeMode = (LinearLayout) findViewById(R.id.baseChargeMode);
        _btnChargeVote = (Button) findViewById(R.id.btnChargeVote);
        _btnChargeBalance = (Button) findViewById(R.id.btnChargeBalance);
        _btnChargeHeart = (Button) findViewById(R.id.btnChargeHeart);
        _txtDescChargeMode = (TextView) findViewById(R.id.txtDescChargeMode);

    }


    @Override
    protected void init() {
        LogUtil.d("FrgVs, init");


    }


    @Override
    public void refresh() {
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @Override
    protected void configureListener() {

    }



    @Override
    public void onResume() {
        super.onResume();

        LogUtil.d("FrgVs, onResume");


//        if (_handlerTimer == null) {
//            _handlerTimer = new Handler() {
//                @Override
//                public void handleMessage(Message msg) {
//                    LogUtil.d("handleMessage");
//                    JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
//                    setMyVoteCountPoint(userInfo);
//
//                    _item_my_vph.setMyVoteCountPoint();
//
//                    setActiveAdTextDisplay(Constants.enum_ad.none, true);
//                    //_handlerTimer.sendEmptyMessageDelayed(0, _timerInterval);
//                }
//            };
//        }
//        if (_handlerTimer != null) {
//            _handlerTimer.sendEmptyMessageDelayed(0, _timerInterval);
//        }

    }





}
