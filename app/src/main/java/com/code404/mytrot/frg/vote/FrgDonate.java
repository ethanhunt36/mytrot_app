package com.code404.mytrot.frg.vote;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.SystemClock;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.etc.AtvDonateDetail;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.AlertPop;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgDonate extends FrgBase implements GListView.IMakeView {


    private GListView _list = null;

    private View _header = null;
    private TextView _txtDonateSummary = null;
    private TextView _txtTicket = null;

    private TextView _txtSeqIng = null;
    private TextView _txtSeqDone = null;
    private TextView _txtSeqST = null;
    private Button _btnCheckFavorite = null;

    private int _tabIndex = 0;

    // 리워드 광고
    private RewardedAd _rewardedAd;


    private int _ad_try_count = 0;


    // 현재 보유한 투표권 개수
    private int _vote_cnt = 0;

    @SuppressLint("ValidFragment")
    public FrgDonate() {
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_artist);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);

        // 헤더
        _header = InflateUtil.inflate(getContext(), R.layout.header_donate, null);
        _list.addHeaderView(_header);

        _txtTicket = _header.findViewById(R.id.txtTicket);
        _txtSeqIng = _header.findViewById(R.id.txtSeqIng);
        _txtSeqDone = _header.findViewById(R.id.txtSeqDone);
        _txtSeqST = _header.findViewById(R.id.txtSeqST);
        _txtDonateSummary = _header.findViewById(R.id.txtDonateSummary);


        _btnCheckFavorite = _header.findViewById(R.id.btnCheckFavorite);

    }


    @Override
    protected void init() {

        _list.setViewMaker(R.layout.row_donate, this);

        _txtDonateSummary.setText("");

        boolean checked = SPUtil.getInstance().getCheckedFavoriteDonate(getContext());

        _btnCheckFavorite.setSelected(checked);

        setMyVoteCountPoint();
        callApi_donate_ing_list(true, false);
    }

    private void setMyVoteCountPoint() {
        try {

            if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
                _txtTicket.setVisibility(View.GONE);
                return;
            }

            JSONObject userJson = SPUtil.getInstance().getUserInfo(getContext());

            int vote_cnt = JSONUtil.getInteger(userJson, "vote_cnt", 0);
            double point = JSONUtil.getDouble(userJson, "point", 0);

            if (_txtTicket == null) {
                _txtTicket = _header.findViewById(R.id.txtTicket);
            }

            _txtTicket.setText("투표권 : " + FormatUtil.toPriceFormat(vote_cnt) + "장 | 포인트 : " + FormatUtil.toPriceFormat(point) + " P");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDonateSummary(JSONObject jsonDone) {
        String donate_price = JSONUtil.getString(jsonDone, "donate_price");
        int donate_count = JSONUtil.getInteger(jsonDone, "donate_count");
        _txtDonateSummary.setText(String.format("%s원 (%d회)", FormatUtil.toPriceFormat(donate_price), donate_count));
    }


    @Override
    public void refresh() {
        try {
            if (_list != null) {
                _list.removeAll();
            }
            setMyVoteCountPoint();

            _tabIndex = 0;
            callApi_list(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void configureListener() {

        _btnCheckFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _btnCheckFavorite.setSelected(!_btnCheckFavorite.isSelected());

                boolean checked = _btnCheckFavorite.isSelected();

                SPUtil.getInstance().setCheckedFavoriteDonate(getContext(), checked);

                JSONObject json = SPUtil.getInstance().getUserInfo(getContext());
                int artist_no = JSONUtil.getInteger(json, "artist_no");

                if (artist_no < 1) {
                    new Alert().showAlert(getContext(), getString(R.string.txt_set_artist));
                    return;
                }

                callApi_list(true);
            }
        });

        _txtSeqIng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _tabIndex = 0;
                callApi_list(true);
            }
        });

        _txtSeqDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _tabIndex = 1;
                callApi_list(true);
            }
        });

        _txtSeqST.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _tabIndex = 2;
                callApi_list(true);
            }
        });

    }


    private void callApi_list(boolean isShowProgress) {
        _txtSeqIng.setBackground(null);
        _txtSeqDone.setBackground(null);
        _txtSeqST.setBackground(null);

        switch (_tabIndex) {
            case 0:
                _btnCheckFavorite.setVisibility(View.VISIBLE);
                _txtSeqIng.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                callApi_donate_ing_list(true, isShowProgress);
                break;
            case 1:
                _btnCheckFavorite.setVisibility(View.GONE);
                _txtSeqDone.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                callApi_donate_done_list(true, isShowProgress);
                break;
            case 2:
                _btnCheckFavorite.setVisibility(View.GONE);
                _txtSeqST.setBackground(getContext().getDrawable(R.drawable.bg_typebox_ent));
                callApi_donate_done_st_list(true, isShowProgress);
                break;
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }


    //중복 클릭 방지 시간 설정 ( 해당 시간 이후에 다시 클릭 가능 )
    private static final long MIN_CLICK_INTERVAL = 600;
    private long mLastClickTime = 0;

    private void callApi_donate_ing_list(final boolean isClear, boolean isShowProgress) {

        LogUtil.e("==========callApi_donate_ing_list : start==========");

        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        LogUtil.d("_txtAd, callApi_donate_ing_list, elapsedTime : " + elapsedTime);

        // 중복클릭 아닌 경우
        if (elapsedTime < MIN_CLICK_INTERVAL) {
            LogUtil.e("_txtAd, callApi_donate_ing_list, 이벤트 중복 발생");
            return;
        }


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.donate_ing_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , -1
        );

        if (isShowProgress) _dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(_dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                    if (isClear) {
                        _list.removeAll();
                    }

                    JSONObject user_info = JSONUtil.getJSONObject(jsonData, "user_info");
                    JSONObject done_data = JSONUtil.getJSONObject(jsonData, "done_data");

                    if (user_info != null) {
                        SPUtil.getInstance().setUserInfo(getContext(), user_info);
                        setMyVoteCountPoint();
                    }

                    setDonateSummary(done_data);

                    boolean checked = _btnCheckFavorite.isSelected();
                    int artist_no = JSONUtil.getInteger(user_info, "artist_no");

                    if (checked && artist_no > 0) {
                        // 최애 맨위로.
                        List<JSONObject> list = new ArrayList<JSONObject>();

                        for (int i = 0; i < jsonList.length(); i++) {
                            JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                            int no = JSONUtil.getInteger(j, "no");
                            list.add(j);

                            if (i > 0 && artist_no == no) {
                                //list.remove(i);
                                list.add(0, j);
                            }
                        }

                        jsonList = new JSONArray();

                        for (int i = 0; i < list.size(); i++) {
                            jsonList.put(i, list.get(i));
                        }
                    }

                    _list.addItems(jsonList);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(_dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    Dialog _dialog = null;

    private void callApi_donate_done_list(final boolean isClear, boolean isShowProgress) {

        LogUtil.e("==========callApi_donate_done_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.donate_done_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , Constants.RECORD_SIZE_MAX
                , -1
        );

        if (isShowProgress) _dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(_dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                    JSONObject done_data = JSONUtil.getJSONObject(jsonData, "done_data");

                    setDonateSummary(done_data);

                    if (isClear) {
                        _list.removeAll();
                        _list.addItems(jsonList);
                    }

                    int donate = 0;

                    for (int i = 0; i < jsonList.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                        int donate_goal_point = JSONUtil.getInteger(j, "donate_goal_point", 0);
                        donate += donate_goal_point;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(_dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_donate_done_st_list(final boolean isClear, boolean isShowProgress) {

        LogUtil.e("==========callApi_donate_done_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.donate_done_st_list(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        if (isShowProgress) _dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(_dialog);

                try {

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                    JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");

                    JSONObject done_data = JSONUtil.getJSONObject(jsonData, "done_data");

                    setDonateSummary(done_data);

                    if (isClear) {
                        _list.removeAll();
                        _list.addItems(jsonList);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(_dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(GListView.GListAdapter adapter, final int position, View convertView, ViewGroup parent) {

        final JSONObject json = adapter.getItem(position);

        LinearLayout baseLeft = (LinearLayout) convertView.findViewById(R.id.baseLeft);
        de.hdodenhof.circleimageview.CircleImageView imgProfile = (de.hdodenhof.circleimageview.CircleImageView) convertView.findViewById(R.id.imgProfile);
        TextView txtArtist = (TextView) convertView.findViewById(R.id.txtArtist);
        TextView txtVoteCount = (TextView) convertView.findViewById(R.id.txtVoteCount);
        TextView txtMyCount = (TextView) convertView.findViewById(R.id.txtMyCount);
        TextView txtDonateTarget = (TextView) convertView.findViewById(R.id.txtDonateTarget);
        TextView txtDonatePer = (TextView) convertView.findViewById(R.id.txtDonatePer);
        View baseDonate = convertView.findViewById(R.id.baseDonate);

        View baseDonateDate = convertView.findViewById(R.id.baseDonateDate);
        TextView txtDonateDateTitle = (TextView) convertView.findViewById(R.id.txtDonateDateTitle);
        TextView txtDonateDate = (TextView) convertView.findViewById(R.id.txtDonateDate);

        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        final String artist_name = JSONUtil.getString(json, "name");
        final String pic1 = JSONUtil.getStringUrl(json, "pic");
        final String img01 = JSONUtil.getStringUrl(json, "img01");
        int artist_no = JSONUtil.getInteger(json, "no", 0);
        final int counts = JSONUtil.getInteger(json, "counts", 0);
        final int donate_goal_no = JSONUtil.getInteger(json, "no", 0);

        final int donate_nth = JSONUtil.getInteger(json, "donate_nth", 0);
        final int donate_cur_point = JSONUtil.getInteger(json, "donate_cur_point", 0);
        int donate_my_point = JSONUtil.getInteger(json, "donate_my_point", 0);
        final int donate_goal_point = JSONUtil.getInteger(json, "donate_goal_point", 0);

        if (_tabIndex == 1) {
            artist_no = JSONUtil.getInteger(json, "artist_no", 0);
        }

        final int artist_no_final = artist_no;

        LogUtil.d("pic1 : " + pic1);

        baseDonate.setVisibility(_tabIndex == 1 || _tabIndex == 2 ? View.GONE : View.VISIBLE);
        baseDonateDate.setVisibility(_tabIndex == 0 ? View.GONE : View.VISIBLE);

        txtArtist.setText(artist_name);
        txtVoteCount.setText(FormatUtil.toPriceFormat(donate_cur_point) + " P");
        txtMyCount.setText(FormatUtil.toPriceFormat(donate_my_point) + " P");
        txtDonateTarget.setText(Html.fromHtml(donate_nth + "차 목표 <font color='#7d4fd9'><b>" + FormatUtil.toPriceFormat(donate_goal_point) + " P</b></font>"));

        if (_tabIndex == 1 || _tabIndex == 2) {
            txtDonateDateTitle.setText(donate_nth + "차 모금 기간");

            String donate_reg_dttm = JSONUtil.getString(json, "donate_reg_dttm");
            if (!FormatUtil.isNullorEmpty(donate_reg_dttm) && donate_reg_dttm.length() >= 10)
                donate_reg_dttm = donate_reg_dttm.substring(0, 10);

            String donate_end_dttm = JSONUtil.getString(json, "donate_end_dttm");
            if (!FormatUtil.isNullorEmpty(donate_end_dttm) && donate_end_dttm.length() >= 10)
                donate_end_dttm = donate_end_dttm.substring(0, 10);

            txtDonateDate.setText(donate_reg_dttm + " ~ " + donate_end_dttm);

            if (_tabIndex == 1) {
                txtDonateDateTitle.setText(donate_nth + "차 모금 기간");
            } else if (_tabIndex == 2) {
                txtDonateDateTitle.setText(Html.fromHtml(String.format("총 <font color='#7d4fd9'><b>%d회</b></font> 기부 완료", counts)));
            }
        }

        double per = donate_cur_point > 0 ? (donate_cur_point * 1.0 / donate_goal_point) * 100.0 : 0;

        txtDonatePer.setText(String.format("%.2f", per) + "%");

        if (!FormatUtil.isNullorEmpty(pic1)) {
            Picasso.with(getContext())
                    .load(pic1)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);
        WebView webView = convertView.findViewById(R.id.webView);

        if (Constants.DISPLAY_AD
                && Constants.DISPLAY_AD_ROW
                && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD
                && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
        ) {
            adViewBody.setVisibility(View.VISIBLE);


            AdRequest adRequest = new AdRequest.Builder().build();
            adView.loadAd(adRequest);
            adView.setVisibility(View.VISIBLE);
            webView.setVisibility(View.GONE);


        } else {
            adViewBody.setVisibility(View.GONE);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_tabIndex == 0) {

                    String donate_active_yn = JSONUtil.getString(json, "donate_active_yn");

                    if (!donate_active_yn.equals("Y")) {
                        new Alert().showAlert(getContext(), "기부펀딩 모금이 종료되었습니다.");
                        return;
                    }

                    AlertPop alert = new AlertPop();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            callApi_donate_ing_list(true, true);
                        }
                    });
                    alert.showDonate(getContext(), artist_no_final, artist_name, pic1, donate_nth, donate_goal_point, donate_cur_point);

                } else if (_tabIndex == 1) {

                    Intent i = new Intent();
                    i.setClass(getContext(), AtvDonateDetail.class);
                    i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no_final);
                    i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
                    i.putExtra(Constants.EXTRAS_DONATE_NTH, donate_nth);
                    i.putExtra(Constants.EXTRAS_DONATE_GOAL_NO, donate_goal_no);
                    i.putExtra(Constants.EXTRAS_URL, img01);
                    startActivity(i);

                } else if (_tabIndex == 2) {

                    Intent i = new Intent();
                    i.setClass(getContext(), AtvDonateDetail.class);
                    i.putExtra(Constants.EXTRAS_ARTIST_NO, artist_no_final);
                    i.putExtra(Constants.EXTRAS_ARTIST_NAME, artist_name);
                    i.putExtra(Constants.EXTRAS_DONATE_NTH, counts);
                    i.putExtra(Constants.EXTRAS_DONATE_GOAL_NO, -1);
                    i.putExtra(Constants.EXTRAS_URL, img01);
                    startActivity(i);

                }


            }
        });

        return convertView;
    }
}

