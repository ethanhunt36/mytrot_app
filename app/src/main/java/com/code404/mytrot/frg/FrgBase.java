package com.code404.mytrot.frg;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.fragment.app.Fragment;

import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.InflateUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.SlideTabView;
import com.google.android.gms.ads.AdSize;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;


public abstract class FrgBase extends Fragment {
    protected View mBase = null;

    protected int[] mDeviceSize = null;


    private SlideTabView _slideTabView = null;

    private int _index = -1;

//    protected AdFullManager _adFullManager = null;
//    protected static AdFullOrgManager _adFullOrgManager = null;
    protected boolean _isFirstInit = true;

//    public void setAdFullManager(AdFullManager adFullManager) {
//        LogUtil.d("setAdFullManager : " + (adFullManager == null ? "null" : "ok"));
//        _adFullManager = adFullManager;
//    }
//    public void setAdFullOrgManager(AdFullOrgManager adFullOrgManager) {
//        LogUtil.d("setAdFullOrgManager : " + (adFullOrgManager == null ? "null" : "ok"));
//        _adFullOrgManager = adFullOrgManager;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        LogUtil.d("FrgBase, onCreate");

        super.onCreate(savedInstanceState);

        try {
            setView();
            getDisplaySize();
            getAdSize();
            findView();
            init();
            configureListener();
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.e("FrgBase, onCreate, ex : " + e.toString());
        }

    }


    public void refresh() {

    }


    public void refresh(String keyword) {

    }

    public void setIndex(int index) {
        _index = index;
    }

    public void setTabNewCount(int count) {

        if (_index < 0) {
            LogUtil.e("setTabNewCount, _index : " + _index + ", error");
            return;
        }

        if (_slideTabView == null) {
            LogUtil.e("setTabNewCount, _slideTabView is null error");
            return;
        }

        _slideTabView.setButtonNewCount(_index, count);
    }


    public void setSlideTabView(SlideTabView slideTabView) {
        _slideTabView = slideTabView;
    }

    protected abstract void setView();


    protected void setView(int resId) {
        mBase = InflateUtil.inflate(getActivity(), resId, null);
    }


    protected void setView(View v) {
        mBase = v;
    }


    protected abstract void findView();


    protected abstract void init();


    protected abstract void configureListener();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup parent = ((ViewGroup) mBase.getParent());
        if (parent != null) parent.removeAllViews();
        return mBase;
    }


    public void onBackPressed() {
        // getActivity().finish();
    }


    public Context getContext() {

//        LogUtil.d("_txtAd, getContext, mContext : " + (mContext == null ? "null" : "ok"));
//        LogUtil.d("_txtAd, getContext, mActivity : " + (mActivity == null ? "null" : "ok"));

        if (mContext != null) {
            return mContext;
        }

        if (mActivity != null) {
            return mActivity;
        }

        return getActivity();
    }

    public Activity getMyActivity() {

        LogUtil.d("_txtAd, getMyActivity, mActivity : " + (mActivity == null ? "null" : "ok"));

        if (mActivity != null) {
            return mActivity;
        }

        return (Activity)super.getActivity();
    }

    private Context mContext = null;
    private Activity mActivity = null;

    @Override
    public void onAttach(Context context) {
        mContext = context;
        if (context instanceof Activity) {
            mActivity = (Activity) context;
        }
        super.onAttach(context);
    }

//    @Override
//    public void onDetach() {
//        mActivity = null;
//        mContext = null;
//        super.onDetach();
//    }


    public View findViewById(int resId) {
        try {
            if (mBase == null) {
                return null;
            }
            return mBase.findViewById(resId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    protected void getDisplaySize() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display display = ((WindowManager) getActivity().getSystemService(getActivity().WINDOW_SERVICE)).getDefaultDisplay();
        display.getMetrics(displayMetrics);

        mDeviceSize = new int[2];
        mDeviceSize[0] = displayMetrics.widthPixels;
        mDeviceSize[1] = displayMetrics.heightPixels;
    }


    private AdSize getAdSize() {
        // Step 2 - Determine the screen width (less decorations) to use for the ad width.
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float widthPixels = outMetrics.widthPixels;
        float density = outMetrics.density;

        int adWidth = (int) (widthPixels / density);

        LogUtil.d("getAdSize, adWidth : " + adWidth);

        // Step 3 - Get adaptive ad size and return for setting on the ad view.
        return AdSize.getPortraitAnchoredAdaptiveBannerAdSize(getActivity(), adWidth);
    }

    private AdSize _adSize = null;

    protected AdSize getAdSizeInfo() {

        if (_adSize == null) _adSize = getAdSize();

        LogUtil.d("getAdSizeInfo,  " + _adSize.toString());

        return _adSize;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    protected void callApi_member_get_by_no(final InterfaceSet.OnCompleteListener listener) {

        LogUtil.e("==========callApi_member_get_by_no : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_no(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                SPUtil.getInstance().setUserInfo(getContext(), userInfo);

                //setBinding_HeartCount();

                if (listener != null) {
                    listener.onComplete();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }



    protected void callApi_logClick_add_log(final String code) {

        LogUtil.e("==========callApi_logClick_add_log : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.logClick_add_log(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , code
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }



    protected void goActionAfterAdView(InterfaceSet.AdFullViewCompleteListener listener) {

        int boardEnterCount = SPUtil.getInstance().getBoardEnterCount(getContext());
        int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(getContext());
        LogUtil.d("boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);


        if (listener != null) listener.onAfterAction();

//        if(boardEnterCount % repeatBoardWriteAd != 0) {
//            if (listener != null) listener.onAfterAction();
//            return;
//        }
//
//        final Dialog dialogAd = ProgressDialogUtil.show(getContext(), null);
//
//        final AdManager adManager = new AdManager();
//        adManager.ready(getActivity(), getContext(), true, new InterfaceSet.OnAd2CompleteListener() {
//            @Override
//            public void onAdLoadSuccess() {
//                LogUtil.d("_txtAd, goActionAfterAdView, onAdLoadSuccess");
//
//                ProgressDialogUtil.dismiss(dialogAd);
//                adManager.showAd();
//            }
//
//            @Override
//            public void onAdLoadFail() {
//                LogUtil.d("_txtAd, goActionAfterAdView, onAdLoadFail");
//                ProgressDialogUtil.dismiss(dialogAd);
//
//                if(adManager.showAd())
//                    return;
//
//                if (listener != null) listener.onAfterAction();
//            }
//
//            @Override
//            public void onAdPlayComplete(Constants.enum_ad enumAd) {
//                LogUtil.d("_txtAd, goActionAfterAdView, onAdPlayComplete");
//                ProgressDialogUtil.dismiss(dialogAd);
//
//                if (listener != null) listener.onAfterAction();
//            }
//        });


    }
}
