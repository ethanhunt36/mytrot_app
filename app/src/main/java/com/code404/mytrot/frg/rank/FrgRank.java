package com.code404.mytrot.frg.rank;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.frg.FrgBase;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.util.Alert;
import com.code404.mytrot.util.FormatUtil;
import com.code404.mytrot.util.JSONUtil;
import com.code404.mytrot.util.LogUtil;
import com.code404.mytrot.util.ProgressDialogUtil;
import com.code404.mytrot.util.SPUtil;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;


@SuppressLint("ValidFragment")
public class FrgRank extends FrgBase implements GListView.IMakeView {

    private String _type = "GET";
    private GListView _list = null;
    private View _baseNoData = null;
    private TextView _txtNoData = null;
    private int _last_no = -1;

    @SuppressLint("ValidFragment")
    public FrgRank() {

    }

    public void setType(String type) {
        _type = type;
        _last_no = -1;
    }


    @Override
    protected void setView() {
        setView(R.layout.frg_like_artist);
    }


    @Override
    protected void findView() {
        _list = (GListView) findViewById(R.id.list);
        _baseNoData = findViewById(R.id.baseNoData);
        _txtNoData = (TextView) findViewById(R.id.txtNoData);
    }


    @Override
    protected void init() {
        _list.setViewMaker(R.layout.row_point, this);
        _list.setNoData(_baseNoData);
        _txtNoData.setText("내역이 없습니다.");
    }


    @Override
    public void refresh() {
        if (_list == null) {
            findView();
        }
        _last_no = -1;
        _list.removeAll();
        callApi_memberPointHistory_listing(true);
    }


    @Override
    protected void configureListener() {
        _list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                LogUtil.d(String.format("onScrollStateChanged, scrollState : %d", scrollState));
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                _list.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);

                LogUtil.d(String.format("onScroll, firstVisibleItem : %d, visibleItemCount : %d, totalItemCount : %d", firstVisibleItem, visibleItemCount, totalItemCount));

                LogUtil.d("_multiList.isBottom() : " + _list.isBottom());
                LogUtil.d("_multiList.isTop() : " + _list.isTop());

                if (_list.isBottom()) {
                    callApi_memberPointHistory_listing(false);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        callApi_memberPointHistory_listing(true);
    }

    private void callApi_memberPointHistory_listing(final boolean isClear) {

        LogUtil.e("==========callApi_memberPointHistory_listing : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = null;


        call = apiInterface.memberPointHistory_listing(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _type
                , Constants.RECORD_SIZE
                , _last_no
        );

        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);


        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);

                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONArray list = JSONUtil.getJSONArray(data, "list");

                if (isClear) {
                    _list.removeAll();
                }

                if (list.length() > 0) {
                    JSONObject infoLast = JSONUtil.getJSONObject(list, list.length() - 1);
                    _last_no = JSONUtil.getInteger(infoLast, "no");
                }

                _list.addItems(list);
                _list.setNoDataVisibility(_list.getCountAll() < 1);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    @Override
    public View makeView(final GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {
        final JSONObject json = adapter.getItem(position);

        RelativeLayout baseTitle = (RelativeLayout) convertView.findViewById(R.id.baseTitle);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);
        TextView txtGubun = (TextView) convertView.findViewById(R.id.txtGubun);
        TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        String title = "";
        String artist_name = JSONUtil.getString(json, "artist_name");
        String vod_name = JSONUtil.getString(json, "vod_name");
        String subject = JSONUtil.getString(json, "subject");
        String reg_dttm = JSONUtil.getString(json, "reg_dttm");
        double point_amount = JSONUtil.getDouble(json, "point_amount");

        reg_dttm = reg_dttm.substring(0, 16);

        if (!FormatUtil.isNullorEmpty(artist_name) && !FormatUtil.isNullorEmpty(vod_name)) {
            title = artist_name + " - " + vod_name;
            subject = "재생 적립";
        } else if (!FormatUtil.isNullorEmpty(vod_name)) {
            title = vod_name;
            subject = "재생 적립";
        } else if (!FormatUtil.isNullorEmpty(artist_name)) {
            title = artist_name;
            subject = "기부";
        } else {
            title = subject;
            subject = "광고 시청";
        }

        txtTitle.setText(title);
        txtDate.setText(reg_dttm);
        txtGubun.setText(subject);
        txtPoint.setText(FormatUtil.toPriceFormat(point_amount) + " P");
        return convertView;
    }


}
