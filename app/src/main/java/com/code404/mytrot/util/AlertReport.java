package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;

import java.util.ArrayList;
import java.util.List;


public class AlertReport extends Alert {


    private Context mContext;


    private String[] _report_items = new String[]{"다른 가수 비난",
            "욕설 및 불쾌한 내용",
            "관련 없는 광고성 내용",
            "음란하거나 선정적 내용",
            "기타 부적절한 내용"};

    private List<Button> _list = new ArrayList<Button>();

    public void showReport(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_report);

        Button btn01 = (Button) dialog.findViewById(R.id.btn01);
        Button btn02 = (Button) dialog.findViewById(R.id.btn02);
        Button btn03 = (Button) dialog.findViewById(R.id.btn03);
        Button btn04 = (Button) dialog.findViewById(R.id.btn04);
        Button btn05 = (Button) dialog.findViewById(R.id.btn05);
        final EditText edtEtc = (EditText) dialog.findViewById(R.id.edtEtc);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);


        _list.clear();
        _list.add(btn01);
        _list.add(btn02);
        _list.add(btn03);
        _list.add(btn04);
        _list.add(btn05);

        edtEtc.setVisibility(View.GONE);

        for (int i = 0; i < _list.size(); i++) {
            final Button btn = _list.get(i);
            final int index = i ;
            btn.setText(_report_items[i]);

            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    btn.setSelected(!btn.isSelected());

                    if(index == _list.size() - 1) {
                        edtEtc.setVisibility(btn.isSelected() ? View.VISIBLE : View.GONE);
                    }
                }
            });
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.CANCEL, "");
                }
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = "";
                for (int i = 0; i < _list.size(); i++) {
                    final Button btn = _list.get(i);

                    if(btn.isSelected()) {
                        if(!FormatUtil.isNullorEmpty(msg)) msg += ", ";
                        msg += btn.getText().toString() ;
                    }
                }

                String etc = edtEtc.getText().toString();

                if(!FormatUtil.isNullorEmpty(etc)) msg += ", " + etc;

                if(FormatUtil.isNullorEmpty(msg)) {
                    new Alert().showAlert(mContext, "신고 사유를 선택해주세요.");
                    return;
                }

                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.BUTTON1, msg);
                }
                dialog.dismiss();
            }
        });


        dialog.setCancelable(false);
        dialog.show();
    }
}