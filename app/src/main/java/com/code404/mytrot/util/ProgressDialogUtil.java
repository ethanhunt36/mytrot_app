package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.code404.mytrot.atv.AtvBase;

public class ProgressDialogUtil {

    public static Dialog show(Context context, Dialog dialog) {
        return show(context, dialog, true, null, null);
    }

    public static Dialog show(Context context, Dialog dialog, boolean isCancelable) {
        return show(context, dialog, isCancelable, null, null);
    }

    public static Dialog show(Context context, Dialog dialog, boolean isCancelable, OnDismissListener dismissListener) {
        return show(context, dialog, isCancelable, dismissListener, null);
    }

    public static Dialog show(Context context, Dialog dialog, boolean isCancelable, OnCancelListener cancelListener) {
        return show(context, dialog, isCancelable, null, cancelListener);
    }

    public static Dialog show(Context context, Dialog dialog, boolean isCancelable, OnDismissListener dismissListener, OnCancelListener cancelListener) {
        try {
            if (dialog != null) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
                return dialog;
            }

            if (context == null)
                return dialog;

            AtvBase atvBase = (AtvBase) context;

            if (atvBase != null) {
                if (atvBase.isFinishing())
                    return dialog;
            }

            //dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);
            dialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Dialog_NoActionBar);

            LinearLayout layout = new LinearLayout(context);
            ProgressBar progressBar = new ProgressBar(context);

            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setGravity(Gravity.CENTER);
            layout.setBackgroundColor(0x00000000); // 백그라운드색
            layout.addView(progressBar, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            dialog.addContentView(layout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            dialog.setCancelable(isCancelable);

            if (cancelListener != null)
                dialog.setOnCancelListener(cancelListener);

            if (dismissListener != null)
                dialog.setOnDismissListener(dismissListener);

            dialog.show();

        } catch (Exception e) {

            e.printStackTrace();

        }

        return dialog;
    }

    public static void dismiss(Dialog dialog) {
        try {
            if (dialog != null && dialog.isShowing())
                dialog.dismiss();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
