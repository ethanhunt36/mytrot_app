package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;

import static com.google.android.gms.ads.formats.NativeAdOptions.ADCHOICES_TOP_LEFT;
import static com.google.android.gms.ads.formats.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_SQUARE;

import androidx.annotation.NonNull;


public class AlertQuit extends Alert {


    private Context mContext;

    private View _baseAdOnlbam = null;
    private AdRequest _adRequest = null;

    private String _ad_id = "";
    private AdLoader _adLoader = null;
    private Button _btnStore = null;

    private Button _btnConfirm = null;
    private Button _btnInstall = null;
    private ImageButton _btnClose = null;

    private com.google.android.ads.nativetemplates.TemplateView _adNativeView = null;

    public void showQuit(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_quit);

        _adNativeView = dialog.findViewById(R.id.adNativeView);
        _btnClose = dialog.findViewById(R.id.btnClose);
        _btnStore = (Button) dialog.findViewById(R.id.btnStore);
        _btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        _btnInstall = dialog.findViewById(R.id.btnInstall);
        _baseAdOnlbam = dialog.findViewById(R.id.baseAdOnlbam);

        _adNativeView.setVisibility(View.INVISIBLE);
        _btnClose.setVisibility(View.INVISIBLE);

        TextView txtHintReview = dialog.findViewById(R.id.txtHintReview);

        if(SPUtil.getInstance().getReviewRewardYn(context).equals("Y")) {
            callApi_logClick_my_action_info(Constants.LOG_CLICK_REVIEW);
            txtHintReview.setVisibility(View.VISIBLE);
        } else {
            txtHintReview.setVisibility(View.GONE);
        }

        //initAd();


        _btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });
        _btnStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(cnt_addVoteTicketReview < Constants.ADD_TICKET_REVIEW) {
                    callApi_vote_put_ticket_ad(Constants.TICKET_REVIEW_COUNT);
                    cnt_addVoteTicketReview++;
                }

                callApi_logClick_add_log(Constants.LOG_CLICK_REVIEW_03);

                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON2);
                }
                dialog.dismiss();
            }
        });

        _btnInstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON3);
                }
                dialog.dismiss();
            }
        });

        _btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });


        dialog.setCancelable(true);
        dialog.show();
    }


    public void showQuitReview(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_quit_review);

        _btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        _btnStore = (Button) dialog.findViewById(R.id.btnStore);

        TextView txtHintReview = dialog.findViewById(R.id.txtHintReview);

        if (SPUtil.getInstance().getReviewRewardYn(context).equals("Y")) {
            callApi_logClick_my_action_info(Constants.LOG_CLICK_REVIEW);
            txtHintReview.setVisibility(View.VISIBLE);
        } else {
            txtHintReview.setVisibility(View.GONE);
        }

        _btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });
        _btnStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cnt_addVoteTicketReview < Constants.ADD_TICKET_REVIEW) {
                    callApi_vote_put_ticket_ad(Constants.TICKET_REVIEW_COUNT);
                    cnt_addVoteTicketReview++;
                }

                callApi_logClick_add_log(Constants.LOG_CLICK_REVIEW_02);

                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON2);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    private Context getContext() {
        return mContext;
    }

    private void initAd() {

        _ad_id = Constants.KEY_AD_NATIVE;
        _adRequest = new AdRequest.Builder()
                .build();

//        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
//            @Override
//            public void onInitializationComplete(InitializationStatus initializationStatus) {
//                LogUtil.d("MobileAds, onInitializationComplete");
//            }
//        });

        _adLoader = new AdLoader.Builder(getContext(), _ad_id)
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        LogUtil.d("MobileAds, onUnifiedNativeAdLoaded, _adLoader.isLoading() : " + _adLoader.isLoading());
                        // some code that displays the ad.

                        if (_adLoader.isLoading()) {
                            // The AdLoader is still loading ads.
                            // Expect more adLoaded or onAdFailedToLoad callbacks.

                            _adNativeView.setVisibility(View.VISIBLE);
                            _baseAdOnlbam.setVisibility(View.INVISIBLE);
                            _btnClose.setVisibility(View.VISIBLE);

                            try {
                                ColorDrawable c = new ColorDrawable();
                                c.setColor(getContext().getResources().getColor(R.color.white));
                                NativeTemplateStyle styles = new
                                        NativeTemplateStyle.Builder().withMainBackgroundColor(c).build();
                                _adNativeView.setStyles(styles);
                                _adNativeView.setNativeAd(nativeAd);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            // The AdLoader has finished loading ads.

                        }
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdClicked() {
                        super.onAdClicked();
                        LogUtil.d("NativeAd, onAdClicked");
                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        LogUtil.d("NativeAd, onAdClosed");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        LogUtil.d("NativeAd, onAdFailedToLoad");
                        LogUtil.d("NativeAd, onAdFailedToLoad, loadAdError : " + loadAdError.toString());
                    }

                    @Override
                    public void onAdImpression() {
                        super.onAdImpression();
                        LogUtil.d("NativeAd, onAdImpression");
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        LogUtil.d("NativeAd, onAdLoaded");
                    }

                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                        LogUtil.d("NativeAd, onAdOpened");
                    }
                })
                .withNativeAdOptions(new com.google.android.gms.ads.nativead.NativeAdOptions.Builder()
                        .setReturnUrlsForImageAssets(false)
                        .setAdChoicesPlacement(ADCHOICES_TOP_LEFT)
                        .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_SQUARE)
                        .build()
                )
                .build();
        loadAd();
    }

    private void loadAd() {
        LogUtil.e("loadAd");
        LogUtil.e("loadAd, _adLoader : " + (_adLoader == null ? "null" : "ok"));
        LogUtil.e("loadAd, _adRequest : " + (_adRequest == null ? "null" : "ok"));

        _adLoader.loadAds(_adRequest, 2);
    }

    private int cnt_addVoteTicketReview = -1;

    private void callApi_logClick_my_action_info(final String code) {

        LogUtil.e("==========callApi_logClick_my_action_info : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.logClick_my_action_info(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , code
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                int cnt = JSONUtil.getInteger(data, "cnt", 0);

                LogUtil.d("code : " + code + ", cnt : " + cnt);

                if (code.equals(Constants.LOG_CLICK_REVIEW)) {
                    cnt_addVoteTicketReview = cnt;
                }
//                if(code.equals(Constants.LOG_CLICK_SHARE)) {
//                    cnt_addVoteTicketShare = cnt;
//                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_vote_put_ticket_ad(final int amount) {
        LogUtil.e("==========callApi_vote_put_ticket_ad : start==========");

        if (!SPUtil.getInstance().getReviewRewardYn(getContext()).equals("Y")) {
            LogUtil.e("callApi_vote_put_ticket_ad, pass");
            return;
        }

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_put_ticket_ad(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , amount
                , ""
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                    new Alert().showAlert(getContext(), resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "투표권 " + amount + "장이 충전되었습니다.";
                }

                // Alert.toastLong(getContext(), resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_logClick_add_log(final String code) {

        LogUtil.e("==========callApi_logClick_add_log : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.logClick_add_log(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , code
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }
}