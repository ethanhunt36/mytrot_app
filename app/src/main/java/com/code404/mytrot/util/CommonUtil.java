package com.code404.mytrot.util;

import static com.kakao.util.helper.Utility.getPackageInfo;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.ScrollView;

import com.code404.mytrot.common.Constants;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import static com.kakao.util.helper.Utility.getPackageInfo;


public class CommonUtil {

    //중복 클릭 방지 시간 설정 ( 해당 시간 이후에 다시 클릭 가능 )
    private static final long MIN_CLICK_INTERVAL = 1000;
    private static long mLastClickTime = 0;

    public synchronized static boolean checkDoubleTab() {

        long currentTime = System.currentTimeMillis();
        long elapsedTime = currentTime - mLastClickTime;

        LogUtil.d("CommonUtil, checkDoubleTab elapsedTime : " + elapsedTime);

        if (elapsedTime < MIN_CLICK_INTERVAL) {
            LogUtil.e("CommonUtil, checkDoubleTab, 이벤트 중복 발생");
            return true;
        }

        mLastClickTime = currentTime;

        return false;
    }


    public static boolean isValidEmail(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    public static String removeEmoticon(String word) {
        Pattern emoticons = Pattern.compile("[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]+");
        Matcher emoticonsMatcher = emoticons.matcher(word);
        word = emoticonsMatcher.replaceAll(" ");

        word = word.replace("\uD83D\uDE0A", "");
        word = word.replace("☺", "");
        return word;
    }


    public static String removeSymbol(String word) {
        return word.replaceAll("!\"#[$]%&\\(\\)\\{\\}@`[*]:[+];-.<>,\\^~|'\\[\\]", "");
    }

    /**
     * 시스템에 설정된 언어
     *
     * @param context
     * @return
     */
    public static String getLanguage(Context context) {
        Locale systemLocale = context.getResources().getConfiguration().locale;
        String language = systemLocale.getLanguage();

        return language;
    }


    /**
     * 시스템에 설정된 국가 정보
     *
     * @param context
     * @return
     */
    public static String getCountry(Context context) {
        Locale systemLocale = context.getResources().getConfiguration().locale;
        String country = systemLocale.getCountry();

        return country;
    }


    /**
     * 현재 버전을 가져온다(versionName)
     *
     * @param context
     * @return
     */
    public static String getCurrentVersion(Context context) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pi.versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }


//    /**
//     * @name 디바이스고유키
//     * @param context
//     * @return
//     */
//    public static String getDeviceKey(Context context)
//    {
//        TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        return getDeviceKey(context, telephonyMgr);
//    }
//
//
//    public static String getDeviceKey(Context context, TelephonyManager telephonyMgr)
//    {
//        String deviceId = telephonyMgr.getDeviceId();
//
//        if (deviceId == null) deviceId = "";
//
//        // String androidId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
//        // if(androidId == null)
//        // androidId = "";
//
//        // String returnId = deviceId + "/" + androidId;
//
//        return deviceId;
//    }


    public static boolean isNetworkOn(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobile = connectivityManager.getNetworkInfo(connectivityManager.TYPE_MOBILE);
        NetworkInfo wifi = connectivityManager.getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (mobile == null || wifi == null) return false;
        else if (mobile == null) return wifi.isConnected();
        else if (wifi == null) return mobile.isConnected();
        else return mobile.isConnected() || wifi.isConnected();
    }


    /**
     * 현재 디바이스의 전화번호를 가져온다.
     *
     * @param context
     * @return
     */
//    public static String getPhoneNumber(Context context)
//    {
//        TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        return getPhoneNumber(context, telephonyMgr);
//    }


//    public static String getPhoneNumber(Context context, TelephonyManager telephonyMgr)
//    {
//        String phoneNumber = telephonyMgr.getLine1Number();
//        if (!FormatUtil.isNullorEmpty(phoneNumber))
//        {
//            phoneNumber = phoneNumber.replace("+82", "0").replaceAll("-", "");
//        }
//        else phoneNumber = "";
//        return phoneNumber;
//    }
    public static String getTelecomCode(Context context) {
        TelephonyManager telephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return getTelecomCode(context, telephonyMgr);
    }


    public static String getTelecomCode(Context context, TelephonyManager telephonyMgr) {
        String telecomCode = telephonyMgr.getSimOperatorName();

        if (!FormatUtil.isNullorEmpty(telecomCode)) telecomCode = telecomCode.replaceAll(" ", "");
        else telecomCode = "";
        return telecomCode;
    }


    /**
     * 연락처에 저장된 핸드폰 번호를 수집
     *
     * @param context
     * @return
     */
    public static ArrayList<String> getContactList(Context context) {
        ArrayList<String> list = new ArrayList<String>();

        String[] arrProjection =
                {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
        String[] arrPhoneProjection =
                {ContactsContract.CommonDataKinds.Phone.NUMBER};

        // get user list
        Cursor clsCursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, arrProjection, ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1", null, null);

        Cursor clsPhoneCursor = null;
        String strContactId = "";
        String number = "";

        while (clsCursor.moveToNext()) {
            strContactId = clsCursor.getString(0);

            // phone number
            clsPhoneCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, arrPhoneProjection, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + strContactId, null, null);

            while (clsPhoneCursor.moveToNext()) {
                number = clsPhoneCursor.getString(0).replace("-", "");

                list.add(number);
                break;
            }

            clsPhoneCursor.close();
        }
        clsCursor.close();

        return list;
    }


    /**
     * 연락처의 이름/번호 조회
     *
     * @param context
     * @return
     */
    public static ArrayList<String[]> getContactNameList(Context context) {
        ArrayList<String[]> list = new ArrayList<String[]>();

        String[] arrProjection =
                {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
        String[] arrPhoneProjection =
                {ContactsContract.CommonDataKinds.Phone.NUMBER};

        // get user list
        Cursor clsCursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, arrProjection, ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1", null, null);

        Cursor clsPhoneCursor = null;
        String strContactId = "";

        String[] row = null;

        while (clsCursor.moveToNext()) {
            row = new String[2];
            strContactId = clsCursor.getString(0);
            row[0] = clsCursor.getString(1);

            LogUtil.d("row[0] : " + row[0]);

            // phone number
            clsPhoneCursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, arrPhoneProjection, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + strContactId, null, null);

            while (clsPhoneCursor.moveToNext()) {
                row[1] = clsPhoneCursor.getString(0).replace("-", "");
                LogUtil.d("row[1] : " + row[1]);

                list.add(row);
                break;
            }

            clsPhoneCursor.close();
        }
        clsCursor.close();

        return list;
    }


    /**
     * 키패드 숨김
     *
     * @param view   해당 뷰
     * @param isHide true : 숨김, false : 보임
     */
    public static void hideKeyPad(View view, boolean isHide) {
        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (isHide) imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        else imm.showSoftInput(view, 0);
    }


    public static boolean isAppRunning(Context context) {
//        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
//        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
//        for (int i = 0; i < procInfos.size(); i++) {
//            LogUtil.d("isAppRunning, procInfos.get(i).processName : " + procInfos.get(i).processName);
//            if (procInfos.get(i).processName.equals(context.getPackageName())) {
//                return true;
//            }
//        }

        ActivityManager activity_manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> task_info = activity_manager.getRunningTasks(9999);

        for (int i = 0; i < task_info.size(); i++) {
            LogUtil.d("isAppRunning, [" + i + "] activity:" + task_info.get(i).topActivity.getPackageName() + " >> " + task_info.get(i).topActivity.getClassName());
            if (task_info.get(i).topActivity.getClassName().startsWith("com.code404.mytrot")) {
                return true;
            }
        }

        return false;
    }


    public static boolean getRunActivity(Context context, String className) {

        ActivityManager activity_manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> task_info = activity_manager.getRunningTasks(9999);

        for (int i = 0; i < task_info.size(); i++) {
            LogUtil.d("getRunActivity, [" + i + "] activity:" + task_info.get(i).topActivity.getPackageName() + " >> " + task_info.get(i).topActivity.getClassName());
            if (task_info.get(i).topActivity.getClassName().equals(className)) {
                return true;
            }
        }

        return false;
    }


    /**
     * 현재 이 어플이 활성중인지 나타냄
     *
     * @param context
     * @param className
     * @return
     */
    public static RunningTaskInfo getAtv(Context context, ComponentName className) {
        return getAtv("DEVSWING_LOG", context, className);
    }


    /**
     * 현재 이 어플이 활성중인지 나타냄
     *
     * @param tag
     * @param context
     * @param className
     * @return
     */
    public static RunningTaskInfo getAtv(String tag, Context context, ComponentName className) {
        RunningTaskInfo retVal = null;
        LogUtil.e("pkageName----->" + className.getPackageName());
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> info = activityManager.getRunningTasks(100);
        for (RunningTaskInfo runningTaskInfo : info) {
            String baseAct = runningTaskInfo.baseActivity.getClassName().toString();
            String topAct = runningTaskInfo.topActivity.getClassName().toString();
            LogUtil.e("topActivity----->" + topAct);
            LogUtil.e("baseActivity----->" + baseAct);

            if (baseAct.contains(className.getPackageName())) {
                retVal = runningTaskInfo;
                break;
            }
        }
        if (info != null) {
            info.clear();
        }
        if (activityManager != null) {
            activityManager = null;
        }
        return retVal;
    }


    public static boolean isPackageInstalled(Context ctx, String pkgName) {
        try {
            ctx.getPackageManager().getPackageInfo(pkgName, PackageManager.GET_ACTIVITIES);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * 카카오톡 메시지 보내기
     */
    public static void sendKakao(Context context, String eventName) {
        try {
            eventName = eventName.replace("#", "");
            String inviteMsg = String.format("[%s]에서 귀하를 초대합니다.", eventName);
            String url = "http://2url.kr/X9H";

            String link = "kakaolink://sendurl?msg=" + inviteMsg + "&url=" + url + "&appid=com.devswing.quickguide&appver=1.0";

            Uri uri = Uri.parse(link);
            Intent intent = new Intent(Intent.ACTION_SEND, uri);

            context.startActivity(intent);
        } catch (Exception e) {
            Alert.toastLong(context, "카카오톡 App이 설치되어 있지 않습니다.");
        }
    }


    /**
     * 마이피플 메시지 보내기
     */
    public static void sendMyPeople(Context context, String eventName) {
        try {
            eventName = eventName.replace("#", "");
            String inviteMsg = String.format("[%s]에서 귀하를 초대합니다.", eventName);
            String url = "http://2url.kr/X9H";

            String link = "myp://sendMessage?message=" + inviteMsg + "&url=" + url + "&appid=com.devswing.appname=&appName=appname";

            Uri uri = Uri.parse(link);
            Intent intent = new Intent(Intent.ACTION_SEND, uri);

            context.startActivity(intent);
        } catch (Exception e) {
            Alert.toastLong(context, "마이피플 App이 설치되어 있지 않습니다.");
        }
    }


    public static void shareMessage(Context context, String titleStr, String contentsStr) {
        String shareUrl = titleStr;
        shareUrl += "\n\n" + contentsStr;

        String message = shareUrl;
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        intent.setData(Uri.parse(message));
        intent.setType("text/plain");
        context.startActivity(intent);
    }


    public static int ConvertDpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }


    public static int ConvertPixelToDp(Context context, int pixels) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return ((int) (pixels / displayMetrics.density));
    }


    public static String findAddress(Context context, double lat, double lng) {
        StringBuffer bf = new StringBuffer();
        Geocoder geocoder = new Geocoder(context, Locale.KOREA);
        List<Address> address;
        String currentLocationAddress = "";
        try {
            if (geocoder != null) {
                // 세번째 인수는 최대결과값인데 하나만 리턴받도록 설정했다
                address = geocoder.getFromLocation(lat, lng, 1);
                // 설정한 데이터로 주소가 리턴된 데이터가 있으면
                if (address != null && address.size() > 0) {
                    // 주소
                    currentLocationAddress = address.get(0).getAddressLine(0).toString();

                    // 전송할 주소 데이터 (위도/경도 포함 편집)
                    bf.append(currentLocationAddress);
                    // .append("#");
                    // bf.append(lat).append("#");
                    // bf.append(lng);
                }
            }

        } catch (IOException e) {

        }
        return bf.toString();
    }


    public static String findAddressEng(Context context, double lat, double lng) {
        StringBuffer bf = new StringBuffer();
        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
        List<Address> address;
        String currentLocationAddress = "";
        try {
            if (geocoder != null) {
                // 세번째 인수는 최대결과값인데 하나만 리턴받도록 설정했다
                address = geocoder.getFromLocation(lat, lng, 1);
                // 설정한 데이터로 주소가 리턴된 데이터가 있으면
                if (address != null && address.size() > 0) {
                    // 주소
                    currentLocationAddress = address.get(0).getAddressLine(0).toString();

                    // 전송할 주소 데이터 (위도/경도 포함 편집)
                    bf.append(currentLocationAddress);
                    // .append("#");
                    // bf.append(lat).append("#");
                    // bf.append(lng);
                }
            }

        } catch (IOException e) {

        }
        return bf.toString();
    }


    public static BitmapFactory.Options getBitmapFactoryFromUri(Context context, Uri uri) {
        InputStream input = null;
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        try {
            input = context.getContentResolver().openInputStream(uri);
            onlyBoundsOptions.inJustDecodeBounds = true;
            onlyBoundsOptions.inDither = true;// optional
            onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// optional
            BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
            input.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return onlyBoundsOptions;
    }


    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String str = "";
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};

            LogUtil.d("context : " + (context == null ? "null" : "ok"));
            LogUtil.d("contentUri : " + (contentUri == null ? "null" : contentUri));

            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            str = cursor.getString(column_index);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return str;
    }

    public static String getMimeType(String url) {

        if (FormatUtil.isNullorEmpty(url)) {
            return "";
        }

        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }
        return type;
    }


    /**
     * 스크롤뷰 하위의 특정 뷰의 위치로 이동하기
     *
     * @param view
     * @param scrollView
     * @param count
     */
    public static void scrollToView(View view, final ScrollView scrollView, int count) {
        if (view != null && view != scrollView) {
            count += view.getTop();
            scrollToView((View) view.getParent(), scrollView, count);
        } else if (scrollView != null) {
            final int finalCount = count;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    scrollView.smoothScrollTo(0, finalCount);
                }
            }, 350);
        }
    }


    public static int convertDpToPixel(Context cx, float dp) {
        Resources resources = cx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }


    public static void copyClipboard(Context context, String text) {
        try {
            ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clipData = ClipData.newPlainText("", text);
            clipboardManager.setPrimaryClip(clipData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getRandom() {
        int seconds = 10;

        Random r = new Random();
        seconds = r.nextInt(5000);

        LogUtil.d("getRandom, seconds : " + seconds);
        return seconds;
    }



    public static String getDeviceUuid(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }



    public static String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return android.util.Base64.encodeToString(md.digest(), android.util.Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                LogUtil.d("Unable to get MessageDigest. signature=" + signature + ", e : " + e.toString());
            }
        }
        return null;
    }


    public static String[] convertStringToArray(String input) {
        if (input == null || input.isEmpty()) {
            return new String[0]; // 입력 문자열이 null이거나 비어 있으면 빈 배열 반환
        }

        String[] result = new String[input.length()]; // 문자열 길이만큼 배열 생성

        for (int i = 0; i < input.length(); i++) {
            result[i] = String.valueOf(input.charAt(i)); // 각 글자를 배열에 저장
        }

        return result;
    }
}
