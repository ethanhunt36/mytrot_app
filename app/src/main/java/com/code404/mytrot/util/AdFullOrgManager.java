package com.code404.mytrot.util;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.lifecycle.ProcessLifecycleOwner;

//import com.applovin.mediation.MaxAd;
//import com.applovin.mediation.MaxAdListener;
//import com.applovin.mediation.MaxError;
//import com.applovin.mediation.MaxReward;
//import com.applovin.mediation.MaxRewardedAdListener;
//import com.applovin.mediation.ads.MaxInterstitialAd;
//import com.applovin.mediation.ads.MaxRewardedAd;
import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.code404.mytrot.MyTrotApp;
import com.code404.mytrot.R;
import com.code404.mytrot.atv.web.AtvWeb;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
//import com.facebook.ads.AudienceNetworkAds;
//import com.facebook.ads.AudienceNetworkAds;
//import com.fsn.cauly.CaulyAdInfo;
//import com.fsn.cauly.CaulyAdInfoBuilder;
//import com.fsn.cauly.CaulyInterstitialAd;
//import com.fsn.cauly.CaulyInterstitialAdListener;
import com.facebook.ads.Ad;
import com.facebook.ads.InterstitialAdListener;
import com.google.ads.mediation.unity.UnityAdapter;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.initialization.AdapterStatus;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.google.gson.JsonObject;
import com.igaworks.ssp.AdPopcornSSP;
import com.igaworks.ssp.AdPopcornSSPUserProperties;
import com.igaworks.ssp.SSPErrorCode;
import com.igaworks.ssp.part.interstitial.AdPopcornSSPInterstitialAd;
import com.igaworks.ssp.part.interstitial.listener.IInterstitialLoadEventCallbackListener;
import com.igaworks.ssp.part.interstitial.listener.IInterstitialShowEventCallbackListener;
import com.igaworks.ssp.part.video.AdPopcornSSPInterstitialVideoAd;
import com.igaworks.ssp.part.video.AdPopcornSSPRewardVideoAd;
import com.igaworks.ssp.part.video.listener.IInterstitialVideoAdEventCallbackListener;
import com.igaworks.ssp.part.video.listener.IRewardVideoAdEventCallbackListener;
import com.igaworks.ssp.AdPopcornSSP;
import com.tnkfactory.ad.AdItem;
import com.tnkfactory.ad.AdListener;
import com.tnkfactory.ad.InterstitialAdItem;
import com.vungle.mediation.VungleAdapter;
import com.vungle.mediation.VungleExtrasBuilder;
import com.vungle.mediation.VungleInterstitialAdapter;
import com.wafour.ads.sdk.interstitial.WInterstitial;
import com.wafour.ads.sdk.WErrorCode;
import com.wafour.ads.sdk.interstitial.WInterstitial;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Random;

import retrofit2.Call;

//import com.ironsource.mediationsdk.IronSource;
//import com.ironsource.mediationsdk.logger.IronSourceError;
//import com.ironsource.mediationsdk.model.Placement;
//import com.ironsource.mediationsdk.sdk.InterstitialListener;
//import com.ironsource.mediationsdk.sdk.RewardedVideoListener;


public class AdFullOrgManager {

    private static String TAG = "AdFullOrgManager";

    private Queue<Constants.enum_ad> _ad_queue = new LinkedList();

    private Constants.enum_ad _last_ad = Constants.enum_ad.none;

    // 리워드 광고
    private RewardedAd _rewardedAd;

    // 전면 광고
    private InterstitialAd _interstitialAd;

    // 보상형 전면 광고
    private RewardedInterstitialAd _rewardedInterstitialAd;

    //    //    // 카울리 전면 광고
//    private CaulyInterstitialAd _interstialCaulyAd;
//
    // WAD
    private WInterstitial _interstitialWad;

    // UnityAds
//    private UnityAdsListener _myAdsListener = null;

    // MaxRewardedAd
    private MaxRewardedAd _maxRewardedAd = null;
    //
    private MaxInterstitialAd _maxInterstitialAd = null;

    // facebook
    private com.facebook.ads.InterstitialAd _interstitialFacebookAd = null;

    // AdColony
//    private AdColonyInterstitial _adColonyInterstitial = null;
    private InterstitialAdItem _interstitialAdTnk = null;

    private InterstitialAdItem _interstitialAdRewardTnk = null;

    private String placementVideoId = "video";
//    private String placementRewardVideoId = "rewardedVideo";


    private String _newspicUrl = "";
    private int _newspic_no = 0;

    private static int _ad_try_count = 0;
    private static int _ad_view_count = 0;


    public int get_ad_try_count_increase() {
        return _ad_try_count++;
    }


    private static Context _context = null;
    private static Activity _activity = null;

    private static boolean _is_initialize = false;

    public static int _adLoadingTimeInterval = 10;
    private static int _adLoadingTimeInterval_facebook = _adLoadingTimeInterval * 2;

    public AdFullOrgManager(Activity activity, Context context) {
        _activity = activity;
        _context = context;

        _adLoadingTimeInterval = SPUtil.getInstance().getAdLoadingTimeInterval(getContext());
    }

//    public void setIsCaulyStatic(boolean isCaulyStatic) {
//        _isCaulyStatic = isCaulyStatic;
//    }

    private boolean _isShowCauly = false;

    public void setActivity(Activity activity) {
        _activity = activity;
    }

    private Activity getActivity() {
        return _activity;
    }

    public void setContext(Context context) {
        _context = context;
    }

    private Context getContext() {
        return _context;
    }


    private static InterfaceSet.OnAdFullManagerListener _onAdFullManagerListener = null;

    public void setOnAdFullManagerListener(InterfaceSet.OnAdFullManagerListener l) {
        _onAdFullManagerListener = l;
    }

    private InterfaceSet.OnAdCompleteListener _onAdCompleteListener = null;

    public void setOnAdCompleteListener(InterfaceSet.OnAdCompleteListener l) {
        _onAdCompleteListener = l;
    }

    private InterfaceSet.OnAdCompleteListener _onAdCompleteMissionListener = null;

    public void setOnAdCompleteMissionListener(InterfaceSet.OnAdCompleteListener l) {
        _onAdCompleteMissionListener = l;
    }

    public boolean isReadyCauly() {
//        if (_interstialCaulyAd != null && _interstialCaulyAd.canShow()) return true;

        return false;
    }


    public void removeCauly() {
//        if (_interstialCaulyAd != null) {
//            _interstialCaulyAd.setInterstialAdListener(null);
//            _interstialCaulyAd.cancel();
//        }
//        _interstialCaulyAd = null;
        _isShowCauly = false;
    }

    public void initAd(Constants.enum_ad failLoadAd) {


        int count = canShowAdCount(false);
        LogUtil.d(TAG, "_txtAd, initAd, canShowAdCount : " + count);

        if (count > 0) return;

        initAd(failLoadAd, false);
    }

    public void initAd(Constants.enum_ad failLoadAd, boolean isFirstInit) {


        LogUtil.d(TAG, "_txtAd, initAd, _is_initialize[1] : " + _is_initialize);


        if (_is_initialize == false) {
            initialize();
        }

        LogUtil.d(TAG, "_txtAd, initAd, _is_initialize[2] : " + _is_initialize);


        String version = SPUtil.getInstance().getInitAdVersion(getContext());

//        initAd_v3(failLoadAd, isFirstInit);

//        if (version.equals("v1")) initAd_v1(failLoadAd, isFirstInit);
//        else if (version.equals("v2")) initAd_v2(failLoadAd, isFirstInit);
//        else if (version.equals("v3")) initAd_v3(failLoadAd, isFirstInit);

//        else if (version.equals("v4"))


        //if(_ad_try_count % 2 == 0)
        initAd_v1(failLoadAd, isFirstInit);
//        else
//            initAd_v4(failLoadAd, isFirstInit);
    }


    private void initialize() {
        try {


            new Thread(
                    () -> {
                        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
                            @Override
                            public void onInitializationComplete(InitializationStatus initializationStatus) {
                                if (initializationStatus != null) {
                                    Map<String, AdapterStatus> statusMap = initializationStatus.getAdapterStatusMap();
                                    for (String adapter : statusMap.keySet()) {
                                        AdapterStatus status = statusMap.get(adapter);

                                        LogUtil.d(String.format("adapter name : %s, desc : %s, latancy : %d", adapter, status.getDescription(), status.getLatency()));
                                    }
                                }
                            }
                        });
                    })
                    .start();


        } catch (Exception e) {
            e.printStackTrace();
        }


//        try {
//            if (Build.VERSION.SDK_INT != 26 && !AudienceNetworkAds.isInitialized(getContext())) {
//                AudienceNetworkAds
//                        .buildInitSettings(getContext())
//                        .withInitListener(new AudienceNetworkAds.InitListener() {
//                            @Override
//                            public void onInitialized(AudienceNetworkAds.InitResult initResult) {
//                                LogUtil.d("_txtAd", "AudienceNetworkAds initResult : " + (initResult == null ? "null" : initResult.toString()));
//                            }
//                        })
//                        .initialize();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        try {
            if (Constants.DISPLAY_AD_TYPE_MAX) {
                AppLovinSdk.getInstance(getContext()).setMediationProvider("max");
                AppLovinSdk.initializeSdk(getContext(), new AppLovinSdk.SdkInitializationListener() {
                    @Override
                    public void onSdkInitialized(AppLovinSdkConfiguration config) {
                        LogUtil.d("AppLovinSdk, onSdkInitialized, config : " + (config == null ? "null" : config.toString()));
                    }
                });

                AppLovinPrivacySettings.setHasUserConsent(true, getContext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            // v3.0.0 이상 버전
//            if (AdPopcornSSP.isInitialized(getContext())) {
//                // Already SSP SDK initialized
//                LogUtil.d("AdPopcornSSP, isInitialized");
//            } else {
//                AdPopcornSSP.init(getContext());
//                LogUtil.d("AdPopcornSSP, init");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        _is_initialize = true;
    }


    private Queue<Constants.enum_ad> _ad_queue_load = new LinkedList();

    private Constants.enum_ad getIndexEnumAdType(int index) {

        JSONArray jsonArray = SPUtil.getInstance().getAdSeqFull_V4(getContext());

//        if(Constants.DEBUG){
//            for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject json = JSONUtil.getJSONObject(jsonArray, i);
//                String ad_name = JSONUtil.getString(json, "ad_name");
//                LogUtil.d(TAG,"_txtAd, getIndexEnumAdType : " + i + ", : " + ad_name);
//            }
//        }


        if (_ad_queue_load.size() < 1) {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = JSONUtil.getJSONObject(jsonArray, i);
                String ad_name = JSONUtil.getString(json, "ad_name");
                _ad_queue_load.offer(Constants.getEnumAd(ad_name));
            }
        }

        Constants.enum_ad enum_ad_type = _ad_queue_load.poll();
        LogUtil.d(TAG, "_txtAd, _ad_queue_load.size(1) : " + _ad_queue_load.size());
        _ad_queue_load.offer(enum_ad_type);
        LogUtil.d(TAG, "_txtAd, _ad_queue_load.size(2) : " + _ad_queue_load.size());

        LogUtil.d(TAG, "_txtAd, getIndexEnumAdType : " + enum_ad_type.name());

        return enum_ad_type;

//        JSONObject json = JSONUtil.getJSONObject(jsonArray, index);
//
//        String ad_name = JSONUtil.getString(json, "ad_name");
//
//        LogUtil.d("_txtAd, getIndexEnumAdType : index : " + index + ", ad_name : " + ad_name);
//
//        return Constants.getEnumAd(ad_name);
    }


    private void initAd_v1(Constants.enum_ad failLoadAd, boolean isFirstInit) {
        JSONObject json = SPUtil.getInstance().getVoteAdInfo(getContext());

        LogUtil.d(TAG, "_txtAd initAd_v1, failLoadAd : " + failLoadAd.toString() + ", isFirstInit : " + isFirstInit);

        LogUtil.d(TAG, "_txtAd, json : " + (json == null ? "null" : json.toString()));

        if (failLoadAd == Constants.enum_ad.none) {

            LogUtil.d(TAG, "_txtAd, initAd_v1[1]");


//            if ((_isShowCauly && _interstialCaulyAd != null) || isFirstInit) {
            if (isFirstInit) {
                removeCauly();
            }
            if (_onAdFullManagerListener != null)
                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
//            }
        } else {
            LogUtil.d(TAG, "_txtAd, initAd_v1[2]");

            int count = this.canShowAdCount(false);
            LogUtil.d(TAG, "_txtAd canShowAdCount : " + count);

            //if(count > 0) {
            return;
            //}
        }

//        if (Constants.DEBUG) {
//
////            if ((_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) && failLoadAd != Constants.enum_ad.cauly_full) {
////                createCaulyAd();
////            }
//
//            createWadFull();
//
//            createAdPopcornReward();
//            createAdPopcornRewardFull();
//            createAdPopcornFull();
//            return;
//        }


        int google_full_screen = JSONUtil.getInteger(json, "google_full_screen", 0);
        int google_reward = JSONUtil.getInteger(json, "google_reward", 1);
        int unity_ads = JSONUtil.getInteger(json, "unity_ads", 1);

        int ad_count = unity_ads + google_full_screen + google_reward;

        LogUtil.d(TAG, "_txtAd, initAd_v1[3]");

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            LogUtil.d(TAG, "_txtAd, initAd_v1[3-1]");
            //if (Constants.IS_VIEW_AD_GOOGLE == true && _rewardedInterstitialAd == null && failLoadAd != Constants.enum_ad.g_reward_full) {
            LogUtil.d(TAG, "_txtAd, initAd_v1[3-1-1]");
            createGoogleRewardFull();
            //}
            //if (Constants.IS_VIEW_AD_GOOGLE == true && (_rewardedAd == null) && failLoadAd != Constants.enum_ad.g_reward) {
            LogUtil.d(TAG, "_txtAd, initAd_v1[3-1-2]");
            createGoogleReward();
            //}
            return;
        }

        LogUtil.d(TAG, "_txtAd, initAd_v1[4]");

        if (canShowAdCount(false) > 1) return;



        if (Constants.IS_VIEW_AD_GOOGLE == true && (_interstitialAd == null) && failLoadAd != Constants.enum_ad.g_full_screen) {
            createGoogleFull();
        }
        int sleepTime = 650;
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
        if (canShowAdCount(false) > 1) return;
        //
        if (Constants.IS_VIEW_AD_GOOGLE == true && _rewardedInterstitialAd == null && failLoadAd != Constants.enum_ad.g_reward_full) {
            createGoogleRewardFull();
        }

//                if (_ad_queue.contains(Constants.enum_ad.unityAds_full) == false) {
//                    createUnityAds();
//                }

//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
        if (canShowAdCount(false) > 1) return;
        //
        if ((_interstitialAdTnk == null || _interstitialAdTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_full) {
            createTnkFull();
        }

        if ((_interstitialFacebookAd == null || _interstitialFacebookAd.isAdLoaded() == false) && failLoadAd != Constants.enum_ad.facebook_full
                && SPUtil.getInstance().getUseFacebookYn(getContext()).equals("Y")) {
            createFacebookAd();
        }

        if (canShowAdCount(false) > 1) return;
        //
        if (Constants.IS_VIEW_AD_GOOGLE == true && (_rewardedAd == null) && failLoadAd != Constants.enum_ad.g_reward) {
            createGoogleReward();
        }
        if ((_maxRewardedAd == null || _maxRewardedAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_reward
                && SPUtil.getInstance().getUseMaxRewardYn(getContext()).equals("Y")) {
            createMaxReward();
        }


//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {


        if (canShowAdCount(false) > 1) return;

        if ((_maxInterstitialAd == null || _maxInterstitialAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_full
                && SPUtil.getInstance().getUseMaxYn(getContext()).equals("Y")) {
            createMaxFull();
        }
        if ((_interstitialAdRewardTnk == null || _interstitialAdRewardTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_reward) {
            createTnkReward();
        }


        if (canShowAdCount(false) > 1) return;
        if ((_adPopcornSSPInterstitialVideoAd == null || _adPopcornSSPInterstitialVideoAd.isReady() == false) && failLoadAd != Constants.enum_ad.adpc_reward_full) {
            createAdPopcornRewardFull();
        }

//        if ((_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) && failLoadAd != Constants.enum_ad.cauly_full) {
//            createCaulyAd();
//        }
//
        if ((_interstitialWad == null || _interstitialWad.isReady() == false) && failLoadAd != Constants.enum_ad.wad_full
                && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
            createWadFull();
        }


        if ((_adPopcornSSPInterstitialAd == null || _adPopcornSSPInterstitialAd.isLoaded() == false) && failLoadAd != Constants.enum_ad.adpc_full) {
            createAdPopcornFull();
        }

        if (canShowAdCount(false) > 1) return;
        if ((_adPopcornSSPRewardVideoAd == null || _adPopcornSSPRewardVideoAd.isReady() == false) && failLoadAd != Constants.enum_ad.adpc_reward) {
            createAdPopcornReward();
        }

//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if (canShowAdCount(false) > 1) return;
//                                        //
//
//
//
////                                        new Handler().postDelayed(new Runnable() {
////                                            @Override
////                                            public void run() {
////                                                if (canShowAdCount(false) > 1) return;
////                                                //
////
//////                                                if (canShowAdCount(false) > 1) return;
////                                                //
//////
////
//////
////
////                                            }
////                                        }, sleepTime);
//                                    }
//                                }, sleepTime);
//                            }
//                        }, sleepTime);
//                    }
//                }, sleepTime);
//            }
//        }, sleepTime);


        if (failLoadAd == Constants.enum_ad.none) {
            _ad_try_count++;
        }

        if (Constants.DEBUG) {
            LogUtil.d("_ad_try_count : " + _ad_try_count + ", " + FormatUtil.getCurrentDateHms());
        }

        LogUtil.d(TAG, "_txtAd initAd, _ad_try_count : " + _ad_try_count + ", unity_ads : " + unity_ads + ", google_full_screen : " + google_full_screen + ", google_reward : " + google_reward + ", failLoadAd : " + failLoadAd);
    }


//    private void initAd_v4(Constants.enum_ad failLoadAd, boolean isFirstInit) {
//        JSONObject json = SPUtil.getInstance().getVoteAdInfo(getContext());
//
//        LogUtil.d(TAG, "_txtAd initAd_v1, failLoadAd : " + failLoadAd.toString());
//
//        LogUtil.d(TAG, "_txtAd, json : " + (json == null ? "null" : json.toString()));
//
//        if (failLoadAd == Constants.enum_ad.none) {
//            //if ((_isShowCauly && _interstialCaulyAd != null) || isFirstInit) {
//            if (isFirstInit) {
//                removeCauly();
//            }
//            if (_onAdFullManagerListener != null)
//                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
//        } else {
//            int count = this.canShowAdCount(false);
//            LogUtil.d(TAG, "_txtAd canShowAdCount : " + count);
//
//            //if(count > 0) {
//            return;
//            //}
//        }
//
//
//        int google_full_screen = JSONUtil.getInteger(json, "google_full_screen", 0);
//        int google_reward = JSONUtil.getInteger(json, "google_reward", 1);
//        int unity_ads = JSONUtil.getInteger(json, "unity_ads", 1);
//
//        int ad_count = unity_ads + google_full_screen + google_reward;
//
//
//
//        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
//            if (Constants.IS_VIEW_AD_GOOGLE == true && _rewardedInterstitialAd == null && failLoadAd != Constants.enum_ad.g_reward_full) {
//                createGoogleRewardFull();
//            }
//            if (Constants.IS_VIEW_AD_GOOGLE == true && (_rewardedAd == null) && failLoadAd != Constants.enum_ad.g_reward) {
//                createGoogleReward();
//            }
//            if (Constants.IS_VIEW_AD_GOOGLE == true && (_interstitialAd == null) && failLoadAd != Constants.enum_ad.g_full_screen) {
//                createGoogleFull();
//            }
//            if ((_interstitialAdRewardTnk == null || _interstitialAdRewardTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_reward) {
//                createTnkReward();
//            }
//            if ((_maxRewardedAd == null || _maxRewardedAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_reward
//                    && SPUtil.getInstance().getUseMaxRewardYn(getContext()).equals("Y")) {
//                createMaxReward();
//            }
////            if ((_adPopcornSSPRewardVideoAd == null || _adPopcornSSPRewardVideoAd.isReady() == false) && failLoadAd != Constants.enum_ad.adpc_reward) {
////                createAdPopcornReward();
////            }
//            return;
//        }
//
//        if (canShowAdCount(false) > 1) return;
//        //
//        initAd_v4_body(getIndexEnumAdType(0), failLoadAd);
//        initAd_v4_body(getIndexEnumAdType(1), failLoadAd);
//        initAd_v4_body(getIndexEnumAdType(2), failLoadAd);
//        int sleepTime = 500;
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (canShowAdCount(false) > 1) return;
//                //
//                initAd_v4_body(getIndexEnumAdType(3), failLoadAd);
//                initAd_v4_body(getIndexEnumAdType(4), failLoadAd);
//                initAd_v4_body(getIndexEnumAdType(5), failLoadAd);
//
//
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (canShowAdCount(false) > 1) return;
//                        //
//                        initAd_v4_body(getIndexEnumAdType(6), failLoadAd);
//                        initAd_v4_body(getIndexEnumAdType(7), failLoadAd);
//                        initAd_v4_body(getIndexEnumAdType(8), failLoadAd);
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (canShowAdCount(false) > 1) return;
//                                //
//                                initAd_v4_body(getIndexEnumAdType(9), failLoadAd);
//                                initAd_v4_body(getIndexEnumAdType(10), failLoadAd);
//                                initAd_v4_body(getIndexEnumAdType(11), failLoadAd);
//                                initAd_v4_body(getIndexEnumAdType(12), failLoadAd);
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if (canShowAdCount(false) > 1) return;
//                                        //
//                                        initAd_v4_body(getIndexEnumAdType(13), failLoadAd);
//                                        initAd_v4_body(getIndexEnumAdType(14), failLoadAd);
//                                        initAd_v4_body(getIndexEnumAdType(15), failLoadAd);
//
//
//
//                                    }
//                                }, sleepTime);
//                            }
//                        }, sleepTime);
//                    }
//                }, sleepTime);
//            }
//        }, sleepTime);
//
//        // 중첩 10개까지만... 11개부터는 리젝되는 듯
//
//        if (failLoadAd == Constants.enum_ad.none) {
//            _ad_try_count++;
//        }
//
//        if (Constants.DEBUG) {
//            LogUtil.d("_ad_try_count : " + _ad_try_count + ", " + FormatUtil.getCurrentDateHms());
//        }
//
//        LogUtil.d(TAG, "_txtAd initAd, _ad_try_count : " + _ad_try_count + ", unity_ads : " + unity_ads + ", google_full_screen : " + google_full_screen + ", google_reward : " + google_reward + ", failLoadAd : " + failLoadAd);
//
//    }
////
////
//    private void initAd_v4_body(Constants.enum_ad ad_type, Constants.enum_ad failLoadAd) {
//
//        if (ad_type == Constants.enum_ad.g_reward_full) {
//            if (Constants.IS_VIEW_AD_GOOGLE == true && _rewardedInterstitialAd == null && failLoadAd != Constants.enum_ad.g_reward_full) {
//                createGoogleRewardFull();
//            }
//        } else if (ad_type == Constants.enum_ad.g_reward) {
//            if (Constants.IS_VIEW_AD_GOOGLE == true && (_rewardedAd == null) && failLoadAd != Constants.enum_ad.g_reward) {
//                createGoogleReward();
//            }
//        } else if (ad_type == Constants.enum_ad.g_full_screen) {
//            if (Constants.IS_VIEW_AD_GOOGLE == true && (_interstitialAd == null) && failLoadAd != Constants.enum_ad.g_full_screen) {
//                createGoogleFull();
//            }
//        } else if (ad_type == Constants.enum_ad.tnk_full) {
//            if ((_interstitialAdTnk == null || _interstitialAdTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_full) {
//                createTnkFull();
//            }
//        } else if (ad_type == Constants.enum_ad.tnk_reward) {
//            if ((_interstitialAdRewardTnk == null || _interstitialAdRewardTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_reward) {
//                createTnkReward();
//            }
//
//
//        } else if (ad_type == Constants.enum_ad.max_full) {
//            if ((_maxInterstitialAd == null || _maxInterstitialAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_full
//                    && SPUtil.getInstance().getUseMaxYn(getContext()).equals("Y")) {
//                createMaxFull();
//            }
//        } else if (ad_type == Constants.enum_ad.max_reward) {
//            if ((_maxRewardedAd == null || _maxRewardedAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_reward
//                    && SPUtil.getInstance().getUseMaxRewardYn(getContext()).equals("Y")) {
//                createMaxReward();
//            }
////        } else if (ad_type == Constants.enum_ad.unityAds_full) {
////            if (_ad_queue.contains(Constants.enum_ad.unityAds_full) == false) {
////                createUnityAds();
////            }
////        } else if (ad_type == Constants.enum_ad.facebook_full) {
////            if ((_interstitialFacebookAd == null || _interstitialFacebookAd.isAdLoaded() == false) && failLoadAd != Constants.enum_ad.facebook_full
////                    && SPUtil.getInstance().getUseFacebookYn(getContext()).equals("Y")) {
////                createFacebookAd();
////            }
//        } else if (ad_type == Constants.enum_ad.cauly_full) {
//            if ((_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) && failLoadAd != Constants.enum_ad.cauly_full) {
//                createCaulyAd();
//            }
//
//
////        } else if (ad_type == Constants.enum_ad.wad_full) {
////            if ((_interstitialWad == null || _interstitialWad.isReady() == false) && failLoadAd != Constants.enum_ad.wad_full
////                    && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
////                createWadFull();
////            }
//        } else if (ad_type == Constants.enum_ad.adpc_reward) {
//            if ((_adPopcornSSPRewardVideoAd == null || _adPopcornSSPRewardVideoAd.isReady() == false) && failLoadAd != Constants.enum_ad.adpc_reward) {
//                createAdPopcornReward();
//            }
//        } else if (ad_type == Constants.enum_ad.adpc_reward_full) {
//            if ((_adPopcornSSPInterstitialVideoAd == null || _adPopcornSSPInterstitialVideoAd.isReady() == false) && failLoadAd != Constants.enum_ad.adpc_reward_full) {
//                createAdPopcornRewardFull();
//            }
//        } else if (ad_type == Constants.enum_ad.adpc_full) {
//            if ((_adPopcornSSPInterstitialAd == null || _adPopcornSSPInterstitialAd.isLoaded() == false) && failLoadAd != Constants.enum_ad.adpc_full) {
//                createAdPopcornFull();
//            }
//        }
////        else if (ad_type == Constants.enum_ad.vungle_full) {
////            if (failLoadAd != Constants.enum_ad.vungle_full
////                    && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
////                createVungle_full();
////            }
////
////
////        } else if (ad_type == Constants.enum_ad.vungle_reward) {
////            if (failLoadAd != Constants.enum_ad.vungle_reward
////                    && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
////                createVungle_Reward();
////            }
////        }
//    }

//    private void initAd_v2(Constants.enum_ad failLoadAd, boolean isFirstInit) {
//
//        LogUtil.d(TAG, "_txtAd initAd_v2, failLoadAd : " + failLoadAd.toString());
//
//        if (failLoadAd == Constants.enum_ad.none) {
//            //if ((_isShowCauly && _interstialCaulyAd != null) || isFirstInit) {
//            if (isFirstInit) {
//                removeCauly();
//            }
//            if (_onAdFullManagerListener != null)
//                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
//        } else {
//            int count = this.canShowAdCount(false);
//            LogUtil.d(TAG, "_txtAd canShowAdCount : " + count);
//
//            //if(count > 0) {
//            return;
//            //}
//        }
//
//
//        if (Constants.IS_VIEW_AD_GOOGLE == true && (_interstitialAd == null) && failLoadAd != Constants.enum_ad.g_full_screen) {
//            createGoogleFull();
//        }
//
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//
//                JSONArray jsonArray = SPUtil.getInstance().getAdSeqFull(getContext());
//
//                if (_ad_try_count % 2 == 0) {
//                    jsonArray = SPUtil.getInstance().getAdSeqReward(getContext());
//                }
//
//                if (jsonArray != null && jsonArray.length() > 0) {
//                    boolean isShow = false;
//
//                    for (int i = 0; i < jsonArray.length(); i++) {
//                        JSONObject jsonAd = JSONUtil.getJSONObject(jsonArray, i);
//                        String seq = JSONUtil.getString(jsonAd, "seq");
//                        String ad_name = JSONUtil.getString(jsonAd, "ad_name");
//
//                        String log = "len : " + jsonArray.length() + ", seq : " + seq + ", ad_name : " + ad_name;
//                        LogUtil.d(TAG, "_txtAd initAd_v2, " + log);
//
//                        if (canShowAdCount(false) > 0) break;
//
////                        if (ad_name.equals(Constants.enum_ad.g_full_screen.name()) && Constants.IS_VIEW_AD_GOOGLE == true && (_interstitialAd == null) && failLoadAd != Constants.enum_ad.g_full_screen) {
////                            createGoogleFull();
////                        } else
//
//                        if (ad_name.equals(Constants.enum_ad.g_reward_full.name()) && Constants.IS_VIEW_AD_GOOGLE == true && _rewardedInterstitialAd == null && failLoadAd != Constants.enum_ad.g_reward_full) {
//                            createGoogleRewardFull();
//                        } else if (ad_name.equals(Constants.enum_ad.tnk_full.name()) && (_interstitialAdTnk == null || _interstitialAdTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_full) {
//                            createTnkFull();
//                        } else if (ad_name.equals(Constants.enum_ad.g_reward.name()) && Constants.IS_VIEW_AD_GOOGLE == true && _rewardedAd == null && failLoadAd != Constants.enum_ad.g_reward) {
//                            createGoogleReward();
//                        } else if (ad_name.equals(Constants.enum_ad.tnk_reward.name()) && (_interstitialAdRewardTnk == null || _interstitialAdRewardTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_reward) {
//                            createTnkReward();
//                        } else if (ad_name.equals(Constants.enum_ad.max_full.name()) && (_maxInterstitialAd == null || _maxInterstitialAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_full
//                                && SPUtil.getInstance().getUseMaxYn(getContext()).equals("Y")) {
//                            createMaxFull();
//                        } else if (ad_name.equals(Constants.enum_ad.unityAds_full.name()) && _ad_queue.contains(Constants.enum_ad.unityAds_full) == false) {
//                            createUnityAds();
//                        } else if (ad_name.equals(Constants.enum_ad.max_reward.name()) && (_maxRewardedAd == null || _maxRewardedAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_reward
//                                && SPUtil.getInstance().getUseMaxRewardYn(getContext()).equals("Y")) {
//                            createMaxReward();
//                        } else if (ad_name.equals(Constants.enum_ad.vungle_full.name()) && failLoadAd != Constants.enum_ad.vungle_full
//                                && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
//                            createVungle_full();
//                        } else if (ad_name.equals(Constants.enum_ad.vungle_reward.name()) && failLoadAd != Constants.enum_ad.vungle_reward
//                                && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
//                            createVungle_Reward();
//                        } else if (ad_name.equals(Constants.enum_ad.facebook_full.name()) && (_interstitialFacebookAd == null || _interstitialFacebookAd.isAdLoaded() == false) && failLoadAd != Constants.enum_ad.facebook_full
//                                && SPUtil.getInstance().getUseFacebookYn(getContext()).equals("Y")) {
//                            createFacebookAd();
//                        } else if (ad_name.equals(Constants.enum_ad.cauly_full.name()) && (_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) && failLoadAd != Constants.enum_ad.cauly_full) {
//                            createCaulyAd();
//                        } else if (ad_name.equals(Constants.enum_ad.wad_full.name()) && (_interstitialWad == null || _interstitialWad.isReady() == false) && failLoadAd != Constants.enum_ad.wad_full
//                                && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
//                            createWadFull();
//                        }
//
//                        try {
//                            Thread.sleep(300);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                    }
//
//                    if (failLoadAd == Constants.enum_ad.none) {
//                        _ad_try_count++;
//                    }
//
//                    if (Constants.DEBUG) {
//                        LogUtil.d("_txtAd, initAd_v2, _ad_try_count : " + _ad_try_count + ", " + FormatUtil.getCurrentDateHms());
//                    }
//
//                }
//            }
//        }).start();
//    }
//
//
//    private final Queue<Constants.enum_ad> _enum_ad_typeList = new LinkedList();
//
//    private void initAd_v3(Constants.enum_ad failLoadAd, boolean isFirstInit) {
//
//        if (_enum_ad_typeList.size() < 1) {
//            JSONArray jsonArray = SPUtil.getInstance().getAdSeqFull(getContext());
//
//            JSONArray jsonRewardArray = SPUtil.getInstance().getAdSeqReward(getContext());
//
//            for (int i = 0; i < jsonRewardArray.length(); i++) {
//                JSONObject jsonAd = JSONUtil.getJSONObject(jsonRewardArray, i);
//                JSONUtil.put(jsonArray, jsonAd, jsonArray.length());
//            }
//
//            for (int i = 0; i < jsonArray.length(); i++) {
//                JSONObject jsonAd = JSONUtil.getJSONObject(jsonArray, i);
//
//                String ad_name = JSONUtil.getString(jsonAd, "ad_name");
//
//                if (ad_name.equals(Constants.enum_ad.g_reward_full.name()) && Constants.IS_VIEW_AD_GOOGLE == true) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.g_reward_full);
//                } else if (ad_name.equals(Constants.enum_ad.tnk_full.name())) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.tnk_full);
//                } else if (ad_name.equals(Constants.enum_ad.g_reward.name()) && Constants.IS_VIEW_AD_GOOGLE == true) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.g_reward);
//                } else if (ad_name.equals(Constants.enum_ad.tnk_reward.name())) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.tnk_reward);
//                } else if (ad_name.equals(Constants.enum_ad.max_full.name()) && SPUtil.getInstance().getUseMaxYn(getContext()).equals("Y")) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.max_full);
//                } else if (ad_name.equals(Constants.enum_ad.unityAds_full.name())) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.unityAds_full);
//                } else if (ad_name.equals(Constants.enum_ad.max_reward.name()) && SPUtil.getInstance().getUseMaxRewardYn(getContext()).equals("Y")) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.max_reward);
//                } else if (ad_name.equals(Constants.enum_ad.vungle_full.name()) && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.vungle_full);
//                } else if (ad_name.equals(Constants.enum_ad.vungle_reward.name()) && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.vungle_reward);
//                } else if (ad_name.equals(Constants.enum_ad.facebook_full.name()) && SPUtil.getInstance().getUseFacebookYn(getContext()).equals("Y")) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.facebook_full);
//                } else if (ad_name.equals(Constants.enum_ad.cauly_full.name())) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.cauly_full);
//                } else if (ad_name.equals(Constants.enum_ad.wad_full.name()) && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
//                    _enum_ad_typeList.offer(Constants.enum_ad.wad_full);
//                }
//            }
//        }
//
//
//        LogUtil.d(TAG, "_txtAd initAd_v3, failLoadAd : " + failLoadAd.toString());
//
//        if (failLoadAd == Constants.enum_ad.none) {
//            //if ((_isShowCauly && _interstialCaulyAd != null) || isFirstInit) {
//            if (isFirstInit) {
//                removeCauly();
//            }
//            if (_onAdFullManagerListener != null)
//                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
//        } else {
//            int count = this.canShowAdCount(false);
//            LogUtil.d(TAG, "_txtAd canShowAdCount : " + count);
//
//            //if(count > 0) {
//            return;
//            //}
//        }
//
//        int sleepTime = 300;
//
//        LogUtil.d("_txtAd, _enum_ad_typeList.size(1) : " + _enum_ad_typeList.size());
//
//        Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//
//        LogUtil.d("_txtAd, _enum_ad_typeList.size(2) : " + _enum_ad_typeList.size());
//
//        createAd(ad_name, failLoadAd, isFirstInit);
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                if (canShowAdCount(false) > 0)
//                    return;
//                //
//
//                Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                createAd(ad_name, failLoadAd, isFirstInit);
//
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (canShowAdCount(false) > 0)
//                            return;
//                        //
//
//                        Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                        createAd(ad_name, failLoadAd, isFirstInit);
//
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                if (canShowAdCount(false) > 0)
//                                    return;
//                                //
//
//                                Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                createAd(ad_name, failLoadAd, isFirstInit);
//
//                                new Handler().postDelayed(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if (canShowAdCount(false) > 0)
//                                            return;
//                                        //
//
//                                        Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                        createAd(ad_name, failLoadAd, isFirstInit);
//
//                                        new Handler().postDelayed(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                if (canShowAdCount(false) > 0)
//                                                    return;
//                                                //
//
//                                                Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                                createAd(ad_name, failLoadAd, isFirstInit);
//
//                                                new Handler().postDelayed(new Runnable() {
//                                                    @Override
//                                                    public void run() {
//                                                        if (canShowAdCount(false) > 0)
//                                                            return;
//                                                        //
//
//                                                        Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                                        createAd(ad_name, failLoadAd, isFirstInit);
//
//                                                        new Handler().postDelayed(new Runnable() {
//                                                            @Override
//                                                            public void run() {
//                                                                if (canShowAdCount(false) > 0)
//                                                                    return;
//                                                                //
//
//                                                                Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                                                createAd(ad_name, failLoadAd, isFirstInit);
//
//                                                                new Handler().postDelayed(new Runnable() {
//                                                                    @Override
//                                                                    public void run() {
//                                                                        if (canShowAdCount(false) > 0)
//                                                                            return;
//                                                                        //
//
//                                                                        Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                                                        createAd(ad_name, failLoadAd, isFirstInit);
//
//                                                                        new Handler().postDelayed(new Runnable() {
//                                                                            @Override
//                                                                            public void run() {
//                                                                                if (canShowAdCount(false) > 0)
//                                                                                    return;
//                                                                                //
//
//                                                                                Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                                                                createAd(ad_name, failLoadAd, isFirstInit);
//
//                                                                                new Handler().postDelayed(new Runnable() {
//                                                                                    @Override
//                                                                                    public void run() {
//                                                                                        if (canShowAdCount(false) > 0)
//                                                                                            return;
//                                                                                        //
//
//                                                                                        Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                                                                        createAd(ad_name, failLoadAd, isFirstInit);
//
//                                                                                        new Handler().postDelayed(new Runnable() {
//                                                                                            @Override
//                                                                                            public void run() {
//                                                                                                if (canShowAdCount(false) > 0)
//                                                                                                    return;
//                                                                                                //
//
//                                                                                                Constants.enum_ad ad_name = _enum_ad_typeList.poll();
//                                                                                                createAd(ad_name, failLoadAd, isFirstInit);
//
//
//                                                                                            }
//                                                                                        }, sleepTime);
//                                                                                    }
//                                                                                }, sleepTime);
//                                                                            }
//                                                                        }, sleepTime);
//                                                                    }
//                                                                }, sleepTime);
//                                                            }
//                                                        }, sleepTime);
//                                                    }
//                                                }, sleepTime);
//                                            }
//                                        }, sleepTime);
//                                    }
//                                }, sleepTime);
//                            }
//                        }, sleepTime);
//                    }
//                }, sleepTime);
//            }
//        }, sleepTime);
//    }


//    private void createAd(Constants.enum_ad ad_name, Constants.enum_ad failLoadAd, boolean isFirstInit) {
//
//        LogUtil.d("_txtAd, createAd.ad_name : " + ad_name + ", failLoadAd : " + failLoadAd);
//
//        if (ad_name.equals(Constants.enum_ad.g_reward_full.name()) && Constants.IS_VIEW_AD_GOOGLE == true && _rewardedInterstitialAd == null && failLoadAd != Constants.enum_ad.g_reward_full) {
//            LogUtil.d("_txtAd, createAd[1] : " + ad_name);
//            createGoogleRewardFull();
//        } else if (ad_name.equals(Constants.enum_ad.tnk_full.name()) && (_interstitialAdTnk == null || _interstitialAdTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_full) {
//            LogUtil.d("_txtAd, createAd[2] : " + ad_name);
//            createTnkFull();
//        } else if (ad_name.equals(Constants.enum_ad.g_reward.name()) && Constants.IS_VIEW_AD_GOOGLE == true && _rewardedAd == null && failLoadAd != Constants.enum_ad.g_reward) {
//            LogUtil.d("_txtAd, createAd[3] : " + ad_name);
//            createGoogleReward();
//        } else if (ad_name.equals(Constants.enum_ad.tnk_reward.name()) && (_interstitialAdRewardTnk == null || _interstitialAdRewardTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_reward) {
//            LogUtil.d("_txtAd, createAd[4] : " + ad_name);
//            createTnkReward();
//        } else if (ad_name.equals(Constants.enum_ad.max_full.name()) && (_maxInterstitialAd == null || _maxInterstitialAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_full
//                && SPUtil.getInstance().getUseMaxYn(getContext()).equals("Y")) {
//            LogUtil.d("_txtAd, createAd[5] : " + ad_name);
//            createMaxFull();
//        } else if (ad_name.equals(Constants.enum_ad.unityAds_full.name()) && _ad_queue.contains(Constants.enum_ad.unityAds_full) == false) {
//            LogUtil.d("_txtAd, createAd[6] : " + ad_name);
//            createUnityAds();
//        } else if (ad_name.equals(Constants.enum_ad.max_reward.name()) && (_maxRewardedAd == null || _maxRewardedAd.isReady() == false) && failLoadAd != Constants.enum_ad.max_reward
//                && SPUtil.getInstance().getUseMaxRewardYn(getContext()).equals("Y")) {
//            LogUtil.d("_txtAd, createAd[7] : " + ad_name);
//            createMaxReward();
//        } else if (ad_name.equals(Constants.enum_ad.vungle_full.name()) && failLoadAd != Constants.enum_ad.vungle_full
//                && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
//            LogUtil.d("_txtAd, createAd[8] : " + ad_name);
//            createVungle_full();
//        } else if (ad_name.equals(Constants.enum_ad.vungle_reward.name()) && failLoadAd != Constants.enum_ad.vungle_reward
//                && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
//            LogUtil.d("_txtAd, createAd[9] : " + ad_name);
//            createVungle_Reward();
//        } else if (ad_name.equals(Constants.enum_ad.facebook_full.name()) && (_interstitialFacebookAd == null || _interstitialFacebookAd.isAdLoaded() == false) && failLoadAd != Constants.enum_ad.facebook_full
//                && SPUtil.getInstance().getUseFacebookYn(getContext()).equals("Y")) {
//            LogUtil.d("_txtAd, createAd[10] : " + ad_name);
//            createFacebookAd();
//        } else if (ad_name.equals(Constants.enum_ad.cauly_full.name()) && (_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) && failLoadAd != Constants.enum_ad.cauly_full) {
//            LogUtil.d("_txtAd, createAd[11] : " + ad_name);
//            createCaulyAd();
//        } else if (ad_name.equals(Constants.enum_ad.wad_full.name()) && (_interstitialWad == null || _interstitialWad.isReady() == false) && failLoadAd != Constants.enum_ad.wad_full
//                && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
//            LogUtil.d("_txtAd, createAd[12] : " + ad_name);
//            createWadFull();
//        }
//
//        _enum_ad_typeList.offer(ad_name);
//    }

    public void initAd_Only_Interstitial(Constants.enum_ad failLoadAd) {


        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            LogUtil.d(TAG, "_txtAd initAd_Only_Interstitial, false");
            return;
        }


        JSONObject json = SPUtil.getInstance().getVoteAdInfo(getContext());

        LogUtil.d(TAG, "_txtAd initAd, failLoadAd : " + failLoadAd.toString());

        LogUtil.d(TAG, "_txtAd, json : " + (json == null ? "null" : json.toString()));

        if (failLoadAd == Constants.enum_ad.none) {
            if (_onAdFullManagerListener != null)
                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
        } else {
            int count = this.canShowAdCount(false);
            LogUtil.d(TAG, "_txtAd canShowAdCount : " + count);

            //if(count > 0) {
            return;
            //}
        }

        int google_full_screen = JSONUtil.getInteger(json, "google_full_screen", 0);
        int google_reward = JSONUtil.getInteger(json, "google_reward", 1);
        int unity_ads = JSONUtil.getInteger(json, "unity_ads", 1);

        int ad_count = unity_ads + google_full_screen + google_reward;

        if (Constants.IS_VIEW_AD_GOOGLE == true && (_interstitialAd == null) && failLoadAd != Constants.enum_ad.g_full_screen) {
            createGoogleFull();
        }

        int sleepTime = 500;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (canShowAdCount(false) > 1) return;
                if ((_interstitialAdTnk == null || _interstitialAdTnk.isLoaded() == false) && failLoadAd != Constants.enum_ad.tnk_full) {
                    createTnkFull();
                }

                if (canShowAdCount(false) > 1) return;
//                if ((_interstitialWad == null || _interstitialWad.isReady() == false) && failLoadAd != Constants.enum_ad.wad_full && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
//                    createWadFull();
//                }
//
//                if ((_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) && failLoadAd != Constants.enum_ad.cauly_full) {
//                    createCaulyAd();
//                }

                //
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
////                        if (canShowAdCount(false) > 1) return;
//                if ((_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) && failLoadAd != Constants.enum_ad.cauly_full) {
//                    createCaulyAd();
//                }
//                        //
////                        new Handler().postDelayed(new Runnable() {
////                            @Override
////                            public void run() {
////
////                            }
////                        }, sleepTime);
//                    }
//                }, sleepTime);
            }
        }, sleepTime);


//        LogUtil.d(TAG, "_txt initAd, (UnityAds.isReady(placementVideoId) : " + (UnityAds.isReady(placementVideoId)));
//        LogUtil.d(TAG, "_txt initAd, (UnityAds.isReady(placementRewardVideoId) : " + (UnityAds.isReady(placementRewardVideoId)));


        if (failLoadAd == Constants.enum_ad.none) {
            _ad_try_count++;
        }
        LogUtil.d(TAG, "_txtAd initAd, _ad_try_count : " + _ad_try_count + ", unity_ads : " + unity_ads + ", google_full_screen : " + google_full_screen + ", google_reward : " + google_reward + ", failLoadAd : " + failLoadAd);
    }


    private int sleepAndCheckCount() {
        int count = 0;
        try {
            Thread.sleep(500);
        } catch (Exception e) {
            e.printStackTrace();
        }
        count = canShowAdCount(false);
        return count;
    }


    private boolean showTest(AdSize adSize) {

        checkAdCondition();

        if (Constants.DEBUG) {

//            if (_adPopcornSSPInterstitialVideoAd != null && _adPopcornSSPInterstitialVideoAd.isReady()) {
//                _adPopcornSSPInterstitialVideoAd.showAd();
//                return true;
//            }
//            if (_adPopcornSSPRewardVideoAd != null && _adPopcornSSPRewardVideoAd.isReady()) {
//                _adPopcornSSPRewardVideoAd.showAd();
//                return true;
//            }

//            if (_interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//                _interstitialVungleAd.play();
//                return true;
//            }
//
//            if (_rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//                _rewardedVungleAd.play();
//                return true;
//            }

//            if (_interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
//                _interstitialAdRewardTnk.show(getContext());
//                return true;
//            }

//            if(_interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
//                _interstitialAdTnk.show(getContext());
//                return true;
//            }
//
//            if(_interstitialAdTnk == null) {
//                createTnkFull();
//            }

//            showUnityAdsFull();

//            AlertAdSpecial alert = new AlertAdSpecial();
//            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                @Override
//                public void onClose(DialogInterface dialog, int which) {
//                    if (_onAdFullManagerListener != null) {
//
//                        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
//
//                        //_onAdFullManagerListener.setActiveAdText(enum_ad.none, false);
//                        _onAdFullManagerListener.setVotePointSync(userInfo, -1, -1);
//                    }
//                }
//            });
//            alert.showAdSpecial(getContext(), adSize);
//
//            if(true){
//                return true;
//            }

//            if (_adColonyInterstitial != null && _adColonyInterstitial.isExpired() == false) {
//                _last_ad = Constants.enum_ad.adcolony_full;
//                ad_log("_txtAd, adcolony_full show [test]");
//                _adColonyInterstitial.show();
//                return true;
//            }

//            LogUtil.d("_maxInterstitialAd.isReady() : " + (_maxInterstitialAd == null ? "null" : _maxInterstitialAd.isReady()));
//            LogUtil.d("_maxRewardedAd.isReady() : " + (_maxRewardedAd == null ? "null" : _maxRewardedAd.isReady()));
//
//            if (_maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
//                _last_ad = Constants.enum_ad.max_full;
//                ad_log("_txtAd, _maxInterstitialAd show [test]");
//                _maxInterstitialAd.showAd();
//                return true;
//            }
//
//            if (_maxRewardedAd != null && _maxRewardedAd.isReady()) {
//                _last_ad = Constants.enum_ad.max_reward;
//                ad_log("_txtAd, _maxRewardedAd show [test]");
//                _maxRewardedAd.showAd();
//                return true;
//            }

//            if (_interstitialWad != null && _interstitialWad.isReady()) {
//                _last_ad = enum_ad.wad_full;
//                ad_log("_txtAd, WAD show [test]");
//                _interstitialWad.show();
//                return true;
//            }

//            if(IronSource.isRewardedVideoAvailable()){
//                LogUtil.d(TAG, "_txtAd, IronSource RewardedVideo show [1-1]");
//                IronSource.showRewardedVideo("DefaultRewardedVideo");
//                return true;
//            }

//            if(IronSource.isOfferwallAvailable()){
//                LogUtil.d(TAG, "_txtAd, IronSource OfferWall show [1-1]");
//                IronSource.showOfferwall("DefaultOfferWall");
//                return true;
//            }

//            if(IronSource.isInterstitialReady()) {
//                LogUtil.d(TAG, "_txtAd, IronSource Interstitial show [1-1]");
//                IronSource.showInterstitial("DefaultInterstitial");
//                return true;
//            }


//            if (_interstialCaulyAd != null && _interstialCaulyAd.canShow()) {
//                LogUtil.d(TAG, "_txtAd, CaulyAd show [1-2]");
//                showCauly();
//                return true;
//            }
//            if (Vungle.canPlayAd(Constants.KEY_VUNGLE_FULL_PLACE_ID)) {
//                LogUtil.d(TAG, "_txtAd, Vungle full Ad show [1-3]");
//                showVungleFull();
//                return true;
//            }
//            if (Vungle.canPlayAd(Constants.KEY_VUNGLE_REWARD_PLACE_ID)) {
//                LogUtil.d(TAG, "_txtAd, Vungle reward Ad show [2-3]");
//                showVungleReward();
//                return true;
//            }
//            if (UnityAds.isReady(placementVideoId) ) {
//                LogUtil.d(TAG, "_txtAd, unityads show [T-Video]");
//                showUnityAdsFull();
//                return true;
//            }
//            if (UnityAds.isReady(placementRewardVideoId)) {
//                LogUtil.d(TAG, "_txtAd, unityads show [T-Reward]");
//                showUnityAdsReward();
//                return true;
//            }
//
//            if (_interstitialAd != null && _interstitialAd.isLoaded()) {
//                ad_log("_txtAd, google fullscreen show [T] - ");
//                _interstitialAd.show();
//                return true;
//            }
//            return true;

//            AlertAdSpecial alert = new AlertAdSpecial();
//            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                @Override
//                public void onClose(DialogInterface dialog, int which) {
//                    if (_onAdFullManagerListener != null) {
//
//                        JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
//
//                        //_onAdFullManagerListener.setActiveAdText(enum_ad.none, false);
//                        _onAdFullManagerListener.setVotePointSync(userInfo, -1, -1);
//                    }
//                }
//            });
//            alert.showAdSpecial(getContext(), adSize);
//            return true;
        }

        return false;
    }


    private void checkAdCondition() {

        if (Constants.DEBUG) {
            LogUtil.d(TAG, "_txtAd. google Ad full : " + (_interstitialAd == null ? "null" : "ok"));
            LogUtil.d(TAG, "_txtAd. google Ad full_reward : " + (_rewardedInterstitialAd == null ? "null" : "ok, "));
            LogUtil.d(TAG, "_txtAd. google Ad reward : " + (_rewardedAd == null ? "null" : "ok"));
//            LogUtil.d(TAG, "_txtAd. CaulyAd : " + (_interstialCaulyAd == null ? "null" : "ok, " + _interstialCaulyAd.canShow()));
            LogUtil.d(TAG, "_txtAd. Wad : " + (_interstitialWad == null ? "null" : "ok, " + _interstitialWad.isReady()));
            LogUtil.d(TAG, "_txtAd. TNK : " + (_interstitialAdTnk == null ? "null" : "ok, " + _interstitialAdTnk.isLoaded()));
            LogUtil.d(TAG, "_txtAd. TNK Reward : " + (_interstitialAdRewardTnk == null ? "null" : "ok, " + _interstitialAdRewardTnk.isLoaded()));

            LogUtil.d(TAG, "_txtAd. getUseCaulyYn : " + SPUtil.getInstance().getUseCaulyYn(getContext()));
            LogUtil.d(TAG, "_txtAd. getUseWadYn : " + SPUtil.getInstance().getUseWadYn(getContext()));

//            if (SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")) {
//                LogUtil.d(TAG, "_txtAd, Vungle, Vungle.isInitialized() : " + VungleAds.isInitialized());
//                LogUtil.d(TAG, "_txtAd, Vungle, _interstitialVungleAd : " + (_interstitialVungleAd == null ? "null" : "ok, " + _interstitialVungleAd.canPlayAd()));
//                LogUtil.d(TAG, "_txtAd, Vungle, _rewardedVungleAd : " + (_rewardedVungleAd == null ? "null" : "ok, " + _rewardedVungleAd.canPlayAd()));
//            }
        }
    }

    private static HashMap<Constants.enum_ad, Long> _enum_adLongHashMap = new HashMap<Constants.enum_ad, Long>();

    public static boolean canAdInitAvailable(Constants.enum_ad enumAdName) {
        return canAdInitAvailable(enumAdName, _adLoadingTimeInterval);
    }

    public static boolean canAdInitAvailable(Constants.enum_ad enumAdName, int seconds) {

        long initAdTime = -1;

        if (SPUtil.getInstance().getAdFullScreen(_context).equals("Y") == false) {
            return true;
        }

        try {
            if (_enum_adLongHashMap == null)
                _enum_adLongHashMap = new HashMap<Constants.enum_ad, Long>();

            if (_enum_adLongHashMap.containsKey(enumAdName)) {
                initAdTime = _enum_adLongHashMap.get(enumAdName);
                _enum_adLongHashMap.remove(enumAdName);
            }

//            LogUtil.d("_txtAd, canAdInitAvailable, seconds : " + seconds);
//            LogUtil.d("_txtAd, canAdInitAvailable, System.currentTimeMillis() : " + System.currentTimeMillis());
//            LogUtil.d("_txtAd, canAdInitAvailable, _initAdTime : " + initAdTime);
//            LogUtil.d("_txtAd, canAdInitAvailable, _initAdTime gap() : " + (System.currentTimeMillis() - initAdTime));

            if (initAdTime > 0 && System.currentTimeMillis() - initAdTime < seconds * 1000) {
                LogUtil.d("_txtAd, canAdInitAvailable false, " + enumAdName.name() + ", remain : " + ((System.currentTimeMillis() - initAdTime) / 1000)
                        + ", seconds : " + seconds);
                return false;
            }

            initAdTime = System.currentTimeMillis();

            if (_enum_adLongHashMap.containsKey(enumAdName)) {
                _enum_adLongHashMap.remove(enumAdName);
            }

            if (!_enum_adLongHashMap.containsKey(enumAdName)) {
                _enum_adLongHashMap.put(enumAdName, initAdTime);
            }

            LogUtil.d("_txtAd, canAdInitAvailable true, " + enumAdName.name());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    private void ad_log(String msg) {
        if (Constants.DEBUG) {
            LogUtil.d(TAG, msg);
            Alert.toastLong(_context, TAG + ", " + msg);
        }
    }

    public int canShowSimpleAdCount() {
        int count = 0;

        // 심사중일때
        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") == false) {
            return count;
        }
//        if (_interstialCaulyAd != null && _interstialCaulyAd.canShow()) {
//            count++;
//        }
//        if (_interstitialWad != null && _interstitialWad.isReady()) {
//            count++;
//        }
        if (_maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            count++;
        }
        if (_interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            count++;
        }
        return count;
    }

    public void showSimpleAd() {
//        if (_interstialCaulyAd != null && _interstialCaulyAd.canShow()) {
//            showCauly();
//        } else if (_interstitialWad != null && _interstitialWad.isReady()) {
//            _interstitialWad.show();
//        } else
        if (_maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            _maxInterstitialAd.showAd();
        } else if (_interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            _interstitialAdTnk.show(getContext());
        }
    }


    public void show(AdSize adSize, boolean isFromCharge) {

        try {
            JSONObject json = SPUtil.getInstance().getVoteAdInfo(getContext());

            int google_full_screen = JSONUtil.getInteger(json, "google_full_screen", 2);
            int google_reward = JSONUtil.getInteger(json, "google_reward", 10);
            int unity_ads = JSONUtil.getInteger(json, "unity_ads", 1);
            int ad_try_count = _ad_try_count - 1;

            int ad_count = google_full_screen + google_reward;
            int repeatPpomkkiAd = SPUtil.getInstance().getRepeatPpomkkiAd(getContext());

            LogUtil.d(TAG, "_txtAd, unity_ads : " + unity_ads);
            LogUtil.d(TAG, "_txtAd, google_full_screen : " + google_full_screen);
            LogUtil.d(TAG, "_txtAd, google_reward : " + google_reward);

            LogUtil.d(TAG, "_txtAd, ad_try_count : " + ad_try_count);
            LogUtil.d(TAG, "_txtAd, ad_count : " + ad_count);
            LogUtil.d(TAG, "_txtAd, repeatPpomkkiAd : " + repeatPpomkkiAd);

            LogUtil.d(TAG, "_txtAd, ad_try_count % ad_count : " + (ad_try_count % ad_count));
            LogUtil.d(TAG, "_txtAd, _ad_try_count % repeatPpomkkiAd : " + (_ad_try_count % repeatPpomkkiAd));


            if (showTest(adSize) == true) {
                return;
            }

            if (canShowAdCount(isFromCharge) <= 1) {
                // 재생가능한 광고가 하나일때는 무조건 재생되도록 이전 재생 광고 이름을 초기화
                _last_ad = Constants.enum_ad.none;
            }


            // 심사중일때...
            if (!SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                if (_rewardedInterstitialAd != null) {
                    ad_log("_txtAd, g reward full show [x-1]");
                    showGoogleRewardFull();
                } else if (_rewardedAd != null) {
                    ad_log("_txtAd, g reward show [x-2]");
                    showGoogleRewardAd();
                } else if (_interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
                    ad_log("_txtAd, g reward show [x-3]");
                    _interstitialAdRewardTnk.show(getContext());
//                } else if (_interstitialFacebookAd != null && _interstitialFacebookAd.isAdLoaded()) {
//                    ad_log("_txtAd, facebook show [x-3]");
//                    _interstitialFacebookAd.show();
//                } else if (SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//                    ad_log("_txtAd, unityads reward show [x-4]");
//                    showUnityAdsReward();
//                } else if (SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && _ad_queue.contains(Constants.enum_ad.unityAds_full)) {
//                    ad_log("_txtAd, unityads reward show [x-5]");
//                    showUnityAdsFull();
//                } else if (_interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//                    ad_log("_txtAd, CaulyAd show [x-4]");
//                    showCauly();
//                } else if (SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && _ad_queue.contains(Constants.enum_ad.unityAds_full)) {
//                    ad_log("_txtAd, unityads reward show [x-5]");
//                    showUnityAdsFull();
//                } else if ( _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//                    ad_log("_txtAd, Vungle reward Ad show [x-6]");
//                    showVungleReward();
//                } else if (_interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//                    ad_log("_txtAd, Vungle full Ad show [x-7]");
//                    showVungleFull();
                } else if (_maxRewardedAd != null && _maxRewardedAd.isReady()) {
                    ad_log("_txtAd, _maxRewardedAd show [x-8]");
                    _maxRewardedAd.showAd();
                } else if (_maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
                    ad_log("_txtAd, _maxInterstitialAd show [x-9]");
                    _maxInterstitialAd.showAd();
//                } else if (_adColonyInterstitial != null && _adColonyInterstitial.isExpired() == false) {
//                    ad_log("_txtAd, adColony show [x-10]");
//                    _adColonyInterstitial.show();
                } else if (_interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
                    ad_log("_txtAd, _interstitialWad show [x-10]");
                    _interstitialWad.show();
                } else {
                    LogUtil.d(TAG, "The rewarded ad wasn't loaded yet.");
                    Alert.toastLong(getContext(), getContext().getString(R.string.alert_ad_no_fill));
                }
            } else {
                // 실제 운영중일때~
                LogUtil.d("getActivity() : " + getActivity());
                LogUtil.d("getActivity().isFinishing() : " + getActivity().isFinishing());


                if (_ad_try_count % repeatPpomkkiAd == (repeatPpomkkiAd - 1)) {
                    _ad_try_count++;

                    JSONArray list = SPUtil.getInstance().getAdBannerFullList(getContext());
                    int randomInt = new Random().nextInt(100);
                    LogUtil.d(TAG, "randomInt : " + randomInt);
                    if (list.length() > 0 && randomInt < 15) {
                        AlertAdHouse alert = new AlertAdHouse();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {

                                callApi_vote_put_ticket_ad_point("ad_house");

                                if (_onAdFullManagerListener != null) {
                                    JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
                                    //_onAdFullManagerListener.setActiveAdText(enum_ad.none, false);
                                    _onAdFullManagerListener.setVotePointSync(userInfo, -1, -1);
                                }
                            }
                        });
                        alert.showAd(getContext());
                    } else {
                        AlertAdSpecial alert = new AlertAdSpecial();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (_onAdFullManagerListener != null) {
                                    JSONObject userInfo = SPUtil.getInstance().getUserInfo(getContext());
                                    //_onAdFullManagerListener.setActiveAdText(enum_ad.none, false);
                                    _onAdFullManagerListener.setVotePointSync(userInfo, -1, -1);
                                }

                                if (which == Alert.BUTTON4) {
                                    callApi_newspic_get_newspic_info("READ_NEWSPIC_C");
                                }

                                if (dialog != null) dialog.dismiss();
                            }
                        });
                        alert.showAdSpecial(getActivity(), adSize);
                    }
                    return;
                } else if ((ad_try_count % ad_count) < google_full_screen) {

                    JSONArray jsonArray = SPUtil.getInstance().getAdSeqFull(getContext());

                    if (jsonArray != null && jsonArray.length() > 0) {
                        boolean isShow = false;

                        int index = ad_try_count < 1 ? 0 : ad_try_count % jsonArray.length();

                        LogUtil.d(TAG, "_txtAd, full ad index : " + index + ", ad_try_count : " + ad_try_count);

                        for (int i = index; i < jsonArray.length(); i++) {
                            JSONObject jsonAd = JSONUtil.getJSONObject(jsonArray, i);
                            String seq = JSONUtil.getString(jsonAd, "seq");
                            String ad_name = JSONUtil.getString(jsonAd, "ad_name");

                            String log = "len : " + jsonArray.length() + ", seq : " + seq + ", ad_name : " + ad_name;
                            LogUtil.d(TAG, "_txtAd, " + log);

                            if (ad_name.equals(Constants.enum_ad.g_reward_full.name())) {
                                if (_last_ad != Constants.enum_ad.g_reward_full && _rewardedInterstitialAd != null) {
                                    _last_ad = Constants.enum_ad.g_reward_full;
                                    ad_log("_txtAd, google _rewardFull show [1] - " + log);
                                    showGoogleRewardFull();
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.g_reward.name())) {
                                if (_last_ad != Constants.enum_ad.g_reward && _rewardedAd != null) {
                                    _last_ad = Constants.enum_ad.g_reward;
                                    ad_log("_txtAd, google g_reward show [2] - " + log);
                                    showGoogleRewardAd();
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.tnk_reward.name())) {
                                if (_last_ad != Constants.enum_ad.tnk_reward && _interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
                                    _last_ad = Constants.enum_ad.tnk_reward;
                                    ad_log("_txtAd, _interstitialAdRewardTnk show [3] - " + log);
                                    _interstitialAdRewardTnk.show(getContext());
                                    _interstitialAdRewardTnk = null;
                                    isShow = true;
                                    break;
                                }
//                            } else if (ad_name.equals(Constants.enum_ad.cauly_full.name())) {
//                                if (_last_ad != Constants.enum_ad.cauly_full && _interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//                                    _last_ad = Constants.enum_ad.cauly_full;
//                                    ad_log("_txtAd, CaulyAd show [4] - " + log + ", _isShowCauly " + _isShowCauly);
//                                    showCauly();
//                                    isShow = true;
//                                    break;
//                                }
                            } else if (ad_name.equals(Constants.enum_ad.facebook_full.name())) {
                                if (_last_ad != Constants.enum_ad.facebook_full && _interstitialFacebookAd != null && _interstitialFacebookAd.isAdLoaded()) {
                                    _last_ad = Constants.enum_ad.facebook_full;
                                    ad_log("_txtAd, facebook_full show [5] - " + log);
                                    _interstitialFacebookAd.show();
                                    isShow = true;
                                    break;
                                }
//                            } else if (ad_name.equals(Constants.enum_ad.adcolony_full.name())) {
//                                if (_last_ad != Constants.enum_ad.adcolony_full && _adColonyInterstitial != null && _adColonyInterstitial.isExpired() == false) {
//                                    _last_ad = Constants.enum_ad.adcolony_full;
//                                    ad_log("_txtAd, facebook_full show [6] - " + log);
//                                    _adColonyInterstitial.show();
//                                    isShow = true;
//                                    break;
//                                }

                            } else if (ad_name.equals(Constants.enum_ad.tnk_full.name())) {
                                if (_last_ad != Constants.enum_ad.tnk_full && _interstitialAdTnk != null) {
                                    _last_ad = Constants.enum_ad.tnk_full;
                                    ad_log("_txtAd, tnk_full show [6] - " + log);
                                    _interstitialAdTnk.show(getContext());
                                    _interstitialAdTnk = null;
                                    isShow = true;
                                    break;
                                }

//                            } else if (ad_name.equals(Constants.enum_ad.unityAds_full.name())) {
//                                if (_last_ad != Constants.enum_ad.unityAds_full && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")
//                                        //&& UnityAds.isReady(placementVideoId)
//                                        && _ad_queue.contains(Constants.enum_ad.unityAds_full)
//                                ) {
//                                    _last_ad = Constants.enum_ad.unityAds_full;
//                                    ad_log("_txtAd, unityAds_full show [7] - " + log);
//                                    showUnityAdsFull();
//                                    isShow = true;
//                                    break;
//                                }
//                            } else if (ad_name.equals(Constants.enum_ad.unityAds_reward.name())) {
//                                if (_last_ad != Constants.enum_ad.unityAds_reward && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//                                    _last_ad = Constants.enum_ad.unityAds_reward;
//                                    ad_log("_txtAd, unityAds_reward show [7] - " + log);
//                                    showUnityAdsReward();
//                                    isShow = true;
//                                    break;
//                                }
//                            } else if (ad_name.equals(Constants.enum_ad.vungle_full.name())) {
//                                if (_last_ad != Constants.enum_ad.vungle_full && _interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//                                    _last_ad = Constants.enum_ad.vungle_full;
//                                    ad_log("_txtAd, vungle_full Ad show [8] - " + log);
//                                    showVungleFull();
//                                    isShow = true;
//                                    break;
//                                }
//                            } else if (ad_name.equals(Constants.enum_ad.vungle_reward.name())) {
//                                if (_last_ad != Constants.enum_ad.vungle_reward && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//                                    _last_ad = Constants.enum_ad.vungle_reward;
//                                    ad_log("_txtAd, vungle_reward Ad show [9] - " + log);
//                                    showVungleReward();
//                                    isShow = true;
//                                    break;
//                                }

                            } else if (ad_name.equals(Constants.enum_ad.g_full_screen.name())) {
                                if (_last_ad != Constants.enum_ad.g_full_screen && _interstitialAd != null) {
                                    _last_ad = Constants.enum_ad.g_full_screen;
                                    ad_log("_txtAd, google fullscreen show [11] - " + log);
                                    _interstitialAd.show(getActivity());
                                    _interstitialAd = null;
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.max_reward.name())) {
                                if (_last_ad != Constants.enum_ad.max_reward && (_maxRewardedAd != null && _maxRewardedAd.isReady())) {
                                    _last_ad = Constants.enum_ad.max_reward;
                                    ad_log("_txtAd, _maxRewardedAd show [12] - " + log);
                                    _maxRewardedAd.showAd();
                                    _maxRewardedAd = null;
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.max_full.name())) {
                                if (_last_ad != Constants.enum_ad.max_full && (_maxInterstitialAd != null && _maxInterstitialAd.isReady())) {
                                    _last_ad = Constants.enum_ad.max_full;
                                    ad_log("_txtAd, _maxInterstitialAd show [13] - " + log);
                                    _maxInterstitialAd.showAd();
                                    _maxInterstitialAd = null;
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.wad_full.name())) {
                                if (_last_ad != Constants.enum_ad.wad_full && _interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
                                    _last_ad = Constants.enum_ad.wad_full;
                                    ad_log("_txtAd, WAD show [14] - " + log);
                                    _interstitialWad.show();
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.adpc_reward.name())) {
                                if (_last_ad != Constants.enum_ad.adpc_reward && (_adPopcornSSPRewardVideoAd != null && _adPopcornSSPRewardVideoAd.isReady())) {
                                    _last_ad = Constants.enum_ad.adpc_reward;
                                    ad_log("_txtAd, adpc_reward show [15] - " + log);
                                    _adPopcornSSPRewardVideoAd.showAd();
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.adpc_reward_full.name())) {
                                if (_last_ad != Constants.enum_ad.adpc_reward_full && (_adPopcornSSPInterstitialVideoAd != null && _adPopcornSSPInterstitialVideoAd.isReady())) {
                                    _last_ad = Constants.enum_ad.adpc_reward_full;
                                    ad_log("_txtAd, adpc_reward_full show [16] - " + log);
                                    _adPopcornSSPInterstitialVideoAd.showAd();
                                    isShow = true;
                                    break;
                                }
                            } else if (ad_name.equals(Constants.enum_ad.adpc_full.name())) {
                                if (_last_ad != Constants.enum_ad.adpc_full && (_adPopcornSSPInterstitialAd != null && _adPopcornSSPInterstitialAd.isLoaded())) {
                                    _last_ad = Constants.enum_ad.adpc_full;
                                    ad_log("_txtAd, adpc_full show [17] - " + log);
                                    _adPopcornSSPInterstitialAd.showAd();
                                    isShow = true;
                                    break;
                                }
                            }
                        }

                        if (isShow == false) {
                            showAdOrder(isFromCharge);
                        }

                    } else {
                        if (_last_ad != Constants.enum_ad.g_full_screen && _interstitialAd != null) {
                            _last_ad = Constants.enum_ad.g_full_screen;
                            ad_log("_txtAd, google fullscreen show [1-1]");
                            _interstitialAd.show(getActivity());
                            _interstitialAd = null;
                        } else if (_last_ad != Constants.enum_ad.tnk_full && _interstitialAdTnk != null) {
                            _last_ad = Constants.enum_ad.tnk_full;
                            ad_log("_txtAd, tnk_full show [1-2]");
                            _interstitialAdTnk.show(getContext());
                            _interstitialAdTnk = null;
//                        } else if (_last_ad != Constants.enum_ad.cauly_full && _interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//                            _last_ad = Constants.enum_ad.cauly_full;
//                            ad_log("_txtAd, CaulyAd show [1-2-1]");
//                            showCauly();
                        } else if (_last_ad != Constants.enum_ad.wad_full && _interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
                            _last_ad = Constants.enum_ad.wad_full;
                            ad_log("_txtAd, WAD show [1-3]");
                            _interstitialWad.show();
//                        } else if (_last_ad != Constants.enum_ad.unityAds_full && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")
//                                //        && UnityAds.isReady(placementVideoId)
//                                && _ad_queue.contains(Constants.enum_ad.unityAds_full)
//                        ) {
//                            _last_ad = Constants.enum_ad.unityAds_full;
//                            ad_log("_txtAd, unityads full show [1-4]");
//                            showUnityAdsFull();
//                        } else if (_last_ad != Constants.enum_ad.vungle_full && _interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//                            _last_ad = Constants.enum_ad.vungle_full;
//                            ad_log("_txtAd, Vungle full Ad show [1-5]");
//                            showVungleFull();
                        } else if (_last_ad != Constants.enum_ad.g_reward_full && _rewardedInterstitialAd != null) {
                            _last_ad = Constants.enum_ad.g_reward_full;
                            ad_log("_txtAd, google _rewardFull show [1-6]");
                            showGoogleRewardFull();
                        } else if (_last_ad != Constants.enum_ad.facebook_full && _interstitialFacebookAd != null && _interstitialFacebookAd.isAdLoaded()) {
                            _last_ad = Constants.enum_ad.facebook_full;
                            ad_log("_txtAd, facebook_full show [1-7]");
                            _interstitialFacebookAd.show();
//                        } else if (_last_ad != Constants.enum_ad.adcolony_full && _adColonyInterstitial != null && _adColonyInterstitial.isExpired() == false) {
//                            _last_ad = Constants.enum_ad.adcolony_full;
//                            ad_log("_txtAd, adcolony_full show [1-8]");
//                            _adColonyInterstitial.show();


                        } else {
                            _last_ad = Constants.enum_ad.none;
                            showAdOrder(isFromCharge);
                        }
                    }
                } else if (((ad_try_count % ad_count) < google_full_screen + google_reward)) {

                    JSONArray jsonArray = SPUtil.getInstance().getAdSeqReward(getContext());

                    if (jsonArray != null && jsonArray.length() > 0) {

                        int index = ad_try_count < 1 ? 0 : ad_try_count % jsonArray.length();

                        LogUtil.d(TAG, "_txtAd, reward ad index : " + index + ", ad_try_count : " + ad_try_count);

                        boolean isShow = false;
                        for (int i = index; i < jsonArray.length(); i++) {
                            JSONObject jsonAd = JSONUtil.getJSONObject(jsonArray, i);
                            String seq = JSONUtil.getString(jsonAd, "seq");
                            String ad_name = JSONUtil.getString(jsonAd, "ad_name");

                            String log = "len : " + jsonArray.length() + ", seq : " + seq + ", ad_name : " + ad_name;
                            LogUtil.d(TAG, log);

                            if (ad_name.equals(Constants.enum_ad.g_reward.name())) {
                                if (_last_ad != Constants.enum_ad.g_reward && _rewardedAd != null) {
                                    _last_ad = Constants.enum_ad.g_reward;
                                    ad_log("_txtAd, google _rewardedAd show [0-1] - " + log);
                                    showGoogleRewardAd();
                                    isShow = true;
                                    break;
                                }
//                            } else if (ad_name.equals(Constants.enum_ad.unityAds_reward.name())) {
//                                if (_last_ad != Constants.enum_ad.unityAds_reward && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//                                    _last_ad = Constants.enum_ad.unityAds_reward;
//                                    ad_log("_txtAd, unityads reward show [0-1] - " + log);
//                                    showUnityAdsReward();
//                                    isShow = true;
//                                    break;
//                                }
                            } else if (ad_name.equals(Constants.enum_ad.tnk_reward.name())) {
                                if (_last_ad != Constants.enum_ad.tnk_reward && _interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
                                    _last_ad = Constants.enum_ad.tnk_reward;
                                    ad_log("_txtAd, _interstitialAdRewardTnk show [0-2] - " + log);
                                    _interstitialAdRewardTnk.show();
                                    _interstitialAdRewardTnk = null;
                                    isShow = true;
                                    break;
                                }
                            }

//                            else if (ad_name.equals(Constants.enum_ad.vungle_reward.name())) {
//                                if (_last_ad != Constants.enum_ad.vungle_reward && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//                                    _last_ad = Constants.enum_ad.vungle_reward;
//                                    ad_log("_txtAd, Vungle reward Ad show [0-3] - " + log);
//                                    showVungleReward();
//                                    isShow = true;
//                                    break;
//                                }
//                            }
                        }
                        if (isShow == false) {
                            showAdOrderReward(isFromCharge);
                        }
                    } else {
                        if (_last_ad != Constants.enum_ad.g_reward && _rewardedAd != null) {
                            _last_ad = Constants.enum_ad.g_reward;
                            ad_log("_txtAd, google _rewardedAd show [2-1]");
                            showGoogleRewardAd();
//                        } else if (_last_ad != Constants.enum_ad.unityAds_reward && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//                            _last_ad = Constants.enum_ad.unityAds_reward;
//                            ad_log("_txtAd, unityads reward show [2-2]");
//                            showUnityAdsReward();
                        } else if (_last_ad != Constants.enum_ad.tnk_reward && _interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
                            _last_ad = Constants.enum_ad.tnk_reward;
                            ad_log("_txtAd, tnk_reward show [2-2]");
                            _interstitialAdRewardTnk.show(getContext());
                            _interstitialAdRewardTnk = null;
//                        } else if (_last_ad != Constants.enum_ad.vungle_reward && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//                            _last_ad = Constants.enum_ad.vungle_reward;
//                            ad_log("_txtAd, Vungle reward Ad show [2-3]");
//                            showVungleReward();
                        } else {
                            showAdOrderReward(isFromCharge);
                        }
                    }
                } else {
                    showAdOrder(isFromCharge);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    private void showUnityAdsFull() {
//
//        _last_ad = Constants.enum_ad.unityAds_full;
//
//        UnityAds.show(getActivity(), placementVideoId, new UnityAdsShowOptions(), new IUnityAdsShowListener() {
//            @Override
//            public void onUnityAdsShowFailure(String placementId, UnityAds.UnityAdsShowError error, String message) {
//                LogUtil.d(TAG, "_txt, onUnityAdsShowFailure: " + error + " - " + message);
//            }
//
//            @Override
//            public void onUnityAdsShowStart(String placementId) {
//                LogUtil.d(TAG, "_txt, onUnityAdsShowStart: " + placementId);
//            }
//
//            @Override
//            public void onUnityAdsShowClick(String placementId) {
//                LogUtil.d(TAG, "_txt, onUnityAdsShowClick: " + placementId);
//            }
//
//            @Override
//            public void onUnityAdsShowComplete(String placementId, UnityAds.UnityAdsShowCompletionState state) {
//                LogUtil.d(TAG, "_txt, onUnityAdsShowComplete: " + placementId);
//                //initAd(Constants.enum_ad.none);
//                callApi_vote_put_ticket_ad_point(Constants.enum_ad.unityAds_full.name());
//            }
//        });
//
//        _ad_queue.remove(Constants.enum_ad.unityAds_full);
//    }


//    private void showVungleFull() {
//
//        if (_interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//            _interstitialVungleAd.play();
//        }
//
//    }

//    private void showVungleReward() {
//
//        if (_rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//            _rewardedVungleAd.play();
//        }
//
//    }


    private void showCauly() {
        if (_isShowCauly) {
            removeCauly();
            return;
        }
        _isShowCauly = true;
//        _interstialCaulyAd.show();
//        _interstialCaulyAd = null;
    }


//    // Implement the IUnityAdsListener interface methods:
//    private class UnityAdsListener implements IUnityAdsListener {
//
//        public void onUnityAdsReady(String placementId) {
//            // Implement functionality for an ad being ready to show.
//            LogUtil.d(TAG, "_txtAd, UnityAdsListener, onUnityAdsReady, placementId : " + placementId);
//
//            if (placementId.equals(placementVideoId)) {
//                _ad_queue.offer(Constants.enum_ad.unityAds_full);
//            } else {
//                _ad_queue.offer(Constants.enum_ad.unityAds_reward);
//            }
//
//            if (_onAdFullManagerListener != null) {
//                if (placementId.equals(placementVideoId)) {
//                    _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.unityAds_full, true);
//                } else {
//                    _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.unityAds_reward, true);
//                }
//            }
//        }
//
//        @Override
//        public void onUnityAdsStart(String placementId) {
//            // Implement functionality for a user starting to watch an ad.
//            LogUtil.d(TAG, "_txtAd, UnityAdsListener, onUnityAdsStart, placementId : " + placementId);
//        }
//
//        @Override
//        public void onUnityAdsFinish(String placementId, UnityAds.FinishState finishState) {
//            // Implement conditional logic for each ad completion status:
//            if (finishState.equals(UnityAds.FinishState.COMPLETED)) {
//                // Reward the user for watching the ad to completion.
//                LogUtil.d(TAG, "_txtAd, UnityAdsListener, onUnityAdsFinish, COMPLETED, _ad_try_count : " + _ad_try_count);
//                callApi_vote_put_ticket_ad_point((placementId.equals(placementVideoId)) ? Constants.enum_ad.unityAds_full.name() : Constants.enum_ad.unityAds_reward.name());
//                LogUtil.d(TAG, "_txt, UnityAdsListener => onUnityAdsFinish, COMPLETED");
//                initAd(Constants.enum_ad.none);
//            } else if (finishState.equals(UnityAds.FinishState.SKIPPED)) {
//                // Do not reward the user for skipping the ad.
//                LogUtil.d(TAG, "_txtAd, UnityAdsListener, onUnityAdsFinish, SKIPPED, _ad_try_count : " + _ad_try_count);
//                callApi_vote_put_ticket_ad_point((placementId.equals(placementVideoId)) ? Constants.enum_ad.unityAds_full.name() : Constants.enum_ad.unityAds_reward.name());
//                LogUtil.d(TAG, "_txt, UnityAdsListener => onUnityAdsFinish, SKIPPED");
//                initAd(Constants.enum_ad.none);
//            } else if (finishState.equals(UnityAds.FinishState.ERROR)) {
//                // Log an error.
//                LogUtil.d(TAG, "_txtAd, UnityAdsListener, onUnityAdsFinish, ERROR, _ad_try_count : " + _ad_try_count);
//
//                if (placementId.equals(placementVideoId))
//                    initAd(Constants.enum_ad.unityAds_full);
//                else
//                    initAd(Constants.enum_ad.unityAds_reward);
//            }
//
//
//        }
//
//        @Override
//        public void onUnityAdsError(UnityAds.UnityAdsError error, String message) {
//            LogUtil.d("UnityAdsListener, onUnityAdsError, error : " + error.toString(), ", message : " + message);
//        }
//    }


    public void createGoogleReward() {

        LogUtil.d(TAG, "_txtAd, createGoogleReward[1]");

//        if (_last_ad == Constants.enum_ad.g_reward) return;

        if (canAdInitAvailable(Constants.enum_ad.g_reward, _adLoadingTimeInterval * 3) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createGoogleReward[2]");

        RewardedAd rewardedAd = null;
        try {
            if (getContext() == null) {
                LogUtil.d(TAG, "_txtAd, createGoogleReward, getContext is null");
                return;
            }

//            rewardedAd = new RewardedAd(getContext(), Constants.KEY_AD_REWARD);
//            RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
//                @Override
//                public void onRewardedAdLoaded() {
//                    // Ad successfully loaded.
//                    LogUtil.d(TAG, "onReward- onRewardedAdLoaded");
//
//                    _ad_queue.offer(Constants.enum_ad.g_reward);
//
//                    if (_onAdFullManagerListener != null)
//                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.g_reward, true);
//                }
//
//                @Override
//                public void onRewardedAdFailedToLoad(int errorCode) {
//                    // Ad failed to load.
//                    LogUtil.d(TAG, "onReward- onRewardedAdFailedToLoad");
//                    LogUtil.d(TAG, "_txt, createAndLoadRewardedAd => onRewardedAdFailedToLoad");
//                    initAd(Constants.enum_ad.g_reward);
//                }
//            };

            RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
                @Override
                public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                    super.onAdFailedToLoad(loadAdError);
                    LogUtil.d(TAG, "_txt, createGoogleReward => onAdFailedToLoad");
                    //initAd(Constants.enum_ad.g_reward);

                    _rewardedAd = null;


                }

                @Override
                public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                    super.onAdLoaded(rewardedAd);

                    LogUtil.d(TAG, "onReward- createGoogleReward, onAdLoaded");

                    _ad_queue.offer(Constants.enum_ad.g_reward);

                    if (_onAdFullManagerListener != null)
                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.g_reward, true);

                    _rewardedAd = rewardedAd;

                    FullScreenContentCallback fullScreenContentCallback =
                            new FullScreenContentCallback() {
                                @Override
                                public void onAdShowedFullScreenContent() {
                                    // Code to be invoked when the ad showed full screen content.
                                    LogUtil.d(TAG, "createGoogleReward- onAdShowedFullScreenContent");
                                }

                                @Override
                                public void onAdDismissedFullScreenContent() {
                                    _rewardedAd = null;
                                    LogUtil.d(TAG, "createGoogleReward- onAdDismissedFullScreenContent");

                                    //callApi_vote_put_ticket_ad_point(Constants.enum_ad.g_reward.name());

                                    //initAd(Constants.enum_ad.none);
                                }
                            };

                    _rewardedAd.setFullScreenContentCallback(fullScreenContentCallback);
                }
            };

            RewardedAd.load(getContext(), Constants.getKeyAdGoogleRewardID(false, SPUtil.getInstance().getChargeMode(getContext())), new AdRequest.Builder().build(), adLoadCallback);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void createGoogleFull() {

        if (_last_ad == Constants.enum_ad.g_full_screen) return;

        if (canAdInitAvailable(Constants.enum_ad.g_full_screen) == false) return;

        if (!SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) return;

        LogUtil.d(TAG, "_txtAd, createGoogleFull");

        try {

            if (getContext() == null) {
                LogUtil.d(TAG, "_txtAd, createGoogleFull, getContext is null");
                return;
            }

//            MediationTestSuite.launch(getContext());

//            Bundle extras = new FacebookExtras()
//                    .setNativeBanner(false)
//                    .build();


            String[] placements = new String[2];
            placements[0] = Constants.KEY_VUNGLE_FULL_PLACE_ID;
            placements[1] = Constants.KEY_VUNGLE_REWARD_PLACE_ID;
            Bundle extras_vungle = new VungleExtrasBuilder(placements).build();


            //extras.putString("max_ad_content_rating", "T");

            AdRequest request = new AdRequest.Builder()

                    .addNetworkExtrasBundle(UnityAdapter.class, new Bundle())
                    .addNetworkExtrasBundle(VungleAdapter.class, extras_vungle)
                    .addNetworkExtrasBundle(VungleInterstitialAdapter.class, extras_vungle)
//                    .addNetworkExtrasBundle(FacebookAdapter.class, extras)
                    .build();

            InterstitialAd.load(getContext(), Constants.getKeyAdGoogleFullScreenID(false, SPUtil.getInstance().getChargeMode(getContext())), request,
                    new InterstitialAdLoadCallback() {
                        @Override
                        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                            // The mInterstitialAd reference will be null until
                            // an ad is loaded.
                            _interstitialAd = interstitialAd;
                            LogUtil.d(TAG, "_txtAd, google full, onAdLoaded");

                            _ad_queue.offer(Constants.enum_ad.g_full_screen);

                            if (_onAdFullManagerListener != null)
                                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.g_full_screen, true);

                            _interstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                                @Override
                                public void onAdClicked() {
                                    // Called when a click is recorded for an ad.
                                    LogUtil.d(TAG, "_txtAd, google full, Ad was clicked.");
                                }

                                @Override
                                public void onAdDismissedFullScreenContent() {
                                    // Called when ad is dismissed.
                                    // Set the ad reference to null so you don't show the ad a second time.
                                    LogUtil.d(TAG, "_txtAd, google full,  Ad dismissed fullscreen content.");
                                    _interstitialAd = null;

                                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.g_full_screen.name());

                                    //initAd(Constants.enum_ad.none);
                                }

                                @Override
                                public void onAdFailedToShowFullScreenContent(AdError adError) {
                                    // Called when ad fails to show.
                                    LogUtil.d(TAG, "_txtAd, google full,  Ad failed to show fullscreen content.");
                                    _interstitialAd = null;
                                }

                                @Override
                                public void onAdImpression() {
                                    // Called when an impression is recorded for an ad.
                                    LogUtil.d(TAG, "_txtAd, google full,  Ad recorded an impression.");
                                }

                                @Override
                                public void onAdShowedFullScreenContent() {
                                    // Called when ad is shown.
                                    LogUtil.d(TAG, "_txtAd, google full,  Ad showed fullscreen content.");
                                }
                            });
                        }

                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            // Handle the error
                            LogUtil.d(TAG, "_txtAd, google full, onAdFailedToLoad, error : " + loadAdError.toString());
                            _interstitialAd = null;
                            //initAd(Constants.enum_ad.g_full_screen);

                        }
                    }
            );

        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//
//    private com.vungle.ads.InterstitialAd _interstitialVungleAd;
//    private com.vungle.ads.RewardedAd _rewardedVungleAd;
//
//    private void createVungle_full() {
//
//        if (_last_ad == Constants.enum_ad.vungle_full) return;
//
//        if (canAdInitAvailable(Constants.enum_ad.vungle_full) == false) return;
//
//        if (VungleAds.isInitialized()) {
//
//            AdConfig adConfig = new AdConfig();
//            adConfig.setAdOrientation(AdConfig.PORTRAIT);
//            adConfig.setBackButtonImmediatelyEnabled(true);
//
//            _interstitialVungleAd = new com.vungle.ads.InterstitialAd(getContext(), Constants.KEY_VUNGLE_FULL_PLACE_ID, adConfig);
//            _interstitialVungleAd.setAdListener(new FullscreenAdListener() {
//                @Override
//                public void onAdLoaded(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdLoaded, id : " + baseAd.getCreativeId());
//
//                    _ad_queue.offer(Constants.enum_ad.vungle_full);
//
//                    if (_onAdFullManagerListener != null)
//                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.vungle_full, true);
//                }
//
//                @Override
//                public void onAdStart(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdStart");
//                }
//
//                @Override
//                public void onAdImpression(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdImpression");
//                }
//
//                @Override
//                public void onAdEnd(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdEnd");
//
//                    _interstitialVungleAd = null;
//
//                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.vungle_full.name());
//
//                    //initAd(Constants.enum_ad.none);
//                }
//
//                @Override
//                public void onAdClicked(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdClicked");
//                }
//
//                @Override
//                public void onAdLeftApplication(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdLeftApplication");
//                }
//
//                @Override
//                public void onAdFailedToLoad(@NonNull BaseAd baseAd, @NonNull VungleError vungleError) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdFailedToLoad, " + vungleError.getErrorMessage());
//                }
//
//                @Override
//                public void onAdFailedToPlay(@NonNull BaseAd baseAd, @NonNull VungleError vungleError) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_full, onAdFailedToPlay");
//                }
//            });
//
//            _interstitialVungleAd.load(null);
//
////            if (!VungleAds.canPlayAd(Constants.KEY_VUNGLE_FULL_PLACE_ID) && _last_ad == Constants.enum_ad.vungle_full) {
////
////                if (canAdInitAvailable(Constants.enum_ad.vungle_full)) {
////
////                    try {
////                        LogUtil.d(TAG, "_txtAd, createVungle_Full");
////
////                        Vungle.loadAd(Constants.KEY_VUNGLE_FULL_PLACE_ID, new LoadAdCallback() {
////                            @Override
////                            public void onAdLoad(String placementReferenceId) {
////                                LogUtil.d(TAG, "_txtAd, Vungle, onAdLoad, full placementReferenceId : " + placementReferenceId);
////
////                                _ad_queue.offer(Constants.enum_ad.vungle_full);
////
////                                if (_onAdFullManagerListener != null)
////                                    _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.vungle_full, true);
////                            }
////
////                            @Override
////                            public void onError(String placementReferenceId, VungleException exception) {
////                                LogUtil.d(TAG, "_txtAd, Vungle, onError, full placementReferenceId : " + placementReferenceId + ", exception : " + exception.toString());
////                            }
////                        });
////                    } catch (OutOfMemoryError e) {
////                        e.printStackTrace();
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////
////                }
////            }
//        }
//    }
//
//    private void createVungle_Reward() {
//
//        if (_last_ad == Constants.enum_ad.vungle_reward) return;
//
//        if (canAdInitAvailable(Constants.enum_ad.vungle_reward) == false) return;
//
//        if (VungleAds.isInitialized()) {
//
//            AdConfig adConfig = new AdConfig();
////            adConfig.setAdOrientation(AdConfig.PORTRAIT);
////            adConfig.setBackButtonImmediatelyEnabled(true);
//
//            _rewardedVungleAd = new com.vungle.ads.RewardedAd(getContext(), Constants.KEY_VUNGLE_REWARD_PLACE_ID, adConfig);
//
//            _rewardedVungleAd.setAdListener(new RewardedAdListener() {
//
//
//
//                @Override
//                public void onAdRewarded(@NonNull BaseAd baseAd) {
//                    _rewardedVungleAd = null;
//
//                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.vungle_reward.name());
//
//                    //initAd(Constants.enum_ad.none);
//                }
//
//                @Override
//                public void onAdLoaded(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdLoaded, id : " + baseAd.getCreativeId());
//
//                    _ad_queue.offer(Constants.enum_ad.vungle_reward);
//
//                    if (_onAdFullManagerListener != null)
//                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.vungle_reward, true);
//                }
//
//                @Override
//                public void onAdStart(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdStart");
//                }
//
//                @Override
//                public void onAdImpression(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdImpression");
//                }
//
//                @Override
//                public void onAdEnd(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdEnd");
//                }
//
//                @Override
//                public void onAdClicked(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdClicked");
//                }
//
//                @Override
//                public void onAdLeftApplication(@NonNull BaseAd baseAd) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdLeftApplication");
//                }
//
//                @Override
//                public void onAdFailedToLoad(@NonNull BaseAd baseAd, @NonNull VungleError vungleError) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdFailedToLoad, " + vungleError.getErrorMessage());
//                }
//
//                @Override
//                public void onAdFailedToPlay(@NonNull BaseAd baseAd, @NonNull VungleError vungleError) {
//                    LogUtil.d(TAG, "_txtAd, createVungle_Reward, onAdStart");
//                }
//            });
//            _rewardedVungleAd.load(null);
////            if (!Vungle.canPlayAd(Constants.KEY_VUNGLE_REWARD_PLACE_ID) && _last_ad == Constants.enum_ad.vungle_reward) {
////
////                if (canAdInitAvailable(Constants.enum_ad.vungle_reward)) {
////                    try {
////                        LogUtil.d(TAG, "_txtAd, createVungle_Reward");
////
////                        Vungle.loadAd(Constants.KEY_VUNGLE_REWARD_PLACE_ID, new LoadAdCallback() {
////                            @Override
////                            public void onAdLoad(String placementReferenceId) {
////                                LogUtil.d(TAG, "_txtAd, Vungle, onAdLoad, reward placementReferenceId : " + placementReferenceId);
////
////                                _ad_queue.offer(Constants.enum_ad.vungle_reward);
////
////                                if (_onAdFullManagerListener != null)
////                                    _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.vungle_reward, true);
////                            }
////
////                            @Override
////                            public void onError(String placementReferenceId, VungleException exception) {
////                                LogUtil.d(TAG, "_txtAd, Vungle, onError, reward placementReferenceId : " + placementReferenceId + ", exception : " + exception.toString());
////                            }
////                        });
////                    } catch (OutOfMemoryError e) {
////                        e.printStackTrace();
////                    } catch (Exception e) {
////                        e.printStackTrace();
////                    }
////
////                }
////            }
//        }
//    }


    private void createWadFull() {

        if (_last_ad == Constants.enum_ad.wad_full) return;
        if (canAdInitAvailable(Constants.enum_ad.wad_full, _adLoadingTimeInterval * 2) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createWadFull [s]");

        try {

//            if (_interstitialWad != null) {
//                _interstitialWad.destroy();
//            }

            _interstitialWad = new WInterstitial(getActivity(), Constants.KEY_WAD_APP_ID, Constants.KEY_WAD_APP_UNIT_ID);
            _interstitialWad.setTesting(false);
            _interstitialWad.setInterstitialAdListener(null);
            _interstitialWad.setInterstitialAdListener(new WInterstitial.InterstitialAdListener() {
                @Override
                public void onInterstitialLoaded(WInterstitial interstitial) {
                    //interstitial.show();
                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialLoaded");

                    _ad_queue.offer(Constants.enum_ad.wad_full);

                    if (_onAdFullManagerListener != null)
                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.wad_full, true);
                }

                @Override
                public void onInterstitialFailed(WInterstitial interstitial, WErrorCode errorCode) {
                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialFailed, errorCode : " + errorCode.toString());
                    if (_interstitialWad != null) {
                        _interstitialWad.destroy();
                    }
                    _interstitialWad = null;
                }

                @Override
                public void onInterstitialShown(WInterstitial interstitial) {
                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialShown");
                }

                @Override
                public void onInterstitialClicked(WInterstitial interstitial) {
                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialClicked");

                    if (_interstitialWad != null) {
                        _interstitialWad.destroy();
                    }
                    _interstitialWad = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.wad_full.name());
                }

                @Override
                public void onInterstitialDismissed(WInterstitial interstitial) {
                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialDismissed");

                    if (_interstitialWad != null) {
                        _interstitialWad.destroy();
                    }
                    _interstitialWad = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.wad_full.name());
                }
            });
            _interstitialWad.load();

            LogUtil.d(TAG, "_txtAd, createWadFull [e]");

        } catch (OutOfMemoryError e) {
            LogUtil.d(TAG, "_txtAd, createWadFull [e1]");
            e.printStackTrace();
        } catch (Exception e) {
            LogUtil.d(TAG, "_txtAd, createWadFull [e2]");
            e.printStackTrace();
            LogUtil.d(e.getMessage());
            LogUtil.d(e.toString());
        }

    }


    public void createTnkFull() {
        if (Constants.DISPLAY_AD_TYPE_TNK == false) return;
        if (_last_ad == Constants.enum_ad.tnk_full) return;
        if (canAdInitAvailable(Constants.enum_ad.tnk_full, _adLoadingTimeInterval * 2) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createTnkFull");

        try {
            _interstitialAdTnk = new InterstitialAdItem(getContext(), Constants.KEY_TNK_FULL, new AdListener() {
                @Override
                public void onLoad(AdItem adItem) {

                    LogUtil.d(TAG, "_txtAd, TNK onInterstitialLoaded");

                    _ad_queue.offer(Constants.enum_ad.tnk_full);

                    if (_onAdFullManagerListener != null)
                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.tnk_full, true);
                }

                @Override
                public void onError(AdItem adItem, com.tnkfactory.ad.AdError adError) {
                    super.onError(adItem, adError);

                    LogUtil.d(TAG, "_txtAd, TNK onInterstitialFailed, errorCode : " + adError.toString());
                    _interstitialAdTnk = null;

                    if (_onAdCompleteMissionListener != null) {
                        _onAdCompleteMissionListener.onAdLoadFail();
                    }
                }

                @Override
                public void onShow(AdItem adItem) {
                    super.onShow(adItem);
                }

                @Override
                public void onClick(AdItem adItem) {
                    super.onClick(adItem);

                    LogUtil.d(TAG, "_txtAd, TNK onClick");
                }

                @Override
                public void onClose(AdItem adItem, int i) {
                    super.onClose(adItem, i);

                    LogUtil.d(TAG, "_txtAd, TNK onClose");

                    _interstitialAdTnk = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.tnk_full.name());
                }


            });

            _interstitialAdTnk.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void createTnkReward() {
        if (Constants.DISPLAY_AD_TYPE_TNK == false) return;
        if (_last_ad == Constants.enum_ad.tnk_reward) return;
        if (canAdInitAvailable(Constants.enum_ad.tnk_reward, _adLoadingTimeInterval * 2) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createTnkReward");

        try {
            _interstitialAdRewardTnk = new InterstitialAdItem(getContext(), Constants.KEY_TNK_REWARD, new AdListener() {
                @Override
                public void onLoad(AdItem adItem) {

                    LogUtil.d(TAG, "_txtAd, TNK reward onInterstitialLoaded");

                    _ad_queue.offer(Constants.enum_ad.tnk_reward);

                    if (_onAdFullManagerListener != null)
                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.tnk_reward, true);
                }

                @Override
                public void onError(AdItem adItem, com.tnkfactory.ad.AdError adError) {
                    super.onError(adItem, adError);

                    LogUtil.d(TAG, "_txtAd, TNK reward onInterstitialFailed, errorCode : " + adError.toString());
                    _interstitialAdRewardTnk = null;

                    if (_onAdCompleteMissionListener != null) {
                        _onAdCompleteMissionListener.onAdLoadFail();
                    }
                }

                @Override
                public void onShow(AdItem adItem) {
                    super.onShow(adItem);
                }

                @Override
                public void onClick(AdItem adItem) {
                    super.onClick(adItem);

                    LogUtil.d(TAG, "_txtAd, TNK reward onClick");
                }

                @Override
                public void onClose(AdItem adItem, int i) {
                    super.onClose(adItem, i);

                    LogUtil.d(TAG, "_txtAd, TNK reward onClose");

                    _interstitialAdRewardTnk = null;


                }

                /**
                 * 동영상이 포함되어 있는 경우 동영상을 끝까지 시청하는 시점에 호출된다.
                 * @param adItem 이벤트 대상이되는 AdItem 객체
                 * @param verifyCode 동영상 시청 완료 콜백 결과.
                 */
                @Override
                public void onVideoCompletion(AdItem adItem, int verifyCode) {
                    super.onVideoCompletion(adItem, verifyCode);

                    LogUtil.d(TAG, "_txtAd, TNK onVideoCompletion, verifyCode : " + verifyCode);

                    if (verifyCode >= VIDEO_VERIFY_SUCCESS_SELF) {
                        // 적립 성공

                        //initAd(Constants.enum_ad.none);
                        callApi_vote_put_ticket_ad_point(Constants.enum_ad.tnk_reward.name());

                    } else {
                        // 적립 실패
                    }
                }

            });

            _interstitialAdRewardTnk.load();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private AdPopcornSSPInterstitialAd _adPopcornSSPInterstitialAd;
    private AdPopcornSSPInterstitialVideoAd _adPopcornSSPInterstitialVideoAd;
    private AdPopcornSSPRewardVideoAd _adPopcornSSPRewardVideoAd;


    public void createAdPopcornFull() {
        if (_last_ad == Constants.enum_ad.adpc_full) return;
        if (canAdInitAvailable(Constants.enum_ad.adpc_full, _adLoadingTimeInterval * 2) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createAdPopcornFull");

        AdPopcornSSPUserProperties userProperties = new AdPopcornSSPUserProperties.Builder()
                //.gender(2)
                .language("KR")
                .country("KO")
                .build();
        AdPopcornSSP.setUserProperties(userProperties);
        AdPopcornSSP.setUserId(getContext(), SPUtil.getInstance().getUserNoEnc(getContext()));

        try {
            _adPopcornSSPInterstitialAd = new AdPopcornSSPInterstitialAd(getContext());
            _adPopcornSSPInterstitialAd.setPlacementId(Constants.KEY_ADPC_FULL_ID);
            _adPopcornSSPInterstitialAd.setCurrentActivity(getActivity());
            _adPopcornSSPInterstitialAd.setInterstitialLoadEventCallbackListener(new IInterstitialLoadEventCallbackListener() {

                @Override
                public void OnInterstitialLoaded() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialLoaded");
                    _ad_queue.offer(Constants.enum_ad.adpc_full);

                    if (_onAdFullManagerListener != null)
                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.adpc_full, true);
                }


                @Override
                public void OnInterstitialReceiveFailed(SSPErrorCode errorCode) {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialReceiveFailed, errorCode : " + errorCode.getErrorCode() + "|" + errorCode.getErrorMessage());
                }
            });
            _adPopcornSSPInterstitialAd.setInterstitialShowEventCallbackListener(new IInterstitialShowEventCallbackListener() {

                @Override
                public void OnInterstitialOpened() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialOpened");
                }

                @Override
                public void OnInterstitialOpenFailed(SSPErrorCode errorCode) {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialOpenFailed, errorCode : " + errorCode.getErrorMessage());
                }

                @Override
                public void OnInterstitialClosed(int CloseEvent) {
                    // UNKNOWN = 0;
                    // CLICK_CLOSE_BTN = 1;
                    // PRESSED_BACK_KEY = 2;
                    // SWIPE_RIGHT_TO_LEFT = 3;
                    // SWIPE_LEFT_TO_RIGHT = 4;
                    // AUTO_CLOSE = 5;
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialClosed, CloseEvent : " + CloseEvent);

                    if (_adPopcornSSPInterstitialAd != null) {
                        _adPopcornSSPInterstitialAd.destroy();
                    }
                    _adPopcornSSPInterstitialAd = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.adpc_full.name());
                }

                @Override
                public void OnInterstitialClicked() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialClicked");
                }
            });
            _adPopcornSSPInterstitialAd.loadAd();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createAdPopcornRewardFull() {
        if (_last_ad == Constants.enum_ad.adpc_reward_full) return;
        if (canAdInitAvailable(Constants.enum_ad.adpc_reward_full, _adLoadingTimeInterval * 2) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createAdPopcornRewardFull");

        AdPopcornSSPUserProperties userProperties = new AdPopcornSSPUserProperties.Builder()
                //.gender(2)
                .language("KR")
                .country("KO")
                .build();
        AdPopcornSSP.setUserProperties(userProperties);
        AdPopcornSSP.setUserId(getContext(), SPUtil.getInstance().getUserNoEnc(getContext()));

        try {
            _adPopcornSSPInterstitialVideoAd = new AdPopcornSSPInterstitialVideoAd(getContext());
            _adPopcornSSPInterstitialVideoAd.setPlacementId(Constants.KEY_ADPC_REWARD_FULL_ID);
            _adPopcornSSPInterstitialVideoAd.setNetworkScheduleTimeout(10);

            _adPopcornSSPInterstitialVideoAd.setEventCallbackListener(new IInterstitialVideoAdEventCallbackListener() {
                @Override
                public void OnInterstitialVideoAdLoaded() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornRewardFull OnInterstitialVideoAdLoaded");

                    _ad_queue.offer(Constants.enum_ad.adpc_reward_full);

                    if (_onAdFullManagerListener != null)
                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.adpc_reward_full, true);
                }

                @Override
                public void OnInterstitialVideoAdLoadFailed(SSPErrorCode sspErrorCode) {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornRewardFull OnInterstitialVideoAdLoadFailed, sspErrorCode : " + sspErrorCode.getErrorCode() + "|" + sspErrorCode.getErrorMessage());
                }

                @Override
                public void OnInterstitialVideoAdOpened() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornRewardFull OnInterstitialVideoAdOpened");
                }

                @Override
                public void OnInterstitialVideoAdOpenFalied() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornRewardFull OnInterstitialVideoAdOpenFalied");
                }

                @Override
                public void OnInterstitialVideoAdClosed() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornRewardFull OnInterstitialVideoAdClosed");

                    if (_adPopcornSSPInterstitialVideoAd != null) {
                        _adPopcornSSPInterstitialVideoAd.destroy();
                    }
                    _adPopcornSSPInterstitialVideoAd = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.adpc_reward_full.name());
                }

                @Override
                public void OnInterstitialVideoAdClicked() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornRewardFull OnInterstitialVideoAdClicked");
                }
            });

            _adPopcornSSPInterstitialVideoAd.loadAd();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void createAdPopcornReward() {
        if (_last_ad == Constants.enum_ad.adpc_reward) return;
        if (canAdInitAvailable(Constants.enum_ad.adpc_reward, _adLoadingTimeInterval * 2) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createAdPopcornReward");

        AdPopcornSSPUserProperties userProperties = new AdPopcornSSPUserProperties.Builder()
                //.gender(2)
                .language("KR")
                .country("KO")
                .build();
        AdPopcornSSP.setUserProperties(userProperties);
        AdPopcornSSP.setUserId(getContext(), SPUtil.getInstance().getUserNoEnc(getContext()));

        try {
            _adPopcornSSPRewardVideoAd = new AdPopcornSSPRewardVideoAd(getContext());
            _adPopcornSSPRewardVideoAd.setPlacementId(Constants.KEY_ADPC_REWARD_ID);
            _adPopcornSSPRewardVideoAd.setNetworkScheduleTimeout(10);
            _adPopcornSSPRewardVideoAd.setRewardVideoAdEventCallbackListener(new IRewardVideoAdEventCallbackListener() {

                @Override
                public void OnRewardVideoAdLoaded() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoAdLoaded");

                    _ad_queue.offer(Constants.enum_ad.adpc_reward);

                    if (_onAdFullManagerListener != null)
                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.adpc_reward, true);
                }

                @Override
                public void OnRewardVideoAdLoadFailed(SSPErrorCode errorCode) {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoAdLoadFailed");

                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoAdLoadFailed, errorCode : " + errorCode.getErrorCode() + "|" + errorCode.toString());
                }


                @Override
                public void OnRewardVideoAdOpened() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoAdOpened");
                }

                @Override
                public void OnRewardVideoAdOpenFalied() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoAdOpenFalied");
                }

                @Override
                public void OnRewardVideoAdClosed() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoAdClosed");
                }

                @Override
                public void OnRewardVideoPlayCompleted(int adNetworkNo, boolean completed) {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoPlayCompleted, adNetworkNo : " + adNetworkNo + ", completed : " + completed);

                    if (_adPopcornSSPRewardVideoAd != null) {
                        _adPopcornSSPRewardVideoAd.destroy();
                    }
                    _adPopcornSSPRewardVideoAd = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.adpc_reward.name());
                }

                @Override
                public void OnRewardVideoAdClicked() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardVideoAdClicked");
                }

                @Override
                public void OnRewardPlusCompleted(boolean b, int i, int i1) {

                    LogUtil.d(TAG, "_txtAd, createAdPopcornReward OnRewardPlusCompleted, b : " + b + ", i : " + i + ", i1 : " + i1);

                    if (_adPopcornSSPRewardVideoAd != null) {
                        _adPopcornSSPRewardVideoAd.destroy();
                    }
                    _adPopcornSSPRewardVideoAd = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.adpc_reward.name());

                }
            });
            _adPopcornSSPRewardVideoAd.loadAd();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createGoogleRewardFull() {
        LogUtil.d(TAG, "_txtAd, createGoogleRewardFull[1]");

        //if (_last_ad == Constants.enum_ad.g_reward_full) return;
        if (canAdInitAvailable(Constants.enum_ad.g_reward_full, _adLoadingTimeInterval * 2) == false)
            return;

        LogUtil.d(TAG, "_txtAd, createGoogleRewardFull[2]");

        try {
            if (getContext() == null) return;
//            RewardedInterstitialAd.load(getContext(), Constants.KEY_AD_REWARD_FULL,
//                    new AdRequest.Builder().build(), new RewardedInterstitialAdLoadCallback() {
//
//                        @Override
//                        public void onRewardedInterstitialAdLoaded(@NonNull RewardedInterstitialAd ad) {
//                            _rewardedInterstitialAd = ad;
//                            if (_rewardedInterstitialAd != null) {
//                                _rewardedInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
//                                    /**
//                                     * Called when the ad failed to show full screen content.
//                                     */
//                                    @Override
//                                    public void onAdFailedToShowFullScreenContent(AdError adError) {
//                                        LogUtil.d(TAG, "_txtAd, onAdFailedToShowFullScreenContent");
//                                        _rewardedInterstitialAd = null;
//                                        showAdOrder(false);
//                                    }
//
//                                    /**
//                                     * Called when ad showed the full screen content.
//                                     */
//                                    @Override
//                                    public void onAdShowedFullScreenContent() {
//                                        LogUtil.d(TAG, "_txtAd, onAdShowedFullScreenContent");
//                                    }
//
//                                    /**
//                                     * Called when full screen content is dismissed.
//                                     */
//                                    @Override
//                                    public void onAdDismissedFullScreenContent() {
//                                        LogUtil.d(TAG, "_txtAd, onAdDismissedFullScreenContent");
//                                    }
//                                });
//                            }
//                        }
//
//
//                        @Override
//                        public void onRewardedInterstitialAdFailedToLoad(LoadAdError e) {
//                            LogUtil.d(TAG, "onRewardedInterstitialAdFailedToLoad, LoadAdError : " + e.toString());
//                        }
//                    });


            RewardedInterstitialAd.load(getContext(), Constants.getKeyAdGoogleRewardFullID(false, SPUtil.getInstance().getChargeMode(getContext())),
                    new AdRequest.Builder().build(), new RewardedInterstitialAdLoadCallback() {
                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            super.onAdFailedToLoad(loadAdError);

                            LogUtil.d(TAG, "_txtAd, createGoogleRewardFull onAdFailedToLoad");

                            _rewardedInterstitialAd = null;

                        }

                        @Override
                        public void onAdLoaded(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
                            super.onAdLoaded(rewardedInterstitialAd);

                            LogUtil.d(TAG, "_txtAd, createGoogleRewardFull onAdLoaded");

                            _rewardedInterstitialAd = rewardedInterstitialAd;

                            _ad_queue.offer(Constants.enum_ad.g_reward_full);

                            if (_onAdFullManagerListener != null)
                                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.g_reward_full, true);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //
    private void createFacebookAd() {

        LogUtil.d(TAG, "_txtAd, createFacebookAd");

        if (_last_ad == Constants.enum_ad.facebook_full) {
            LogUtil.d(TAG, "_txtAd, createFacebookAd, return [1]");
            return;
        }
        if (canAdInitAvailable(Constants.enum_ad.facebook_full, _adLoadingTimeInterval_facebook) == false) {
            LogUtil.d(TAG, "_txtAd, createFacebookAd, return [2]");
            return;
        }
        if (Build.VERSION.SDK_INT == 26) {
            LogUtil.d(TAG, "_txtAd, createFacebookAd, return [3]");
            return;
        }

        try {
            _interstitialFacebookAd = new com.facebook.ads.InterstitialAd(getActivity(), "877400656474635_877403363141031");
            // Create listeners for the Interstitial Ad
            InterstitialAdListener interstitialAdListener = new InterstitialAdListener() {

                @Override
                public void onInterstitialDisplayed(Ad ad) {
                    // Interstitial ad displayed callback
                    LogUtil.d("_txt, com.facebook.ads.InterstitialAd ad displayed.");
                }

                @Override
                public void onInterstitialDismissed(Ad ad) {
                    // Interstitial dismissed callback
                    LogUtil.d("_txt, com.facebook.ads.InterstitialAd ad dismissed.");
                    if (_interstitialFacebookAd != null) {
                        _interstitialFacebookAd.destroy();
                    }
                    _interstitialFacebookAd = null;

                    //initAd(Constants.enum_ad.none);
                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.facebook_full.name());
                }

                @Override
                public void onError(Ad ad, com.facebook.ads.AdError adError) {
                    LogUtil.d("_txt, com.facebook.ads.InterstitialAd ad onError :: " + adError.getErrorCode() + "::" + adError.getErrorMessage());
                    if (_interstitialFacebookAd != null) {
                        _interstitialFacebookAd.destroy();
                    }
                    _interstitialFacebookAd = null;

                    if (adError.getErrorCode() == 1002) {
                        //Ad was re-loaded too frequently
                        _adLoadingTimeInterval_facebook = _adLoadingTimeInterval * 20;
                    }
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    // Interstitial ad is loaded and ready to be displayed
                    LogUtil.d("_txt, com.facebook.ads.InterstitialAd ad onAdLoaded");
                    // Show the ad
                    _ad_queue.offer(Constants.enum_ad.facebook_full);
                }

                @Override
                public void onAdClicked(Ad ad) {
                    // Ad clicked callback
                    LogUtil.d("_txt, com.facebook.ads.InterstitialAd ad clicked!");
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                    // Ad impression logged callback
                    LogUtil.d("_txt, com.facebook.ads.InterstitialAd ad impression logged!");
                }
            };

            // For auto play video ads, it's recommended to load the ad
            // at least 30 seconds before it is shown
            if (_interstitialFacebookAd.isAdLoaded() == false) {
                _interstitialFacebookAd.loadAd(
                        _interstitialFacebookAd.buildLoadAdConfig()
                                .withAdListener(interstitialAdListener)
                                .build());
            }
            LogUtil.d(TAG, "_txtAd, createFacebookAd ok");
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


//    private void createUnityAds() {
//        if (_last_ad == Constants.enum_ad.unityAds_full) return;
//        if (canAdInitAvailable(Constants.enum_ad.unityAds_full) == false) return;
//
//        try {
//            UnityAds.load(placementVideoId, new IUnityAdsLoadListener() {
//                @Override
//                public void onUnityAdsAdLoaded(String placementId) {
//                    LogUtil.d(TAG, "_txtAd, onUnityAdsAdLoaded, Ad for " + placementId + " loaded");
//
//                    _ad_queue.offer(Constants.enum_ad.unityAds_full);
//
//                    if (_onAdFullManagerListener != null)
//                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.unityAds_full, true);
//                }
//
//                @Override
//                public void onUnityAdsFailedToLoad(String placementId, UnityAds.UnityAdsLoadError error, String message) {
//                    LogUtil.d(TAG, "_txtAd, onUnityAdsFailedToLoad, Ad for " + placementId + " failed to load: [" + error + "] " + message);
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


//    private void createCaulyAd() {
//
//        if (_last_ad == Constants.enum_ad.cauly_full) return;
//        if (canAdInitAvailable(Constants.enum_ad.cauly_full) == false) return;
//        LogUtil.d(TAG, "_txtAd, createCaulyAd");
//
//        try {
//
//            // CaulyAdInfo 생성
//            CaulyAdInfo adInfo = new CaulyAdInfoBuilder(Constants.KEY_CAULY_AD_ID)
//                    // statusbarHide(boolean) : 상태바 가리기 옵션(true : 상태바 가리기)
//                    .build();
//
//            LogUtil.d(TAG, "_txtAd, Cauly, init");
//
//            _interstialCaulyAd = new CaulyInterstitialAd();
//            _interstialCaulyAd.setAdInfo(adInfo);
//            _interstialCaulyAd.setInterstialAdListener(new CaulyInterstitialAdListener() {
//                @Override
//                public void onReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, boolean b) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onReceiveInterstitialAd, b : " + b + ", _interstialCaulyAd : " + (_interstialCaulyAd == null ? "null" : "ok"));
//
//                    if (!b) {
//                        _interstialCaulyAd.setInterstialAdListener(null);
//                        _interstialCaulyAd = null;
//                    } else {
//
//                        if (_interstialCaulyAd != null) {
//                            LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onReceiveInterstitialAd, b : " + b);
//
//                            _ad_queue.offer(Constants.enum_ad.cauly_full);
//
//                            if (_onAdFullManagerListener != null)
//                                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.cauly_full, true);
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailedToReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, int i, String s) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onFailedToReceiveInterstitialAd, i : " + i + ", s : " + s);
//
//                    if (_interstialCaulyAd != null)
//                        _interstialCaulyAd.setInterstialAdListener(null);
//                    _interstialCaulyAd = null;
//                }
//
//                @Override
//                public void onClosedInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onClosedInterstitialAd");
//
//                    if (_interstialCaulyAd != null)
//                        _interstialCaulyAd.setInterstialAdListener(null);
//                    _interstialCaulyAd = null;
//
//                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.cauly_full.name());
//
//                    //initAd(Constants.enum_ad.none);
//                }
//
//                @Override
//                public void onLeaveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onLeaveInterstitialAd");
//
//                    if (_interstialCaulyAd != null) {
//                        _interstialCaulyAd.setInterstialAdListener(null);
//                        _interstialCaulyAd.cancel();
//                    }
//
//                    _interstialCaulyAd = null;
//
//                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.cauly_full.name());
//
//                    //initAd(Constants.enum_ad.none);
//                }
//            });
//
//            // 전면광고 노출 후 back 버튼 사용을 막기 원할 경우 disableBackKey();을 추가한다
//            // 단, requestInterstitialAd 위에서 추가되어야 합니다.
//            // interstitialAd.disableBackKey();
//
//            // 광고 요청. 광고 노출은 CaulyInterstitialAdListener의 onReceiveInterstitialAd에서 처리한다.
//            if (_interstialCaulyAd != null && getActivity() != null) {
//                _interstialCaulyAd.requestInterstitialAd(getActivity());
//            }
//        } catch (OutOfMemoryError e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    private void createMaxReward() {

        if (Constants.DISPLAY_AD_TYPE_MAX == false) return;
        if (_last_ad == Constants.enum_ad.max_reward) return;
        if (canAdInitAvailable(Constants.enum_ad.max_reward) == false) return;

        LogUtil.d(TAG, "_txtAd, createMaxReward");

        try {
            if (_maxRewardedAd == null || _maxRewardedAd.isReady() == false) {

                _maxRewardedAd = MaxRewardedAd.getInstance(Constants.KEY_MAX_REWARD_ID, getActivity());
                _maxRewardedAd.setListener(new MaxRewardedAdListener() {
//                    @Override
//                    public void onRewardedVideoStarted(MaxAd ad) {
//                        LogUtil.d("_maxRewardedAd, onRewardedVideoStarted, ad : " + ad.toString());
//                    }
//
//                    @Override
//                    public void onRewardedVideoCompleted(MaxAd ad) {
//                        LogUtil.d("_maxRewardedAd, onRewardedVideoCompleted, ad : " + ad.toString());
//                    }

                    @Override
                    public void onUserRewarded(MaxAd ad, MaxReward reward) {
                        LogUtil.d("_maxRewardedAd, onUserRewarded, ad : " + ad.toString());
                        if (_maxRewardedAd != null) {
                            _maxRewardedAd.destroy();
                        }
                        _maxRewardedAd = null;

                        callApi_vote_put_ticket_ad_point(Constants.enum_ad.max_reward.name());
                    }

                    @Override
                    public void onAdLoaded(MaxAd ad) {
                        LogUtil.d("_maxRewardedAd, onAdLoaded, ad : " + ad.toString());
                        _ad_queue.offer(Constants.enum_ad.max_reward);
                    }

                    @Override
                    public void onAdDisplayed(MaxAd ad) {
                        LogUtil.d("_maxRewardedAd, onAdDisplayed, ad : " + ad.toString());
                    }

                    @Override
                    public void onAdHidden(MaxAd ad) {
                        LogUtil.d("_maxRewardedAd, onAdHidden, ad : " + ad.toString());
                    }

                    @Override
                    public void onAdClicked(MaxAd ad) {
                        LogUtil.d("_maxRewardedAd, onAdClicked, ad : " + ad.toString());
                    }

                    @Override
                    public void onAdLoadFailed(String adUnitId, MaxError error) {
                        LogUtil.d("_maxRewardedAd, onAdLoadFailed, adUnitId : " + adUnitId + ", error : " + error.toString());

                        if (_onAdCompleteMissionListener != null) {
                            _onAdCompleteMissionListener.onAdLoadFail();
                        }
                    }

                    @Override
                    public void onAdDisplayFailed(MaxAd ad, MaxError error) {
                        LogUtil.d("_maxRewardedAd, onAdDisplayFailed, ad : " + ad.toString());


                    }
                });

                _maxRewardedAd.loadAd();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void createMaxFull() {

        if (Constants.DISPLAY_AD_TYPE_MAX == false) return;
        if (_last_ad == Constants.enum_ad.max_full) return;
        if (canAdInitAvailable(Constants.enum_ad.max_full) == false) return;

        LogUtil.d(TAG, "_txtAd, createMaxFull");

        try {
            if (_maxInterstitialAd == null || _maxInterstitialAd.isReady() == false) {

                _maxInterstitialAd = new MaxInterstitialAd(Constants.KEY_MAX_FULL_ID, getActivity());
                _maxInterstitialAd.setListener(new MaxAdListener() {
                    @Override
                    public void onAdLoaded(@NonNull MaxAd maxAd) {
                        LogUtil.d("_maxInterstitialAd, onAdLoaded, maxAd : " + maxAd.toString());
                        _ad_queue.offer(Constants.enum_ad.max_full);
                    }


                    @Override
                    public void onAdDisplayed(MaxAd ad) {
                        LogUtil.d("_maxInterstitialAd, onAdDisplayed, ad : " + ad.toString());
                    }

                    @Override
                    public void onAdHidden(MaxAd ad) {
                        LogUtil.d("_maxInterstitialAd, onAdHidden, ad : " + ad.toString());
                        if (_maxInterstitialAd != null) {
                            _maxInterstitialAd.destroy();
                        }
                        _maxInterstitialAd = null;

                        callApi_vote_put_ticket_ad_point(Constants.enum_ad.max_full.name());
                    }

                    @Override
                    public void onAdClicked(MaxAd ad) {
                        LogUtil.d("_maxInterstitialAd, onAdClicked, ad : " + ad.toString());
                    }

                    @Override
                    public void onAdLoadFailed(@NonNull String s, @NonNull MaxError maxError) {
                        LogUtil.d("_maxInterstitialAd, onAdLoadFailed, s : " + s + ", error : " + maxError.toString());

                        if (_onAdCompleteMissionListener != null) {
                            _onAdCompleteMissionListener.onAdLoadFail();
                        }
                    }


                    @Override
                    public void onAdDisplayFailed(MaxAd ad, MaxError error) {
                        LogUtil.d("_maxInterstitialAd, onAdDisplayFailed, ad : " + ad.toString() + ", error : " + error.toString());

                        if (_onAdCompleteMissionListener != null) {
                            _onAdCompleteMissionListener.onAdLoadFail();
                        }
                    }
                });

                // Load the first ad
                _maxInterstitialAd.loadAd();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void createAdColonyAd() {
//
//        if (_last_ad == Constants.enum_ad.adcolony_full) return;
//        if (canAdInitAvailable(Constants.enum_ad.adcolony_full) == false) return;
//
//        try {
//            LogUtil.d(TAG, "_txtAd, createAdColonyAd");
//
//            if (_adColonyInterstitial != null && _adColonyInterstitial.isExpired()) {
//                _adColonyInterstitial.destroy();
//            }
//
//            int user_no = SPUtil.getInstance().getUserNo(getContext());
//
//            //AdColony
//            AdColonyAppOptions appOptions = new AdColonyAppOptions()
//                    .setUserID("user_id_" + user_no)
//                    .setKeepScreenOn(true);
//            AdColony.configure(getActivity(), appOptions, "app760a820fdcba447bb6", "vz2f19ad7c0c4541efaf");
//
//            // Ad specific options to be sent with request
//            final AdColonyAdOptions adOptions = new AdColonyAdOptions();
//            AdColonyInterstitialListener listener = new AdColonyInterstitialListener() {
//                @Override
//                public void onRequestFilled(AdColonyInterstitial ad) {
//                    // Ad passed back in request filled callback, ad can now be shown
//                    _adColonyInterstitial = ad;
//                    _ad_queue.offer(Constants.enum_ad.adcolony_full);
//                    LogUtil.d("_txtAd, AdColonyInterstitial, onRequestFilled");
//
//                    if (_onAdFullManagerListener != null)
//                        _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.adcolony_full, true);
//                }
//
//                @Override
//                public void onRequestNotFilled(AdColonyZone zone) {
//                    // Ad request was not filled
//                    LogUtil.d("_txtAd, AdColonyInterstitial, onRequestNotFilled");
//
//                    if (_adColonyInterstitial != null) {
//                        _adColonyInterstitial.destroy();
//                    }
//                    _adColonyInterstitial = null;
//                }
//
//                @Override
//                public void onOpened(AdColonyInterstitial ad) {
//                    // Ad opened, reset UI to reflect state change
//                    LogUtil.d("_txtAd, AdColonyInterstitial, onOpened");
//
//                    if (_adColonyInterstitial != null) {
//                        _adColonyInterstitial.destroy();
//                    }
//                    _adColonyInterstitial = null;
//
//                    initAd(Constants.enum_ad.none);
//                    callApi_vote_put_ticket_ad_point(Constants.enum_ad.adcolony_full.name());
//                }
//
//                @Override
//                public void onExpiring(AdColonyInterstitial ad) {
//                    // Request a new ad if ad is expiring
//                    LogUtil.d("_txtAd, AdColonyInterstitial, onExpiring");
//                    if (_adColonyInterstitial != null) {
//                        _adColonyInterstitial.destroy();
//                    }
//                    _adColonyInterstitial = null;
//                }
//            };
//            AdColony.requestInterstitial("vz2f19ad7c0c4541efaf", listener, adOptions);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }


    public void showAdOrder_Interstitial() {

        if (_last_ad != Constants.enum_ad.tnk_full && _interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrder_Interstitial, _maxInterstitialAd show [3I-1]");
            _interstitialAdTnk.show(getContext());
//        } else if (_last_ad != Constants.enum_ad.cauly_full && _interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//            ad_log("_txtAd, showAdOrder_Interstitial, CaulyAd show [3I-2]");
//            showCauly();
//            _last_ad = Constants.enum_ad.cauly_full;
        } else if (_last_ad != Constants.enum_ad.wad_full && _interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
            ad_log("_txtAd, showAdOrder_Interstitial WAD show [3I-3]");
            _interstitialWad.show();
            _last_ad = Constants.enum_ad.wad_full;
        } else if (_last_ad != Constants.enum_ad.g_full_screen && _interstitialAd != null) {
            ad_log("_txtAd, showAdOrder_Interstitial, google _interstitialAd show [3I-4]");
            _interstitialAd.show(getActivity());
            _last_ad = Constants.enum_ad.g_full_screen;

        } else if (_interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrder_Interstitial, _maxInterstitialAd show [3I2-1]");
            _interstitialAdTnk.show(getContext());
//        } else if (_interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//            ad_log("_txtAd, showAdOrder_Interstitial, CaulyAd show [3I2-2]");
//            showCauly();
        } else if (_interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
            ad_log("_txtAd, showAdOrder_Interstitial WAD show [3I2-3]");
            _interstitialWad.show();
        } else if (_interstitialAd != null) {
            ad_log("_txtAd, showAdOrder_Interstitial, google _interstitialAd show [3I2-4]");
            _interstitialAd.show(getActivity());
        } else if (_maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            ad_log("_txtAd, showAdOrder_Interstitial, _maxInterstitialAd show [3I2-5]");
            _maxInterstitialAd.showAd();
        } else if (_adPopcornSSPInterstitialAd != null && _adPopcornSSPInterstitialAd.isLoaded()) {
            ad_log("_txtAd, showAdOrder_Interstitial, _adPopcornSSPInterstitialAd show [3I2-6]");
            _adPopcornSSPInterstitialAd.showAd();
        }
    }

    /**
     * 광고를 순서대로 보여줍니다.
     */
    public void showAdOrder(boolean isFromCharge) {


//        if (showTest() == true) {
//            return;
//        }

        _last_ad = Constants.enum_ad.none;

        Constants.enum_ad order_ad = _ad_queue.size() > 0 ? _ad_queue.poll() : Constants.enum_ad.none;

        LogUtil.d(TAG, "_txtAd, showAdOrder, order_ad : " + order_ad.name() + ", _ad_queue.size() : " + _ad_queue.size());

        if (order_ad == Constants.enum_ad.g_full_screen && _interstitialAd != null) {
            ad_log("_txtAd, showAdOrder, google _interstitialAd show [3v-1]");
            _interstitialAd.show(getActivity());
            _interstitialAd = null;
//        } else if (order_ad == Constants.enum_ad.cauly_full && _interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//            ad_log("_txtAd, showAdOrder, CaulyAd show [3v-2]");
//            showCauly();
        } else if (order_ad == Constants.enum_ad.tnk_full && _interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrder, _interstitialAdTnk show [3v-2]");
            _interstitialAdTnk.show(getContext());
            _interstitialAdTnk = null;
        } else if (order_ad == Constants.enum_ad.g_reward_full && _rewardedInterstitialAd != null) {
            ad_log("_txtAd, showAdOrder google _rewardFull show [3v-3]");
            showGoogleRewardFull();
        } else if (order_ad == Constants.enum_ad.wad_full && _interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
            ad_log("_txtAd, showAdOrder WAD show [3v-4]");
            _interstitialWad.show();
        } else if (order_ad == Constants.enum_ad.g_reward && _rewardedAd != null) {
            ad_log("_txtAd, showAdOrder, google _rewardedAd show [3v-5]");
            showGoogleRewardAd();
        } else if (order_ad == Constants.enum_ad.tnk_reward && _interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrder, _interstitialAdRewardTnk show [3v-6]");
            _interstitialAdRewardTnk.show(getContext());
            _interstitialAdRewardTnk = null;
//        } else if (order_ad == Constants.enum_ad.unityAds_full && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")
////                && UnityAds.isReady(placementVideoId)
////                && _ad_queue.contains(Constants.enum_ad.unityAds_full)
//        ) {
//            ad_log("_txtAd, showAdOrder, unityads show [3v-6]");
//            showUnityAdsFull();
//        } else if (order_ad == Constants.enum_ad.unityAds_reward && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//            ad_log("_txtAd, showAdOrder, unityads show [3v-7]");
//            showUnityAdsReward();
//        } else if (order_ad == Constants.enum_ad.vungle_full && _interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrder vungle full show [3v-8]");
//            showVungleFull();
//        } else if (order_ad == Constants.enum_ad.vungle_reward && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrder, vungle rewardedAd show [3v-9]");
//            showVungleReward();
        } else if (order_ad == Constants.enum_ad.facebook_full && _interstitialFacebookAd != null && _interstitialFacebookAd.isAdLoaded()) {
            ad_log("_txtAd, showAdOrder, facebook_full show [3v-10]");
            _interstitialFacebookAd.show();
//        } else if (order_ad == Constants.enum_ad.adcolony_full && _adColonyInterstitial != null && _adColonyInterstitial.isExpired() == false) {
//            ad_log("_txtAd, showAdOrder, adcolony_full show [3v-11]");
//            _adColonyInterstitial.show();
        } else if (order_ad == Constants.enum_ad.max_reward && _maxRewardedAd != null && _maxRewardedAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxRewardedAd show [3v-12]");
            _maxRewardedAd.showAd();
            _maxRewardedAd = null;
        } else if (order_ad == Constants.enum_ad.max_full && _maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxInterstitialAd show [3v-13]");
            _maxInterstitialAd.showAd();
            _maxInterstitialAd = null;
        } else if (_last_ad != Constants.enum_ad.g_full_screen && _interstitialAd != null) {
            ad_log("_txtAd, showAdOrder, google _interstitialAd show [3-1]");
            _interstitialAd.show(getActivity());
            _interstitialAd = null;
        } else if (_last_ad != Constants.enum_ad.tnk_reward && _interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrder, _interstitialAdRewardTnk show [3-2]");
            _interstitialAdRewardTnk.show(getContext());
            _interstitialAdRewardTnk = null;
        } else if (_last_ad != Constants.enum_ad.tnk_full && _interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrder, tnk show [3-3]");
            _interstitialAdTnk.show(getContext());
            _interstitialAdTnk = null;
//        } else if (_last_ad != Constants.enum_ad.cauly_full && _interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//            ad_log("_txtAd, showAdOrder, CaulyAd show [3-4]");
//            showCauly();
        } else if (_last_ad != Constants.enum_ad.g_reward_full && _rewardedInterstitialAd != null) {
            ad_log("_txtAd, showAdOrder google _rewardFull show [3-5]");
            showGoogleRewardFull();
        } else if (_last_ad != Constants.enum_ad.wad_full && _interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
            ad_log("_txtAd, showAdOrder WAD show [3-6]");
            _interstitialWad.show();
        } else if (_last_ad != Constants.enum_ad.g_reward && _rewardedAd != null) {
            ad_log("_txtAd, showAdOrder, google _rewardedAd show [3-7]");
            showGoogleRewardAd();


//        } else if (_last_ad != Constants.enum_ad.unityAds_full && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")
//                //&& UnityAds.isReady(placementVideoId)
//                && _ad_queue.contains(Constants.enum_ad.unityAds_full)
//        ) {
//            ad_log("_txtAd, showAdOrder, unityads show [3-8]");
//            showUnityAdsFull();
//        } else if (_last_ad != Constants.enum_ad.unityAds_reward && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//            ad_log("_txtAd, showAdOrder, unityads show [3-9]");
//            showUnityAdsReward();
//        } else if (_last_ad != Constants.enum_ad.vungle_full && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")
//                && _interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrder vungle full show [3-11]");
//            showVungleFull();
//        } else if (_last_ad != Constants.enum_ad.vungle_reward && SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")
//                && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrder, vungle rewardedAd show [3-12]");
//            showVungleReward();
        } else if (_last_ad != Constants.enum_ad.facebook_full && _interstitialFacebookAd != null && _interstitialFacebookAd.isAdLoaded()) {
            ad_log("_txtAd, showAdOrder, facebook_full show [3-13]");
            _interstitialFacebookAd.show();
//        } else if (_last_ad != Constants.enum_ad.adcolony_full && _adColonyInterstitial != null && _adColonyInterstitial.isExpired() == false) {
//            ad_log("_txtAd, showAdOrder, adcolony_full show [3-14]");
//            _adColonyInterstitial.show();
        } else if (_last_ad != Constants.enum_ad.max_reward && _maxRewardedAd != null && _maxRewardedAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxRewardedAd show [3-6]");
            _maxRewardedAd.showAd();
            _maxRewardedAd = null;
        } else if (_last_ad != Constants.enum_ad.max_full && _maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxInterstitialAd show [3-6]");
            _maxInterstitialAd.showAd();
            _maxInterstitialAd = null;
        } else if (_last_ad != Constants.enum_ad.adpc_full && _adPopcornSSPInterstitialAd != null && _adPopcornSSPInterstitialAd.isLoaded()) {
            ad_log("_txtAd, showAdOrder adpc_full show [3-7]");
            _adPopcornSSPInterstitialAd.showAd();
        } else if (_last_ad != Constants.enum_ad.adpc_reward && _adPopcornSSPRewardVideoAd != null && _adPopcornSSPRewardVideoAd.isReady()) {
            ad_log("_txtAd, showAdOrder adpc_full show [3-8]");
            _adPopcornSSPRewardVideoAd.showAd();
        } else if (_last_ad != Constants.enum_ad.adpc_reward_full && _adPopcornSSPInterstitialVideoAd != null && _adPopcornSSPInterstitialVideoAd.isReady()) {
            ad_log("_txtAd, showAdOrder adpc_full show [3-9]");
            _adPopcornSSPInterstitialVideoAd.showAd();
        } else {

            _last_ad = Constants.enum_ad.none;

//            ad_log("_txtAd, showAdOrder NOT show [3-99]");


            if (_onAdFullManagerListener != null)
                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
            //initAd(Constants.enum_ad.none);

            if (isFromCharge && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                if (!FormatUtil.isNullorEmpty(_newspicUrl) && getActivity() != null) {
                    //ad_log("_txtAd, READ_NEWSPIC_A show [14]");


//                    if (_newspicUrl.contains("ponong.co.kr")) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_newspicUrl));
//                        getActivity().startActivity(intent);
//
//                        Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                callApi_vote_put_ticket_ad_point("ponong");
//                            }
//                        }, 5000);
//                        return;
//                    }


                    Intent i = new Intent();
                    i.setClass(getContext(), AtvWeb.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "READ_NEWSPIC_A");
                    i.putExtra(Constants.EXTRAS_NO, _newspic_no);
                    i.putExtra(Constants.EXTRAS_URL, _newspicUrl);
                    getActivity().startActivity(i);
                    _newspicUrl = "";
                    return;
                } else {
                    if (AtvWeb._failedHour != FormatUtil.getCurrentHour()) {
                        ad_log("_txtAd, READ_NEWSPIC_D show [15]");
                        callApi_newspic_get_newspic_info("READ_NEWSPIC_D");
                        return;
                    }
                }
            }

            Alert.toastLong(getContext(), getContext().getString(R.string.alert_ad_no_fill));
        }


    }


    /**
     * 리워드 광고 우선으로 보여줍니다.
     */
    public void showAdOrderReward(boolean isFromCharge) {
        LogUtil.d(TAG, "_txtAd, showAdOrderReward");

        Constants.enum_ad order_ad = _ad_queue.size() > 0 ? _ad_queue.poll() : Constants.enum_ad.none;

        LogUtil.d(TAG, "_txtAd, showAdOrderReward, order_ad : " + order_ad.name() + ", _ad_queue.size() : " + _ad_queue.size());

        if (order_ad == Constants.enum_ad.g_reward && _rewardedAd != null) {
            ad_log("_txtAd, showAdOrderReward, google _rewardedAd show [4v-1]");
            showGoogleRewardAd();
        } else if (order_ad == Constants.enum_ad.tnk_reward && _interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrderReward, _interstitialAdRewardTnk show [4v-2]");
            _interstitialAdRewardTnk.show(getContext());
//        } else if (order_ad == Constants.enum_ad.unityAds_full && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")
////                && UnityAds.isReady(placementVideoId)
////                && _ad_queue.contains(Constants.enum_ad.unityAds_full)
//        ) {
//            ad_log("_txtAd, showAdOrderReward, unityads show [4v-2]");
//            showUnityAdsFull();
//        } else if (order_ad == Constants.enum_ad.unityAds_reward && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//            ad_log("_txtAd, showAdOrderReward, unityads show [4v-3]");
//            showUnityAdsReward();
//        } else if (order_ad == Constants.enum_ad.vungle_reward && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrderReward, vungle rewardedAd show [4v-4]");
//            showVungleReward();
        } else if (order_ad == Constants.enum_ad.g_full_screen && _interstitialAd != null) {
            ad_log("_txtAd, showAdOrderReward, google _interstitialAd show [4v-5]");
            _interstitialAd.show(getActivity());
            _interstitialAd = null;
//        } else if (order_ad == Constants.enum_ad.cauly_full && _interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//            ad_log("_txtAd, showAdOrderReward, CaulyAd show [4v-6]");
//            showCauly();
        } else if (order_ad == Constants.enum_ad.g_reward_full && _rewardedInterstitialAd != null) {
            ad_log("_txtAd, showAdOrderReward google _rewardFull show [4v-7]");
            showGoogleRewardFull();
        } else if (order_ad == Constants.enum_ad.tnk_full && _interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrderReward _interstitialAdTnk show [4v-71]");
            _interstitialAdTnk.show(getContext());
            _interstitialAdTnk = null;
        } else if (order_ad == Constants.enum_ad.wad_full && _interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
            ad_log("_txtAd, showAdOrderReward WAD show [4v-8]");
            _interstitialWad.show();
//        } else if (order_ad == Constants.enum_ad.vungle_full && _interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrderReward vungle full show [4v-9]");
//            showVungleFull();
        } else if (order_ad == Constants.enum_ad.max_full && _maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxInterstitialAd show [4v-10]");
            _maxInterstitialAd.showAd();
            _maxInterstitialAd = null;
        } else if (order_ad == Constants.enum_ad.max_reward && _maxRewardedAd != null && _maxRewardedAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxRewardedAd show [4v-11]");
            _maxRewardedAd.showAd();
            _maxRewardedAd = null;
        } else if (_last_ad != Constants.enum_ad.g_reward && _rewardedAd != null) {
            ad_log("_txtAd, showAdOrderReward, google _rewardedAd show [4-1]");
            showGoogleRewardAd();
        } else if (_last_ad == Constants.enum_ad.tnk_reward && _interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrderReward, _interstitialAdRewardTnk show [4-2]");
            _interstitialAdRewardTnk.show(getContext());
            _interstitialAdRewardTnk = null;
//        } else if (_last_ad != Constants.enum_ad.unityAds_full && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")
//                && _ad_queue.contains(Constants.enum_ad.unityAds_full)
//        ) {
//            ad_log("_txtAd, showAdOrderReward, unityads show [4-2]");
//            showUnityAdsFull();
//        } else if (_last_ad != Constants.enum_ad.unityAds_reward && SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y") && UnityAds.isReady(placementRewardVideoId)) {
//            ad_log("_txtAd, showAdOrderReward, unityads show [4-3]");
//            showUnityAdsReward();
//        } else if (_last_ad != Constants.enum_ad.vungle_reward && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrderReward, vungle rewardedAd show [4-4]");
//            showVungleReward();
        } else if (_last_ad != Constants.enum_ad.g_full_screen && _interstitialAd != null) {
            ad_log("_txtAd, showAdOrderReward, google _interstitialAd show [4-5]");
            _interstitialAd.show(getActivity());
            _interstitialAd = null;
//        } else if (_last_ad != Constants.enum_ad.cauly_full && _interstialCaulyAd != null && _interstialCaulyAd.canShow() && SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//            ad_log("_txtAd, showAdOrderReward, CaulyAd show [4-6]");
//            showCauly();
        } else if (_last_ad != Constants.enum_ad.g_reward_full && _rewardedInterstitialAd != null) {
            ad_log("_txtAd, showAdOrderReward google _rewardFull show [4-7]");
            showGoogleRewardFull();
        } else if (_last_ad != Constants.enum_ad.tnk_full && _interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            ad_log("_txtAd, showAdOrderReward _interstitialAdTnk show [4-71]");
            _interstitialAdTnk.show(getContext());
            _interstitialAdTnk = null;
        } else if (_last_ad != Constants.enum_ad.wad_full && _interstitialWad != null && _interstitialWad.isReady() && SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
            ad_log("_txtAd, showAdOrderReward WAD show [4-8]");
            _interstitialWad.show();
//        } else if (_last_ad != Constants.enum_ad.vungle_full && _interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//            ad_log("_txtAd, showAdOrderReward vungle full show [4-9]");
//            showVungleFull();
        } else if (_last_ad != Constants.enum_ad.max_full && _maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxInterstitialAd show [4-10]");
            _maxInterstitialAd.showAd();
            _maxInterstitialAd = null;
        } else if (_last_ad != Constants.enum_ad.max_reward && _maxRewardedAd != null && _maxRewardedAd.isReady()) {
            ad_log("_txtAd, showAdOrder _maxRewardedAd show [4-11]");
            _maxRewardedAd.showAd();
            _maxRewardedAd = null;
        } else if (_last_ad != Constants.enum_ad.adpc_reward && _adPopcornSSPRewardVideoAd != null && _adPopcornSSPRewardVideoAd.isReady()) {
            ad_log("_txtAd, showAdOrder adpc_reward show [4-12]");
            _adPopcornSSPRewardVideoAd.showAd();
        } else if (_last_ad != Constants.enum_ad.adpc_reward_full && _adPopcornSSPInterstitialVideoAd != null && _adPopcornSSPInterstitialVideoAd.isReady()) {
            ad_log("_txtAd, showAdOrder adpc_reward_full show [4-13]");
            _adPopcornSSPInterstitialVideoAd.showAd();
        } else if (_last_ad != Constants.enum_ad.adpc_full && _adPopcornSSPInterstitialAd != null && _adPopcornSSPInterstitialAd.isLoaded()) {
            ad_log("_txtAd, showAdOrder adpc_full show [4-14]");
            _adPopcornSSPInterstitialAd.showAd();
        } else {
            ad_log("_txtAd, showAdOrderReward NOT show [4-99]");


            _last_ad = Constants.enum_ad.none;

            if (_onAdFullManagerListener != null)
                _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
            //initAd(Constants.enum_ad.none);

            if (isFromCharge && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                if (!FormatUtil.isNullorEmpty(_newspicUrl) && getActivity() != null) {
                    //ad_log("_txtAd, READ_NEWSPIC_A show [14]");

//                    if (_newspicUrl.contains("ponong.co.kr")) {
//                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_newspicUrl));
//                        getActivity().startActivity(intent);
//
//                        Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                callApi_vote_put_ticket_ad_point("ponong");
//                            }
//                        }, 1000);
//                        return;
//                    }

                    Intent i = new Intent();
                    i.setClass(getContext(), AtvWeb.class);
                    i.putExtra(Constants.EXTRAS_TYPE, "READ_NEWSPIC_A");
                    i.putExtra(Constants.EXTRAS_NO, _newspic_no);
                    i.putExtra(Constants.EXTRAS_URL, _newspicUrl);
                    getActivity().startActivity(i);
                    _newspicUrl = "";
                    return;
                } else {
                    if (AtvWeb._failedHour != FormatUtil.getCurrentHour()) {
                        ad_log("_txtAd, READ_NEWSPIC_D show [15]");
                        callApi_newspic_get_newspic_info("READ_NEWSPIC_D");
                        return;
                    }
                }

                Alert.toastLong(getContext(), getContext().getString(R.string.alert_ad_no_fill));
            }
        }
    }


    private void showGoogleRewardFull() {
        LogUtil.d(TAG, "_txtAd, google reward_full show ");
        try {
            if (_rewardedInterstitialAd != null) {
                _rewardedInterstitialAd.show(getActivity(), new OnUserEarnedRewardListener() {
                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                        LogUtil.d(TAG, "_txtAd, rewardItem : " + rewardItem.toString());

                        _rewardedInterstitialAd = null;
                        //initAd(Constants.enum_ad.none);
                        callApi_vote_put_ticket_ad_point(Constants.enum_ad.g_reward_full.name());
                    }
                });

                _rewardedInterstitialAd = null;
            }
        } catch (Exception e) {
            e.printStackTrace();

            _rewardedInterstitialAd = null;
        }
    }

    private void showGoogleRewardAd() {

        LogUtil.d(TAG, "_txtAd, google reward show ");

        _rewardedAd.show(getActivity(), new OnUserEarnedRewardListener() {
            @Override
            public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                //initAd(Constants.enum_ad.none);
                callApi_vote_put_ticket_ad_point(Constants.enum_ad.g_reward.name());

            }
        });

        _rewardedAd = null;

    }


    /**
     * 재생할 수 있는 광고 플랫폼 개수 조회
     *
     * @return
     */
    public int canShowAdCount(boolean isFromCharge) {
        int count = 0;

        try {
            count += canShowAdCount_Interstitial(false, isFromCharge);
            count += canShowAdCount_Reward();
        } catch (Exception e) {
            e.printStackTrace();
        }

        LogUtil.d(TAG, "_txtAd, canShowAdCount, count : " + count);
        return count;
    }


    public int canShowAdCount_Interstitial(boolean isExcludeAd, boolean isFromCharge) {
        int count = 0;

//        if (_isShowCauly && _interstialCaulyAd != null) {
//            _interstialCaulyAd.setInterstialAdListener(null);
//            _interstialCaulyAd.cancel();
//            _interstialCaulyAd = null;
//            _isShowCauly = false;
//        }

        if (_interstitialAd != null) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Interstitial, g _interstitialAd");
            count++;
        }
//        if (isExcludeAd == false && _interstialCaulyAd != null && _interstialCaulyAd.canShow()) {
//            LogUtil.d(TAG, "_txtAd, canShowAdCount_Interstitial, _interstialCaulyAd");
//            count++;
//        }
        if (_interstitialWad != null && _interstitialWad.isReady()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Interstitial, _interstitialWad");
            count++;
        }
        if (_interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Interstitial, _interstitialAdTnk");
            count++;
        }
        if (_adPopcornSSPInterstitialAd != null && _adPopcornSSPInterstitialAd.isLoaded()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Interstitial, _adPopcornSSPInterstitialAd");
            count++;
        }
//        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y") && isFromCharge && !FormatUtil.isNullorEmpty(_newspicUrl) && _newspic_no > 0) {
//            LogUtil.d(TAG, "_txtAd, canShowAdCount_Interstitial, _newspicUrl : " + _newspicUrl);
//            count++;
//        }


        LogUtil.d(TAG, "_txtAd, canShowAdCount_Interstitial, count : " + count);
        return count;
    }


    public int canShowAdCount_Reward() {
        int count = 0;

        if (_rewardedAd != null) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, g _rewardedAd");
            count++;
        }
        if (_rewardedInterstitialAd != null) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, g _rewardedInterstitialAd");
            count++;
        }

//        if (SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")
////                && UnityAds.isReady(placementVideoId)
//                && _ad_queue.contains(Constants.enum_ad.unityAds_full)
//        ) {
//            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, UnityAds placementVideoId");
//            count++;
//        }
//        if (SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")
//                && _interstitialVungleAd != null && _interstitialVungleAd.canPlayAd()) {
//            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, VUNGLE_FULL");
//            count++;
//        }
//        if (SPUtil.getInstance().getUseVungleYn(getContext()).equals("Y")
//                && _rewardedVungleAd != null && _rewardedVungleAd.canPlayAd()) {
//            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, VUNGLE_REWARD");
//            count++;
//        }
        if (_maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, _maxInterstitialAd");
            count++;
        }
        if (_maxRewardedAd != null && _maxRewardedAd.isReady()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, _maxRewardedAd");
            count++;
        }

        if (_interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, _interstitialAdRewardTnk");
            count++;
        }

        if (_adPopcornSSPRewardVideoAd != null && _adPopcornSSPRewardVideoAd.isReady()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, _adPopcornSSPRewardVideoAd");
            count++;
        }
        if (_adPopcornSSPInterstitialVideoAd != null && _adPopcornSSPInterstitialVideoAd.isReady()) {
            LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, _adPopcornSSPInterstitialVideoAd");
            count++;
        }

        LogUtil.d(TAG, "_txtAd, canShowAdCount_Reward, count : " + count);
        return count;
    }


    public void callApi_vote_put_ticket_ad_point(final String ad_type) {
        LogUtil.d(TAG, "_txtAd, callApi_vote_put_ticket_ad_point, _onAdCompleteListener : " + (_onAdCompleteListener == null ? "null" : "ok"));


        if (CommonUtil.checkDoubleTab()) {
            LogUtil.e("_txtAd, callApi_vote_put_ticket_ad_point, 이벤트 중복 발생");
            return;
        }


        if (_onAdCompleteMissionListener != null) {
            _onAdCompleteMissionListener.onAdClose();
            return;
        }
        if (_onAdCompleteListener != null) {
            _onAdCompleteListener.onAdClose();
            return;
        }

        _ad_view_count++;

        if (ad_type.equals(Constants.enum_ad.newspic_a.name())) {
            _ad_view_count = 0;
        }

        LogUtil.e("==========callApi_vote_put_ticket_ad_point : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.vote_put_ticket_ad_point(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , ad_type
                , SPUtil.getInstance().getIsHaveBoosterItem(getContext()) ? "Y" : "N"
                , SPUtil.getInstance().getAndroidID(getContext())
                , _ad_view_count
                , _newspic_no
                , SPUtil.getInstance().getChargeMode(getContext())
        );

//        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                try {

                    _newspicUrl = "";
                    _newspic_no = 0;

                    LogUtil.d(TAG, "resultCode : " + resultCode);
                    LogUtil.d(TAG, "resultMessage : " + resultMessage);
                    LogUtil.json(json);

                    // 1로 내려오는 에러는 무시하고 그 이상의 에러는 Alert 처리 합니다.
                    if (resultCode > 1 && !FormatUtil.isNullorEmpty(resultMessage)) {
                        new Alert().showAlert(getContext(), resultMessage);
                        return;
                    }


                    if (resultCode == 0) {
                        JSONObject data = JSONUtil.getJSONObject(json, "data");
                        JSONObject userInfo = JSONUtil.getJSONObject(data, "member");

                        LogUtil.json(userInfo);

                        SPUtil.getInstance().setUserInfo(getContext(), userInfo);

                        final int amount_ticket_ad = JSONUtil.getInteger(data, "amount_ticket_ad", -1);
                        final double point_amount = JSONUtil.getDouble(data, "point_amount", -1);

                        _newspicUrl = JSONUtil.getStringUrl(data, "newspic_url");
                        _newspic_no = JSONUtil.getInteger(data, "newspic_no", 0);

                        Alert.toastLong(getContext(), resultMessage);

                        if (_onAdFullManagerListener != null) {
                            _onAdFullManagerListener.setActiveAdText(Constants.enum_ad.none, false);
                            _onAdFullManagerListener.setVotePointSync(userInfo, amount_ticket_ad, point_amount);
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d(TAG, "onFailure");
            }
        });
    }


    private boolean _canViewNewspic = true;


    public boolean isCanViewNewspic() {
        return _canViewNewspic;
    }


    public void callApi_newspic_get_newspic_info(final String extra_type) {
        LogUtil.e("==========callApi_newspic_get_newspic_info : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.newspic_get_newspic_info(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );

        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                try {
                    LogUtil.json(json);

                    JSONObject data = JSONUtil.getJSONObject(json, "data");

                    int newspic_no = JSONUtil.getInteger(data, "newspic_no", 0);
                    String newspic_url = JSONUtil.getStringUrl(data, "newspic_url");

                    if (newspic_no > 0 && !FormatUtil.isNullorEmpty(newspic_url)) {

//                        if (newspic_url.contains("ponong.co.kr")) {
//
//                            AlertPop alertPop = new AlertPop();
//                            alertPop.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                                @Override
//                                public void onClose(DialogInterface dialog, int which) {
//                                    if (which == Alert.BUTTON1) {
//                                        _newspic_no = newspic_no;
//                                        if (dialog != null) dialog.dismiss();
//                                        callApi_vote_put_ticket_ad_point("ponong");
//                                    }
//                                }
//                            });
//                            alertPop.showNewspicAd(getContext(), newspic_no, newspic_url);
//
//                            return;
//                        }


                        Intent i = new Intent();
                        i.setClass(getContext(), AtvWeb.class);
                        i.putExtra(Constants.EXTRAS_TYPE, extra_type);
                        i.putExtra(Constants.EXTRAS_NO, newspic_no);
                        i.putExtra(Constants.EXTRAS_URL, newspic_url);
                        getActivity().startActivity(i);

                    } else {
                        _canViewNewspic = false;
                        Alert.toastLong(getContext(), getContext().getString(R.string.alert_ad_no_fill));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


}
