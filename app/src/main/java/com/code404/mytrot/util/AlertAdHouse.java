package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

import static com.google.android.gms.ads.formats.NativeAdOptions.ADCHOICES_TOP_LEFT;
import static com.google.android.gms.ads.formats.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_SQUARE;


public class AlertAdHouse extends Alert {


    private Context mContext;
    private ImageButton _btnClose = null;
    private ImageView _imgAd = null;

    Dialog _dialog = null;
    /**
     * 개발자 후원
     */
    public void showAd(final Context context) {
        mContext = context;

        _dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        _dialog.setContentView(R.layout.alert_ad_house);

        _btnClose = _dialog.findViewById(R.id.btnClose);
        _imgAd = _dialog.findViewById(R.id.imgAd);

        initAd();

        _btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(_dialog, Alert.CANCEL);
                }
                _dialog.dismiss();
            }
        });
        _dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        _dialog.setCancelable(true);
        _dialog.show();
    }

    private Context getContext() {
        return mContext;
    }

    private void initAd(){
        JSONArray list =  SPUtil.getInstance().getAdBannerFullList(getContext());
        JSONObject jsonAd = JSONUtil.getJSONObject(list, 0);

        String img_url = JSONUtil.getStringUrl(jsonAd, "img_url");
        final String click_url = JSONUtil.getStringUrl(jsonAd, "click_url");
        final String no = JSONUtil.getString(jsonAd, "no");

        Picasso.with(getContext())
                .load(img_url)
                .fit()
                .placeholder(R.drawable.img_noimg)
                .into(_imgAd);


        _imgAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApi_logClick_add_log(Constants.LOG_CLICK_MORE_AD_FULL + no);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(click_url));
                getContext().startActivity(i);

                if (_closeListener != null) {
                    _closeListener.onClose(_dialog, Alert.BUTTON1);
                }

                _dialog.dismiss();
            }
        });
    }

    private void callApi_logClick_add_log(final String code) {

        LogUtil.e("==========callApi_logClick_add_log : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.logClick_add_log(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , code
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }
}