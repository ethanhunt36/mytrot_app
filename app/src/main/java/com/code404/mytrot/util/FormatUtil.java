package com.code404.mytrot.util;

import android.content.Context;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.widget.EditText;

import com.code404.mytrot.common.Constants;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FormatUtil {
    private static double distance = 1000.0;


    /**
     * null 체크
     *
     * @param targetString
     * @return null이면 true, null이 아니면 false 반환
     */
    public static boolean isNullorEmpty(String targetString) {
        if (targetString == null || targetString.trim().equals("") || targetString.trim().equals("null"))
            return true;

        return false;
    }

    public static String removeDomainUrl(String url) {
        if (FormatUtil.isNullorEmpty(url))
            return url;

        url = url.replace(Constants.SERVER_URL, "");
        url = url.replace(Constants.SERVER_URL_DEBUG, "");

        return url;
    }


    public static String trimRight(String value) {
        String result = value;

        if (isNullorEmpty(value)) {
            result = "";
        } else {
            result = value.replaceAll("\\s+$", "");
        }

        return result;
    }


    public static String trimLeft(String value) {
        String result = value;

        if (isNullorEmpty(value)) {
            result = "";
        } else {
            result = value.replaceAll("^\\s+", "");
        }

        return result;
    }


    public static String toPriceFormat(double lSource) {
        return toPriceFormat(lSource, false);
    }


    public static String toPriceFormat(double lSource, boolean hasWon) {
        String won = "원";

        try {
            // if((lSource != 0.0) && (lSource % 10000 == 0) && hasWon)
            // {
            // won = "만원";
            // lSource = lSource / 10000;
            // }
            String sPattern = "###,###,###,###,###,###.##";
            DecimalFormat decimalformat = new DecimalFormat(sPattern);

            String result = decimalformat.format(lSource);
            result = hasWon ? result + won : result + "";
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";

    }

    public static String toPriceFormatZero(double lSource) {

        String sPattern = "###,###,###,###,###,###.0";
        DecimalFormat decimalformat = new DecimalFormat(sPattern);

        String result = decimalformat.format(lSource);
        return result;
    }


    public static String toPriceFormat(String sSource) {
        return toPriceFormat(sSource, false);
    }


    public static String toPriceFormat(String sSource, boolean hasWon) {
        if (isNullorEmpty(sSource)) return sSource;

        try {
            return toPriceFormat(Double.valueOf(sSource), hasWon);
        } catch (NumberFormatException e) {
            return sSource;
        }

    }


    public static String toTime(int second) {
        String time = "00:00";

        int h = second / 60 / 60;

        int m = second / 60;

        int s = second % 60;

        // LogUtil.d("h : " + h);
        // LogUtil.d("s : " + m);
        // LogUtil.d("s : " + m);

        if (h > 0) time = twoNumber(h) + ":" + twoNumber(m) + ":" + twoNumber(s);
        else time = twoNumber(m) + ":" + twoNumber(s);

        return time;
    }


    public static String twoNumber(int n) {
        String num = "0" + n;

        return num.substring(num.length() - 2, num.length());
    }


    public static String addDate(Date date, int days, String format) {
        DateFormat df = new SimpleDateFormat(format);

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.add(Calendar.DATE, days);

        return df.format(cal.getTime());
    }


    /**
     * 기준 날짜 기준 날짜 이동
     **/
    public static String returnDate(String birth, int plusDate, String spliter) {
        String result = "";
        int year = Integer.parseInt(birth.substring(0, 4));
        int month = Integer.parseInt(birth.substring(5, 7));
        int day = Integer.parseInt(birth.substring(8, 10));

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);

        cal.add(Calendar.DATE, plusDate);

        int resultYear = cal.get(Calendar.YEAR);
        int resultMonth = cal.get(Calendar.MONTH);
        int resultDay = cal.get(Calendar.DAY_OF_MONTH);

        if (year != resultYear) {
            resultMonth = cal.get(Calendar.MONTH) + 1;
        }

        result = String.valueOf(resultYear) + spliter + String.valueOf(convertTwoNumber(resultMonth)) + spliter + String.valueOf(convertTwoNumber(resultDay));

        return result;
    }


    public static String convertTwoNumber(long day) {
        String temp = "0" + day;

        return temp.substring(temp.length() - 2, temp.length());
    }


    public static int convertOneNumber(long num) {
        String str = String.valueOf(num);
        if (str.startsWith("0")) {
            str.substring(str.length() - 1, str.length());
        }
        return Integer.parseInt(str);
    }


    public static String convertMD5(String str) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            LogUtil.w(e);
        }

        String eip;
        byte[] bip;

        String temp = "";
        String tst = str;

        bip = md5.digest(tst.getBytes());

        for (int i = 0; i < bip.length; i++) {
            eip = "" + Integer.toHexString((int) bip[i] & 0x000000ff);
            if (eip.length() < 2) eip = "0" + eip;

            temp = temp + eip;
        }

        return temp;

    }


    /**
     * 천단위로 자동으로 콤마를 찍어준다.
     *
     * @param et
     * @return
     */
    public static EditText setAutoPriceComma(final EditText et) {
        et.addTextChangedListener(new TextWatcher() {
            String strPrice = "";


            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }


            public void afterTextChanged(Editable s) {
            }


            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(strPrice)) {
                    strPrice = FormatUtil.toPriceFormat(s.toString().replace(",", ""), false);
                    et.setText(strPrice);
                    Editable e = et.getText();
                    Selection.setSelection(e, strPrice.length());
                }
            }
        });

        return et;
    }


    /**
     * 단어에 한글 포함 여부 검사
     *
     * @param
     * @return
     */
    public static boolean isContainsKorean(String word) {
        if (word.matches(".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*")) {
            return true;
        }

        return false;
    }


    /**
     * 이메일 패턴검사
     *
     * @param email
     * @return
     */
    public static boolean isEmailPattern(String email) {
        if (isNullorEmpty(email)) {
            return false;
        }
        Pattern pattern = Pattern.compile("\\w+[@]\\w+\\.\\w+");
        Matcher match = pattern.matcher(email);
        return match.find();
    }


    private static final String Password_PATTERN = "^(?=.*[a-zA-Z]+)(?=.*[!@#$%^*+=-]|.*[0-9]+).{8,32}$";


    public static boolean isPasswordValidate(final String hex) {
        Pattern pattern = Pattern.compile(Password_PATTERN);
        Matcher matcher = pattern.matcher(hex);
        return matcher.matches();
    }


    private static final String NAME_ENG_PATTERN = "^[,_A-Za-z0-9+]*$";

    public static boolean isNameEngValidate(final String hex) {
        Pattern pattern = Pattern.compile(NAME_ENG_PATTERN);
        Matcher matcher = pattern.matcher(hex);
        return matcher.matches();
    }


    public static Date FormatStringGetDate(String dateAndTimeStr) {
        return FormatStringGetDate(dateAndTimeStr, "yyyyMMddHHmmss");
    }


    public static Date FormatStringGetDate(String dateAndTimeStr, String formatStr) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(formatStr);
        Date fromDate = null;
        try {
            fromDate = fromFormat.parse(dateAndTimeStr);
        } catch (Exception e) {
            fromDate = new Date(0);
            e.printStackTrace();
        }

        return fromDate;
    }


    public static String FormatGetDate(String dateAndTimeStr, String formatStr) {
        try {
            return FormatGetDate((new SimpleDateFormat(formatStr)).parse(dateAndTimeStr));
        } catch (Exception e) {
            return "";
        }
    }

    public static String FormatGetDateHM(String dateAndTimeStr, String formatStr) {
        try {
            return FormatGetDateHM((new SimpleDateFormat(formatStr)).parse(dateAndTimeStr));
        } catch (Exception e) {
            return "";
        }
    }


    public static String FormatGetDate(Date date) {
        long nowTime = System.currentTimeMillis();

        long remainTime = nowTime - date.getTime();

        String result = "";

//        if (remainTime < 24 * 60 * 60000)
//        {
//            result = (remainTime / (60 * 60000)) + "시간전";
//        }
//        else if (remainTime < 24 * 60 * 60000 * 7)
//        {
//            result = (remainTime / (24 * 60 * 60000)) + "일전";
//        }
//        else
//        {
        String yyyy = getCurrentDate().substring(0, 4);
        String format = "yyyy.MM.dd";
        LogUtil.d("yyyy : " + yyyy);

        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy.MM.dd");
        try {
            result = fromFormat.format(date);
            //result = result.replace(yyyy + ".", "");
        } catch (Exception e) {
        }
//        }


        return result;
    }


    /**
     * 매칭 날짜 조회 - 16시 이전의 날짜는 -1일 계산 해서 리턴
     *
     * @param date
     * @return
     */
    public static String getMatchDate(Date date) {
        String result = "";
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat h_format = new SimpleDateFormat("H");
        try {
            result = fromFormat.format(date);

            LogUtil.d("getMatchDate, result[1] : " + result);
            int hour = Integer.parseInt(h_format.format(date));

            if (hour < 16) {
                result = FormatUtil.addDate(date, -1, "yyyy-MM-dd");
                LogUtil.d("getMatchDate, result[2] : " + result);
            }
        } catch (Exception e) {
        }

        return result;
    }

    public static long getRemainMinute(Date date) {
        long nowTime = System.currentTimeMillis();

        long remainTime = date.getTime() - nowTime;

        return remainTime / (1000 * 60);
    }

    public static long getRemainSecond(Date date) {
        if (date == null) {
            return 0;
        }

        long KR_TIME_DIFF = 9 * 60 * 60 * 1000;

        long nowTime = System.currentTimeMillis();
        long utc = (date.getTimezoneOffset() * 60 * 1000);

        LogUtil.d("getRemainSecond, KR_TIME_DIFF : " + KR_TIME_DIFF + ", utc : " + utc);
        long remainTime = (date.getTime() - KR_TIME_DIFF) - (nowTime + utc);

        return remainTime / 1000;
    }


    public static String FormatGetDateHM(Date date) {
        long nowTime = System.currentTimeMillis();

        long remainTime = nowTime - date.getTime();

        String result = "";

//        if (remainTime < 24 * 60 * 60000)
//        {
//            result = (remainTime / (60 * 60000)) + "시간전";
//        }
//        else if (remainTime < 24 * 60 * 60000 * 7)
//        {
//            result = (remainTime / (24 * 60 * 60000)) + "일전";
//        }
//        else
//        {
        String yyyy = getCurrentDate().substring(0, 4);
        String format = "yyyy.MM.dd";
        LogUtil.d("yyyy : " + yyyy);

        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        try {
            result = fromFormat.format(date);
            //result = result.replace(yyyy + ".", "");
        } catch (Exception e) {
        }
//        }


        return result;
    }


    public static String FormatDateConvertString(String dateAndTimeStr, String formatStr, String retformatStr) {
        if (isNullorEmpty(dateAndTimeStr)) return "";

        SimpleDateFormat fromFormat = new SimpleDateFormat(formatStr);
        SimpleDateFormat toFormat = new SimpleDateFormat(retformatStr);

        Date fromDate = null;
        try {
            fromDate = fromFormat.parse(dateAndTimeStr);
            return toFormat.format(fromDate);
        } catch (Exception e) {
            return dateAndTimeStr;
        }
    }


    public static String FormatDateConvertString(String dateAndTimeStr, String retformatStr) {
        return FormatDateConvertString(dateAndTimeStr, "yyyy-MM-dd HH:mm:ss", retformatStr);
    }


    public static String FormatDateGetString(Date date, String formatStr) {
        String strDate = "";

        // LogUtil.e(formatStr);

        SimpleDateFormat fromFormat;
        fromFormat = new SimpleDateFormat(formatStr);
        try {
            strDate = fromFormat.format(date.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            date = new Date();
            strDate = fromFormat.format(date.getTime());
        }

        return strDate;
    }


    /**
     * input : dateString
     *
     * @return calendar
     */
    public static Calendar FormatStringGetCalendar(String dateAndTimeStr, String formatStr) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(formatStr);
        Calendar cal = new GregorianCalendar();
        Date fromDate = null;
        try {
            fromDate = fromFormat.parse(dateAndTimeStr);
        } catch (Exception e) {
            fromDate = new Date();
            e.printStackTrace();
        }
        cal.setTime(fromDate);

        return cal;
    }


    public static String FormatCalendarGetString(Calendar cal, String formatStr) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(formatStr);
        String str = fromFormat.format(cal.getTime());

        return str;
    }


    public static String FormatCalendarGetString(Calendar cal) {
        return FormatCalendarGetString(cal, "yyyyMMddHHmmss");
    }


    public static String FormatCalendarGetDayOfWeek(Calendar cal) {
        String[] dayOfWeek =
                {"", "일", "월", "화", "수", "목", "금", "토"};
        return dayOfWeek[cal.get(Calendar.DAY_OF_WEEK)];
    }


    public static String getDateDay(String date, String dateType) {
        String day = "";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(dateType);
            Date nDate = dateFormat.parse(date);

            Calendar cal = Calendar.getInstance();
            cal.setTime(nDate);

            int dayNum = cal.get(Calendar.DAY_OF_WEEK);

            switch (dayNum) {
                case 1:
                    day = "일";
                    break;
                case 2:
                    day = "월";
                    break;
                case 3:
                    day = "화";
                    break;
                case 4:
                    day = "수";
                    break;
                case 5:
                    day = "목";
                    break;
                case 6:
                    day = "금";
                    break;
                case 7:
                    day = "토";
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return day;
    }


    /**
     * 특정 시간의 오전/오후 정보를 가져온다.
     *
     * @param strDate "hh:mm" 형태로 특정시간를 입력
     * @return 오전 or 오후 를 반환
     */
    public static String getAmPm(String strDate) {
        Locale locale = Locale.KOREA;
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm", locale);
        try {
            Date date = sdf.parse(strDate);
            SimpleDateFormat sdf2 = new SimpleDateFormat("a", locale);
            return sdf2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static String convertDateAmPm(String strDate) {
        String format = "yyyy-MM-dd a hh:mm:ss";
        return convertDateAmPm(strDate, format);
    }

    public static String convertDateAmPm(String strDate, String format) {
        String str = "";


        Locale locale = Locale.KOREA;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);
        try {
            Date date = sdf.parse(strDate);
            SimpleDateFormat sdf2 = new SimpleDateFormat(format, locale);
            str = sdf2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return str;
    }

    public static String convertDateAmPmRemoveToday(String strDate) {
        String str = "";
        String format = "yyyy-MM-dd a hh:mm:ss";
        String today = FormatUtil.getCurrentDate();

        Locale locale = Locale.KOREA;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);
        try {
            Date date = sdf.parse(strDate);
            SimpleDateFormat sdf2 = new SimpleDateFormat(format, locale);
            str = sdf2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (str.contains(today)) {
            str = str.replace(today, "").trim();
        }

        return str;
    }


    /**
     * 특정 시간의 오전 정보만을 가져온다.
     *
     * @param "HH:mm" 형태로 특정시간를 입력
     * @return 12:00 이상일때 오전시간만을 반환
     */
    public static String getOnlyAM(String strTime) {
        Locale locale = Locale.KOREA;
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm", locale);
        try {
            Date date = sdf.parse(strTime);
            return sdf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static boolean isPhoneNumber(String phoneNum) {
        Pattern p = Pattern.compile("01[016789][1-9]{1}[0-9]{2,3}[0-9]{4}$");
        Matcher matcher = p.matcher(phoneNum);

        return matcher.matches();
    }


    /**
     * 하이픈 변환
     *
     * @param phoneNum
     * @return
     */
    public static String toPhoneNumber(String phoneNum) {
        if (isNullorEmpty(phoneNum)) return "";

        String phone = PhoneNumberUtils.formatNumber(phoneNum);

        try {
            if (!phone.contains("-")) {
                if (phone.startsWith("02")) {
                    if (phone.length() == 9) {
                        phone = phone.substring(0, 2) + "-" + phone.substring(2, 5) + "-" + phone.substring(5, 9);
                    } else if (phone.length() == 10) {
                        phone = phone.substring(0, 2) + "-" + phone.substring(2, 6) + "-" + phone.substring(6, 10);
                    }
                } else if (phone.length() == 10) {
                    phone = phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6, 10);
                } else if (phone.length() == 11) {
                    phone = phone.substring(0, 3) + "-" + phone.substring(3, 7) + "-" + phone.substring(7, 11);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return phone;
    }


    public static String nullStringToBlank(String str) {
        if (isNullorEmpty(str)) return "";

        return str;
    }


    public static int getStringNumber(String str) {
        int retVal = -1;
        if (isNullorEmpty(str)) {
            return retVal;
        } else {
            try {
                retVal = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            return retVal;
        }
    }


    public static int getByteLength(String str, String charset) {

        int len = -1;

        try {
            len = str.getBytes(charset).length;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return len;
    }


    public static String getStringOfBundle(Bundle bnd, String key) {
        return getStringOfBundle(bnd, key, "");
    }


    public static String getStringOfBundle(Bundle bnd, String key, String defVal) {
        if (bnd == null) return defVal;

        String value = bnd.getString(key);
        return FormatUtil.isNullorEmpty(value) ? defVal : value;
    }

    public static String getCurrentYm() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();

        currentCal.add(Calendar.DATE, 0);

        String year = Integer.toString(currentCal.get(Calendar.YEAR));
        String month = df.format(currentCal.get(Calendar.MONTH) + 1);
        String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));

        return year + month;
    }

    public static String getCurrentDate() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();

        currentCal.add(Calendar.DATE, 0);

        String year = Integer.toString(currentCal.get(Calendar.YEAR));
        String month = df.format(currentCal.get(Calendar.MONTH) + 1);
        String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));

        return year + "-" + month + "-" + day;
    }

    public static String getCurrentMatchDate() {
        int hour = getCurrentHour();

        if (hour >= 16) {
            return getCurrentDate();
        }

        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();

        currentCal.add(Calendar.DATE, -1);

        String year = Integer.toString(currentCal.get(Calendar.YEAR));
        String month = df.format(currentCal.get(Calendar.MONTH) + 1);
        String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));

        return year + "-" + month + "-" + day;
    }

    public static int getCurrentYear() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();
        currentCal.add(Calendar.DATE, 0);
        return currentCal.get(Calendar.YEAR);
    }


    public static int getCurrentHour() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();
        currentCal.add(Calendar.DATE, 0);
        return currentCal.get(Calendar.HOUR_OF_DAY);
    }

    public static int getCurrentMinute() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();
        currentCal.add(Calendar.DATE, 0);
        return currentCal.get(Calendar.MINUTE);
    }


    public static String getCurrentDateAndHour() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();

        currentCal.add(Calendar.DATE, 0);

        String year = Integer.toString(currentCal.get(Calendar.YEAR));
        String month = df.format(currentCal.get(Calendar.MONTH) + 1);
        String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));
        String hour = df.format(currentCal.get(Calendar.HOUR));

        return year + "-" + month + "-" + day + " " + hour;
    }


    public static String getCurrentDateHms() {
        DecimalFormat df = new DecimalFormat("00");
        Calendar currentCal = Calendar.getInstance();

        currentCal.add(Calendar.DATE, 0);

        String year = Integer.toString(currentCal.get(Calendar.YEAR));
        String month = df.format(currentCal.get(Calendar.MONTH) + 1);
        String day = df.format(currentCal.get(Calendar.DAY_OF_MONTH));
        String hour = df.format(currentCal.get(Calendar.HOUR));
        String m = df.format(currentCal.get(Calendar.MINUTE));
        String s = df.format(currentCal.get(Calendar.SECOND));

        return year + month + day + "_" + hour + m + s;
    }


    /**
     * 금일 날짜는 삭제 후에 오후 02:10 식으로 리턴
     * <p>
     * 지난 날짜는 해당 날짜 그대로 리턴
     *
     * @param date
     * @return
     */
    public static String convertTodayRemoveDate(String date) {
        String today = FormatUtil.getCurrentDate();

        if (date.contains(today)) {
            date = date.replace(today, "").replace(" ", "");
            date = FormatUtil.getAmPm(date) + " " + date;
        }

        return date;
    }


    /**
     * 금일 날짜는 오늘 13:02
     * <p>
     * 지난 날짜는 해당 날짜 그대로 리턴
     *
     * @param date
     * @return
     */
    public static String convertTodayDisplyDate(String date) {
        String today = FormatUtil.getCurrentDate();
        date = FormatDateConvertString(date, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
        LogUtil.d("date : " + date);
        LogUtil.d("today : " + today);
        if (date.contains(today)) {
            date = date.replace(today, "").replace(" ", "");
            date = "오늘 " + date;
        }

        return date;
    }


    public static String toDateTime(String dateTime) {
        String today = FormatUtil.getCurrentDate();
        dateTime = FormatDateConvertString(dateTime, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd");
        LogUtil.d("dateTime : " + dateTime);
        LogUtil.d("today : " + today);
        if (dateTime.contains(today)) {
            dateTime = "오늘";
        }
        return dateTime;
    }


    /**
     * 날짜 변환 (Calendar -> String)
     *
     * @param cal
     * @param formatStr
     * @return
     */
    public static String getDateCalendarToString(Calendar cal, String formatStr) {
        SimpleDateFormat fromFormat = new SimpleDateFormat(formatStr);
        String str = "";
        try {
            str = fromFormat.format(cal.getTime());
        } catch (Exception e) {
        }

        return str;
    }


    public static String getContentsDateFormat(String date) {
        if (FormatUtil.isNullorEmpty(date)) return "";

        String today = FormatUtil.getCurrentDate();
        int todayDate = Integer.valueOf(today.substring(8, 10));
        String dateT = FormatDateConvertString(date, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
        int dateTDate = Integer.valueOf(dateT.substring(8, 10));
        if (dateT.startsWith(today)) {
            dateT = "오늘";
        } else if (todayDate == dateTDate + 1) {
            dateT = "어제";
        } else {
            dateT = dateT.substring(0, 10);
        }

        return dateT;
    }


    public static String getReplyDateFormat(String date) {
        if (FormatUtil.isNullorEmpty(date)) return "";

        Calendar currentTime = Calendar.getInstance();
        String today = FormatUtil.getCurrentDate();
        int todayDate = Integer.valueOf(today.substring(8, 10));
        int todayMinute = 0;
        String dateT = FormatDateConvertString(date, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
        LogUtil.e("dateT" + dateT);

        int dateTYear = Integer.valueOf(dateT.substring(0, 4));
        int dateTDate = Integer.valueOf(dateT.substring(8, 10));
        int dateTHour = Integer.valueOf(dateT.substring(11, 13));
        int dateTMinute = Integer.valueOf(dateT.substring(14));


        int todayYear = currentTime.get(Calendar.YEAR);
        int todayHour = currentTime.get(Calendar.HOUR_OF_DAY);
        LogUtil.e("dateTYear : " + dateTYear);
        LogUtil.e("todayYear : " + todayYear);

        if (dateT.startsWith(today)) {
            dateT = String.valueOf(todayHour - dateTHour);
            todayMinute = currentTime.get(Calendar.MINUTE);

            LogUtil.e("dateT" + dateT);

            if ("0".equals(dateT) && ((todayMinute - dateTMinute) < 4)) {
                dateT = "방금전";
            } else if (("0".equals(dateT) && ((todayMinute - dateTMinute) > 3))) {
                dateT = (todayMinute - dateTMinute) + "분전";
            } else {
                dateT = String.valueOf(todayHour - dateTHour) + "시간전";
            }

        } else if (todayDate == dateTDate + 1) {
            dateT = "어제";
        } else {
            if (todayYear != dateTYear) {
                dateT = dateT.substring(0, 4) + "." + dateT.substring(5, 7) + "." + dateT.substring(8, 10);
            } else {
                dateT = dateT.substring(5, 7) + "." + dateT.substring(8, 10);
            }
        }

        return dateT;
    }


    public static String getHeartDateFormat(String date) {
        if (FormatUtil.isNullorEmpty(date)) return "";
        String dateT = "방금";
        try {

            Date format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
            Date format2 = new Date();

            long diffMin = (format2.getTime() - format1.getTime()) / 60000; //분 차이

            if (diffMin < 1) {
                dateT = "방금전";
            } else {
                dateT = diffMin + "분전";
            }

        } catch (ParseException e) {

        }
        return dateT;
    }


    public static String getZzimDate(String date) {
        if (FormatUtil.isNullorEmpty(date)) return "";

        Calendar currentTime = Calendar.getInstance();
        String today = FormatUtil.getCurrentDate();
        int todayDate = Integer.valueOf(today.substring(8, 10));
        String today2 = FormatUtil.getCurrentDateAndHour(); // yyyy-MM-dd HH
        // int todayHour = Integer.valueOf(today2.substring(11));
        int todayMinute = 0;
        String dateT = FormatDateConvertString(date, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");
        LogUtil.e("dateT" + dateT);
        int dateTDate = Integer.valueOf(dateT.substring(8, 10));
        int dateTHour = Integer.valueOf(dateT.substring(11, 13));
        int dateTMinute = Integer.valueOf(dateT.substring(14));

        int todayHour = currentTime.get(Calendar.HOUR_OF_DAY);

        if (dateT.startsWith(today)) {
            dateT = "D-3";

        } else if (todayDate == dateTDate + 1) {
            dateT = "D-2";
        } else if (todayDate == dateTDate + 2) {
            dateT = "D-1";
        } else {
            dateT = "D-0";
        }

        return dateT;
    }


    public static String getRegistDateFormat(String registDT) {
        String result = "";

        if (FormatUtil.isNullorEmpty(registDT)) return "";
        // String date = FormatDateConvertString(registDT, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm");

        Calendar cal = new GregorianCalendar();

        cal.set(Integer.parseInt(registDT.substring(0, 4)), Integer.parseInt(registDT.substring(5, 7)), Integer.parseInt(registDT.substring(8, 10)), Integer.parseInt(registDT.substring(11, 13)), Integer.parseInt(registDT.substring(14, 16)), Integer.parseInt(registDT.substring(17, 19)));

        String DayOfWeek = "";

        switch (cal.get(Calendar.DAY_OF_WEEK)) {
            case 1:
                DayOfWeek = "일";
                break;
            case 2:
                DayOfWeek = "월";
                break;
            case 3:
                DayOfWeek = "화";
                break;
            case 4:
                DayOfWeek = "수";
                break;
            case 5:
                DayOfWeek = "목";
                break;
            case 6:
                DayOfWeek = "금";
                break;
            case 7:
                DayOfWeek = "토";
                break;
        }

        result = "최종수정일 : " + cal.get(Calendar.YEAR) + "." + cal.get(Calendar.MONTH) + "." + cal.get(Calendar.DATE) + "(" + DayOfWeek + ") " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.HOUR_OF_DAY);

        return result;
    }


    public static ArrayList<int[]> getSpans(String body, char prefix) {
        ArrayList<int[]> spans = new ArrayList<int[]>();

        Pattern pattern = Pattern.compile(prefix + "\\w+");
        Matcher matcher = pattern.matcher(body);

        // Check all occurrences
        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }


    public static String replaceTag(String cont) {

        if (FormatUtil.isNullorEmpty(cont)) {
            return "";
        }

        cont = cont.replace("\n", "<br>");
        cont = cont.replace("<p>", "");
        cont = cont.replace("</p>", "");

        return cont;
    }


    public static HashMap<String, String> getQueryStringData(String query) {
        HashMap<String, String> map = new HashMap<String, String>();

        query = query.replace("&amp;", "&");

        String[] arr = query.split("&");

        for (String v : arr) {
            String[] values = v.split("=");
            if (values.length == 2) {
                map.put(values[0], values[1]);
            } else {
                map.put(values[0], "");
            }
        }

        return map;
    }


    public static byte[] getBytes(final String data, final String charset) {

        if (data == null) {
            throw new IllegalArgumentException("data may not be null");
        }

        if (charset == null || charset.length() == 0) {
            throw new IllegalArgumentException("charset may not be null or empty");
        }

        try {
            return data.getBytes(charset);
        } catch (UnsupportedEncodingException e) {
            return data.getBytes();
        }
    }


    public static String getAdDesc(Context cx, int _amount_ticket_ad, double _point_amount, boolean isActive, int adCount){
        String txt = "";
        if (isActive && adCount > 0) {
//            if (_amount_ticket_ad > 0 && _point_amount > 0) {
//                txt = "동영상 광고를 시청하고<br><font color='red'>투표권 " + _amount_ticket_ad + "장</font>, <font color='red'> 포인트 " + FormatUtil.toPriceFormat(_point_amount) + "P</font>를 받으세요";
//            } else {
//                txt = "동영상 광고를 시청하고<br><font color='red'>투표권 " + SPUtil.getInstance().getAmountTicketAd(cx) + "장</font>, <font color='red'>포인트</font>를 받으세요";
//            }

            txt = "동영상 광고를 시청하고<br><font color='red'>투표권</font> / <font color='red'>포인트</font> / <font color='red'>하트</font>를 받으세요";
            
            if(SPUtil.getInstance().getAdFullScreen(cx).equals("Y") == false)
                txt = "동영상 광고를 시청하고<br><font color='red'>투표권</font>을 받으세요";

        } else if (adCount < 1 || isActive == false) {
//            txt = "동영상 광고를 시청하고<br>투표권 " + SPUtil.getInstance().getAmountTicketAd(cx) + "장, 포인트 " + FormatUtil.toPriceFormat(_point_amount) + "P를 받으세요";

            txt = "동영상 광고를 시청하고<br>투표권 / 포인트 / 하트를 받으세요";

            if(SPUtil.getInstance().getAdFullScreen(cx).equals("Y") == false)
                txt = "동영상 광고를 시청하고<br>투표권을 받으세요";
        }
        return txt;
    }


    public static String insertRandomAsterisks(String input, int asteriskCount) {
        if (input == null || input.length() < 2 || input.length() > 10) {
            throw new IllegalArgumentException("입력 문자열은 2글자에서 10글자 사이여야 합니다.");
        }

        // 문자열을 한 글자씩 배열로 변환
        String[] characters = input.split("");

        // 랜덤 위치 선택을 위한 인덱스 배열 생성
        Random random = new Random();
        int length = characters.length;

        // 랜덤으로 인덱스를 선택하여 별표로 대치
        for (int i = 0; i < asteriskCount; i++) {
            int randomIndex;
            do {
                randomIndex = random.nextInt(length); // 0부터 문자열 길이 사이의 인덱스 선택
            } while (characters[randomIndex].equals("*")); // 이미 별표로 대치된 경우 건너뜀
            characters[randomIndex] = "*";
        }

        // 배열을 다시 문자열로 결합
        return String.join("", characters);
    }
}