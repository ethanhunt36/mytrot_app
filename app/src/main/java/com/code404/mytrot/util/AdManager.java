package com.code404.mytrot.util;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.ads.MaxRewardedAd;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.OnUserEarnedRewardListener;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAd;
import com.google.android.gms.ads.rewardedinterstitial.RewardedInterstitialAdLoadCallback;
import com.igaworks.ssp.AdPopcornSSP;
import com.igaworks.ssp.AdPopcornSSPUserProperties;
import com.igaworks.ssp.SSPErrorCode;
import com.igaworks.ssp.part.interstitial.AdPopcornSSPInterstitialAd;
import com.igaworks.ssp.part.interstitial.listener.IInterstitialLoadEventCallbackListener;
import com.igaworks.ssp.part.interstitial.listener.IInterstitialShowEventCallbackListener;
import com.igaworks.ssp.part.video.AdPopcornSSPInterstitialVideoAd;
import com.igaworks.ssp.part.video.AdPopcornSSPRewardVideoAd;
import com.tnkfactory.ad.AdItem;
import com.tnkfactory.ad.AdListener;
import com.tnkfactory.ad.InterstitialAdItem;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class AdManager {

    private static String TAG = "AdManager";
    private Context _context = null;
    private Activity _activity = null;
    private InterfaceSet.OnAd2CompleteListener _onAdCompleteListener;


    private RewardedAd _rewardedAd = null;
    private RewardedInterstitialAd _rewardedInterstitialAd;
    private InterstitialAd _interstitialAd;

    private InterstitialAdItem _interstitialAdTnk = null;
    private InterstitialAdItem _interstitialAdRewardTnk = null;
//    // 카울리 전면 광고
//    private CaulyInterstitialAd _interstialCaulyAd;
//    // WAD
//    private WInterstitial _interstitialWad;

    // MaxRewardedAd
    private MaxRewardedAd _maxRewardedAd = null;

    private MaxInterstitialAd _maxInterstitialAd = null;


    private AdPopcornSSPInterstitialAd _adPopcornSSPInterstitialAd;
    private AdPopcornSSPInterstitialVideoAd _adPopcornSSPInterstitialVideoAd;
    private AdPopcornSSPRewardVideoAd _adPopcornSSPRewardVideoAd;


    private int MAX_WAIT_AD_LOADING_TIME = 2500;

    private Handler _handler = null;

    private Runnable _runnable_g_full = null;
    private Runnable _runnable_g_reward = null;
    private Runnable _runnable_g_reward_full = null;

    private Runnable _runnable_tnk_full = null;
    private Runnable _runnable_tnk_reward = null;

    private Runnable _runnable_interstitialAdPopCorn = null;
    private Runnable _runnable_interstitialCauly = null;
    private Runnable _runnable_interstitialMax = null;
    private Runnable _runnable_interstitialWad = null;
    private Runnable _runnable_interstitialUnity = null;


    private Context getContext() {
        return _context;
    }


    private Activity getActivity() {
        return _activity;
    }


    private AdSize _adSize = null;


    private boolean _is_show_failed = false;


    public void setOnAdCompleteListener(InterfaceSet.OnAd2CompleteListener onAdCompleteListener) {

        _onAdCompleteListener = onAdCompleteListener;

    }


    public void setAdSize(AdSize adSize) {

    }

    public boolean showAd() {

        if (_rewardedAd != null) {
            LogUtil.d(TAG, "_txtAd, show g reward");
            _rewardedAd.show(getActivity(), new OnUserEarnedRewardListener() {
                @Override
                public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                    LogUtil.d(TAG, "_txtAd, createGoogleReward- onUserEarnedReward");
                    _rewardedAd = null;
                }
            });

            removeRunnable_G_Reward();
            return true;
        } else if (_rewardedInterstitialAd != null) {
            LogUtil.d(TAG, "_txtAd, show g full reward");
            _rewardedInterstitialAd.show(getActivity(), new OnUserEarnedRewardListener() {
                @Override
                public void onUserEarnedReward(@NonNull RewardItem rewardItem) {
                    LogUtil.d(TAG, "_txtAd, onUserEarnedReward rewardItem : " + rewardItem.toString());

                    _rewardedInterstitialAd = null;

                    if (_onAdCompleteListener != null)
                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.g_reward_full);
                }
            });
            removeRunnable_G_Reward_Full();
            return true;
        } else if (_interstitialAd != null) {
            LogUtil.d(TAG, "_txtAd, show g full");
            _interstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
                @Override
                public void onAdClicked() {
                    // Called when a click is recorded for an ad.
                    LogUtil.d(TAG, "_txtAd, google full, Ad was clicked.");
                }

                @Override
                public void onAdDismissedFullScreenContent() {
                    // Called when ad is dismissed.
                    // Set the ad reference to null so you don't show the ad a second time.
                    LogUtil.d(TAG, "_txtAd, google full,  Ad dismissed fullscreen content.");
                    _interstitialAd = null;

                    if (_onAdCompleteListener != null)
                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.g_full_screen);

                }

                @Override
                public void onAdFailedToShowFullScreenContent(AdError adError) {
                    // Called when ad fails to show.
                    LogUtil.d(TAG, "_txtAd, google full,  Ad failed to show fullscreen content.");
                    _interstitialAd = null;
                }

                @Override
                public void onAdImpression() {
                    // Called when an impression is recorded for an ad.
                    LogUtil.d(TAG, "_txtAd, google full,  Ad recorded an impression.");

                    _ad_queue_ready_complete.offer(Constants.enum_ad.g_full_screen);
                }

                @Override
                public void onAdShowedFullScreenContent() {
                    // Called when ad is shown.
                    LogUtil.d(TAG, "_txtAd, google full,  Ad showed fullscreen content.");
                }
            });
            _interstitialAd.show(getActivity());
            removeRunnable_G_Full();
            return true;
        } else if (_interstitialAdTnk != null && _interstitialAdTnk.isLoaded()) {
            LogUtil.d(TAG, "_txtAd, show tnk full");
            _interstitialAdTnk.show(getContext());
            removeRunnable_Tnk_Full();
            return true;
        } else if (_interstitialAdRewardTnk != null && _interstitialAdRewardTnk.isLoaded()) {
            LogUtil.d(TAG, "_txtAd, show tnk reward");
            _interstitialAdRewardTnk.show(getContext());
            removeRunnable_Tnk_Reward();
            return true;
//        } else if (_interstialCaulyAd != null && _interstialCaulyAd.canShow()) {
//            LogUtil.d(TAG, "_txtAd, show cauly");
//            _interstialCaulyAd.show();
//            removeRunnable_Cauly();
//            return true;
        } else if (_adPopcornSSPInterstitialAd != null && _adPopcornSSPInterstitialAd.isLoaded()) {
            LogUtil.d(TAG, "_txtAd, show adpopcorn");
            _adPopcornSSPInterstitialAd.showAd();
            removeRunnable_AdPopcorn();
            return true;
//        } else if (_interstitialWad != null && _interstitialWad.isReady()) {
//            LogUtil.d(TAG, "_txtAd, show wad");
//            _interstitialWad.show();
//            removeRunnable_WAD();
//            return true;
        } else if (_maxInterstitialAd != null && _maxInterstitialAd.isReady()) {
            LogUtil.d(TAG, "_txtAd, show max");
            _maxInterstitialAd.showAd();
            removeRunnable_MAX_FULL();
            return true;
        }
//        else {
//            Constants.enum_ad ad_type = _ad_queue_ready_complete.poll();
//
//            if (ad_type == Constants.enum_ad.unityAds_full) {
//
//                LogUtil.d(TAG, "_txtAd, show unity");
//
//                showUnityAdsFull();
//                removeRunnable_UNITY();
//                return true;
//            }
//        }
        LogUtil.d(TAG, "_txtAd, show failed....................................");
        return false;
    }

    private int _tryCount = 0;
    private static int _tryCountStatic = 0;

    private void readyNext() {
        _tryCount++;


        int size = _ad_queue_load.size();

        Constants.enum_ad ad_type = _ad_queue_load.poll();

        LogUtil.d(TAG, "========================================================================");
        LogUtil.d(TAG, "_txtAd, readyNext, ad_type : " + ad_type + ", _tryCount : " + _tryCount + ", size : " + size);

        if (ad_type == Constants.enum_ad.g_full_screen) createGoogleFull();
        else if (ad_type == Constants.enum_ad.g_reward_full) createGoogleRewardFull();
        else if (ad_type == Constants.enum_ad.g_reward) createGoogleReward();
        else if (ad_type == Constants.enum_ad.tnk_full) createTnkFull();
        else if (ad_type == Constants.enum_ad.tnk_reward) createTnkReward();

//        else if (ad_type == Constants.enum_ad.cauly_full) createCaulyAd();
//        else if (ad_type == Constants.enum_ad.wad_full) createWadFull();
//        else if (ad_type == Constants.enum_ad.unityAds_full) createUnityAds();
        else if (ad_type == Constants.enum_ad.max_full) createMaxFull();
        else if (ad_type == Constants.enum_ad.adpc_full) createAdPopcornFull();
        else {
            if (_onAdCompleteListener != null) _onAdCompleteListener.onAdLoadFail();
        }
    }

    private Queue<Constants.enum_ad> _ad_queue_ready_complete = new LinkedList();
    private Queue<Constants.enum_ad> _ad_queue_load = new LinkedList();

    private boolean _isOnlyFull = true;

    public void ready(Activity activity, Context context, InterfaceSet.OnAd2CompleteListener listener) {
        ready(activity, context, false, listener);
    }


    public void ready(Activity activity, Context context, boolean isOnlyFull, InterfaceSet.OnAd2CompleteListener listener) {
        _activity = activity;
        _context = context;
        _onAdCompleteListener = listener;

        _tryCount = 0;
        _ad_queue_load.clear();
        _ad_queue_ready_complete.clear();

        List<Constants.enum_ad> ad_type_list = new ArrayList<Constants.enum_ad>();
        List<Constants.enum_ad> ad_type_list_02 = new ArrayList<Constants.enum_ad>();

        _isOnlyFull = isOnlyFull;

        LogUtil.d(TAG, "_txtAd, ready, isOnlyFull : " + isOnlyFull);

        if (isOnlyFull == false) {
            if (Constants.IS_VIEW_AD_GOOGLE) ad_type_list.add(Constants.enum_ad.g_reward_full);
            if (Constants.IS_VIEW_AD_GOOGLE) ad_type_list.add(Constants.enum_ad.g_reward);

            if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
                ad_type_list.add(Constants.enum_ad.tnk_reward);
                ad_type_list.add(Constants.enum_ad.max_full);
            }
        }

        if (SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")) {
//            ad_type_list.add(Constants.enum_ad.cauly_full);
//            ad_type_list.add(Constants.enum_ad.wad_full);
//            ad_type_list.add(Constants.enum_ad.unityAds_full);
            ad_type_list.add(Constants.enum_ad.tnk_full);
//            ad_type_list.add(Constants.enum_ad.adpc_full);
            if (Constants.IS_VIEW_AD_GOOGLE) ad_type_list.add(Constants.enum_ad.g_full_screen);
        }


        JSONArray jsonArray = SPUtil.getInstance().getAdSeqFull_V4(getContext());

        for (int i = 0; i < jsonArray.length(); i++) {
            Constants.enum_ad ad_type = getIndexEnumAdType(i);

            if (ad_type == null || ad_type == Constants.enum_ad.none) continue;
//            if (!ad_type_list.contains(ad_type)) continue;

            ad_type_list_02.add(ad_type);
        }

        _tryCountStatic++;


        LogUtil.d(TAG, "========================================================================");
        LogUtil.d(TAG, "_txtAd, ad_type_list : " + ad_type_list.size());
        LogUtil.d(TAG, "_txtAd, ad_type_list_02 : " + ad_type_list_02.size());

        if (_tryCountStatic % 3 == 0 || isOnlyFull) {
            Collections.shuffle(ad_type_list);
            Collections.shuffle(ad_type_list_02);

            LogUtil.d(TAG, "_txtAd, ad_type_list_02, shuffle");

            if (_tryCountStatic > 9999) _tryCountStatic = 0;
        }

        LogUtil.d(TAG, "========================================================================");

        for (Constants.enum_ad s : ad_type_list) {
            LogUtil.d(TAG, "_txtAd, ad_type[1] : " + s);
        }

        LogUtil.d(TAG, "========================================================================");

        for (Constants.enum_ad s : ad_type_list_02) {
            LogUtil.d(TAG, "_txtAd, ad_type[2] : " + s);
            _ad_queue_load.offer(s);

            //if (isOnlyFull && _ad_queue_load.size() > 4) break;
        }

        LogUtil.d(TAG, "_txtAd, ad_type, size : " + _ad_queue_load.size());

        readyNext();
    }

    private Constants.enum_ad getIndexEnumAdType(int index) {

        JSONArray jsonArray = SPUtil.getInstance().getAdSeqFull_V4(getContext());

        JSONObject json = JSONUtil.getJSONObject(jsonArray, index);

        String ad_name = JSONUtil.getString(json, "ad_name");

        LogUtil.d("_txtAd, getIndexEnumAdType : index : " + index + ", ad_name : " + ad_name);

        return Constants.getEnumAd(ad_name);
    }


    private void createGoogleReward() {
        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.g_reward) == false) {
            readyNext();
            return;
        }

        RewardedAd rewardedAd = null;
        try {


            RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
                @Override
                public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                    super.onAdFailedToLoad(loadAdError);
                    LogUtil.d(TAG, "_txt, createGoogleReward => onAdFailedToLoad");


                    if (_runnable_g_reward == null) {
                        LogUtil.d(TAG, "_txtAd, createGoogleReward, already postDelayed, onAdLoadFailed[1]");
                        return;
                    }

                    //if(_onAdCompleteListener != null) _onAdCompleteListener.onAdLoadFail();
                    removeRunnable_G_Reward();
                    readyNext();
                    _rewardedAd = null;
                }

                @Override
                public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
                    super.onAdLoaded(rewardedAd);

                    LogUtil.d(TAG, "_txtAd, onReward- createGoogleReward, onAdLoaded");

                    _ad_queue_ready_complete.offer(Constants.enum_ad.g_reward);
                    _rewardedAd = rewardedAd;


                    if (_runnable_g_reward == null) {
                        LogUtil.d(TAG, "_txtAd, createGoogleReward, already postDelayed, onAdLoadFailed[2]");
                        return;
                    }

                    if (_onAdCompleteListener != null) _onAdCompleteListener.onAdLoadSuccess();

                    removeRunnable_G_Reward();

                    FullScreenContentCallback fullScreenContentCallback =
                            new FullScreenContentCallback() {
                                @Override
                                public void onAdShowedFullScreenContent() {
                                    // Code to be invoked when the ad showed full screen content.
                                    LogUtil.d(TAG, "_txtAd, createGoogleReward- onAdShowedFullScreenContent");
                                }

                                @Override
                                public void onAdDismissedFullScreenContent() {
                                    _rewardedAd = null;
                                    LogUtil.d(TAG, "_txtAd, createGoogleReward- onAdDismissedFullScreenContent");

                                    if (_onAdCompleteListener != null)
                                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.g_reward);
                                }
                            };

                    _rewardedAd.setFullScreenContentCallback(fullScreenContentCallback);
                }
            };

            RewardedAd.load(getContext(), Constants.getKeyAdGoogleRewardID(_isOnlyFull, SPUtil.getInstance().getChargeMode(getContext())), new AdRequest.Builder().build(), adLoadCallback);


            _handler = new Handler();
            _runnable_g_reward = new Runnable() {
                @Override
                public void run() {

                    if (_rewardedAd == null) {
                        LogUtil.d(TAG, "_txtAd, createGoogleReward, postDelayed, onAdLoadFailed");
                        readyNext();
                    }

                    _rewardedAd = null;
                    _runnable_g_reward = null;
                }
            };
            _handler.postDelayed(_runnable_g_reward, MAX_WAIT_AD_LOADING_TIME);

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.d(TAG, "_txtAd, createGoogleReward, Exception, " + e.toString());


            removeRunnable_G_Reward();
            readyNext();
            _rewardedAd = null;

        }

    }


    private void createGoogleRewardFull() {

        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.g_reward_full, AdFullOrgManager._adLoadingTimeInterval * 2) == false) {
            readyNext();
            return;
        }
        LogUtil.d(TAG, "_txtAd, createGoogleRewardFull");

        try {
            if (getContext() == null) return;


            RewardedInterstitialAd.load(getContext(), Constants.getKeyAdGoogleRewardFullID(_isOnlyFull, SPUtil.getInstance().getChargeMode(getContext())),
                    new AdRequest.Builder().build(), new RewardedInterstitialAdLoadCallback() {
                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            super.onAdFailedToLoad(loadAdError);

                            LogUtil.d(TAG, "_txtAd, createGoogleRewardFull onAdFailedToLoad");

                            if (_runnable_g_reward_full == null) {
                                LogUtil.d(TAG, "_txtAd, createGoogleRewardFull, already postDelayed, onAdLoadFailed[1]");
                                return;
                            }

                            removeRunnable_G_Reward_Full();
                            readyNext();
                            _rewardedInterstitialAd = null;
                        }

                        @Override
                        public void onAdLoaded(@NonNull RewardedInterstitialAd rewardedInterstitialAd) {
                            super.onAdLoaded(rewardedInterstitialAd);

                            LogUtil.d(TAG, "_txtAd, createGoogleRewardFull onAdLoaded");

                            _ad_queue_ready_complete.offer(Constants.enum_ad.g_reward_full);

                            _rewardedInterstitialAd = rewardedInterstitialAd;

                            if (_runnable_g_reward_full == null) {
                                LogUtil.d(TAG, "_txtAd, createGoogleRewardFull, already postDelayed, onAdLoadFailed[2]");
                                return;
                            }

                            removeRunnable_G_Reward_Full();

                            if (_onAdCompleteListener != null)
                                _onAdCompleteListener.onAdLoadSuccess();
                        }
                    });


            _handler = new Handler();
            _runnable_g_reward_full = new Runnable() {
                @Override
                public void run() {

                    if (_rewardedInterstitialAd == null) {
                        LogUtil.d(TAG, "_txtAd, createGoogleRewardFull, postDelayed, onAdLoadFailed");
                        readyNext();
                    }

                    _rewardedInterstitialAd = null;
                    _runnable_g_reward_full = null;
                }
            };
            _handler.postDelayed(_runnable_g_reward_full, MAX_WAIT_AD_LOADING_TIME);

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.d(TAG, "_txtAd, createGoogleRewardFull, Exception, " + e.toString());


            removeRunnable_G_Reward_Full();
            readyNext();
            _rewardedInterstitialAd = null;
        }
    }


    private void createGoogleFull() {

        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.g_full_screen) == false) {
            readyNext();
            return;
        }

        LogUtil.d(TAG, "_txtAd, createGoogleFull");

        try {

            if (getContext() == null) {
                LogUtil.d(TAG, "_txtAd, createGoogleFull, getContext is null");
                return;
            }

//            MediationTestSuite.launch(getContext());

//            Bundle extras_facebook = new FacebookExtras()
//                    .setNativeBanner(false)
//                    .build();

//            String[] placements = new String[2];
//            placements[0] = Constants.KEY_VUNGLE_FULL_PLACE_ID;
//            placements[1] = Constants.KEY_VUNGLE_REWARD_PLACE_ID;
//            Bundle extras_vungle = new VungleExtrasBuilder(placements).build();

//            Bundle extras_appLovin = new AppLovinExtras.Builder()
//                    .setMuteAudio(true)
//                    .build();

            Bundle extras_unity = new Bundle();
            //extras.putString("max_ad_content_rating", "T");

            AdRequest request = new AdRequest.Builder()
//                    .addNetworkExtrasBundle(VungleAdapter.class, extras_vungle)
//                    .addNetworkExtrasBundle(VungleInterstitialAdapter.class, extras_vungle)
//                    .addNetworkExtrasBundle(FacebookAdapter.class, extras_facebook)
//                    .addNetworkExtrasBundle(ApplovinAdapter.class, extras_appLovin)
//                    .addNetworkExtrasBundle(UnityAdapter.class, extras_unity)
                    .build();


            InterstitialAd.load(getContext(), Constants.getKeyAdGoogleFullScreenID(_isOnlyFull, SPUtil.getInstance().getChargeMode(getContext())), request,
                    new InterstitialAdLoadCallback() {
                        @Override
                        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                            // The mInterstitialAd reference will be null until
                            // an ad is loaded.
                            _interstitialAd = interstitialAd;
                            LogUtil.d(TAG, "_txtAd, google full, onAdLoaded");

                            if (_runnable_g_full == null) {
                                LogUtil.d(TAG, "_txtAd, google full, already postDelayed, onAdLoadFailed[1]");
                                return;
                            }


                            if (_onAdCompleteListener != null)
                                _onAdCompleteListener.onAdLoadSuccess();

                            removeRunnable_G_Full();
                        }

                        @Override
                        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                            // Handle the error
                            LogUtil.d(TAG, "_txtAd, google full, onAdFailedToLoad");

                            if (_runnable_g_full == null) {
                                LogUtil.d(TAG, "_txtAd, google full, already postDelayed, onAdLoadFailed[2]");
                                return;
                            }

                            removeRunnable_G_Full();
                            readyNext();
                            _interstitialAd = null;
                        }
                    }
            );


            _handler = new Handler();
            _runnable_g_full = new Runnable() {
                @Override
                public void run() {
                    if (_interstitialAd == null) {
                        LogUtil.d(TAG, "_txtAd, google full,, postDelayed, onAdLoadFailed");
                        readyNext();
                    }
                    _interstitialAd = null;
                    _runnable_g_full = null;
                }
            };
            _handler.postDelayed(_runnable_g_full, MAX_WAIT_AD_LOADING_TIME);

        } catch (Exception e) {
            e.printStackTrace();

            LogUtil.d(TAG, "_txtAd, google full,, Exception, " + e.toString());


            removeRunnable_G_Full();
            readyNext();
            _interstitialAd = null;
        }

    }


    private void createTnkFull() {

        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.tnk_full) == false) {
            readyNext();
            return;
        }

        LogUtil.d(TAG, "_txtAd, createTnkFull");

        try {
            _interstitialAdTnk = new InterstitialAdItem(getContext(), Constants.KEY_TNK_FULL, new AdListener() {
                @Override
                public void onLoad(AdItem adItem) {

                    LogUtil.d(TAG, "_txtAd, TNK onInterstitialLoaded");

                    _ad_queue_ready_complete.offer(Constants.enum_ad.tnk_full);


                    if (_runnable_tnk_full == null) {
                        LogUtil.d(TAG, "_txtAd, TNK, already postDelayed, onAdLoadFailed[1]");
                        return;
                    }

                    removeRunnable_Tnk_Full();

                    if (_onAdCompleteListener != null)
                        _onAdCompleteListener.onAdLoadSuccess();
                }

                @Override
                public void onError(AdItem adItem, com.tnkfactory.ad.AdError adError) {
                    super.onError(adItem, adError);

                    LogUtil.d(TAG, "_txtAd, TNK onInterstitialFailed, errorCode : " + adError.toString());

                    if (_runnable_tnk_full == null) {
                        LogUtil.d(TAG, "_txtAd, TNK, already postDelayed, onAdLoadFailed[2]");
                        return;
                    }


                    removeRunnable_Tnk_Full();
                    readyNext();
                    _interstitialAdTnk = null;
                }

                @Override
                public void onShow(AdItem adItem) {
                    super.onShow(adItem);
                }

                @Override
                public void onClick(AdItem adItem) {
                    super.onClick(adItem);

                    LogUtil.d(TAG, "_txtAd, TNK onClick");
                }

                @Override
                public void onClose(AdItem adItem, int i) {
                    super.onClose(adItem, i);

                    LogUtil.d(TAG, "_txtAd, TNK onClose");

                    _interstitialAdTnk = null;

                    //initAd(Constants.enum_ad.none);
                    if (_onAdCompleteListener != null)
                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.tnk_full);
                }
            });

            _interstitialAdTnk.load();


            _handler = new Handler();
            _runnable_tnk_full = new Runnable() {
                @Override
                public void run() {
                    if (_interstitialAdTnk == null || _interstitialAdTnk.isLoaded() == false) {
                        LogUtil.d(TAG, "_txtAd, TNK, postDelayed, onAdLoadFailed");
                        readyNext();
                    }
                    _interstitialAdTnk = null;
                    _runnable_tnk_full = null;
                }
            };
            _handler.postDelayed(_runnable_tnk_full, MAX_WAIT_AD_LOADING_TIME);

        } catch (Exception e) {
            e.printStackTrace();

            LogUtil.d(TAG, "_txtAd, TNK, postDelayed, Exception : " + e.toString());
            _interstitialAdTnk = null;
            removeRunnable_Tnk_Full();
            readyNext();
        }
    }


    private void createTnkReward() {

        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.tnk_reward) == false) {
            readyNext();
            return;
        }
        LogUtil.d(TAG, "_txtAd, createTnkReward");

        try {
            _interstitialAdRewardTnk = new InterstitialAdItem(getContext(), Constants.KEY_TNK_REWARD, new AdListener() {
                @Override
                public void onLoad(AdItem adItem) {

                    LogUtil.d(TAG, "_txtAd, TNK reward onInterstitialLoaded");

                    _ad_queue_ready_complete.offer(Constants.enum_ad.tnk_reward);

                    if (_runnable_tnk_reward == null) {
                        LogUtil.d(TAG, "_txtAd, TNK reward, already postDelayed, onAdLoadFailed[1]");
                        return;
                    }

                    removeRunnable_Tnk_Reward();
                    if (_onAdCompleteListener != null)
                        _onAdCompleteListener.onAdLoadSuccess();
                }

                @Override
                public void onError(AdItem adItem, com.tnkfactory.ad.AdError adError) {
                    super.onError(adItem, adError);

                    LogUtil.d(TAG, "_txtAd, TNK reward onInterstitialFailed, errorCode : " + adError.toString());

                    if (_runnable_tnk_reward == null) {
                        LogUtil.d(TAG, "_txtAd, TNK reward, already postDelayed, onAdLoadFailed[2]");
                        return;
                    }


                    removeRunnable_Tnk_Reward();
                    readyNext();
                    _interstitialAdRewardTnk = null;
                }

                @Override
                public void onShow(AdItem adItem) {
                    super.onShow(adItem);
                }

                @Override
                public void onClick(AdItem adItem) {
                    super.onClick(adItem);

                    LogUtil.d(TAG, "_txtAd, TNK reward onClick");
                }

                @Override
                public void onClose(AdItem adItem, int i) {
                    super.onClose(adItem, i);

                    LogUtil.d(TAG, "_txtAd, TNK reward onClose");

                    _interstitialAdRewardTnk = null;


                }

                /**
                 * 동영상이 포함되어 있는 경우 동영상을 끝까지 시청하는 시점에 호출된다.
                 * @param adItem 이벤트 대상이되는 AdItem 객체
                 * @param verifyCode 동영상 시청 완료 콜백 결과.
                 */
                @Override
                public void onVideoCompletion(AdItem adItem, int verifyCode) {
                    super.onVideoCompletion(adItem, verifyCode);

                    LogUtil.d(TAG, "_txtAd, TNK onVideoCompletion, verifyCode : " + verifyCode);

                    if (verifyCode >= VIDEO_VERIFY_SUCCESS_SELF) {
                        // 적립 성공

                        //initAd(Constants.enum_ad.none);
                        if (_onAdCompleteListener != null)
                            _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.tnk_reward);

                    } else {
                        // 적립 실패
                    }
                }

            });

            _interstitialAdRewardTnk.load();


            _handler = new Handler();
            _runnable_tnk_reward = new Runnable() {
                @Override
                public void run() {
                    if (_interstitialAdRewardTnk == null || _interstitialAdRewardTnk.isLoaded() == false) {
                        LogUtil.d(TAG, "_txtAd, TNK Reward, postDelayed, onAdLoadFailed");
                        readyNext();
                    }
                    _interstitialAdRewardTnk = null;
                    _runnable_tnk_reward = null;
                }
            };
            _handler.postDelayed(_runnable_tnk_reward, MAX_WAIT_AD_LOADING_TIME);


        } catch (Exception e) {
            e.printStackTrace();


            LogUtil.d(TAG, "_txtAd, TNK Reward, Exception : " + e.toString());

            _interstitialAdRewardTnk = null;
            removeRunnable_Tnk_Reward();
            readyNext();


        }
    }


//    private void createCaulyAd() {
//
//        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.cauly_full) == false || !SPUtil.getInstance().getUseCaulyYn(getContext()).equals("Y")) {
//            readyNext();
//            return;
//        }
//
//        LogUtil.d(TAG, "_txtAd, createCaulyAd");
//
//        try {
//
//            // CaulyAdInfo 생성
//            CaulyAdInfo adInfo = new CaulyAdInfoBuilder(Constants.KEY_CAULY_AD_ID)
//                    // statusbarHide(boolean) : 상태바 가리기 옵션(true : 상태바 가리기)
//                    .build();
//
//            LogUtil.d(TAG, "_txtAd, Cauly, init");
//
//            _interstialCaulyAd = new CaulyInterstitialAd();
//            _interstialCaulyAd.setAdInfo(adInfo);
//            _interstialCaulyAd.setInterstialAdListener(new CaulyInterstitialAdListener() {
//                @Override
//                public void onReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, boolean b) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onReceiveInterstitialAd, b : " + b + ", _interstialCaulyAd : " + (_interstialCaulyAd == null ? "null" : "ok"));
//
////                    if (!b) {
////                        _interstialCaulyAd.setInterstialAdListener(null);
////                        _interstialCaulyAd = null;
////                    } else {
//
//                    _ad_queue_ready_complete.offer(Constants.enum_ad.cauly_full);
//
//                    if (_runnable_interstitialCauly == null) {
//                        LogUtil.d(TAG, "_txtAd, interstialCaulyAd, already postDelayed, onAdLoadFailed");
//                        return;
//                    }
//
//
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onReceiveInterstitialAd, b : " + b);
//
//
//                    if (_onAdCompleteListener != null)
//                        _onAdCompleteListener.onAdLoadSuccess();
//
//
//                    removeRunnable_Cauly();
////                    }
//                }
//
//                @Override
//                public void onFailedToReceiveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd, int i, String s) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onFailedToReceiveInterstitialAd, i : " + i + ", s : " + s);
//
//                    if (_interstialCaulyAd != null)
//                        _interstialCaulyAd.setInterstialAdListener(null);
//
//
//                    removeRunnable_Cauly();
//                    readyNext();
//                    _interstialCaulyAd = null;
//                }
//
//                @Override
//                public void onClosedInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onClosedInterstitialAd");
//
//                    if (_interstialCaulyAd != null)
//                        _interstialCaulyAd.setInterstialAdListener(null);
//                    _interstialCaulyAd = null;
//
//
//                    if (_onAdCompleteListener != null)
//                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.cauly_full);
//                }
//
//                @Override
//                public void onLeaveInterstitialAd(CaulyInterstitialAd caulyInterstitialAd) {
//                    LogUtil.d(TAG, "_txtAd, interstialCaulyAd, onLeaveInterstitialAd");
//
//                    if (_interstialCaulyAd != null) {
//                        _interstialCaulyAd.setInterstialAdListener(null);
//                        _interstialCaulyAd.cancel();
//                    }
//
//                    _interstialCaulyAd = null;
//
//                    if (_onAdCompleteListener != null)
//                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.cauly_full);
//                }
//            });
//
//            // 전면광고 노출 후 back 버튼 사용을 막기 원할 경우 disableBackKey();을 추가한다
//            // 단, requestInterstitialAd 위에서 추가되어야 합니다.
//            // interstitialAd.disableBackKey();
//
//            // 광고 요청. 광고 노출은 CaulyInterstitialAdListener의 onReceiveInterstitialAd에서 처리한다.
//            if (_interstialCaulyAd != null && getActivity() != null) {
//                _interstialCaulyAd.requestInterstitialAd(getActivity());
//            }
//
//
//            _handler = new Handler();
//            _runnable_interstitialCauly = new Runnable() {
//                @Override
//                public void run() {
//                    if (_interstialCaulyAd == null || _interstialCaulyAd.canShow() == false) {
//                        LogUtil.d(TAG, "_txtAd, interstialCaulyAd, postDelayed, onAdLoadFailed");
//                        readyNext();
//                    }
//                    _interstialCaulyAd = null;
//                }
//            };
//            _handler.postDelayed(_runnable_interstitialCauly, MAX_WAIT_AD_LOADING_TIME);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            LogUtil.d(TAG, "_txtAd, interstialCaulyAd, Exception : " + e.toString());
//
//            _interstialCaulyAd = null;
//            removeRunnable_Cauly();
//            readyNext();
//
//
//        }
//    }
//
//
//    private void createWadFull() {
//
//        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.wad_full) == false || !SPUtil.getInstance().getUseWadYn(getContext()).equals("Y")) {
//            readyNext();
//            return;
//        }
//
//        LogUtil.d(TAG, "_txtAd, createWadFull");
//
//        try {
//
//            if (_interstitialWad != null) {
//                _interstitialWad.destroy();
//            }
//
//            _interstitialWad = new WInterstitial(getActivity(), Constants.KEY_WAD_APP_ID, Constants.KEY_WAD_APP_UNIT_ID);
//            _interstitialWad.setTesting(false);
//            _interstitialWad.setInterstitialAdListener(null);
//            _interstitialWad.setInterstitialAdListener(new WInterstitial.InterstitialAdListener() {
//                @Override
//                public void onInterstitialLoaded(WInterstitial interstitial) {
//                    //interstitial.show();
//                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialLoaded");
//
//                    _ad_queue_ready_complete.offer(Constants.enum_ad.wad_full);
//
//                    if (_runnable_interstitialWad == null) {
//                        LogUtil.d(TAG, "_txtAd, Wad, already postDelayed, onAdLoadFailed[1]");
//                        return;
//                    }
//
//                    if (_onAdCompleteListener != null)
//                        _onAdCompleteListener.onAdLoadSuccess();
//
//                    removeRunnable_WAD();
//                }
//
//                @Override
//                public void onInterstitialFailed(WInterstitial interstitial, WErrorCode errorCode) {
//                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialFailed, errorCode : " + errorCode.toString());
//                    if (_interstitialWad != null) {
//                        _interstitialWad.destroy();
//                    }
//
//                    if (_runnable_interstitialWad == null) {
//                        LogUtil.d(TAG, "_txtAd, Wad, already postDelayed, onAdLoadFailed[2]");
//                        return;
//                    }
//
//                    removeRunnable_WAD();
//                    readyNext();
//                    _interstitialWad = null;
//                }
//
//                @Override
//                public void onInterstitialShown(WInterstitial interstitial) {
//                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialShown");
//                }
//
//                @Override
//                public void onInterstitialClicked(WInterstitial interstitial) {
//                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialClicked");
//
//                    if (_interstitialWad != null) {
//                        _interstitialWad.destroy();
//                    }
//                    _interstitialWad = null;
//
//                    //initAd(Constants.enum_ad.none);
//                    //callApi_vote_put_ticket_ad_point(Constants.enum_ad.wad_full.name());
//                    if (_onAdCompleteListener != null)
//                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.wad_full);
//                }
//
//                @Override
//                public void onInterstitialDismissed(WInterstitial interstitial) {
//                    LogUtil.d(TAG, "_txtAd, Wad onInterstitialDismissed");
//
//                    if (_interstitialWad != null) {
//                        _interstitialWad.destroy();
//                    }
//                    _interstitialWad = null;
//
//                    //initAd(Constants.enum_ad.none);
//                    if (_onAdCompleteListener != null)
//                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.wad_full);
//                }
//            });
//            _interstitialWad.load();
//
//
//            _handler = new Handler();
//            _runnable_interstitialWad = new Runnable() {
//                @Override
//                public void run() {
//                    if (_interstitialWad == null || _interstitialWad.isReady() == false) {
//                        LogUtil.d(TAG, "_txtAd, Wad, postDelayed, onAdLoadFailed");
//                        if (_interstitialWad != null) {
//                            _interstitialWad.destroy();
//                        }
//                        readyNext();
//                        _interstitialWad = null;
//                        _runnable_interstitialWad = null;
//                    }
//                }
//            };
//            _handler.postDelayed(_runnable_interstitialWad, MAX_WAIT_AD_LOADING_TIME);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            LogUtil.d(TAG, "_txtAd, Wad onInterstitialFailed, Exception");
//
//            if (_interstitialWad != null) _interstitialWad.destroy();
//            removeRunnable_WAD();
//            readyNext();
//            _interstitialWad = null;
//            _runnable_interstitialWad = null;
//
//
//        }
//
//    }

    private void removeRunnable_G_Full() {
        if (_handler != null && _runnable_g_full != null) {
            _handler.removeCallbacks(_runnable_g_full);
            _runnable_g_full = null;
            _handler = null;
        }
    }

    private void removeRunnable_G_Reward() {
        if (_handler != null && _runnable_g_reward != null) {
            _handler.removeCallbacks(_runnable_g_reward);
            _runnable_g_reward = null;
            _handler = null;
        }
    }

    private void removeRunnable_G_Reward_Full() {
        if (_handler != null && _runnable_g_reward_full != null) {
            _handler.removeCallbacks(_runnable_g_reward_full);
            _runnable_g_reward_full = null;
            _handler = null;
        }
    }

    private void removeRunnable_Tnk_Full() {
        if (_handler != null && _runnable_tnk_full != null) {
            _handler.removeCallbacks(_runnable_tnk_full);
            _runnable_tnk_full = null;
            _handler = null;
        }
    }

    private void removeRunnable_Tnk_Reward() {
        if (_handler != null && _runnable_tnk_reward != null) {
            _handler.removeCallbacks(_runnable_tnk_reward);
            _runnable_tnk_reward = null;
            _handler = null;
        }
    }

    private void removeRunnable_AdPopcorn() {
        if (_handler != null && _runnable_interstitialAdPopCorn != null) {
            _handler.removeCallbacks(_runnable_interstitialAdPopCorn);
            _runnable_interstitialAdPopCorn = null;
            _handler = null;
        }
    }

    private void removeRunnable_Cauly() {
        if (_handler != null && _runnable_interstitialCauly != null) {
            _handler.removeCallbacks(_runnable_interstitialCauly);
            _runnable_interstitialCauly = null;
            _handler = null;
        }
    }

    private void removeRunnable_WAD() {
        if (_handler != null && _runnable_interstitialWad != null) {
            _handler.removeCallbacks(_runnable_interstitialWad);
            _runnable_interstitialWad = null;
            _handler = null;
        }
    }

    private void removeRunnable_MAX_FULL() {
        if (_handler != null && _runnable_interstitialMax != null) {
            _handler.removeCallbacks(_runnable_interstitialMax);
            _runnable_interstitialMax = null;
            _handler = null;
        }
    }

    private void removeRunnable_UNITY() {

        LogUtil.d("removeRunnable_UNITY, _handler : " + (_handler == null ? "null" : "ok"));
        LogUtil.d("removeRunnable_UNITY, _runnable_interstitialUnity : " + (_runnable_interstitialUnity == null ? "null" : "ok"));

        if (_handler != null && _runnable_interstitialUnity != null) {
            _handler.removeCallbacks(_runnable_interstitialUnity);
            _runnable_interstitialUnity = null;
            _handler = null;
        }
    }


//    private void createUnityAds() {
//        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.unityAds_full) == false || !SPUtil.getInstance().getUseUnityAdsYn(getContext()).equals("Y")) {
//            readyNext();
//            return;
//        }
//
//        try {
//
//            UnityAds.load("video", new IUnityAdsLoadListener() {
//                @Override
//                public void onUnityAdsAdLoaded(String placementId) {
//                    LogUtil.d(TAG, "_txtAd, onUnityAdsAdLoaded, Ad for " + placementId + " loaded");
//
//
//                    if (_runnable_interstitialUnity == null) {
//                        LogUtil.d(TAG, "_txtAd, onUnityAdsAdLoaded, already postDelayed, onAdLoadFailed[1]");
//                        return;
//                    }
//
//                    _ad_queue_ready_complete.offer(Constants.enum_ad.unityAds_full);
//
//                    removeRunnable_UNITY();
//
//                    if (_onAdCompleteListener != null)
//                        _onAdCompleteListener.onAdLoadSuccess();
//                }
//
//                @Override
//                public void onUnityAdsFailedToLoad(String placementId, UnityAds.UnityAdsLoadError error, String message) {
//                    LogUtil.d(TAG, "_txtAd, onUnityAdsFailedToLoad, Ad for " + placementId + " failed to load: [" + error + "] " + message);
//
//
//                    if (_runnable_interstitialUnity == null) {
//                        LogUtil.d(TAG, "_txtAd, onUnityAdsAdLoaded, already postDelayed, onAdLoadFailed[2]");
//                        return;
//                    }
//
//                    removeRunnable_UNITY();
//                    readyNext();
//
//                }
//            });
//
//            _handler = new Handler();
//            _runnable_interstitialUnity = new Runnable() {
//                @Override
//                public void run() {
//                    LogUtil.d(TAG, "_txtAd, onUnityAdsFailedToLoad, postDelayed, onAdLoadFailed");
//
//                    readyNext();
//                    _runnable_interstitialUnity = null;
//                }
//            };
//            _handler.postDelayed(_runnable_interstitialUnity, MAX_WAIT_AD_LOADING_TIME);
//
//        } catch (Exception e) {
//            e.printStackTrace();
//
//            removeRunnable_UNITY();
//            readyNext();
//        }
//    }


//    private void showUnityAdsFull() {
//
//        UnityAds.show(getActivity(), "video", new UnityAdsShowOptions(), new IUnityAdsShowListener() {
//            @Override
//            public void onUnityAdsShowFailure(String placementId, UnityAds.UnityAdsShowError error, String message) {
//                LogUtil.d(TAG, "_txtAd, onUnityAdsShowFailure: " + error + " - " + message);
//            }
//
//            @Override
//            public void onUnityAdsShowStart(String placementId) {
//                LogUtil.d(TAG, "_txtAd, onUnityAdsShowStart: " + placementId);
//            }
//
//            @Override
//            public void onUnityAdsShowClick(String placementId) {
//                LogUtil.d(TAG, "_txtAd, onUnityAdsShowClick: " + placementId);
//            }
//
//            @Override
//            public void onUnityAdsShowComplete(String placementId, UnityAds.UnityAdsShowCompletionState state) {
//                LogUtil.d(TAG, "_txtAd, onUnityAdsShowComplete: " + placementId);
//                //initAd(Constants.enum_ad.none);
//                //callApi_vote_put_ticket_ad_point(Constants.enum_ad.unityAds_full.name());
//
//                if (_onAdCompleteListener != null)
//                    _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.unityAds_full);
//            }
//        });
//
//        _ad_queue_ready_complete.remove(Constants.enum_ad.unityAds_full);
//    }


    private void createMaxFull() {

        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.max_full, AdFullOrgManager._adLoadingTimeInterval * 4) == false || !SPUtil.getInstance().getUseMaxYn(getContext()).equals("Y")) {
            readyNext();
            return;
        }

        LogUtil.d(TAG, "_txtAd, createMaxFull");

        try {
            if (_maxInterstitialAd == null || _maxInterstitialAd.isReady() == false) {


                _maxInterstitialAd = new MaxInterstitialAd(Constants.KEY_MAX_FULL_ID, getActivity());

                _maxInterstitialAd.setListener(new MaxAdListener() {
                    @Override
                    public void onAdLoaded(MaxAd ad) {
                        LogUtil.d(TAG, "_maxInterstitialAd, onAdLoaded, ad : " + ad.toString());

                        _ad_queue_ready_complete.offer(Constants.enum_ad.max_full);

                        if (_runnable_interstitialMax == null) {
                            LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, already postDelayed, onAdLoadFailed[1]");
                            return;
                        }

                        if (_onAdCompleteListener != null)
                            _onAdCompleteListener.onAdLoadSuccess();

                        removeRunnable_MAX_FULL();
                    }

                    @Override
                    public void onAdDisplayed(MaxAd ad) {
                        LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, onAdDisplayed, ad : " + ad.toString());
                    }

                    @Override
                    public void onAdHidden(MaxAd ad) {
                        LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, onAdHidden, ad : " + ad.toString());
                        if (_maxInterstitialAd != null) {
                            _maxInterstitialAd.destroy();
                        }
                        _maxInterstitialAd = null;

                        if (_onAdCompleteListener != null)
                            _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.max_full);
                    }

                    @Override
                    public void onAdClicked(MaxAd ad) {
                        LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, onAdClicked, ad : " + ad.toString());
                    }

                    @Override
                    public void onAdLoadFailed(String adUnitId, MaxError error) {
                        LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, onAdLoadFailed, adUnitId : " + adUnitId + ", error : " + error.toString());

                        if (_runnable_interstitialMax == null) {
                            LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, already postDelayed, onAdLoadFailed[2]");
                            return;
                        }


                        removeRunnable_MAX_FULL();
                        readyNext();
                        _maxInterstitialAd = null;
                    }

                    @Override
                    public void onAdDisplayFailed(MaxAd ad, MaxError error) {
                        LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, onAdDisplayFailed, ad : " + ad.toString() + ", error : " + error.toString());

                        //_maxInterstitialAd = null;

                        //readyNext();
                    }
                });

                // Load the first ad
                _maxInterstitialAd.loadAd();


                _handler = new Handler();
                _runnable_interstitialMax = new Runnable() {
                    @Override
                    public void run() {
                        if (_maxInterstitialAd == null || _maxInterstitialAd.isReady() == false) {
                            LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, postDelayed, onAdLoadFailed");
                            if (_maxInterstitialAd != null) {
                                _maxInterstitialAd.destroy();
                            }
                            readyNext();
                        }
                        _maxInterstitialAd = null;
                        _runnable_interstitialMax = null;
                    }
                };
                _handler.postDelayed(_runnable_interstitialMax, MAX_WAIT_AD_LOADING_TIME);
            }

        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.d(TAG, "_txtAd, _maxInterstitialAd, e : " + e.toString());
            if (_maxInterstitialAd != null) _maxInterstitialAd.destroy();

            _maxInterstitialAd = null;
            removeRunnable_MAX_FULL();
            readyNext();
        }
    }


    public void createAdPopcornFull() {
        if (AdFullOrgManager.canAdInitAvailable(Constants.enum_ad.adpc_full, AdFullOrgManager._adLoadingTimeInterval * 6) == false) {
            readyNext();
            return;
        }


        LogUtil.d(TAG, "_txtAd, createAdPopcornFull");

        AdPopcornSSPUserProperties userProperties = new AdPopcornSSPUserProperties.Builder()
                .gender(2)
                .language("KR")
                .country("KO")
                .build();
        AdPopcornSSP.setUserProperties(userProperties);
        AdPopcornSSP.setUserId(getContext(), SPUtil.getInstance().getUserNoEnc(getContext()));

        try {
            _adPopcornSSPInterstitialAd = new AdPopcornSSPInterstitialAd(getContext());
            _adPopcornSSPInterstitialAd.setPlacementId(Constants.KEY_ADPC_FULL_ID);
            _adPopcornSSPInterstitialAd.setCurrentActivity(getActivity());
            _adPopcornSSPInterstitialAd.setInterstitialLoadEventCallbackListener(new IInterstitialLoadEventCallbackListener() {

                @Override
                public void OnInterstitialLoaded() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialLoaded");
                    _ad_queue_ready_complete.offer(Constants.enum_ad.adpc_full);

                    if (_runnable_interstitialAdPopCorn == null) {
                        LogUtil.d(TAG, "_txtAd, createAdPopcornFull, already postDelayed, onAdLoadFailed[1]");
                        return;
                    }

                    removeRunnable_AdPopcorn();

                    if (_onAdCompleteListener != null)
                        _onAdCompleteListener.onAdLoadSuccess();
                }


                @Override
                public void OnInterstitialReceiveFailed(SSPErrorCode errorCode) {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialReceiveFailed, errorCode : " + errorCode.getErrorMessage());

                    if (_runnable_interstitialAdPopCorn == null) {
                        LogUtil.d(TAG, "_txtAd, createAdPopcornFull, already postDelayed, onAdLoadFailed[2]");
                        return;
                    }

                    removeRunnable_AdPopcorn();
                    readyNext();
                    _adPopcornSSPInterstitialAd = null;

                }
            });
            _adPopcornSSPInterstitialAd.setInterstitialShowEventCallbackListener(new IInterstitialShowEventCallbackListener() {

                @Override
                public void OnInterstitialOpened() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialOpened");
                }

                @Override
                public void OnInterstitialOpenFailed(SSPErrorCode errorCode) {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialOpenFailed, errorCode : " + errorCode.getErrorMessage());


                }

                @Override
                public void OnInterstitialClosed(int CloseEvent) {
                    // UNKNOWN = 0;
                    // CLICK_CLOSE_BTN = 1;
                    // PRESSED_BACK_KEY = 2;
                    // SWIPE_RIGHT_TO_LEFT = 3;
                    // SWIPE_LEFT_TO_RIGHT = 4;
                    // AUTO_CLOSE = 5;
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialClosed, CloseEvent : " + CloseEvent);

                    if (_adPopcornSSPInterstitialAd != null) {
                        _adPopcornSSPInterstitialAd.destroy();
                    }
                    _adPopcornSSPInterstitialAd = null;

                    if (_onAdCompleteListener != null)
                        _onAdCompleteListener.onAdPlayComplete(Constants.enum_ad.adpc_full);
                }

                @Override
                public void OnInterstitialClicked() {
                    LogUtil.d(TAG, "_txtAd, createAdPopcornFull OnInterstitialClicked");
                }
            });
            _adPopcornSSPInterstitialAd.loadAd();


            _handler = new Handler();
            _runnable_interstitialAdPopCorn = new Runnable() {
                @Override
                public void run() {
                    if (_adPopcornSSPInterstitialAd == null || _adPopcornSSPInterstitialAd.isLoaded() == false) {
                        LogUtil.d(TAG, "_txtAd, createAdPopcornFull, postDelayed, onAdLoadFailed");
                        if (_adPopcornSSPInterstitialAd != null) {
                            _adPopcornSSPInterstitialAd.destroy();
                        }
                        readyNext();
                        _adPopcornSSPInterstitialAd = null;
                    }
                }
            };
            _handler.postDelayed(_runnable_interstitialAdPopCorn, MAX_WAIT_AD_LOADING_TIME);
        } catch (Exception e) {
            e.printStackTrace();

            LogUtil.d(TAG, "_txtAd, createAdPopcornFull Failed, Exception");

            if (_adPopcornSSPInterstitialAd != null) _adPopcornSSPInterstitialAd.destroy();

            removeRunnable_AdPopcorn();
            readyNext();
            _adPopcornSSPInterstitialAd = null;

        }
    }

}
