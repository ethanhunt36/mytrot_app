package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.atv.star.AtvStarInfo;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.view.GListView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import retrofit2.Call;

public class BoardViewUtil {

    private Context _context = null;


    private boolean _isInProfile = false;


    private ImageView _imgHeart = null;
    private TextView _txtLike = null;
    private TextView _txtHeartCount = null;
    private View _baseNoticeHeart = null;

    private String _artist_board_id = "";

    public void setIsInProfile(boolean isInProfile) {
        _isInProfile = isInProfile;
    }

    public View setViewEvent(Context context, final JSONObject json, View convertView, final GListView.GListAdapter adapter, boolean isShowDetail) {
        return setViewEvent(context, json, convertView, adapter, isShowDetail, "");
    }

    public View setViewEvent(Context context, final JSONObject json, View convertView, final GListView.GListAdapter adapter, boolean isShowDetail, String artist_board_id) {

        _context = context;
        _artist_board_id = artist_board_id;

        final JSONObject member_info = JSONUtil.getJSONObject(json, "member_info");

        ImageView img01 = convertView.findViewById(R.id.img01);
        ImageView img02 = convertView.findViewById(R.id.img02);
        ImageView img03 = convertView.findViewById(R.id.img03);


        _imgHeart = (ImageView) convertView.findViewById(R.id.imgHeart);
        _txtLike = (TextView) convertView.findViewById(R.id.txtLike);
        TextView txtNick = (TextView) convertView.findViewById(R.id.txtNick);
        TextView txtDate = (TextView) convertView.findViewById(R.id.txtDate);

        TextView txtRead = (TextView) convertView.findViewById(R.id.txtRead);
        TextView txtContent = (TextView) convertView.findViewById(R.id.txtContent);

        View baseDetail = convertView.findViewById(R.id.baseDetail);

        int category = JSONUtil.getInteger(json, "category");
        String url_01 = JSONUtil.getStringUrl(json, "img01");
        String url_02 = JSONUtil.getStringUrl(json, "img02");
        String url_03 = JSONUtil.getStringUrl(json, "img03");


        LogUtil.d("url_01 : " + url_01);
        String cont = JSONUtil.getString(json, "cont");
        String reg_dttm = JSONUtil.getString(json, "reg_dttm");

        if (!FormatUtil.isNullorEmpty(reg_dttm)) reg_dttm = reg_dttm.substring(0, 16);

        int is_ilike = JSONUtil.getInteger(json, "is_ilike");

        String nick = "";
        String pic1 = "";
        String pic2 = "";
        String pic3 = "";
        String pic4 = "";
        String member_no = "";
        final int board_no = JSONUtil.getInteger(json, "no");

        LogUtil.d("board_no : " + board_no);

        if (member_info != null) {
            if(SPUtil.getInstance().getAdFullScreen(_context).equals("Y")) {
                nick = JSONUtil.getString(member_info, "nick") + " Lv." + JSONUtil.getString(member_info, "lv");
            } else {
                nick = JSONUtil.getString(member_info, "nick");
            }
            member_no = JSONUtil.getString(member_info, "member_no");
            pic1 = JSONUtil.getStringUrl(member_info, "pic1");
            pic2 = JSONUtil.getStringUrl(member_info, "pic2");
            pic3 = JSONUtil.getStringUrl(member_info, "pic3");
            pic4 = JSONUtil.getStringUrl(member_info, "pic4");
        } else {
            if(SPUtil.getInstance().getAdFullScreen(_context).equals("Y")) {
                nick = JSONUtil.getString(json, "member_nick") + " Lv." + JSONUtil.getString(json, "lv");
            } else {
                nick = JSONUtil.getString(json, "member_nick");
            }
            member_no = JSONUtil.getString(json, "member_no");
            pic1 = JSONUtil.getStringUrl(json, "member_pic1");
            pic2 = JSONUtil.getStringUrl(json, "member_pic2");
            pic3 = JSONUtil.getStringUrl(json, "member_pic3");
            pic4 = JSONUtil.getStringUrl(json, "member_pic4");
        }

        final String member_no_final = member_no;
        _imgHeart.setImageResource(is_ilike > 0 ? R.drawable.bt_heart_on : R.drawable.bt_heart_off);

        txtNick.setText(nick);
        txtDate.setText(reg_dttm);

        img01.setBackground(context.getDrawable(R.drawable._s_background_rounding));
        img01.setClipToOutline(true);

        if (!FormatUtil.isNullorEmpty(url_01)) {
            Picasso.with(context)
                    .load(url_01)
                    //.fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(img01);
            img01.setVisibility(View.VISIBLE);
        } else {
            img01.setVisibility(View.GONE);
        }

        if (!FormatUtil.isNullorEmpty(url_02)) {
            Picasso.with(context)
                    .load(url_02)
                    //.fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(img02);
            img02.setVisibility(View.VISIBLE);
        } else {
            img02.setVisibility(View.GONE);
        }

        if (!FormatUtil.isNullorEmpty(url_03)) {
            Picasso.with(context)
                    .load(url_03)
                    //.fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(img03);
            img03.setVisibility(View.VISIBLE);
        } else {
            img03.setVisibility(View.GONE);
        }

        cont = FormatUtil.replaceTag(cont);
        txtContent.setLinksClickable(true);
        txtContent.setMovementMethod(LinkMovementMethod.getInstance());
        txtContent.setText(Html.fromHtml(cont));

        //txtLike.setText(String.format("%d명이 좋아요 했습니다.", JSONUtil.getInteger(json, "like_cnt")));
        _txtLike.setText(String.format("%d명 좋아요", JSONUtil.getInteger(json, "like_cnt", 0)));


        if (category == 1 || category == 3) {
            txtRead.setText(String.format("조회 %s"
                    , FormatUtil.toPriceFormat(JSONUtil.getString(json, "read_cnt", "0"))));
        } else {
            txtRead.setText(String.format("저장 %s | 조회 %s"
                    , FormatUtil.toPriceFormat(JSONUtil.getString(json, "download_cnt", "0"))
                    , FormatUtil.toPriceFormat(JSONUtil.getString(json, "read_cnt", "0"))));
        }

        if (isShowDetail == false) {
            baseDetail.setVisibility(View.GONE);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    i.setClass(_context, AtvStarInfo.class);
                    i.putExtra(Constants.EXTRAS_JSON_STRING, json.toString());

                    _context.startActivity(i);
                }
            });
        }


        _txtLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLike(json, adapter);
            }
        });
        _imgHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLike(json, adapter);
            }
        });

        return convertView;
    }


    private void onLike(JSONObject json, final GListView.GListAdapter adapter) {
        int board_no = JSONUtil.getInteger(json, "no");
        int category = JSONUtil.getInteger(json, "category");
        String member_no = JSONUtil.getString(json, "member_no");
        String member_no_enc = SPUtil.getInstance().getUserNoEnc(_context);

        LogUtil.d("member_no : " + member_no);
        LogUtil.d("member_no_enc : " + member_no_enc);

        if ((category == 2 || category == 3) && member_no.equals(member_no_enc)) {
            new Alert().showAlert(_context, "본인 작성한 글은 좋아요 할 수 없습니다.");
            return;
        }

        callApi_board_inc_like(json, board_no, category, adapter);
    }

    private void callApi_board_inc_like(final JSONObject jsonBoard, int board_no, int category, final GListView.GListAdapter adapter) {

        LogUtil.e("==========callApi_board_inc_like : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_inc_like(
                SPUtil.getInstance().getUserNoEnc(_context),
                board_no
        );

        if (category == 3) {
            call = apiInterface.boardPromo_inc_like(
                    SPUtil.getInstance().getUserNoEnc(_context),
                    board_no
            );
        }

        if (!FormatUtil.isNullorEmpty(_artist_board_id)) {
            call = apiInterface.boardStar_inc_like(
                    SPUtil.getInstance().getUserNoEnc(_context),
                    board_no,
                    _artist_board_id
            );
        }

        final Dialog dialog = ProgressDialogUtil.show(_context, null);


        call.enqueue(new CustomJsonHttpResponseHandler(_context, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                try {

                    ProgressDialogUtil.dismiss(dialog);

                    LogUtil.d("resultCode : " + resultCode);
                    LogUtil.d("resultMessage : " + resultMessage);

                    LogUtil.json(json);

                    if (resultCode != 0) {
                        new Alert().showAlert(_context, resultMessage);
                        return;
                    }

                    String inc_dec = JSONUtil.getString(json, "data");


                    int is_ilike = JSONUtil.getInteger(jsonBoard, "is_ilike");
                    int like_cnt = JSONUtil.getInteger(jsonBoard, "like_cnt");

                    is_ilike = inc_dec.equals("inc") ? 1 : 0;
                    like_cnt = inc_dec.equals("inc") ? like_cnt + 1 : like_cnt - 1;

                    JSONUtil.puts(jsonBoard, "is_ilike", is_ilike);
                    JSONUtil.puts(jsonBoard, "my_like_cnt", is_ilike);

                    JSONUtil.puts(jsonBoard, "like_cnt", like_cnt);

                    LogUtil.d("inc_dec : " + inc_dec);
                    LogUtil.d("is_ilike : " + is_ilike);
                    LogUtil.d("like_cnt : " + like_cnt);

                    _imgHeart.setImageResource(is_ilike > 0 ? R.drawable.bt_heart_on : R.drawable.bt_heart_off);
                    _txtLike.setText(String.format("%d명 좋아요", like_cnt));

                    if (adapter != null) {
                        adapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_profile_check_open_profile(final JSONObject jsonUser) {

        LogUtil.e("==========callApi_profile_check_open_profile : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        String target_member_no = JSONUtil.getString(jsonUser, "member_no");

        String pause_yn = JSONUtil.getString(jsonUser, "pause_yn");

        if ("Y".equals(pause_yn)) {
            Alert alert = new Alert();
            alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                @Override
                public void onClose(DialogInterface dialog, int which) {

                }
            });
            alert.showAlert(_context, "이용정지된 회원입니다.");
            return;
        }


        String my_member_no = SPUtil.getInstance().getUserNoEnc(_context);


    }

    Handler _handler = null;
    Runnable _runnable = new Runnable() {
        @Override
        public void run() {
            _baseNoticeHeart.setVisibility(View.GONE);
        }
    };


}
