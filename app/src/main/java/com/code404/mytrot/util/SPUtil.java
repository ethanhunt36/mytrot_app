package com.code404.mytrot.util;

//import org.apache.commons.httpclient.Cookie;

import android.annotation.SuppressLint;
import android.content.Context;

import com.code404.mytrot.common.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;


public class SPUtil extends SPUtilBase {

    private static SPUtil helper = new SPUtil();
    //private Cookie[] cookies = null;

    public static final String NAME_SYS = "spu.pn.sys";
    public static final String NAME_USER = "spu.pn.user";

    private static final String SPU_K_IS_LOGIN = "SPU_K_IS_LOGIN";
    private static final String SPU_K_IS_LOGIN_AUTO = "SPU_K_IS_LOGIN_AUTO";
    private static final String SPU_K_IS_SOUND = "SPU_K_IS_SOUND";
    private static final String SPU_K_IS_VIBRATOR = "SPU_K_IS_VIBRATOR";
    private static final String SPU_K_IS_PUSH = "SPU_K_IS_PUSH";
    private static final String SPU_K_IS_POP = "SPU_K_IS_POP";
    private static final String SPU_K_IS_LOCATION = "SPU_K_IS_LOCATION";

    private static final String SPU_K_DARK_MODE = "SPU_K_DARK_MODE";

    private static final String SPU_K_PUSH_TYPE = "SPU_K_PUSH_TYPE";
    private static final String SPU_K_COOKIE = "SPU_K_COOKIE";
    private static final String SPU_K_REGISTKEY = "SPU_K_REGISTKEY";
    private static final String SPU_K_REGISTKEY_DAYTE = "SPU_K_REGISTKEY_DAYTE";
    private static final String SPU_K_USER_INFO = "SPU_K_USER_INFO";
    private static final String SPU_K_LOGIN_TYPE = "SPU_K_LOGIN_TYPE";

    private static final String SPU_K_AUTH_TOKEN = "SPU_K_AUTH_TOKEN";

    private static final String SPU_K_USER_Address = "SPU_K_USER_Address";

    private static final String SPU_K_USER_Location = "SPU_K_USER_Location";

    private static final String SPU_K_Guide_VIEW = "SPU_K_Guide_VIEW";
    private static final String SPU_K_AD_URL = "SPU_K_AD_URL";

    private static final String SPU_K_USER_LoginTime = "SPU_K_USER_LoginTime";
    private static final String SPU_K_USER_LimitID = "SPU_K_USER_LimitID";

    private static final String SPU_K_USER_ID = "SPU_K_USER_ID";
    private static final String SPU_K_USER_NO_ENC = "SPU_K_USER_NO_ENC";
    private static final String SPU_K_USER_FBID = "SPU_K_USER_FBID";
    private static final String SPU_K_USER_PW = "SPU_K_USER_PW";
    private static final String SPU_K_USER_SESSION_ID = "SPU_K_USER_SESSION_ID";
    private static final String SPU_K_USER_NUMBER = "SPU_K_USER_NUMBER";

    private static final String SPU_K_MARKET_URL = "SPU_K_MARKET_URL";
    private static final String SPU_K_MARKET_URL_IOS = "SPU_K_MARKET_URL_IOS";

    private static final String SPU_K_NEW_COUNT = "SPU_K_NEW_COUNT";
    private static final String SPU_K_NEW_COUNT_ALRIM = "SPU_K_NEW_COUNT_ALRIM";

    private static final String SPU_K_SERVER_VERSION = "SPU_K_SERVER_VERSION";

    private static final String SPU_K_LOCAL_SEARCH_HISTORY = "SPU_K_LOCAL_SEARCH_HISTORY";

    private static final String SPU_K_MSG_ROOM_NO = "SPU_K_MSG_ROOM_NO";
    private static final String SPU_K_INVITE_ALERT_SHOW_COUNT = "SPU_K_INVITE_ALERT_SHOW_COUNT";

    private static final String SPU_K_DATE_EVENT_REGIST_7 = "SPU_K_DATE_EVENT_REGIST_7";

    private static final String SPU_K_LOGIN_NAVER_YN = "SPU_K_LOGIN_NAVER_YN";
    private static final String SPU_K_LOGIN_KAKAO_YN = "SPU_K_LOGIN_KAKAO_YN";
    private static final String SPU_K_MATCH_YN = "SPU_K_MATCH_YN";
    private static final String SPU_K_MATCH_ADD_YN = "SPU_K_MATCH_ADD_YN";

    private static final String SPU_K_AD_PROFILE_YN = "SPU_K_AD_PROFILE_YN";
    private static final String SPU_K_AVAILABLE_BOARD_LEVEL = "SPU_K_AVAILABEL_BOARD_LEVEL";


    private static final String SPU_K_AD_CAULY_YN = "SPU_K_AD_CAULY_YN";
    private static final String SPU_K_AD_WAD_YN = "SPU_K_AD_WAD_YN";
    private static final String SPU_K_AD_MAX_YN = "SPU_K_AD_MAX_YN";
    private static final String SPU_K_AD_MAX_REWARD_YN = "SPU_K_AD_MAX_REWARD_YN";
    private static final String SPU_K_AD_VUNGLE_YN = "SPU_K_AD_VUNGLE_YN";
    private static final String SPU_K_AD_FACEBOOK_YN = "SPU_K_AD_FACEBOOK_YN";
    private static final String SPU_K_AD_UNITYADS_YN = "SPU_K_AD_UNITYADS_YN";
    private static final String SPU_K_AD_INTRO_YN = "SPU_K_AD_INTRO_YN";


    private static final String SPU_K_AD_COUPANG_YN = "SPU_K_AD_COUPANG_YN";
    private static final String SPU_K_USE_PROMOTION_YN = "SPU_K_USE_PROMOTION_YN";

    private static final String SPU_K_AD_REMOVE_INFO = "SPU_K_AD_REMOVE_INFO";
    private static final String SPU_K_PRODUCT_AUTO_TICKET_INFO = "SPU_K_PRODUCT_AUTO_TICKET_INFO";

    private static final String SPU_K_ARTIST_LIST = "SPU_K_ARTIST_LIST_V2";

    private static final String SPU_K_VOTE_AD_INFO = "SPU_K_VOTE_AD_INFO";
    private static final String SPU_K_BOARD_PROMO_INFO = "SPU_K_BOARD_PROMO_INFO";
    private static final String SPU_K_BTN_INFO = "SPU_K_BTN_INFO";

    private static final String SPU_K_IS_HAVE_AD_REMOVE_ITEM = "SPU_K_IS_HAVE_AD_REMOVE_ITEM";
    private static final String SPU_K_IS_HAVE_BOOSTER_ITEM = "SPU_K_IS_HAVE_BOOSTER_ITEM";

    private static final String SPU_K_MAIN_TOAST_DATE = "SPU_K_MAIN_TOAST_DATE";
    private static final String SPU_K_KEYWORD_LIST = "SPU_K_KEYWORD_LIST";
    private static final String SPU_K_OPEN_ATV_VOD = "SPU_K_OPEN_ATV_VOD";
    private static final String SPU_K_OPEN_FULL_SCREEN = "SPU_K_OPEN_FULL_SCREEN";
    private static final String SPU_K_AMOUNT_TICKET_AD = "SPU_K_AMOUNT_TICKET_AD";


    private static final String SPU_K_ANDROID_ID = "SPU_K_ANDROID_ID";

    private static final String SPU_K_BOARD_NICK = "SPU_K_BOARD_NICK";
    private static final String SPU_K_USE_AD_YOUTUBE = "SPU_K_USE_AD_YOUTUBE";
    private static final String SPU_K_USE_AD_FULL_SCREEN = "SPU_K_USE_AD_FULL_SCREEN";
    private static final String SPU_K_USE_REVIEW_REWARD_YN = "SPU_K_USE_REVIEW_REWARD_YN";
    private static final String SPU_K_YOUTUBE_ALERT_DATE = "SPU_K_YOUTUBE_ALERT_DATE";

    private static final String SPU_K_YOUTUBE_JSON_ARRAY = "SPU_K_YOUTUBE_JSON_ARRAY";

    private static final String SPU_K_AD_SEQ_FULL_ARRAY = "SPU_K_AD_SEQ_FULL_ARRAY";
    private static final String SPU_K_AD_SEQ_FULL_ARRAY_V4 = "SPU_K_AD_SEQ_FULL_ARRAY_V4";
    private static final String SPU_K_AD_SEQ_REWARD_ARRAY = "SPU_K_AD_SEQ_REWARD_ARRAY";

    private static final String SPU_K_MARKETING_EX_JSON_ARRAY = "SPU_K_MARKETING_EX_JSON_ARRAY";
    private static final String SPU_K_MARKETING_ALERT_DATE = "SPU_K_MARKETING_ALERT_DATE";

    private static final String SPU_K_ATTEND_DATE = "SPU_K_ATTEND_DATE";
    
    // 동영상광고 안내 메시지 출력 날짜
    private static final String SPU_K_AD_INFO_DATE = "SPU_K_AD_INFO_DATE";
    private static final String SPU_K_ARTIST_LIST_DATE = "SPU_K_ARTIST_LIST_DATE";


    // 디비 상의 가장 최신 공지사항 번호
    private static final String SPU_K_LASTEST_NOTICE_NO = "SPU_K_LASTEST_NOTICE_NO";

    // 클라이언트가 확인한 최신 공지사항 번호
    private static final String SPU_K_LASTEST_NOTICE_NO_VIEWED = "SPU_K_LASTEST_NOTICE_NO_VIEWED";

    private static final String SPU_K_REVIEW_TYPE = "SPU_K_REVIEW_TYPE";

    // 더보기 접속한 횟수
    private static final String SPU_K_ENTER_MORE_COUNT = "SPU_K_ENTER_MORE_COUNT";

    // 홈 접속한 횟수
    private static final String SPU_K_ENTER_HOME_COUNT = "SPU_K_ENTER_HOME_COUNT";

    private static final String SPU_K_AD_POINT_AMOUNT = "SPU_K_AD_POINT_AMOUNT";
    private static final String SPU_K_AD_TICKET_AMOUNT = "SPU_K_AD_TICKET_AMOUNT";

    private static final String SPU_K_CHECKED_FAVORITE_VOTE = "SPU_K_CHECKED_FAVORITE_VOTE";
    private static final String SPU_K_CHECKED_FAVORITE_DONATE = "SPU_K_CHECKED_FAVORITE_DONATE";
    private static final String SPU_K_CHECKED_FAVORITE_POPUP_ARTIST = "SPU_K_CHECKED_FAVORITE_POPUP_ARTIST";
    private static final String SPU_K_RCMD_CODE = "SPU_K_RCMD_CODE";

    private static final String SPU_K_REPEAT_PPOBKKI_AD = "SPU_K_REPEAT_PPOBKKI_AD";
    private static final String SPU_K_REPEAT_BOARD_WRITE_AD = "SPU_K_REPEAT_BOARD_WRITE_AD";


    private static final String SPU_K_BANNER_LIST = "SPU_K_BANNER_LIST";
    private static final String SPU_K_BANNER_FULL_LIST = "SPU_K_BANNER_FULL_LIST";


    private static final String SPU_K_RANK_SEQ = "SPU_K_RANK_SEQ";
    private static final String SPU_K_RANK_GENDER = "SPU_K_RANK_GENDER";


    private static final String SPU_K_IS_AGREE_WRITE_BOARD = "SPU_K_IS_AGREE_WRITE_BOARD";
    private static final String SPU_K_BLOG_URL = "SPU_K_BLOG_URL";

    private static final String SPU_K_BOARD_ENTER_COUNT = "SPU_K_BOARD_ENTER_COUNT";

    private static final String SPU_K_CLICK_AD_VIEW = "SPU_K_CLICK_AD_VIEW";

    private static final String SPU_K_CLICK_AD_GAP = "SPU_K_CLICK_AD_GAP";
    private static final String SPU_K_AD_VIEW_LIMIT_YN = "SPU_K_AD_VIEW_LIMIT_YN";

    private static final String SPU_K_IS_VIEW_SEQ_AD = "SPU_K_IS_VIEW_SEQ_AD";
    private static final String SPU_K_COUNT_LOAD_AD = "SPU_K_COUNT_LOAD_AD";
    private static final String SPU_K_AD_LOADING_TIME_INTERVAL = "SPU_K_AD_LOADING_TIME_INTERVAL";

    private static final String SPU_K_VOD_KEY = "SPU_K_VOD_KEY";

    private static final String SPU_K_AUTO_VIEW_NEWSPICK_YN = "SPU_K_AUTO_VIEW_NEWSPICK_YN";
    private static final String SPU_K_IS_VIEW_BASECHARGE2_YN = "SPU_K_IS_VIEW_BASECHARGE2_YN";
    private static final String SPU_K_IS_VIEW_BASECHARGE3_YN = "SPU_K_IS_VIEW_BASECHARGE3_YN";
    private static final String SPU_K_IS_VIEW_BASECHARGE4_YN = "SPU_K_IS_VIEW_BASECHARGE4_YN";
    private static final String SPU_K_IS_VIEW_BASECHARGE5_YN = "SPU_K_IS_VIEW_BASECHARGE5_YN";

    private static final String SPU_K_INIT_AD_VERSION = "SPU_K_INIT_AD_VERSION";

    private static final String SPU_K_SECONDS_VIEW_ADPICK = "SPU_K_SECONDS_VIEW_ADPICK";
    private static final String SPU_K_CHARGE_MODE = "SPU_K_CHARGE_MODE_V2";


    public static SPUtil getInstance() {
        if (helper == null) {
            helper = new SPUtil();
        }
        return helper;
    }

//        public void setCookie(Context context, Cookie[] c)
//        {
//                String strCookie = "";
//                LogUtil.d(TAG, "COOKE SAVE============================================================================");
//                this.cookies = c;
//
//                for (int i = 0; i < c.length; i++) {
//                        JSONObject job = new JSONObject();
//
//                        try {
//                                job.put("cookieDomain", c[i].getDomain());
//                                job.put("cookieName", c[i].getName());
//                                job.put("cookiePath", c[i].getPath());
//                                job.put("cookieValue", c[i].getValue());
//
//                                LogUtil.d(TAG, "i : " + i);
//                                LogUtil.d(TAG, "job.toString() : " + job.toString());
//
//                                strCookie = FormatUtil.isNullorEmpty(strCookie) ? job.toString() : strCookie + "///" + job.toString();
//
//                        } catch (JSONException e) {
//                                e.printStackTrace();
//                        }
//
//                }
//                writeString(context, NAME_SYS, SPU_K_COOKIE, strCookie);
//
//                LogUtil.d(TAG, "COOKE SAVE============================================================================");
//        }
//
//        public Cookie[] getCookie(Context context)
//        {
//                LogUtil.d(TAG, "COOKE READ============================================================================");
//                if (this.cookies == null) {
//                        String strCookie = readString(context, NAME_SYS, SPU_K_COOKIE, "");
//                        if (FormatUtil.isNullorEmpty(strCookie)) {
//                                LogUtil.d(TAG, "COOKE READ 없음=======================================================================");
//                                return null;
//                        }
//
//                        String strCookies[] = strCookie.split("///");
//
//                        Cookie[] resultCookie = new Cookie[strCookies.length];
//
//                        for (int i = 0; i < strCookies.length; i++) {
//                                JSONObject job = JSONUtil.createObject(strCookies[i]);
//                                LogUtil.d(TAG, "i : " + i);
//                                LogUtil.d(TAG, "job.toString() : " + job.toString());
//
//                                Cookie cookieTemp = new Cookie();
//                                cookieTemp.setDomain(JSONUtil.getString(job, "cookieDomain"));
//                                cookieTemp.setName(JSONUtil.getString(job, "cookieName"));
//                                cookieTemp.setPath(JSONUtil.getString(job, "cookiePath"));
//                                cookieTemp.setValue(JSONUtil.getString(job, "cookieValue"));
//
//                                resultCookie[i] = cookieTemp;
//                        }
//                        LogUtil.d(TAG, "COOKE READ FILE ======================================================================");
//                        this.cookies = resultCookie;
//                } else {
//                        LogUtil.d(TAG, "COOKE READ MEMORY ====================================================================");
//                }
//                return this.cookies;
//        }
//
//        public void removeCookie(Context context)
//        {
//
//                LogUtil.d(TAG, "COOKE REMOVE==========================================================================");
//                this.cookies = null;
//                deleteData(context, NAME_SYS, SPU_K_COOKIE);
//        }

    // ***************************************************************************************************************************************
    // 푸쉬키
    // ***************************************************************************************************************************************

    /**
     * @param context
     * @return
     * @name 푸쉬키
     */
    public String getRegistPushKey(Context context) {
        return readString(context, NAME_SYS, SPU_K_REGISTKEY, "");
    }

    public void setRegistPushKey(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_REGISTKEY, key);
    }


    public int getRegistPushKeyDate(Context context) {
        return readInt(context, NAME_SYS, SPU_K_REGISTKEY_DAYTE, 0);
    }

    public void setRegistPushKeyDate(Context context, int date) {
        writeInt(context, NAME_SYS, SPU_K_REGISTKEY_DAYTE, date);
    }

    // ***************************************************************************************************************************************
    // API Auth Token
    // ***************************************************************************************************************************************
    public void setAuthToken(Context context, String auth_token) {
        writeString(context, NAME_SYS, SPU_K_AUTH_TOKEN, auth_token);
    }

    public String getAuthToken(Context context) {
        return readString(context, NAME_SYS, SPU_K_AUTH_TOKEN, "");
    }

    // ***************************************************************************************************************************************
    // 광고 본 유무
    // ***************************************************************************************************************************************
    public void setAdView(Context context, boolean isGuide) {
        writeBoolean(context, NAME_SYS, SPU_K_Guide_VIEW, isGuide);
    }

    public boolean getAdView(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_Guide_VIEW, false);
    }

    // ***************************************************************************************************************************************
    // 로그인
    // ***************************************************************************************************************************************
    public void setIsLogin(Context context, boolean isLogin) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_LOGIN, isLogin);
    }

    public boolean getIsLogin(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_LOGIN, false);
    }

    public void setIsAutoLogin(Context context, boolean isAutoLogin) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_LOGIN_AUTO, isAutoLogin);
    }

    public boolean getIsAutoLogin(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_LOGIN_AUTO, true);
    }

    /**
     * 로그인 타입 <br>
     * LOGIN_TYPE_NORMAL = 1 , LOGIN_TYPE_AGENT = 2
     *
     * @param context
     * @return
     */

    // public int getLoginType(Context context)
    // {
    // return readInt(context, NAME_SYS, SPU_K_LOGIN_TYPE, -1);
    // }
    //
    // public void setLoginType(Context context, int type)
    // {
    // writeInt(context, NAME_SYS, SPU_K_LOGIN_TYPE, type);
    // }

    // ***************************************************************************************************************************************
    // 로그인 5회 이상 틀릴시 5분 접속 제한
    // ***************************************************************************************************************************************
    public void setUserLoginTime(Context context, long time) {
        writeLong(context, NAME_USER, SPU_K_USER_LoginTime, time);
    }

    public long getUserLoginTime(Context context) {
        return readLong(context, NAME_USER, SPU_K_USER_LoginTime);
    }

    public String getUserLimitID(Context context) {
        return readString(context, NAME_USER, SPU_K_USER_LimitID, "");
    }

    public void setUserLimitID(Context context, String key) {
        writeString(context, NAME_USER, SPU_K_USER_LimitID, key);
    }

    // ***************************************************************************************************************************************
    // 푸시 관련
    // ***************************************************************************************************************************************
    public void setIsOnPush(Context context, boolean isOnSound) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_PUSH, isOnSound);
    }

    public boolean getIsOnPush(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_PUSH, true);
    }

    public void setIsOnSound(Context context, boolean isOnSound) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_SOUND, isOnSound);
    }

    public boolean getIsOnSound(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_SOUND, true);
    }

    public void setIsOnVib(Context context, boolean isOnVib) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_VIBRATOR, isOnVib);
    }

    public boolean getIsOnVib(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_VIBRATOR, true);
    }

    public void setIsOnPop(Context context, boolean isOnPop) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_POP, isOnPop);
    }

    public boolean getIsOnPop(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_POP, false);
    }

    // ***************************************************************************************************************************************
    // 유저정보
    // ***************************************************************************************************************************************
    public JSONObject getUserInfo(Context context) {
        return readJSONObject(context, NAME_USER, SPU_K_USER_INFO);
    }


    public int getUserFavoriteArtistNo(Context context) {
        JSONObject json = SPUtil.getInstance().getUserInfo(context);
        LogUtil.json(json);
        return JSONUtil.getInteger(json, "artist_no", -1);
    }

    public String getUserFavoriteArtistName(Context context) {
        JSONObject json = SPUtil.getInstance().getUserInfo(context);
        LogUtil.json(json);
        return JSONUtil.getString(json, "artist_name");
    }

    public String getUserFavoriteArtistPic(Context context) {
        JSONObject json = SPUtil.getInstance().getUserInfo(context);
        LogUtil.json(json);
        return JSONUtil.getString(json, "artist_pic");
    }

    public String getUserSex(Context context) {
        JSONObject json = SPUtil.getInstance().getUserInfo(context);
        return JSONUtil.getString(json, "sex");
    }

    public int getUserHeart(Context context) {
        JSONObject json = SPUtil.getInstance().getUserInfo(context);
        return JSONUtil.getInteger(json, "heart_cnt", 0);
    }

    /**
     * 등록한 사진 개수
     *
     * @param context
     * @return
     */
    public int getUserPhotoCount(Context context) {
        int count = 0;
        JSONObject json = SPUtil.getInstance().getUserInfo(context);
        String pic1 = JSONUtil.getStringUrl(json, "pic1");
        String pic2 = JSONUtil.getStringUrl(json, "pic2");
        String pic3 = JSONUtil.getStringUrl(json, "pic3");
        String pic4 = JSONUtil.getStringUrl(json, "pic4");

        if (FormatUtil.isNullorEmpty(pic1)) {
            pic1 = JSONUtil.getStringUrl(json, "member_pic1");
        }
        if (FormatUtil.isNullorEmpty(pic2)) {
            pic2 = JSONUtil.getStringUrl(json, "member_pic2");
        }
        if (FormatUtil.isNullorEmpty(pic3)) {
            pic3 = JSONUtil.getStringUrl(json, "member_pic3");
        }
        if (FormatUtil.isNullorEmpty(pic4)) {
            pic4 = JSONUtil.getStringUrl(json, "member_pic4");
        }

        if (!FormatUtil.isNullorEmpty(pic1)) {
            count++;
        }
        if (!FormatUtil.isNullorEmpty(pic2)) {
            count++;
        }
        if (!FormatUtil.isNullorEmpty(pic3)) {
            count++;
        }
        if (!FormatUtil.isNullorEmpty(pic4)) {
            count++;
        }

        return count;
    }

    public void setUserInfo(Context context, JSONObject userInfo) {
        if (userInfo != null) {


            LogUtil.json(userInfo);

            int member_no = JSONUtil.getInteger(userInfo, "no");
            String member_no_enc = JSONUtil.getString(userInfo, "member_no");


            if (member_no > 0 && !FormatUtil.isNullorEmpty(member_no_enc)) {

                SPUtil.getInstance().setIsLogin(context, true);
                SPUtil.getInstance().setUserNo(context, member_no);
                SPUtil.getInstance().setUserNoEnc(context, member_no_enc);

                try {
                    writeString(context, NAME_USER, SPU_K_USER_INFO, userInfo.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void removeUserInfo(Context context) {
        deleteData(context, NAME_USER, SPU_K_USER_INFO);
    }

    // ***************************************************************************************************************************************
    // 유저정보 위치 정보
    // ***************************************************************************************************************************************
    public JSONObject getUserAddressInfo(Context context) {
        return readJSONObject(context, NAME_USER, SPU_K_USER_Address);
    }

    public void setUserAddressInfo(Context context, JSONObject json) {
        if (json != null)
            writeString(context, NAME_USER, SPU_K_USER_Address, json.toString());
    }

    /**
     * @param context
     * @return 1 지역설정 0 gps
     */
    public int getLocationType(Context context) {
        return readInt(context, NAME_USER, SPU_K_USER_Location, 0);
    }

    /**
     * @param context
     * @param type    1 지역설정 0 gps
     */
    public void setLocationType(Context context, int type) {
        writeInt(context, NAME_USER, SPU_K_USER_Location, type);
    }

    // ***************************************************************************************************************************************
    // 실제 계정 아이디
    // ***************************************************************************************************************************************

    /**
     * @param context
     * @return
     * @name 아이디
     */
    public String getUserID(Context context) {
        return readString(context, NAME_SYS, SPU_K_USER_ID, "");
    }

    public void setUserID(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_USER_ID, key);
    }

    // ***************************************************************************************************************************************
    // 실제 계정 패스워드
    // ***************************************************************************************************************************************

    /**
     * @param context
     * @return
     * @name 패스워드
     */
    public String getUserPW(Context context) {
        return readString(context, NAME_SYS, SPU_K_USER_PW, "");
    }

    public void setUserPW(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_USER_PW, key);
    }

    // ***************************************************************************************************************************************
    // SessionID
    // ***************************************************************************************************************************************

    /**
     * @param context
     * @return
     * @name SessionID
     */
    public String getUserSessionID(Context context) {
        return readString(context, NAME_SYS, SPU_K_USER_SESSION_ID, "");
    }

    public void setUserSessionID(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_USER_SESSION_ID, key);
    }

    // ***************************************************************************************************************************************
    // 실제 계정 페이스 북 아이디
    // ***************************************************************************************************************************************

    /**
     * @param context
     * @return
     * @name 아이디
     */
    public String getUserFBID(Context context) {
        return readString(context, NAME_SYS, SPU_K_USER_FBID, "");
    }

    public void setUserFBID(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_USER_FBID, key);
    }


    public String getUserNoEnc(Context context) {
        if (Constants.DEBUG) {
            //지평선Y (V1pVcVJXTkd3Z1FacEd4UFFId0tOdz09)
            //흥민이 (S2ZheWRPdWQrVjI3RFJDaTZDZ2ZXQT09)
//            return "RE5iZTRmUHpqdi9rZGd6YStqeDVPQT09";
//            return "VmpoZU5YTEVRbGRJMDJLTFlSa3FWUT09";      // 강인이
            // 팬58508
//            return "WmRObWZ0SkN5KzlsNUxmUmxHVFZGUT09";
//            return "eng5aEdXRUFzV1hudXdJVGNGLzAzZz09";
        }



        try {
            return readString(context, NAME_SYS, SPU_K_USER_NO_ENC, "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public void setUserNoEnc(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_USER_NO_ENC, key);
    }

    // ***************************************************************************************************************************************
    // 실제 계정 회원 번호
    // ***************************************************************************************************************************************

    /**
     * @param context
     * @return
     * @name 회원 번호
     */
    public int getUserNo(Context context) {

        if (Constants.DEBUG) {
        }

        return readInt(context, NAME_SYS, SPU_K_USER_NUMBER, 0);
    }

    public void setUserNo(Context context, int key) {
        writeInt(context, NAME_SYS, SPU_K_USER_NUMBER, key);
    }

    // ***************************************************************************************************************************************
    // 로그아웃 처리
    // ***************************************************************************************************************************************
    public void logout(Context context) {

        //removeCookie(context);
        removeUserInfo(context);


        deleteData(context, NAME_USER, SPU_K_USER_INFO);

        deleteData(context, NAME_SYS, SPU_K_USER_NUMBER);
        deleteData(context, NAME_SYS, SPU_K_USER_NO_ENC);

        deleteData(context, NAME_SYS, SPU_K_USER_ID);
        deleteData(context, NAME_SYS, SPU_K_IS_LOGIN);
        deleteData(context, NAME_SYS, SPU_K_NEW_COUNT);
        deleteData(context, NAME_SYS, SPU_K_NEW_COUNT_ALRIM);
        deleteData(context, NAME_SYS, SPU_K_AUTH_TOKEN);

        deleteData(context, NAME_SYS, SPU_K_DATE_EVENT_REGIST_7);
        deleteData(context, NAME_SYS, SPU_K_MSG_ROOM_NO);
        deleteData(context, NAME_SYS, SPU_K_MAIN_TOAST_DATE);
        deleteData(context, NAME_SYS, SPU_K_IS_HAVE_AD_REMOVE_ITEM);
        deleteData(context, NAME_SYS, SPU_K_IS_HAVE_BOOSTER_ITEM);
        deleteData(context, NAME_SYS, SPU_K_ATTEND_DATE);
        deleteData(context, NAME_SYS, SPU_K_AD_INFO_DATE);
        deleteData(context, NAME_SYS, SPU_K_ANDROID_ID);



    }


    public void setMarketUrlIOS(Context context, String url) {
        writeString(context, NAME_SYS, SPU_K_MARKET_URL_IOS, "");
    }

    public void setServerVersion(Context context, String version) {
        writeString(context, NAME_SYS, SPU_K_SERVER_VERSION, version);
    }

    public String getServerVersion(Context context) {
        return readString(context, NAME_SYS, SPU_K_SERVER_VERSION, "");
    }


    // ***************************************************************************************************************************************
    // 푸시타입 : -1알림끄기, 1모두알림
    // ***************************************************************************************************************************************
    public int getPushType(Context context) {
        return readInt(context, NAME_SYS, SPU_K_PUSH_TYPE, 1);
    }

    public void setPushType(Context context, int type) {
        writeInt(context, NAME_SYS, SPU_K_PUSH_TYPE, type);
    }

    // ***************************************************************************************************************************************
    // 위치 정보 동의
    // ***************************************************************************************************************************************
    public boolean getAgreeLocation(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_LOCATION, false);
    }

    public void setAgreeLocation(Context context, boolean isAgree) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_LOCATION, isAgree);
    }

    // ***************************************************************************************************************************************
    // 대화함 뉴카운트
    // ***************************************************************************************************************************************
    public void setNewCount(Context context, int newCount) {
        writeInt(context, NAME_SYS, SPU_K_NEW_COUNT, newCount);
    }

    public int getNewCount(Context context) {
        return readInt(context, NAME_SYS, SPU_K_NEW_COUNT, 0);
    }

    // ***************************************************************************************************************************************
    // 알림 뉴카운트
    // ***************************************************************************************************************************************
    public void setNewCountAlrim(Context context, int newCount) {
        writeInt(context, NAME_SYS, SPU_K_NEW_COUNT_ALRIM, newCount);
    }

    public int getNewCountAlrim(Context context) {
        return readInt(context, NAME_SYS, SPU_K_NEW_COUNT_ALRIM, 0);
    }

    // ***************************************************************************************************************************************
    // 로컬 검색 키워드 리스트
    // ***************************************************************************************************************************************
    public void setLocalSearchHistory(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_LOCAL_SEARCH_HISTORY, jsonArray);
    }

    public JSONArray getLocalSearchHistory(Context context) {
        return readJSONArray(context, NAME_SYS, SPU_K_LOCAL_SEARCH_HISTORY);
    }


    public void setArtistList(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_ARTIST_LIST, jsonArray);
    }

    public JSONArray getArtistList(Context context) {
        try {
            return readJSONArray(context, NAME_SYS, SPU_K_ARTIST_LIST);
        } catch (Exception e) {
            return null;
        }
    }

    public void saveLocalSearchKeyword(Context context, String keyword) {
        JSONArray jsonArray = SPUtil.getInstance().getLocalSearchHistory(context);

        if (jsonArray == null) {
            jsonArray = new JSONArray();
        }

        JSONObject json = null;
        String k = "";
        boolean isConstains = false;
        for (int i = 0; i < jsonArray.length(); i++) {
            json = JSONUtil.getJSONObject(jsonArray, i);
            k = JSONUtil.getString(json, "keyword");

            if (k.equals(keyword)) {
                isConstains = true;
                break;
            }
        }

        if (isConstains == false) {
            JSONObject jsonTemp = new JSONObject();
            JSONUtil.puts(jsonTemp, "keyword", keyword);

            // JSONUtil.put(jsonArray, jsonTemp,
            // jsonArray.length());
            jsonArray.put(jsonTemp);
        }

        SPUtil.getInstance().setLocalSearchHistory(context, jsonArray);
    }

    @SuppressLint("NewApi")
    public void removeLocalSearchKeyword(Context context, String keyword) {
        JSONArray jsonArray = SPUtil.getInstance().getLocalSearchHistory(context);

        if (jsonArray == null) {
            jsonArray = new JSONArray();
        }

        JSONObject json = null;
        String k = "";

        for (int i = 0; i < jsonArray.length(); i++) {
            json = JSONUtil.getJSONObject(jsonArray, i);
            k = JSONUtil.getString(json, "keyword");

            if (k.equals(keyword)) {
                jsonArray.remove(i);
                break;
            }
        }

        SPUtil.getInstance().setLocalSearchHistory(context, jsonArray);
    }


    /**
     * @param context
     * @return
     * @name 현재 참여 중인 채팅 룸 번호
     */
    public int getMsgRoomNo(Context context) {
        return readInt(context, NAME_SYS, SPU_K_MSG_ROOM_NO, 0);
    }

    public void setMsgRoomNo(Context context, int key) {
        writeInt(context, NAME_SYS, SPU_K_MSG_ROOM_NO, key);
    }


    /**
     * @param context
     * @return
     * @name 회원가입 7일 이벤트 팝업 오픈 날짜
     */
    public String getDateEventRegist7(Context context) {
        return readString(context, NAME_SYS, SPU_K_DATE_EVENT_REGIST_7, "");
    }

    public void setDateEventRegist7(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_DATE_EVENT_REGIST_7, key);
    }

    private String _adFullScreen = "";

    public String getAdFullScreen(Context context) {
        if (_adFullScreen.equals("")) {
            _adFullScreen = readString(context, NAME_SYS, SPU_K_USE_AD_FULL_SCREEN, "N");
        }

        LogUtil.d("getAdFullScreen : " + _adFullScreen);

        return _adFullScreen;
    }

    public void setAdFullScreen(Context context, String adFullScreen) {
        LogUtil.d("adFullScreen : " + adFullScreen);
        _adFullScreen = adFullScreen;
        writeString(context, NAME_SYS, SPU_K_USE_AD_FULL_SCREEN, adFullScreen);
    }

    public String getAdYoutube(Context context) {
        return readString(context, NAME_SYS, SPU_K_USE_AD_YOUTUBE, "N");
    }

    public void setAdYoutube(Context context, String yn) {
        LogUtil.d("setReviewRewardYn : " + yn);
        writeString(context, NAME_SYS, SPU_K_USE_AD_YOUTUBE, yn);
    }

    public String getReviewRewardYn(Context context) {
        return readString(context, NAME_SYS, SPU_K_USE_REVIEW_REWARD_YN, "N");
    }

    public void setReviewRewardYn(Context context, String yn) {
        LogUtil.d("setReviewRewardYn : " + yn);
        writeString(context, NAME_SYS, SPU_K_USE_REVIEW_REWARD_YN, yn);
    }


    /**
     * @param context
     * @return
     * @name 초대하기 홍보 alert 관련
     */
    public int getInviteAlertShowCount(Context context) {
        return readInt(context, NAME_SYS, SPU_K_INVITE_ALERT_SHOW_COUNT, 0);
    }

    public void setInviteAlertShowCount(Context context, int key) {
        writeInt(context, NAME_SYS, SPU_K_INVITE_ALERT_SHOW_COUNT, key);
    }

    /**
     * 보상형 광고 시청 후, 리워드 투표권 갯수
     *
     * @param context
     * @return
     */
    public int getAmountTicketAd(Context context) {
        int defaultValue = 2;
        try {
            return readInt(context, NAME_SYS, SPU_K_AMOUNT_TICKET_AD, defaultValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return defaultValue;
    }

    public void setAmountTicketAd(Context context, int amount) {
        writeInt(context, NAME_SYS, SPU_K_AMOUNT_TICKET_AD, amount);
    }


    public void setAppConfig(Context context, JSONObject config_info) {

        LogUtil.d("setAppConfig, start");

        if (config_info == null || !config_info.has("login_naver_yn")) {
            return;
        }

        String login_naver_yn = JSONUtil.getString(config_info, "login_naver_yn");
        String login_kakao_yn = JSONUtil.getString(config_info, "login_kakao_yn");
        String match_yn = JSONUtil.getString(config_info, "match_yn");
        String match_add_yn = JSONUtil.getString(config_info, "match_add_yn");
        String ad_profile_yn = JSONUtil.getString(config_info, "ad_profile_yn");
        String review_type = JSONUtil.getString(config_info, "review_type");
        String use_cauly_yn = JSONUtil.getString(config_info, "use_cauly_yn");
        String use_wad_yn = JSONUtil.getString(config_info, "use_wad_yn");
        String use_max_yn = JSONUtil.getString(config_info, "use_max_yn");
        String use_max_reward_yn = JSONUtil.getString(config_info, "use_max_reward_yn");
        String use_vungle_yn = JSONUtil.getString(config_info, "use_vungle_yn");
        String use_intro_yn = JSONUtil.getString(config_info, "use_intro_yn", "N");
        String use_facebook_yn = JSONUtil.getString(config_info, "use_facebook_yn");
        String use_unityAds_yn = JSONUtil.getString(config_info, "use_unityAds_yn");

        String use_coupang_yn = JSONUtil.getString(config_info, "use_coupang_yn");
        String use_promotion_yn = JSONUtil.getString(config_info, "use_promotion_yn");
        String blog_url = JSONUtil.getString(config_info, "blog_url");

        int available_board_level = JSONUtil.getInteger(config_info, "available_board_level", 1);
        int adLoadingTimeInterval = JSONUtil.getInteger(config_info, "adLoadingTimeInterval", 5);


        int lastest_notice_no = JSONUtil.getInteger(config_info, "lastest_notice_no", -1);
        int ad_view_gap = JSONUtil.getInteger(config_info, "ad_view_gap", 30);

        String ad_view_limit_yn = JSONUtil.getString(config_info, "ad_view_limit_yn", "Y");
        String is_view_seq_ad = JSONUtil.getString(config_info, "is_view_seq_ad", "N");
        int count_load_ad = JSONUtil.getInteger(config_info, "count_load_ad", 2);


        JSONObject jsonAdRemoveInfo = JSONUtil.getJSONObject(config_info, "remove_ad_info");
        JSONObject jsonAuto_ticket_info = JSONUtil.getJSONObject(config_info, "auto_ticket_info");
        JSONObject board_promo_info = JSONUtil.getJSONObject(config_info, "board_promo_info");

        //JSONObject jsonVoteAdInfo = JSONUtil.getJSONObject(config_info, "vote_ad_info");
        JSONObject jsonVoteAdInfo = JSONUtil.getJSONObject(config_info, "vote_ad_info_v2");
        JSONObject btn_info = JSONUtil.getJSONObject(config_info, "btn_info");

        JSONArray keyword_list = JSONUtil.getJSONArray(config_info, "keyword_list");
        JSONArray marketing_ex_list = JSONUtil.getJSONArray(config_info, "marketing_ex_list");

        JSONArray ad_full_seq = JSONUtil.getJSONArray(config_info, "ad_full_seq");
        JSONArray ad_full_seq_v4 = JSONUtil.getJSONArray(config_info, "ad_full_seq_v4");
        JSONArray ad_reward_seq = JSONUtil.getJSONArray(config_info, "ad_reward_seq");

        int repeat_ad_full_screen = JSONUtil.getInteger(config_info, "repeat_ad_full_screen", 4);
        int amount_ticket_ad = JSONUtil.getInteger(config_info, "amount_ticket_ad", 2);
        int repeat_ppobkki_ad = JSONUtil.getInteger(config_info, "repeat_ppobkki_ad", 2);
        int repeat_board_write_ad = JSONUtil.getInteger(config_info, "repeat_board_write_ad", 25);

        String use_ad_full_screen = JSONUtil.getString(config_info, "use_ad_full_screen", "N");
        String use_ad_youtube = JSONUtil.getString(config_info, "use_ad_youtube", "N");
        String use_review_reward = JSONUtil.getString(config_info, "use_review_reward", "N");

        JSONArray banner_list = JSONUtil.getJSONArray(config_info, "banner_list");
        JSONArray banner_full_list = JSONUtil.getJSONArray(config_info, "banner_full_list");

        String vod_key = JSONUtil.getString(config_info, "vod_key", "dev");
        String auto_view_newspick_yn = JSONUtil.getString(config_info, "auto_view_newspick_yn", "N");
        String is_view_baseCharge2_yn = JSONUtil.getString(config_info, "is_view_baseCharge2_yn", "N");
        String is_view_baseCharge3_yn = JSONUtil.getString(config_info, "is_view_baseCharge3_yn", "N");
        String is_view_baseCharge4_yn = JSONUtil.getString(config_info, "is_view_baseCharge4_yn", "N");
        String is_view_baseCharge5_yn = JSONUtil.getString(config_info, "is_view_baseCharge5_yn", "Y");

        String initAd_version = JSONUtil.getString(config_info, "initAd_version", "v1");
        int seconds_view_adpick = JSONUtil.getInteger(config_info, "seconds_view_adpick", 20);

        JSONObject ad_info = JSONUtil.getJSONObject(config_info, "ad_info");

        setAdInfo(ad_info);

        SPUtil.getInstance().setLoginNaverYN(context, login_naver_yn);
        SPUtil.getInstance().setLoginKakaoYN(context, login_kakao_yn);
        SPUtil.getInstance().setMatchYN(context, match_yn);
        SPUtil.getInstance().setMatchAddYN(context, match_add_yn);
        SPUtil.getInstance().setAdProfileYN(context, ad_profile_yn);
        SPUtil.getInstance().setAdRemoveInfo(context, jsonAdRemoveInfo);
        SPUtil.getInstance().setProductAutoTicketInfo(context, jsonAuto_ticket_info);
        SPUtil.getInstance().setKeywordList(context, keyword_list);
        SPUtil.getInstance().setOpenFullScreen(context, repeat_ad_full_screen);
        SPUtil.getInstance().setAmountTicketAd(context, amount_ticket_ad);
        SPUtil.getInstance().setAdFullScreen(context, use_ad_full_screen);
        SPUtil.getInstance().setAdYoutube(context, use_ad_youtube);
        SPUtil.getInstance().setReviewRewardYn(context, use_review_reward);
        SPUtil.getInstance().setVoteAdInfo(context, jsonVoteAdInfo);
        SPUtil.getInstance().setBtnInfo(context, btn_info);
        SPUtil.getInstance().setLastestNoticeNo(context, lastest_notice_no);
        SPUtil.getInstance().setReviewType(context, review_type);
        SPUtil.getInstance().setBoardPromoInfo(context, board_promo_info);
        SPUtil.getInstance().setUseCaulyYn(context, use_cauly_yn);
        SPUtil.getInstance().setUseWadYn(context, use_wad_yn);
        SPUtil.getInstance().setUseMaxYn(context, use_max_yn);
        SPUtil.getInstance().setUseMaxRewardYn(context, use_max_reward_yn);
        SPUtil.getInstance().setUseVungleYn(context, use_vungle_yn);
        SPUtil.getInstance().setUseIntroYn(context, use_intro_yn);
        SPUtil.getInstance().setUseFacebookYn(context, use_facebook_yn);
        SPUtil.getInstance().setUseUnityAdsYn(context, use_unityAds_yn);

        SPUtil.getInstance().setUseCoupangYn(context, use_coupang_yn);
        SPUtil.getInstance().setUsePromotionYn(context, use_promotion_yn);
        SPUtil.getInstance().setMarketingExJsonArray(context, marketing_ex_list);

        SPUtil.getInstance().setAdSeqFull(context, ad_full_seq);
        SPUtil.getInstance().setAdSeqFull_V4(context, ad_full_seq_v4);
        SPUtil.getInstance().setAdSeqReward(context, ad_reward_seq);
        SPUtil.getInstance().setRepeatPpomkkiAd(context, repeat_ppobkki_ad);

        SPUtil.getInstance().setAdBannerList(context, banner_list);
        SPUtil.getInstance().setAdBannerFullList(context, banner_full_list);

        SPUtil.getInstance().setAvailableBoardLevel(context, available_board_level);
        SPUtil.getInstance().setBlogUrl(context, blog_url);
        SPUtil.getInstance().setRepeatBoardWriteAd(context, repeat_board_write_ad);

        SPUtil.getInstance().setClickAdViewGap(context, ad_view_gap);
        SPUtil.getInstance().setAdViewLimitYn(context, ad_view_limit_yn);

        SPUtil.getInstance().setIsViewSeqAd(context, is_view_seq_ad);
        SPUtil.getInstance().setCountLoadAd(context, count_load_ad);
        SPUtil.getInstance().setAdLoadingTimeInterval(context, adLoadingTimeInterval);
        SPUtil.getInstance().setVodKey(context, vod_key);
        SPUtil.getInstance().setAutoViewNewspickYn(context, auto_view_newspick_yn);
        SPUtil.getInstance().setIsViewBaseCharge2Yn(context, is_view_baseCharge2_yn);
        SPUtil.getInstance().setIsViewBaseCharge3Yn(context, is_view_baseCharge3_yn);
        SPUtil.getInstance().setIsViewBaseCharge4Yn(context, is_view_baseCharge4_yn);
        SPUtil.getInstance().setIsViewBaseCharge5Yn(context, is_view_baseCharge5_yn);
        SPUtil.getInstance().setInitAdVersion(context, initAd_version);

        SPUtil.getInstance().setSecondsViewAdpick(context, seconds_view_adpick);


        if (is_view_seq_ad.equals("Y")) {
            Constants.IS_VIEW_SEQ_AD = true;
        } else {
            Constants.IS_VIEW_SEQ_AD = false;
        }

        LogUtil.d("setAppConfig, ok");
    }


    private void setAdInfo(JSONObject json){
        String g_full_screen_01 = JSONUtil.getString(json, "g_full_screen_01");
        String g_full_screen_02 = JSONUtil.getString(json, "g_full_screen_02");
        String g_full_screen_03 = JSONUtil.getString(json, "g_full_screen_03");

        String g_reward_01 = JSONUtil.getString(json, "g_reward_01");
        String g_reward_02 = JSONUtil.getString(json, "g_reward_02");
        String g_reward_03 = JSONUtil.getString(json, "g_reward_03");

        String g_reward_full_01 = JSONUtil.getString(json, "g_reward_full_01");
        String g_reward_full_02 = JSONUtil.getString(json, "g_reward_full_02");
        String g_reward_full_03 = JSONUtil.getString(json, "g_reward_full_03");

        if(!FormatUtil.isNullorEmpty(g_full_screen_01)) Constants.KEY_AD_FULL_SCREEN_01 = g_full_screen_01;
        if(!FormatUtil.isNullorEmpty(g_full_screen_02)) Constants.KEY_AD_FULL_SCREEN_02 = g_full_screen_02;
        if(!FormatUtil.isNullorEmpty(g_full_screen_03)) Constants.KEY_AD_FULL_SCREEN_03 = g_full_screen_03;

        if(!FormatUtil.isNullorEmpty(g_reward_01)) Constants.KEY_AD_REWARD_01 = g_reward_01;
        if(!FormatUtil.isNullorEmpty(g_reward_02)) Constants.KEY_AD_REWARD_02 = g_reward_02;
        if(!FormatUtil.isNullorEmpty(g_reward_03)) Constants.KEY_AD_REWARD_03 = g_reward_03;

        if(!FormatUtil.isNullorEmpty(g_reward_full_01)) Constants.KEY_AD_REWARD_FULL_01 = g_reward_full_01;
        if(!FormatUtil.isNullorEmpty(g_reward_full_02)) Constants.KEY_AD_REWARD_FULL_02 = g_reward_full_02;
        if(!FormatUtil.isNullorEmpty(g_reward_full_03)) Constants.KEY_AD_REWARD_FULL_03 = g_reward_full_03;

        LogUtil.d("setAdInfo, g_full_screen_01 : " + g_full_screen_01);
        LogUtil.d("setAdInfo, g_reward_01 : " + g_reward_01);
        LogUtil.d("setAdInfo, g_reward_full_01 : " + g_reward_full_01);
    }




    public void setAdRemoveInfo(Context context, JSONObject info) {
        writeJSONObject(context, NAME_USER, SPU_K_AD_REMOVE_INFO, info);
    }

    public JSONObject getAdRemoveInfo(Context context) {
        try {
            return readJSONObject(context, NAME_USER, SPU_K_AD_REMOVE_INFO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }

    public void setProductAutoTicketInfo(Context context, JSONObject info) {
        writeJSONObject(context, NAME_USER, SPU_K_PRODUCT_AUTO_TICKET_INFO, info);
    }

    public JSONObject getProductAutoTicketInfo(Context context) {
        try {
            return readJSONObject(context, NAME_USER, SPU_K_PRODUCT_AUTO_TICKET_INFO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }


    public void setVoteAdInfo(Context context, JSONObject info) {
        writeJSONObject(context, NAME_USER, SPU_K_VOTE_AD_INFO, info);
    }

    public JSONObject getVoteAdInfo(Context context) {
        try {
            return readJSONObject(context, NAME_USER, SPU_K_VOTE_AD_INFO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }

    public void setBoardPromoInfo(Context context, JSONObject info) {
        writeJSONObject(context, NAME_USER, SPU_K_BOARD_PROMO_INFO, info);
    }

    public JSONObject getBoardPromoInfo(Context context) {
        try {
            return readJSONObject(context, NAME_USER, SPU_K_BOARD_PROMO_INFO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }

    /**
     * 버튼 관리
     *
     * @param context
     * @param info
     */
    public void setBtnInfo(Context context, JSONObject info) {
        writeJSONObject(context, NAME_USER, SPU_K_BTN_INFO, info);
    }

    public JSONObject getBtnInfo(Context context) {
        try {
            return readJSONObject(context, NAME_USER, SPU_K_BTN_INFO);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new JSONObject();
    }


    /**
     * 네이버 로그인 가능 여부
     */
    public String getLoginNaverYN(Context context) {
        return readString(context, NAME_SYS, SPU_K_LOGIN_NAVER_YN, "N");
    }

    public void setLoginNaverYN(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_LOGIN_NAVER_YN, value);
    }

    /**
     * 카카오톡 로그인 가능 여부
     */
    public String getLoginKakaoYN(Context context) {
        return readString(context, NAME_SYS, SPU_K_LOGIN_KAKAO_YN, "N");
    }

    public void setLoginKakaoYN(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_LOGIN_KAKAO_YN, value);
    }

    /**
     * 매칭 서비스 가능 여부
     */
    public String getMatchYN(Context context) {
        return readString(context, NAME_SYS, SPU_K_MATCH_YN, "N");
    }

    public void setMatchYN(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_MATCH_YN, value);
    }

    /**
     * 매칭 추가 서비스 가능 여부
     */
    public String getMatchAddYN(Context context) {
        return readString(context, NAME_SYS, SPU_K_MATCH_ADD_YN, "N");
    }

    public void setMatchAddYN(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_MATCH_ADD_YN, value);
    }

    /**
     * 프로필 화면에 광고 표시 여부
     */
    public String getAdProfileYN(Context context) {
        return readString(context, NAME_SYS, SPU_K_AD_PROFILE_YN, "N");
    }

    public void setAdProfileYN(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_PROFILE_YN, value);
    }


    private String _useCaluyYn = "";

    /**
     * 카울리 광고 사용 여부
     */
    public String getUseCaulyYn(Context context) {
//        return readString(context, NAME_SYS, SPU_K_AD_CAULY_YN, "N");
        if (_useCaluyYn.equals("")) {
            _useCaluyYn = readString(context, NAME_SYS, SPU_K_AD_CAULY_YN, "N");
        }
        return _useCaluyYn;
    }

    public void setUseCaulyYn(Context context, String value) {
        _useCaluyYn = value;
        writeString(context, NAME_SYS, SPU_K_AD_CAULY_YN, value);
    }

    public String getUseWadYn(Context context) {
        return readString(context, NAME_SYS, SPU_K_AD_WAD_YN, "N");
    }

    public void setUseWadYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_WAD_YN, value);
    }

    public String getAutoViewNewspickYn(Context context) {
        return readString(context, NAME_SYS, SPU_K_AUTO_VIEW_NEWSPICK_YN, "N");
    }

    public void setAutoViewNewspickYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AUTO_VIEW_NEWSPICK_YN, value);
    }

    public String getIsViewBaseCharge2Yn(Context context) {
        return readString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE2_YN, "N");
    }

    public void setIsViewBaseCharge2Yn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE2_YN, value);
    }

    public String getIsViewBaseCharge3Yn(Context context) {
        return readString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE3_YN, "N");
    }

    public void setIsViewBaseCharge3Yn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE3_YN, value);
    }


    public String getIsViewBaseCharge4Yn(Context context) {
        return readString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE4_YN, "N");
    }

    public void setIsViewBaseCharge4Yn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE4_YN, value);
    }


    public String getIsViewBaseCharge5Yn(Context context) {
        return readString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE5_YN, "N");
    }

    public void setIsViewBaseCharge5Yn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_IS_VIEW_BASECHARGE5_YN, value);
    }


    public String getUseMaxYn(Context context) {
        String useMaxYn = readString(context, NAME_SYS, SPU_K_AD_MAX_YN, "N");
//        LogUtil.d("getUseMaxYn, use_max_yn : " + useMaxYn);
        return useMaxYn;
    }

    public void setUseMaxYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_MAX_YN, value);
    }

    public String getUseMaxRewardYn(Context context) {
        String useMaxYn = readString(context, NAME_SYS, SPU_K_AD_MAX_REWARD_YN, "N");
//        LogUtil.d("getUseMaxRewardYn, use_max_reward_yn : " + useMaxYn);
        return useMaxYn;
    }

    public void setUseMaxRewardYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_MAX_REWARD_YN, value);
    }

    public String getUseVungleYn(Context context) {
        String useVungleYn = readString(context, NAME_SYS, SPU_K_AD_VUNGLE_YN, "N");
//        LogUtil.d("getUseVungleYn, use_vungle_yn : " + useVungleYn);
        return useVungleYn;
    }

    public void setUseVungleYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_VUNGLE_YN, value);
    }

    public String getUseIntroYn(Context context) {
        String useYn = readString(context, NAME_SYS, SPU_K_AD_INTRO_YN, "Y");
        LogUtil.d("getUseIntroYn, useYn : " + useYn);
        return useYn;
    }

    public void setUseIntroYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_INTRO_YN, value);
    }


    public String getUseFacebookYn(Context context) {
        String useFacebookYn = readString(context, NAME_SYS, SPU_K_AD_FACEBOOK_YN, "N");
        LogUtil.d("getUseFacebookYn, use_facebook_yn : " + useFacebookYn);
        return useFacebookYn;
    }

    public void setUseFacebookYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_FACEBOOK_YN, value);
    }


    public String getUseUnityAdsYn(Context context) {
        String useUnityAdsYn = readString(context, NAME_SYS, SPU_K_AD_UNITYADS_YN, "N");
//        LogUtil.d("getUseUnityAdsYn, use_unityAds_yn : " + useUnityAdsYn);
        return useUnityAdsYn;
    }

    public void setUseUnityAdsYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_UNITYADS_YN, value);
    }


    public String getUseCoupangYn(Context context) {
        return readString(context, NAME_SYS, SPU_K_AD_COUPANG_YN, "N");
    }

    public void setUseCoupangYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_AD_COUPANG_YN, value);
    }

    public String getUsePromotionYn(Context context) {
        return readString(context, NAME_SYS, SPU_K_USE_PROMOTION_YN, "N");
    }

    public void setUsePromotionYn(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_USE_PROMOTION_YN, value);
    }


    /**
     * 메인 화면 진입시, 안내 토스트 띄운 날짜
     */
    public String getMainToastDate(Context context) {
        return readString(context, NAME_SYS, SPU_K_MAIN_TOAST_DATE, "");
    }

    public void setMainToastDate(Context context, String value) {
        writeString(context, NAME_SYS, SPU_K_MAIN_TOAST_DATE, value);
    }


    /**
     * 키워드 리스트
     */
    public JSONArray getKeywordList(Context context) {
        return readJSONArray(context, NAME_USER, SPU_K_KEYWORD_LIST);
    }

    public void setKeywordList(Context context, JSONArray json) {
        if (json == null) {
            json = new JSONArray();
        }

        writeString(context, NAME_USER, SPU_K_KEYWORD_LIST, json.toString());
    }

    /**
     * atvVod 오픈 횟수
     */
    public void setOpenAtvVod(Context context, int count) {
        writeInt(context, NAME_SYS, SPU_K_OPEN_ATV_VOD, count);
    }

    public int getOpenAtvVod(Context context) {
        return readInt(context, NAME_SYS, SPU_K_OPEN_ATV_VOD, 0);
    }

    /**
     * 전면 광고 오픈 타이밍 횟수
     */
    public void setOpenFullScreen(Context context, int count) {
        writeInt(context, NAME_SYS, SPU_K_OPEN_FULL_SCREEN, count);
    }

    public int getOpenFullScreen(Context context) {
        return readInt(context, NAME_SYS, SPU_K_OPEN_FULL_SCREEN, 10);
    }


    /**
     * 광고 제거 아이템을 갖고 있는지 여부
     *
     * @param context
     * @param isHave
     */
    public void setIsHaveAdRemoveItem(Context context, boolean isHave) {
        _isHaveAdRemoveItem = -1;
        writeBoolean(context, NAME_SYS, SPU_K_IS_HAVE_AD_REMOVE_ITEM, isHave);
    }

    private int _isHaveAdRemoveItem = -1;

    public boolean getIsHaveAdRemoveItem(Context context) {

        if (_isHaveAdRemoveItem == -1) {
            _isHaveAdRemoveItem = readBoolean(context, NAME_SYS, SPU_K_IS_HAVE_AD_REMOVE_ITEM, false) ? 1 : 0;
        }

        return _isHaveAdRemoveItem == 1 ? true : false;
    }


    /**
     * 부스터 아이템을 갖고 있는지 여부
     *
     * @param context
     * @param isHave
     */
    public void setIsHaveBoosterItem(Context context, boolean isHave) {
        _isHaveBoosterItem = -1;
        writeBoolean(context, NAME_SYS, SPU_K_IS_HAVE_BOOSTER_ITEM, isHave);
    }

    private int _isHaveBoosterItem = -1;

    public boolean getIsHaveBoosterItem(Context context) {

        if (_isHaveBoosterItem == -1) {
            _isHaveBoosterItem = readBoolean(context, NAME_SYS, SPU_K_IS_HAVE_BOOSTER_ITEM, false) ? 1 : 0;
        }

        return _isHaveBoosterItem == 1 ? true : false;
    }


    public String getAndroidID(Context context) {
        String uniqueID = readString(context, NAME_SYS, SPU_K_ANDROID_ID, "");

        LogUtil.d("getAndroidID, uniqueID : " + uniqueID);
        if (FormatUtil.isNullorEmpty(uniqueID)) {
            uniqueID = UUID.randomUUID().toString();
            LogUtil.d("getAndroidID, uniqueID : " + uniqueID);
        }

        setAndroidID(context, uniqueID);

        return uniqueID;
    }


    public void refreshAndroidID(Context context) {
        String uniqueID = UUID.randomUUID().toString();
        setAndroidID(context, uniqueID);
    }

    private void setAndroidID(Context context, String key) {
        writeString(context, NAME_SYS, SPU_K_ANDROID_ID, key);
    }


    public String getBoardNick(Context context) {
        return readString(context, NAME_SYS, SPU_K_BOARD_NICK, "");
    }

    public void setBoardNick(Context context, String nick) {
        writeString(context, NAME_SYS, SPU_K_BOARD_NICK, nick);
    }

    /**
     * 유튜브 화면 재생 관련 알림 표시한 날짜
     *
     * @param context
     * @return
     */
    public String getYoutubeAlertDate(Context context) {
        return readString(context, NAME_SYS, SPU_K_YOUTUBE_ALERT_DATE, "");
    }

    public void setYoutubeAlertDate(Context context, String date) {
        writeString(context, NAME_SYS, SPU_K_YOUTUBE_ALERT_DATE, date);
    }

    public String getMarketingAlertDate(Context context) {
        return readString(context, NAME_SYS, SPU_K_MARKETING_ALERT_DATE, "");
    }

    public void setMarketingAlertDate(Context context, String date) {
        writeString(context, NAME_SYS, SPU_K_MARKETING_ALERT_DATE, date);
    }


    public String getRcmdCode(Context context) {
        return readString(context, NAME_SYS, SPU_K_RCMD_CODE, "");
    }

    public void setRcmdCode(Context context, String code) {
        writeString(context, NAME_SYS, SPU_K_RCMD_CODE, code);
    }

    /**
     * 출석체크 한 날짜
     *
     * @param context
     * @return
     */
    public String getAttendDate(Context context) {
        return readString(context, NAME_SYS, SPU_K_ATTEND_DATE, "");
    }

    public void setAttendDate(Context context, String date) {
        writeString(context, NAME_SYS, SPU_K_ATTEND_DATE, date);
    }


    /**
     * 동영상 광고 관련 안내 메시지 출력 날짜
     * @param context
     * @return
     */
    public String getAdInfoDate(Context context) {
        return readString(context, NAME_SYS, SPU_K_AD_INFO_DATE, "");
    }

    public void setAdInfoDate(Context context, String date) {
        writeString(context, NAME_SYS, SPU_K_AD_INFO_DATE, date);
    }


    public String getArtistListDate(Context context) {
        return readString(context, NAME_SYS, SPU_K_ARTIST_LIST_DATE, "");
    }

    public void setArtistListDate(Context context, String date) {
        writeString(context, NAME_SYS, SPU_K_ARTIST_LIST_DATE, date);
    }

    /**
     * AtvVod 에서 재생할 유튜브 데이터 JSONARRAY
     *
     * @param context
     * @return
     */
    public JSONArray getYoutubeJsonArray(Context context) {
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = readJSONArray(context, NAME_SYS, SPU_K_YOUTUBE_JSON_ARRAY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public void setYoutubeJsonArray(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_YOUTUBE_JSON_ARRAY, jsonArray);
    }


    public JSONArray getMarketingExJsonArray(Context context) {
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = readJSONArray(context, NAME_SYS, SPU_K_MARKETING_EX_JSON_ARRAY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public void setMarketingExJsonArray(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_MARKETING_EX_JSON_ARRAY, jsonArray);
    }


    // ***************************************************************************************************************************************
    // 가장 최신 공지사항 번호
    // ***************************************************************************************************************************************
    public void setLastestNoticeNo(Context context, int no) {
        writeInt(context, NAME_SYS, SPU_K_LASTEST_NOTICE_NO, no);
    }

    public int getLastestNoticeNo(Context context) {
        return readInt(context, NAME_SYS, SPU_K_LASTEST_NOTICE_NO, 0);
    }

    // ***************************************************************************************************************************************
    // 클라이언트가 확인한 가장 최신 공지사항 번호
    // ***************************************************************************************************************************************
    public void setLastestNoticeNoViewed(Context context, int no) {
        writeInt(context, NAME_SYS, SPU_K_LASTEST_NOTICE_NO_VIEWED, no);
    }

    public int getLastestNoticeNoViewed(Context context) {
        return readInt(context, NAME_SYS, SPU_K_LASTEST_NOTICE_NO_VIEWED, 0);
    }


    // ***************************************************************************************************************************************
    // 리뷰 참여 방법
    // ***************************************************************************************************************************************
    public String getReviewType(Context context) {
        return readString(context, NAME_SYS, SPU_K_REVIEW_TYPE, "store");
    }

    public void setReviewType(Context context, String type) {
        writeString(context, NAME_SYS, SPU_K_REVIEW_TYPE, type);
    }

    // ***************************************************************************************************************************************
    // 더보기 화면에 접속한 횟수 조회
    // ***************************************************************************************************************************************
    public void setEnterMoreCount(Context context, int count) {
        writeInt(context, NAME_SYS, SPU_K_ENTER_MORE_COUNT, count);
    }

    public int getEnterMoreCount(Context context) {
        return readInt(context, NAME_SYS, SPU_K_ENTER_MORE_COUNT, 1);
    }

    // ***************************************************************************************************************************************
    // Home 화면에 접속한 횟수 조회
    // ***************************************************************************************************************************************
    public void setEnterHomeCount(Context context, int count) {
        writeInt(context, NAME_SYS, SPU_K_ENTER_HOME_COUNT, count);
    }

    public int getEnterHomeCount(Context context) {
        return readInt(context, NAME_SYS, SPU_K_ENTER_HOME_COUNT, 1);
    }


    public void setAdPointAmount(Context context, float point) {
        writeFloat(context, NAME_SYS, SPU_K_AD_POINT_AMOUNT, point);
    }

    public double getAdPointAmount(Context context) {
        return readFloat(context, NAME_SYS, SPU_K_AD_POINT_AMOUNT, 1);
    }

    public void setAdTicketAmount(Context context, int ticketAmount) {
        writeInt(context, NAME_SYS, SPU_K_AD_TICKET_AMOUNT, ticketAmount);
    }

    public int getAdTicketAmount(Context context) {
        return readInt(context, NAME_SYS, SPU_K_AD_TICKET_AMOUNT, 1);
    }


    public void setCheckedFavoriteVote(Context context, boolean checked) {
        writeBoolean(context, NAME_SYS, SPU_K_CHECKED_FAVORITE_VOTE, checked);
    }

    public boolean getCheckedFavoriteVote(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_CHECKED_FAVORITE_VOTE, false);
    }

    public void setCheckedFavoriteDonate(Context context, boolean checked) {
        writeBoolean(context, NAME_SYS, SPU_K_CHECKED_FAVORITE_DONATE, checked);
    }

    public boolean getCheckedFavoriteDonate(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_CHECKED_FAVORITE_DONATE, false);
    }

    public void setCheckedFavoritePopupArtist(Context context, boolean checked) {
        writeBoolean(context, NAME_SYS, SPU_K_CHECKED_FAVORITE_POPUP_ARTIST, checked);
    }

    public boolean getCheckedFavoritePopupArtist(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_CHECKED_FAVORITE_POPUP_ARTIST, false);
    }


    public JSONArray getAdSeqFull(Context context) {
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = readJSONArray(context, NAME_SYS, SPU_K_AD_SEQ_FULL_ARRAY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public void setAdSeqFull(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_AD_SEQ_FULL_ARRAY, jsonArray);
    }

    public JSONArray getAdSeqFull_V4(Context context) {
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = readJSONArray(context, NAME_SYS, SPU_K_AD_SEQ_FULL_ARRAY_V4);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public void setAdSeqFull_V4(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_AD_SEQ_FULL_ARRAY_V4, jsonArray);
    }

    public JSONArray getAdSeqReward(Context context) {
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = readJSONArray(context, NAME_SYS, SPU_K_AD_SEQ_REWARD_ARRAY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public void setAdSeqReward(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_AD_SEQ_REWARD_ARRAY, jsonArray);
    }


    public void setRepeatPpomkkiAd(Context context, int newCount) {
        writeInt(context, NAME_SYS, SPU_K_REPEAT_PPOBKKI_AD, newCount);
    }

    public int getRepeatPpomkkiAd(Context context) {

//        if(Constants.DEBUG) return 3;

        return readInt(context, NAME_SYS, SPU_K_REPEAT_PPOBKKI_AD, 30);
    }


    public void setRepeatBoardWriteAd(Context context, int newCount) {
        writeInt(context, NAME_SYS, SPU_K_REPEAT_BOARD_WRITE_AD, newCount);
    }

    public int getRepeatBoardWriteAd(Context context) {
        return readInt(context, NAME_SYS, SPU_K_REPEAT_BOARD_WRITE_AD, 25);
    }


    public JSONArray getAdBannerList(Context context) {
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = readJSONArray(context, NAME_SYS, SPU_K_BANNER_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (jsonArray.length() > 1) {
            JSONObject j = JSONUtil.getJSONObject(jsonArray, 0);
            JSONUtil.put(jsonArray, j, jsonArray.length());
            jsonArray.remove(0);
            setAdBannerList(context, jsonArray);
        }

        return jsonArray;
    }

    public void setAdBannerList(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_BANNER_LIST, jsonArray);
    }

    public JSONArray getAdBannerFullList(Context context) {
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = readJSONArray(context, NAME_SYS, SPU_K_BANNER_FULL_LIST);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (jsonArray.length() > 1) {
            JSONObject j = JSONUtil.getJSONObject(jsonArray, 0);
            JSONUtil.put(jsonArray, j, jsonArray.length());
            jsonArray.remove(0);
            setAdBannerFullList(context, jsonArray);
        }

        return jsonArray;
    }

    public void setAdBannerFullList(Context context, JSONArray jsonArray) {
        writeJSONArray(context, NAME_SYS, SPU_K_BANNER_FULL_LIST, jsonArray);
    }


    public String getRankSeq(Context context) {
        return readString(context, NAME_SYS, SPU_K_RANK_SEQ, "vote_cnt_month");
    }

    public void setRankSeq(Context context, String type) {
        writeString(context, NAME_SYS, SPU_K_RANK_SEQ, type);
    }


    public String getRankGender(Context context) {
        return readString(context, NAME_SYS, SPU_K_RANK_GENDER, "");
    }

    public void setRankGender(Context context, String type) {
        writeString(context, NAME_SYS, SPU_K_RANK_GENDER, type);
    }


    public int getAvailableBoardLevel(Context context) {
        return readInt(context, NAME_SYS, SPU_K_AVAILABLE_BOARD_LEVEL, 5);
    }

    public void setAvailableBoardLevel(Context context, int key) {
        writeInt(context, NAME_SYS, SPU_K_AVAILABLE_BOARD_LEVEL, key);
    }


    public void setIsAgreeWriteBoard(Context context, boolean isOnPop) {
        writeBoolean(context, NAME_SYS, SPU_K_IS_AGREE_WRITE_BOARD, isOnPop);
    }

    public boolean getIsAgreeWriteBoard(Context context) {
        return readBoolean(context, NAME_SYS, SPU_K_IS_AGREE_WRITE_BOARD, true);
    }


    public String getBlogUrl(Context context) {
        return readString(context, NAME_SYS, SPU_K_BLOG_URL, "https://blog.naver.com/code404app");
    }

    public void setBlogUrl(Context context, String url) {
        LogUtil.d("setBlogUrl : " + url);
        writeString(context, NAME_SYS, SPU_K_BLOG_URL, url);
    }


    private void setBoardEnterCount(Context context, int count) {
        writeInt(context, NAME_SYS, SPU_K_BOARD_ENTER_COUNT, count);
    }

    public int getBoardEnterCount(Context context) {
        int count = readInt(context, NAME_SYS, SPU_K_BOARD_ENTER_COUNT, 0);
        count++;

        if (count >= 10000) count = 0;
        setBoardEnterCount(context, count);

        return count;
    }

    private int getBoardEnterCount_ReadOnly(Context context) {
        int count = readInt(context, NAME_SYS, SPU_K_BOARD_ENTER_COUNT, 0);
        return count;
    }


    /**
     * 클릭 시 광고가 나올 수 있게 카운팅 조정(-2까지 조정... 2클릭 시 노출)
     *
     * @param context
     */
    public void setBoardEnterShort(Context context) {
        int boardEnterCount = SPUtil.getInstance().getBoardEnterCount_ReadOnly(context);
        int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(context);

        boardEnterCount = boardEnterCount % repeatBoardWriteAd;

        if (boardEnterCount < repeatBoardWriteAd - 1) {
            boardEnterCount = repeatBoardWriteAd - 2;
        }

        setBoardEnterCount(context, boardEnterCount);

        LogUtil.d("boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);
    }


    public void setClickAdView(Context context, long time) {
        writeLong(context, NAME_USER, SPU_K_CLICK_AD_VIEW, time);
    }

    public long getClickAdView(Context context) {
        return readLong(context, NAME_USER, SPU_K_CLICK_AD_VIEW);
    }

    public void setClickAdViewGap(Context context, long time) {
        writeLong(context, NAME_USER, SPU_K_CLICK_AD_GAP, time);
    }

    public long getClickAdViewGap(Context context) {
        return readLong(context, NAME_USER, SPU_K_CLICK_AD_GAP);
    }

    public void setAdViewLimitYn(Context context, String yn) {
        writeString(context, NAME_USER, SPU_K_AD_VIEW_LIMIT_YN, yn);
    }

    public String getAdViewLimitYn(Context context) {
        String txt = readString(context, NAME_USER, SPU_K_AD_VIEW_LIMIT_YN, "");
        LogUtil.d("getAdViewLimitYn : " + txt);
        return txt;
    }


    public boolean isAvailableBoard(Context context) {
        JSONObject jsonUser = SPUtil.getInstance().getUserInfo(context);
        int lv = JSONUtil.getInteger(jsonUser, "lv", 0);
        int availableBoardLevel = SPUtil.getInstance().getAvailableBoardLevel(context);

        LogUtil.d("setActiveAdTextDisplay, lv : " + lv + ", availableBoardLevel : " + availableBoardLevel);

        return (lv >= availableBoardLevel);
    }


//    private static final String SPU_K_IS_VIEW_SEQ_AD = "SPU_K_IS_VIEW_SEQ_AD";
//    private static final String SPU_K_COUNT_LOAD_AD = "SPU_K_COUNT_LOAD_AD";


    /**
     * 광고 로딩 방법 Y/N
     *
     * @param context
     * @param yn
     */
    public void setIsViewSeqAd(Context context, String yn) {
        writeString(context, NAME_USER, SPU_K_IS_VIEW_SEQ_AD, yn);
    }

    public String getIsViewSeqAd(Context context) {
        try {
            return readString(context, NAME_USER, SPU_K_IS_VIEW_SEQ_AD, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Y";
    }


    /**
     * 광고를 한번에 로딩할 개수
     *
     * @param context
     * @param count
     */
    private void setCountLoadAd(Context context, int count) {
        writeInt(context, NAME_SYS, SPU_K_COUNT_LOAD_AD, count);
    }

    public int getCountLoadAd(Context context) {
        int count = readInt(context, NAME_SYS, SPU_K_COUNT_LOAD_AD, 3);
        return count;
    }


    /**
     * 광고를 한번 로딩한 후에 2번째 로딩하기 위한 시간 간격
     *
     * @param context
     * @param time
     */
    private void setAdLoadingTimeInterval(Context context, int time) {
        writeInt(context, NAME_SYS, SPU_K_AD_LOADING_TIME_INTERVAL, time);
    }

    public int getAdLoadingTimeInterval(Context context) {
        int count = readInt(context, NAME_SYS, SPU_K_AD_LOADING_TIME_INTERVAL, 15);

        if (!SPUtil.getInstance().getAdFullScreen(context).equals("Y")) {
            count = 15;
        }
        return count;
    }


    private void setSecondsViewAdpick(Context context, int time) {
        writeInt(context, NAME_SYS, SPU_K_SECONDS_VIEW_ADPICK, time);
    }

    public int getSecondsViewAdpick(Context context) {
        int count = readInt(context, NAME_SYS, SPU_K_SECONDS_VIEW_ADPICK, 20);
        return count;
    }


    public void setVodKey(Context context, String key) {
        writeString(context, NAME_USER, SPU_K_VOD_KEY, key);
    }

    public String getVodKey(Context context) {
        try {
            return readString(context, NAME_USER, SPU_K_VOD_KEY, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "dev";
    }


    public void setInitAdVersion(Context context, String v) {
        writeString(context, NAME_USER, SPU_K_INIT_AD_VERSION, v);
    }

    public String getInitAdVersion(Context context) {
        try {
            return readString(context, NAME_USER, SPU_K_INIT_AD_VERSION, "V1");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "V1";
    }


    public void setChargeMode(Context context, String key) {
        writeString(context, NAME_USER, SPU_K_CHARGE_MODE, key);
    }

    public String getChargeMode(Context context) {
        try {
            return readString(context, NAME_USER, SPU_K_CHARGE_MODE, "vote_only");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "balance";
    }
}
