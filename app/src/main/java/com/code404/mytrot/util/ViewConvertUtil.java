package com.code404.mytrot.util;

import android.content.Context;
import android.widget.TextView;

import com.code404.mytrot.R;

public class ViewConvertUtil {

    public static void setVodTypeText(Context ctx, TextView txtType, String gb){
        //ENT:예능, TRO:트롯, ETC:기타
        if (gb.equals("TRO")) {
            txtType.setText("트롯");
            txtType.setBackgroundResource(R.drawable.bg_type_sing);
            txtType.setTextColor(ctx.getResources().getColor(R.color.purpley));
        } else if (gb.equals("ENT")) {
            txtType.setText("예능");
            txtType.setBackgroundResource(R.drawable.bg_type_ent);
            txtType.setTextColor(ctx.getResources().getColor(R.color.tangerine));
        } else if (gb.equals("ETC")) {
            txtType.setText("기타");
            txtType.setBackgroundResource(R.drawable.bg_type_etc);
            txtType.setTextColor(ctx.getResources().getColor(R.color.apple_green));
        }
    }

}
