package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdRevenueListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.nativeAds.MaxNativeAdListener;
import com.applovin.mediation.nativeAds.MaxNativeAdLoader;
import com.applovin.mediation.nativeAds.MaxNativeAdView;
import com.applovin.mediation.nativeAds.MaxNativeAdViewBinder;
import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
//import com.fsn.cauly.CaulyAdInfo;
//import com.fsn.cauly.CaulyNativeAdInfoBuilder;
//import com.fsn.cauly.CaulyNativeAdView;
//import com.fsn.cauly.CaulyNativeAdViewListener;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.nativead.NativeAd;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;

import static com.google.android.gms.ads.formats.NativeAdOptions.ADCHOICES_TOP_LEFT;
import static com.google.android.gms.ads.formats.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_SQUARE;

import androidx.annotation.NonNull;


public class AlertAdSpecial extends Alert {
    private static String TAG = "AlertAdSpecial";

    private Context mContext;

    private AdRequest _adRequest = null;

    private String _ad_id = "";
    private AdLoader _adLoader = null;
    private ImageButton _btnClose = null;
    private ImageButton _btnClose2 = null;
    private View _baseAd_Native = null;
    private RelativeLayout _base_cauly_ad = null;


    private com.google.android.ads.nativetemplates.TemplateView _adNativeView = null;

    private RelativeLayout _baseParent = null;
    private RelativeLayout _baseAd_body = null;
    private LinearLayout _baseAd = null;
    private LinearLayout _baseAd_01 = null;
    private TextView _txtRewardName = null;
    private TextView _txtRewardCount = null;
    private ImageView _imgReward = null;
    private LinearLayout _baseAd_02 = null;
    private TextView _txtRewardCount_01 = null;
    private TextView _txtRewardCount_02 = null;
    private View _baseBlind = null;
    private ImageView _imgBlind01 = null;
    private ImageView _imgBlind02 = null;
    private ImageView _imgBlind03 = null;
    private ImageView _imgBlind04 = null;
    private ImageView _imgBlind05 = null;
    private ImageView _imgBlind06 = null;
    private ImageView _imgBlind07 = null;
    private ImageView _imgBlind08 = null;
    private ImageView _imgBlind09 = null;
    private ImageView _imgBlind10 = null;
    private LinearLayout _baseAd_00 = null;
    private LinearLayout _baseAd_Desc = null;

    private TextView _txtDesc01 = null;
    private TextView _txtDesc02 = null;
    private Button _btnRefresh = null;
    private FrameLayout _adViewContainer;

    private MaxNativeAdLoader nativeAdLoader;
    private MaxAd loadedNativeAd;

    private List<ImageView> _img_list = new ArrayList<ImageView>();

    private boolean _is_auto_view = true;

    private int _remainCountTarget;
    private int _no = -1;
    private Dialog _dialog;

    private AdSize _adSize;

    private Vibrator _vibrator;
    private int _tryApiCount = 0;

    /**
     * 특수 광고
     */
    public void showAdSpecial(final Context context, AdSize adSize) {
        mContext = context;
        _adSize = adSize;

        try {

            _dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
            _dialog.setContentView(R.layout.alert_ad_special);

            _baseParent = (RelativeLayout) _dialog.findViewById(R.id.baseParent);
            _baseAd_body = (RelativeLayout) _dialog.findViewById(R.id.baseAd_body);
            _base_cauly_ad = (RelativeLayout) _dialog.findViewById(R.id.base_cauly_ad);


            _baseAd = (LinearLayout) _dialog.findViewById(R.id.baseAd);
            _baseAd_01 = (LinearLayout) _dialog.findViewById(R.id.baseAd_01);
            _txtRewardName = (TextView) _dialog.findViewById(R.id.txtRewardName);
            _txtRewardCount = (TextView) _dialog.findViewById(R.id.txtRewardCount);
            _imgReward = (ImageView) _dialog.findViewById(R.id.imgReward);
            _baseAd_02 = (LinearLayout) _dialog.findViewById(R.id.baseAd_02);
            _txtRewardCount_01 = (TextView) _dialog.findViewById(R.id.txtRewardCount_01);
            _txtRewardCount_02 = (TextView) _dialog.findViewById(R.id.txtRewardCount_02);
            _baseBlind = _dialog.findViewById(R.id.baseBlind);
            _imgBlind01 = (ImageView) _dialog.findViewById(R.id.imgBlind01);
            _imgBlind02 = (ImageView) _dialog.findViewById(R.id.imgBlind02);
            _imgBlind03 = (ImageView) _dialog.findViewById(R.id.imgBlind03);
            _imgBlind04 = (ImageView) _dialog.findViewById(R.id.imgBlind04);
            _imgBlind05 = (ImageView) _dialog.findViewById(R.id.imgBlind05);
            _imgBlind06 = (ImageView) _dialog.findViewById(R.id.imgBlind06);
            _imgBlind07 = (ImageView) _dialog.findViewById(R.id.imgBlind07);
            _imgBlind08 = (ImageView) _dialog.findViewById(R.id.imgBlind08);
            _imgBlind09 = (ImageView) _dialog.findViewById(R.id.imgBlind09);
            _imgBlind10 = (ImageView) _dialog.findViewById(R.id.imgBlind10);
            _baseAd_00 = (LinearLayout) _dialog.findViewById(R.id.baseAd_00);
            _baseAd_Desc = (LinearLayout) _dialog.findViewById(R.id.baseAd_Desc);
            _baseAd_Native = (RelativeLayout) _dialog.findViewById(R.id.baseAd_Native);
            _adNativeView = (com.google.android.ads.nativetemplates.TemplateView) _dialog.findViewById(R.id.adNativeView);
            _btnClose = (ImageButton) _dialog.findViewById(R.id.btnClose);

            _txtDesc01 = _dialog.findViewById(R.id.txtDesc01);
            _txtDesc02 = _dialog.findViewById(R.id.txtDesc02);
            _adViewContainer = _dialog.findViewById(R.id.adViewContainer);
            _btnRefresh = _dialog.findViewById(R.id.btnRefresh);

            _btnClose2 = _dialog.findViewById(R.id.btnClose2);

            _img_list.add(_imgBlind01);
            _img_list.add(_imgBlind02);
            _img_list.add(_imgBlind03);
            _img_list.add(_imgBlind04);
            _img_list.add(_imgBlind05);
            _img_list.add(_imgBlind06);
            _img_list.add(_imgBlind07);
            _img_list.add(_imgBlind08);
            _img_list.add(_imgBlind09);
            _img_list.add(_imgBlind10);

            _baseAd_body.setVisibility(View.GONE);

            _vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);

            Integer[] msgArr = new Integer[]{1, 2, 3};
            int ran = new Random().nextInt(msgArr.length);
            LogUtil.d(TAG, "ran : " + ran);
            if (ran >= msgArr.length) ran = msgArr.length - 1;
            if (ran < 0) ran = 0;
            _remainCountTarget = msgArr[ran];
            LogUtil.d(TAG, "remainCountTarget: " + _remainCountTarget);
            LogUtil.d(TAG, "_img_list.size() : " + _img_list.size());

            _btnRefresh.setVisibility(View.GONE);

            initAd();

            callApi_ppobkkiAd_get_ready();

            _baseBlind.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (_vibrator != null) {
                        _vibrator.vibrate(new long[]{100, 50}, -1);
                    }
                    if (_img_list.size() == 0) {
                        return;
                    }
                    int r = 0;

                    while (_img_list.size() > 1) {
                        r = new Random().nextInt(_img_list.size() - 1);
                        if (_img_list.size() > r) {
                            break;
                        }
                    }

                    LogUtil.d(TAG, "_img_list.size() : " + _img_list.size());
                    LogUtil.d(TAG, "r : " + r);

                    if (_img_list.size() > r) {
                        _img_list.get(r).setVisibility(View.INVISIBLE);
                        _img_list.remove(r);
                    }

                    if (_img_list.size() == _remainCountTarget) {
                        _baseAd_Native.setVisibility(View.VISIBLE);
                    }

                    if (_img_list.size() == 0) {
                        callApi_ppobkkiAd_play();
                    }
                }
            });

            _btnRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callApi_ppobkkiAd_get_ready();
                }
            });

            _dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    if (_closeListener != null) {
                        _closeListener.onClose(dialog, Alert.CANCEL);
                    }
                    _dialog.dismiss();
                }
            });

            _btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    _is_auto_view = false;
                    _baseAd_Native.setVisibility(View.GONE);
                    //loadAd();
                }
            });

            _btnClose2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (which == Alert.BUTTON1) {
                                if (_closeListener != null) {
                                    // 뉴스 기사로 이어지게 처리합니다.
                                    _closeListener.onClose(dialog, Alert.BUTTON4);
                                }
                                _dialog.dismiss();
                            }
                        }
                    });
                    alert.showAlert(getContext(), "선물 포장 광고를 중단하시겠습니까?", true, "확인", "취소");
                }
            });

            _dialog.setCancelable(false);
            _dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Context getContext() {
        return mContext;
    }

    private void initAd() {

        _adNativeView.setVisibility(View.GONE);
        _adViewContainer.setVisibility(View.GONE);

//        if (Constants.DEBUG)
//            loadAd_Google();
//        else
//            loadAd_Max();

        // MAX - CaulyBig - CaulySmall - Google
        loadAd_Max();
    }

    private void loadAd_Google() {

        _ad_id = Constants.KEY_AD_NATIVE;
        _adRequest = new AdRequest.Builder()
                .build();

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                LogUtil.d(TAG, "MobileAds, onInitializationComplete");
            }
        });


        _adLoader = new AdLoader.Builder(getContext(), _ad_id)
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        LogUtil.d(TAG, "MobileAds, onUnifiedNativeAdLoaded, _adLoader.isLoading() : " + _adLoader.isLoading());
                        // some code that displays the ad.

                        if (_adLoader.isLoading()) {
                            // The AdLoader is still loading ads.
                            // Expect more adLoaded or onAdFailedToLoad callbacks.

                            if (_is_auto_view) {

                                _baseAd_body.setVisibility(View.VISIBLE);
                                _baseAd_00.setVisibility(View.GONE);

                                if (_adNativeView != null)
                                    _adNativeView.setVisibility(View.VISIBLE);
                                if (_btnClose != null)
                                    _btnClose.setVisibility(View.VISIBLE);
                                if (_baseAd_Native != null)
                                    _baseAd_Native.setVisibility(View.VISIBLE);

                                if (_base_cauly_ad != null)
                                    _base_cauly_ad.setVisibility(View.GONE);

                                if (_no > 0) {
                                    _txtDesc01.setText("터치해서 선물 포장을 뜯어주세요.");
                                    _txtDesc02.setText("");
                                }
                            }


                            try {
                                ColorDrawable c = new ColorDrawable();
                                c.setColor(getContext().getResources().getColor(R.color.white));
                                NativeTemplateStyle styles = new
                                        NativeTemplateStyle.Builder().withMainBackgroundColor(c).build();
                                _adNativeView.setStyles(styles);
                                _adNativeView.setNativeAd(nativeAd);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            // The AdLoader has finished loading ads.
                            //


                        }
                    }
                })
                .withAdListener(new AdListener() {
                    @Override
                    public void onAdClicked() {
                        super.onAdClicked();
                        LogUtil.d(TAG, "NativeAd, onAdClicked");
                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                        LogUtil.d(TAG, "NativeAd, onAdClosed");
                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        super.onAdFailedToLoad(loadAdError);
                        LogUtil.d(TAG, "NativeAd, onAdFailedToLoad");
                        LogUtil.d(TAG, "NativeAd, onAdFailedToLoad, loadAdError : " + loadAdError.toString());
                    }

                    @Override
                    public void onAdImpression() {
                        super.onAdImpression();
                        LogUtil.d(TAG, "NativeAd, onAdImpression");
                    }

                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        LogUtil.d(TAG, "NativeAd, onAdLoaded");
                    }

                    @Override
                    public void onAdOpened() {
                        super.onAdOpened();
                        LogUtil.d(TAG, "NativeAd, onAdOpened");
                    }
                })
                .withNativeAdOptions(new com.google.android.gms.ads.nativead.NativeAdOptions.Builder()
                        .setReturnUrlsForImageAssets(false)
                        .setAdChoicesPlacement(ADCHOICES_TOP_LEFT)
                        .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_SQUARE)
                        .build()
                )
                .build();

        LogUtil.e(TAG, "loadAd");
        LogUtil.e(TAG, "loadAd, _adLoader : " + (_adLoader == null ? "null" : "ok"));
        LogUtil.e(TAG, "loadAd, _adRequest : " + (_adRequest == null ? "null" : "ok"));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (_baseAd_00.getVisibility() == View.VISIBLE) {
                    Alert.toastLong(getContext(), "광고 로딩에 실패하여 기사 광고로 이동합니다.");
                    if (_closeListener != null) {
                        _closeListener.onClose(_dialog, Alert.BUTTON4);
                    }
                    _dialog.dismiss();
                }
            }
        }, 6000);


        _adLoader.loadAds(_adRequest, 2);
    }


//    private void loadAd_Cauly_Small() {
//        CaulyAdInfo adInfo = new CaulyNativeAdInfoBuilder(Constants.KEY_CAULY_AD_ID)
//                .layoutID(R.layout.activity_native_view)
//                .iconImageID(R.id.icon)
//                .titleID(R.id.title)
//                .subtitleID(R.id.subtitle)
//                .sponsorPosition(R.id.sponsor, CaulyAdInfo.Direction.CENTER)
//                .build();
//        CaulyNativeAdView nativeAd = new CaulyNativeAdView(getContext());
//        nativeAd.setAdInfo(adInfo);
//        nativeAd.setAdViewListener(new CaulyNativeAdViewListener() {
//            @Override
//            public void onReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, boolean b) {
//                LogUtil.d(TAG, "loadAd_Cauly_Small, onReceiveNativeAd, b : " + b);
//
//                caulyNativeAdView.attachToView(_base_cauly_ad);
//
//                _baseAd_body.setVisibility(View.VISIBLE);
//                _baseAd_00.setVisibility(View.GONE);
//                _adNativeView.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onFailedToReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, int i, String s) {
//                LogUtil.d(TAG, "loadAd_Cauly_Small, onFailedToReceiveNativeAd, i : " + i + ", s : " + s);
//
//                loadAd_Google();
//            }
//        });
//        nativeAd.request();
//    }
//
//    private void loadAd_Cauly_Big() {
//        CaulyAdInfo adInfo = new CaulyNativeAdInfoBuilder(Constants.KEY_CAULY_AD_ID)
//                .layoutID(R.layout.activity_native_cardlistview) // 네이티브애드에 보여질 디자인을 작성하여 등록한다.
//                .mainImageID(R.id.image)                        // 메인 이미지 등록
//                .iconImageID(R.id.icon)                            // 아이콘 등록
//                .titleID(R.id.title)                            // 제목 등록
//                .subtitleID(R.id.subtitle)                        // 부제목 등록
//                .textID(R.id.description)                        //자세한 설명 등록
//                .mainImageOrientation(CaulyAdInfo.Orientation.PORTRAIT)
//                .sponsorPosition(R.id.sponsor, CaulyAdInfo.Direction.LEFT)
//                .build();
//        CaulyNativeAdView nativeView = new CaulyNativeAdView(getContext());
//        nativeView.setAdInfo(adInfo);
//        nativeView.setAdViewListener(new CaulyNativeAdViewListener() {
//            @Override
//            public void onReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, boolean b) {
//                LogUtil.d(TAG, "loadAd_Cauly_Big, onReceiveNativeAd, b : " + b);
//
//                caulyNativeAdView.attachToView(_base_cauly_ad);
//
//                _baseAd_body.setVisibility(View.VISIBLE);
//                _baseAd_00.setVisibility(View.GONE);
//                _adNativeView.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onFailedToReceiveNativeAd(CaulyNativeAdView caulyNativeAdView, int i, String s) {
//                LogUtil.d(TAG, "loadAd_Cauly_Big, onFailedToReceiveNativeAd, i : " + i + ", s : " + s);
//                loadAd_Cauly_Small();
//            }
//        });
//        nativeView.request();
//    }


    private void loadAd_Max() {
        nativeAdLoader = new MaxNativeAdLoader(Constants.KEY_MAX_NATIVE_ID, getContext());
        nativeAdLoader.setRevenueListener(new MaxAdRevenueListener() {
            @Override
            public void onAdRevenuePaid(MaxAd ad) {
                LogUtil.d(TAG, "loadAd_Max, onAdRevenuePaid");
            }
        });
        nativeAdLoader.setNativeAdListener(new MaxNativeAdListener() {
            @Override
            public void onNativeAdLoaded(final MaxNativeAdView nativeAdView, final MaxAd nativeAd) {
                LogUtil.d(TAG, "loadAd_Max, onNativeAdLoaded, nativeAdView : " + (nativeAdView == null ? "null" : nativeAdView.toString()));
                LogUtil.d(TAG, "loadAd_Max, onNativeAdLoaded, nativeAd : " + (nativeAd == null ? "null" : nativeAd.toString()));

                try {

                    // Clean up any pre-existing native ad to prevent memory leaks.
                    if (loadedNativeAd != null) {
                        nativeAdLoader.destroy(loadedNativeAd);
                    }

                    // Save ad for cleanup.
                    loadedNativeAd = nativeAd;

                    _base_cauly_ad.removeAllViews();
                    _base_cauly_ad.addView(nativeAdView);

                    _baseAd_body.setVisibility(View.VISIBLE);
                    _baseAd_00.setVisibility(View.GONE);
                    _adNativeView.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
//                    loadAd_Cauly_Big();
                    loadAd_Google();
                }

            }

            @Override
            public void onNativeAdLoadFailed(final String adUnitId, final MaxError error) {
                // Native ad load failed.
                // AppLovin recommends retrying with exponentially higher delays up to a maximum delay.
                LogUtil.d(TAG, "loadAd_Max, onNativeAdLoadFailed");

//                loadAd_Cauly_Big();
                loadAd_Google();
            }
        });

        nativeAdLoader.loadAd();
    }


    private void callApi_ppobkkiAd_get_ready() {

        LogUtil.e("==========callApi_ppobkkiAd_get_ready : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.ppobkkiAd_get_ready(
                SPUtil.getInstance().getUserNoEnc(getContext())
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                _tryApiCount++;

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d(TAG, "resultCode : " + resultCode);
                LogUtil.d(TAG, "resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                dataBinding(data);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d(TAG, "onFailure");

                dataBinding(null);
            }
        });
    }


    private void callApi_ppobkkiAd_play() {

        LogUtil.e("==========callApi_ppobkkiAd_play : start==========");
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.ppobkkiAd_play(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , _no
        );
        final Dialog dialog = ProgressDialogUtil.show(getContext(), null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d(TAG, "resultCode : " + resultCode);
                LogUtil.d(TAG, "resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");
                JSONObject userInfo = JSONUtil.getJSONObject(data, "member");

                LogUtil.json(userInfo);

                SPUtil.getInstance().setUserInfo(getContext(), userInfo);

                if (!FormatUtil.isNullorEmpty(resultMessage)) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialog, int which) {
                            if (_closeListener != null) {
                                _closeListener.onClose(dialog, Alert.BUTTON1);
                            }
                            _dialog.dismiss();
                        }
                    });
                    alert.showAlert(getContext(), resultMessage);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d(TAG, "onFailure");
            }
        });
    }


    private void dataBinding(JSONObject data) {
        _no = JSONUtil.getInteger(data, "no", 0);
        int bonus_vote_cnt = JSONUtil.getInteger(data, "bonus_vote_cnt", 0);
        double bonus_point = JSONUtil.getDouble(data, "bonus_point", 0);

        _baseAd_01.setVisibility(View.GONE);
        _baseAd_02.setVisibility(View.GONE);
        _btnRefresh.setVisibility(View.GONE);


        // TEST
//        if (_tryApiCount < 3) {
//            _no = 0;
//            bonus_vote_cnt = 0;
//            bonus_point = 0;
//        }

        if (_tryApiCount > 1) {
            _txtDesc01.setText("터치해서 선물 포장을 뜯어주세요.");
            _txtDesc02.setText("");
        }

        if (bonus_point > 0 && bonus_vote_cnt > 0) {
            _baseAd_02.setVisibility(View.VISIBLE);
            _txtRewardCount_01.setText(bonus_vote_cnt + "장");
            _txtRewardCount_02.setText(bonus_point + "P");
        } else if (bonus_point > 0) {
            _baseAd_01.setVisibility(View.VISIBLE);
            _txtRewardName.setText("포인트");
            _txtRewardCount.setText(bonus_point + "P");
            _imgReward.setImageResource(R.drawable.ic_specialad_p);
        } else if (bonus_vote_cnt > 0) {
            _baseAd_01.setVisibility(View.VISIBLE);
            _txtRewardName.setText("투표권");
            _txtRewardCount.setText(bonus_vote_cnt + "장");
            _imgReward.setImageResource(R.drawable.ic_specialad_vote);
        } else {
            _txtDesc02.setText("재료 준비에 실패했습니다.\n새로고침을 눌러서 재료를 다시 준비해주세요.");
            _btnRefresh.setVisibility(View.VISIBLE);
        }
    }

}