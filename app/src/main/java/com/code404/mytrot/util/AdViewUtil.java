package com.code404.mytrot.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;

public class AdViewUtil {


    private static AdManager _adManager = null;


    public static boolean isReadyCheck(Activity activity, Context context) {
        if (_adManager == null) {
            init(activity, context);
            return false;
        }


        return true;
    }


    public static void goActionAfterAdView(Activity activity, Context context, InterfaceSet.AdFullViewCompleteListener listener) {
        goActionAfterAdView(activity, context, false, true, listener);
    }

    public static void goActionAfterAdView(Activity activity, Context context, boolean mustTry, boolean isOnlyFull, InterfaceSet.AdFullViewCompleteListener listener) {


        if (_adManager == null) {
            if (listener != null) listener.onAfterAction();
            return;
        }




        _adManager.setOnAdCompleteListener(new InterfaceSet.OnAd2CompleteListener() {
            @Override
            public void onAdLoadSuccess() {
                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdLoadSuccess");
                _adManager = null;
            }

            @Override
            public void onAdLoadFail() {
                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdLoadFail");

                if (listener != null) listener.onAfterAction();

                _adManager = null;
            }

            @Override
            public void onAdPlayComplete(Constants.enum_ad enumAd) {
                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdPlayComplete");

                if (listener != null) listener.onAfterAction();


                _adManager = null;
            }
        });

        boolean isShow = _adManager.showAd();

        if(isShow == false) {
            _adManager = null;
            if (listener != null) listener.onAfterAction();
        }


//
//
//
//        if (listener != null) listener.onAfterAction();
//
//


//        if(Constants.DEBUG){
//            repeatBoardWriteAd = 2;
//            LogUtil.d("AdViewUtil, boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);
//        }


//        if(mustTry == false && ((boardEnterCount % repeatBoardWriteAd != 0) || SPUtil.getInstance().getAdFullScreen(context).equals("Y") == false)) {
//            if (listener != null) listener.onAfterAction();
//            return;
//        }
//
//        final Dialog dialogAd = ProgressDialogUtil.show(context, null);
//
//        final AdManager adManager = new AdManager();
//        adManager.ready(activity, context, isOnlyFull, new InterfaceSet.OnAd2CompleteListener() {
//            @Override
//            public void onAdLoadSuccess() {
//                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdLoadSuccess");
//
//                ProgressDialogUtil.dismiss(dialogAd);
//                adManager.showAd();
//            }
//
//            @Override
//            public void onAdLoadFail() {
//                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdLoadFail");
//                ProgressDialogUtil.dismiss(dialogAd);
//
//                if(adManager.showAd())
//                    return;
//
//                if (listener != null) listener.onAfterAction();
//            }
//
//            @Override
//            public void onAdPlayComplete(Constants.enum_ad enumAd) {
//                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdPlayComplete");
//                ProgressDialogUtil.dismiss(dialogAd);
//
//                if (listener != null) listener.onAfterAction();
//            }
//        });


    }


    private static void init(Activity activity, Context context) {
        boolean isOnlyFull = true;

        final AdManager adManager = new AdManager();
        adManager.ready(activity, context, isOnlyFull, new InterfaceSet.OnAd2CompleteListener() {
            @Override
            public void onAdLoadSuccess() {
                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdLoadSuccess");

                //ProgressDialogUtil.dismiss(dialogAd);
                //adManager.showAd();

                _adManager = adManager;
            }

            @Override
            public void onAdLoadFail() {
                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdLoadFail");
                //ProgressDialogUtil.dismiss(dialogAd);

                //if(adManager.showAd())
                //return;

                //if (listener != null) listener.onAfterAction();

                _adManager = null;
            }

            @Override
            public void onAdPlayComplete(Constants.enum_ad enumAd) {
                LogUtil.d("AdViewUtil, goActionAfterAdView, onAdPlayComplete");
                //ProgressDialogUtil.dismiss(dialogAd);

                ////if (listener != null) listener.onAfterAction();
            }
        });
    }


    public static void ready(Activity activity, Context context) {
        ready(activity, context, false);
    }

    public static void ready(Activity activity, Context context, boolean mustTry) {

        int boardEnterCount = SPUtil.getInstance().getBoardEnterCount(context);
        int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(context);


        LogUtil.d("AdViewUtil, boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);


        if (_adManager != null) {
            LogUtil.d("AdViewUtil, ready complete");
            return;
        }
        if (mustTry == false && ((boardEnterCount % repeatBoardWriteAd != 0) || SPUtil.getInstance().getAdFullScreen(context).equals("Y") == false)) {
            LogUtil.d("AdViewUtil, ready skip");
            return;
        }


        init(activity, context);
    }

}
