package com.code404.mytrot.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.view.GListView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;

public class VodBindUtil {
    private Context _context = null;
    private JSONArray _jsonVodList = null;
    private GListView.GListAdapter _adapter = null;

    public Context getContext() {
        return _context;
    }

    public View setViewEvent(Context context, final JSONObject json, JSONArray jsonVodList, View convertView, final int position, final GListView.GListAdapter adapter) {
        return setViewEvent(context, json, jsonVodList, convertView, position, adapter, -1, SPUtil.getInstance().getAdYoutube(getContext()).equals("Y") ? false : true);
    }

    public View setViewEvent(Context context, final JSONObject json, JSONArray jsonVodList, View convertView, final int position, final GListView.GListAdapter adapter, int vodIndex) {
        return setViewEvent(context, json, jsonVodList, convertView, position, adapter, vodIndex, SPUtil.getInstance().getAdYoutube(getContext()).equals("Y") ? false : true);
    }

    public View setViewEvent(Context context, final JSONObject json, JSONArray jsonVodList, View convertView, final int position, final GListView.GListAdapter adapter, int vodIndex, boolean isMustGone) {
        _context = context;
        _jsonVodList = jsonVodList;
        _adapter = adapter;

        String gb = JSONUtil.getString(json, "gb");
        String img01 = JSONUtil.getStringUrl(json, "img01");
        String vod_no_col_name = JSONUtil.getString(json, "vod_no_col_name");
        final String youtube_id = JSONUtil.getString(json, "youtube_id");
        final int vod_no = JSONUtil.getInteger(json, "vod_no".equals(vod_no_col_name) ? "vod_no" : "no");

        final int artist_no = JSONUtil.getInteger(json, "artist_no");
        int is_bookmark_temp = JSONUtil.getInteger(json, "is_bookmark", -1);

        LogUtil.d("vod_no_col_name : " + vod_no_col_name);
        LogUtil.d("artist_no : " + artist_no);
        LogUtil.d("vod_no : " + vod_no);
        LogUtil.d("is_bookmark_temp : " + is_bookmark_temp);
        LogUtil.d("img01 : " + img01);

        final int is_bookmark = is_bookmark_temp >= 0 ? is_bookmark_temp : 1;
        LogUtil.d("is_bookmark : " + is_bookmark);

        TextView txtType = (TextView) convertView.findViewById(R.id.txtType);
        TextView txtName = (TextView) convertView.findViewById(R.id.txtName);
        ImageView imgYoutube = (ImageView) convertView.findViewById(R.id.imgYoutube);
        TextView txtPlayCount = (TextView) convertView.findViewById(R.id.txtPlayCount);
        TextView txtPlayCountMy = (TextView) convertView.findViewById(R.id.txtPlayCountMy);
        TextView txtPlayMinute = (TextView) convertView.findViewById(R.id.txtPlayMinute);

        ImageButton btnHeart = (ImageButton) convertView.findViewById(R.id.btnHeart);
        View baseLine = (View) convertView.findViewById(R.id.baseLine);

        View adViewBody = convertView.findViewById(R.id.adViewBody);
        AdView adView = convertView.findViewById(R.id.adView);

        Button btnYoutube = convertView.findViewById(R.id.btnYoutube);


        if (isMustGone) {
            adViewBody.setVisibility(View.GONE);
        } else {
            if (Constants.DISPLAY_AD
                    && SPUtil.getInstance().getAdFullScreen(getContext()).equals("Y")
                    && !SPUtil.getInstance().getIsHaveAdRemoveItem(getContext())
                    && ((vodIndex < 0 && position % Constants.AD_UNIT == Constants.AD_UNIT_MOD)
                    || (vodIndex >= 0 && position == vodIndex))
            ) {
                adViewBody.setVisibility(View.VISIBLE);
                AdRequest adRequest = new AdRequest.Builder().build();
                adView.loadAd(adRequest);
            } else {
                adViewBody.setVisibility(View.GONE);
            }
        }


        btnHeart.setSelected(is_bookmark == 1);

        ViewConvertUtil.setVodTypeText(getContext(), txtType, gb);


        if (!FormatUtil.isNullorEmpty(img01)) {
            Picasso.with(getContext())
                    .load(img01)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgYoutube);
        }
        String title = JSONUtil.getString(json, "title");
        title = title.replace("&amp;", "&");
        txtName.setText(title);
        txtPlayCount.setText("총 재생 : " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "play_cnt")));
        txtPlayCountMy.setText("My 재생 : " + FormatUtil.toPriceFormat(JSONUtil.getString(json, "my_play_cnt")));
        txtPlayMinute.setText(FormatUtil.twoNumber(JSONUtil.getInteger(json, "vod_min")) + ":" + FormatUtil.twoNumber(JSONUtil.getInteger(json, "vod_sec")));

        btnHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SPUtil.getInstance().getIsLogin(getContext()) == false) {
                    new Alert().showAlert(getContext(), "로그인 이후에 이용할 수 있습니다.");
                    return;
                }


                if (is_bookmark == 0) {
                    callApi_memberBookmark_set(position, artist_no, vod_no);

                    int boardEnterCount = SPUtil.getInstance().getBoardEnterCount(context);
                    int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(context);
                    LogUtil.d("AdViewUtil, boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);
                    if(boardEnterCount % repeatBoardWriteAd == 0) {
                        Alert.toastLong(getContext(), "즐겨찾기에 추가되었습니다.");
                    }
                    else if(boardEnterCount % repeatBoardWriteAd == 1) {
                        Alert.toastLong(getContext(), "'더보기 > 영상보관함'에서 시청할 수 있습니다.");
                    }

                } else {
                    callApi_memberBookmark_del(position, artist_no, vod_no);
                }
            }
        });

        btnYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("http://youtu.be/" + youtube_id));

                    _context.startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                int boardEnterCount = SPUtil.getInstance().getBoardEnterCount(context);
                int repeatBoardWriteAd = SPUtil.getInstance().getRepeatBoardWriteAd(context);
                LogUtil.d("AdViewUtil, boardEnterCount : " + boardEnterCount + ", repeatBoardWriteAd : " + repeatBoardWriteAd);
                if(boardEnterCount % repeatBoardWriteAd == 0) {
                    Alert.toastLong(getContext(), "유튜브로 이동하여 재생합니다.");
                }

            }
        });

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String name = JSONUtil.getString(json, "name");
//
//                Intent i = new Intent();
//                i.setClass(getContext(), AtvVod.class);
//                i.putExtra(Constants.EXTRAS_VOD_LIST_JSON_STRING, _jsonVodList.toString());
//                i.putExtra(Constants.EXTRAS_VOD_NO, vod_no);
//                i.putExtra(Constants.EXTRAS_TITLE, name);
//                _context.startActivity(i);
//            }
//        });

        return convertView;
    }

    private void callApi_memberBookmark_set(final int position, int artist_no, int vod_no) {
        LogUtil.e("==========callApi_memberBookmark_set : start==========");

        setListBookmark(position, 1);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberBookmark_set(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , artist_no
                , vod_no
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_memberBookmark_del(final int position, int artist_no, int vod_no) {
        LogUtil.e("==========callApi_memberBookmark_set : start==========");

        setListBookmark(position, 0);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.memberBookmark_del(
                SPUtil.getInstance().getUserNoEnc(getContext())
                , artist_no
                , vod_no
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);

        call.enqueue(new CustomJsonHttpResponseHandler(getContext(), call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                if (resultCode != 0) {
                    new Alert().showAlert(getContext(), resultMessage);
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                //ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void setListBookmark(int position, int is_bookmark) {
        try {
            JSONObject json = _adapter.getItem(position);
            JSONUtil.puts(json, "is_bookmark", is_bookmark);
            _adapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
