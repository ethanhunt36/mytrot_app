package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.view.GMultiListView;
import com.code404.mytrot.view.SquareImageView;
import com.code404.mytrot.widget.ItemPayment;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;


public class AlertAgree extends Alert {


    private Context mContext;

    public static enum enum_agree_type {agree_ugc, agree_policy}

    public void showAgree(final Context context) {
        showAgree(context, enum_agree_type.agree_ugc);
    }

    public void showAgree(final Context context, enum_agree_type enumAgreeType) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_agree);

        WebView webView =  dialog.findViewById(R.id.webView);

        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = dialog.findViewById(R.id.btnConfirm);


        WebSettings settings = webView.getSettings();
        settings.setUseWideViewPort(true);
        settings.setSupportZoom(false);

        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setLoadWithOverviewMode(true);

        settings.setDomStorageEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        if (android.os.Build.VERSION.SDK_INT >= 11) {
//            initWeb_SDK11();
            WebSettings setting = webView.getSettings();

            webView.setMotionEventSplittingEnabled(true);

            // settings.setEnableSmoothTransition(true);
            setting.setAllowFileAccess(true);
            if (android.os.Build.VERSION.SDK_INT >= 16) {
                setting.setAllowFileAccessFromFileURLs(true);
            }
        }


        webView.setScrollbarFadingEnabled(true);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setVerticalScrollBarEnabled(true);


        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            webView.getSettings().setTextZoom(100);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        String url = Constants.getServerUrl() + "/agree/agree_ugc.html";

        if(enumAgreeType == enum_agree_type.agree_policy) {
            url = Constants.getServerUrl() + "/agree/agree_policy.html";
        }

        webView.loadUrl(url);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener  != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener  != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });


        dialog.setCancelable(false);
        dialog.show();
    }
}