package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.nativead.NativeAd;

import static com.google.android.gms.ads.formats.NativeAdOptions.ADCHOICES_TOP_LEFT;
import static com.google.android.gms.ads.formats.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_SQUARE;


public class AlertAd extends Alert {


    private Context mContext;

    private AdRequest _adRequest = null;

    private String _ad_id = "";
    private AdLoader _adLoader = null;
    private ImageButton _btnClose = null;
    private com.google.android.ads.nativetemplates.TemplateView _adNativeView = null;


    public void showAd(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_ad);

        _adNativeView = dialog.findViewById(R.id.adNativeView);
        _btnClose = dialog.findViewById(R.id.btnClose);
        _adNativeView.setVisibility(View.INVISIBLE);
        _btnClose.setVisibility(View.INVISIBLE);

        initAd();

        _btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(true);
        dialog.show();
    }

    private Context getContext() {
        return mContext;
    }

    private void initAd() {
//        if (false) {
//            _ad_id = "ca-app-pub-3940256099942544/2247696110";
//            _adRequest = new AdRequest.Builder()
//                    .addTestDevice("33BE2250B43518CCDA7DE426D04EE231")  // An example device ID
//                    .build();
//        } else {
        _ad_id = Constants.KEY_AD_NATIVE;
        _adRequest = new AdRequest.Builder()
                .build();
//        }

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
                LogUtil.d("MobileAds, onInitializationComplete");
            }
        });


        _adLoader = new AdLoader.Builder(getContext(), _ad_id)
                .forNativeAd(new NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(NativeAd nativeAd) {
                        LogUtil.d("MobileAds, onUnifiedNativeAdLoaded, _adLoader.isLoading() : " + _adLoader.isLoading());
                        // some code that displays the ad.

                        if (_adLoader.isLoading()) {
                            // The AdLoader is still loading ads.
                            // Expect more adLoaded or onAdFailedToLoad callbacks.

                            _adNativeView.setVisibility(View.VISIBLE);
                            _btnClose.setVisibility(View.VISIBLE);

                            try {
                                ColorDrawable c = new ColorDrawable();
                                c.setColor(getContext().getResources().getColor(R.color.white));
                                NativeTemplateStyle styles = new
                                        NativeTemplateStyle.Builder().withMainBackgroundColor(c).build();
                                _adNativeView.setStyles(styles);
                                _adNativeView.setNativeAd(nativeAd);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            // The AdLoader has finished loading ads.

                        }
                    }
                })
                .withNativeAdOptions(new com.google.android.gms.ads.nativead.NativeAdOptions.Builder()
                        .setReturnUrlsForImageAssets(false)
                        .setAdChoicesPlacement(ADCHOICES_TOP_LEFT)
                        .setMediaAspectRatio(NATIVE_MEDIA_ASPECT_RATIO_SQUARE)
                        .build()
                )
                .build();
        loadAd();
    }

    private void loadAd() {
        LogUtil.e("loadAd");
        LogUtil.e("loadAd, _adLoader : " + (_adLoader == null ? "null" : "ok"));
        LogUtil.e("loadAd, _adRequest : " + (_adRequest == null ? "null" : "ok"));

        _adLoader.loadAds(_adRequest, 2);
    }
}