package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.view.GListView;
import com.google.android.ads.nativetemplates.NativeTemplateStyle;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import org.json.JSONArray;
import org.json.JSONObject;

import static com.google.android.gms.ads.formats.NativeAdOptions.ADCHOICES_TOP_LEFT;
import static com.google.android.gms.ads.formats.NativeAdOptions.NATIVE_MEDIA_ASPECT_RATIO_SQUARE;


public class AlertSearch extends Alert {


    private Context mContext;

    private ImageButton _btnTopBack = null;
    private ImageButton _btnSearch = null;
    private EditText _edtKeyword = null;

    private GListView _list = null;


    private Dialog dialog = null;


    public void show(final Context context) {
        mContext = context;

        dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_search);

        _btnTopBack = dialog.findViewById(R.id.btnTopBack);
        _btnSearch = dialog.findViewById(R.id.btnSearch);
        _edtKeyword = dialog.findViewById(R.id.edtKeyword);

        _list = dialog.findViewById(R.id.list);

        _list.setVisibility(View.VISIBLE);
        _list.setViewMaker(R.layout.row_keyword, new GListView.IMakeView() {
            @Override
            public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

                JSONObject json = adapter.getItem(position);

                TextView txtKeyword01 = convertView.findViewById(R.id.txtKeyword01);
                TextView txtKeyword02 = convertView.findViewById(R.id.txtKeyword02);

                final String key01 = JSONUtil.getString(json, "key01");
                final String key02 = JSONUtil.getString(json, "key02");
                txtKeyword01.setText(key01);
                txtKeyword02.setText(key02);

                txtKeyword01.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_stringListener != null) {
                            _stringListener.onClose(dialog, Alert.BUTTON1, key01);
                        }
                        dialog.dismiss();
                    }
                });
                txtKeyword02.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (_stringListener != null) {
                            _stringListener.onClose(dialog, Alert.BUTTON1, key02);
                        }
                        dialog.dismiss();
                    }
                });

                return convertView;
            }
        });

        _edtKeyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        // 검색 동작
                        onSearch();
                        break;
                    default:
                        // 기본 엔터키 동작
                        return false;
                }
                return true;
            }
        });

        _btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onSearch();
            }
        });


        _btnTopBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.CANCEL, "");
                }
                dialog.dismiss();
            }
        });


        JSONArray keywordList = SPUtil.getInstance().getKeywordList(context);

        _list.addItems(keywordList);


        dialog.setCancelable(true);
        dialog.show();
    }

    private Context getContext() {
        return mContext;
    }


    private void onSearch() {
        String keyword = _edtKeyword.getText().toString();

        if (FormatUtil.isNullorEmpty(keyword)) {
            new Alert().showAlert(mContext, "검색 키워드를 입력해주세요.");
            return;
        }

        if (_stringListener != null) {
            _stringListener.onClose(dialog, Alert.BUTTON1, keyword);
        }
        dialog.dismiss();
    }
}