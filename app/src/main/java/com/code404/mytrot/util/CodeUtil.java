package com.code404.mytrot.util;

import com.code404.mytrot.bean.CodeBean;

import java.util.Random;

public class CodeUtil {

    public static CodeBean[] codeReportBoardArr = new CodeBean[]{
            new CodeBean(80, "사진/글 불량")
            , new CodeBean(81, "사진/글 광고")
            , new CodeBean(99, "기타")
    };

    public static String getSnsName(String login_type) {
        if (login_type.equals("E")) return "일반 이메일";
        if (login_type.equals("N")) return "네이버";
        if (login_type.equals("K")) return "카카오톡";
        if (login_type.equals("G")) return "구글";

        return "-";
    }

    /**
     * 신고 유형 텍스트 배열 - 밤스타
     *
     * @return
     */
    public static String[] getReportBoardCodeNameArr() {
        String[] codes = new String[codeReportBoardArr.length];

        for (int i = 0; i < codeReportBoardArr.length; i++) {
            codes[i] = codeReportBoardArr[i].codeName;
        }

        return codes;
    }

    public static String getReportBoardCodeName(int index) {
        try {
            return codeReportBoardArr[index].codeName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "-";
    }

    public static int getReportBoardCode(int index) {
        try {
            return codeReportBoardArr[index].code;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return -1;
    }

    public static String getCoupangUrl() {
        String[] arr = new String[]{
                "<a href='https://coupa.ng/bRLLax' target='_blank' referrerpolicy='unsafe-url'><img src='https://ads-partners.coupang.com/banners/441991?subId=&traceId=V0-301-879dd1202e5c73b2-I441991&w=320&h=50' alt=''></a>",
                "<a href='https://coupa.ng/bRLLeJ' target='_blank' referrerpolicy='unsafe-url'><img src='https://ads-partners.coupang.com/banners/442850?subId=&traceId=V0-301-969b06e95b87326d-I442850&w=320&h=50' alt=''></a>",
                "<a href='https://coupa.ng/bRLLgY' target='_blank' referrerpolicy='unsafe-url'><img src='https://ads-partners.coupang.com/banners/442233?subId=&traceId=V0-301-bae0f72e5e59e45f-I442233&w=320&h=50' alt=''></a>",
                "<a href='https://coupa.ng/bRLLkb' target='_blank' referrerpolicy='unsafe-url'><img src='https://ads-partners.coupang.com/banners/444199?subId=&traceId=V0-301-50c6c2b97fba9aee-I444199&w=320&h=50' alt=''></a>",
                "<a href='https://coupa.ng/bRLLlu' target='_blank' referrerpolicy='unsafe-url'><img src='https://ads-partners.coupang.com/banners/444202?subId=&traceId=V0-301-371ae01f4226dec2-I444202&w=320&h=50' alt=''></a>"
        };

        int r = new Random().nextInt(arr.length);

        LogUtil.d("getCoupangUrl : r : " + r);

        return arr[r];
    }
}
