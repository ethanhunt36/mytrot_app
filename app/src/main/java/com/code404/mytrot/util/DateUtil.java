package com.code404.mytrot.util;

import android.content.Context;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateUtil {




    private static long getDateBetweenDays(String data1, String date2) {

        long days = 0;

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            Date FirstDate = format.parse(data1);
            Date SecondDate = format.parse(date2);

            long calDate = FirstDate.getTime() - SecondDate.getTime();

            LogUtil.d("FrgStore, calDate : " + calDate);

            days = calDate / (24 * 60 * 60 * 1000);

            days = Math.abs(days);

            LogUtil.d("FrgStore, 두 날짜의 날짜 차이: " + days);

        } catch (ParseException e) {
            // 예외 처리
        }

        return days;
    }


    private static long getDateBetweenHours(String data1) {

        long hours = 0;

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Date FirstDate = format.parse(data1);
            Date SecondDate = new Date();

            long calDate = FirstDate.getTime() - SecondDate.getTime();

            LogUtil.d("FrgStore, calDate : " + calDate);

            hours = calDate / (60 * 60 * 1000);

            hours = Math.abs(hours);

            LogUtil.d("FrgStore, 두 날짜의 시간 차이: " + hours);

        } catch (ParseException e) {
            // 예외 처리
        }

        return hours;
    }
}