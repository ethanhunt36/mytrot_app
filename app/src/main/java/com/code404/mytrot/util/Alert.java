package com.code404.mytrot.util;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.code404.mytrot.common.InterfaceSet;


public class Alert {
    public final static int CANCEL = -1;
    public final static int BUTTON1 = 1;
    public final static int BUTTON2 = 2;
    public final static int BUTTON3 = 3;
    public final static int BUTTON4 = 4;

    protected static Toast mToast;
    protected static int mToastTime;

    private Dialog mDialog = null;


    public static void toastShort(Context context, String msg) {
        toast(context, msg, 1);
    }


    public static void toastShort(Context context, int msgRes) {
        String msg = context.getString(msgRes);
        toast(context, msg, 1);
    }


    public static void toastLong(Context context, String msg) {
        toast(context, msg, 2);
    }


    public static void toastLong(Context context, int msgRes) {
        String msg = context.getString(msgRes);
        toast(context, msg, 2);
    }


    private static void toast(Context context, String msg, int lengthType) {
        if (mToast == null) {
            switch (lengthType) {
                case 1:
                    mToastTime = Toast.LENGTH_SHORT;
                    break;

                case 2:
                    mToastTime = Toast.LENGTH_LONG;
                    break;
            }
            mToast = Toast.makeText(context.getApplicationContext(), msg, mToastTime);
            mToast.show();
            return;
        }
        mToast.setText(msg);
        mToast.show();
    }


    /**
     * 기본 얼럿 (확인버튼)
     *
     * @param message
     * @return
     */
    public Builder showAlert(Context context, String message) {
        return showAlert(context, null, message, true, "확인");
    }


    public Builder showAlert(Context context, String message, boolean cancelable) {
        return showAlert(context, null, message, cancelable, "확인");
    }


    /**
     * 일반 팝업 메시지만적용(버튼은 최대 두개까지)
     *
     * @param message
     * @return
     */
    public Builder showAlert(Context context, String message, boolean cancelable, String... btns) {
        return showAlert(context, null, message, cancelable, btns);
    }


    /**
     * 일반 팝업 메시지,타이틀적용(버튼은 최대 두개까지)
     */
    public Builder showAlert(Context context, String title, String message, final boolean cancelable, String... btns) {
//        mDialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
//        mDialog.setContentView(R.layout.alert_message);
//        mDialog.setCancelable(cancelable);
//
//        TextView txtMessage = (TextView) mDialog.findViewById(R.id.txtMessage);
//        txtMessage.setText(message);
//
//        final View baseAlertOut = mDialog.findViewById(R.id.baseAlertOut);
//        baseAlertOut.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//                if (cancelable)
//                {
//                    mDialog.dismiss();
//                }
//            }
//        });
//        mDialog.findViewById(R.id.btnConfirm).setOnClickListener(new View.OnClickListener()
//        {
//
//            @Override
//            public void onClick(View v)
//            {
//                mDialog.dismiss();
//                if (_closeListener != null) _closeListener.onClose(mDialog, BUTTON1, null);
//            }
//        });
//        mDialog.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener()
//        {
//
//            @Override
//            public void onClick(View v)
//            {
//                mDialog.dismiss();
//                if (_closeListener != null) _closeListener.onClose(mDialog, BUTTON2, null);
//            }
//        });
//
//        if (btns.length == 1)
//        {
//            mDialog.findViewById(R.id.baseSplit).setVisibility(View.GONE);
//            mDialog.findViewById(R.id.btnCancel).setVisibility(View.GONE);
//        }
//
//        if (btns.length > 0)
//        {
//            ((TextView) mDialog.findViewById(R.id.btnConfirm)).setText(btns[0]);
//        }
//
//        if (btns.length > 1)
//        {
//            ((TextView) mDialog.findViewById(R.id.btnCancel)).setText(btns[1]);
//        }
//
//        mDialog.show();
//
//        return null;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        try {

            builder.setCancelable(cancelable);
            builder.setTitle(title);
            builder.setMessage(message);
            builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                    if (_closeListener != null) _closeListener.onClose(dialog, CANCEL);
                }
            });
            if (btns != null) {
                if (btns.length == 2) {
                    builder.setPositiveButton(btns[0], new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (_closeListener != null) _closeListener.onClose(dialog, BUTTON1);
                        }
                    });
                    builder.setNegativeButton(btns[1], new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (_closeListener != null) _closeListener.onClose(dialog, BUTTON2);
                        }
                    });
                } else if (btns.length == 3) {
                    builder.setPositiveButton(btns[0], new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (_closeListener != null) _closeListener.onClose(dialog, BUTTON1);
                        }
                    });
                    builder.setNegativeButton(btns[1], new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (_closeListener != null) _closeListener.onClose(dialog, BUTTON2);
                        }
                    });
                    builder.setNeutralButton(btns[2], new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (_closeListener != null) _closeListener.onClose(dialog, BUTTON3);
                        }
                    });
                } else {
                    builder.setPositiveButton(btns[0], new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (_closeListener != null) _closeListener.onClose(dialog, BUTTON1);
                        }
                    });
                }
            }
            builder.setCancelable(cancelable);
            builder.show();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return builder;
    }


    public AlertDialog.Builder showSeletItem(Context context, String[] items) {
        AlertDialog.Builder builder = new Builder(context);
        builder.setCancelable(true);

        final AlertDialog alertDialog = builder.create();

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (_closeListener != null)
                    _closeListener.onClose(dialog, which);
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface dialog) {
                if (_closeListener != null)
                    _closeListener.onClose(dialog, CANCEL);
            }
        });
        builder.show();
        return builder;

    }


    protected InterfaceSet.OnCloseListener _closeListener = null;
    protected InterfaceSet.OnStringListener _stringListener = null;
    protected InterfaceSet.OnClickJsonListener _jsonListener = null;

    public void setOnCloseListener(InterfaceSet.OnCloseListener listener) {
        _closeListener = listener;
    }

    public void setOnStringListener(InterfaceSet.OnStringListener listener) {
        _stringListener = listener;
    }

    public void setOnClickJsonListener(InterfaceSet.OnClickJsonListener listener) {
        _jsonListener = listener;
    }
}