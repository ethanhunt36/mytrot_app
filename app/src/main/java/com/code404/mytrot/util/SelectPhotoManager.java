package com.code404.mytrot.util;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.code404.mytrot.R;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.UCropActivity;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SelectPhotoManager {
    private final String TAG = "SelectPhotoManager";
    public final static int PHOTO_SELECT_FAILED_BY_CRASH = 3000;
    public final static int PHOTO_SELECT_FAILED_BY_SIZE_LIMIT = 3001;

    private final String SDCARD_FOLDER = "Mytrot";

    private final int MAX_RESOLUTION = 800;
    private final int MAX_IMAGE_SIZE = 5120;

    private Activity mActivity = null;

    private static Uri mImageCaptureUri = null;
    private static Uri mCropImageUri = null;


    public final static int PICK_FROM_FILE = 2;

    private PhotoSelectCallback mCallback = null;
    private boolean m_isCrop = true;

    private boolean m_isCrop_Freestyle = false;


    public interface PhotoSelectCallback {
        void onSelectImageDone(Bitmap image, File file);

        void onFailedSelectImage(int errorCode, String err);

        void onDeleteImage();

        void onPermissionUpdated(int permission_type, boolean isGranted);
    }

    public void setPhotoSelectCallback(PhotoSelectCallback cb) {
        mCallback = cb;
    }

    public SelectPhotoManager() {
        // Exists only to defeat instantiation.
    }

    public SelectPhotoManager(Activity activity) {
        Log.d(TAG, "SelectPhotoManager");
        Log.d(TAG, activity.getClass().getSimpleName());
        mActivity = activity;
    }


    /**
     * 임시 파일 삭제
     */
    public void removeTempFile(Context context) {
        //File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), SDCARD_FOLDER);
        File folder = mActivity.getExternalFilesDir("/photos/");

        if(folder != null && !folder.exists()) {
            folder.mkdirs();
        }

        File[] childFileList = folder.listFiles();
        if (folder.exists()) {
            for (File childFile : childFileList) {
                if (childFile != null && childFile.isFile()) {

                    LogUtil.d("removeTempFile, childFile : " + childFile.getAbsolutePath());
                    childFile.delete();
                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(childFile)));
                }
            }
        }
        //dir.delete();
    }


    public void setIsCropFreestyle(boolean isCrop_Freestyle) {
        m_isCrop_Freestyle = isCrop_Freestyle;
    }

    public void setIsCrop(boolean isCrop) {
        m_isCrop = isCrop;
    }


    public void doPickFromGallery() {

        LogUtil.d("doPickFromGallery");

//        TedPermission.with(mActivity)
//                .setPermissionListener(new PermissionListener() {
//                    @Override
//                    public void onPermissionGranted() {
//
//                        LogUtil.d("onPermissionGranted");
//
//
//                        mCallback.onPermissionUpdated(1, true);
//
//                        Intent intent = new Intent(Intent.ACTION_PICK);
//                        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
//                        mActivity.startActivityForResult(intent, PICK_FROM_FILE);
//
//                    }
//
//                    @Override
//                    public void onPermissionDenied(List<String> deniedPermissions) {
//
//                        LogUtil.d("onPermissionDenied, " + deniedPermissions.toString());
//
//                        // Possibility of denied even if this app granted permissions
//                        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//                            mCallback.onPermissionUpdated(1, true);
//
//                            Intent intent = new Intent(Intent.ACTION_PICK);
//                            intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
//                            mActivity.startActivityForResult(intent, PICK_FROM_FILE);
//                        } else {
//                            mCallback.onPermissionUpdated(1, false);
//                        }
//                    }
//                })
//                .setDeniedMessage("접근 거부하셨습니다.\n[설정] - [권한]에서 권한을 허용해주세요.")
//                .setPermissions( new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE } )
//                .check();


        Intent intent = new Intent(Intent.ACTION_PICK);
        //intent.setType("image/*");
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);

//        if (DeliveryMan.photo_file_not_found_exception_count >= 2) {
//            intent.setPackage("com.sec.android.gallery3d");
//        }

        try {

            mActivity.startActivityForResult(intent, PICK_FROM_FILE);

        } catch (ActivityNotFoundException e) {

            intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            mActivity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FROM_FILE);
        }
    }

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        Log.d(TAG, "onActivityResult");
        Log.d(TAG, "requestCode : " + requestCode);
        Log.d(TAG, "resultCode : " + resultCode);
        Log.d(TAG, "data : " + data);

        Log.i(TAG, "m_resultCode = " + resultCode);
        Log.i(TAG, "m_Activity.RESULT_OK = " + Activity.RESULT_OK);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }

        onActivityResult2(requestCode, resultCode, data);
    }

    private void onActivityResult2(int requestCode, int resultCode, Intent data) {

        try {
            if (requestCode == UCrop.REQUEST_CROP) {

                if (mCropImageUri != null) {

                    BitmapFactory.Options onlyBoundsOptions = getBitmapFactoryFromUri(mCropImageUri);
                    if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
                        return;
                    }
                    double ratio = getRatio(onlyBoundsOptions);

                    Bitmap bitmap = getResizeBitmapFromUriWithRatio(mCropImageUri, ratio);
                    if (bitmap == null) {
                        return;
                    }

                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    LogUtil.d("SelectPhotoManager, crop ts : " + ts);
                    LogUtil.d("SelectPhotoManager, crop mImageCaptureUri : " + mImageCaptureUri);
                    String filename = "crop_" + ts + ".png";

                    //File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), SDCARD_FOLDER);
                    File folder = mActivity.getExternalFilesDir("/photos/");

                    if(folder != null && !folder.exists()) {
                        folder.mkdirs();
                    }

                    File file = new File(folder.toString(), filename);
                    Uri tempUri = Uri.fromFile(file);

                    // 크랍된 이미지가 회전 되서 나오는 경우가 있어서, 회전 해줍니다.
//				bitmap = checkOrientationAndRotate(bitmap, mCropImageUri.getPath());
//				storeCropImage(bitmap, mCropImageUri.getPath());
                    bitmap = checkOrientationAndRotate(bitmap, mCropImageUri.getPath());
                    storeCropImage(bitmap, tempUri.getPath());

//                try {
//                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), SDCARD_FOLDER);
//                    Uri contentUri = Uri.fromFile(f);
//                    mediaScanIntent.setData(contentUri);
//                    mActivity.sendBroadcast(mediaScanIntent);
//                } catch (Exception e) {
//
//                }

                    file = new File(mImageCaptureUri.getPath());
                    if (file.exists()) {
                        file.delete();
                    }
                }

            } else if (requestCode == PICK_FROM_FILE) {

                mImageCaptureUri = data.getData();
                Log.d(TAG, "requestCode == PICK_FROM_FILE");
                Log.d(TAG, "mImageCaptureUri : " + mImageCaptureUri);

                boolean go_without_crop = false;

                if (m_isCrop) {

                    try {
                        doCrop();
                    } catch (ActivityNotFoundException e) {
                        go_without_crop = true;
                    }
                } else {
                    go_without_crop = true;
                }

                if (go_without_crop) {
                    Long tsLong = System.currentTimeMillis() / 1000;
                    String ts = tsLong.toString();
                    LogUtil.d("SelectPhotoManager, pick ts : " + ts);
                    LogUtil.d("SelectPhotoManager, pick mImageCaptureUri : " + mImageCaptureUri);
                    String filename = "pick_" + ts + ".png";

                    //File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), SDCARD_FOLDER);
                    File folder = mActivity.getExternalFilesDir("/photos/");

                    if(folder != null && !folder.exists()) {
                        folder.mkdirs();
                    }
                    File file = new File(folder.toString(), filename);
                    Uri tempUri = Uri.fromFile(file);

                    BitmapFactory.Options onlyBoundsOptions = getBitmapFactoryFromUri(mImageCaptureUri);
                    if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
                        return;
                    }
                    double ratio = getRatio(onlyBoundsOptions);

                    Bitmap bitmap = getResizeBitmapFromUriWithRatio(mImageCaptureUri, ratio);
                    if (bitmap == null) {
                        return;
                    }

                    bitmap = checkOrientationAndRotate(bitmap, mImageCaptureUri.getPath());
                    storeCropImage(bitmap, tempUri.getPath());

//                try {
//                    Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), SDCARD_FOLDER);
//                    Uri contentUri = Uri.fromFile(f);
//                    mediaScanIntent.setData(contentUri);
//                    mActivity.sendBroadcast(mediaScanIntent);
//                } catch (Exception e) {
//
//                }

                    if (file.exists() && m_isCrop) {
                        file.delete();
                    }
                }


            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }


    public Bitmap getResizeBitmapFromUriWithRatio(Uri uri, double ratio) {
//		ratio = 1.0;
        InputStream input = null;
        Bitmap bitmap = null;
        try {
            input = mActivity.getContentResolver().openInputStream(uri);
            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

            bitmapOptions.inSampleSize = getPowerOfTwoForSampleRatio(ratio);


//			bitmapOptions.inDither = true;// optional
//			bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// optional
            bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
            input.close();

        } catch (FileNotFoundException e1) {

        } catch (IOException e) {

        }

        return bitmap;
    }

    public double getRatio(BitmapFactory.Options onlyBoundsOptions) {
        int originalSize = (onlyBoundsOptions.outHeight >= onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;
        double ratio = (originalSize > MAX_RESOLUTION) ? (originalSize / MAX_RESOLUTION) : 1.0;
        return ratio;
    }

    public BitmapFactory.Options getBitmapFactoryFromUri(Uri uri) {
        InputStream input = null;
        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        try {
            input = mActivity.getContentResolver().openInputStream(uri);
            onlyBoundsOptions.inJustDecodeBounds = true;
            onlyBoundsOptions.inDither = true;// optional
            onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// optional
            BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
            input.close();
        } catch (FileNotFoundException e1) {

        } catch (IOException e) {

        }

        return onlyBoundsOptions;
    }

    public Bitmap checkOrientationAndRotate(Bitmap bitmap, String filename) {
        ExifInterface ei = null;
        int orientation = -1;
        try {
            ei = new ExifInterface(filename);
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        } catch (IOException e) {

        }

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmap = rotateImage(bitmap, 90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmap = rotateImage(bitmap, 180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmap = rotateImage(bitmap, 270);
                break;
        }
        return bitmap;
    }

    private void doCrop() {

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String filename = ts + ".png";

        //File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), SDCARD_FOLDER);
        File folder = mActivity.getExternalFilesDir("/photos/");

        if(folder != null && !folder.exists()) {
            folder.mkdirs();
        }
        File file = new File(folder.toString(), filename);
        mCropImageUri = Uri.fromFile(file);

        UCrop uCrop = UCrop.of(mImageCaptureUri, mCropImageUri);


        UCrop.Options options = new UCrop.Options();
        if (m_isCrop_Freestyle) {
            options.setFreeStyleCropEnabled(true);
        } else {
            uCrop.withAspectRatio(1, 1);
        }
        options.setAllowedGestures(UCropActivity.NONE, UCropActivity.SCALE, UCropActivity.NONE);
//		options.setHideBottomControls(true);
        options.setStatusBarColor(Color.parseColor("#383f4b"));
//		options.setToolbarColor(Color.parseColor("#F70A1D"));
//		options.setToolbarTitle("사진 자르기");

        uCrop.withOptions(options);


        uCrop.start(mActivity);


    }

    private void storeCropImage(Bitmap bitmap, String filePath) {
        File copyFile = new File(filePath);
        BufferedOutputStream out;
        try {
            // Returns true if the named file does not exist and was successfully created, false if the named file already exists
            copyFile.createNewFile();
            out = new BufferedOutputStream(new FileOutputStream(copyFile));
            bitmap.compress(CompressFormat.JPEG, 100, out); // 2017-02-02 추가

            int file_size = Integer.parseInt(String.valueOf(copyFile.length() / 1024));

            if (file_size > MAX_IMAGE_SIZE) {
                mCallback.onFailedSelectImage(PHOTO_SELECT_FAILED_BY_SIZE_LIMIT, mActivity.getResources().getString(R.string.image_max_size_limit_msg));
                if (copyFile.exists()) {
                    copyFile.delete();
                }
            }

            out.flush();
            out.close();

            mCallback.onSelectImageDone(bitmap, copyFile);
        } catch (IOException e) {
            mCallback.onFailedSelectImage(PHOTO_SELECT_FAILED_BY_CRASH, e.toString());
            if (copyFile.exists()) {
                copyFile.delete();
            }
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private static int getPowerOfTwoForSampleRatio(double ratio) {
        int k = Integer.highestOneBit((int) Math.floor(ratio));
        k = k == 0 ? 1 : k;
        return k;
    }
}
