package com.code404.mytrot.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.view.GMultiListView;
import com.code404.mytrot.view.SquareImageView;
import com.code404.mytrot.widget.ItemPayment;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;


public class AlertAction extends Alert {


    private Context mContext;
    private ArrayList<ImageView> _imgList = new ArrayList<ImageView>();
    private TextView _txtMessage;

    public void showAlertAction(final Context context, String title, String msg, int imgResourceID) {
        mContext = context;

        Activity atv = (Activity) context;

        if (atv == null || atv.isFinishing()) {
            LogUtil.d("showAlertAction, atv is " + (atv == null ? "null" : atv.isFinishing()));
            return;
        }

        try {
            final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.setContentView(R.layout.alert_action);

            RelativeLayout baseParent = (RelativeLayout) dialog.findViewById(R.id.baseParent);
            TextView txtTitle = (TextView) dialog.findViewById(R.id.txtTitle);
            ImageView imgAction = (ImageView) dialog.findViewById(R.id.imgAction);
            TextView txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
            Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

            if (!FormatUtil.isNullorEmpty(title)) {
                txtTitle.setText(title);
            }

            txtMessage.setText(msg);
            imgAction.setImageResource(imgResourceID);

            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (_closeListener != null) _closeListener.onClose(dialog, Alert.BUTTON1);

                    dialog.dismiss();
                }
            });


            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showAlertActionHeart(final Context context) {
        mContext = context;

        Activity atv = (Activity) context;

        if (atv == null || atv.isFinishing()) {
            LogUtil.d("showAlertAction, atv is " + (atv == null ? "null" : atv.isFinishing()));
            return;
        }

        try {
            final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
            dialog.setContentView(R.layout.alert_action_heart);

            RelativeLayout baseParent = (RelativeLayout) dialog.findViewById(R.id.baseParent);
            ImageView imgAction = (ImageView) dialog.findViewById(R.id.imgAction);
            ImageView imgHeart01 = (ImageView) dialog.findViewById(R.id.imgHeart01);
            ImageView imgHeart02 = (ImageView) dialog.findViewById(R.id.imgHeart02);
            ImageView imgHeart03 = (ImageView) dialog.findViewById(R.id.imgHeart03);
            ImageView imgHeart04 = (ImageView) dialog.findViewById(R.id.imgHeart04);
            ImageView imgHeart05 = (ImageView) dialog.findViewById(R.id.imgHeart05);
            ImageView imgHeart06 = (ImageView) dialog.findViewById(R.id.imgHeart06);
            ImageView imgHeart07 = (ImageView) dialog.findViewById(R.id.imgHeart07);
            ImageView imgHeart08 = (ImageView) dialog.findViewById(R.id.imgHeart08);
            ImageView imgHeart09 = (ImageView) dialog.findViewById(R.id.imgHeart09);
            ImageView imgHeart10 = (ImageView) dialog.findViewById(R.id.imgHeart10);
            _txtMessage = (TextView) dialog.findViewById(R.id.txtMessage);
            Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

            _imgList = new ArrayList<ImageView>();

            _imgList.add(imgHeart01);
            _imgList.add(imgHeart02);
            _imgList.add(imgHeart03);
            _imgList.add(imgHeart04);
            _imgList.add(imgHeart05);
            _imgList.add(imgHeart06);
            _imgList.add(imgHeart07);
            _imgList.add(imgHeart08);
            _imgList.add(imgHeart09);
            _imgList.add(imgHeart10);

            for (int i = 0; i < _imgList.size(); i++) {
                _imgList.get(i).setVisibility(View.INVISIBLE);
            }

            btnConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (_closeListener != null) _closeListener.onClose(dialog, Alert.BUTTON1);

                    dialog.dismiss();
                }
            });

            dialog.setCancelable(true);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setHeartStart(int heart_cnt, String msg) {

        LogUtil.d("_imgList.size() : " + _imgList.size());

        int interval = 100;

        _txtMessage.setText("과연 하트 몇개가 나올까요?");

        // 순서대로 점등 on
        Handler handler = new Handler();
        for (int i = 0; i < _imgList.size(); i++) {
            final int intFinal = i;

            interval += 80;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    _imgList.get(intFinal).setVisibility(View.VISIBLE);
                }
            }, interval);
        }

        // 순서대로 점등 off
        interval += 100;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < _imgList.size(); i++) {
                    _imgList.get(i).setVisibility(View.INVISIBLE);
                }
            }
        }, interval);

        // 순서대로 점등 on
        for (int i = 0; i < _imgList.size(); i++) {
            final int intFinal = i;

            interval += 80;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    _imgList.get(intFinal).setVisibility(View.VISIBLE);
                }
            }, interval);
        }

        // 순서대로 점등 off
        interval += 100;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < _imgList.size(); i++) {
                    _imgList.get(i).setVisibility(View.INVISIBLE);
                }
            }
        }, interval);


        // 최종갯수대로 on
        for (int i = 0; i < heart_cnt; i++) {
            final int intFinal = i;

            interval += 80;
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    _imgList.get(intFinal).setVisibility(View.VISIBLE);
                }
            }, interval);
        }

        interval += 200;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                _txtMessage.setText(Html.fromHtml("<font color='#d4364f'>하트 " + heart_cnt + "개</font>가 적립 되었습니다."));
            }
        }, interval);


    }
}