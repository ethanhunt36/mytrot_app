package com.code404.mytrot.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabWidget;
import android.widget.TextView;

import com.code404.mytrot.R;
import com.code404.mytrot.common.Constants;
import com.code404.mytrot.common.InterfaceSet;
import com.code404.mytrot.network.APIClient;
import com.code404.mytrot.network.APIInterface;
import com.code404.mytrot.network.CustomJsonHttpResponseHandler;
import com.code404.mytrot.view.GListView;
import com.code404.mytrot.view.GMultiListView;
import com.code404.mytrot.view.SquareImageView;
import com.code404.mytrot.widget.ItemPayment;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;


public class AlertPop extends Alert {


    private Context mContext;

    //중복 클릭 방지 시간 설정 ( 해당 시간 이후에 다시 클릭 가능 )
    private long mLastClickTime = 0;

    /**
     * 투료권 자동 충전권
     */
    public void showAutoTicketPayment(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_auto_ticket);

        RelativeLayout baseParent = (RelativeLayout) dialog.findViewById(R.id.baseParent);

        LinearLayout basePayment = dialog.findViewById(R.id.basePayment);
        basePayment.removeAllViews();

        callApi_product_auto_vote_ticket_list(dialog, basePayment);

        dialog.findViewById(R.id.btnClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_jsonListener != null) {
                    _jsonListener.onClick(null, -1, null);
                }
                dialog.dismiss();
            }
        });


        dialog.setCancelable(false);
        dialog.show();
    }


    /**
     * 개발자 후원
     */
    public void showPayment(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_payment);

        RelativeLayout baseParent = (RelativeLayout) dialog.findViewById(R.id.baseParent);

        LinearLayout basePayment = dialog.findViewById(R.id.basePayment);
        basePayment.removeAllViews();

        callApi_product_get_all(dialog, basePayment);

        dialog.findViewById(R.id.btnClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_jsonListener != null) {
                    _jsonListener.onClick(null, -1, null);
                }
                dialog.dismiss();
            }
        });


        dialog.setCancelable(false);
        dialog.show();
    }


    private int _artist_no = -1;
    private int _event_fund_no = -1;
    private TextView _txtArtist = null;
    private TextView _txtTip03 = null;
    private ImageButton _btnCheck = null;

    /**
     * 응원글 작성
     *
     * @param context
     */
    public void showWriteBoard(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_write_board);

        RelativeLayout baseArtist = (RelativeLayout) dialog.findViewById(R.id.baseArtist);
        _txtArtist = (TextView) dialog.findViewById(R.id.txtArtist);
        final EditText edtContents = (EditText) dialog.findViewById(R.id.edtContents);
        final EditText edtNick = dialog.findViewById(R.id.edtNick);
        _txtTip03 = dialog.findViewById(R.id.txtTip03);
        _btnCheck = dialog.findViewById(R.id.btnCheck);
        final View baseTip = dialog.findViewById(R.id.baseTip);
        final ScrollView scrollView = dialog.findViewById(R.id.scrollView);


        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

        JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
        String nick = JSONUtil.getString(json, "nick");
        int artist_no = JSONUtil.getInteger(json, "artist_no");
        String artist_name = JSONUtil.getString(json, "artist_name");

        if (!FormatUtil.isNullorEmpty(nick)) {
            edtNick.setText(nick);
            edtNick.setEnabled(false);
        }

        // 최애가 있으면 가수 자동 선택
        if (artist_no > 0 && !FormatUtil.isNullorEmpty(artist_name)) {
            _artist_no = artist_no;
            _txtArtist.setText(artist_name);
        }

        if (SPUtil.getInstance().getIsAgreeWriteBoard(mContext)) {
            _btnCheck.setSelected(true);
        }

        edtContents.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                //if (b) {

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            scrollView.scrollTo(0, 2000);
                        } catch (Exception e) {
                        }
                    }
                }, 500);

                //}
            }
        });

        edtContents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            scrollView.scrollTo(0, 2000);
                        } catch (Exception e) {
                        }
                    }
                }, 500);
            }
        });
        _btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertAgree alert = new AlertAgree();
                alert.showAgree(mContext);
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            _btnCheck.setSelected(true);
                        } else {
                            _btnCheck.setSelected(false);
                        }
                    }
                });
            }
        });

        _txtTip03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertAgree alert = new AlertAgree();
                alert.showAgree(mContext);
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            _btnCheck.setSelected(true);
                        } else {
                            _btnCheck.setSelected(false);
                        }
                    }
                });
            }
        });


        // 응원글 가수 선택
        baseArtist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showVoteArtist(mContext);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_artist_no < 1) {
                    new Alert().showAlert(mContext, "가수를 선택해 주세요.");
                    return;
                }

                String nick = edtNick.getText().toString();
                String contents = edtContents.getText().toString();

                if (FormatUtil.isNullorEmpty(nick)) {
                    new Alert().showAlert(mContext, "닉네임을 입력해 주세요.");
                    return;
                }

                if (FormatUtil.isNullorEmpty(contents)) {
                    new Alert().showAlert(mContext, "응원글 내용을 입력해 주세요.");
                    return;
                }

                if (_btnCheck.isSelected() == false) {
                    new Alert().showAlert(mContext, "사용자 제작 콘텐츠(UGC) 약관에 동의해주세요.");
                    return;
                }


                SPUtil.getInstance().setIsAgreeWriteBoard(mContext, true);

                callApi_board_ins(_artist_no, nick, contents, dialog);

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String contents = edtContents.getText().toString();

                if (!FormatUtil.isNullorEmpty(contents)) {
                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialogAlert, int which) {
                            if (which == Alert.BUTTON1) {
                                dialog.dismiss();
                                if (_stringListener != null) {
                                    _stringListener.onClose(dialog, Alert.CANCEL, "");
                                }
                            }
                        }
                    });
                    alert.showAlert(mContext, "입력 중인 내용이 있습니다.\n\n취소 하시겠습니까?", true, "확인", "아니오, 계속 입력합니다.");
                    return;
                }

                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.CANCEL, null);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    GMultiListView _multiList = null;
    Dialog _dialogVoteArtist = null;

    public void showVoteArtist(Context context) {
        showVoteArtist(context, false, "", true);
    }

    private String _sort = "";
    private View _baseNoData;

    public void showVoteArtist(Context context, boolean isCancelable, String title, boolean isShowCheck) {

        mContext = context;
        _dialogVoteArtist = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        _dialogVoteArtist.setContentView(R.layout.alert_vote_artist);

        ImageButton btnClose = _dialogVoteArtist.findViewById(R.id.btnClose);
        btnClose.setVisibility(isCancelable ? View.VISIBLE : View.GONE);
//        AdView adView = _dialogVoteArtist.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);

        TextView txtTopTitle = _dialogVoteArtist.findViewById(R.id.txtTopTitle);
        _multiList = _dialogVoteArtist.findViewById(R.id.multiList);
        _baseNoData = _dialogVoteArtist.findViewById(R.id.baseNoData);

        View header = InflateUtil.inflate(mContext, R.layout.header_artist_popup, null);

        final View baseCheckFavorite = header.findViewById(R.id.baseCheckFavorite);
        final View baseSeq = header.findViewById(R.id.baseSeq);

        final Button btnCheckFavorite = header.findViewById(R.id.btnCheckFavorite);
        final TextView txtSeq = header.findViewById(R.id.txtSeq);
        final TextView txtSeqName = header.findViewById(R.id.txtSeqName);
        final EditText edtTitle = header.findViewById(R.id.edtTitle);
        Button btnSearch = header.findViewById(R.id.btnSearch);

        boolean checked = SPUtil.getInstance().getCheckedFavoritePopupArtist(mContext);

        btnCheckFavorite.setSelected(checked);
        txtSeq.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));

        if (!FormatUtil.isNullorEmpty(title)) {
            txtTopTitle.setText(title);
        }
        _sort = "sortno";

        baseCheckFavorite.setVisibility(isShowCheck ? View.VISIBLE : View.GONE);

        _multiList.addHeaderView(header);
        _multiList.setRowItemCounts(3);
        _multiList.setViewMaker(R.layout.row_vote_artist, new GMultiListView.IMakeView() {
            @Override
            public View makeView(GMultiListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

                final ArrayList<JSONObject> list = adapter.getItem(position);

                SquareImageView img01 = convertView.findViewById(R.id.img01);
                SquareImageView img02 = convertView.findViewById(R.id.img02);
                SquareImageView img03 = convertView.findViewById(R.id.img03);

                TextView txtName01 = convertView.findViewById(R.id.txtName01);
                TextView txtName02 = convertView.findViewById(R.id.txtName02);
                TextView txtName03 = convertView.findViewById(R.id.txtName03);

                JSONObject info01 = list.size() > 0 ? list.get(0) : null;
                JSONObject info02 = list.size() > 1 ? list.get(1) : null;
                JSONObject info03 = list.size() > 2 ? list.get(2) : null;

                setViewBinding(img01, txtName01, info01);
                setViewBinding(img02, txtName02, info02);
                setViewBinding(img03, txtName03, info03);
                return convertView;
            }
        });


        callApi_artist_vote_listing(checked, true, "");

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String srch_text = edtTitle.getText().toString();

                callApi_artist_vote_listing(checked, false, srch_text);
            }
        });

        btnCheckFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnCheckFavorite.setSelected(!btnCheckFavorite.isSelected());

                boolean checked = btnCheckFavorite.isSelected();

                SPUtil.getInstance().setCheckedFavoritePopupArtist(mContext, checked);

                JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                int artist_no = JSONUtil.getInteger(json, "artist_no");

                if (artist_no < 1) {
                    new Alert().showAlert(mContext, mContext.getString(R.string.txt_set_artist));
                    return;
                }

                callApi_artist_vote_listing(checked, true);
            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_dialogVoteArtist != null) {
                    _dialogVoteArtist.dismiss();
                    _dialogVoteArtist = null;
                }

                if (_jsonListener != null) {
                    _jsonListener.onClick(null, -1, null);
                }
            }
        });

        txtSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean checked = btnCheckFavorite.isSelected();
                _sort = "sortno";
                txtSeq.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));
                txtSeqName.setBackground(null);

                String srch_text = edtTitle.getText().toString();
                callApi_artist_vote_listing(checked, false, srch_text);
            }
        });
        txtSeqName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean checked = btnCheckFavorite.isSelected();
                _sort = "name";
                txtSeq.setBackground(null);
                txtSeqName.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));

                String srch_text = edtTitle.getText().toString();
                callApi_artist_vote_listing(checked, false, srch_text);
            }
        });

        edtTitle.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    boolean checked = btnCheckFavorite.isSelected();
                    String srch_text = edtTitle.getText().toString();
                    callApi_artist_vote_listing(checked, false, srch_text);
                }
                return false;
            }
        });

        _dialogVoteArtist.setCancelable(isCancelable);
        _dialogVoteArtist.show();
    }

    private void setViewBinding(SquareImageView img, TextView txt, final JSONObject info) {
        String url = JSONUtil.getStringUrl(info, "pic");
        String name = JSONUtil.getString(info, "name");

        txt.setText(name);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_txtArtist != null) {
                    _artist_no = JSONUtil.getInteger(info, "no");
                    String name = JSONUtil.getString(info, "name");
                    _txtArtist.setText(name);
                }

                if (_dialogVoteArtist != null) {
                    _dialogVoteArtist.dismiss();
                    _dialogVoteArtist = null;
                }

                if (_jsonListener != null) {
                    _jsonListener.onClick(null, -1, info);
                }

            }
        });

        if (FormatUtil.isNullorEmpty(url)) {
            img.setVisibility(View.INVISIBLE);
            return;
        }

        // 모서리 라운딩 처리
        img.setBackground(mContext.getDrawable(R.drawable._s_background_rounding));
        img.setClipToOutline(true);

        Picasso.with(mContext)
                .load(url)
                .fit()
                .placeholder(R.drawable.img_noimg)
                .into(img);

        img.setVisibility(View.VISIBLE);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_txtArtist != null) {
                    _artist_no = JSONUtil.getInteger(info, "no");
                    String name = JSONUtil.getString(info, "name");
                    _txtArtist.setText(name);
                }

                if (_dialogVoteArtist != null) {
                    _dialogVoteArtist.dismiss();
                    _dialogVoteArtist = null;
                }

                if (_jsonListener != null) {
                    _jsonListener.onClick(null, -1, info);
                }

            }
        });

    }


    private void callApi_product_get_all(final Dialog dialog, final LinearLayout basePayment) {

        LogUtil.e("==========callApi_product_get_all : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.product_get_all(
                SPUtil.getInstance().getUserNoEnc(mContext)
        );

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);


                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

                JSONArray list = JSONUtil.getJSONArray(json, "data");

                for (int i = 0; i < list.length(); i++) {
                    final int idx = i;
                    final JSONObject j = JSONUtil.getJSONObject(list, i);

                    String product_type = JSONUtil.getString(j, "product_type");

                    if (product_type.equals("H")) {
                        ItemPayment item = new ItemPayment(mContext);
                        item.setData(j);
                        item.setOnClickViewListener(new InterfaceSet.OnClickJsonListener() {
                            @Override
                            public void onClick(View v, int pos, JSONObject json) {
                                if (_jsonListener != null) {
                                    _jsonListener.onClick(null, idx, j);
                                }
                                dialog.dismiss();
                            }
                        });
                        basePayment.addView(item);
                    }
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_product_auto_vote_ticket_list(final Dialog dialog, final LinearLayout basePayment) {

        LogUtil.e("==========callApi_product_auto_vote_ticket_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.product_get_all(
                SPUtil.getInstance().getUserNoEnc(mContext)
        );

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);


                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

                JSONArray list = JSONUtil.getJSONArray(json, "data");

                for (int i = 0; i < list.length(); i++) {
                    final int idx = i;
                    final JSONObject j = JSONUtil.getJSONObject(list, i);

                    String sku = JSONUtil.getString(j, "sku");

                    if (sku.startsWith("auto_ticket")) {
                        ItemPayment item = new ItemPayment(mContext);
                        item.setData(j);
                        item.setOnClickViewListener(new InterfaceSet.OnClickJsonListener() {
                            @Override
                            public void onClick(View v, int pos, JSONObject json) {
                                if (_jsonListener != null) {
                                    _jsonListener.onClick(null, idx, j);
                                }
                                dialog.dismiss();
                            }
                        });
                        basePayment.addView(item);
                    }
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_artist_vote_listing(final boolean checkFaviteArtist, boolean isCache) {
        callApi_artist_vote_listing(checkFaviteArtist, isCache, "");
    }

    private void callApi_artist_vote_listing(final boolean checkFaviteArtist, boolean isCache, String srch_text) {
        LogUtil.e("==========callApi_artist_listing : start==========");

        String date = SPUtil.getInstance().getArtistListDate(mContext);
        final String currentDate = FormatUtil.getCurrentDateAndHour();
        JSONArray jsonArray = SPUtil.getInstance().getArtistList(mContext);

        LogUtil.d("callApi_artist_vote_listing, _sort : " + _sort);
        LogUtil.d("callApi_artist_vote_listing, date : " + date);
        LogUtil.d("callApi_artist_vote_listing, currentDate : " + currentDate);
        LogUtil.d("callApi_artist_vote_listing, jsonArray : " + jsonArray);

        String srch_type = "";


        if (!FormatUtil.isNullorEmpty(srch_text)) {
            srch_type = "name";
        }

        if ((!date.equals(currentDate)
                || (jsonArray == null || jsonArray.length() < 1))
                || isCache == false) {


            LogUtil.d("callApi_artist_vote_listing, api call");

            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

            Call<JsonObject> call = apiInterface.artist_vote_listing(
                    SPUtil.getInstance().getUserNoEnc(mContext)
                    , Constants.RECORD_SIZE_MAX
                    , -1
                    , _sort
                    , ""
                    , "N"
                    , srch_type
                    , srch_text
            );

            final Dialog dialog = ProgressDialogUtil.show(mContext, null);
            call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
                @Override
                public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                    super.onResponse(resultCode, resultMessage, json);

                    try {
                        _multiList.removeAll();

                        ProgressDialogUtil.dismiss(dialog);

                        LogUtil.d("resultCode : " + resultCode);
                        LogUtil.d("resultMessage : " + resultMessage);
                        LogUtil.json(json);

                        JSONObject jsonData = JSONUtil.getJSONObject(json, "data");
                        JSONArray jsonList = JSONUtil.getJSONArray(jsonData, "list_data");


                        // 인기순일 때만, 검색키워드가 없을때만 로컬에 저장합니다.
                        if ((FormatUtil.isNullorEmpty(_sort) || "sortno".equals(_sort)) && FormatUtil.isNullorEmpty(srch_text)) {
                            SPUtil.getInstance().setArtistListDate(mContext, currentDate);
                            SPUtil.getInstance().setArtistList(mContext, jsonList);
                        }

                        // 최애 가수 설정 되어 있는지 체크해서 재정렬
                        jsonList = reOrderAritst(checkFaviteArtist, jsonList);

                        _multiList.removeAll();
                        _multiList.addItems(jsonList);

//                        if(_baseNoData != null) {
                        _baseNoData.setVisibility(jsonList.length() > 0 ? View.GONE : View.VISIBLE);
                        if (jsonList.length() < 1)
                            _multiList.setBackgroundColor(0x00000000);
                        else
                            _multiList.setBackgroundResource(R.color.default_bg);
                        //_multiList.setVisibility(jsonList.length() > 0 ? View.VISIBLE : View.GONE);
//                        }
                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    super.onFailure(call, t);
                    ProgressDialogUtil.dismiss(dialog);
                    LogUtil.d("onFailure");
                }
            });
        } else {
            LogUtil.d("callApi_artist_vote_listing, use cache");

            // 최애 가수 설정 되어 있는지 체크해서 재정렬
            jsonArray = reOrderAritst(checkFaviteArtist, jsonArray);
            _multiList.removeAll();
            _multiList.addItems(jsonArray);
        }
    }


    private JSONArray reOrderAritst(boolean checkFaviteArtist, JSONArray jsonList) {
        JSONObject jsonUser = SPUtil.getInstance().getUserInfo(mContext);
        int artist_no = JSONUtil.getInteger(jsonUser, "artist_no");

        if (checkFaviteArtist && artist_no > 0) {
            // 최애 맨위로.
            List<JSONObject> list = new ArrayList<JSONObject>();

            for (int i = 0; i < jsonList.length(); i++) {
                JSONObject j = JSONUtil.getJSONObject(jsonList, i);
                int no = JSONUtil.getInteger(j, "no");
                list.add(j);

                if (artist_no == no) {
                    list.remove(i);
                    list.add(0, j);
                }
            }

            jsonList = new JSONArray();

            for (int i = 0; i < list.size(); i++) {
                try {
                    jsonList.put(i, list.get(i));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return jsonList;
    }


    private void callApi_board_ins(int _artist_no, final String nick, String cont, final Dialog dialog) {
        LogUtil.e("==========callApi_board_ins : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.board_ins(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , _artist_no
                , nick
                , cont
        );

        final Dialog dialog2 = ProgressDialogUtil.show(mContext, null, false);
        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog2);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (!FormatUtil.isNullorEmpty(resultMessage)) {
                    new Alert().showAlert(mContext, resultMessage);
                }

                if (resultCode == 0) {

                    SPUtil.getInstance().setBoardNick(mContext, nick);

                    if (_closeListener != null) {
                        _closeListener.onClose(dialog, Alert.BUTTON1);
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog2);
                LogUtil.d("onFailure");
            }
        });
    }


    public void showYoutudePolicy(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_youtube_msg);

        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    Dialog _dialogAuthPhone = null;
    EditText _edtPhoneAuth = null;

    /**
     * 핸드폰 번호 변경
     *
     * @param context
     */
    public void showAuthPhone(final Context context) {
        mContext = context;

        _dialogAuthPhone = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        _dialogAuthPhone.setContentView(R.layout.alert_auth_phone);

        final EditText edtPhone = (EditText) _dialogAuthPhone.findViewById(R.id.edtPhone);
        _edtPhoneAuth = (EditText) _dialogAuthPhone.findViewById(R.id.edtPhoneAuth);

        View baseAuth = _dialogAuthPhone.findViewById(R.id.baseAuth);

        Button btnAuth = (Button) _dialogAuthPhone.findViewById(R.id.btnAuth);
        Button btnAuthNumber = (Button) _dialogAuthPhone.findViewById(R.id.btnAuthNumber);

        Button btnCancel = (Button) _dialogAuthPhone.findViewById(R.id.btnCancel);

        final ImageButton btnCheck = _dialogAuthPhone.findViewById(R.id.btnCheck);

//        AdView adView = _dialogAuthPhone.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);
        baseAuth.setVisibility(View.GONE);

        if (Constants.IS_ONE_STORE) {
            baseAuth.setVisibility(View.VISIBLE);
        }

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertAgree alert = new AlertAgree();
                alert.showAgree(context, AlertAgree.enum_agree_type.agree_policy);
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (which == Alert.BUTTON1) {
                            btnCheck.setSelected(true);
                        } else {
                            btnCheck.setSelected(false);
                        }
                    }
                });
            }
        });

        btnAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                String phone1 = JSONUtil.getString(json, "phone");

                String phone = edtPhone.getText().toString();

                if (FormatUtil.isNullorEmpty(phone)) {
                    new Alert().showAlert(mContext, "핸드폰 번호를 입력해주세요.");
                    return;
                }

                if (phone.length() < 11) {
                    new Alert().showAlert(mContext, "핸드폰 번호가 유효하지 않습니다.");
                    return;
                }

                if (phone.equals(phone1)) {
                    new Alert().showAlert(mContext, "현재 등록된 번호와 동일합니다.");
                    return;
                }

                if (Constants.IS_ONE_STORE && btnCheck.isSelected() == false) {
                    new Alert().showAlert(mContext, "개인정보 수집, 활용에 대한 약관에 동의 바랍니다.");
                    return;
                }

                callApi_member_send_auth_pin(phone);
            }
        });

        btnAuthNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phone = edtPhone.getText().toString();
                String phoneAuth = _edtPhoneAuth.getText().toString();

                if (FormatUtil.isNullorEmpty(phoneAuth)) {
                    new Alert().showAlert(mContext, "인증 번호를 입력해주세요.");
                    return;
                }

                if (phoneAuth.length() < 6) {
                    new Alert().showAlert(mContext, "인증 번호가 유효하지 않습니다.");
                    return;
                }

                callApi_member_verify_auth_pin(phone, phoneAuth);
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(_dialogAuthPhone, Alert.CANCEL);
                }
                _dialogAuthPhone.dismiss();
            }
        });

        _dialogAuthPhone.setCancelable(false);
        _dialogAuthPhone.show();
    }


    /**
     * 기부
     *
     * @param context
     */
    public void showDonate(final Context context, int artist_no, String artist_name, String pic_url, int donate_nth, int pointTarget, int pointCurrent) {
        mContext = context;

        _artist_no = artist_no;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_donate);

        CircleImageView imgProfile = dialog.findViewById(R.id.imgProfile);
        TextView txtHintDonate = dialog.findViewById(R.id.txtHintDonate);
        TextView txtMyPoint = dialog.findViewById(R.id.txtMyPoint);

        TextView txtDonateNth = dialog.findViewById(R.id.txtDonateNth);
        TextView txtPointTarget = dialog.findViewById(R.id.txtPointTarget);
        TextView txtCurrentPoint = dialog.findViewById(R.id.txtCurrentPoint);

        final Button btnCheck = dialog.findViewById(R.id.btnCheck);

        final EditText edtDonate = dialog.findViewById(R.id.edtDonate);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

//        AdView adView = dialog.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);

        String txt = "<b>[NAME]</b> 님 이름으로<br>기부할 포인트를 입력해주세요.";
        txt = txt.replace("[NAME]", artist_name);
        txtHintDonate.setText(Html.fromHtml(txt));

        JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
        double point = JSONUtil.getDouble(json, "point");

        if (!FormatUtil.isNullorEmpty(pic_url)) {
            Picasso.with(mContext)
                    .load(pic_url)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

        txtDonateNth.setText(donate_nth + "차 목표");
        txtMyPoint.setText(FormatUtil.toPriceFormat(point) + " P");
        txtPointTarget.setText(FormatUtil.toPriceFormat(pointTarget) + " P");
        txtCurrentPoint.setText(FormatUtil.toPriceFormat(pointCurrent) + " P");

        callApi_member_get_by_no(txtMyPoint, "point");

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCheck.setSelected(!btnCheck.isSelected());

                if (btnCheck.isSelected()) {
                    JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                    double point = JSONUtil.getDouble(json, "point");
                    int pointInt = (int) point;
                    edtDonate.setText(String.valueOf(pointInt));
                } else {
                    edtDonate.setText("");
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonUtil.checkDoubleTab()) {
                    LogUtil.e("btnConfirm, 이벤트 중복 발생");
                    return;
                }

                if (_artist_no < 1) {
                    new Alert().showAlert(mContext, "가수를 선택해 주세요.");
                    return;
                }

                String donate = edtDonate.getText().toString();

                if (FormatUtil.isNullorEmpty(donate)) {
                    new Alert().showAlert(mContext, "기부 포인트를 입력해 주세요.");
                    return;
                }

                long donateInt = Long.parseLong(donate);
                JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                double point = JSONUtil.getDouble(json, "point");

                if (donateInt < 10) {
                    new Alert().showAlert(mContext, "10P 부터 기부할 수 있습니다.");
                    return;
                }

                if (point < donateInt) {
                    new Alert().showAlert(mContext, "보유하고 있는 " + FormatUtil.toPriceFormat(point) + "P 까지 기부할 수 있습니다.");
                    return;
                }


                String msg = String.format("%s 님 기부펀딩에\n%sP를 사용 하시겠습니까?", artist_name, FormatUtil.toPriceFormat(donateInt));

                int myFavoriteArtistNo = SPUtil.getInstance().getUserFavoriteArtistNo(mContext);

                if (_artist_no != myFavoriteArtistNo && myFavoriteArtistNo > 0) {
                    String myFavoriteArtistName = SPUtil.getInstance().getUserFavoriteArtistName(mContext);

                    msg = String.format("회원님의 최애 가수님은 %s 님이세요.\n\n그래도 ", myFavoriteArtistName) + msg;
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialogAlert, int which) {

                        if (which == Alert.BUTTON1) {
                            callApi_donate_donate(dialog, _artist_no, donateInt);
                        }
                    }
                });
                alert.showAlert(mContext, msg, false, "확인", "취소");


            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public void showEventFund(final Context context, int event_fund_no, int artist_no, String title, String pic_url, int heartTarget, int heartCurrent) {
        mContext = context;

        _event_fund_no = event_fund_no;
        _artist_no = artist_no;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_event_fund);

        CircleImageView imgProfile = dialog.findViewById(R.id.imgProfile);
        TextView txtTitle = dialog.findViewById(R.id.txtTitle);
        TextView txtHintDonate = dialog.findViewById(R.id.txtHintDonate);
        TextView txtMyPoint = dialog.findViewById(R.id.txtMyPoint);

        TextView txtDonateNth = dialog.findViewById(R.id.txtDonateNth);
        TextView txtPointTarget = dialog.findViewById(R.id.txtPointTarget);
        TextView txtCurrentPoint = dialog.findViewById(R.id.txtCurrentPoint);

        final Button btnCheck = dialog.findViewById(R.id.btnCheck);

        final EditText edtDonate = dialog.findViewById(R.id.edtDonate);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

//        AdView adView = dialog.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);

//        String txt = "<b>[NAME]</b> 님 이름으로<br>기부할 포인트를 입력해주세요.";
//        txt = txt.replace("[NAME]", artist_name);
//        txtHintDonate.setText(Html.fromHtml(txt));

        JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
        double heart = JSONUtil.getDouble(json, "heart");

        //txtDonateNth.setText(donate_nth + "차 목표");

        if (!FormatUtil.isNullorEmpty(pic_url)) {
            Picasso.with(mContext)
                    .load(pic_url)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
        }

        txtTitle.setText(title);
        txtMyPoint.setText(FormatUtil.toPriceFormat(heart) + " 개");
        txtPointTarget.setText(FormatUtil.toPriceFormat(heartTarget) + " 개");
        txtCurrentPoint.setText(FormatUtil.toPriceFormat(heartCurrent) + " 개");

        callApi_member_get_by_no(txtMyPoint, "heart");

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCheck.setSelected(!btnCheck.isSelected());

                if (btnCheck.isSelected()) {
                    JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                    double heart = JSONUtil.getDouble(json, "heart");
                    int heartInt = (int) heart;
                    edtDonate.setText(String.valueOf(heartInt));
                } else {
                    edtDonate.setText("");
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonUtil.checkDoubleTab()) {
                    LogUtil.e("btnConfirm, 이벤트 중복 발생");
                    return;
                }

                if (_event_fund_no < 1) {
                    new Alert().showAlert(mContext, "광고펀딩을 다시 선택해 주세요.");
                    return;
                }

                String donate = edtDonate.getText().toString();

                if (FormatUtil.isNullorEmpty(donate)) {
                    new Alert().showAlert(mContext, "사용할 하트를 입력해 주세요.");
                    return;
                }

                long donateInt = Long.parseLong(donate);
                JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                double heart = JSONUtil.getDouble(json, "heart");

                if (donateInt < 1) {
                    new Alert().showAlert(mContext, "사용할 하트를 입력해 주세요.");
                    return;
                }

                if (heart < donateInt) {
                    new Alert().showAlert(mContext, "보유하고 있는 하트 " + FormatUtil.toPriceFormat(heart) + "개 까지 사용할 수 있습니다.");
                    return;
                }


                String msg = String.format("%s\n광고펀딩에\n하트 %s개를 사용 하시겠습니까?", title, FormatUtil.toPriceFormat(donateInt));

                int myFavoriteArtistNo = SPUtil.getInstance().getUserFavoriteArtistNo(mContext);

                if (_artist_no != myFavoriteArtistNo && myFavoriteArtistNo > 0) {
                    String myFavoriteArtistName = SPUtil.getInstance().getUserFavoriteArtistName(mContext);

                    msg = String.format("회원님의 최애 가수님은 %s 님이세요.\n\n그래도 ", myFavoriteArtistName) + msg;
                }


                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialogAlert, int which) {

                        if (which == Alert.BUTTON1) {
                            callApi_eventFund_fund(dialog, _event_fund_no, donateInt);
                        }
                    }
                });
                alert.showAlert(mContext, msg, false, "확인", "취소");


            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * 투표
     *
     * @param context
     */
    public void showVote(final Context context, int artist_no, String artist_name, String pic_url
            , int vote_cnt_month, int my_vote_count) {
        mContext = context;

        _artist_no = artist_no;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_vote);

        CircleImageView imgProfile = dialog.findViewById(R.id.imgProfile);
        TextView txtHintDonate = dialog.findViewById(R.id.txtHintDonate);
        TextView txtMyPoint = dialog.findViewById(R.id.txtMyPoint);
        TextView txtArtistVote = dialog.findViewById(R.id.txtArtistVote);
        TextView txtMyVote = dialog.findViewById(R.id.txtMyVote);


        TextView txtCurrentPoint = dialog.findViewById(R.id.txtCurrentPoint);

        final Button btnCheck = dialog.findViewById(R.id.btnCheck);

        final EditText edtDonate = dialog.findViewById(R.id.edtDonate);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

        String txt = "<b>[NAME]</b> 님에게 투표하세요.";
        txt = txt.replace("[NAME]", artist_name);
        txtHintDonate.setText(Html.fromHtml(txt));

        JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
        double vote_cnt = JSONUtil.getDouble(json, "vote_cnt");

        if (!FormatUtil.isNullorEmpty(pic_url)) {
            Picasso.with(context)
                    .load(pic_url)
                    .fit()
                    .placeholder(R.drawable.img_noimg)
                    .into(imgProfile);
            imgProfile.setVisibility(View.VISIBLE);
        } else {
            imgProfile.setVisibility(View.GONE);
        }

        txtMyPoint.setText(FormatUtil.toPriceFormat(vote_cnt) + " 장");
        txtArtistVote.setText(FormatUtil.toPriceFormat(vote_cnt_month) + " 장");
        txtMyVote.setText(FormatUtil.toPriceFormat(my_vote_count) + " 장");

        callApi_member_get_by_no(txtMyPoint, "vote_cnt");

        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnCheck.setSelected(!btnCheck.isSelected());

                if (btnCheck.isSelected()) {
                    JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                    int vote_cnt = JSONUtil.getInteger(json, "vote_cnt");
                    edtDonate.setText(String.valueOf(vote_cnt));
                } else {
                    edtDonate.setText("");
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (CommonUtil.checkDoubleTab()) {
                    LogUtil.e("btnConfirm, 이벤트 중복 발생");
                    return;
                }

                if (_artist_no < 1) {
                    new Alert().showAlert(mContext, "가수를 선택해 주세요.");
                    return;
                }

                String vote_input = edtDonate.getText().toString();

                if (FormatUtil.isNullorEmpty(vote_input)) {
                    new Alert().showAlert(mContext, "투표권을 입력해 주세요.");
                    return;
                }

                long voteInt = Long.parseLong(vote_input);
                JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
                int vote_cnt = JSONUtil.getInteger(json, "vote_cnt");

                if (voteInt < 1) {
                    new Alert().showAlert(mContext, "1장 부터 투표 할 수 있습니다.");
                    return;
                }

                if (vote_cnt < voteInt) {
                    new Alert().showAlert(mContext, "보유하고 있는 " + FormatUtil.toPriceFormat(vote_cnt) + "장 까지 투표할 수 있습니다.");
                    return;
                }

                String msg = String.format("%s 님에게 투표권 %s장을 투표하시겠습니까?", artist_name, FormatUtil.toPriceFormat(voteInt));

                int myFavoriteArtistNo = SPUtil.getInstance().getUserFavoriteArtistNo(mContext);

                if (_artist_no != myFavoriteArtistNo && myFavoriteArtistNo > 0) {
                    String myFavoriteArtistName = SPUtil.getInstance().getUserFavoriteArtistName(mContext);

                    msg = String.format("회원님의 최애 가수님은 %s 님이세요.\n\n그래도 ", myFavoriteArtistName) + msg;
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialogAlert, int which) {

                        if (which == Alert.BUTTON1) {
                            if (_stringListener != null) {
                                _stringListener.onClose(dialog, Alert.BUTTON1, String.valueOf(voteInt));
                            }
                            dialog.dismiss();
                        }
                    }
                });
                alert.showAlert(mContext, msg, false, "확인", "취소");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.CANCEL, "");
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    /**
     * 닉네임 변경
     *
     * @param context
     */
    public void showChangeNick(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_change_nick);

        final EditText edtNick = dialog.findViewById(R.id.edtNick);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

//        AdView adView = dialog.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);

        JSONObject json = SPUtil.getInstance().getUserInfo(mContext);
        String nick = JSONUtil.getString(json, "nick");

        if (!FormatUtil.isNullorEmpty(nick)) {
            edtNick.setText(nick);
        }

        edtNick.setFilters(new InputFilter[]{filterEmoji});
        edtNick.addTextChangedListener(new NicknameTextWatcher(edtNick));

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nick = edtNick.getText().toString();

                if (FormatUtil.isNullorEmpty(nick)) {
                    new Alert().showAlert(mContext, "닉네임을 입력해주세요.");
                    return;
                }

                callApi_member_set_nick(nick, dialog);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.CANCEL, null);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public void showArtistKeyword(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_artist_keyword);

        final EditText edtEtc = dialog.findViewById(R.id.edtEtc);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyword = edtEtc.getText().toString();

                if (FormatUtil.isNullorEmpty(keyword) || keyword.length() > 15) {
                    new Alert().showAlert(mContext, "키워드를 15자 이내로 입력해주세요.");
                    return;
                }

                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.BUTTON1, keyword);
                }
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.CANCEL, null);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    /**
     * 추천인 코드 등록
     *
     * @param context
     */
    public void showInputRcmd(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_input_rcmd);

        final EditText edtNick = dialog.findViewById(R.id.edtNick);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);

        JSONObject json = SPUtil.getInstance().getUserInfo(mContext);


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rcmd_code = edtNick.getText().toString();

                if (FormatUtil.isNullorEmpty(rcmd_code)) {
                    new Alert().showAlert(mContext, "추천인 코드를 입력해주세요.");
                    return;
                }

                if (rcmd_code.length() < 6) {
                    new Alert().showAlert(mContext, "추천인 코드는 6자리입니다.");
                    return;
                }

                callApi_member_put_rcmd_code(rcmd_code, dialog);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_stringListener != null) {
                    _stringListener.onClose(dialog, Alert.CANCEL, null);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    /**
     * 이모티콘 사용 제한
     */
    protected InputFilter filterEmoji = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                // 이모티콘 패턴
                Pattern unicodeOutliers = Pattern.compile("[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]+");
                // '-' 입력 받고 싶을 경우 : unicodeOutliers.matcher(source).matches() && !source.toString().matches(".*-.*")
                // if(unicodeOutliers.matcher(source).matches()) {
                if (unicodeOutliers.matcher(source).matches() && !source.toString().matches(".*-.*")) {
                    new Alert().showAlert(mContext, "이모티콘은 입력할 수 없습니다.");
                    return "";
                }
            }
            return null;
        }
    };

    private class NicknameTextWatcher implements TextWatcher {
        EditText et;
        String beforeText;

        public NicknameTextWatcher(EditText et) {
            this.et = et;
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() == 0)
                return;

            // 비밀번호 형식 체크
            Pattern p = Pattern.compile("^[a-zA-Z0-9가-힣ㄱ-ㅎㅏ-ㅣ\\u318D\\u119E\\u11A2\\u2022\\u2025a\\u00B7\\uFE55]+$");
            Matcher m = p.matcher(s.toString());
            if (m.find()) {

            } else {

                et.setText(beforeText);

                if ((et.getText().length() > 0 && beforeText.length() > 0) && (et.getText().length() >= beforeText.length() - 1)) {
                    et.setSelection(beforeText.length() - 1);
                }
                new Alert().showAlert(mContext, "기호는 입력할 수 없습니다.");
                return;
            }

            if (s.toString().length() > 10) {

                try {
                    et.setText(beforeText);
                    et.setSelection(et.getText().toString().length());

                } catch (Exception e) {

                }

                new Alert().showAlert(mContext, "닉네임은 10자 이하로 입력바랍니다.");
            }
        }


        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // TODO Auto-generated method stub
            beforeText = s.toString();
        }


        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // TODO Auto-generated method stub
        }

    }


    private void callApi_member_put_rcmd_code(String rcmd_code, final Dialog dialogAlert) {
        LogUtil.e("==========callApi_member_put_rcmd_code : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_put_rcmd_code(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , rcmd_code
        );
        final Dialog dialog = ProgressDialogUtil.show(mContext, null);


        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                if (resultCode != 0) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

                if (FormatUtil.isNullorEmpty(resultMessage)) {
                    resultMessage = "등록 되었습니다.";
                }

                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (_closeListener != null) {
                            _closeListener.onClose(dialog, Alert.BUTTON1);
                        }

                        if (dialogAlert != null) {
                            dialogAlert.dismiss();
                        }
                    }
                });
                alert.showAlert(mContext, resultMessage);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_get_by_no(final TextView txt, final String key) {

        LogUtil.e("==========callApi_member_get_by_no : start==========");


        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_get_by_no(
                SPUtil.getInstance().getUserNoEnc(mContext)
        );
        //final Dialog dialog = ProgressDialogUtil.show(getContext(), null);
        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                //ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                JSONObject userInfo = JSONUtil.getJSONObject(json, "data");

                if (resultCode == 0) {
                    try {
                        SPUtil.getInstance().setUserInfo(mContext, userInfo);

                        if (key.equals("point")) {
                            double point = JSONUtil.getDouble(userInfo, "point");
                            txt.setText(FormatUtil.toPriceFormat(point) + " P");
                        } else if (key.equals("vote_cnt")) {
                            double vote_cnt = JSONUtil.getDouble(userInfo, "vote_cnt");
                            txt.setText(FormatUtil.toPriceFormat(vote_cnt) + " 장");
                        } else if (key.equals("heart")) {
                            double heart = JSONUtil.getDouble(userInfo, "heart");
                            txt.setText(FormatUtil.toPriceFormat(heart) + " 개");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                // ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_donate_donate(final Dialog dialog, int artist_no, long point) {

        LogUtil.e("==========callApi_donate_donate : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.donate_donate(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , artist_no
                , point
        );
        final Dialog dialog2 = ProgressDialogUtil.show(mContext, null, false);
        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog2);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);
//                Alert alert = new Alert();
//                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
//                    @Override
//                    public void onClose(DialogInterface dialogAlert, int which) {
//                        if (dialog != null) {
//                            dialog.dismiss();
//                        }
//
//                        if (_closeListener != null) {
//                            _closeListener.onClose(dialog, Alert.BUTTON1);
//                        }
//                    }
//                });
//                alert.showAlert(mContext, resultMessage);

                AlertAction alert = new AlertAction();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialogAlert, int which) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        if (_closeListener != null) {
                            _closeListener.onClose(dialog, Alert.BUTTON1);
                        }
                    }
                });
                String title = "기부 펀딩 완료";
                String msg = resultMessage;
                int imgResourceID = FormatUtil.getCurrentMinute() % 2 == 0 ? R.drawable.img_pop_donate_1 : R.drawable.img_pop_donate_2;
                alert.showAlertAction(mContext, title, msg, imgResourceID);


                JSONObject userInfo = SPUtil.getInstance().getUserInfo(mContext);
                double f_point = JSONUtil.getDouble(userInfo, "point");
                f_point = f_point - point;

                JSONUtil.puts(userInfo, "point", f_point);
                SPUtil.getInstance().setUserInfo(mContext, userInfo);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog2);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_eventFund_fund(final Dialog dialog, int event_fund_no, long heart) {

        LogUtil.e("==========callApi_eventFund_fund : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.eventFund_fund(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , event_fund_no
                , heart
        );
        final Dialog dialog2 = ProgressDialogUtil.show(mContext, null, false);
        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog2);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                AlertAction alert = new AlertAction();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialogAlert, int which) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }

                        if (_closeListener != null) {
                            _closeListener.onClose(dialog, Alert.BUTTON1);
                        }
                    }
                });
                String title = "광고펀딩 완료";
                String msg = resultMessage;
                int imgResourceID = FormatUtil.getCurrentMinute() % 2 == 0 ? R.drawable.img_pop_donate_1 : R.drawable.img_pop_donate_2;
                alert.showAlertAction(mContext, title, msg, imgResourceID);


                JSONObject userInfo = SPUtil.getInstance().getUserInfo(mContext);
                long f_heart = JSONUtil.getLong(userInfo, "heart");
                f_heart = f_heart - heart;

                JSONUtil.puts(userInfo, "heart", f_heart);
                SPUtil.getInstance().setUserInfo(mContext, userInfo);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog2);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_set_nick(final String nick, final Dialog dialog) {
        LogUtil.e("==========callApi_member_set_nick : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_set_nick(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , nick
        );

        final Dialog dialog2 = ProgressDialogUtil.show(mContext, null);
        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, final String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog2);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

                if (resultCode == 0) {
                    // 닉네임 로컬 데이터 변경
                    JSONObject jsomMember = SPUtil.getInstance().getUserInfo(mContext);
                    JSONUtil.puts(jsomMember, "nick", nick);
                    SPUtil.getInstance().setUserInfo(mContext, jsomMember);

                    Alert alert = new Alert();
                    alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                        @Override
                        public void onClose(DialogInterface dialogAlert, int which) {
                            if (_closeListener != null) {
                                _closeListener.onClose(dialog, Alert.BUTTON1);
                            }

                            if (dialog != null) {
                                dialog.dismiss();
                            }
                        }
                    });
                    alert.showAlert(mContext, "변경되었습니다.");
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog2);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_member_verify_auth_pin(final String phone, String pin) {

        LogUtil.e("==========callApi_member_verify_auth_pin : start==========");

        final Dialog dialog = ProgressDialogUtil.show(mContext, null);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_verify_auth_pin(
                phone
                , pin
                , Constants.APP_DEVICE_ID
        );

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                    new Alert().showAlert(mContext, resultMessage);
                    _edtPhoneAuth.setText("");
                    return;
                }

                if (resultCode == 0) {
                    JSONObject jsonData = JSONUtil.getJSONObject(json, "data");

                    if (jsonData == null) {
                        callApi_member_choice(phone);
                    } else {
                        // 기존 인증되어 있던 회원 정보로 업데이트
                        JSONObject old_member_info = JSONUtil.getJSONObject(jsonData, "old_member_info");

                        JSONObject userInfo_current = SPUtil.getInstance().getUserInfo(mContext);

                        String nick_old = JSONUtil.getString(old_member_info, "nick");
                        String nick_current = JSONUtil.getString(userInfo_current, "nick");


                        String msg = "핸드폰번호 '%s'으로 발견된 기존계정(%s)이 있습니다.\n\n'%s'에서 '%s'으로 계정을 이동하시겠습니까?";
                        msg += "\n\n취소를 누르면 핸드폰 인증도 취소됩니다.";
                        msg += "\n\n기존 계정 대신 현재 계정에 핸드폰 등록을 원하시면, 문의하기에 문의글을 남겨주세요.";
                        msg += "\n\n감사합니다.";
                        msg = String.format(msg, phone, nick_old, nick_current, nick_old);

                        Alert alert = new Alert();
                        alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                            @Override
                            public void onClose(DialogInterface dialog, int which) {
                                if (which == Alert.BUTTON1) {
                                    SPUtil.getInstance().setUserInfo(mContext, old_member_info);
                                    callApi_member_choice(phone);
                                }
                            }
                        });
                        alert.showAlert(mContext, msg, false, "확인", "취소");


                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_member_send_auth_pin(String phone) {

        LogUtil.e("==========callApi_member_send_auth_pin : start==========");

        final Dialog dialog = ProgressDialogUtil.show(mContext, null);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_send_auth_pin(
                phone
        );

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (!FormatUtil.isNullorEmpty(resultMessage)) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }

    private void callApi_member_choice(String phone) {

        LogUtil.e("==========callApi_member_choice : start==========");

        final Dialog dialog = ProgressDialogUtil.show(mContext, null);

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.member_choice(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , phone
                , Constants.APP_DEVICE_ID
                , CommonUtil.getCurrentVersion(mContext)
                , SPUtil.getInstance().getRegistPushKey(mContext)
        );

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0 && !FormatUtil.isNullorEmpty(resultMessage)) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

                if (_closeListener != null) {
                    _closeListener.onClose(_dialogAuthPhone, Alert.CANCEL);
                }
                _dialogAuthPhone.dismiss();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private String _favorite_yn = "";
    private TextView _txtSeq = null;
    private TextView _txtSeqName = null;
    private TextView _txtSeqFavorite = null;

    public void showSnsList(Context context) {

        mContext = context;
        _dialogVoteArtist = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        _dialogVoteArtist.setContentView(R.layout.alert_sns_list);

        ImageButton btnClose = _dialogVoteArtist.findViewById(R.id.btnClose);
//        btnClose.setVisibility(isCancelable ? View.VISIBLE : View.GONE);
//        AdView adView = _dialogVoteArtist.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        adView.loadAd(adRequest);

        TextView txtTopTitle = _dialogVoteArtist.findViewById(R.id.txtTopTitle);
        GListView list = _dialogVoteArtist.findViewById(R.id.list);
        _baseNoData = _dialogVoteArtist.findViewById(R.id.baseNoData);
        Button btnWrite = _dialogVoteArtist.findViewById(R.id.btnWrite);

        View header = InflateUtil.inflate(mContext, R.layout.header_sns_list_popup, null);


        _txtSeq = header.findViewById(R.id.txtSeq);
        _txtSeqName = header.findViewById(R.id.txtSeqName);
        _txtSeqFavorite = header.findViewById(R.id.txtSeqFavorite);

        final EditText edtTitle = header.findViewById(R.id.edtTitle);
        Button btnSearch = header.findViewById(R.id.btnSearch);


        list.addHeaderView(header);
        list.setViewMaker(R.layout.row_sns, new GListView.IMakeView() {
            @Override
            public View makeView(GListView.GListAdapter adapter, int position, View convertView, ViewGroup parent) {

                JSONObject jsonRow = adapter.getItem(position);

                final int sns_no = JSONUtil.getInteger(jsonRow, "no");
                final String favorite_yn = JSONUtil.getString(jsonRow, "favorite_yn");

                TextView txtSns = (TextView) convertView.findViewById(R.id.txtSns);
                TextView txtUrl = (TextView) convertView.findViewById(R.id.txtUrl);
                TextView txtPoint = (TextView) convertView.findViewById(R.id.txtPoint);
                ImageButton btnHeart = convertView.findViewById(R.id.btnHeart);

                txtSns.setText(JSONUtil.getString(jsonRow, "sns_name"));
                txtUrl.setText(JSONUtil.getString(jsonRow, "sns_url"));
                txtPoint.setText(JSONUtil.getInteger(jsonRow, "order_no", 0) + "건");
                btnHeart.setSelected(favorite_yn.equals("Y"));

                btnHeart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String favorite_yn_str = favorite_yn.equals("N") ? "Y" : "N";

                        callApi_snsList_set_favorite(sns_no, favorite_yn_str);

                        JSONUtil.puts(jsonRow, "favorite_yn", favorite_yn_str);
                        adapter.notifyDataSetChanged();
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (_dialogVoteArtist != null) {
                            _dialogVoteArtist.dismiss();
                            _dialogVoteArtist = null;
                        }

                        if (_jsonListener != null) {
                            _jsonListener.onClick(null, position, jsonRow);
                        }
                    }
                });


                return convertView;
            }
        });

        _sort = "order_no";
        _txtSeq.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));
        _txtSeqName.setBackground(null);
        _txtSeqFavorite.setBackground(null);

        callApi_snsList_get_list(list, "", _sort, "");

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String srch_text = edtTitle.getText().toString();

                callApi_snsList_get_list(list, srch_text, _sort, _favorite_yn);
            }
        });


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_dialogVoteArtist != null) {
                    _dialogVoteArtist.dismiss();
                    _dialogVoteArtist = null;
                }

                if (_jsonListener != null) {
                    _jsonListener.onClick(null, -1, null);
                }


            }
        });

        _txtSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _sort = "order_no";
                _favorite_yn = "";
                _txtSeq.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqName.setBackground(null);
                _txtSeqFavorite.setBackground(null);
                String srch_text = edtTitle.getText().toString();
                callApi_snsList_get_list(list, srch_text, _sort, "");
            }
        });
        _txtSeqName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _sort = "sns_name";
                _favorite_yn = "";
                _txtSeq.setBackground(null);
                _txtSeqName.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));
                _txtSeqFavorite.setBackground(null);
                String srch_text = edtTitle.getText().toString();
                callApi_snsList_get_list(list, srch_text, _sort, "");
            }
        });

        _txtSeqFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _sort = "sns_name";
                _favorite_yn = "Y";
                _txtSeq.setBackground(null);
                _txtSeqName.setBackground(null);
                _txtSeqFavorite.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));
                String srch_text = edtTitle.getText().toString();
                callApi_snsList_get_list(list, srch_text, _sort, _favorite_yn);
            }
        });

        btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_dialogVoteArtist != null) {
                    _dialogVoteArtist.dismiss();
                    _dialogVoteArtist = null;
                }

                if (_jsonListener != null) {
                    _jsonListener.onClick(null, -10, null);
                }
            }
        });

        _dialogVoteArtist.setCancelable(false);
        _dialogVoteArtist.show();
    }


    public void showReqRegistSns(Context context) {

        mContext = context;
        final Dialog dialog = new Dialog(mContext, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_req_sns);

        EditText edtSns = (EditText) dialog.findViewById(R.id.edtSns);
        EditText edtUrl = (EditText) dialog.findViewById(R.id.edtUrl);
        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null) {
                    dialog.dismiss();
                }

                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sns = edtSns.getText().toString();
                String url = edtUrl.getText().toString();

                if (FormatUtil.isNullorEmpty(sns)) {
                    new Alert().showAlert(mContext, "커뮤니티 이름을 입력해주세요.");
                    return;
                }
                if (FormatUtil.isNullorEmpty(url)) {
                    new Alert().showAlert(mContext, "커뮤니티 웹주소(URL)를 입력해주세요.");
                    return;
                }

                callApi_snsList_ins(dialog, sns, url);
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    private boolean _isFirstCallSnsList = true;

    private void callApi_snsList_get_list(GListView gList, String srch_text, String sort, String favorite_yn) {

        LogUtil.e("==========callApi_snsList_get_list : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.snsList_get_list(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , srch_text
                , sort
                , favorite_yn
                , -1
                , Constants.RECORD_SIZE_MAX
        );

        final Dialog dialog = ProgressDialogUtil.show(mContext, null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);

                gList.removeAll();

                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

                JSONObject data = JSONUtil.getJSONObject(json, "data");

                JSONArray list = JSONUtil.getJSONArray(data, "list_data");


                if (_isFirstCallSnsList) {
                    JSONArray favList = new JSONArray();

                    for (int i = 0; i < list.length(); i++) {
                        JSONObject j = JSONUtil.getJSONObject(list, i);

                        String favorite_yn = JSONUtil.getString(j, "favorite_yn");

                        if (favorite_yn.equals("Y")) {
                            favList.put(j);
                        }
                    }

                    if (favList.length() > 0) {
                        list = favList;
                        _favorite_yn = "Y";
                        _txtSeq.setBackground(null);
                        _txtSeqName.setBackground(null);
                        _txtSeqFavorite.setBackground(mContext.getDrawable(R.drawable.bg_typebox_ent));
                    }

                    _isFirstCallSnsList = false;
                }


                gList.addItems(list);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_snsList_set_favorite(int sns_no, String favorite_yn) {

        LogUtil.e("==========callApi_snsList_set_favorite : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.snsList_set_favorite(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , sns_no
                , favorite_yn
        );

        final Dialog dialog = ProgressDialogUtil.show(mContext, null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);


                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private void callApi_snsList_ins(Dialog reqDialog, String sns_name, String sns_url) {

        LogUtil.e("==========callApi_snsList_ins : start==========");

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<JsonObject> call = apiInterface.snsList_ins(
                SPUtil.getInstance().getUserNoEnc(mContext)
                , sns_name
                , sns_url
        );

        final Dialog dialog = ProgressDialogUtil.show(mContext, null, false);

        call.enqueue(new CustomJsonHttpResponseHandler(mContext, call.toString()) {
            @Override
            public void onResponse(int resultCode, String resultMessage, JSONObject json) {
                super.onResponse(resultCode, resultMessage, json);


                ProgressDialogUtil.dismiss(dialog);

                LogUtil.d("resultCode : " + resultCode);
                LogUtil.d("resultMessage : " + resultMessage);
                LogUtil.json(json);

                if (resultCode != 0) {
                    new Alert().showAlert(mContext, resultMessage);
                    return;
                }


                Alert alert = new Alert();
                alert.setOnCloseListener(new InterfaceSet.OnCloseListener() {
                    @Override
                    public void onClose(DialogInterface dialog, int which) {
                        if (reqDialog != null) {
                            reqDialog.dismiss();
                        }

                        if (_closeListener != null) {
                            _closeListener.onClose(reqDialog, Alert.BUTTON1);
                        }
                    }
                });
                alert.showAlert(mContext, resultMessage);

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                super.onFailure(call, t);
                ProgressDialogUtil.dismiss(dialog);
                LogUtil.d("onFailure");
            }
        });
    }


    private long _newspic_start = -1;

    private long _newspic_view_seconds = -1;
    private int _newspic_no = -1;

    public void showNewspicAd(final Context context, int newspic_no, String newspic_url) {
        mContext = context;

        int seconds_view_adpick = SPUtil.getInstance().getSecondsViewAdpick(mContext);

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_newspic_ad);

        TextView txtDesc = dialog.findViewById(R.id.txtDesc);
        Button btnReadNews = (Button) dialog.findViewById(R.id.btnReadNews);
        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);


        txtDesc.setText("이동하는 화면에서\n" + seconds_view_adpick + "초 이상 뉴스를 보시면,\n 투표권과 포인트를 적립해드립니다.\n\n진행하시겠습니까?");


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        btnReadNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (_closeListener != null) {
//                    _closeListener.onClose(dialog, Alert.BUTTON1);
//                }
                //dialog.dismiss();

                _newspic_no = newspic_no;
                _newspic_start = System.currentTimeMillis();

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newspic_url));
                context.startActivity(intent);

                btnReadNews.setEnabled(false);
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_newspic_start < 1) {
                    new Alert().showAlert(context, "뉴스를 먼저 읽어주세요.");
                    return;
                }

                long newspic_end = System.currentTimeMillis();

                int a = (int) ((newspic_end - _newspic_start) / 1000);

                int seconds_view_adpick = SPUtil.getInstance().getSecondsViewAdpick(mContext);

                if (a <= seconds_view_adpick) {
                    _newspic_start = System.currentTimeMillis();
                    Alert.toastLong(context, seconds_view_adpick + "초 이상 기사를 읽어주세요.");
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newspic_url));
                    context.startActivity(intent);
                    return;
                }

                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public void showPaymentPoint(final Context context, String msg) {
        mContext = context;

        int seconds_view_adpick = SPUtil.getInstance().getSecondsViewAdpick(mContext);

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_payment_point);

        ImageButton btnClose = (ImageButton) dialog.findViewById(R.id.btnClose);
        TextView txtDesc = (TextView) dialog.findViewById(R.id.txtDesc);
        Button btnPayPoint = (Button) dialog.findViewById(R.id.btnPayPoint);
        Button btnPayInApp = (Button) dialog.findViewById(R.id.btnPayInApp);

        txtDesc.setText(msg);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        btnPayPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });
        btnPayInApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON2);
                }
                dialog.dismiss();
            }
        });


        dialog.setCancelable(false);
        dialog.show();
    }


    public void showFloating(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_floating);

        Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


    public void showFloatingVote(final Context context) {
        mContext = context;

        final Dialog dialog = new Dialog(context, android.R.style.Theme_Translucent_NoTitleBar);
        dialog.setContentView(R.layout.alert_floating_vote);

        View baseBtn01 = dialog.findViewById(R.id.baseBtn01);
        View baseBtn02 = dialog.findViewById(R.id.baseBtn02);
        View baseBtn03 = dialog.findViewById(R.id.baseBtn03);

        ImageView btnClose = dialog.findViewById(R.id.btnClose);

        baseBtn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON1);
                }
                dialog.dismiss();
            }
        });

        baseBtn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON2);
                }
                dialog.dismiss();
            }
        });

        baseBtn03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.BUTTON3);
                }
                dialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_closeListener != null) {
                    _closeListener.onClose(dialog, Alert.CANCEL);
                }
                dialog.dismiss();
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }


}