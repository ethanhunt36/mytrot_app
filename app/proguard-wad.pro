-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-keepnames class * implements android.os.Parcelable {
    public static final ** CREATOR;
}

-keep class com.google.android.gms.ads.** { *; }
-dontwarn com.google.android.gms.ads.**

-dontwarn com.squareup.picasso.**
-dontwarn com.bumptech.glide.**

-keep class com.wafour.ads.sdk.** { *; }
-dontwarn com.wafour.**

-keep class com.tnkfactory.** { *;}
-keepnames class com.example.youtubeplayerview.Sample2Fragment

-keep public class com.nhn.android.naverlogin.** {
    public protected *;
}

-dontwarn com.facebook.infer.**
-dontwarn com.unity3d.ads.**
-dontwarn com.unity3d.services.**